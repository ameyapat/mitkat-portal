import { call, put, takeEvery } from 'redux-saga/effects';
import { GET_RIME_EVENT_PINS } from '../actions/rimeActions/constant';
import { getRimeFeed } from '../api/api';

function* fetchRimeEventPins(action){
    try{
        const response = yield call(getRimeFeed(action.payload));
    }catch(error){

    }
}

export function* rimeSaga(){
    yield takeEvery(GET_RIME_EVENT_PINS, fetchRimeEventPins)
}