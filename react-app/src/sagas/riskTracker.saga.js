import { takeEvery, put, call } from 'redux-saga/effects';
import {
  GET_RISK_ALERTS,
  fetchRiskSuccess,
  fetchRiskError,
} from '../actions/riskTrackerAction';
import { setEventDetails, setDefaultEventDetails } from '../actions';
import { getRiskAlerts } from '../api/api';

function* fetchRiskAlerts(action) {
  try {
    const response = yield call(getRiskAlerts, action.payload);
    // yield put(fetchRiskSuccess(response));
    yield put(setEventDetails(response));
    yield put(setDefaultEventDetails(response));
    return response;
  } catch (e) {
    yield put(fetchRiskError([]));
    return [];
  }
}

export function* riskTrackerSaga() {
  yield takeEvery(GET_RISK_ALERTS, fetchRiskAlerts);
}
