import { call, put, takeEvery, all } from 'redux-saga/effects';
import { UPLOAD_IMAGE, IMAGE_UPLOADED } from '../actions/uploadImageAction';
import { uploadImage } from '../api/api';
import {rimeSaga} from './rime.saga';
import { riskTrackerSaga } from './riskTracker.saga';

function* fetchUser(action){
    try{
        const temp = yield call(uploadImage(action.payload));
        yield put({ type: "IMAGE_UPLOADED", temp });
    }catch(e){
        yield put({ type: 'IMAGE_UPLOAD_FAILED' });
    }
}

function* mySaga(){
    yield takeEvery(UPLOAD_IMAGE, fetchUser);
}


function* rootSaga(){
    yield all([
        mySaga(),
        rimeSaga(),
        riskTrackerSaga()
    ])
}

export default rootSaga;

