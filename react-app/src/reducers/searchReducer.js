import { TOGGLE_SEARCH_BAR } from '../actions/searchActions';

const initialState = {
    showSearchBar: false,
}

const searchReducer = (state = initialState, action) => {
    switch(action.type){
        case TOGGLE_SEARCH_BAR:
            return {...state, showSearchBar: action.payload};
        default:
            return state;
    }
}

export default searchReducer;