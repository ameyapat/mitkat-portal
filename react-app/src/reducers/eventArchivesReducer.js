import * as actionTypes from '../actions/actionTypes';

const initialArchiveFilters = {
    risklevel: [1,2,3,4,5],
    riskcategory: [1,2,3,4,5,6,7,8,9,10,11,12],
    selectedCountries: null,
    selectedStates: null,
    selectedCites: null,
    startDate: "2019-01-01",
    endDate:"2019-12-31"
}

const initialTor = {
        civil: true,
        cyber: true,
        crime: true,
        infrastructure: true,
        health: true,
        environment: true,
        insurgency: true,
        travelRisks: true,
        naturalDisasters: true,
        externalThreats: true,
        political: true,
        regulatory: true,
}

const initialLor = {
        verylow: true,
        low: true,
        medium: true,
        high: true,
        veryhigh: true
}

const initialState = {
    archiveFilters: {...initialArchiveFilters},
    pageNo: 0,
    typeOfRisk: {...initialTor},
    levelOfRisk: {...initialLor},
    selectAllTOR: true,
    selectAllLOR: true
}

export default function eventArchivesReducer(state = initialState, action){
    switch(action.type){
        case actionTypes.APPLY_ARCHIVE_FILTERS:
            state = {...state, archiveFilters: action.payload}
            return state;
        case actionTypes.RESET_PAGENO:
            state = {...state, pageNo: action.payload}
            return state;
        case actionTypes.RESET_EVENT_FILTERS:
            state = {
                ...state,
                 archiveFilters: initialArchiveFilters,
                 typeOfRisk: initialTor,
                 levelOfRisk: initialLor
                };
            return state;            
        case actionTypes.HANDLE_PAGE_NUM:
            state = {...state, pageNo: action.payload};
            return state;     
        case actionTypes.SET_TOR_FILTERS:
            state = {...state, typeOfRisk: action.payload};
            return state;                   
        case actionTypes.SET_LOR_FILTERS:
            state = {...state, levelOfRisk: action.payload};
            return state; 
        case actionTypes.HANDLE_SELECT_ALL_ARCHIVES_TOR:
            return {...state, selectAllTOR: action.payload};
        case actionTypes.HANDLE_SELECT_ALL_ARCHIVES_LOR:
            return {...state, selectAllLOR: action.payload};
        default:
            return state;
    }
}