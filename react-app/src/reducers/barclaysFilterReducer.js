import moment from 'moment';
import { UPDATE_DASHBOARD_FILTERS } from '../actions/barclaysActions';

export const initialState = {
  filters: {
    startDate: moment()
      .startOf('year')
      .format('YYYY-DD-MM'),
    endDate: new Date(),
    selectedAssets: [],
    selectedCities: [],
    selectedCountries: [],
    selectedRiskLevel: [],
    selectedCategory: [],
    distance: 0,
  },
};

const barclaysFilterReducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_DASHBOARD_FILTERS:
      return { ...state, filters: action.payload };
    default:
      return state;
  }
};

export default barclaysFilterReducer;
