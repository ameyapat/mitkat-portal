import * as actionTypes from '../actions/actionTypes';

const initialState = {
  filters: {
    eventtense: [0, 1, 2],
    riskcategory: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
    risklevel: [1, 2, 3, 4, 5],
    region: [1, 2, 3, 4, 5],
    days: 1,
  },
  typeOfRegions: [],
  typeOfRisk: {
    civil: true,
    cyber: true,
    crime: true,
    infrastructure: true,
    health: true,
    environment: true,
    insurgency: true,
    travelRisks: true,
    naturalDisasters: true,
    externalThreats: true,
    political: true,
    regulatory: true,
  },
  levelOfRisk: {
    verylow: true,
    low: true,
    medium: true,
    high: true,
    veryhigh: true,
  },
  typeOfEvent: {
    upcoming: true,
    live: true,
    recent: true,
  },
  eventDetails: [],
  defaultEventDetails: [],
  hourValue: 1,
  selectAllTOE: true,
  selectAllTOR: true,
  selectAllLOR: true,
  selectAllRegions: true,
  watchList: {},
};

function mapStateReducer(state = initialState, action) {
  switch (action.type) {
    case actionTypes.SET_FILTERS:
      state = { ...state, filters: action.payload };
      return state;
    case actionTypes.SET_REGIONS:
      return { ...state, typeOfRegions: action.payload };
    case actionTypes.SET_SELECT_ALL_REGIONS:
      return { ...state, selectAllRegions: action.payload };
    case actionTypes.SET_TOR:
      state = { ...state, typeOfRisk: action.payload };
      return state;
    case actionTypes.SET_LOR:
      state = { ...state, levelOfRisk: action.payload };
      return state;
    case actionTypes.SET_TOE:
      state = { ...state, typeOfEvent: action.payload };
      return state;
    case actionTypes.SET_DAYS:
      state = { ...state, hourValue: action.payload };
      return state;
    case actionTypes.SET_EVENT_DETAILS:
      state = {
        ...state,
        eventDetails: action.payload,
      };
      return state;
    case actionTypes.SET_DEFAULT_EVENT_DETAILS:
      state = {
        ...state,
        defaultEventDetails: action.payload,
      };
      return state;
    case actionTypes.HANDLE_SELECT_ALL_TOE:
      return { ...state, selectAllTOE: action.payload };
    case actionTypes.HANDLE_SELECT_ALL_TOR:
      return { ...state, selectAllTOR: action.payload };
    case actionTypes.HANDLE_SELECT_ALL_LOR:
      return { ...state, selectAllLOR: action.payload };
    case actionTypes.UPDATE_WATCH_LIST:
      return { ...state, watchList: action.payload };
    default:
      return state;
  }
}

export default mapStateReducer;
