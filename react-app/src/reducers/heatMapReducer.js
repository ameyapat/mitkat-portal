import {
  SET_BREADCRUMBS,
  SET_REGION_DATA,
  SET_SELECTED_REGION,
  RESET_SELECTED_REGION,
  SET_HEAT_MAP_OFFICE_LOCATIONS,
} from '../actions/heatMapAction';

let initialState = {
  breadcrumbArray: [],
  regionDetails: {},
  selectedRegion: {},
  officeLocations: [],
};

export default function heatMapReducer(state = initialState, action) {
  switch (action.type) {
    case SET_REGION_DATA:
      return { ...state, regionDetails: action.payload };
    case SET_BREADCRUMBS:
      return { ...state, breadcrumbArray: action.payload };
    case SET_SELECTED_REGION:
      const catId = action.payload.catId;
      const categoryMeta = action.payload.catMeta;
      const selectedRegion = categoryMeta[catId][0];

      return { ...state, selectedRegion };
    case RESET_SELECTED_REGION:
      return { ...state, selectedRegion: action.payload };
    case SET_HEAT_MAP_OFFICE_LOCATIONS:
      return { ...state, officeLocations: action.payload };
    default:
      return state;
  }
}
