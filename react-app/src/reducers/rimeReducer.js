import {
  SET_BOUNDS,
  SET_MANUAL_BOUNDS,
  UPDATE_CATEGORIES,
  LOADING_FEED,
  SHOW_LOCATION_PIN,
  UPDATE_RELEVANCY,
  UPDATE_EVENT_TYPE,
  UPDATE_FEED,
  UPDATE_SELECT_ALL_RISK_CAT,
  UPDATE_SELECT_ALL_RELEVANCY,
  UPDATE_SELECT_ALL_LANGUAGE,
  SET_LIVE_LIST_ITEM_DETAILS,
  UPDATE_LOCATIONS,
  UPDATE_REFRESH_DURATION,
  UPDATE_OPERATIONS,
  UPDATE_TAGS,
  UPDATE_START_DATE,
  UPDATE_END_DATE,
  UPDATE_LANGUAGE,
  SEARCH_FEED,
  UPDATE_SEARCH_FEED,
  UPDATE_ADMIN_SELECTED_FEED,
  LIVE_MONITOR_SEARCH_FEED,
  UPDATE_LIVE_MONITOR_SEARCH_FEED,
  UPDATE_EVENT_SEARCH_CATEGORIES,
  UPDATE_EVENT_SEARCH_LOCATIONS,
  UPDATE_EVENT_SEARCH_OPERATIONS,
  UPDATE_EVENT_SEARCH_RELEVANCY,
  UPDATE_EVENT_SEARCH_TAGS,
  UPDATE_EVENT_SEARCH_LANGUAGE,
  UPDATE_EVENT_SEARCH_START_DATE,
  UPDATE_EVENT_SEARCH_END_DATE,
  UPDATE_EVENT_SEARCH_SELECT_ALL_RISK_CAT,
  UPDATE_EVENT_SEARCH_SELECT_ALL_RELEVANCY,
  UPDATE_EVENT_SEARCH_SELECT_ALL_LANGUAGE,
  GET_RIME_SUBSCRIPTION_DATA,
  SHOW_API_HIT_TIME,
  SHOW_GEOFENCE_EVENT_NOS,
} from '../actions/rimeActions/constant';
import * as actionTypes from '../actions/actionTypes';
import {
  startDate,
  endDate,
  eventSearchStartDate,
  eventSearchEndDate,
} from './constants';

const initialBounds = {
  northeast: {
    lat: '90',
    lng: '180',
  },
  southwest: {
    lat: '-90',
    lng: '-180',
  },
};
const initialManualBounds = {
  latMin: '-90',
  latMax: '90',
  lngMin: '-180',
  lngMax: '180',
};
const initialState = {
  isLoading: false,
  riskCategories: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
  relevancy: [1, 2, 3],
  locations: [],
  eventType: [],
  refreshDuration: 60,
  tags: '',
  operations: 'or',
  language: [
    'en',
    'hi',
    'zh',
    'ar',
    'es',
    'ur',
    'mr',
    'fr',
    'bn',
    'pt',
    'id',
    'ml',
    'ta',
    'kn',
    'ms',
    'th',
    'vi',
  ],
  eventSearchriskCategories: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
  eventSearchrelevancy: [1, 2, 3],
  eventSearchlocations: [],
  eventSearchtags: '',
  eventSearchoperations: 'or',
  eventSearchLanguage: [
    'en',
    'hi',
    'zh',
    'ar',
    'es',
    'ur',
    'mr',
    'fr',
    'bn',
    'pt',
    'id',
    'ml',
    'ta',
    'kn',
    'ms',
    'th',
    'vi',
  ],
  startDate: startDate,
  endDate: endDate,
  eventSearchStartDate: eventSearchStartDate,
  eventSearchEndDate: eventSearchEndDate,
  placeBounds: initialBounds,
  manualPlaceBounds: initialManualBounds,
  showLocationPin: true,
  showRimeFilter: false,
  showRimeEventArchives: false,
  showSettings: false,
  lockScreen: true,
  updatedFeed: [],
  updateAdminSelectedFeed: [],
  searchFeed: [],
  updateSearchFeed: [],
  liveMonitorSearchFeed: [],
  updatedLiveMonitorSearchFeed: [],

  selectAllRiskCat: true,
  selectAllRelevancy: true,
  selectAllLanguage: true,
  eventSearchSelectAllRiskCat: true,
  eventSearchSelectAllRelevancy: true,
  eventSearchSelectAllLanguage: true,
  subscriptionData: [],
  geoFenceNumbers: 0,
  liveListItemDetails: {
    showMarker: false,
  },
  apiHitTime: true,
};

export default function rimeReducer(state = initialState, action) {
  switch (action.type) {
    case SET_BOUNDS:
      return { ...state, placeBounds: action.payload };
    case SET_MANUAL_BOUNDS:
      return { ...state, manualPlaceBounds: action.payload };
    case UPDATE_CATEGORIES:
      return { ...state, riskCategories: action.payload };
    case UPDATE_RELEVANCY:
      return { ...state, relevancy: action.payload };
    case UPDATE_EVENT_TYPE:
      return { ...state, eventType: action.payload };
    case UPDATE_LOCATIONS:
      return { ...state, locations: action.payload };
    case UPDATE_REFRESH_DURATION:
      return { ...state, refreshDuration: action.payload };
    case UPDATE_OPERATIONS:
      return { ...state, operations: action.payload };
    case UPDATE_START_DATE:
      return { ...state, startDate: action.payload };
    case UPDATE_END_DATE:
      return { ...state, endDate: action.payload };
    case UPDATE_TAGS:
      return { ...state, tags: action.payload };
    case UPDATE_LANGUAGE:
      return { ...state, language: action.payload };
    case SEARCH_FEED:
      return { ...state, searchFeed: action.payload };
    case UPDATE_SEARCH_FEED:
      return { ...state, updateSearchFeed: action.payload };
    case UPDATE_ADMIN_SELECTED_FEED:
      return { ...state, updateAdminSelectedFeed: action.payload };
    case LIVE_MONITOR_SEARCH_FEED:
      return { ...state, liveMonitorSearchFeed: action.payload };
    case UPDATE_LIVE_MONITOR_SEARCH_FEED:
      return { ...state, updatedLiveMonitorSearchFeed: action.payload };
    case LOADING_FEED:
      return { ...state, isLoading: action.payload };
    case SHOW_LOCATION_PIN:
      return { ...state, showLocationPin: action.payload };
    case actionTypes.TOGGLE_RIME_FILTER:
      return { ...state, showRimeFilter: action.payload };
    case actionTypes.TOGGLE_RIME_EVENT_AECHIVES:
      return { ...state, showRimeEventArchives: action.payload };
    case actionTypes.TOGGLE_SHOW_SETTINGS:
      return { ...state, showSettings: action.payload };
    case actionTypes.TOGGLE_LOCK_SCREEN:
      return { ...state, lockScreen: action.payload };
    case UPDATE_FEED:
      return { ...state, updatedFeed: [...action.payload] };
    case UPDATE_SELECT_ALL_RISK_CAT:
      return { ...state, selectAllRiskCat: action.payload };
    case UPDATE_SELECT_ALL_RELEVANCY:
      return { ...state, selectAllRelevancy: action.payload };
    case UPDATE_SELECT_ALL_LANGUAGE:
      return { ...state, selectAllLanguage: action.payload };
    case SET_LIVE_LIST_ITEM_DETAILS:
      return { ...state, liveListItemDetails: action.payload };
    case UPDATE_EVENT_SEARCH_CATEGORIES:
      return { ...state, eventSearchriskCategories: action.payload };
    case UPDATE_EVENT_SEARCH_RELEVANCY:
      return { ...state, eventSearchrelevancy: action.payload };
    case UPDATE_EVENT_SEARCH_LOCATIONS:
      return { ...state, eventSearchlocations: action.payload };
    case UPDATE_EVENT_SEARCH_OPERATIONS:
      return { ...state, eventSearchoperations: action.payload };
    case UPDATE_EVENT_SEARCH_TAGS:
      return { ...state, eventSearchtags: action.payload };
    case UPDATE_EVENT_SEARCH_LANGUAGE:
      return { ...state, eventSearchLanguage: action.payload };
    case UPDATE_EVENT_SEARCH_START_DATE:
      return { ...state, eventSearchStartDate: action.payload };
    case UPDATE_EVENT_SEARCH_END_DATE:
      return { ...state, eventSearchEndDate: action.payload };
    case UPDATE_EVENT_SEARCH_SELECT_ALL_RISK_CAT:
      return { ...state, eventSearchSelectAllRiskCat: action.payload };
    case UPDATE_EVENT_SEARCH_SELECT_ALL_RELEVANCY:
      return { ...state, eventSearchSelectAllRelevancy: action.payload };
    case UPDATE_EVENT_SEARCH_SELECT_ALL_LANGUAGE:
      return { ...state, eventSearchSelectAllLanguage: action.payload };
    case GET_RIME_SUBSCRIPTION_DATA:
      return { ...state, subscriptionData: action.payload };
    case SHOW_API_HIT_TIME:
      return { ...state, apiHitTime: action.payload };
    case SHOW_GEOFENCE_EVENT_NOS:
      return { ...state, geoFenceNumbers: action.payload };
    default:
      return state;
  }
}
