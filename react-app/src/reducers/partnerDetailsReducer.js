import { SET_PARTNER_DETAILS } from '../actions';

let initialState = {
  partnerDetails: {
    partnerName: '',
    darkLogo: '',
    whiteLogo: '',
    latitude: '',
    longitude: '',
    focus: '',
  },
};

export default function partnerDetailsReducer(state = initialState, action) {
  switch (action.type) {
    case SET_PARTNER_DETAILS:
      return { ...state, partnerDetails: action.payload };
    default:
      return state;
  }
}
