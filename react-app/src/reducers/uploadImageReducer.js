import { UPLOAD_IMAGE, IMAGE_UPLOADED } from '../actions/uploadImageAction';

const initialState = {
    data: {
        firstImage: {},
        secondImage: {},
        thirdImage: {},
    },
}

export default function uploadImageReducer(state = initialState, action){
    
    switch (action.type) {
      case UPLOAD_IMAGE:
        return null;
      case IMAGE_UPLOADED:
        return null;
      default:
        return state;
    }
}