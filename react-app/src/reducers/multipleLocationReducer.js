
import { UPDATE_MULTIPLE_LOCATION } from '../actions/multipleLocationActions'

const initialState = {
    selectedMultipleLocations: []
}

const multipleLocationReducer = (state = initialState, action) => {
    switch(action.type){
        case UPDATE_MULTIPLE_LOCATION:
            return {...state, selectedMultipleLocations: action.payload}
        default:
            return state;
    }
}

export default multipleLocationReducer