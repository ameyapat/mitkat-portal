import {
  SET_DASHBOARD_DATA,
  // TOGGLE_BARCLAYS_LOCATION,
  // TOGGLE_EVENT_AND_RISK_DATA,
  // TOGGLE_UPLOAD_MODAL,
} from '../actions/barclaysActions';

const initialState = {
  dashboardData: {
    eventlist: [],
    toprisks: [],
    riskcategorydata: [],
    riskleveldata: [],
    eventdistributiongraph: [],
    mapcoordinates: [],
    totalevents: '',
  },
  // toggleBarclaysLocation: true,
  // showUploads: false,
  // togglEventAndRiskData: {
  //   toggleEventList: true,
  //   toggleTopRiskEventList: true,
  //   toggleEventDistribution: true,
  //   toggleRiskLevelDistribution: true,
  //   toggleRiskCategoryDistribution: true,
  // },
};

const barclaysReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_DASHBOARD_DATA:
      return { ...state, dashboardData: action.payload };
    // case TOGGLE_BARCLAYS_LOCATION:
    //   return { ...state, toggleBarclaysLocation: action.payload };
    // case TOGGLE_UPLOAD_MODAL:
    //   return { ...state, showUploads: action.payload };
    // case TOGGLE_EVENT_AND_RISK_DATA:
    //   return { ...state, togglEventAndRiskData: action.payload };
    default:
      return state;
  }
};

export default barclaysReducer;
