import {
  UPDATE_DASHBOARD_EVENT_DETAILS,
  TOGGLE_SETTINGS_MODAL,
  TOGGLE_BARCLAYS_LOCATION,
  TOGGLE_EVENT_AND_RISK_DATA,
  TOGGLE_UPLOAD_MODAL,
} from '../actions/barclaysActions';

const initialState = {
  eventDetails: {},
  showSettings: false,
  showBarclaysLocation: true,
  showUploads: false,
  eventAndRiskData: {
    showEventList: true,
    showTopRiskEventList: true,
    showEventDistribution: true,
    showRiskLevelDistribution: true,
    showRiskCategoryDistribution: true,
  },
};

const barclaysEventDetailsReducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_DASHBOARD_EVENT_DETAILS:
      return { ...state, eventDetails: action.payload };
    case TOGGLE_SETTINGS_MODAL:
      return { ...state, showSettings: action.payload };
    case TOGGLE_BARCLAYS_LOCATION:
      return { ...state, showBarclaysLocation: action.payload };
    case TOGGLE_UPLOAD_MODAL:
      return { ...state, showUploads: action.payload };
    case TOGGLE_EVENT_AND_RISK_DATA:
      return { ...state, eventAndRiskData: action.payload };
    default:
      return state;
  }
};

export default barclaysEventDetailsReducer;
