import { SET_IMPACT_RADIUS } from '../actions/EventModalAction';

const initialState = {
  impactRadius: 1500,
};

export default function impactRadiusReducer(state = initialState, action) {
  switch (action.type) {
    case SET_IMPACT_RADIUS:
      return { ...state, impactRadius: action.payload };
    default:
      return state;
  }
}
