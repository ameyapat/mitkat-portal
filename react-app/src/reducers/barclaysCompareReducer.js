import {
  SET_DASHBOARD_DATA_LEFT,
  UPDATE_DASHBOARD_FILTERS_LEFT,
  SET_DASHBOARD_DATA_RIGHT,
  UPDATE_DASHBOARD_FILTERS_RIGHT,
} from '../actions/barclaysActions';

import moment from 'moment';

export const initialState = {
  filtersLeft: {
    startDate: moment()
      .startOf('year')
      .format('YYYY-DD-MM'),
    endDate: new Date(),
    selectedAssets: [],
    selectedCities: [],
    selectedCountries: [],
    selectedRiskLevel: [],
    selectedCategory: [],
    distance: 0,
  },
  filtersRight: {
    startDate: moment()
      .startOf('year')
      .format('YYYY-DD-MM'),
    endDate: new Date(),
    selectedAssets: [],
    selectedCities: [],
    selectedCountries: [],
    selectedRiskLevel: [],
    selectedCategory: [],
    distance: 0,
  },
  dashboardDataLeft: {
    eventlist: [],
    toprisks: [],
    riskcategorydata: [],
    riskleveldata: [],
    eventdistributiongraph: [],
    mapcoordinates: [],
    totalevents: '',
  },
  dashboardDataRight: {
    eventlist: [],
    toprisks: [],
    riskcategorydata: [],
    riskleveldata: [],
    eventdistributiongraph: [],
    mapcoordinates: [],
    totalevents: '',
  },
};

const barclaysCompareReducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_DASHBOARD_FILTERS_LEFT:
      return { ...state, filtersLeft: action.payload };
    case UPDATE_DASHBOARD_FILTERS_RIGHT:
      return { ...state, filtersRight: action.payload };
    case SET_DASHBOARD_DATA_LEFT:
      return { ...state, dashboardDataLeft: action.payload };
    case SET_DASHBOARD_DATA_RIGHT:
      return { ...state, dashboardDataRight: action.payload };
    default:
      return state;
  }
};

export default barclaysCompareReducer;
