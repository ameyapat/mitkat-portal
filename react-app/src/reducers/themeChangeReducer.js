import { THEME_CHANGE_TOGGLE } from '../actions/themeAction';

const initialState = {
  setDarkTheme: true,
};

const themeChangeReducer = (state = initialState, action) => {
  switch (action.type) {
    case THEME_CHANGE_TOGGLE:
      return { ...state, setDarkTheme: action.payload };
    default:
      return state;
  }
};

export default themeChangeReducer;
