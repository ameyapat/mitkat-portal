import * as actionTypes from '../actions/actionTypes';

let initialState = {
  authToken: '',
  showToast: false,
  toastMsg: '',
  isFetching: false,
  userType: 'admin',
  userRole: null,
  refreshEvents: '',
  isLoaded: true,
  enableMultipleLocations: false,
  isCovidDashAuth: false,
  appPermissions: {
    defaultsubscription: 1,
    querysubscription: 1,
    forecastsubscription: 0,
    coviddashboardsubscription: 0,
    vaccinationsubscription: 0,
    analyticssubscription: 0,
    rimesubscription: 0,
    locationPinSetting: 1,
    colorThemeSetting: 0,
    citybriefsubscription: 0,
    riskExposureSubscription: 1,
    riskTrackerSubscription: 1,
    // additional options
    resources: 1,
    calendar: 1,
    firstViewId: 1,
    home: 1,
    archives: 1,
    events: 1,
    monitor: 1,
    eventsearch: 1,
    rimeSubscriptions: 1,
    locationAnalysis: 1,
    clientLocationAnalysis: 1,
    topicAnalaysis: 1,
    comingSoon: 1,
    settings: 1,
  },
  currentHeaderTitle: '',
  restrictAccess: false,
};

export default function appStateReducer(state = initialState, action) {
  switch (action.type) {
    case actionTypes.SET_AUTH_TOKEN:
      state = Object.assign({ ...state, authToken: action.payload });
      return state;
    case actionTypes.TOGGLE_TOAST:
      state = Object.assign({
        ...state,
        showToast: action.payload.showToast,
        toastMsg: action.payload.toastMsg,
      });
      return state;
    case actionTypes.TOGGLE_IS_FETCHING:
      state = { ...state, isFetching: action.payload };
      return state;
    case actionTypes.TOGGLE_USER_TYPE:
      state = { ...state, userType: action.payload };
      return state;
    case actionTypes.SET_USER_ROLES:
      return (state = { ...state, userRole: action.payload });
    case actionTypes.REFRESH_EVENTS:
      return (state = { ...state, refreshEvents: action.payload });
    case actionTypes.IS_LOADED:
      return (state = { ...state, isLoaded: action.payload });
    case actionTypes.SET_IS_COVID_DASH_AUTH:
      return (state = { ...state, isCovidDashAuth: action.payload });
    case actionTypes.SET_APP_PERMISSION:
      return {
        ...state,
        appPermissions: { ...state.appPermissions, ...action.payload },
      };
    case actionTypes.SET_HEADER:
      return { ...state, currentHeaderTitle: action.payload };
    case actionTypes.RESTRICT_ACCESS:
      return { ...state, restrictAccess: action.payload };
    default:
      return state;
  }
}
