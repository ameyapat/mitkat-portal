let myDate = new Date();
myDate.setDate(myDate.getDate());

export const startDate =
  ('0' + myDate.getMonth()).slice(-2) +
  '/' +
  ('0' + myDate.getDate()).slice(-2) +
  '/' +
  myDate.getFullYear();

export const endDate =
  ('0' + (myDate.getMonth() + 1)).slice(-2) +
  '/' +
  ('0' + myDate.getDate()).slice(-2) +
  '/' +
  myDate.getFullYear();

export const eventSearchStartDate =
  ('0' + (myDate.getMonth() + 1)).slice(-2) +
  '/' +
  ('0' + (myDate.getDate() + 1)).slice(-2) +
  '/' +
  (myDate.getFullYear() - 1);

export const eventSearchEndDate =
  ('0' + (myDate.getMonth() + 1)).slice(-2) +
  '/' +
  ('0' + myDate.getDate()).slice(-2) +
  '/' +
  myDate.getFullYear();
