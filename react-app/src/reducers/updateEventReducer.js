import * as actionType from '../actions/actionTypes';

const initialState = {
    isTitleEditable: false,
    title: '',
    id: '',
    selectedTrackers: [],
    trackers: []
}

export default function updateEventReducer(state = initialState, action) {
    switch (action.type) {
      case actionType.SET_UPDATE_EDITABLE:
        state = { ...state, isTitleEditable: action.payload };
        return state;
      case actionType.UPDATE_EVENT_TITLE:
        state = Object.assign({
          ...state,
          isTitleEditable: action.payload.isTitleEditable,
          title: action.payload.title,
          id: action.payload.id,
        });
        return state;
      case actionType.SET_TRACKER_DETAILS:
        return {
          ...state,
          selectedTrackers: action.payload.selectedTrackers || [],
          trackers: action.payload.trackers || [],
        };
      default:
        return state;
    }
}