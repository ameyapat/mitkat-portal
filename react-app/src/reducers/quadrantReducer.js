import {
  SET_QUADRANT_LIST,
  UPDATE_QUADRANT_DETAIL,
  UPDATE_QUADRANT_POINTS,
  UPDATE_QUADRANTS,
  SET_SELECTED_QUADRANT,
  SET_SELECTED_EVENT_ITEM,
  UPDATE_FILTER_OPTIONS,
  UPDATE_REGION,
} from '../actions/quadrantActions';

const initialState = {
  quadrantEventList: [],
  quadrantDetails: {
    id: null,
    title: '',
    topicid: null,
    riskCategoryid: null,
    likelihood: null,
    impact: null,
    regionScale: null,
    forecast: '',
  },
  quadrantPoints: [],
  quadrantPointDetails: {
    month: new Date(),
    year: new Date(),
    likelihood: 0,
    impact: 0,
    regionScale: 0,
    development: '',
    interplay: '',
  },
  quadrants: {},
  selectedQuadrant: 'Forecast',
  selectedEventItem: {},
  kfilters: {
    categories: [],
    regions: [],
    selectedRegionIds: [],
    selectedCategoryIds: [],
    selectedRegion: {
      id: 3,
      region_name: 'APAC',
    },
  },
};

const quadrantReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_QUADRANT_LIST:
      return { ...state, quadrantEventList: action.payload };
    case UPDATE_QUADRANT_DETAIL:
      return { ...state, quadrantDetails: action.payload };
    case UPDATE_QUADRANT_POINTS:
      return { ...state, quadrantPoints: action.payload };
    case UPDATE_QUADRANTS:
      return { ...state, quadrants: action.payload };
    case SET_SELECTED_QUADRANT:
      return { ...state, selectedQuadrant: action.payload };
    case SET_SELECTED_EVENT_ITEM:
      return { ...state, selectedEventItem: action.payload };
    case UPDATE_FILTER_OPTIONS:
      return { ...state, kfilters: action.payload };
    case UPDATE_REGION:
      return {
        ...state,
        kfilters: { ...state.kfilters, selectedRegion: action.payload },
      };
    default:
      return state;
  }
};

export default quadrantReducer;
