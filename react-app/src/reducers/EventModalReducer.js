import { OPEN_EVENT_MODAL } from '../actions/EventModalAction';

const initialState = {
  showEventModal: false,
  eventId: null,
};

export default function EventModalReducer(state = initialState, action) {
  switch (action.type) {
    case OPEN_EVENT_MODAL:
      return {
        ...state,
        showEventModal: action.payload.showEventModal,
        eventId: action.payload.eventId,
      };
    default:
      return state;
  }
}
