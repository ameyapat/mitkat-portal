import { combineReducers } from 'redux';
import appStateReducer from './appStateReducer';
import mapStateReducer from './mapStateReducer';
import eventArchivesReducer from './eventArchivesReducer';
import updateEventReducer from './updateEventReducer';
import uploadImageReducer from './uploadImageReducer';
import rimeReducer from './rimeReducer';
import eventModalReducer from './EventModalReducer';
import RiskTrackerReducer from './riskTrackerReducer';
import searchReducer from './searchReducer';
import multipleLocationReducer from './multipleLocationReducer';
import barclaysReducer from './barclaysReducer';
import barclaysEventDetailsReducer from './barclaysEventDetailsReducer';
import barclaysFilterReducer from './barclaysFilterReducer';
import quadrantReducer from './quadrantReducer';
import barclaysCompareReducer from './barclaysCompareReducer';
import riskExposureReducer from './riskExposureReducer';
import empTrackingReducer from './empTrackingReducer';
import impactRadiusReducer from './impactRadiusReducer';
import partnerDetailsReducer from './partnerDetailsReducer';
import topicAnalysisReducer from './topicAnalysisReducer';
import heatMapReducer from './heatMapReducer';
import themeChangeReducer from './themeChangeReducer';
import keywordMonitorReducer from './keywordMonitorReducer';

let rootReducer = combineReducers({
  appState: appStateReducer,
  mapState: mapStateReducer,
  eventArchiveState: eventArchivesReducer,
  updateEvent: updateEventReducer,
  uploadImage: uploadImageReducer,
  rime: rimeReducer,
  eventModal: eventModalReducer,
  riskTracker: RiskTrackerReducer,
  search: searchReducer,
  multipleLocations: multipleLocationReducer,
  barclays: barclaysReducer,
  barclaysEventDetails: barclaysEventDetailsReducer,
  barclaysFilter: barclaysFilterReducer,
  quadrantState: quadrantReducer,
  barclaysCompare: barclaysCompareReducer,
  riskExposure: riskExposureReducer,
  empTracking: empTrackingReducer,
  impactRadius: impactRadiusReducer,
  partnerDetails: partnerDetailsReducer,
  topicAnalysis: topicAnalysisReducer,
  heatMapDetails: heatMapReducer,
  themeChange: themeChangeReducer,
  keywordMonitor: keywordMonitorReducer,
});

export default rootReducer;
