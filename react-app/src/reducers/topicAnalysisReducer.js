import {
  SET_TOPIC_ANALYSIS_DATA,
  SET_SELECTED_ITEM,
} from '../actions/topicAnalysisActions';

const initialState = {
  topicAnalysisData: {
    clientlocationGraph: [],
    impactLevelGraph: [],
    locationRatingList: [],
    quadrant: [],
    resources: [],
    timelapse: [],
    timeline: [],
    selectedItem: {},
  },
};

const topicAnalysisReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_TOPIC_ANALYSIS_DATA:
      return { ...state, topicAnalysisData: action.payload };
    case SET_SELECTED_ITEM:
      return { ...state, selectedItem: action.payload };
    default:
      return state;
  }
};

export default topicAnalysisReducer;
