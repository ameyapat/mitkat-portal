import { SET_EMP_TRACKING_DATA } from '../actions/employeeTrackingActions';

const initialState = {
  empTrackingData: {
    title: '',
    risklevel: '',
    riskcategory: '',
    riskLevelString: '',
    riskCategoryString: '',
    impactRadius: '',
    affectedEmployees: [],
    affectedOffices: [],
    reportLocations: [],
  },
};

const empTrackingReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_EMP_TRACKING_DATA:
      return { ...state, empTrackingData: action.payload };
    default:
      return state;
  }
};

export default empTrackingReducer;
