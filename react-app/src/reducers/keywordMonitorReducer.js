import { SET_QUERY_NEWS_DATA } from '../actions/keywordMonitorAction';

const initialState = {
  queryNewsData: {
    totalResults: '',
    articles: [],
    pageNumber: 0,
  },
};

const keywordMonitorReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_QUERY_NEWS_DATA:
      return { ...state, queryNewsData: action.payload };
    default:
      return state;
  }
};

export default keywordMonitorReducer;
