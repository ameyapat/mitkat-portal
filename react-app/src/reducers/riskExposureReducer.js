import { SET_RISK_DASHBOARD_DATA } from '../actions/riskExposureActions';

const initialState = {
  riskExposureData: {
    locations: [],
    organizationriskrating: '',
    riskCategoryRadar: [],
    topRisks: [],
    topics: [],
    locationDistribution: [],
    totalEvents: '',
    totalLocations: '',
  },
};

const riskExposureReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_RISK_DASHBOARD_DATA:
      return { ...state, riskExposureData: action.payload };
    default:
      return state;
  }
};

export default riskExposureReducer;
