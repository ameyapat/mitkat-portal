import { TOGGLE_RISK_TRACKER_INFO } from '../actions/actionTypes';
import { GET_RISK_ALERTS_SUCCESS, GET_RISK_ALERTS_ERROR, GET_RISK_ALERTS_CLEAR, TOGGLE_ENABLE_SEARCH  } from '../actions/riskTrackerAction';

const initialState = {
    riskTrackerDetails: {},
    eventList: [],
    pauseInterval: false,
}

const RiskTrackerReducer  = (state = initialState, action) => {
    switch(action.type){
        case TOGGLE_RISK_TRACKER_INFO:
            return {...state, riskTrackerDetails: action.payload.riskTrackerDetails};
        case GET_RISK_ALERTS_SUCCESS:
            return {...state, eventList: action.payload}
        case GET_RISK_ALERTS_ERROR:
            return {...state, eventList: action.payload}
        case GET_RISK_ALERTS_CLEAR:
            return {...state, eventList: action.payload}
        case TOGGLE_ENABLE_SEARCH:
            return {...state, pauseInterval: action.payload}
        default:
            return state;
    }
}

export default RiskTrackerReducer;