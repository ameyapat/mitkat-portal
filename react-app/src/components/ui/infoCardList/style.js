export default {
  rootInfoCardList: {
    display: 'flex',
    padding: '20px 15px',
    flexWrap: 'wrap',
    overflow: 'auto',
    background: 'var(--backgroundSolid)',
  },
};
