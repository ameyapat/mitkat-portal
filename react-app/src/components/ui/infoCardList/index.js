import React, { Component } from 'react';
import injectSheet from 'react-jss';
import styles from './style';
//
import clsx from 'clsx';
import './InfoCardList.scss';

class InfoCardList extends Component {

    state = {
        dyHeight: null
    }

    componentDidMount(){
        const windowHeight = window.innerHeight;
        const rootInfoList = document.getElementById('rootInfoList').getBoundingClientRect();
        const dyHeight = windowHeight - rootInfoList.top;
        this.setState({dyHeight});
    }

    render() {
        let { classes, classNames, isClient = false } = this.props;
        let { dyHeight } = this.state;
        return (
            <div id="rootInfoList" style={{height: `${dyHeight}px`}} className={clsx(classes.rootInfoCardList, isClient && classNames.root)}>
                {this.props.children}
            </div>
        )
    }
}

export default injectSheet(styles)(InfoCardList)
