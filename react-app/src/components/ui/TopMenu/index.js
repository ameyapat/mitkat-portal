import React, { Component } from 'react';
import './style.scss';
import logo from '../../../assets/MitKat_new_logo.png';
import { withCookies } from 'react-cookie';
import { NavLink, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { toggleSettingsModal } from '../../../actions/barclaysActions';
import ModalWithPortal from '../modalWithPortal/ModalWithPortal';
import CustomModal from '../modal';
import { FormControlLabel, Switch } from '@material-ui/core';
import Tooltip from '@material-ui/core/Tooltip';
import styles from './style';
import { withStyles } from '@material-ui/core/styles';

@withStyles(styles)
@withRouter
@withCookies
class TopMenu extends Component {
  handleLogout = () => {
    let { cookies } = this.props;
    cookies.remove('authToken');
    cookies.remove('userRole');
    setTimeout(() => {
      this.props.history.push('/');
    }, 2000);
  };

  handleOpenSettings = () => {
    this.props.dispatch(toggleSettingsModal(true));
  };
  closeSettings = () => {
    this.props.dispatch(toggleSettingsModal(false));
  };

  render() {
    const {
      showClientLogo = false,
      classes,
      toggleSwitchItems,
      toggleOfficeLocation,
    } = this.props;
    const {
      showSettings,
      eventAndRiskData: {
        showEventList,
        showTopRiskEventList,
        showEventDistribution,
        showRiskLevelDistribution,
        showRiskCategoryDistribution,
      },
      showBarclaysLocation,
    } = this.props.barclaysEventDetails;

    return (
      <>
        <div id="topMenu">
          {showClientLogo && (
            <div className="leftGrid rootViewSwitch__Grid">
              <div className="logo__wrap client__logo">
                <img src={this.props.clientLogo} alt="barclays" />
              </div>
            </div>
          )}
          <div className="leftGrid rootViewSwitch__Grid">
            <div className="logo__wrap">
              <img src={logo} alt="mitkat-logo-transparent" />
            </div>
          </div>

          <div className="rightGrid filter rootViewSwitch__Grid">
            <Tooltip title="Filter">
              <button
                className="btnEllipsis"
                onClick={this.props.handleOpenFilter}
              >
                <i className="fas fa-filter"></i>
              </button>
            </Tooltip>
          </div>

          <div className="rightGrid rootViewSwitch__Grid">
            <Tooltip title="Settings">
              <button className="btnEllipsis" onClick={this.handleOpenSettings}>
                <i className="fas fa-cogs"></i>
              </button>
            </Tooltip>
          </div>
          <div className="rightGrid rootViewSwitch__Grid">
            <Tooltip title="Logout">
              <button className="btnAvatar" onClick={this.handleLogout}>
                <i className="fas fa-sign-out-alt"></i>
              </button>
            </Tooltip>
          </div>
          <div className="rightGrid rootViewSwitch__Grid">
            <Tooltip title="Profile">
              <NavLink
                exact
                to="/barclays/admin"
                target="_blank"
                className="btnAvatar"
              >
                <i className="fas fa-user-circle"></i>
              </NavLink>
            </Tooltip>
          </div>
          <div className="rightGrid rootViewSwitch__Grid">
            <Tooltip title="Compare">
              <NavLink
                exact
                to="/barclays/dashboard/compare"
                target="_blank"
                className="btnAvatar"
              >
                {/* <i class="fas fa-balance-scale"></i> */}
                {/** @rinku when you use an external html tag, please check if class is changed to className remove this commet later */}
                <i className="fas fa-clone"></i>
              </NavLink>
            </Tooltip>
          </div>
        </div>
        <ModalWithPortal>
          <CustomModal showModal={showSettings}>
            <div id="rimeUserSettings">
              <div className="userSettings__header">
                <h1>Settings</h1>
                <button className="btn--close" onClick={this.closeSettings}>
                  &times;
                </button>
              </div>
              <div className="userSettings__body">
                <div className="userSettings_toggles">
                  <span className="userSettings__label">
                    Show Office Locations
                  </span>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={showBarclaysLocation}
                        onChange={() => toggleOfficeLocation()}
                        name="toggleBarclaysLocation"
                      />
                    }
                    classes={{
                      root: classes.rootControlLabel,
                      label: classes.rootLabelControlLabel,
                    }}
                  />
                </div>
                <div className="userSettings_toggles">
                  <span className="userSettings__label">Show Event List</span>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={showEventList}
                        onChange={() => toggleSwitchItems('showEventList')}
                        name="togglEventAndRiskData"
                      />
                    }
                    classes={{
                      root: classes.rootControlLabel,
                      label: classes.rootLabelControlLabel,
                    }}
                  />
                </div>
                <div className="userSettings_toggles">
                  <span className="userSettings__label">Show Top 5 event</span>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={showTopRiskEventList}
                        onChange={() =>
                          toggleSwitchItems('showTopRiskEventList')
                        }
                        name="togglEventAndRiskData"
                      />
                    }
                    classes={{
                      root: classes.rootControlLabel,
                      label: classes.rootLabelControlLabel,
                    }}
                  />
                </div>
                <div className="userSettings_toggles">
                  <span className="userSettings__label">
                    Show Event Distribution
                  </span>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={showEventDistribution}
                        onChange={() =>
                          toggleSwitchItems('showEventDistribution')
                        }
                        name="togglEventAndRiskData"
                      />
                    }
                    classes={{
                      root: classes.rootControlLabel,
                      label: classes.rootLabelControlLabel,
                    }}
                  />
                </div>
                <div className="userSettings_toggles">
                  <span className="userSettings__label">
                    Show Severity Level
                  </span>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={showRiskLevelDistribution}
                        onChange={() =>
                          toggleSwitchItems('showRiskLevelDistribution')
                        }
                        name="togglEventAndRiskData"
                      />
                    }
                    classes={{
                      root: classes.rootControlLabel,
                      label: classes.rootLabelControlLabel,
                    }}
                  />
                </div>
                <div className="userSettings_toggles">
                  <span className="userSettings__label">
                    Show Risk Category Distribution
                  </span>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={showRiskCategoryDistribution}
                        onChange={() =>
                          toggleSwitchItems('showRiskCategoryDistribution')
                        }
                        name="togglEventAndRiskData"
                      />
                    }
                    classes={{
                      root: classes.rootControlLabel,
                      label: classes.rootLabelControlLabel,
                    }}
                  />
                </div>
              </div>
            </div>
          </CustomModal>
        </ModalWithPortal>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    barclays: state.barclays,
    barclaysEventDetails: state.barclaysEventDetails,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TopMenu);
