import React from 'react';
import clsx from 'clsx';
import injectSheet from 'react-jss';

const styles = {
  rootStatus: {
    width: 6,
    height: 6,
    marginLeft: 8,
    borderRadius: '50%',
    display: 'inline-block',
  },
  veryLow: {
    background: '#9bcc3b',
  },
  low: {
    background: '#f1c749',
  },
  medium: {
    background: '#ff7f00',
  },
  high: {
    background: '#ed4b3f',
  },
  veryHigh: {
    background: '#e13530',
  },
};

const RiskLevels = ({ type, classes }) => (
  <span
    className={clsx(
      classes.rootStatus,
      type === 1 && classes.veryLow,
      type === 2 && classes.low,
      type === 3 && classes.medium,
      type === 4 && classes.high,
      type === 5 && classes.veryHigh,
    )}
  />
);

export default injectSheet(styles)(RiskLevels);
