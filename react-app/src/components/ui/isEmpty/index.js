import React from 'react';
import './style.scss';
const IsEmpty = ({title}) => <div className="is__empty"><p>{title}</p></div>
export default IsEmpty;