import React, { Component } from 'react';
import style from './style';
import injectSheet from 'react-jss';
//material ui components
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import OutlinedInput from '@material-ui/core/OutlinedInput';

@injectSheet(style)
class SingleSelect extends Component {

    state = {
        labelWidth: 0
    }

    handleChange = (e, type) => {
        this.props.handleChange(e, type);
    }

    getListData = () => {
        let { listData } = this.props;
        let listXML = [];

        listXML = listData.length > 0 && listData.map((item, index) => {
            return <MenuItem value={item.id}>{item.level}</MenuItem>
        })

        return listXML;
    }

    render(){
        let { listData, label, type, selected, fullWidth } = this.props;

        return(
            <FormControl variant="outlined" fullWidth={fullWidth}>
                <InputLabel
                    ref={ref => {
                    this.InputLabelRef = ref;
                    }}
                    htmlFor="outlined-age-simple"
                >
                    {label}
                </InputLabel>
                    <Select
                        value={selected}
                        onChange={(e) => this.handleChange(e, type)}
                        input={
                        <OutlinedInput
                            labelWidth={this.state.labelWidth}
                            name="age"
                            id="outlined-age-simple"
                        />
                        }
                    >
                        { listData && this.getListData() }
                    </Select>
                </FormControl>
        )
    }
}

export default SingleSelect;
