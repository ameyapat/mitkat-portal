import React from 'react';
import './style.scss';

const loader = () => (
    <div id="loader">
        <div className="spinner"></div>
    </div>
)

export default loader;