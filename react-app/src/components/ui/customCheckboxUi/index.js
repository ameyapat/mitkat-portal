import React, { useState, useEffect } from 'react';
import Checkbox from '@material-ui/core/Checkbox';
import FormLabel from '@material-ui/core/FormLabel';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import injectSheet from 'react-jss';
import styles from './style';
import clsx from 'clsx';

const pinMappingHex = {
  0: '#EEA672',
  1: '#23CD98',
  2: '#4BB2E7',
  3: '#B07AFB',
  4: '#7FFBFF',
  5: '#FD87CB',
  6: '#FFDC34',
  7: '#EE72A9',
  'showClientLocationPins': '#4BB2E7'
}

const CustomCheckboxUi = ({ id, updateCategories, isSelected, classes, label, name }) => {
  const [isChecked, setChecked] = useState(isSelected);

  useEffect(() => {
    setChecked(isSelected)
  }, [isSelected])

  function handleIsChecked(id) {   
    setChecked(!isSelected); 
    updateCategories(id);
  }

  return (
    <div className={classes.formControl}>
      <Checkbox 
        checked={isChecked}
        onChange={() => handleIsChecked(id)}
        value={id}
        inputProps={{
          'aria-label': 'rime-categories'
        }}
        classes={{
          root: classes.customRootCheckbox,
          checked: classes.checked,
        }}
        icon={<CheckBoxOutlineBlankIcon className={classes.sizeIcon} />}
        checkedIcon={<CheckBoxIcon className={classes.sizeIcon} />}
        name={name}
      />
      <div className={classes.labelWrapper}>
        <FormLabel className={classes.formLabel}>{label}</FormLabel>
        {/* <span className={clsx(classes.indicators)} style={{ background: pinMappingHex[id], boxShadow: `0 0 10px 0 ${pinMappingHex[id]}` }}></span> */}
      </div>
    </div>
  );
};

export default injectSheet(styles)(CustomCheckboxUi);
