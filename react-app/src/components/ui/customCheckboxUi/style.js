import * as globalCss from '../../../sass/globalVariables.scss';

export default {
  customRootCheckbox: {
    borderColor: 'red',
  },
  formControl: {
    display: 'flex',
    alignItems: 'center',
  },
  formLabel: {
    color: `var(--whiteHighEmp) !important`,
    fontSize: 15,
  },
  checked: {
    color: 'var(--whiteHighEmp) !important ',
  },
  sizeIcon: {
    color: `rgba(171, 193, 66, 1)`,
    fontSize: 18,
  },
  indicators: {
    height: 6,
    width: 6,
    borderRadius: '50%',
    background: 'red',
    display: 'block',
    margin: '0 10px',
  },
  labelWrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
  },
};
