import React from 'react';
import { Pie } from 'react-chartjs-2';

const PieChart = ({ data, width = 100, height = 50, options }) => {
  const dataSetValues = data.datasets[0].data || [];

  return dataSetValues?.length > 0 ? (
    <Pie data={data} width={width} height={height} options={options} />
  ) : (
    <p>No data available</p>
  );
};

export default PieChart;
