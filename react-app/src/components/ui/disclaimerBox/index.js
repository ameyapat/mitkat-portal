import React from 'react';
import './style.scss';

const DisclaimerBox = () => (
    <div className="notice notice--information">
        <h4>Disclaimer</h4>
        <p>
            MitKat dashboard is updated as and when official announcements are made by State and Union Health Ministries, therefore total number of cases will always reflect a more updated figure than other official sources. The total count includes confirmed, recovered and fatal cases.
        </p>
    </div>
)

export default DisclaimerBox;