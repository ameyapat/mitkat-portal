const validationRulesCreateLocation = [
  {
    field: 'title',
    method: 'isEmpty',
    validWhen: false,
    message: 'Title is required',
  },
  {
    field: 'latitude',
    method: 'isEmpty',
    validWhen: false,
    message: 'Latitude is required',
  },
  {
    field: 'longitude',
    method: 'isEmpty',
    validWhen: false,
    message: 'Longitude is required',
  },
];

export default validationRulesCreateLocation;