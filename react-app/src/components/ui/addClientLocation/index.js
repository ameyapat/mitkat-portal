import React, { Component } from 'react';
//components @material-ui
import TextField from '@material-ui/core/TextField';
import './addLocation.scss';
import { connect } from 'react-redux';
import store from '../../../store';
import { toggleToast } from '../../../actions';
import { withCookies } from 'react-cookie';
import FormValidator from '../../../helpers/validation/FormValidator';
import CustomMessage from '../customMessage';
import { LOCATION_INPUT } from '../../client/helpers/constants';
import validationRulesCreateLocation from './validationRules';
import { setNewClientLocation } from '../../../requestor/mitkatWeb/requestor';

@withCookies
@connect(state => {
  return {
    appState: state.appState,
  };
})
class AddClientLocation extends Component {
  validator = new FormValidator(validationRulesCreateLocation);
  submitted = false;

  state = {
    latitude: '',
    longitude: '',
    title: '',
    isLat: false,
    isLong: false,
    validation: this.validator.valid(),
  };

  handleChange = (type, value) => {
    const LAT_RE = /^[+-]?(([1-8]?[0-9])(\.[0-9]{1,6})?|90(\.0{1,6})?)$/;  // To Validate Latitude Values
    const LONG_RE = /^[+-]?((([1-9]?[0-9]|1[0-7][0-9])(\.[0-9]{1,6})?)|180(\.0{1,6})?)$/; // To Validate Longitude Values
    this.setState({ [type]: value }, () => {
      if (type === 'latitude') {
        if (LAT_RE.test(value)) {
          this.setState({ isLat: true });
        } else {
          this.setState({ isLat: false });
        }
      } else if (type === 'longitude') {
        if (LONG_RE.test(value)) {
          this.setState({ isLong: true });
        } else {
          this.setState({ isLong: false });
        }
      }
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    const { isLat, isLong } = this.state;
    let validation = this.validator.validate(this.state);
    this.setState({ validation });
    this.submitted = true;

    if (validation.isValid && isLat && isLong) {
      let reqBody = {
        latitude: this.state.latitude,
        longitude: this.state.longitude,
        title: this.state.title,
      };
      setNewClientLocation(reqBody)
        .then(data => {
          if (data.success) {
            store.dispatch(
              toggleToast({ showToast: true, toastMsg: data.message }),
            );
            this.props.getLocationList();
          } else {
            store.dispatch(
              toggleToast({ showToast: true, toastMsg: data.message }),
            );
          }
          this.props.handleCloseModal();
        })
        .catch(e => {
          console.log(e);
          store.dispatch(
            toggleToast({ showToast: true, toastMsg: 'Something went wrong!' }),
          );
          this.props.handleCloseModal();
        });
    }
  };

  render() {
    let validation = this.submitted
      ? this.validator.validate(this.state)
      : this.state.validation;
    const { isLat, isLong } = this.state;

    return (
      <div className="createUserWrap">
        <div className="create__user__inner">
          <div className="form__wrap">
            <h2>Enter Location Details</h2>
            <form onSubmit={this.handleSubmit}>
              {LOCATION_INPUT.map(input => (
                <TextField
                  error={validation[input.id].isInvalid}
                  key={input.id}
                  id={input.id}
                  label={input.label}
                  fullWidth
                  type={input.type === 'password' ? 'password' : 'text'}
                  value={this.state[input.type]}
                  onChange={e => this.handleChange(input.type, e.target.value)}
                  margin="normal"
                />
              ))}
              {(!isLat || !isLong) && (
                <CustomMessage>
                  <p>
                    <span className="customMsg__icon">
                      <i className="fas fa-lock"></i>
                    </span>
                    Please enter values for:
                  </p>
                  <ul>
                    <li>Latitude between -90 to +90</li>
                    <li>Longitude between -180 to +180</li>
                  </ul>
                </CustomMessage>
              )}
              <div className="submit-wrap">
                <button
                  className="solidBtn createUserBtn"
                  onClick={this.handleSubmit}
                >
                  Add Location
                </button>
                <button
                  className="solidBtn createUserBtn"
                  onClick={this.props.handleCloseModal}
                >
                  Close
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default AddClientLocation;
