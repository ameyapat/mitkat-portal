import React, { Component } from 'react';
import injectSheet from 'react-jss';
import styles from './style';
import clsx from 'clsx';
import './style.scss';
import updateTagIcon from '../../../assets/icons/update-tag.png';
import moment from 'moment';

@injectSheet(styles)
class TransparentList extends Component {
  render() {
    const {
      styles,
      classNames,
      classes,
      update,
      title,
      timestamp,
      parentHandler,
      id,
    } = this.props;

    return (
      <div
        onClick={e => parentHandler(e, id)}
        {...styles}
        className={clsx(classNames && classNames, classes.rootTransparent)}
      >
        <div className="transparent--list--inner">
          <div className="updateIcon">
            <span className="update--icon live--pulse">
              {/* <img 
                                src={updateTagIcon} 
                                alt="update--icon" 
                                className="" 
                            /> */}
            </span>
          </div>
          <div className="update--info">
            <p className="title">{title}</p>
            <p className="timestamp">
              {moment(timestamp).format('Do MMM YYYY, h:mm:ss a')}
            </p>
            <div
              dangerouslySetInnerHTML={{ __html: update }}
              className="update"
            ></div>
          </div>
        </div>
      </div>
    );
  }
}

export default TransparentList;