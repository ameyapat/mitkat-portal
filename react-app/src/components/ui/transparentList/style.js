export default {
  rootTransparent: {
    marginBottom: '10px',
    padding: 8,
    borderRadius: 5,
    cursor: 'pointer',
    background: 'var(--darkGrey) !important',

    '&:hover': {
      background: 'rgba(0,0,0,0.6)',
    },

    '& .timestamp': {
      color: 'rgba(255,255,255, 0.6)',
      fontSize: '13px',
    },
    '& .title': {
      fontWeight: '600',
      color: 'var(--whiteMediumEmp)',
      fontSize: '14px',
      paddingBottom: '5px',
    },
    '& .update': {
      '& p': {
        color: 'var(--whiteMediumEmp)',
        fontSize: '13px',
        fontWeight: '600',
      },
    },
  },
};
