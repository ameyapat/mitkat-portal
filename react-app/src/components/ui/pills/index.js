import React, { Component } from 'react'
import injectSheet from 'react-jss'
import styles from './style'
import { classes } from 'coa';

@injectSheet(styles)
class Pills extends Component {
    render() {
        let { classes, pillItems=[], selectedItem, handleViewChange } = this.props;

        return (
            <ul className={classes.rootPills}>
                { 
                    pillItems.length > 0 && 
                    pillItems.map((i, index) => <li 
                        className={`pill-item ${selectedItem === index ? 'active' : ''}`}
                        onClick={() => handleViewChange(index)}
                        >
                        {i.label}
                    </li>)
                }
            </ul>
        )
    }
}

export default Pills
