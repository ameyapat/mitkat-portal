export default {
  rootPills: {
    listStyle: 'none',
    display: 'flex',
    padding: '0 15px 15px',
    borderBottom: '1px solid #f1f1f1',

    '& .pill-item': {
      padding: 10,
      border: '1px solid #fff',
      margin: '0 10px',
      borderRadius: 5,
      cursor: 'pointer',
      color: 'var(--whiteMediumEmp)',

      '&.active': {
        background: 'var(--darkActive)',
        border: 'transparent',
        color: '#000',
      },
    },
  },
};
