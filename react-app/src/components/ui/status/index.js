import React from 'react';
import clsx from 'clsx';
import injectSheet from 'react-jss';

const styles = {
  rootStatus: {
    width: 6,
    height: 6,
    marginLeft: 8,
    borderRadius: '50%',
    display: 'inline-block'
  },
  approved: {
    background: '#2ecc71'
  },
  pending: {
    background: '#f1c40f'
  },
  rejected: {
    background: '#e74c3c'
  }
};

const Status = ({ type, classes }) => (
  <span
    className={clsx(
      classes.rootStatus,
      type === 'approved' && classes.approved,
      type === 'pending' && classes.pending,
      type === 'rejected' && classes.rejected
    )}
  />
);

export default injectSheet(styles)(Status);
