import React, { Component, Fragment } from 'react'
//wysiwyg
import { Editor } from 'react-draft-wysiwyg';
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import '../../../../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';

const styles = {
    root:{
        // background: 'red',
        // border: '1px solid #d3d3d3',
    },
    rootEditor:{
        background: '#fff',
        border: '1px solid #d3d3d3',
        height: 150,
        padding: 5
    }
}

export default class CustomEditor extends Component {

    state = {
        editorState: EditorState.createEmpty()
    }

    componentDidMount(){        
        let { editorState, type } = this.props;        
        const contentBlock = htmlToDraft(editorState);
        if(contentBlock){
            const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
            const editorStateNew = EditorState.createWithContent(contentState);
            this.setState({ editorState: editorStateNew, type })
        }            
    }

    onChange = (value) => {
        let { editorState, type} = this.state;

        this.setState({editorState: value}, () => {
            this.props.onEditorStateChange(editorState, type)
        })
    }

    render() {
        let { editorState } = this.state;
        
        return (
            <Fragment>                
                <Editor                 
                    wrapperStyle={styles.root}
                    editorStyle={styles.rootEditor}
                    toolbarStyle={styles.rootToolbar}                
                    editorState={editorState}
                    onEditorStateChange={this.onChange}                
                />
                {/* <textarea
                    style={{width: '100%'}}
                    disabled
                    value={draftToHtml(convertToRaw(editorState.getCurrentContent()))}
                />   */}
            </Fragment>
        )
    }
}