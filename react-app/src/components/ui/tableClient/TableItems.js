import React, { Component } from 'react';
import './style.scss';
import Button from '@material-ui/core/Button';
//helpers
import { API_ROUTES } from '../../../helpers/http/apiRoutes';
import { fetchApi } from '../../../helpers/http/fetch';
//redux
import { connect } from 'react-redux';
import store from '../../../store';
import { toggleToast } from '../../../actions';
import { withCookies } from 'react-cookie';
import validator from 'validator';
import AlertDialog from '../../ui/alertDialog';
import { editUserRole } from '../../../requestor/admin/requestor';
import { deleteClientUser } from '../../../requestor/mitkatWeb/requestor';

@withCookies
@connect(state => {
  return {
    authToken: state.appState.authToken,
  };
})
class ClientTableItems extends Component {
  state = {
    name: '',
    username: '',
    password: '',
    email: '',
    showAlert: false,
  };

  componentDidMount() {
    let { name, username, password, email } = this.props;
    this.setState({ name, username, password, email });
  }

  handleInputChange = (type, value) => {
    this.setState({ [type]: value });
  };

  handlePasswordChange = (e, type, id) => {
    e.preventDefault();
    let { cookies } = this.props;
    const { password } = this.state;
    let authTokenFromCookie = cookies.get('authToken');

    let params = {
      url: `${API_ROUTES.adminUpdateUser}/${id}/${type}`,
      method: 'POST',
      isAuth: true,
      authToken: authTokenFromCookie,
      data: { password: password },
      showToggle: true,
    };

    if (validator.isStrongPassword(password)) {
      fetchApi(params)
        .then(res => {
          if (res.success) {
            store.dispatch(
              toggleToast({ showToast: true, toastMsg: res.message }),
            );
            this.props.getUserList();
          } else {
            store.dispatch(
              toggleToast({ showToast: true, toastMsg: res.message }),
            );
          }
        })
        .catch(e => {
          store.dispatch(
            toggleToast({ showToast: true, toastMsg: 'Something went wrong!' }),
          );
        });
    } else {
      this.props.togglePasswordAlert(true);
    }
  };

  updateUserDetail = (e, type, id) => {
    e.preventDefault();
    let { cookies } = this.props;
    let authTokenFromCookie = cookies.get('authToken');

    let params = {
      url: `${API_ROUTES.adminUpdateUser}/${id}/${type}/${this.state[type]}`,
      method: 'POST',
      isAuth: true,
      authToken: authTokenFromCookie,
      showToggle: true,
    };

    fetchApi(params)
      .then(res => {
        if (res.success) {
          store.dispatch(
            toggleToast({ showToast: true, toastMsg: res.message }),
          );
          this.props.getUserList();
        } else {
          store.dispatch(
            toggleToast({ showToast: true, toastMsg: res.message }),
          );
        }
      })
      .catch(e => {
        store.dispatch(
          toggleToast({ showToast: true, toastMsg: 'Something went wrong!' }),
        );
      });
  };

  handleDeleteClientUser = id => {
    deleteClientUser(id)
      .then(data => {
        if (data.success) {
          store.dispatch(
            toggleToast({ showToast: true, toastMsg: data.message }),
          );
          this.props.getUserList();
          this.handleCloseAlert();
        } else {
          store.dispatch(
            toggleToast({ showToast: true, toastMsg: data.message }),
          );
        }
      })
      .catch(e => {
        console.log(e);
        store.dispatch(
          toggleToast({ showToast: true, toastMsg: 'Something went wrong!' }),
        );
        this.handleCloseAlert();
      });
  };

  handleShowAlert = eventId => {
    this.setState({ showAlert: true, eventId });
  };

  handleCloseAlert = () => {
    this.setState({ showAlert: false, eventId: '' });
  };

  render() {
    let { id } = this.props;
    let { name, username, password, email, showAlert } = this.state;

    return (
      <tr>
        <td>
          <form onSubmit={e => this.updateUserDetail(e, 'name', id)}>
            <input
              value={name}
              type="text"
              onChange={e => this.handleInputChange('name', e.target.value)}
            />
          </form>
        </td>
        <td>
          <form onSubmit={e => this.updateUserDetail(e, 'username', id)}>
            <input
              value={username}
              type="text"
              onChange={e => this.handleInputChange('username', e.target.value)}
            />
          </form>
        </td>
        <td>
          <form onSubmit={e => this.updateUserDetail(e, 'email', id)}>
            <input
              value={email}
              type="email"
              onChange={e => this.handleInputChange('email', e.target.value)}
            />
          </form>
        </td>
        <td>
          <form onSubmit={e => this.handlePasswordChange(e, 'password', id)}>
            <input
              value={password}
              type="password"
              onChange={e => this.handleInputChange('password', e.target.value)}
            />
          </form>
        </td>
        {
          <td>
            <Button
              className="deleteUser"
              onClick={() => this.handleShowAlert(id)}
              variant="outlined"
              color="default"
              title="Delete User"
            >
              <i className="fas fa-trash" style={{ marginRight: 5 }}></i>
              Delete
            </Button>
          </td>
        }
        {showAlert && (
          <AlertDialog
            isOpen={showAlert}
            title="Delete User"
            description="You are about to delete the user, are you sure?"
            parentEventHandler={() => this.handleDeleteClientUser(id)}
            handleClose={() => this.handleCloseAlert()}
          />
        )}
      </tr>
    );
  }
}

export default ClientTableItems;
