import React, { Component } from 'react';
import TableItems from './TableItems';
import CustomModal from '../modal';
import CustomMessage from '../customMessage';
import './style.scss';

class ClientTable extends Component {
  state = {
    showPasswordAlert: false,
  };

  togglePasswordAlert = showPasswordAlert => {
    this.setState({ showPasswordAlert });
  };

  render() {
    let {
      tableData,
      showViewDetails,
      handleInputChange,
      getUserList,
      meta,
    } = this.props;
    const { showPasswordAlert } = this.state;

    return (
      <>
        <table id="user-list-table" className="user--list--inner">
          <thead>
            <tr>
              <th>Name</th>
              <th>Username</th>
              <th>Email</th>
              <th>Password</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {tableData &&
              tableData.map((item, index) => {
                return (
                  <TableItems
                    key={item.id}
                    id={item.id}
                    handleInputChange={handleInputChange}
                    showViewDetails={showViewDetails}
                    name={item.name}
                    username={item.username}
                    password={item.password}
                    email={item.email}
                    clearance={item.roles}
                    getUserList={getUserList}
                    togglePasswordAlert={this.togglePasswordAlert}
                    meta={meta}
                  />
                );
              })}
          </tbody>
        </table>
        <CustomModal showModal={showPasswordAlert}>
          <CustomMessage classes={'modal__inner customMsg__wrap__userTable'}>
            <p>
              <span className="customMsg__icon">
                <i className="fas fa-lock"></i>
              </span>
              Your password needs to:
            </p>
            <ul>
              <li>be atleast 8 characters long.</li>
              <li>include both uppercase &amp; lowercase characters.</li>
              <li>be atleast one number or symbol.</li>
            </ul>
            <div className="btnPasswordWrapper">
              <button
                className="btn btnPasswordClose"
                onClick={() => this.togglePasswordAlert(false)}
              >
                Close
              </button>
            </div>
          </CustomMessage>
        </CustomModal>
      </>
    );
  }
}

export default ClientTable;
