import React, { Component } from 'react';
import { Bar } from 'react-chartjs-2';
import './style.scss';

class CustomBarChart extends Component {

  render() {
    const {chartLabel, chartData, chartLabels} = this.props;
    const options = {
      responsive: true,
      plugins: {
        legend: {
          position: 'top',
        },
        title: {
          display: true,
        },
      },
      scales: {
        yAxes: [
          {
            ticks: {
              beginAtZero: true,
            },
          },
        ],
        xAxes: [
          {
            barPercentage: 0.5,
            ticks: {
              fontSize: window.innerWidth > 1550 ? 12 : 10,
              stepSize: 1,
              beginAtZero: true,
              autoSkip: false,
              maxRotation: 90,
              minRotation: 0,
            },
          },
        ],
      },
      responsive: true,
      maintainAspectRatio: false,
    };

    const dataChart = [...chartData]
    const labels = chartLabels;
    const data = {
      labels,
      datasets: [
        {
          label: chartLabel,
          data: dataChart,
          borderColor: 'rgba(13, 153, 255, 1)',
          borderWidth: 0.5,
          backgroundColor: 'rgba(53, 162, 235, 0.5)',
          hoverBorderColor: 'rgba(53, 162, 235, 0.8)',
          backgroundColor: context => {
            const ctx = context.chart.ctx;
            const gradient = ctx.createLinearGradient(0, 0, 0, 200);
            gradient.addColorStop(0, 'rgba(53, 162, 235, 0.8)');
            gradient.addColorStop(1, 'rgba(53, 162, 235, 0.2)');
            return gradient;
          },
        },
      ],
    };

    return (
      <div className="locationbarchart">
        <Bar options={options} data={data} />
      </div>
    );
  }
}

export default CustomBarChart;