import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import injectSheet from 'react-jss';
import styles from './style';

import businessView from '../../../assets/icons/business-light.png';
import heatMap from '../../../assets/icons/heatmap-light.png';
import { restrictAccess } from '../../../actions';
import { connect } from 'react-redux';

const iconMapping = {
  riskTracker: 'far fa-address-card',
  rime: 'far fa-registered',
  businessView: businessView,
  heatMap: heatMap,
  riskExposure: 'fas fa-exclamation-circle',
  covid19: 'fas fa-virus',
};

@injectSheet(styles)
@connect(
  state => {
    return {};
  },
  dispatch => {
    return { dispatch };
  },
)
class ToggleButton extends Component {
  render() {
    const {
      to,
      btnTxt,
      classes,
      icon,
      btnDescription,
      appPermissions,
      isBeta,
    } = this.props;

    return (
      <NavLink
        to={to}
        activeClassName="mapSelected"
        className={classes.toggleSwitchBtn}
        onClick={e => {
          if (!appPermissions.enabled) {
            e.preventDefault();
            this.props.dispatch(restrictAccess(true));
          }
        }}
      >
        <span className="toggle-icon">
          <i className={iconMapping[icon]}></i>
        </span>
        <span className="toggle-btn-txt">
          {btnTxt} {isBeta && <span className="toggleIcon__beta">beta</span>}
        </span>
        <p className="toggle-btn-description">{btnDescription}</p>
      </NavLink>
    );
  }
}

export default ToggleButton;
