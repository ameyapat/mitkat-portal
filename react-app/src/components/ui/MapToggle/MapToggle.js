import React, { Component } from 'react';
import ToggleButton from './ToggleButtton';
import views from './constants';
import injectSheet from 'react-jss';
import styles from './style';
import { connect } from 'react-redux';

@injectSheet(styles)
@connect(state => {
  return {
    appPermissions: state.appState.appPermissions,
  };
})
class MapToggle extends Component {
  render() {
    const { classes, appPermissions } = this.props;

    return (
      <div id="mapToggle" className={classes.rootMapToggle}>
        {views.map(v => {
          return (
            <ToggleButton
              key={v.id}
              to={v.to}
              btnTxt={v.label}
              icon={v.icon}
              isBeta={v.isBeta}
              header={v.header}
              btnDescription={v.description}
              appPermissions={{
                enabled: appPermissions[v.accessName] === 1,
              }}
            />
          );
        })}
      </div>
    );
  }
}

export default MapToggle;
