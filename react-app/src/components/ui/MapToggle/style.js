export default {
  rootMapToggle: {
    display: 'flex',
    flexDirection: 'column',
  },
  toggleSwitchBtn: {
    display: 'flex',
    minHeight: '120px',
    width: '100%',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    flexDirection: 'column',
    color: 'var(--whiteHighEmp)',
    marginBottom: '15px',
    padding: '10px 20px',
    transition: 'all 0.1s cubic-bezier(0.17,0.67,0.83,0.67)',
    '& .toggle-btn-txt': {
      fontSize: '15px !important',
      display: 'flex',
      alignItems: 'center',
    },
    '& .toggleIcon__beta': {
      textTransform: 'uppercase',
      fontWeight: 'bold',
      color: '#fff',
      background: '#66101f',
      fontSize: '8px !important',
      padding: 3,
      borderRadius: 3,
      marginLeft: 4,
    },
    '& .toggle-btn-description': {
      fontSize: '11px !important',
    },
    '&.mapSelected': {
      color: 'var(--whiteHighEmp)',
      background: 'var(--highlightBackgroundLight)',
    },
    '&:hover': {
      color: 'var(--whiteHighEmp)',
      background: 'var(--highlightBackgroundLight)',
    },
    '& .toggle-icon': {
      paddingBottom: 5,
      marginRight: 15,
      fontSize: '16px !important',
      color: 'var(--whiteHighEmp)',
    },
  },
};
