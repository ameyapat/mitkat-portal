export default [
  {
    id: 1,
    isBeta: false,
    icon: 'riskTracker',
    accessName: 'riskTrackerSubscription',
    to: '/client/dashboard',
    label: 'Risk Tracker',
    type: '',
    header: 'riskTracker',
    description:
      'Risk Tracker Portal is an interactive platform that is updated on real-time basis with curated and analysed information on events of critical importance to businesses. ',
  },
  {
    id: 2,
    isBeta: false,
    icon: 'rime',
    accessName: 'rimesubscription',
    to: '/client/rime',
    label: 'RIME',
    type: 'rimesubscription',
    header: 'rime',
    description:
      'Risk Intelligence Monitoring Engine (RIME) is an AI-powered platform providing live feed of risk events across globe.',
  },
  {
    id: 5,
    isBeta: true,
    icon: 'riskExposure',
    accessName: 'analyticssubscription',
    to: '/client/riskExposure',
    label: 'Threat Intelligence Platform',
    type: '',
    header: 'threatIntelligencePlatform',
    description:
      'Threat intelligence platform is an analytical tool designed to analyze individual events and its impact on client assets. Data collected over years by predictive risk intelligence team and RIME is consumed for modelling the risk for Major events as well as Locations.',
  },
  {
    id: 6,
    isBeta: false,
    icon: 'covid19',
    accessName: 'coviddashboardsubscription',
    to: '/covid-19',
    label: 'COVID19',
    type: '',
    header: 'coviddashboard',
    description:
      'COVID19 dashboard presents live data of nationwide and state-wise statistics in terms of infections, deaths and recovered case counts, along with comprehensive graphical trend analyses',
  },
];
