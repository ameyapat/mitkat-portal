import React from 'react';
import './style.scss';

const MultipleLocationList = ({ selectedMultipleLocations, handleRemove, type = "list" }) => {
    return(
        <ul className="multipleLocationList">
            {
                selectedMultipleLocations.map((item) => 
                    <li>
                        <div className="multipleLocationList__info">
                            <p>{item.formattedAddress}</p>
                            {
                                type !== "detail" &&
                                <button className="btn btnRemove" onClick={() => handleRemove(item.id)}><i className="fas fa-trash-alt"></i></button>
                            }
                        </div>
                    </li>
                )
            }
        </ul>
    )
}

export default MultipleLocationList