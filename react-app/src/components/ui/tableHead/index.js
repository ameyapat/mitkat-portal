import React from 'react';
import injectSheet from 'react-jss';

const styles = {
    rootTableHead:{
        display: 'flex',
        borderBottom: '1px dashed #f1f1f1'
    },
    label:{
        fontSize: 13,
        color: '#666'
    },
    items:{
        flex: 1,
        padding: 10,
        textAlign: 'center'
    }    
}

const TableHead = ({classes, items, render}) => (
    <div className={classes.rootTableHead}>
        {
            items && (
                items.map(i => <div key={i.id} className={classes.items}><label className={classes.label}>{i.title}</label></div>)
            )
        }
        {
            render && <div className={classes.items}>{render()}</div>
        }       
    </div>
)

export default injectSheet(styles)(TableHead)