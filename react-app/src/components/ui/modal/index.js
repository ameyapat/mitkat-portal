import React, { Component } from 'react';
import Modal from '@material-ui/core/Modal';
import injectSheet from 'react-jss';
import style from './style';
import './style.scss';
import clsx from 'clsx';
@injectSheet(style)
class CustomModal extends Component {
  render() {
    const {
      showModal,
      closeModal,
      children,
      showCloseOption = false,
      classes,
      id,
      disableBackdropClick = true,
      BackdropProps,
    } = this.props;
    return (
      <Modal
        open={showModal}
        id={id ? id : null}
        onBackdropClick={!disableBackdropClick ? () => closeModal(false) : null}
        disableBackdropClick={disableBackdropClick}
        disableAutoFocus={false}
        BackdropProps={{ ...(BackdropProps || {}) }}
      >
        <>
          {showCloseOption && (
            <span
              className={clsx(classes.closeTimes, 'close--times')}
              onClick={() => closeModal(false)}
            >
              &times;
            </span>
          )}
          {children}
        </>
      </Modal>
    );
  }
}

export default CustomModal;
