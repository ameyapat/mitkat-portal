import React, { Component, Fragment } from 'react';
//helpers
import {
  RISK_LEVEL_COLORS,
  RISK_CATEGORY_ICONS,
} from '../../../helpers/constants';
import { API_ROUTES } from '../../../helpers/http/apiRoutes';
import { fetchApi } from '../../../helpers/http/fetch';

class ModalInnerSms extends Component {
  generateXML = (data, type) => {
    let dataXML = [];
    dataXML = data.map((item, index) => (
      <li key={index}>
        <span className="pills">{item[type]}</span>
      </li>
    ));
    return dataXML;
  };

  downloadPdf = () => {
    let {
      eventDetail: { id },
      cookies,
    } = this.props;
    let authToken = cookies.get('authToken');

    let reqObj = {
      url: `${API_ROUTES.downloadEventPdf}/${id}`,
      isAuth: true,
      authToken,
      isBlob: true,
      method: 'POST',
      showToggle: true,
    };

    fetchApi(reqObj).then(blobObj => {
      var a = document.createElement('a');
      document.body.appendChild(a);
      let objUrl = new Blob([blobObj], { type: 'application/pdf' });
      let url = window.URL.createObjectURL(objUrl);
      a.href = url;
      a.download = 'mitkat-report';
      a.click();
      window.URL.revokeObjectURL(url);
    });
  };

  render() {
    let { eventDetail } = this.props;

    return (
      <Fragment>
        <div className="event__detail">
          <div className="title__wrap">
            <div>
              {
                <span
                  className="s--icon"
                  style={{
                    color: RISK_LEVEL_COLORS[eventDetail.riskLevel1.id],
                  }}
                >
                  <i
                    className={
                      RISK_CATEGORY_ICONS[eventDetail.riskCategory1.id]
                    }
                  ></i>
                </span>
              }
            </div>
            <h2>{eventDetail.title}</h2>
          </div>
          <div className="event--detail">
            <div className="event">
              <label>Date</label>
              <p>{eventDetail.alertdate}</p>
            </div>
          </div>
          <div className="event--detail">
            <div className="event">
              <label>Cities</label>
              <ul className="smsAlertPlaces">
                {this.generateXML(eventDetail.cities, 'cityname')}
              </ul>
            </div>
            <div className="event">
              <label>Countries</label>
              <ul className="smsAlertPlaces">
                {this.generateXML(eventDetail.countries, 'countryname')}
              </ul>
            </div>
            <div className="event">
              <label>State</label>
              <ul className="smsAlertPlaces">
                {this.generateXML(eventDetail.state, 'statename')}
              </ul>
            </div>
            <div className="pdf__btn">
              <button onClick={this.downloadPdf} className="pdf--btn">
                Download Pdf
              </button>
              <button
                className="btn btn--close"
                onClick={this.props.closeModal}
              >
                Close
              </button>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default ModalInnerSms;
