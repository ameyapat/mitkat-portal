import React, { Component, Fragment } from 'react';
//components
import BulletList from '../bulletList';
//helpers
import {
  RISK_CATEGORY_NAME,
  RISK_LEVEL_COLORS_NEON,
} from '../../../helpers/constants';
import Moment from 'react-moment';
import { API_ROUTES } from '../../../helpers/http/apiRoutes';
import { fetchApi } from '../../../helpers/http/fetch';
//
import clsx from 'clsx';
import styles from './style';
import injectSheet from 'react-jss';

import { withCookies } from 'react-cookie';
import { NavLink, useRouteMatch } from 'react-router-dom';
import store from '../../../store';
import { setImpactRadius } from '../../../actions/EventModalAction';
import { showEventDetail } from '../../../api/api';
import { Divider } from '@material-ui/core';
import './style.scss';
import CustomModal from '.';
import EmployeeTracking from '../../client/components/employeeTracking';
import { connect } from 'react-redux';

@withCookies
@injectSheet(styles)
@connect(state => {
  return {
    themeChange: state.themeChange,
    eventModal: state.eventModal,
  };
})
class ModalInner extends Component {
  state = {
    showEventDetails: true,
    showUpdates: false,
    showRelatedEvents: false,
    relatedEventList: [],
    showAffectedEntities: false,
  };

  componentDidMount() {
    this.handleToggleTabs(true, this.state.showEventDetails);
    this.props.eventModal.eventId && this.getRelatedEvents();
    store.dispatch(
      setImpactRadius(this.props.eventDetail?.impactRadius || 1500),
    );
  }

  getUpdateList = () => {
    let {
      eventDetail: { updates },
    } = this.props;
    let updatesXML = [];
    if (updates) {
      updatesXML = updates.map(item => {
        return (
          <li key={item.key} className="update__item">
            <h2 className="update--title">{item.title}</h2>
            <p className="update--desc">{item.description}</p>
            <p className="update--time">{item.timeupdate}</p>
          </li>
        );
      });
    }

    return updatesXML;
  };

  downloadPdf = () => {
    let {
      eventDetail: { id },
      cookies,
    } = this.props;
    let authToken = cookies.get('authToken');

    let reqObj = {
      url: `${API_ROUTES.downloadEventPdf}/${id}`,
      isAuth: true,
      authToken,
      isBlob: true,
      method: 'POST',
      showToggle: true,
    };

    fetchApi(reqObj).then(blobObj => {
      var a = document.createElement('a');
      document.body.appendChild(a);
      let objUrl = new Blob([blobObj], { type: 'application/pdf' });
      let url = window.URL.createObjectURL(objUrl);
      a.href = url;
      a.download = 'mitkat-report';
      a.click();
      window.URL.revokeObjectURL(url);
    });
  };

  getRelatedEvents = () => {
    let {
      eventDetail: { id },
      cookies,
      eventModal: { eventId },
    } = this.props;
    let authToken = cookies.get('authToken');

    let reqObj = {
      url: `${API_ROUTES.getRelatedEvents}/${eventId}`,
      isAuth: true,
      authToken,
      method: 'GET',
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        this.setState({ relatedEventList: data });
      })
      .catch(e => console.log(e));
  };

  handleCloseModal = () => {
    this.props.closeModal();
  };

  generateBulletPoints = (type, list) => {
    return <BulletList list={list} type={type} />;
  };

  handleToggleTabs = (tabvalue, tabtype) => {
    this.setState({ [tabtype]: tabvalue }, () => {
      if (tabtype === 'showEventDetails') {
        this.setState({ showUpdates: false, showRelatedEvents: false });
      } else if (tabtype === 'showUpdates') {
        this.setState({ showEventDetails: false, showRelatedEvents: false });
      } else if (tabtype === 'showRelatedEvents') {
        this.setState({ showEventDetails: false, showUpdates: false });
      }
    });
  };

  showEvents = eventId => {
    this.props.showDetails(eventId);
    this.setState({
      showUpdates: false,
      showRelatedEvents: false,
      showEventDetails: true,
    });
  };

  render() {
    let {
      eventDetail,
      risklevel,
      riskcategory,
      showClose = true,
      classes,
      hasDarkTheme = false,
    } = this.props;

    const {
      showEventDetails,
      showUpdates,
      showRelatedEvents,
      relatedEventList,
      showAffectedEntities,
    } = this.state;
    let eventId = '';
    if (eventDetail && eventDetail.id) eventId = eventDetail.id;
    const {
      themeChange: { setDarkTheme },
    } = this.props;

    return (
      <Fragment>
        {showClose && (
          <div className="modal--header">
            <span className="btn-times" onClick={this.handleCloseModal}>
              &times;
            </span>
          </div>
        )}
        <div className={`modal--body ${hasDarkTheme ? 'dark--body' : null}`}>
          <div className="event__detail">
            <div className="eventTabs">
              <div className="eventTab">
                <button
                  className={`eventTab__btn ${showEventDetails &&
                    'tabIsActive'}`}
                  onClick={() =>
                    this.handleToggleTabs(true, 'showEventDetails')
                  }
                >
                  Event Details
                </button>
              </div>
              <div className="eventTab">
                <button
                  className={`eventTab__btn ${showUpdates && 'tabIsActive'}`}
                  onClick={() => this.handleToggleTabs(true, 'showUpdates')}
                >
                  Current Updates
                </button>
              </div>
              <div className="eventTab">
                <button
                  className={`eventTab__btn ${showRelatedEvents &&
                    'tabIsActive'}`}
                  onClick={() =>
                    this.handleToggleTabs(true, 'showRelatedEvents')
                  }
                >
                  Related Events
                </button>
              </div>
            </div>
            <div className="title__wrap">
              <h2>{eventDetail?.title || ''}</h2>
            </div>
            <span className={clsx('s--icon pill', classes.rootPill)}>
              <span
                style={{
                  color: `${RISK_LEVEL_COLORS_NEON[risklevel]}`,
                  border: 'none',
                  // color: 'rgba(18,18,18,1)',
                }}
              >
                {RISK_CATEGORY_NAME[riskcategory]} -{' '}
                {eventDetail?.riskSubCategoryName}
              </span>
            </span>
            {showEventDetails && (
              <>
                <div className="event--detail">
                  <div className="event">
                    {eventDetail?.validitydate && (
                      <>
                        <p>
                          <label>Date</label>
                          <span className="date">
                            <Moment format="YYYY/MM/DD">
                              {eventDetail?.validitydate}
                            </Moment>
                          </span>
                        </p>
                      </>
                    )}
                    {eventDetail?.trackingId && (
                      <p>
                        <label>Tracking Id</label> {eventDetail?.trackingId}
                      </p>
                    )}
                  </div>
                </div>
                <Divider className="divider-pd-15" />
                {/** description */}
                {(eventDetail?.description ||
                  eventDetail?.description2 ||
                  eventDetail?.descriptionbullets?.length > 0) && (
                  <div className="event--detail">
                    <div className="event">
                      <label>Description</label>
                    </div>
                  </div>
                )}
                {eventDetail?.description && (
                  <div className="event--detail">
                    <div className="event">
                      <p>{eventDetail?.description}</p>
                    </div>
                  </div>
                )}
                {eventDetail?.description2 && (
                  <div className="event--detail">
                    <div className="event">
                      <p>{eventDetail?.description2}</p>
                    </div>
                  </div>
                )}
                {eventDetail?.descriptionbullets?.length > 0 && (
                  <div className="event--detail">
                    {eventDetail?.descriptionbullets &&
                      this.generateBulletPoints(
                        'description',
                        eventDetail.descriptionbullets,
                      )}
                  </div>
                )}
                {/** background */}
                {(eventDetail?.background ||
                  eventDetail?.background2 ||
                  eventDetail?.backgroundbullets?.length > 0) && (
                  <div className="event--detail">
                    <div className="event">
                      <label>Background</label>
                    </div>
                  </div>
                )}
                {eventDetail?.background && (
                  <div className="event--detail">
                    <div className="event">
                      <p>{eventDetail?.background}</p>
                    </div>
                  </div>
                )}
                {eventDetail?.background2 && (
                  <div className="event--detail">
                    <div className="event">
                      <p>{eventDetail?.background2}</p>
                    </div>
                  </div>
                )}
                {eventDetail?.backgroundbullets?.length > 0 && (
                  <div className="event--detail">
                    {this.generateBulletPoints(
                      'background',
                      eventDetail?.backgroundbullets,
                    )}
                  </div>
                )}
                {/** background ends */}
                {/** impact begins */}
                {(eventDetail?.impact ||
                  eventDetail?.impact2 ||
                  eventDetail?.impactbullets?.length > 0) && (
                  <div className="event--detail">
                    <div className="event">
                      <label>Impact</label>
                    </div>
                  </div>
                )}
                {eventDetail?.impact && (
                  <div className="event--detail">
                    <div className="event">
                      <p>{eventDetail?.impact}</p>
                    </div>
                  </div>
                )}

                {eventDetail?.impact2 && (
                  <div className="event--detail">
                    <div className="event">
                      <p>{eventDetail?.impact2}</p>
                    </div>
                  </div>
                )}
                {eventDetail?.impactbullets?.length > 0 && (
                  <div className="event--detail">
                    {this.generateBulletPoints(
                      'impact',
                      eventDetail.impactbullets,
                    )}
                  </div>
                )}
                {/** impact ends */}
                {/** Recommendation begins */}
                {(eventDetail?.recommendation ||
                  eventDetail?.recommendation2 ||
                  eventDetail?.recommendationbullets?.length > 0) && (
                  <div className="event--detail">
                    <div className="event">
                      <label>Recommendation</label>
                    </div>
                  </div>
                )}
                {eventDetail?.recommendation && (
                  <div className="event--detail">
                    <div className="event">
                      <p>{eventDetail?.recommendation}</p>
                    </div>
                  </div>
                )}
                {eventDetail?.recommendation2 && (
                  <div className="event--detail">
                    <div className="event">
                      <p>{eventDetail?.recommendation2}</p>
                    </div>
                  </div>
                )}
                {eventDetail?.recommendationbullets?.length > 0 && (
                  <div className="event--detail">
                    {this.generateBulletPoints(
                      'recommendation',
                      eventDetail?.recommendationbullets,
                    )}
                  </div>
                )}
                {/** recommendation ends */}
                {/** Web portal image */}
                {eventDetail?.imageURLwebportal && (
                  <div className="event--detail">
                    <div className="event">
                      <label>Image</label>

                      {eventDetail?.imageURLwebportal && (
                        <img
                          src={eventDetail?.imageURLwebportal}
                          alt="webportal"
                          style={{
                            width: '700px',
                            maxWidth: '700px',
                            marginTop: '15px',
                          }}
                        />
                      )}
                    </div>
                  </div>
                )}
                {eventDetail?.sourceList?.length > 0 && (
                  <div className="event--detail">
                    <div className="event">
                      <label>Sources</label>
                    </div>
                  </div>
                )}
                {eventDetail?.sourceList?.length > 0 && (
                  <div className="event--detail">
                    {this.generateBulletPoints(
                      'sourceList',
                      eventDetail?.sourceList,
                    )}
                  </div>
                )}
              </>
            )}

            {showUpdates && (
              <>
                {eventDetail?.updates.map(update => (
                  <div className="liveUpdatesWrap">
                    <div className="liveUpdates--title">
                      {update?.title || ''}
                    </div>
                    <div className="liveUpdates--timeupdate">
                      {update?.timeupdate || ''}
                    </div>
                  </div>
                ))}
              </>
            )}

            {showRelatedEvents && (
              <>
                {relatedEventList && (
                  <>
                    <div className="event--detail">
                      <div className="event">
                        <label>Related Events</label>
                      </div>
                    </div>
                    <ul className="related--list">
                      {relatedEventList.map(item => (
                        <li onClick={() => this.showEvents(item.eventId)}>
                          <h3>Title: {item.title} </h3>
                          <h4>Description: {item.update}</h4>
                          <p> {item.timestamp}</p>
                        </li>
                      ))}
                    </ul>
                  </>
                )}
              </>
            )}
          </div>
        </div>
        <div className="modal--footer">
          <div className="pdf__btn">
            <button onClick={this.downloadPdf} className="pdf--btn">
              Download Pdf
            </button>
          </div>

          {/* <NavLink
            to={`/client/dashboard/${eventId}`}
            target="_blank"
            className="entity--btn"
          >
            Affected Entities
          </NavLink> */}
          <div className="pdf__btn">
            <button
              onClick={() => this.setState({ showAffectedEntities: true })}
              className="pdf--btn"
            >
              Affected Entities
            </button>
          </div>

          {false && (
            <div>
              <button
                onClick={() => this.props.trackLiveEvents(eventId)}
                className={clsx('pdf--btn', classes.btnLiveTracker)}
              >
                <i className="fas fa-sync"></i> Track Live Updates
              </button>
            </div>
          )}
        </div>

        <CustomModal
          showModal={showAffectedEntities}
          closeModal={() => this.setState({ showAffectedEntities: false })}
        >
          <div
            className={`affectedEntities__inner ${
              setDarkTheme ? 'dark' : 'light'
            }`}
          >
            <div className="affectedEntities__header">
              <span className="affectedEntities--heading">
                Affected Entities
              </span>
              <span
                className="affectedEntities--close"
                onClick={() => this.setState({ showAffectedEntities: false })}
              >
                &times;
              </span>
            </div>
            <div className="affectedEntities__body">
              <EmployeeTracking eventId={eventId} eventDetail={eventDetail} />
            </div>
          </div>
        </CustomModal>
      </Fragment>
    );
  }
}
export default ModalInner;
