import React, { Component } from 'react'
import MapWithAMarker from './MapWithAMarker'

class MapApi extends Component{
    render(){
        return(
            <div>
                <MapWithAMarker
                    googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyCYOTLyn6xbysArlvlW4zpxnH9p6H3An08&v=3.exp&libraries=geometry,drawing,places"
                    loadingElement={<div style={{ height: `100%` }} />}
                    containerElement={
                    <div style={{ height: `100vh`, position: 'relative' }} />
                    }
                    mapElement={<div style={{ height: `100%` }} />}
                />
            </div>
        )
    }
}

export default MapApi