import React, { useEffect, useRef, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { compose, withProps, lifecycle } from "recompose"
import { withScriptjs } from 'react-google-maps'
import { StandaloneSearchBox } from "react-google-maps/lib/components/places/StandaloneSearchBox"

import { updateMultipleLocation } from '../../../actions/multipleLocationActions'

import './styles/style.scss';
import MultipleLocationList from '../multipleLocationList'

const googleMapURL = "https://maps.googleapis.com/maps/api/js?key=AIzaSyCYOTLyn6xbysArlvlW4zpxnH9p6H3An08&v=3.exp&libraries=geometry,drawing,places"

const PlacesWithStandaloneSearchBox = compose(
  withProps({
    googleMapURL,
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `400px` }} />,
  }),
  withScriptjs  
)(props => {

  const [searchInputValue, setSearchInputValue] = useState("");
  const searchBox = useRef(null);
  const { showList = true } = props;
  let [places, setPlaces] = useState([])

  const { selectedMultipleLocations } = useSelector((state) => {
    return state && state.multipleLocations && state.multipleLocations
  })

  const onPlacesChanged = () => {
    const newPlaces = [];
    const newAddress = { 
      id: searchBox.current.getPlaces()[0].place_id,
      formattedAddress: searchBox.current.getPlaces()[0].formatted_address,
      lat: searchBox.current.getPlaces()[0].geometry.location.lat(),
      lng: searchBox.current.getPlaces()[0].geometry.location.lng(),
    }

    newPlaces.push(newAddress);

    setPlaces([...places, ...newPlaces]);
    setSearchInputValue("");
    props.setCenter(newAddress.lat, newAddress.lng);
  }

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(updateMultipleLocation(places))
  }, [places])

  useEffect(() => {
    setPlaces(selectedMultipleLocations);
  },[])

  const handleRemove = (id) => {
    const index = selectedMultipleLocations.findIndex((item) => item.id === id);
    selectedMultipleLocations.splice(index, 1);
    dispatch(updateMultipleLocation(selectedMultipleLocations))
  }

  const handleInputChange = (value) => {
    setSearchInputValue(value);
  }

  return(
  <div data-standalone-searchbox="" id="multipleLocationSearchBox">
    <StandaloneSearchBox
      ref={searchBox}
      bounds={props.bounds}
      onPlacesChanged={onPlacesChanged}
    >
      <input
        type="text"
        placeholder="Search Location..."
        className="multipleSearchInput"
        value={searchInputValue}
        onChange={(e) => handleInputChange(e.target.value)}
      />
    </StandaloneSearchBox>
    <ol>
      {places.map(({ place_id, formatted_address }) =>
        <li key={place_id}>
          {formatted_address}
        </li>
      )}
    </ol>
    {
      showList && selectedMultipleLocations.length > 0 &&
      <div id="multipleAddressList">
        <MultipleLocationList 
          selectedMultipleLocations={selectedMultipleLocations} 
          handleRemove={handleRemove}
        />
      </div>
    }
  </div>
  )

});

export default PlacesWithStandaloneSearchBox