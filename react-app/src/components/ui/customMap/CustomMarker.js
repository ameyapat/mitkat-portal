import React, { Component } from 'react';
import {
    Marker,
    InfoWindow
  } from "react-google-maps";
// import style from './style';
import { withCookies } from 'react-cookie';
import { connect } from 'react-redux'

import { updateMultipleLocation } from '../../../actions/multipleLocationActions'
@withCookies
@connect((state) => {
    return {
        multipleLocations: state.multipleLocations
    }
},
(dispatch) => {
    return {
        dispatch
    }
})
// @injectSheet(style)
class CustomMarker extends Component {

    state = {
        showInfoBox : false,
        showModal : false,
        eventDetail: ""
    }

    markerRef = React.createRef();

    handleShowInfoBox = (showInfoBox) => {
        this.setState({showInfoBox});
    }

    handleUpdateStats = () => {
        this.props.updateStats(this.props.feedDetails);
        this.handleShowInfoBox(true);
    }

    getReverseGeoCode = (lat, lng, id) => {
        let reqObj = {
            url: `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&key=AIzaSyCYOTLyn6xbysArlvlW4zpxnH9p6H3An08&v=3.exp`,
            method: 'GET',
            isAuth: false
        }

        fetch(reqObj.url)
        .then(data => data.json())
        .then(res => {
            let address = res.results[0].formatted_address;
            this.setState({ address }, () => this.handlePinUpdate(lat, lng, id));
        })
        .catch(e => console.log(e))
    }

    handlePinUpdate = (lat, lng, id) => {
        const { address } = this.state;
        const { selectedMultipleLocations } = this.props.multipleLocations
        const { dispatch } = this.props;
        const index = selectedMultipleLocations.findIndex((item) => item.id === id)

        selectedMultipleLocations[index].lat = lat;
        selectedMultipleLocations[index].lng = lng;
        selectedMultipleLocations[index].formattedAddress = address;
        this.props.setCenter(lat, lng)
        dispatch(updateMultipleLocation(selectedMultipleLocations))
    }

    handleMarkerDragEnd = (dragObj) => {
        const { id } = this.props;
        const newLat = dragObj.latLng.lat();
        const newLng = dragObj.latLng.lng();

        this.getReverseGeoCode(newLat, newLng, id);
    }

  render() {

    let { 
        title, 
        lat, 
        lng, 
        customIcon, 
        classes,
        showTitles,
    } = this.props;
    let { showInfoBox } = this.state;

    return (
        <Marker
            // onMouseOver={() => showStatsOnHover ? this.handleUpdateStats() : this.handleShowInfoBox(true)}
            // onMouseOut={() => this.handleShowInfoBox(false)}
            position={{ lat, lng }}
            // onClick={showStatsOnHover ? () => updateStats(feedDetails) : () => console.log('not allowed!!!')}
            icon={customIcon}
            ref={this.markerRef}
            optimized={false}
            draggable={true}
            onDragEnd={(dragObj) => this.handleMarkerDragEnd(dragObj)}
        >
            {
                // showInfoBox && showTitles &&
                // <React.Fragment>
                //     <InfoWindow
                //         onCloseClick={() => this.handleShowInfoBox(false)}
                //     >
                //         <div className={classes.rootAlertListWrap}>
                //             <p className="alert--title">{ title }</p>
                //         </div>
                //     </InfoWindow>                    
                // </React.Fragment>
            }
        </Marker>
    )
  }
}

export default CustomMarker;
