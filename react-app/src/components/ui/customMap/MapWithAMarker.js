import React, { Component } from 'react';
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
} from 'react-google-maps';
import CustomMarker from './CustomMarker';
import { connect } from 'react-redux';
import { withCookies } from 'react-cookie';
import locationIcon from '../../../assets/icons/pin.png'
import PlacesWithStandaloneSearchBox from './StandaloneSearchBox'

@withCookies
@connect(
  state => {
    return {
      appState: state.appState,
      multipleLocations: state.multipleLocations
    }
  },
  dispatch => {
    return {
      dispatch,
    };
  },
)
@withScriptjs
@withGoogleMap
class MapWithAMarker extends Component { 

  state = {
    eventDetails: [],
    clientPinDetails: [],
    showMarkers: false,
    showLocation: true,
    latState: 16.950209,
    lngState: 93.207995,
    defaultOptions: {
      mapTypeControl: false,
      streetViewControl: false,
      zoomControl: false,
      fullscreenControl: false,
      draggable: true,
    }
  };

  setCenter = (lat, lng) => this.setState({ latState: lat, lngState: lng });

  render() {
    const { eventInfo, riskTracker, multipleLocations: { selectedMultipleLocations } } = this.props;
    let { latState, lngState, defaultOptions } = this.state;
    let CustomMarkerXML = [];

    if (selectedMultipleLocations) {
      CustomMarkerXML =
        selectedMultipleLocations.length > 0 &&
        selectedMultipleLocations.map((list, index) => {
          let lat = parseFloat(list.lat);
          let lng = parseFloat(list.lng);

            return (
              <CustomMarker
                id={list.id}
                key={index}
                lat={lat}
                lng={lng}
                customIcon={locationIcon}
                // title={list.title}
                setCenter={this.setCenter}
              />
            );
        });
    }

    const enableCenter = riskTracker && riskTracker.riskTrackerDetails.show;
    const centerOne = {
      lat: latState,
      lng: lngState,
    };

    return (
      <GoogleMap
        defaultZoom={3.7}
        defaultOptions={defaultOptions}
        options={defaultOptions}
        defaultCenter={{ lat: latState, lng: lngState }}
        center={{ lat: latState, lng: lngState }}
        onTilesLoaded={() => this.setState({ showMarkers: true })}
        {...(enableCenter && { center: centerOne })}
      >
        {this.state.showMarkers && CustomMarkerXML}
        <PlacesWithStandaloneSearchBox setCenter={this.setCenter} />
      </GoogleMap>
    );
  }
}

export default MapWithAMarker;
