import React from 'react';
import './style.scss';

const CustomLabel = ({title, classes, isRequired}) => <div className={`labelWrap ${classes ? classes : ''}`}><label>{title}{isRequired && <span className="asterisk">*</span>}</label></div>

export default CustomLabel