import React, { Component, createRef } from 'react';
import CustomListItem from './CustomListItem';
import WatchListItem from './WatchListItem';
import './list.scss';
import { localDateTime } from '../../../helpers/utils';

class CustomList extends Component {
  renderWatchList = () => {
    const {
      isWatchList = true,
      watchList,
      removeWatchList,
      parentHandler,
      onHighlight,
    } = this.props;

    if (watchList.length > 0) {
      return watchList
        .sort(
          (x, y) =>
            new Date(y.latestUpdatedatetime).getTime() -
            new Date(x.latestUpdatedatetime).getTime(),
        )
        .map(f => (
          <WatchListItem
            key={f.id}
            f={f}
            eventId={f.id}
            title={f.title}
            description={f.description}
            parentHandler={parentHandler}
            riskCategory={f.riskcategory}
            riskLevel={f.risklevel}
            timestamp={localDateTime(f.eventdatetime)}
            onHighlight={onHighlight}
            updates={f.updates}
            removeWatchList={removeWatchList}
            isWatchList={isWatchList}
          />
        ));
      // .reverse()
    }
  };

  render() {
    const {
      feed,
      parentHandler,
      onHighlight,
      addWatchList,
      removeWatchList,
      isWatchList = false,
      isFirstTime,
    } = this.props;
    return (
      <ul className="customListWrap">
        {isWatchList && this.renderWatchList()}
        {!isWatchList &&
          feed.length > 0 &&
          feed.map((f, idx) => (
            <CustomListItem
              key={f.id}
              f={f}
              eventId={f.id}
              title={f.title}
              parentHandler={parentHandler}
              riskCategory={f.riskcategory}
              riskLevel={f.risklevel}
              dateTime={localDateTime(f.datetime)}
              onHighlight={onHighlight}
              addWatchList={addWatchList}
              removeWatchList={removeWatchList}
              isFirstTime={isFirstTime}
            />
          ))}
      </ul>
    );
  }
}

export default CustomList;
