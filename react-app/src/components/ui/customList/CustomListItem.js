import React, { Component } from 'react';
import './list.scss';
import '../../../sass/globalVariables.scss';
import CyberIcon from '../../../assets/icons/cyber-icon-new.png';
import TerrorIcon from '../../../assets/icons/terrorism.png';
import CivilIcon from '../../../assets/icons/civil-disturbance.png';
import EnvironmentIcon from '../../../assets/icons/enviornment.png';
import HealthIcon from '../../../assets/icons/health.png';
import InfraIcon from '../../../assets/icons/infrastructure.png';
import CrimeIcon from '../../../assets/icons/terrorism.png'; //temporary
import { toggleRiskTrackerBox } from '../../../actions';
import store from '../../../store';
import { riskCategoryUrls } from '../../../context/mapping';

import { connect } from 'react-redux';
import clsx from 'clsx';
import moment from 'moment';

const RISK_CAT_MAP = {
  1: CyberIcon, // Cyber
  2: TerrorIcon, // Insurgency / Terrorism
  3: CivilIcon, // Civil Disturbance
  4: EnvironmentIcon, // Environment
  5: HealthIcon, // Health
  6: InfraIcon, // Critical Infrastructure
  7: CrimeIcon, // Crime
};

const RISK_CAT_MAP_LABELS = {
  1: 'Cyber', // Cyber
  2: 'Extremism', // Insurgency / Terrorism
  3: 'Civil Disturbance', // Civil Disturbance
  4: 'Environment', // Environment
  5: 'Health', // Health
  6: 'Infrastructure', // Critical Infrastructure
  7: 'Crime',
  8: 'Travel Risks',
  9: 'Natural Disasters',
  10: 'External Threats',
  11: 'Political',
  12: 'Regulatory',
};

const RISK_CAT_MAP_COLORS = {
  1: 'veryLow',
  2: 'low',
  3: 'medium',
  4: 'high',
  5: 'veryHigh',
};

const RISK_CATEGORY_URLS = riskCategoryUrls;
const TIMEOUT = 600000;

@connect(state => {
  return {
    rime: state.rime,
    themeChange: state.themeChange,
    watchList: state.mapState.watchList,
  };
})
class CustomListItem extends Component {
  state = {
    isNew: true,
  };

  componentDidMount() {
    setTimeout(() => {
      this.setState({ isNew: false });
    }, TIMEOUT);
  }

  highlightBookmark = () => {
    const { watchList, eventId } = this.props;
    return Object.keys(watchList).includes(String(eventId));
  };

  handleWatchlist = e => {
    const { eventId, addWatchList, removeWatchList } = this.props;
    e.stopPropagation();
    if (this.highlightBookmark()) {
      removeWatchList(eventId);
    } else {
      addWatchList(eventId);
    }
  };

  render() {
    const {
      riskCategory,
      title,
      dateTime,
      riskLevel,
      onHighlight,
      isFirstTime,
    } = this.props;
    const {
      themeChange: { setDarkTheme },
    } = this.props;

    return (
      <li
        className={`customlist--item ${setDarkTheme ? 'dark' : 'light'} ${
          RISK_CAT_MAP_COLORS[riskLevel]
        }`}
        onClick={() => this.props.parentHandler(this.props.eventId)}
        onMouseOver={() =>
          this.props.rime.lockScreen &&
          onHighlight({ ...this.props.f, show: true })
        }
        // onMouseOut={() => onHighlight({...this.props.f, show: false})}
        onMouseOut={() =>
          store.dispatch(
            toggleRiskTrackerBox({
              riskTrackerDetails: { lat: '', lng: '', show: false },
            }),
          )
        }
      >
        <div className="right--list">
          <h1 className="list--title">{title}</h1>
          <span
            className={`customlist--item__pills ${RISK_CAT_MAP_COLORS[riskLevel]}`}
          >
            {RISK_CAT_MAP_LABELS[riskCategory]}
          </span>
          <div className="list--time--tense">
            <ul className="tense--list">
              <li className="timestamp--list">
                {moment(dateTime).format('Do MMM YYYY, h:mm:ss a')}
              </li>
            </ul>
            <div className="watchListBtnInner">
              <span
                className="watchListBtnWrap"
                onClick={e => this.handleWatchlist(e)}
              >
                <i
                  className={clsx({
                    fa: this.highlightBookmark(),
                    'fa-bookmark': true,
                    far: !this.highlightBookmark(),
                  })}
                  title={`${
                    this.highlightBookmark() ? 'Remove from' : 'Add to'
                  } watchlist`}
                ></i>
              </span>
              {this.state.isNew && !isFirstTime && (
                <span
                  className={`cat__pill flash`}
                  style={{
                    fontWeight: 400,
                    color: 'rgba(18,18,18,1)',
                  }}
                >
                  New
                </span>
              )}
            </div>
          </div>
        </div>
      </li>
    );
  }
}

export default CustomListItem;
