import React, { Component } from 'react';
import './list.scss';
import '../../../sass/globalVariables.scss';
import CyberIcon from '../../../assets/icons/cyber-icon-new.png';
import TerrorIcon from '../../../assets/icons/terrorism.png';
import CivilIcon from '../../../assets/icons/civil-disturbance.png';
import EnvironmentIcon from '../../../assets/icons/enviornment.png';
import HealthIcon from '../../../assets/icons/health.png';
import InfraIcon from '../../../assets/icons/infrastructure.png';
import CrimeIcon from '../../../assets/icons/terrorism.png'; //temporary
import { toggleRiskTrackerBox } from '../../../actions';
import store from '../../../store';
import { riskCategoryUrls } from '../../../context/mapping';

import { connect } from 'react-redux';
import clsx from 'clsx';
import moment from 'moment';

const RISK_CAT_MAP = {
  1: CyberIcon, // Cyber
  2: TerrorIcon, // Insurgency / Terrorism
  3: CivilIcon, // Civil Disturbance
  4: EnvironmentIcon, // Environment
  5: HealthIcon, // Health
  6: InfraIcon, // Critical Infrastructure
  7: CrimeIcon, // Crime
};

const RISK_CAT_MAP_LABELS = {
  1: 'Cyber', // Cyber
  2: 'Extremism', // Insurgency / Terrorism
  3: 'Civil Disturbance', // Civil Disturbance
  4: 'Environment', // Environment
  5: 'Health', // Health
  6: 'Infrastructure', // Critical Infrastructure
  7: 'Crime',
  8: 'Travel Risks',
  9: 'Natural Disasters',
  10: 'External Threats',
  11: 'Political',
  12: 'Regulatory',
};

const RISK_CAT_MAP_COLORS = {
  1: 'veryLow',
  2: 'low',
  3: 'medium',
  4: 'high',
  5: 'veryHigh',
};

const RISK_CATEGORY_URLS = riskCategoryUrls;
const TIMEOUT = 600000;

@connect(state => {
  return {
    rime: state.rime,
    themeChange: state.themeChange,
    watchList: state.mapState.watchList,
  };
})
class WatchListItem extends Component {
  state = {
    isNew: true,
    isBookmarked: false,
  };

  componentDidMount() {
    setTimeout(() => {
      this.setState({ isNew: false });
    }, TIMEOUT);
    this.isWatchlisted();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.watchList !== this.props.watchList) {
      this.isWatchlisted();
    }
  }

  isWatchlisted = () => {
    const { watchList, eventId } = this.props;
    for (const id in watchList) {
      if (Number(id) === Number(eventId)) {
        this.setState({ isBookmarked: true });
      }
    }
  };

  highlightBookmark = isBookmarked => {
    this.setState({ isBookmarked });
  };

  handleWatchlist = e => {
    const { eventId, removeWatchList } = this.props;
    e.stopPropagation();
    removeWatchList(eventId);
  };

  render() {
    const {
      riskCategory,
      title,
      description,
      updates,
      timestamp,
      riskLevel,
      onHighlight,
    } = this.props;
    const {
      themeChange: { setDarkTheme },
    } = this.props;
    const { isBookmarked } = this.state;
    const imgUrl =
      (RISK_CATEGORY_URLS[riskCategory] &&
        RISK_CATEGORY_URLS[riskCategory].riskLevels[riskLevel] &&
        RISK_CATEGORY_URLS[riskCategory].riskLevels[riskLevel]['url']) ||
      RISK_CAT_MAP[riskCategory];

    return (
      <li
        className={`customlist--item ${setDarkTheme ? 'dark' : 'light'} ${
          RISK_CAT_MAP_COLORS[riskLevel]
        }`}
        onClick={() => this.props.parentHandler(this.props.eventId)}
        onMouseOver={() =>
          this.props.rime.lockScreen &&
          onHighlight({ ...this.props.f, show: true })
        }
        onMouseOut={() =>
          store.dispatch(
            toggleRiskTrackerBox({
              riskTrackerDetails: { lat: '', lng: '', show: false },
            }),
          )
        }
      >
        <div className="right--list">
          <h1 className="list--title">{title}</h1>
          <span
            className={`customlist--item__pills ${RISK_CAT_MAP_COLORS[riskLevel]}`}
          >
            {RISK_CAT_MAP_LABELS[riskCategory]}
          </span>
          {/* <h6 className="watchListItem-heading">Description</h6> */}
          {description && (
            <div className="watchListItem-desc-listWrap description">
              <p className="list--description">{description}</p>
            </div>
          )}
          {updates.length > 0 && (
            <h6 className="watchListItem-heading">Current Updates</h6>
          )}
          {updates.length > 0 && (
            <div className="watchListItem-desc-listWrap updates">
              <ul className="watchListItem-desc-list">
                {updates
                  .map(update => {
                    return (
                      <li className="watchListItem-desc-list-item">
                        <p>{update.title}</p>
                        <span className="timestamp--updates">
                          {update.timestamp}
                        </span>
                      </li>
                    );
                  })
                  .reverse()}
              </ul>
            </div>
          )}

          <div className="list--time--tense">
            <ul className="tense--list">
              <li className="timestamp--list">
                {moment(timestamp).format('Do MMM YYYY, h:mm:ss a')}
              </li>
            </ul>
            <div className="watchListBtnInner">
              <span
                className="watchListBtnWrap"
                onClick={e => this.handleWatchlist(e)}
              >
                <i
                  className={clsx({
                    'fa-trash': true,
                    fas: true,
                  })}
                  title={'Remove from watchlist'}
                >
                  <span className="fa-trash-remove">Remove</span>
                </i>
              </span>
            </div>
          </div>
        </div>
      </li>
    );
  }
}

export default WatchListItem;
