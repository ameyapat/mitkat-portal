export default {
  input: {
    display: 'none',
  },
  uploadWrap: {
    margin: '15px 0',
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    border: '0.1em dashed #f1f1f1',
    borderRadius: 3,
    padding: 20,
    '&:last-child': {
      // borderBottom: 0
    },
  },
  heading: {
    color: 'grey',
    fontWeight: 'normal',
  },
  logoWrap: {
    paddingBottom: 15,
    textAlign: 'center',
    '& img': {
      width: 64,
      '&.uploadedImg': {
        width: 270,
      },
    },
    '& p': {
      fontSize: 14,
      color: 'grey',
      margin: '10px 0 0',
    },
    '& .file--name': {
      color: '#333',
    },
    '& .error-txt': {
      color: 'red',
    },
  },
  rootAction: {
    textAlign: 'center',
    marginTop: 20,
  },
  indicator: {
    background: '#34495e',
  },
  rootTabs: {
    // background: 'red'
  },
  rootAppBar: {
    background: 'transparent',
    boxShadow: 'none',
    marginTop: 15,
  },
  btnRoot: {
    color: '#34495e',
    borderColor: '#34495e',

    '&:hover': {
      borderColor: '#34495e',
      color: '#34495e',
      background: 'rgba(52, 73, 94, 0.1)',
    },
  },
  rootLabelLight: {
    color: 'rgba(0,0,0, 0.67) ',
  },
  rootInputLight: {
    color: 'rgba(0,0,0, 0.87) ',
  },
  notchedOutlineLight: {
    borderColor: '#757575 ',
  },
  bodyWrapper: {
    maxHeight: 400,
    overflowY: 'auto',
    overflowX: 'hidden',
    '&::-webkit-scrollbar': {
      width: 0,
    },
  },
  displayList: {
    padding: 0,
  },
  displayListItem: {
    paddingBottom: 5,
    '& a': {
      display: 'block',
      paddingTop: 5,
    },
  },
};