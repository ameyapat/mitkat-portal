import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Select from 'react-select';
import MultiSelect from '../../ui/multiSelect';
import CustomLabel from '../../ui/customLabel';
import CustomDatePicker from '../customDatePicker';
//assets
import fileIcon from './assets/file-icon.svg';
import imgIcon from './assets/img-icon.png';
//helpers
import injectSheet from 'react-jss';
import styles from './style';

import { getLiveTopics } from '../../../api/api';
import {
  fetchCountries,
  countriesObjToArr,
  getSelectedTopic,
} from '../../../helpers/utils';
import { customStyles } from './selectStyle';
import './style.scss';

const initialState = {
  heading: 'Add New',
  imageFile: null,
  document: null,
  title: '',
  tabValue: 0,
  date: new Date(),
  selectedLiveTopic: null,
  selectedCountries: [],
  allLiveTopics: [],
  allCountries: [],
  linkURL: '',
};

@injectSheet(styles)
class CustomReportsForm extends Component {
  state = {
    ...initialState,
  };

  componentDidMount() {
    let { heading } = this.props;
    this.getAllLiveTopics();
    this.getAllCountries();
    this.setState({ heading });
  }

  resetInitForm = () => {
    this.setState({ 
      imageFile: null,
      document: null,
      title: '',
      date: new Date(),
      selectedLiveTopic: null,
      selectedCountries: [],
     });
  };

  initForm = () => {
    const {
      details: {
        title,
        dispaypicurl: displayUrl,
        advisoryurl,
        uploadtime,
        uploadTime,
        advisorycountries,
        topicid,
        linkURL = null,
      },
    } = this.props;

    this.setState({
      // imageFile: displayUrl,
      // document: advisoryurl,
      displayUrl: displayUrl || null,
      advisoryUrl: advisoryurl || null,
      title,
      date: uploadtime || uploadTime,
      linkURL,
      selectedLiveTopic: getSelectedTopic({
        id: topicid,
        allTopics: this.state.allLiveTopics,
      }),
      selectedCountries: countriesObjToArr(advisorycountries),
    });
  };

  getAllCountries = () => {
    fetchCountries()
      .then(allCountries => {
        this.setState({ allCountries });
      })
      .catch(e => {
        console.log('Error fetching countries', e);
      });
  };

  getAllLiveTopics = () => {
    getLiveTopics()
      .then(data => {
        let allLiveTopics = [];
        allLiveTopics = data.map(t => {
          let allLiveTopicsObj = {};
          allLiveTopicsObj['value'] = t.id;
          allLiveTopicsObj['label'] = t.topicName;
          return allLiveTopicsObj;
        });
        this.setState({ allLiveTopics }, () => {
          if (this.props.isEdit) {
            this.initForm();
          } else {
            this.resetInitForm();
          }
        });
      })
      .catch(e => console.log('Error fetching live topics', e));
  };

  handleInputChange = (type, value) => {
    this.setState({ [type]: value });
  };

  handleFileChange = (type, file) => {
    this.setState({ [type]: file });
  };

  handleSubmit = () => {
    this.props.handleSubmit(this.state);
  };

  handleTabChange = (e, value) => {
    this.setState({ tabValue: value });
  };

  render() {
    let {
      classes,
      hasCustomProps,
      titlePlaceHolder,
      imagePlaceHolder,
      filePlaceHolder,
      showTopics = false,
      showCountries = false,
      showDate = false,
      showPodcastLink = false,
      showUploadTabs = true,
      isEdit,
      showLinks = false,
    } = this.props;
    let {
      title,
      heading,
      imageFile,
      document,
      tabValue,
      allLiveTopics,
      selectedLiveTopic,
      selectedCountries,
      allCountries,
      date,
      linkURL,
      displayUrl,
      advisoryUrl,
    } = this.state;
    
    return (
      <div className={classes && classes.root}>
        <div className={classes.bodyWrapper}>
          <h4 className={classes.heading}>{heading}</h4>
          <TextField
            label="Title"
            // className={classes}
            value={title}
            placeholder={titlePlaceHolder}
            onChange={e => this.handleInputChange('title', e.target.value)}
            fullWidth={true}
            margin="normal"
            variant="outlined"
          />
          {showPodcastLink && (
            <div className="">
              <TextField
                label="Podcast Link"
                value={linkURL}
                placeholder={'Podcast Link'}
                onChange={e =>
                  this.handleInputChange('linkURL', e.target.value)
                }
                fullWidth={true}
                margin="normal"
                variant="outlined"
              />
            </div>
          )}
          {showTopics && (
            <div className="selectTopicWrap">
              <CustomLabel
                title="Select Topic"
                classes="selectTopic--label"
                isRequired={false}
              />
              <Select
                defaultValue={selectedLiveTopic}
                className="select--options"
                value={selectedLiveTopic}
                placeholder={'Select Topic'}
                onChange={value => this.setState({ selectedLiveTopic: value })}
                options={allLiveTopics}
                styles={customStyles}
              />
            </div>
          )}

          {showCountries && (
            <div className="selectTopicWrap">
              <CustomLabel
                title="Select Countries"
                classes="selectTopic--label"
                isRequired={false}
              />
              <MultiSelect
                value={selectedCountries}
                placeholder={'Select Countries'}
                parentEventHandler={value =>
                  this.handleInputChange('selectedCountries', value)
                }
                options={allCountries}
                hasCustomStyles={true}
                customStyles={customStyles}
              />
            </div>
          )}
          {showDate && (
            <CustomDatePicker
              label={'Select Date'}
              value={date}
              handleDateChange={(type, date) =>
                this.handleInputChange('date', date)
              }
              inputLabelClasses={{
                root: classes.rootLabelLight,
              }}
              inputPropsClasses={{
                root: classes.rootInputLight,
                input: classes.rootInputLight,
                notchedOutline: classes.notchedOutlineLight,
              }}
            />
          )}
          {showLinks && (
            <div>
              <ul className={classes.displayList}>
                <li className={classes.displayListItem}>
                  <strong>Uploaded Image:</strong>
                  <a href={displayUrl} target="_blank">
                    {displayUrl || 'No Image uploaded'}
                  </a>
                </li>
                <li className={classes.displayListItem}>
                  <strong>Uploaded PDF:</strong>
                  <a href={advisoryUrl} target="_blank">
                    {advisoryUrl || 'No PDF uploaded'}
                  </a>
                </li>
              </ul>
            </div>
          )}
          {showUploadTabs && (
            <>
              <AppBar
                position="static"
                color="default"
                classes={{ root: classes.rootAppBar }}
              >
                <Tabs
                  fullWidth={true}
                  classes={{
                    indicator: classes.indicator,
                    root: classes.rootTabs,
                  }}
                  // variant="fullWidth"
                  value={tabValue}
                  onChange={this.handleTabChange}
                >
                  <Tab label="Upload Image" />
                  <Tab label="Upload Pdf" />
                </Tabs>
              </AppBar>
              <div>
                {tabValue === 0 ? (
                  <div className={classes.uploadWrap}>
                    <div className={classes.logoWrap}>
                      {imageFile ? (
                        <img
                          src={
                            !isEdit ? URL.createObjectURL(imageFile) : imageFile
                          }
                          className="uploadedImg"
                          alt="fileIcon"
                        />
                      ) : (
                        <img src={imgIcon} alt="upload-icon" />
                      )}
                      {!imageFile && <p>{imagePlaceHolder}</p>}
                    </div>
                    <input
                      accept="image/*"
                      className={classes.input}
                      id="contained-button-file"
                      multiple
                      type="file"
                      onChange={e =>
                        this.handleFileChange('imageFile', e.target.files[0])
                      }
                    />
                    <label htmlFor="contained-button-file">
                      <Button variant="outlined" component="span">
                        {!imageFile ? 'Upload' : 'Update'}
                      </Button>
                    </label>
                  </div>
                ) : (
                  <div className={classes.uploadWrap}>
                    <div className={classes.logoWrap}>
                      {document ? (
                        <>
                          <p className="file--name">{document.name}</p>
                          {document && document.type !== 'application/pdf' && (
                            <p className="error-txt">
                              Please upload a .pdf file
                            </p>
                          )}
                        </>
                      ) : (
                        <img src={fileIcon} alt="upload-icon" />
                      )}
                      {!document && <p>{filePlaceHolder}</p>}
                    </div>
                    <input
                      accept="application/pdf"
                      className={classes.input}
                      id="contained-button-file1"
                      multiple
                      type="file"
                      onChange={e => {
                        this.handleFileChange('document', e.target.files[0]);
                      }}
                    />
                    <label htmlFor="contained-button-file1">
                      <Button variant="outlined" component="span">
                        {!document ? 'Upload' : 'Update'}
                      </Button>
                    </label>
                  </div>
                )}
              </div>
            </>
          )}
          {hasCustomProps && this.props.customRender()}
        </div>
        <div className={classes.rootAction}>
          <Button
            // disabled={
            //   title === '' ||
            //   (showUploadTabs &&
            //     (imageFile === null ||
            //       document === null ||
            //       document?.type !== 'application/pdf'))
            // }
            fullWidth={true}
            classes={{ root: classes.btnRoot }}
            variant="outlined"
            color="primary"
            component="span"
            onClick={() => this.handleSubmit()}
          >
            Submit
          </Button>
        </div>
      </div>
    );
  }
}

export default CustomReportsForm;
