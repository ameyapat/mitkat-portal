export const customStyles = {
  control: provided => ({
    ...provided,
    backgroundColor: '#ffffff',
    color: 'var(--backgroundSolid)',
  }),
  option: (provided, state) => ({
    ...provided,
    zIndex: '99999',
  }),
  placeholder: provided => ({
    ...provided,
    color: 'var(--backgroundSolid)',
  }),
  singleValue: provided => ({
    ...provided,
    color: 'var(--backgroundSolid)',
  }),
};
