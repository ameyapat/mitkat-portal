import { ThemeContext, themes } from '../../../sass/theme/themeContext';
import React, { useState } from 'react';
import { Button } from '@material-ui/core';

function SwitchTheme() {
  const [darkMode, setDarkMode] = useState(true);

  return (
    <div className="App">
      <header className="App-header">
        <h1 className="text-warning">Dark/Light mode</h1>
        <ThemeContext.Consumer>
          {({ changeTheme }) => (
            <Button
              color="link"
              onClick={() => {
                setDarkMode(!darkMode);
                changeTheme(darkMode ? themes.light : themes.dark);
              }}
            >
              <i className={darkMode ? 'fas fa-sun' : 'fas fa-moon'}></i>
              <span className="d-lg-none d-md-block">Switch mode</span>
            </Button>
          )}
        </ThemeContext.Consumer>{' '}
      </header>
    </div>
  );
}

export default SwitchTheme;
