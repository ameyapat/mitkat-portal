export const USER_ROLES = {
  ROLE_CREATOR: 'nep',
  ROLE_APPROVER: 'pum',
  ROLE_PARTNER_CREATOR: 'rpc',
  ROLE_PARTNER_APPROVER: 'rpa',
};
