import React, { Component } from 'react';
import './style.scss';
import Button from '@material-ui/core/Button';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
//helpers
import { API_ROUTES } from '../../../helpers/http/apiRoutes';
import { fetchApi } from '../../../helpers/http/fetch';
import { USER_ROLES } from './constants';
//redux
import { connect } from 'react-redux';
//
import store from '../../../store';
import { toggleToast } from '../../../actions';
import { withCookies } from 'react-cookie';
import validator from 'validator';
import styles from './style';
import injectSheet from 'react-jss';

import { editUserRole } from '../../../requestor/admin/requestor';

@injectSheet(styles)
@withCookies
@connect(state => {
  return {
    authToken: state.appState.authToken,
  };
})
class TableItems extends Component {
  state = {
    name: '',
    username: '',
    password: '',
    email: '',
    currentRole: 1,
  };

  componentDidMount() {
    let { name, username, password, email, clearance } = this.props;
    let { currentRole } = this.state;
    currentRole = clearance[0]?.name === 'ROLE_CREATOR' ? 1 : 2;
    this.setState({ name, username, password, email, currentRole });
  }

  handleInputChange = (type, value) => {
    this.setState({ [type]: value });
  };

  handlePasswordChange = (e, type, id) => {
    e.preventDefault();
    let { cookies } = this.props;
    const { password } = this.state;
    let authTokenFromCookie = cookies.get('authToken');

    let params = {
      url: `${API_ROUTES.adminUpdateUser}/${id}/${type}`,
      method: 'POST',
      isAuth: true,
      authToken: authTokenFromCookie,
      data: { password: password },
      showToggle: true,
    };

    if (validator.isStrongPassword(password)) {
      fetchApi(params)
        .then(res => {
          if (res.success) {
            store.dispatch(
              toggleToast({ showToast: true, toastMsg: res.message }),
            );
            this.props.getUserList();
          } else {
            store.dispatch(
              toggleToast({ showToast: true, toastMsg: res.message }),
            );
          }
        })
        .catch(e => {
          store.dispatch(
            toggleToast({ showToast: true, toastMsg: 'Something went wrong!' }),
          );
        });
    } else {
      this.props.togglePasswordAlert(true);
    }
  };

  updateUserDetail = (e, type, id) => {
    e.preventDefault();
    let { cookies } = this.props;
    let authTokenFromCookie = cookies.get('authToken');

    let params = {
      url: `${API_ROUTES.adminUpdateUser}/${id}/${type}/${this.state[type]}`,
      method: 'POST',
      isAuth: true,
      authToken: authTokenFromCookie,
      showToggle: true,
    };

    fetchApi(params)
      .then(res => {
        if (res.success) {
          store.dispatch(
            toggleToast({ showToast: true, toastMsg: res.message }),
          );
          this.props.getUserList();
        } else {
          store.dispatch(
            toggleToast({ showToast: true, toastMsg: res.message }),
          );
        }
      })
      .catch(e => {
        store.dispatch(
          toggleToast({ showToast: true, toastMsg: 'Something went wrong!' }),
        );
      });
  };

  handleCleranceChange = e => {
    const { value } = e.target;
    const { id, getUserList } = this.props;
    const reqData = {
      userid: id,
      clearance: Number(value),
    };

    this.setState({ currentRole: Number(value) });

    editUserRole(reqData)
      .then(data => {
        let toastObj = {
          toastMsg: data.message,
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
        getUserList();
      })
      .catch(e => {
        let toastObj = {
          toastMsg: 'Something went wrong!',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
        console.log(e, 'Error changing roles');
      });
  };

  render() {
    let {
      showViewDetails = false,
      handleUserDetails,
      id,
      clearance,
      classes,
      meta,
    } = this.props;
    let { name, username, password, email, currentRole } = this.state;

    return (
      <tr>
        <td>
          <form onSubmit={e => this.updateUserDetail(e, 'name', id)}>
            <input
              value={name}
              type="text"
              onChange={e => this.handleInputChange('name', e.target.value)}
            />
          </form>
        </td>
        <td>
          <form onSubmit={e => this.updateUserDetail(e, 'username', id)}>
            <input
              value={username}
              type="text"
              onChange={e => this.handleInputChange('username', e.target.value)}
            />
          </form>
        </td>
        <td>
          <form onSubmit={e => this.updateUserDetail(e, 'email', id)}>
            <input
              value={email}
              type="email"
              onChange={e => this.handleInputChange('email', e.target.value)}
            />
          </form>
        </td>
        <td>
          <form onSubmit={e => this.handlePasswordChange(e, 'password', id)}>
            <input
              value={password}
              type="password"
              onChange={e => this.handleInputChange('password', e.target.value)}
            />
          </form>
        </td>
        <td>
          <label className={`labelTxt ${USER_ROLES[clearance[0].name]}`}>
            {clearance[0].name}
          </label>
        </td>
        {meta?.enableClearance && (
          <td>
            <FormControl component="fieldset">
              <RadioGroup
                aria-label={'switch-roles'}
                name={'switch-roles'}
                value={currentRole}
                onChange={e => this.handleCleranceChange(e)}
                className="radioWrapper"
              >
                <FormControlLabel
                  value={1}
                  control={
                    <Radio
                      classes={{
                        root: classes.rootRadio,
                        checked: classes.radioChecked,
                      }}
                    />
                  }
                  label="Creator"
                  className={classes.radioLabel}
                />
                <FormControlLabel
                  value={2}
                  control={
                    <Radio
                      classes={{
                        root: classes.rootRadio,
                        checked: classes.radioChecked,
                      }}
                    />
                  }
                  label="Approver"
                  className={classes.radioLabel}
                />
              </RadioGroup>
            </FormControl>
          </td>
        )}
        {showViewDetails && (
          <td>
            <Button
              className="viewDetails"
              onClick={() => handleUserDetails(true, id)}
              variant="outlined"
              color="default"
              title="View Details"
            >
              <i class="fas fa-eye"></i>
            </Button>
          </td>
        )}
      </tr>
    );
  }
}

export default TableItems;
