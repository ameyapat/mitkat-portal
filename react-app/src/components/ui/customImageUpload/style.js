export default {
  input: {
    display: 'none',
  },
  uploadWrap: {
    margin: '15px 0',
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    border: '0.1em dashed #f1f1f1',
    borderRadius: 3,
    padding: 20,
    '&:last-child': {
      // borderBottom: 0
    },
  },
  heading: {
    color: 'grey',
    fontWeight: 'normal',
  },
  logoWrap: {
    paddingBottom: 15,
    textAlign: 'center',
    '& img': {
      width: 64,
      '&.uploadedImg': {
        width: 270,
      },
    },
    '& p': {
      fontSize: 14,
      color: 'grey',
      margin: '10px 0 0',
    },
    '& .file--name': {
      color: '#333',
    },
    '& .error-txt': {
      color: 'red',
    },
  },
  rootAction: {
    textAlign: 'center',
  },
  indicator: {
    background: '#34495e',
  },
  rootTabs: {
    // background: 'red'
  },
  rootAppBar: {
    background: 'transparent',
    boxShadow: 'none',
  },
  btnRoot: {
    color: '#34495e',
    borderColor: '#34495e',

    '&:hover': {
      borderColor: '#34495e',
      color: '#34495e',
      background: 'rgba(52, 73, 94, 0.1)',
    },
  },
};
