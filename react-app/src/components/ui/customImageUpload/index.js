import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
//assets
import imgIcon from './assets/img-icon.png';
//helpers
import injectSheet from 'react-jss';
import styles from './style';
import './style.scss';

@injectSheet(styles)
class CustomImageUpload extends Component {
  state = {
    heading: 'Add New',
    imageFile: null,
  };

  componentDidMount() {
    let { heading } = this.props;

    this.setState({ heading });
  }

  handleFileChange = (type, file) => {
    this.setState({ [type]: file });
  };

  handleSubmit = () => {
    this.props.handleSubmit(this.state);
  };

  render() {
    let { classes, imagePlaceHolder } = this.props;
    let { heading, imageFile } = this.state;

    return (
      <div className={classes && classes.root}>
        <h4 className={classes.heading}>{heading}</h4>

        <div className={classes.uploadWrap}>
          <div className={classes.logoWrap}>
            {imageFile ? (
              <img
                src={URL.createObjectURL(imageFile)}
                className="uploadedImg"
                alt="fileIcon"
              />
            ) : (
              <img src={imgIcon} alt="upload-icon" />
            )}
            {!imageFile && <p>{imagePlaceHolder}</p>}
          </div>
          <input
            accept="image/*"
            className={classes.input}
            id="contained-button-file"
            multiple
            type="file"
            onChange={e =>
              this.handleFileChange('imageFile', e.target.files[0])
            }
          />
          <label htmlFor="contained-button-file">
            <Button variant="outlined" component="span">
              {!imageFile ? 'Upload' : 'Update'}
            </Button>
          </label>
        </div>

        <div className={classes.rootAction}>
          <Button
            disabled={imageFile === null}
            fullWidth={true}
            classes={{ root: classes.btnRoot }}
            variant="outlined"
            color="primary"
            component="span"
            onClick={() => this.handleSubmit()}
          >
            Submit
          </Button>
        </div>
      </div>
    );
  }
}

export default CustomImageUpload;
