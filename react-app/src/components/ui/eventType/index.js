import React from 'react';
import clsx from 'clsx';
import injectSheet from 'react-jss';

const styles = {
  rootStatus: {
    width: 6,
    height: 6,
    marginLeft: 8,
    borderRadius: '50%',
    display: 'inline-block',
  },
  notification: {
    background: '#f1c749',
  },
  warning: {
    background: '#ff7f00',
  },
  crucial: {
    background: '#ed4b3f',
  },
};

const EventType = ({ type, classes }) => (
  <span
    className={clsx(
      classes.rootStatus,
      type === 1 && classes.notification,
      type === 2 && classes.warning,
      type === 3 && classes.crucial,
    )}
  />
);

export default injectSheet(styles)(EventType);
