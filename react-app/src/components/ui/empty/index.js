import React from 'react';
import './style.scss';

import clsx from 'clsx';

const defaultSrc= 'https://img.icons8.com/cotton/64/000000/empty-box.png';

const Empty = ({title, imgSrc = defaultSrc, classes, alt = "default"}) => (
    <div className={clsx("boxIsEmpty", classes && classes.box)}>
        <img 
            src={imgSrc}
            alt={alt} 
        />
        <p>{title}</p>
    </div>
)

export default Empty;