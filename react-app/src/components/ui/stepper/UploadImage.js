import React, { Component } from 'react';
import styles from './styles';
import injectSheet from 'react-jss';
import imgIcon from '../customReportsForm/assets/img-icon.png';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import { uploadImage } from '../../../actions/uploadImageAction';
import { withCookies } from 'react-cookie';

@withCookies
class UploadImage extends Component {
  state = {
    imageFile: '',
  };

  componentDidMount() {
    this.setState({ imageFile: this.props.defaultImage });
  }

  handleFileChange = (type, file, id) => {
    this.setState({ [type]: file }, () => {
      const data = {};
      const authToken = this.props.cookies.get('authToken');
      data['image'] = file;
      data['eventId'] = this.props.eventId; //eventid
      data['imageId'] = id; //imageid
      this.props.dispatch(uploadImage(data));
      this.props.getRiskWatch(authToken);
    });
  };

  render() {
    const {
      classes,
      label = 'Upload Display Image',
      id,
      defaultImage,
    } = this.props;
    const { imageFile } = this.state;

    return (
      <div className={classes.uploadWrap}>
        <div className={classes.logoWrap}>
          {imageFile ? (
            <img
              src={
                imageFile && defaultImage === imageFile
                  ? imageFile
                  : URL.createObjectURL(imageFile)
              }
              className="uploadedImg"
              alt="fileIcon"
            />
          ) : (
            <img src={imgIcon} alt="upload-icon" />
          )}
          {!imageFile && <p>{label}</p>}
        </div>
        <input
          accept="image/*"
          className={classes.input}
          id={`input-${id}`}
          multiple
          type="file"
          onChange={e =>
            this.handleFileChange('imageFile', e.target.files[0], id)
          }
        />
        <label htmlFor={`input-${id}`}>
          <Button variant="outlined" component="span">
            {!imageFile ? 'Upload' : 'Update'}
          </Button>
        </label>
      </div>
    );
  }
}

const mapStateToProps = state => ({ uploadImage: state.uploadImage });
const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(injectSheet(styles)(UploadImage));
