export default {
    rootStepper: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        height: '100%',
    },
    input:{
        display: 'none'
    },
    uploadWrap:{
        margin: '15px auto',
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column',
        border: '0.1em dashed #f1f1f1',
        borderRadius: 3,
        padding: 20,
        maxWidth: 600,
        width: '100%'
    },
    logoWrap:{
        paddingBottom: 15,
        textAlign: 'center',
        "& img":{
            width: 64,
            "&.uploadedImg":{
                width: 270
            }
        },
        "& p":{
            fontSize: 14,
            color: 'grey',
            margin: '10px 0 0'
        },
        "& .file--name":{
            color: '#333'
        },
        "& .error-txt":{
            color: 'red'
        }
    },
    btnWrapper: {
        display: 'flex',
        justifyContent: 'center',
    }
}