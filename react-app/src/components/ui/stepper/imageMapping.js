export const imageMapping = [
    {
        activeWhen: 0,
        label: 'Upload First Image',
        id: 1,
    },
    {
        activeWhen: 1,
        label: 'Upload Second Image',
        id: 2,
    },
    {
        activeWhen: 2,
        label: 'Upload Third Image',
        id: 3,
    },
]