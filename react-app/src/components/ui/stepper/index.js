import React, { useState } from 'react';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import injectSheet from 'react-jss';
import styles from './styles';
import imgIcon from "../customReportsForm/assets/img-icon.png";
import UploadImage from './UploadImage';
import { imageMapping } from './imageMapping';

function getSteps() {
    return ['First Image', 'Second Image', 'Third Image'];
} 

function CustomStepper(props) {
    const { classes, eventDetails } = props;
    const [activeStep, setActiveStep] = useState(0);
    // const [imageFile, setImageFile] = useState("");
    const steps = getSteps();

    const handleNext = () => {
        setActiveStep(prevActiveStep => prevActiveStep + 1);
    }

    const handleBack = () => {
        setActiveStep(prevActiveStep => prevActiveStep - 1);
    }

    return (
        <div className={classes.rootStepper}>
            <Stepper activeStep={activeStep} alternativeLabel>
                {
                    steps.map(label => (
                        <Step 
                            key={label} 
                        >
                            <StepLabel 
                                StepIconProps={{
                                classes: {
                                    active: {background: 'red', color: 'green'},
                                    completed: {background: 'green', color: 'red'},
                                    alternativeLabel: {background: 'green', color: 'red'}
                                }
                            }}
                            >
                            {label}
                            </StepLabel>
                        </Step>
                    ))
                }
            </Stepper>
            { imageMapping.map((image, idx) => activeStep === image.activeWhen && (
                <UploadImage 
                    key={image.id} 
                    id={image.id} 
                    label={image.label}
                    eventId={props.eventId}
                    defaultImage={eventDetails[`image${image.id}`] || null}
                    getRiskWatch={props.getRiskWatch}
                />)
            )}
    
            <div className={classes.btnWrapper}>
                <Button 
                    disabled={activeStep === 0} 
                    variant="outlined" 
                    color="secondary" 
                    onClick={handleBack}
                >
                    Back
                </Button>
                <Button style={{ marginLeft: 10 }} disabled={activeStep === steps.length - 1} variant="outlined" color="secondary" onClick={handleNext}>
                    Next
                </Button>
            </div>
        </div>
    )
}

export default injectSheet(styles)(CustomStepper);

