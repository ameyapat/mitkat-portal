import React, { Component } from 'react';
import './style.scss';

export default class LoadMoreBtn extends Component {
  render() {
    return (
      <div id="loadMoreBtn">
        <button onClick={this.props.parentEventHandler}>Load More...</button>
      </div>
    )
  }
}
