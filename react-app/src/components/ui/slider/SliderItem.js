import React from 'react';
//helper constant
import {
  RISK_LEVEL_COLORS,
  RISK_CATEGORY_ICONS,
} from '../../../helpers/constants';

const SliderItem = ({ item, index, currIndex, parentEventHandler }) => (
  <div
    className={`slider--item ${currIndex === index ? 'active--slide' : ''}`}
    onClick={() =>
      parentEventHandler({
        id: item.id,
        title: item.title,
        risklevel: item.risklevel,
        riskcategory: item.riskcategory,
      })
    }
  >
    <div className="slider__info">
      <span
        className="s--icon"
        style={{ color: RISK_LEVEL_COLORS[item.risklevel] }}
      >
        <i className={RISK_CATEGORY_ICONS[item.riskcategory]}></i>
      </span>
    </div>
    <div className="slider__info">
      <p>{item.title}</p>
    </div>
  </div>
);

export default SliderItem;
