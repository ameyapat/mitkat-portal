import React, { Component } from 'react';
//components
import SliderItem from './SliderItem';
import CustomModal from '../../ui/modal';
import ModalInner from '../../ui/modal/ModalInner';
//scss
import './style.scss';
//helper functions
import { startSlide } from './helpers';
//
import { fetchApi } from '../../../helpers//http/fetch';
import { API_ROUTES } from '../../../helpers/http/apiRoutes';
import { withCookies } from 'react-cookie';

@withCookies
class Slider extends Component{

    state = {
        sliderData: [],
        currIndex: 0,
        showModal: false,
        eventDetail: null
    }

    componentDidMount(){
        let { currIndex, sliderData } = this.state;
        this.setState({sliderData: this.props.sliderData}, () => startSlide(currIndex, sliderData, this.handleNext, this.resetSlide));
    }

    handleOpenEvent = (eventObj) => {
        // console.log(eventObj, ' << event obj');
        //open modal & show event
        this.setState({
            showModal: true, 
            risklevel: eventObj.risklevel,
            riskcategory: eventObj.riskcategory
        });
        this.getEventDetail(eventObj.id);
        //call one more api to fetch event details..
        
    }

    getEventDetail = (eventId) => {

        let { cookies } = this.props;
        let authToken = cookies.get('authToken');

        let reqObj = {
            method: 'POST',
            isAuth: true,
            authToken,
            url: `${API_ROUTES.getEventDetails}/${eventId}`,
            showToggle: true, 
        }

        fetchApi(reqObj)
        .then((data) => {
            this.setState({eventDetail: data});
        })
    }

    getSlides = () => {
        let { currIndex, sliderData } = this.state;
        let SlideXML = [];
    
        SlideXML = sliderData && sliderData.map((item, index) => {
            return <SliderItem 
                key={index} 
                index={index}
                currIndex={currIndex}
                item={item} 
                parentEventHandler={this.handleOpenEvent}
            />
        })

        return SlideXML;
    }

    resetSlide = () => {
        this.setState({currIndex: 0});
    }

    handleNext = () => {
        let { currIndex, sliderData } = this.state;
    
        if(currIndex >= sliderData.length - 1){
            this.resetSlide();
        }else{
            this.setState({currIndex: currIndex + 1});
        }
    }

    handlePrev = () => {
        let { currIndex } = this.state;
        this.setState({currIndex: currIndex - 1});
    }

    handleCloseModal = () => {
        this.setState({showModal: false});
    }

    render(){
        let { currIndex, sliderData, showModal,
            eventDetail, riskcategory, risklevel } = this.state;
    
        return(
            <div id="slider">
                <div className="sliderOuter">
                    <div 
                        className={`slider__inner active--slider--${currIndex}`} 
                        style={{
                            transform: `translate(-${currIndex * (100 / sliderData.length)}%)`}
                        }>
                        { this.getSlides() }
                    </div>
                </div>
                {/* <div className="navs">
                    <span className={`nav--left ${currIndex === 0 ? 'disabled' : ''}`} onClick={currIndex !== 0 ? this.handlePrev : null}><i className="icon-uniE955"></i></span>
                    <span className={`nav--right ${currIndex >= sliderData.length ? 'disabled' : ''}`} onClick={currIndex >= sliderData.length ? null : this.handleNext }><i className="icon-uniE952"></i></span>
                </div>                 */}
                <CustomModal showModal={showModal} closeModal={this.handleCloseModal}>
                    <div className="modal__inner">
                        <ModalInner 
                            {...this.props}
                            eventDetail={eventDetail && eventDetail}
                            risklevel={risklevel} 
                            riskcategory={riskcategory} 
                            closeModal={this.handleCloseModal}
                        />
                    </div>
                </CustomModal>
            </div>
        )
    }
}

export default Slider;