let nextInterval;

export function startSlide(currIndex, sliderData, handleNext){
        nextInterval = setInterval(handleNext, 5000);
}

export function stopSlide(){
        clearInterval(nextInterval);
}