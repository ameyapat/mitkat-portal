export default {
  rootInfoCard: {
    borderRadius: 10,
    width: '31.3%',
    background: 'rebeccapurple',
    // padding: 10,
    margin: 10,
    height: 200,
    display: 'flex',
    flexWrap: 'wrap',
    alignItems: 'flex-end',
    backgroundPosition: 'top center !important',
    backgroundSize: 'cover !important',
    boxShadow: '0 2px 8px 2px rgba(0, 0, 0, 0.3)',
  },
  rootInfoWrap: {
    width: '100%',
    background: `linear-gradient(rgba(0, 0, 0, 0.4) 5%, rgba(52, 73, 94, 1) 100%, transparent)`,
    padding: 6,
    color: 'var(--whiteMediumEmp)',
    'border-bottom-right-radius': 10,
    'border-bottom-left-radius': 10,

    '& h1': {
      textTransform: 'capitalize',
      paddingBottom: 10,
      fontSize: 17,
    },
    '& p': {
      fontSize: 12,
      paddingBottom: 15,
    },
  },
  rootActions: {
    '& button, & a': {
      margin: '0 8px 0 0',
      background: 'transparent',
      border: '1px solid #fff',
      color: 'var(--whiteMediumEmp)',
      borderRadius: 3,
      padding: 5,
      cursor: 'pointer',
      '&:hover': {
        // background: '#f1f1f1',
        transform: 'translate3d(0px, -2px , 0px)',
        // boxShadow: '0px 0px 7px 0 #d3d3d3'
      },
    },

    '& a': {
      fontSize: 13,
      color: '#000',
      '&:hover': {
        transform: 'translate3d(0px, -2px , 0px)',
        // boxShadow: '0px 0px 7px 0 #d3d3d3'
      },
    },
  },
};
