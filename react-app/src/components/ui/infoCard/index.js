import React from 'react';
import injectSheet from 'react-jss';
import styles from './style';
//
import clsx from 'clsx';

const InfoCard = props => {
  let {
    classes,
    date,
    title,
    imgUrl,
    id,
    handleView,
    handleEdit,
    handleDelete,
    handleDownload,
    isClient = false,
  } = props;
  return (
    <div
      className={classes.rootInfoCard}
      style={{
        background: `url(${imgUrl})`,
      }}
    >
      <div className={clsx(classes.rootInfoWrap, classes.rootClientInfo)}>
        <div className={classes.rootInfoTxt}>
          <h1>{title}</h1>
          <p>{date}</p>
        </div>
        <div className={clsx(classes.rootActions, classes.rootBtn)}>
          <button onClick={() => handleView(id)}>View</button>
          <button onClick={() => handleEdit(id)}>Edit</button>
          <button onClick={() => handleDownload(id)}>Download</button>
          {/* <a target='_blank' href={API_ROUTES.downloadAdvisory + '/' + id}>Download</a> */}
          {!isClient && (
            <button onClick={() => handleDelete(id)}>Delete</button>
          )}
        </div>
      </div>
    </div>
  );
};

export default injectSheet(styles)(InfoCard);
