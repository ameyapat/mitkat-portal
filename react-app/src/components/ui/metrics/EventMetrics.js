import React from 'react';
import './style.scss';

const EventMetrics = (props) => (
    <div className="event--metrics">
        <div className="event__metrics">
            <label className="label--value">{props.totalEvents}</label>
            <label className="label--txt">Total Events</label>
        </div>
    </div>
)

export default EventMetrics;