export default {
    rootSubscribeBox:{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        maxWidth: 440,
        borderRadius: 5,
        boxShadow: '0 0 6px 0 #d3d3d3',
        padding: 20,
        background: '#f1f1f1',
        position: 'relative'
    }
}