import React from 'react';
import withStyles from 'react-jss';
import style from './style';
import './style.scss';

const SubscribeBox = ({classes, handleCloseModals}) => (
    <div className={classes.rootSubscribeBox}>
        <span className="close-box" onClick={handleCloseModals}><i>&times;</i></span>
        {/* <div className="iconWrap"><i>&times;</i></div> */}
        <h2 className="titleWrap">Access Restricted to this feature</h2>
        <p className="descWrap">The current subscription for this application does not allow you to access this feature, notify the admin to upgrade.</p>
        {/* <div className="callToAction"><button className="btn btn-notify">Notify Admin</button></div> */}
    </div>
)

export default withStyles(style)(SubscribeBox)