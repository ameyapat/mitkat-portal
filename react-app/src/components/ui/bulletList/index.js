import React from 'react';
import './style.scss';

const BulletList = ({ list, type }) => {
  const isSourceList = type === 'sourceList';
  const Component = isSourceList ? 'a' : 'p';

  return (
    <ul className="bulletList">
      {list.map(l => (
        <li>
          <span className="b-list--icon">
            <i className="fab fa-slack-hash"></i>
          </span>
          <Component {...(isSourceList ? { href: l, target: '_blank' } : {})}>
            {l}
          </Component>
        </li>
      ))}
    </ul>
  );
};

export default BulletList
