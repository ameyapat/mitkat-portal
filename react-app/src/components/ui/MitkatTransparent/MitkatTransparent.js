import React from 'react';
import './MitkatTranparent.scss';
import logo from '../../../assets/mitkat-transparent-bg-svg.png';

const MitkatTransparent = () => (
  <div className="mitkatTransparentLogo__wrap">
    <img src={logo} alt="mitkat-logo-transparent" />
  </div>
);

export default MitkatTransparent;
