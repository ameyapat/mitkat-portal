import React, { Component } from 'react';
import FullCalendar from '@fullcalendar/react';
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin from '@fullcalendar/interaction';
import listPlugin from '@fullcalendar/list';
import './main.scss';

const defaultEvents = [
  { title: 'event 1', date: '2019-04-01' },
  { title: 'event 2', date: '2019-04-01' },
  { title: 'event 3', date: '2019-04-02' },
];

class CustomCalendar extends Component {
  componentDidMount() {
    this.props.getCalendarEvents();
  }

  render() {
    let {
      parentEventHandler,
      defaultView = 'listMonth',
      events = defaultEvents,
    } = this.props;

    return (
      <FullCalendar
        defaultView={defaultView}
        plugins={[dayGridPlugin, interactionPlugin, listPlugin]}
        events={events}
        eventClick={parentEventHandler}
        allDaySlot={true}
        allDayText={''}
      />
    );
  }
}

export default CustomCalendar;
