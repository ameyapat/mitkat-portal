import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import './GlobalSearchBar.scss';

import {
  getEventList,
  getDefaultEventDetails,
} from '../../../selectors/mapState.selectors';
import { searchRiskTrackerEvents } from '../../../helpers/utils';
import { setEventDetails, setDefaultEventDetails } from '../../../actions';

const GlobalSearchBar = ({
  placeholder = 'type to search',
  onClose,
  onApply,
}) => {
  const { eventDetails, defaultEventDetails } = useSelector(state => {
    return {
      eventDetails: getEventList(state),
      defaultEventDetails: getDefaultEventDetails(state),
    };
  });

  const textInput = React.useRef();
  const history = useHistory();
  const dispatch = useDispatch();

  const handleSearch = e => {
    let value = e.target.value;
    if (value) {
      if (history.location.pathname === '/client/dashboard') {
        let filteredEventDetails = searchRiskTrackerEvents(
          value,
          defaultEventDetails,
        );
        dispatch(setEventDetails(filteredEventDetails));
      } else {
        let filteredEventDetails = searchRiskTrackerEvents(
          value,
          defaultEventDetails,
        );
      }
    } else {
      dispatch(setEventDetails(defaultEventDetails));
    }
  };

  const handleOnClose = () => {
    textInput.current.value = '';
    dispatch(setEventDetails(defaultEventDetails));
    onClose();
  };

  return (
    <div id="globalSearchBar">
      <div className="inputWrapper">
        <span className="inputWrapper__icon">
          <i className="fas fa-search-location"></i>
        </span>
        <input
          type="text"
          placeholder={placeholder}
          onChange={handleSearch}
          ref={textInput}
        />
        <span className="inputBtnWrapper">
          <button onClick={handleOnClose}>&times;</button>
        </span>
      </div>
    </div>
  );
};

export default GlobalSearchBar;