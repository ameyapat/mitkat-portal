import React from 'react';
import MapWithMarker from './MapWithMarker';

const CustomGoogleMap = ({
  googleMapURL,
  loadingElement,
  containerElement,
  mapElement,
  list,
  officelocations,
  mapStyles,
  classes,
  customIcon,
  customLocationIcon,
  onPinClick,
}) => {
  return (
    <MapWithMarker
      googleMapURL={googleMapURL}
      loadingElement={loadingElement}
      containerElement={containerElement}
      mapElement={mapElement}
      list={list}
      officelocations={officelocations}
      mapStyles={mapStyles}
      classes={classes}
      customIcon={customIcon}
      customLocationIcon={customLocationIcon}
      onPinClick={onPinClick}
    />
  );
};

export default CustomGoogleMap;
