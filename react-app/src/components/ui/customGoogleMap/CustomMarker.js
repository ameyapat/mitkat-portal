import React, { Component } from 'react';
import { Marker, InfoWindow } from 'react-google-maps';
import { connect } from 'react-redux';
import './style.scss';

@connect(mapStateToProps, mapDispatchToProps)
class CustomMarker extends Component {
  state = {
    showInfoBox: false,
  };

  markerRef = React.createRef();

  setShowInfoBox = show => this.setState({ showInfoBox: show });

  render() {
    const {
      id,
      title = '',
      lat,
      lng,
      customIcon,
      distance = '',
      locationName = '',
      showLocations = true,
      onPinClick,
    } = this.props;

    const { showInfoBox } = this.state;

    return (
      <Marker
        onMouseOver={() => this.setShowInfoBox(true)}
        onMouseOut={() => this.setShowInfoBox(false)}
        position={{ lat, lng }}
        onClick={() =>
          onPinClick ? onPinClick(id) : console.log('Not Allowed!')
        }
        icon={customIcon}
        ref={this.markerRef}
        optimized={false}
      >
        {showInfoBox && (
          <>
            <InfoWindow onCloseClick={() => this.handleShowInfoBox(false)}>
              <>
                <p className="alert--title space-bottom">{title}</p>
                {showLocations && (
                  <>
                    <p>
                      Distance:{' '}
                      {distance
                        ? `${Math.floor(distance)} kms`
                        : 'Not Applicable'}
                    </p>
                    <p>
                      Nearest Client Location:{' '}
                      {locationName ? locationName : '-'}
                    </p>
                  </>
                )}
              </>
            </InfoWindow>
          </>
        )}
      </Marker>
    );
  }
}

const mapStateToProps = state => {
  return {
    riskTracker: state.riskTracker,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch,
  };
};

export default React.forwardRef((props, ref) => (
  <CustomMarker innerRef={ref} {...props} />
));
