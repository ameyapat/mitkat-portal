import React, { Component } from 'react';
import { withScriptjs, withGoogleMap, GoogleMap } from 'react-google-maps';
import CustomMarker from './CustomMarker';
import { connect } from 'react-redux';

const defaultOptions = {
  mapTypeControl: false,
  streetViewControl: false,
  zoomControl: false,
  fullscreenControl: false,
};

@withScriptjs
@withGoogleMap
@connect(state => {
  return {
    barclays: state.barclays,
    barclaysEventDetails: state.barclaysEventDetails,
  };
})
class MapWithMarker extends Component {
  state = {
    latState: -5.5086192,
    lngState: 103.9100664,
  };
  renderMarkers = () => {
    let { list, customIcon, onPinClick } = this.props;

    return list.map(item => {
      return (
        <CustomMarker
          id={item.id}
          key={item.id}
          lat={item.latitude}
          lng={item.longitude}
          title={item.title}
          distance={item.distance}
          locationName={item.locationName}
          customIcon={customIcon[item.riskcategory]}
          showLocations={true}
          onPinClick={onPinClick}
        />
      );
    });
  };

  renderOfficeLocations = () => {
    let { officelocations, customLocationIcon } = this.props;

    return officelocations.map(item => {
      return (
        <CustomMarker
          id={item.id}
          key={item.id}
          lat={item.latitude}
          lng={item.longitude}
          title={item.address}
          customIcon={customLocationIcon}
          showLocations={false}
        />
      );
    });
  };

  setCenter = (lat, lng) => this.setState({ latState: lat, lngState: lng });

  render() {
    let {
      mapStyles,
      list,
      officelocations,
      barclaysEventDetails: { showBarclaysLocation },
    } = this.props;
    let { latState, lngState } = this.state;

    return (
      <GoogleMap
        defaultZoom={3}
        defaultOptions={{ ...defaultOptions, styles: mapStyles }}
        defaultCenter={{
          lat: latState || 16.950209,
          lng: lngState || 93.207995,
        }}
      >
        {list.length > 0 && this.renderMarkers()}
        {officelocations.length > 0 &&
          showBarclaysLocation &&
          this.renderOfficeLocations()}
      </GoogleMap>
    );
  }
}

export default MapWithMarker;
