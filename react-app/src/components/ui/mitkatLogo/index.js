import React from 'react';
import './style.scss';
import logo from '../../../assets/logo1.png';

const MitkatLogo = () => <div className="mitkatLogo__wrap"><img src={logo} alt="mitkat-logo" /></div>

export default MitkatLogo