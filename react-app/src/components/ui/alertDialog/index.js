import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import injectSheet from 'react-jss';
import style from './style.js';

@injectSheet(style)
class AlertDialog extends React.Component {
  render() {
    const { classes, rootPaper, rootText } = this.props;
    let {
      isOpen,
      handleClose,
      title,
      parentEventHandler,
      description,
    } = this.props;
    return (
      <Dialog
        open={isOpen}
        onClose={() => handleClose()}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        classes={{
          paper: { ...classes.rootPaper, ...rootPaper },
        }}
      >
        <DialogTitle
          classes={{
            root: { ...classes.rootText, ...rootText },
          }}
        >
          {title}
        </DialogTitle>
        <DialogContent>
          <DialogContentText
            id="alert-dialog-description"
            classes={{
              root: { ...classes.rootText, ...rootText },
            }}
          >
            {description}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => handleClose()} color="primary">
            No
          </Button>
          <Button onClick={() => parentEventHandler()} color="primary">
            Yes
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default AlertDialog;
