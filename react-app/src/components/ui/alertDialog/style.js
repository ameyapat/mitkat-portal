export default {
  rootPaper: {
    backgroundColor: 'var(--backgroundSolid) !important',
  },
  rootText: {
    color: '#dedede !important',
  },
};
