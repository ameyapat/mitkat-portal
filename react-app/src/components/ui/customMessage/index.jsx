import React from 'react';
import clsx from 'clsx';

const CustomMessage = ({ message, classes, children }) => (
  <div className={clsx(classes, 'customMessage')}>
    {message && <p className={clsx('customMessage__info')}>{message}</p>}
    {children}
  </div>
);

export default CustomMessage;
