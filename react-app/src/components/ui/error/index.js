import React from 'react';
import errorImg from '../../../assets/error-404.png';
import Button from '@material-ui/core/Button';
//
import { Link } from 'react-router-dom';
//
import './style.scss';

const Error = () => <div className="error-404">
        <img src={errorImg} alt="not found" />
        <p className="error-txt">Something went wrong!</p>
        <Link to="/"><Button variant="contained" color="secondary">Go Back</Button></Link>
    </div>

export default Error;