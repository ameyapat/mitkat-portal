const validationRulesCreateUser = [
  {
    field: 'name',
    method: 'isEmpty',
    validWhen: false,
    message: 'Name is required',
  },
  {
    field: 'username',
    method: 'isEmpty',
    validWhen: false,
    message: 'Username is required',
  },
  {
    field: 'password',
    method: 'isEmpty',
    validWhen: false,
    message: 'Password is required',
  },
  {
    field: 'email',
    method: 'isEmpty',
    validWhen: false,
    message: 'Email is required',
  },
  {
    field: 'password',
    method: 'isStrongPassword',
    validWhen: true,
    message: 'Password',
  },
];

export default validationRulesCreateUser;
