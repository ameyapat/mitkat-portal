import React, { Component } from 'react';
//component @material-ui
import TextField from '@material-ui/core/TextField';
import './createUser.scss';
import { connect } from 'react-redux';
import store from '../../../store';
import { toggleToast } from '../../../actions';
import { withCookies } from 'react-cookie';
import FormValidator from '../../../helpers/validation/FormValidator';
import CustomMessage from '../../ui/customMessage';
import { USER_INPUT } from '../../client/helpers/constants';
import validationRulesCreateUser from './validationRules';
import { setNewClientUser } from '../../../requestor/mitkatWeb/requestor';

@withCookies
@connect(state => {
  return {
    appState: state.appState,
  };
})
class CreateClientUser extends Component {
  validator = new FormValidator(validationRulesCreateUser);
  submitted = false;

  state = {
    name: '',
    username: '',
    email: '',
    password: '',
    confirmPassword: '',
    isPasswordMatch: false,
    validation: this.validator.valid(),
  };

  handleChange = (type, value) => {
    this.setState({ [type]: value }, () => {
      if (this.state.confirmPassword === this.state.password) {
        this.setState({ isPasswordMatch: true });
      } else {
        this.setState({ isPasswordMatch: false });
      }
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    let validation = this.validator.validate(this.state);
    this.setState({ validation });
    this.submitted = true;

    if (validation.isValid && this.state.isPasswordMatch) {
      let reqBody = {
        name: this.state.name,
        username: this.state.username,
        email: this.state.email,
        password: this.state.password,
      };
      setNewClientUser(reqBody)
        .then(data => {
          if (data.success) {
            store.dispatch(
              toggleToast({ showToast: true, toastMsg: data.message }),
            );
            this.props.getUserList();
            this.props.handleCloseModal();
          } else {
            store.dispatch(
              toggleToast({ showToast: true, toastMsg: data.message }),
            );
          }
        })
        .catch(e => {
          console.log(e);
          store.dispatch(
            toggleToast({ showToast: true, toastMsg: 'Something went wrong!' }),
          );
          this.props.handleCloseModal();
        });
    }
  };

  handleConfirmPasswordChange = value => {
    this.setState({ confirmPassword: value }, () => {
      if (this.state.confirmPassword === this.state.password) {
        this.setState({ isPasswordMatch: true });
      } else {
        this.setState({ isPasswordMatch: false });
      }
    });
  };

  render() {
    let validation = this.submitted
      ? this.validator.validate(this.state)
      : this.state.validation;
    const { password } = this.state.validation;

    return (
      <div className="createUserWrap">
        <div className="create__user__inner">
          <div className="form__wrap">
            <h2>Enter Credentials</h2>
            <form onSubmit={this.handleSubmit}>
              {USER_INPUT.map(input => (
                <TextField
                  error={validation[input.id].isInvalid}
                  key={input.id}
                  id={input.id}
                  label={input.label}
                  fullWidth
                  type={input.type === 'password' ? 'password' : 'text'}
                  value={this.state[input.type]}
                  onChange={e => this.handleChange(input.type, e.target.value)}
                  margin="normal"
                />
              ))}
              <TextField
                label="Confirm Password"
                fullWidth
                type="password"
                value={this.state.confirmPassword}
                onChange={e => this.handleConfirmPasswordChange(e.target.value)}
                margin="normal"
              />
              {password.isInvalid && (
                <CustomMessage>
                  <p>
                    <span className="customMsg__icon">
                      <i className="fas fa-lock"></i>
                    </span>
                    Your password needs to:
                  </p>
                  <ul>
                    <li>be atleast 8 characters long.</li>
                    <li>include both uppercase &amp; lowercase characters.</li>
                    <li>be atleast one number or symbol.</li>
                  </ul>
                </CustomMessage>
              )}
              {!this.state.isPasswordMatch && (
                <CustomMessage>
                  <p style={{ paddingTop: 10 }}>
                    <span className="customMsg__icon">
                      <i className="fas fa-exclamation"></i>
                    </span>
                    Your passwords Do not Match
                  </p>
                </CustomMessage>
              )}
              <div className="submit-wrap">
                <button
                  className="solidBtn createUserBtn"
                  onClick={this.handleSubmit}
                >
                  Create User
                </button>
                <button
                  className="solidBtn createUserBtn"
                  onClick={this.props.handleCloseModal}
                >
                  Close
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default CreateClientUser;
