import React, { Component } from 'react';
import './style.scss';
import clsx from 'clsx';

class SearchBox extends Component {
  searchRef = React.createRef();

  componentDidMount() {
    this.searchRef.current.focus();
  }

  render() {
    let { classes, placeholder, parentHandler, value } = this.props;
    return (
      <div className={clsx('searchBox', classes ? classes.root : null)}>
        <span className={classes ? classes.iconWrap : null}>
          <i className={`fa fa-search ${classes ? classes.icon : null}`}></i>{' '}
        </span>
        <input
          value={value}
          type="text"
          placeholder={placeholder}
          onChange={e => parentHandler(e)}
          className={classes ? classes.input : null}
          ref={this.searchRef}
        />
      </div>
    );
  }
}

export default SearchBox;
