import React, { Component } from 'react';

export default class TextArea extends Component {
  render() {
    return (
        <TextField
        id="outlined-textarea"
        label="Multiline Placeholder"
        placeholder="Placeholder"
        multiline
        // className={classes.textField}
        margin="normal"
        variant="outlined"
    />
    )
  }
}
