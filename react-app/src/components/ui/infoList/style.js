export default {
  rootInfoCard: {
    borderRadius: 10,
    width: '30%',
    background: 'rebeccapurple',
    // padding: 10,
    margin: 10,
    height: 200,
    display: 'flex',
    flexWrap: 'wrap',
    alignItems: 'flex-end',
    backgroundPosition: 'top center !important',
    backgroundSize: 'cover !important',
    boxShadow: '0 2px 8px 2px rgba(0, 0, 0, 0.3)',
    backgroundSize: 'cover !important',
  },
  rootInfoWrap: {
    width: '100%',
    color: 'var(--whiteMediumEmp)',
    background: 'rgba(20, 33, 54, 0.85)',
    minHeight: 100,
    padding: 5,

    '& h1': {
      textTransform: 'capitalize',
      paddingBottom: 10,
      fontSize: 17,
      fontWeight: 100,
    },
    '& p': {
      fontSize: 12,
      paddingBottom: 15,
      '& a': {
        color: 'var(--darkActive)',
        float: 'right',
        border: '1px solid var(--darkActive)',
        borderRadius: 3,
        padding: 5,
        cursor: 'pointer',
        '&:hover': {
          transform: 'translate3d(0px, -1px , 0px)',
        },
      },
    },
  },
  rootActions: {
    '& button, & a': {
      margin: '0 8px 0 0',
      background: 'transparent',
      border: '1px solid #fff',
      color: 'var(--whiteMediumEmp)',
      borderRadius: 3,
      padding: 5,
      cursor: 'pointer',
      '&:hover': {
        // background: '#f1f1f1',
        transform: 'translate3d(0px, -2px , 0px)',
        // boxShadow: '0px 0px 7px 0 #d3d3d3'
      },
    },

    '& a': {
      fontSize: 13,
      color: '#000',
      '&:hover': {
        transform: 'translate3d(0px, -2px , 0px)',
        // boxShadow: '0px 0px 7px 0 #d3d3d3'
      },
    },
  },
};
