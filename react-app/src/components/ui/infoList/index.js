import React from 'react';
import injectSheet from 'react-jss';
import styles from './style';
//
import clsx from 'clsx';

const InfoList = props => {
  let {
    classes,
    date,
    title,
    id,
    imgUrl,
    document,
    handleDelete,
    isClient = false,
  } = props;
  return (
    <div
      className={classes.rootInfoCard}
      style={{
        background: `url(${imgUrl})`,
      }}
    >
      <div className={clsx(classes.rootInfoWrap)}>
        <div className={classes.rootInfoTxt}>
          <h1>{title}</h1>
          <p>
            {date}
            <a target="_blank" href={document}>
              view
            </a>
          </p>
        </div>
      </div>
    </div>
  );
};

export default injectSheet(styles)(InfoList);
