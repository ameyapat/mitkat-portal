import React, { Component } from 'react';
import Select from 'react-select';
import clsx from 'clsx';
import { withStyles } from '@material-ui/core';

import PropTypes from 'prop-types';

const styles = () => ({});

const customStyles = {
  control: provided => ({
    ...provided,
    backgroundColor: 'var(--backgroundSolid)',
    color: '#ffffff ',
  }),
  option: (provided, state) => ({
    ...provided,
    zIndex: '99999',
  }),
  placeholder: provided => ({
    ...provided,
    color: '#ffffff',
  }),
  singleValue: provided => ({
    ...provided,
    color: '#ffffff',
  }),
};

@withStyles(styles)
class MultiSelect extends Component {
  render() {
    let {
      defaultValue,
      value,
      placeholder,
      parentEventHandler,
      options,
      classes,
      hasCustomStyles = false,
      allowSelectAll = false,
    } = this.props;

    return (
      <Select
        isMulti
        defaultValue={defaultValue}
        className={clsx('select--options', classes.root ? classes.root : null)}
        value={value}
        placeholder={placeholder}
        onChange={parentEventHandler}
        options={options}
        styles={hasCustomStyles ? this.props.customStyles : customStyles}
      />
    );
  }
}

MultiSelect.propTypes = {
  allowSelectAll: PropTypes.bool,
};

export default MultiSelect;
