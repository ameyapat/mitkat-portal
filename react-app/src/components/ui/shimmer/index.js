import React from 'react';
import './style.scss';

const Shimmer = ({style, rows = 4}) => {
    let i = 0;
    let shimmerXml = [];

    while(i <= rows){
        shimmerXml.push(<div style={style} className="shimmer" key={`${i}-shim`}>
            <p className="line"></p>
            <p className="line-2"></p>
        </div>)
        i++;
    }

    return shimmerXml;
};

export default Shimmer;

