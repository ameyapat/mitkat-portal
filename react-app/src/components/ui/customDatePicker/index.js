import React from 'react';
import { DatePicker } from 'material-ui-pickers';
import injectSheet from 'react-jss';
import styles from './style';

const CustomDatePicker = ({
  label,
  value,
  type,
  classes,
  handleDateChange,
  views,
  inputLabelClasses,
  inputPropsClasses,
  format,
  defaultValue,
}) => {
  return (
    <DatePicker
      defaultValue={defaultValue}
      label={label}
      value={value}
      format={format}
      fullWidth
      views={views}
      onChange={value => handleDateChange(type, value)}
      animateYearScrolling
      InputLabelProps={{
        shrink: true,
        classes: inputLabelClasses,
      }}
      InputProps={{
        classes: inputPropsClasses,
        endAdornment: null,
      }}
    />
  );
};

export default injectSheet(styles)(CustomDatePicker);
