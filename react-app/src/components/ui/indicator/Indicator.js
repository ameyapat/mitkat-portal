import React from 'react';
import './Indicator.scss';

const Indicator = ({value}) => <div className="indicator-cont">{value}</div>;

export default Indicator;