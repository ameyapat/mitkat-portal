import React from 'react';
import injectSheet from 'react-jss'

const styles = {
    rootSearchWrapper:{
        '& h1':{
            color: '#333',
            background: '#f1f1f1',
            padding: 10
        }
    }
}

const SearchWrapTable = ({classes, children, title}) => (
    <div className={classes.rootSearchWrapper}>
        <h1>{title}</h1>
        {
            children && children
        }
    </div>
)

export default injectSheet(styles)(SearchWrapTable);