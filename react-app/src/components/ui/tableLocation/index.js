import React, { Component } from 'react';
import TableItems from './TableItems';
import './style.scss';

class ClientLocationTable extends Component {
  render() {
    let {
      tableData,
      showViewDetails,
      handleInputChange,
      getUserList,
    } = this.props;

    return (
      <>
        <table id="user-list-table" className="user--list--inner">
          <thead>
            <tr>
              <th>Sr No</th>
              <th>Location Name</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {tableData &&
              tableData.map((item, index) => {
                return (
                  <TableItems
                    key={item.locationid}
                    id={item.locationid}
                    srNo={index + 1}
                    handleInputChange={handleInputChange}
                    showViewDetails={showViewDetails}
                    title={item.title}
                    getUserList={getUserList}
                  />
                );
              })}
          </tbody>
        </table>
      </>
    );
  }
}

export default ClientLocationTable;
