import React, { Component } from 'react';
import './style.scss';
import Button from '@material-ui/core/Button';
//redux
import { connect } from 'react-redux';
import store from '../../../store';
import { toggleToast } from '../../../actions';
import { withCookies } from 'react-cookie';
import AlertDialog from '../alertDialog';
import { deleteClientLocation } from '../../../requestor/mitkatWeb/requestor';

@withCookies
@connect(state => {
  return {
    authToken: state.appState.authToken,
  };
})
class TableItems extends Component {
  state = {
    showAlert: false,
  };

  handleDeleteClientLocation = id => {
    deleteClientLocation(id)
      .then(data => {
        this.handleCloseAlert();
        if (data.success) {
          store.dispatch(
            toggleToast({ showToast: true, toastMsg: data.message }),
          );
          this.props.getUserList();
        } else {
          store.dispatch(
            toggleToast({ showToast: true, toastMsg: data.message }),
          );
        }
      })
      .catch(e => {
        console.log(e);
        store.dispatch(
          toggleToast({ showToast: true, toastMsg: 'Something went wrong!' }),
        );
      });
  };

  handleShowAlert = eventId => {
    this.setState({ showAlert: true, eventId });
  };

  handleCloseAlert = () => {
    this.setState({ showAlert: false, eventId: '' });
  };

  render() {
    let { id, title, srNo } = this.props;
    let { showAlert } = this.state;

    return (
      <tr>
        <td>{srNo}</td>
        <td>{title} </td>
        {
          <td>
            <Button
              className="deleteLocation"
              onClick={() => this.handleShowAlert(id)}
              variant="outlined"
              title="Delete Location"
            >
              <i className="fas fa-trash" style={{ marginRight: 5 }}></i>
              Delete
            </Button>
          </td>
        }
        {showAlert && (
          <AlertDialog
            isOpen={showAlert}
            title="Delete Location"
            description="You are about to delete the location, are you sure?"
            parentEventHandler={() => this.handleDeleteClientLocation(id)}
            handleClose={() => this.handleCloseAlert()}
          />
        )}
      </tr>
    );
  }
}

export default TableItems;
