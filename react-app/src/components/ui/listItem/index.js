import React, { Component } from 'react';
import './style.scss';

class ListItem extends Component{
    render(){

        let { id, icon, iconColor, title, parentHandler, risklevel, riskcategory } = this.props;

        return(
            <li onClick={() => parentHandler(id, riskcategory, risklevel)}>
                <div className="list__item">
                    <span style={{backgroundColor: iconColor}} className="icon__wrap">
                        <i className={icon}></i>
                    </span>
                    <span className="title__wrap">
                        <p>{title}</p>
                    </span>
                    <span className="btn__wrap">
                        <button>View</button>
                    </span>
                </div>
            </li>
        )
    }
}

export default ListItem;