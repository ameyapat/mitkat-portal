import React from 'react';
import Slider from '@material-ui/core/Slider';
import { withStyles } from '@material-ui/core';
import CustomModal from '../modal';
import CustomLabel from '../customLabel';

const PrettoSlider = withStyles({
  root: {
    color: '#52af77',
    height: 8,
  },
  thumb: {
    height: 24,
    width: 24,
    backgroundColor: '#fff',
    border: '2px solid currentColor',
    marginTop: -8,
    marginLeft: -12,
    '&:focus, &:hover, &$active': {
      boxShadow: 'inherit',
    },
  },
  active: {},
  valueLabel: {
    left: 'calc(-50% + 4px)',
  },
  track: {
    height: 8,
    borderRadius: 4,
  },
  rail: {
    height: 8,
    borderRadius: 4,
  },
})(Slider);

const CustomSlider = ({
  ariaLabel,
  defaultValue = 0,
  value,
  step,
  min,
  max,
  handleOnChange,
  hasLabel = true,
  labelTitle,
  labelClasses,
  isRequired = false,
}) => {
  return (
    <div>
      {hasLabel && (
        <CustomLabel
          title={labelTitle}
          classes={labelClasses}
          isRequired={isRequired}
        />
      )}
      <PrettoSlider
        valueLabelDisplay="auto"
        aria-label={ariaLabel}
        defaultValue={defaultValue}
        value={value}
        step={step}
        min={min}
        max={max}
        onChange={handleOnChange}
      />
    </div>
  );
};

export default CustomSlider;
