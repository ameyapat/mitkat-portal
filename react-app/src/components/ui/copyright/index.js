import React from 'react';
import './style.scss';

const Copyright = () => (<div id="copyright">
    <p>&copy; 2010-{(new Date().getFullYear())} Mitkat Advisory Pvt Ltd. All Rights Reserved</p>
</div>)

export default Copyright;