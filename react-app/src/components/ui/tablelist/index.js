import clsx from 'clsx';
import React, { Component } from 'react';
//style
import './style.scss';

class TableList extends Component {
  state = {
    calcHeight: '100%',
  };

  componentDidMount() {
    const { id = 'rootTableList' } = this.props;
    const tRoot = document.querySelector('#' + id).getBoundingClientRect().top;
    this.setState({ calcHeight: window.innerHeight - tRoot + 'px' });
  }

  render() {
    let { classes, id = 'rootTableList' } = this.props;
    return (
      <div
        id={id}
        className={clsx(
          `tablelist__wrap`,
          classes && classes.root ? classes.root : '',
        )}
        style={{ height: this.state.calcHeight }}
      >
        {this.props.children}
      </div>
    );
  }
}

export default TableList;
