import React from 'react';

const TableListRow = ({ children, classes }) => {
  return (
    <div className={`tablelist__row ${classes && classes.root}`}>
      {children}
    </div>
  );
};

export default TableListRow;
