import React, { Component } from 'react';

class TableListCell extends Component {
  state = {
    sortBy: 'asc',
  };

  handleSort = () => {
    const { type } = this.props;
    let { sortBy } = this.state;
    let sortByTemp = '';

    if (sortBy === 'asc') {
      sortByTemp = 'dsc';
    } else {
      sortByTemp = 'asc';
    }
    this.setState({ sortBy: sortByTemp }, () => {
      this.props.eventHandler(type, this.state.sortBy);
    });
  };

  render() {
    const {
      children,
      value,
      classes,
      renderProp,
      meta,
      styles,
      type,
      customStyles,
    } = this.props;

    return (
      <div
        style={{ ...styles }}
        className={`tablelist__item ${classes ? classes.root : ''}`}
        onClick={() => (type ? this.handleSort() : null)}
      >
        {renderProp ? (
          renderProp(value, meta)
        ) : value === 0 ? (
          <p>0</p>
        ) : (
          <p style={{ ...customStyles }}>{value}</p>
        )}
        {/* { value === 0 ? <p>0</p> : <p>{value}</p> } */}
        {children && children}
      </div>
    );
  }
}

export default TableListCell;
