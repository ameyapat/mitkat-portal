import React, { Component } from 'react';
import './style.scss';

class TimelineList extends Component {
  render() {
    const { dataDate, title, img, description } = this.props;
    return (
      <li class="date" data-date={dataDate}>
        <h3>{title}</h3>
        <img src={img} />
        <p>{description}</p>
      </li>
    );
  }
}

export default TimelineList;
