import React, { Component } from 'react';
import './style.scss';

class VerticleTimeline extends Component {
  render() {
    const { header } = this.props;
    return (
      <div id="verticleTimeline">
        <h1 className="verticleTimeline-header">{header}</h1>
      </div>
    );
  }
}

export default VerticleTimeline;
