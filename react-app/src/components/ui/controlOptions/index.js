import React, { Component } from 'react';
import './style.scss';
import controlIcon from '../../../assets/icons/control.png';
import CustomModal from '../modal';
import RiskAlerts from '../../client/components/riskAlerts';
import ClientProfileOption from '../../client/components/clientProfileOption';
import {Transition} from 'react-spring/renderprops'


class ControlOptions extends Component{

    state = {
        showControlModal: false,
    }

    toggleControlOptions = (show) => {
        this.setState({ showControlModal: show });
    }

    render(){
        const { showControlModal } = this.state;
        return(
            <div id="controlOptions">   
                {/* <div className="control--button">
                    <button onClick={() => this.setState({ showControlModal: true })}>
                        <span>
                            <img src={controlIcon} alt="control-icon" />
                        </span>
                    </button>
                    <span className="control-txt">control</span>
                </div>                 */}
                <CustomModal 
                    showModal={this.state.showControlModal} 
                    closeModal={() => this.setState({ showControlModal: false })}
                >
                    <Transition
                        items={showControlModal}
                        from={{ opacity: 0 }}
                        enter={{ opacity: 1 }}
                        leave={{ opacity: 0 }}
                    >
                        {
                            showControlModal => showControlModal && (props => (
                                <div style={props} className="control--modal">
                                    <button className="control--close-times" onClick={() => this.setState({ showControlModal: false })}>
                                        <span className="close--icon">&times;</span>
                                    </button>
                                    {/* <div> */}
                                        {/* <RiskAlerts toggleControlOptions={this.toggleControlOptions} /> */}
                                    {/* </div> */}
                                    {/* <ClientProfileOption />  */}
                                </div>
                            ))}
                    </Transition>
                </CustomModal>
            </div>
        )
    }
}

export default ControlOptions;