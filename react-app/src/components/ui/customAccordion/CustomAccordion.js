import React, { useState } from 'react';

import './style';
import './style.scss';

import clsx from 'clsx';
import PropTypes from 'prop-types';

const CustomAccordion = ({ children, title, showDefault }) => {
  const [show, setShow] = useState(showDefault);

  return (
    <div className="accordionWrap" onClick={() => setShow(!show)}>
      <div className="accordion__header">
        <h3 className="accordion__title">{title}</h3>
        <span className="accordion__icon">
          <i
            className={clsx({
              'fas fa-chevron-circle-down': show,
              'fas fa-chevron-circle-up': !show,
            })}
          ></i>
        </span>
      </div>
      {show && <div className="accordion__body">{children}</div>}
    </div>
  );
};

CustomAccordion.propTypes = {
  children: PropTypes.func,
  title: PropTypes.string,
  showDefault: PropTypes.bool,
};

export default CustomAccordion;
