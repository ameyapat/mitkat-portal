import React, { Component } from 'react';

import GlobalSearchBar from '../ui/GlobalSearchBar/GlobalSearchBar';

const withSearch = (WrappedComponent) => {
    return class extends Component{
        render(){
            return <>
                <div id="searchWrapper">
                    <GlobalSearchBar />
                </div>
                <WrappedComponent {...this.props} />
            </>
        }
    }
}

export default withSearch;