import React, { Component } from 'react';
import Header from './components/Header';
import KBody from './components/KBody';

import circleImg from '../../../../assets/icons/bubble/crimeBubble.png';

import cyberBubble from '../../../../assets/icons/bubble/cyberBubble.png';
import terrorismBubble from '../../../../assets/icons/bubble/terrorismBubble.png';
import civilBubble from '../../../../assets/icons/bubble/civilBubble.png';
import envBubble from '../../../../assets/icons/bubble/envBubble.png';
import healthBubble from '../../../../assets/icons/bubble/healthBubble.png';
import criticalBubble from '../../../../assets/icons/bubble/criticalBubble.png';
import crimeBubble from '../../../../assets/icons/bubble/crimeBubble.png';
import travelBubble from '../../../../assets/icons/bubble/travelBubble.png';
import naturalBubble from '../../../../assets/icons/bubble/naturalBubble.png';
import externalBubble from '../../../../assets/icons/bubble/externalBubble.png';
import politicalBubble from '../../../../assets/icons/bubble/politicalBubble.png';
import regulatoryBubble from '../../../../assets/icons/bubble/regulatoryBubble.png';

import {
  getQuadrants,
  getRegions,
} from '../../../../requestor/quadrants/requestor';
import { getRiskCategories } from '../../../../api/api';
import {
  updateQuadrants,
  setSelectedEventItem,
  setSelectedQuadrant,
  updateFilterOptions,
} from '../../../../actions/quadrantActions';
import { normalizeByMonthName } from './helpers/utils';
import { connect } from 'react-redux';
import './style.scss';

const RISK_CATEGORY = {
  1: cyberBubble, // 'Cyber / Technology',
  2: terrorismBubble, //'Insurgency / Terrorism',
  3: civilBubble, //'Civil Disturbance',
  4: envBubble, //'Environment'
  5: healthBubble, //'Health',
  6: criticalBubble, //'Critical Infrastructure',
  7: crimeBubble, //'Crime',
  8: travelBubble, //'Travel Risks',
  9: naturalBubble, //'Natural Disasters',
  10: externalBubble, //'External Threats',
  11: politicalBubble, //'Political',
  12: regulatoryBubble, //'Regulatory',
};

class KaleidoscopeClient extends Component {
  state = {
    datasets: [],
    dynamicHeight: 0,
    showSingleEvent: false,
    selectedEventId: null,
  };

  componentDidMount() {
    const { eventId = '' } = this.props.match.params;
    const dynamicHeight = window.innerHeight + 'px';
    this.setState({ dynamicHeight });

    getQuadrants().then(data => {
      normalizeByMonthName(data).then(payload => {
        this.props.dispatch(updateQuadrants(payload));
        this.prepareDataSets();
        this.setInitialEventItem();
        this.setSelectedEvent();
      });
    });

    Promise.all([getRegions(), getRiskCategories()])
      .then(values => {
        this.initFilterOptions(values[0], values[1]);
      })
      .catch(e => {
        console.log(e);
      });

    if (eventId) {
      this.setState({
        showSingleEvent: true,
        selectedEventId: eventId,
      });
    }
  }

  componentDidUpdate(prevProps) {
    const { quadrantState: prevQuadrantState } = prevProps;
    const { quadrantState: currQuadrantState } = this.props;

    if (
      currQuadrantState.selectedQuadrant !==
        prevQuadrantState.selectedQuadrant ||
      currQuadrantState.selectedEventItem.eventid !==
        prevQuadrantState.selectedEventItem.eventid
    ) {
      this.prepareDataSets();
    }

    if (
      currQuadrantState.kfilters.selectedRegion.id !==
      prevQuadrantState.kfilters.selectedRegion.id
    ) {
      this.prepareDataSets();
    }
  }

  initFilterOptions = (regions, categories) => {
    const { kfilters } = this.props.quadrantState;
    const regionIds = [];
    const categoryIds = [];

    regions = regions.map(region => {
      regionIds.push(region.id);
      return { ...region, isChecked: true };
    });

    categories = categories.map(category => {
      categoryIds.push(category.value);
      return { ...category, isChecked: true };
    });

    kfilters['regions'] = regions;
    kfilters['selectedRegionIds'] = regionIds;
    kfilters['categories'] = categories;
    kfilters['selectedCategoryIds'] = categoryIds;

    this.props.dispatch(updateFilterOptions(kfilters));
  };

  setSelectedPeriod = value => {
    this.props.dispatch(setSelectedQuadrant(value));

    const {
      selectedEventItem: { eventid },
      quadrants,
    } = this.props.quadrantState;

    if (eventid) {
      const updatedEventItem = quadrants[value].filter(
        item => item.eventid === eventid,
      );

      this.updateSelectedEventItem(updatedEventItem[0]);
    }
  };

  setSelectedEvent = () => {
    const { quadrants, selectedQuadrant } = this.props.quadrantState;
    const { selectedEventId } = this.state;

    if (selectedEventId) {
      const updatedEventItem = quadrants[selectedQuadrant].filter(
        item => item.eventid === parseInt(selectedEventId),
      );

      this.updateSelectedEventItem(updatedEventItem[0]);
    }
  };

  setInitialEventItem = () => {
    const { quadrants, selectedQuadrant } = this.props.quadrantState;
    const selectedItem = {
      selected: true,
      ...quadrants[selectedQuadrant][0],
    };
    this.props.dispatch(setSelectedEventItem(selectedItem));
    this.prepareDataSets();
  };

  updateSelectedEventItem = selectedItem => {
    const updatedSelectedItem = {
      selected: true,
      ...selectedItem,
    };
    this.props.dispatch(setSelectedEventItem(updatedSelectedItem));
  };

  prepareDataSets = () => {
    const {
      quadrants,
      selectedQuadrant,
      kfilters: {
        selectedRegion: { id: regionId },
      },
    } = this.props.quadrantState;
    const { showSingleEvent, selectedEventId } = this.state;
    const updatedDataSets = [];

    selectedQuadrant &&
      quadrants &&
      quadrants[selectedQuadrant].map((item, idx) => {
        // @filters according to region ID & eventId
        const parseByEvents = showSingleEvent
          ? parseInt(item.eventid) === parseInt(selectedEventId)
          : parseInt(item.redionIds) === parseInt(regionId);

        if (parseByEvents) {
          var customCircleImg = new Image(
            item.regionScale * 25,
            item.regionScale * 25,
          );
          // @dynamic bubble images
          customCircleImg.src = RISK_CATEGORY[item.riskCategoryid] || circleImg;

          updatedDataSets.push({
            label: item.evantName,
            pointStyle: [customCircleImg],
            data: [
              {
                x: item.impact,
                y: item.likelihood,
                r: item.regionScale * 25,
              },
            ],
            backgroundColor: '#757F9A',
            hoverBackgroundColor: '#757F9A',
          });
        }
      });
    this.setState({
      datasets: updatedDataSets,
    });
  };

  render() {
    const {
      quadrants,
      selectedQuadrant,
      selectedEventItem,
      kfilters: {
        selectedRegion: { region_name },
      },
    } = this.props.quadrantState;
    const { datasets, showSingleEvent } = this.state;
    const { isKEvent } = this.props;
    return (
      <div
        id="kaleidoscopeClient"
        style={{
          height: this.state.dynamicHeight,
        }}
      >
        <h1 className="rootHeader">
          MitKat Risk Kaleidoscope
          {isKEvent && (
            <button
              className="rootBackBtn"
              onClick={() =>
                this.props.history.push('/client/dashboard/kaleidoscope-client')
              }
            >
              <i className="fas fa-arrow-left"></i>
            </button>
          )}
        </h1>
        {/* <Header
          title="MitKat Risk Kaleidoscope"
          isKEvent={this.props.isKEvent}
        /> */}
        <KBody
          graphDataSets={datasets}
          quadrants={quadrants}
          selectedQuadrant={selectedQuadrant}
          selectedEventItem={selectedEventItem}
          updateSelectedEventItem={this.updateSelectedEventItem}
          setSelectedPeriod={this.setSelectedPeriod}
          isKEvent={this.props.isKEvent}
          prepareDataSets={() => {}}
          showSingleEvent={true}
          eventTitle={
            showSingleEvent
              ? selectedEventItem.evantName
              : `Top risks for ${region_name} region`
          }
        />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return { quadrantState: state.quadrantState };
};

const mapDispatchToProps = dispatch => {
  return { dispatch };
};

export default connect(mapStateToProps, mapDispatchToProps)(KaleidoscopeClient);
