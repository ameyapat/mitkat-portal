export const normalizeByMonthName = data => {
  return new Promise((resolve, reject) => {
    try {
      const normalizedData = normalize(data);
      resolve(normalizedData);
    } catch (e) {
      const emptyData = [];
      reject(emptyData);
    }
  });
};

const normalize = data => {
  return data.reduce((acc, item) => {
    return { ...acc, [item['monthName']]: item.coordinateData };
  }, {});
};
