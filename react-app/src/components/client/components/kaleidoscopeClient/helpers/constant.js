export const RISK_CATEGORY_COLORS = {
  1: '#C47482',
  2: '#B8E0F6',
  3: '#A15D98',
  4: '#76CDCD',
  5: '#E4CEE0',
  6: '#F7CE76',
  7: '#7F9320',
  8: '#DFAE9A',
  9: '#98D4BB',
  10: '#C54B6C',
  11: '#84A6D6',
  12: '#218B82',
};
