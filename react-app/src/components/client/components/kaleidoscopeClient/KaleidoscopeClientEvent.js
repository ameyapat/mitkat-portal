import React from 'react';
import KaleidoscopeClient from './KaleidoscopeClient';

const KaleidoscopeClientEvent = props => (
  <KaleidoscopeClient {...props} isKEvent={true} />
);

export default KaleidoscopeClientEvent;
