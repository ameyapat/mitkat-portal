import React, { Component } from 'react';
import './KGraph.scss';
import { Bubble } from 'react-chartjs-2';
import { Chart } from 'chart.js';
import clsx from 'clsx';

Chart.pluginService.register({
  beforeRender: function(chart) {
    if (chart.config.options.showAllTooltips) {
      // create an array of tooltips
      // we can't use the chart tooltip because there is only one tooltip per chart
      chart.pluginTooltips = [];
      chart.config.data.datasets.forEach(function(dataset, i) {
        chart.getDatasetMeta(i).data.forEach(function(sector, j) {
          chart.pluginTooltips.push(
            new Chart.Tooltip(
              {
                _chart: chart.chart,
                _chartInstance: chart,
                _data: chart.data,
                _options: chart.options.tooltips,
                _active: [sector],
              },
              chart,
            ),
          );
        });
      });

      // turn off normal tooltips
      chart.options.tooltips.enabled = false;
    }
  },
  afterDraw: function(chart, easing) {
    if (chart.config.options.showAllTooltips) {
      // we don't want the permanent tooltips to animate, so don't do anything till the animation runs atleast once
      if (!chart.allTooltipsOnce) {
        if (easing !== 1) return;
        chart.allTooltipsOnce = true;
      }

      // turn on tooltips
      chart.options.tooltips.enabled = true;
      Chart.helpers.each(chart.pluginTooltips, function(tooltip) {
        tooltip.initialize();
        tooltip.update();
        // we don't actually need this since we are not animating tooltips
        tooltip.pivot();
        tooltip.transition(easing).draw();
      });
      chart.options.tooltips.enabled = false;
    }
  },
});

class KGraph extends Component {
  state = {
    dynamicGHeight: 0,
  };

  componentDidMount() {
    const kGraph = document.querySelector('#kGraph');
    if (kGraph) {
      const gTopPos = kGraph.getBoundingClientRect().y;
      const wHeight = window.innerHeight;
      const headerHeight = this.props.isKEvent ? 180 : 120;
      const dynamicGHeight = wHeight - gTopPos - headerHeight;

      this.setState({
        dynamicGHeight,
      });
    }
  }

  render() {
    const { graphDataSets, isKEvent } = this.props;
    const { dynamicGHeight } = this.state;
    return (
      <div id="kGraph">
        <div
          className={clsx({
            kGraph__body: true,
            screen__one: !isKEvent,
            screen__two: isKEvent,
          })}
        >
          {graphDataSets.length > 0 && dynamicGHeight && (
            <Bubble
              data={{ datasets: graphDataSets }}
              // height={isKEvent ? 280 : 360}
              options={{
                tooltips: {
                  enabled: true,
                  xAlign: 'right',
                  mode: 'single',
                  callbacks: {
                    label: function(tooltipItems, data) {
                      return data.datasets[tooltipItems.datasetIndex].label;
                    },
                  },
                },
                maintainAspectRatio: true,
                showAllTooltips: true,
                legend: {
                  display: false,
                },
                plugins: {},
                scales: {
                  xAxes: [
                    {
                      gridLines: {
                        display: true,
                      },
                      ticks: {
                        suggestedMin: 0,
                        suggestedMax: 5,
                      },
                      scaleLabel: {
                        display: true,
                        labelString: 'Impact',
                      },
                    },
                  ],
                  yAxes: [
                    {
                      gridLines: {
                        display: true,
                      },
                      ticks: {
                        suggestedMin: 0,
                        suggestedMax: 5,
                      },
                      scaleLabel: {
                        display: true,
                        labelString: 'Likelihood',
                      },
                    },
                  ],
                },
              }}
            />
          )}
        </div>
      </div>
    );
  }
}

export default KGraph;
