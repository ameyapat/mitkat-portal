import React, { useState } from 'react';
import './KMetrics.scss';
import KMetricsItem from './KMetricsItem';
import CustomModal from '../../../../../ui/modal';
import ModalWithPortal from '../../../../../ui/modalWithPortal/ModalWithPortal';

const KMetrics = ({ selectedEventItem }) => {
  const [showModal, setShowModal] = useState(false);
  const [eventDetail, setEventDetail] = useState({});

  return (
    <div
      id="kMetrics"
      style={{
        maxHeight: `calc(100vh - 100px)`,
        minHeight: `calc(100vh - 100px)`,
      }}
    >
      <KMetricsItem
        title={'Forecast'}
        info={selectedEventItem.forecast}
        setEventDetail={setEventDetail}
        // setShowModal={setShowModal}
      />
      <KMetricsItem
        title={'Development'}
        info={selectedEventItem.development}
        setEventDetail={setEventDetail}
        // setShowModal={setShowModal}
      />
      <KMetricsItem
        title={'Interplay'}
        info={selectedEventItem.interplay}
        setEventDetail={setEventDetail}
        // setShowModal={setShowModal}
      />
      {/* <ModalWithPortal>
        <CustomModal showModal={showModal}>
          <div className="modal__inner modal--kMetrics">
            <div className="modal--kMetrics__header">
              <h1 className="kMetricsItem__title">{eventDetail.title}</h1>
              <span
                className="modal--kMetrics__close"
                onClick={() => setShowModal(false)}
              >
                &times;
              </span>
            </div>
            <div className="modal--kMetrics__body">
              <p className="kMetricsItem__info">{eventDetail.info}</p>
            </div>
          </div>
        </CustomModal>
      </ModalWithPortal> */}
    </div>
  );
};

export default KMetrics;
