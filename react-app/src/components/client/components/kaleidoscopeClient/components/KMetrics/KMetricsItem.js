import React, { useEffect, useState } from 'react';
import './KMetrics.scss';

const KMetricsItem = ({ title, info = '', setEventDetail, setShowModal }) => {
  const [itemHeight, setItemHeight] = useState(0);
  const kMetrics = document.querySelector('#kMetrics');
  const limit = 500;

  useEffect(() => {
    if (kMetrics) {
      let height = kMetrics.getBoundingClientRect().height;
      height = height / 3;

      setItemHeight(height);
    }
  });

  const handleReadMore = () => {
    setShowModal(true);
    setEventDetail({ title, info });
  };

  return (
    <div
      className="kMetricsItem"
      style={{
        minHeight: itemHeight + 'px',
        maxHeight: itemHeight + 'px',
        overflow: 'hidden',
      }}
    >
      <h1 className="kMetricsItem__title">{title}</h1>
      <p
        className="kMetricsItem__info"
        style={{
          minHeight: itemHeight - 15 + 'px',
          maxHeight: itemHeight - 15 + 'px',
        }}
      >
        {' '}
        {info}
        {/* {info.substr(0, limit)}{' '}
        {info.length > limit && (
          <span className="limit--dots">
            ...
            <br />
            <button
              type="button"
              onClick={handleReadMore}
              className="readmore--btn"
            >
              Read more
            </button>
          </span>
        )} */}
      </p>
    </div>
  );
};

export default KMetricsItem;
