import React from 'react';
import './KEventList.scss';
import KEventListItem from './KEventListItem';
import KOptions from '../KOptions';
import { useSelector } from 'react-redux';

const KEventList = ({
  quadrants,
  selectedQuadrant,
  selectedEventItem,
  updateSelectedEventItem,
  prepareDataSets,
}) => {
  const quadrantSelector = state => {
    const { kfilters } = state.quadrantState;
    return kfilters;
  };

  const kfilters = useSelector(quadrantSelector);

  return (
    <div id="kEventList">
      {/* <div className="kEventList__optionsWrapper">
        <div className="filterLabel">
          <label>Filter By:</label>
        </div>
        <KOptions
          toggleInfo={false}
          toggleRegion={true}
          toggleCategory={false}
          prepareDataSets={prepareDataSets}
        />
      </div> */}
      {quadrants &&
        quadrants[selectedQuadrant] &&
        quadrants[selectedQuadrant].length > 0 &&
        quadrants[selectedQuadrant]
          .filter(
            item =>
              kfilters['selectedCategoryIds'].includes(item.riskCategoryid) &&
              parseInt(kfilters['selectedRegion'].id) === item.redionIds,
          )
          .map(item => {
            return (
              <KEventListItem
                item={item}
                selectedEventItem={selectedEventItem}
                updateSelectedEventItem={updateSelectedEventItem}
              />
            );
          })}
    </div>
  );
};

export default KEventList;
