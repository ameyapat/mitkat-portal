import React from 'react';
import { NavLink, useRouteMatch } from 'react-router-dom';
import './KEventList.scss';

import { RISK_CATEGORY_NAME } from '../../../../../../helpers/constants';
import { RISK_CATEGORY_COLORS } from '../../helpers/constant';

const KEventListItem = ({
  item,
  selectedEventItem,
  updateSelectedEventItem,
}) => {
  const { url } = useRouteMatch();

  return (
    <NavLink
      to={`${url}/${item.eventid}`}
      // target="_blank"
      onClick={() => updateSelectedEventItem(item)}
    >
      <div
        className={`kEventListItem ${item.eventid ===
          selectedEventItem.eventid && 'selected'}`}
        onClick={() => updateSelectedEventItem(item)}
      >
        <span className="iconWrapper">
          <i className="fab fa-periscope"></i>{' '}
        </span>
        <div className="kEventListItem__meta">
          <p className="eventName">{item.evantName}</p>
          <span className="riskCategoryLabelWrap">
            <span
              className="riskCategoryLabel"
              style={{
                color: RISK_CATEGORY_COLORS[item.riskCategoryid],
                fontWeight: 500,
                fontSize: 'var(--font-size-span)',
              }}
            >
              {RISK_CATEGORY_NAME[item.riskCategoryid]}
            </span>
          </span>
        </div>
      </div>
    </NavLink>
  );
};

export default KEventListItem;
