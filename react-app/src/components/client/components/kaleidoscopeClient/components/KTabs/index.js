import React, { useEffect, useState } from 'react';
import './KTabs.scss';

import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';

import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControl from '@material-ui/core/FormControl';

import { connect, useSelector, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';

import {
  updateFilterOptions,
  updateRegion,
} from '../../../../../../actions/quadrantActions';

import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import styles from './styles';
import { AppBar, Tab, Tabs } from '@material-ui/core';

const KTabs = ({ type, classes, prepareDataSets }) => {
  useEffect(() => {
    setSelectedRegionId(kfilters.selectedRegion.id);
  }, []);

  const quadrantSelector = state => {
    const { kfilters } = state.quadrantState;
    return kfilters;
  };
  const [selectedRegionId, setSelectedRegionId] = useState();
  const dispatch = useDispatch();
  const kfilters = useSelector(quadrantSelector);

  const updateSelectedRegion = (e, id, regionName) => {
    const selectedRegion = {
      id: id,
      region_name: regionName,
    };
    setSelectedRegionId(id);
    dispatch(updateRegion(selectedRegion));
    prepareDataSets();
  };

  return (
    <>
      <AppBar
        position="static"
        color="default"
        classes={{ root: classes.rootAppBar }}
      >
        <Tabs
          classes={{ indicator: classes.indicator, root: classes.rootTabs }}
          value={selectedRegionId - 1}
          centered
        >
          {kfilters[type].map(item => {
            return (
              <Tab
                key={item.id}
                classes={{
                  root: classes.rootTab,
                  selected: classes.selectedTab,
                }}
                label={item.region_name}
                onClick={e =>
                  updateSelectedRegion(e, item.id, item.region_name)
                }
              />
            );
          })}
        </Tabs>
      </AppBar>
    </>
  );
};

KTabs.propTypes = {
  prepareDataSets: PropTypes.func,
};

export default injectSheet(styles)(KTabs);
