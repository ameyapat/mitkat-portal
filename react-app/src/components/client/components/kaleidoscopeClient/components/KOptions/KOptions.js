import React, { useState } from 'react';
import './KOptions.scss';
import Tooltip from '@material-ui/core/Tooltip';

import CustomModal from '../../../../../ui/modal';
import ModalWithPortal from '../../../../../ui/modalWithPortal/ModalWithPortal';

import KFilters from './KFilters';

import PropTypes from 'prop-types';
import clsx from 'clsx';

const slideOne =
  'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/impact.PNG';
const slideTwo =
  'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/likelihood.PNG';
const slideThree =
  'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/overallrisk.PNG';

const KOptions = props => {
  const {
    toggleInfo = true,
    toggleCategory = false,
    toggleRegion = true,
    prepareDataSets,
    showEventTitle = false,
    eventTitle = '',
  } = props;
  const [showCategory, setShowCategory] = useState(false);
  const [showRegion, setShowRegion] = useState(false);
  const [showInfo, setShowInfo] = useState(false);
  const [selectedSlide, setSelectedSlide] = useState(slideOne);

  return (
    <div
      id="KOptions"
      className={clsx({ selectedEventWrapper: showEventTitle })}
    >
      {showEventTitle && (
        <span className="selectedEvent">
          <h3>{eventTitle}</h3>
        </span>
      )}
      <div className="KOptionsWrapper">
        {toggleInfo && (
          <span className="infoBtnWrapper">
            <Tooltip title="More Information">
              <button className="infoBtn" onClick={() => setShowInfo(true)}>
                More Info
                <i className="fas fa-info"></i>
              </button>
            </Tooltip>
          </span>
        )}
        {toggleCategory && (
          <span className="infoBtnWrapper">
            <Tooltip title="Filterby Category">
              <button
                className="filterBtn"
                onClick={() => setShowCategory(true)}
              >
                Categories
              </button>
            </Tooltip>
          </span>
        )}
        {toggleRegion && (
          <span className="infoBtnWrapper">
            <Tooltip title="Filterby Region">
              <button className="filterBtn" onClick={() => setShowRegion(true)}>
                Regions
              </button>
            </Tooltip>
          </span>
        )}
      </div>
      <ModalWithPortal>
        <CustomModal
          showModal={showInfo}
          showCloseOption={true}
          closeModal={() => setShowInfo(false)}
          classes={{ closeTimes: 'KCloseTimes' }}
        >
          <div className="modal__inner sliderMainWrapper">
            <div className="sliderMain">
              <img src={selectedSlide} alt="slide--main" />
            </div>
          </div>
          <div className="miniSlider">
            <div
              className="miniSlider__slide"
              onClick={() => setSelectedSlide(slideOne)}
            >
              <img src={slideOne} alt="impact" />
            </div>
            <div
              className="miniSlider__slide"
              onClick={() => setSelectedSlide(slideTwo)}
            >
              <img src={slideTwo} alt="likelihood" />
            </div>
            <div
              className="miniSlider__slide"
              onClick={() => setSelectedSlide(slideThree)}
            >
              <img src={slideThree} alt="overallrisk" />
            </div>
          </div>
        </CustomModal>
      </ModalWithPortal>

      <ModalWithPortal>
        <CustomModal showModal={showCategory}>
          <div className="modal__inner koptions__filters">
            <KFilters
              type={'categories'}
              handleCloseModal={() => setShowCategory(false)}
              filterType="checkbox"
              prepareDataSets={prepareDataSets}
            />
          </div>
        </CustomModal>
      </ModalWithPortal>

      <ModalWithPortal>
        <CustomModal showModal={showRegion}>
          <div className="modal__inner koptions__filters">
            <KFilters
              type={'regions'}
              handleCloseModal={() => setShowRegion(false)}
              filterType="radio"
              prepareDataSets={prepareDataSets}
            />
          </div>
        </CustomModal>
      </ModalWithPortal>
    </div>
  );
};

KOptions.propTypes = {
  toggleInfo: PropTypes.bool,
  toggleCategory: PropTypes.bool,
  toggleRegion: PropTypes.bool,
  prepareDataSets: PropTypes.func,
};

export default KOptions;
