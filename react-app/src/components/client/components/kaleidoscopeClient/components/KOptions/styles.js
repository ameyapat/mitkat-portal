export default {
  customRootCheckbox: {
    padding: 0,
    marginLeft: 10,
    color: '#a9bf42 !important',
  },
  labelEvent: {
    color: 'rgba(255,255,255, 0.67)',
    fontSize: '0.70em',
    paddingRight: 5,
    marginLeft: 5,
  },
  labelRoot: {
    paddingBottom: 10,
  },
  radioLabel: {
    color: 'var(--whiteMediumEmp)',
    paddingLeft: 10,
  },
  rootHeader: {
    padding: '13px 30px',
    color: 'var(--lightText)',
    background: 'var(--darkSolid)',
  },
  rootTabs: {
    '& .MuiTabs-scroller': {
      display: 'flex',
      justifyContent: 'center',
    },
  },
  indicator: {
    display: 'none',
  },
  rootTab: {
    color: 'var(--whiteMediumEmp)',
    opacity: 1,
    textAlign: 'left',
    fontSize: '1em !important',
    minHeight: '35px !important',
  },
  rootAppBar: {
    background: 'var(--darkGrey)',
    boxShadow: 'none',
    flex: '25%',
  },
  selectedTab: {
    borderBottom: '3px solid var(--darkActive)',
  },
};
