import React, { useEffect, useState } from 'react';
import './KFilters.scss';

import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';

import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControl from '@material-ui/core/FormControl';

import { connect, useSelector, useDispatch } from 'react-redux';
import injectSheet from 'react-jss';

import {
  updateFilterOptions,
  updateRegion,
} from '../../../../../../actions/quadrantActions';

import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import styles from './styles';

const useStyles = makeStyles({
  root: {
    '&:hover': {
      backgroundColor: 'transparent',
    },
  },
  icon: {
    borderRadius: '50%',
    width: 16,
    height: 16,
    boxShadow:
      'inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)',
    backgroundColor: '#f5f8fa',
    backgroundImage:
      'linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))',
    '$root.Mui-focusVisible &': {
      outline: '2px auto rgba(19,124,189,.6)',
      outlineOffset: 2,
    },
    'input:hover ~ &': {
      backgroundColor: '#ebf1f5',
    },
    'input:disabled ~ &': {
      boxShadow: 'none',
      background: 'rgba(206,217,224,.5)',
    },
  },
  checkedIcon: {
    backgroundColor: '#a9bf42',
    backgroundImage:
      'linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))',
    '&:before': {
      display: 'block',
      width: 16,
      height: 16,
      backgroundImage: 'radial-gradient(#fff,#fff 28%,transparent 32%)',
      content: '""',
    },
    'input:hover ~ &': {
      backgroundColor: '#a9bf42',
    },
  },
});

function StyledRadio(props) {
  const classes = useStyles();

  return (
    <Radio
      className={classes.root}
      disableRipple
      color="default"
      checkedIcon={<span className={clsx(classes.icon, classes.checkedIcon)} />}
      icon={<span className={classes.icon} />}
      {...props}
    />
  );
}

const KFilters = ({
  type,
  classes,
  handleCloseModal,
  filterType,
  prepareDataSets,
}) => {
  const quadrantSelector = state => {
    const { kfilters } = state.quadrantState;
    return kfilters;
  };
  const dispatch = useDispatch();
  const kfilters = useSelector(quadrantSelector);

  const handleIsChecked = (id, isChecked, type) => {
    const category = kfilters[type].find(item => item.id === id);
    category.isChecked = isChecked;

    if (!isChecked) {
      if (type === 'categories') {
        const catIndex = kfilters['selectedCategoryIds'].indexOf(id);
        kfilters['selectedCategoryIds'].splice(catIndex, 1);
      } else {
        const catIndex = kfilters['selectedRegionIds'].indexOf(id);
        kfilters['selectedRegionIds'].splice(catIndex, 1);
      }
    } else {
      if (
        type === 'categories' &&
        !kfilters['selectedCategoryIds'].includes(id)
      ) {
        kfilters['selectedCategoryIds'].push(id);
      } else if (
        type === 'regions' &&
        !kfilters['selectedRegionIds'].includes(id)
      ) {
        kfilters['selectedRegionIds'].push(id);
      }
    }

    dispatch(updateFilterOptions(kfilters));
  };

  const updateSelectedRegion = e => {
    const selectedRegion = {
      id: e.target.value,
      region_name: e.target.name,
    };
    dispatch(updateRegion(selectedRegion));
    prepareDataSets();
  };

  return (
    <div className="kfilters__wrapper">
      <div className="kfilters__header">
        <h3 className="filter__title">Filter by {type}</h3>
        <span className="filter__close" onClick={handleCloseModal}>
          &times;
        </span>
      </div>
      {filterType === 'checkbox' &&
        kfilters[type].map(item => {
          return (
            <FormControlLabel
              classes={{
                label: classes.labelEvent,
                root: classes.labelRoot,
              }}
              label={
                type === 'categories' ? item.riskcategory : item.region_name
              }
              control={
                <Checkbox
                  checked={item.isChecked}
                  onChange={() =>
                    handleIsChecked(item.id, !item.isChecked, type)
                  }
                  value={item.id.toString()}
                  classes={{
                    root: classes.customRootCheckbox,
                  }}
                  icon={
                    <CheckBoxOutlineBlankIcon className={classes.sizeIcon} />
                  }
                  checkedIcon={<CheckBoxIcon className={classes.sizeIcon} />}
                />
              }
            />
          );
        })}
      {filterType === 'radio' && (
        <RadioGroup
          aria-label="regions"
          name="kregions"
          value={parseInt(kfilters.selectedRegion.id)}
          onChange={e => updateSelectedRegion(e)}
          defaultValue={kfilters.selectedRegion.id}
        >
          {kfilters[type].map(item => {
            return (
              <>
                <FormControlLabel
                  value={item.id}
                  control={<StyledRadio name={item.region_name} />}
                  label={item.region_name}
                  className={classes.radioLabel}
                />
              </>
            );
          })}
        </RadioGroup>
      )}
    </div>
  );
};

export default injectSheet(styles)(KFilters);
