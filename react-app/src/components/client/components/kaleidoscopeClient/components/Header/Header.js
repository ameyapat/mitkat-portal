import React, { Component } from 'react';
import './Header.scss';
import { withCookies } from 'react-cookie';
import { withRouter } from 'react-router-dom';

import Tooltip from '@material-ui/core/Tooltip';
import { connect } from 'react-redux';

@withRouter
@withCookies
@connect(state => {
  return {
    partnerDetails: state.partnerDetails.partnerDetails,
  };
})
class Header extends Component {
  goTo = path => {
    this.props.history.push(path);
  };

  render() {
    const { showClientLogo = false, clientLogo, title, isKEvent } = this.props;
    let { whiteLogo, partnerName } = this.props.partnerDetails;
    return (
      <div id="headerKClient">
        <div className="leftGrid">
          <div className="logo__wrap">
            <img src={whiteLogo} alt={partnerName} />
          </div>
        </div>
        <div className="centerGrid">
          <h1 className="kClient-title">{title}</h1>
        </div>
        <div className="rightGrid">
          {!isKEvent && (
            <Tooltip title="Main Dashboard">
              <button
                className="btnAvatar"
                onClick={() => this.goTo('/client/dashboard')}
              >
                <i className="fas fa-home"></i>
              </button>
            </Tooltip>
          )}
          {isKEvent && (
            <Tooltip title="Kaleidoscope Dashboard">
              <button
                className="btnAvatar"
                onClick={() => this.goTo('/kaleidoscope-client')}
              >
                <i className="fas fa-arrow-circle-left"></i>
              </button>
            </Tooltip>
          )}
        </div>
      </div>
    );
  }
}

export default Header;
