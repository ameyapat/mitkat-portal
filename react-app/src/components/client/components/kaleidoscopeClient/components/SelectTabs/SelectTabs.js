import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import Timeline from './Timeline';

const useStyles = makeStyles({
  root: {
    flexGrow: 1,
    background: 'transparent',
  },
  rootTabs: {
    background: '#f1f1f1',
    borderRadius: 25,
    boxShadow: 'inset 1px 1px 5px #3d4a5f',
    minHeight: 30,
  },
  rootTab: {
    background: '#fff',
    border: '1px solid #3d4a5f',
    margin: '6px 15px',
    minHeight: 20,
    borderRadius: 5,
    padding: 0,
    color: '#3d4a5f',
  },
  selectedTab: {
    background: '#3d4a5f',
    color: 'var(--whiteHighEmp) !important ',
  },
  indicator: {
    background: 'transparent',
  },
});

export default function SelectTabs({
  setSelectedPeriod,
  quadrants,
  selectedQuadrant,
}) {
  const classes = useStyles();
  const [value, setValue] = React.useState('');

  const handleChange = newValue => {
    setValue(newValue);
    setSelectedPeriod(newValue);
  };

  const setInitTab = () => {
    const tabs = [];
    if (quadrants) {
      for (let tab in quadrants) {
        if (quadrants[tab].length > 0) {
          tabs.push(tab);
        }
      }
    }
    if (tabs.length > 0) {
      setValue(tabs[tabs.length - 1]);
      setSelectedPeriod(tabs[tabs.length - 1]);
    }
  };

  useEffect(() => {
    if (quadrants) {
      setInitTab();
    }
  }, [quadrants]);

  return (
    <Timeline
      selectedQuadrant={selectedQuadrant}
      quadrants={quadrants}
      handleChange={handleChange}
    />
  );
}
