import React from 'react';
import './Timeline.scss';

const Timeline = ({ quadrants, handleChange, selectedQuadrant }) => {
  return (
    <div className="timeline-wrapper">
      {quadrants &&
        Object.keys(quadrants).map(tab => {
          if (quadrants[tab].length === 0) {
            return;
          } else {
            return (
              <div
                className={`period ${selectedQuadrant === tab ? 'active' : ''}`}
                key={tab}
                onClick={() => handleChange(tab)}
              >
                <span className="period--label">{tab}</span>
              </div>
            );
          }
        })}
    </div>
  );
};

export default Timeline;
