import React from 'react';
import './KBody.scss';
import KGraph from '../KGraph';
import KEventList from '../KEventList';
import KMetrics from '../KMetrics';
import SelectTabs from '../SelectTabs';
import Recommendation from '../Recommendation';
import KOptions from '../KOptions';
import clsx from 'clsx';
import KTabs from '../KTabs';

const KBody = ({
  graphDataSets,
  quadrants,
  selectedQuadrant,
  selectedEventItem,
  updateSelectedEventItem,
  setSelectedPeriod,
  isKEvent,
  prepareDataSets,
  showSingleEvent,
  eventTitle = '',
}) => {
  return (
    <div id="kBody">
      {!isKEvent && (
        <div className="kTabs__header">
          <KTabs type={'regions'} prepareDataSets={prepareDataSets} />
        </div>
      )}

      <div
        className="kGridWrapper"
        style={{
          maxHeight: `calc(100vh - 60px)`,
          minHeight: `calc(100vh - 60px)`,
        }}
      >
        {!isKEvent && (
          <div className="kBody__leftGrid flex-25">
            <KEventList
              quadrants={quadrants}
              selectedQuadrant={selectedQuadrant}
              selectedEventItem={selectedEventItem}
              updateSelectedEventItem={updateSelectedEventItem}
              prepareDataSets={prepareDataSets}
            />
          </div>
        )}

        <div className={clsx({ kBody__rightGrid: true, 'flex-75': !isKEvent })}>
          <div className="kGraph__header">
            <KOptions
              toggleCategory={false}
              toggleInfo={true}
              toggleRegion={false}
              showEventTitle={showSingleEvent}
              eventTitle={eventTitle}
            />
          </div>
          <div
            className={clsx({
              bubbleGraphWrapper: true,
            })}
            style={{
              height: isKEvent ? `calc(95vh - 280px)` : `calc(90vh - 205px)`,
            }}
          >
            {graphDataSets && graphDataSets.length > 0 ? (
              <KGraph
                graphDataSets={graphDataSets}
                setSelectedPeriod={setSelectedPeriod}
                quadrants={quadrants}
                isKEvent={isKEvent}
              />
            ) : (
              <p className="kGraph__noDataMsg">
                No data available for the following selection
              </p>
            )}
          </div>
          {isKEvent && (
            <div className="kGraph__subfooter">
              <Recommendation title={selectedEventItem.quadrantName} />
            </div>
          )}
          <div className="kGraph__footer">
            {graphDataSets && graphDataSets.length > 0 && (
              <SelectTabs
                setSelectedPeriod={setSelectedPeriod}
                quadrants={quadrants}
                selectedQuadrant={selectedQuadrant}
              />
            )}
          </div>
        </div>
        {isKEvent && (
          <div className="kBody__rightGrid">
            <KMetrics selectedEventItem={selectedEventItem} />
          </div>
        )}
      </div>
    </div>
  );
};

export default KBody;
