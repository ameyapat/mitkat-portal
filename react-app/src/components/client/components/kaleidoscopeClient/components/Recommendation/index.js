import React, { useState } from 'react';
import './Recommendation.scss';
import Tooltip from '@material-ui/core/Tooltip';
import Popover from '@material-ui/core/Popover';

const Recommendation = ({ title }) => {
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;

  return (
    <div className={'recommendationWrap'}>
      <h3 className="recommendationWrap__title">Current Quadrant: {title}</h3>
      <span className="recommendationWrap__info">
        <button onClick={e => handleClick(e)}>
          <i className="far fa-question-circle"></i>
        </button>
        <Popover
          id={id}
          open={open}
          anchorEl={anchorEl}
          onClose={handleClose}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
        >
          <ul className="recommendationList">
            <li>
              <span className="rList__pill greyRhinos">Grey Rhino</span>{' '}
              <p>Frequent Incidents and Very High Impact</p>
            </li>
            <li>
              <span className="rList__pill blackSwans">Black Swan</span>
              <p>Low Frequency &amp; Catastrophic Impact</p>
            </li>
            <li>
              <span className="rList__pill preventiveActions">
                Preventive Actions and Documentation
              </span>{' '}
              <p>Frequent Incidents and Low Impact</p>
            </li>
            <li>
              <span className="rList__pill document">Document and Monitor</span>{' '}
              <p>Low Frequency and Low inpact</p>
            </li>
          </ul>
        </Popover>
      </span>
    </div>
  );
};

export default Recommendation;
