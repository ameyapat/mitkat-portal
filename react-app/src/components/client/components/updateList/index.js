import React, { Component } from 'react';
import TransparentList from '../../../../components/ui/transparentList';
import { localDateTime } from '../../../../helpers/utils';

class UpdateList extends Component {
  render() {
    const { feed, parentHandler } = this.props;
    return (
      <>
        {feed &&
          feed.length > 0 &&
          feed.map((f, idx) => (
            <TransparentList
              key={f.eventId + idx}
              id={f.eventId}
              title={f.title}
              update={f.update}
              timestamp={localDateTime(f.dateTime)}
              parentHandler={() => parentHandler(f.eventId)}
            />
          ))}
      </>
    );
  }
}

export default UpdateList;
