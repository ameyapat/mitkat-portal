import {
  AppBar,
  Button,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  Tab,
  Tabs,
  Toolbar,
} from '@material-ui/core';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import injectSheet from 'react-jss';
import styles from './style';
import { withCookies } from 'react-cookie';
import {
  setBreadcrumbs,
  setRegionDetails,
  setSelectedRegion,
  resetSelectedRegion,
  setHeatMapOfficeLocation,
} from '../../../../actions/heatMapAction';
import store from '../../../../store';
import './style.scss';
import HeatMapGraph from './components/heatmapGraph';
import MajorRisks from './components/majorRisks';
import EventList from './components/eventList';
import HeatMap from './components/heatmap';
import Categories from './components/categories';
import regionList from './helpers/regionList.json';
import {
  fetchCities,
  fetchCountries,
  fetchStates,
  generateRandomColors,
} from '../../../../helpers/utils';
import CustomModal from '../../../ui/modal';
import { showEventDetail } from '../../../../api/api';
import { openEventModal } from '../../../../actions/EventModalAction';
import ModalInner from '../../../ui/modal/ModalInner';
import { RISK_LEVEL_COLORS_NEON } from '../../../../helpers/constants';

import { fetchLocationAnalysisData } from '../../../../requestor/mitkatWeb/requestor';
import { TABS } from './constants';

import PieChart from '../../../ui/pieChart';

@withCookies
@injectSheet(styles)
@connect(state => {
  return {
    partnerDetails: state.partnerDetails.partnerDetails,
    regionDetails: state.heatMapDetails.regionDetails,
    heatMapDetails: state.heatMapDetails,
    eventModal: state.eventModal,
    themeChange: state.themeChange,
  };
})
class LocationAnalysis extends Component {
  state = {
    regionId: '',
    locationId: '',
    locationList: [],
    countries: [],
    states: [],
    metros: [],
    showModal: false,
    eventId: null,
    eventDetail: null,
    locationLevels: [],
    riskCategoryLevels: [],
    currentLevel: '',
    tabValue: 0,
    options: {
      maintainAspectRatio: true,
      legend: {
        display: true,
        position: 'bottom',
      },
    },
    pieChartObj: {
      labels: [],
      datasets: [
        {
          label: 'Risk Index',
          data: [],
          backgroundColor: [],
          borderColor: [],
          borderWidth: 1,
        },
      ],
    },
  };

  componentDidMount() {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');

    fetchCountries(authToken).then(countries => this.setState({ countries }));
    fetchStates(authToken).then(states => this.setState({ states }));
    fetchCities(authToken).then(metros => this.setState({ metros }));
    // reset all objects
    store.dispatch(setRegionDetails({}));
    store.dispatch(setBreadcrumbs([]));
    store.dispatch(resetSelectedRegion({}));
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      prevProps.heatMapDetails.selectedRegion !==
      this.props.heatMapDetails.selectedRegion
    ) {
      this.initPieChart();
    }
  }

  initPieChart() {
    const {
      selectedRegion: { riskCategories = [] },
    } = this.props.heatMapDetails;
    const { pieChartObj } = this.state;
    const dataSets = [];
    const labels = [];
    const randomColors = [];

    riskCategories.map(cat => {
      dataSets.push(cat.totalEvents);
      labels.push(cat.categoryName);
      randomColors.push(generateRandomColors());
    });

    pieChartObj.datasets[0]['data'] = dataSets;
    pieChartObj.datasets[0]['backgroundColor'] = randomColors;
    pieChartObj.datasets[0]['borderColor'] = randomColors;
    pieChartObj.labels = labels;

    this.setState({ pieChartObj });
  }

  renderRegionList = () => {
    const { classes } = this.props;

    return regionList.map(item => (
      <MenuItem
        key={item.id}
        value={item.id}
        classes={{ root: classes.rootMenuItem }}
      >
        {item.region}
      </MenuItem>
    ));
  };

  changeLocationType = e => {
    this.setState({ regionId: e.target.value }, () => {
      this.setLocationList();
    });
  };

  setLocationList = () => {
    let { countries, states, metros, regionId, locationList } = this.state;

    if (parseInt(regionId) === 1) {
      locationList = countries;
    } else if (parseInt(regionId) === 2) {
      locationList = states;
    } else if (parseInt(regionId) === 3) {
      locationList = metros;
    } else {
      alert('Please select location type');
    }

    this.setState({ locationList });
  };

  renderLocationList = () => {
    const { locationList } = this.state;

    let { classes } = this.props;

    let locationListArr = [];
    locationListArr = locationList.map(item => (
      <MenuItem
        key={item.value}
        value={item.value}
        classes={{ root: classes.rootMenuItem }}
      >
        {item.label}
      </MenuItem>
    ));
    return locationListArr;
  };

  handleChangeLocation = e => {
    this.setState({ locationId: e.target.value });
  };

  handleShowModal = () => {
    this.setState({ showModal: true });
  };

  handleCloseModal = () => {
    this.setState({ showModal: false });
  };

  fetchData = (regionId, locationId) => {
    let { locationLevels } = this.state;
    let reqData = {
      regionid: regionId,
      locationid: locationId,
    };

    fetchLocationAnalysisData(reqData)
      .then(data => {
        const locationMapped = [];
        locationMapped.push(data);

        locationMapped.reduce((accumulator, currObj) => {
          let key = currObj.locationName;
          let value = currObj;

          if (!accumulator[key]) {
            accumulator[key] = [];
          }
          accumulator[key].push(value);
          locationLevels.push(key);

          let resultDataRiskCat = value.riskCategories.reduce(
            (accumulatorRiskCat, currObjRiskCat) => {
              let keyRiskCat = currObjRiskCat.categoryName;
              let valueRiskCat = currObjRiskCat;

              if (!accumulatorRiskCat[keyRiskCat]) {
                accumulatorRiskCat[keyRiskCat] = [];
              }

              let resultDataSubRiskCat = valueRiskCat.riskCategories.reduce(
                (accumulatorSubRiskCat, currObjRiskCat) => {
                  let keySubRiskCat = currObjRiskCat.categoryName;
                  let valueSubRiskCat = currObjRiskCat;

                  if (!accumulatorSubRiskCat[keySubRiskCat]) {
                    accumulatorSubRiskCat[keySubRiskCat] = [];
                  }

                  accumulatorSubRiskCat[keySubRiskCat].push(valueSubRiskCat);

                  return accumulatorSubRiskCat;
                },
                {},
              );
              accumulatorRiskCat[keyRiskCat].push(valueRiskCat);

              let riskSubRiskData = {
                ...accumulatorRiskCat,
                ...resultDataSubRiskCat,
              };

              return riskSubRiskData;
            },
            {},
          );

          let finalData = {
            ...accumulator,
            ...resultDataRiskCat,
          };

          store.dispatch(setRegionDetails(finalData));

          return accumulator;
        }, {});

        store.dispatch(
          setSelectedRegion({
            catId: locationLevels[0],
            catMeta: this.props.heatMapDetails.regionDetails,
          }),
        );

        store.dispatch(setBreadcrumbs(locationLevels));
        store.dispatch(setHeatMapOfficeLocation(data.officeLocations));
      })
      .catch(e => console.log(e));
  };

  getLocationDetails = () => {
    let { locationId, regionId } = this.state;
    this.setState({ showModal: false, locationLevels: [] }, () => {
      this.fetchData(regionId, locationId);
    });
  };

  showSubRiskData = riskCatName => {
    let { locationLevels } = this.state;
    let meta = this.props.heatMapDetails.regionDetails;

    locationLevels.push(riskCatName);

    store.dispatch(setBreadcrumbs(locationLevels));
    store.dispatch(
      setSelectedRegion({
        catId: riskCatName,
        catMeta: meta,
      }),
    );
  };

  handleBreadCrumbs = value => {
    let { breadcrumbArray = [] } = this.props.heatMapDetails;
    let meta = this.props.heatMapDetails.regionDetails;
    let indexOf = breadcrumbArray.indexOf(value);
    const updatedArray = breadcrumbArray.splice(0, indexOf + 1);

    this.setState({ locationLevels: updatedArray });
    store.dispatch(setBreadcrumbs(updatedArray));

    if (value) {
      store.dispatch(
        setSelectedRegion({
          catId: value,
          catMeta: meta,
        }),
      );
    }
  };

  handleOpenEvent = eventId => {
    const eventObj = {
      showEventModal: true,
      eventId,
    };
    store.dispatch(openEventModal(eventObj));
    showEventDetail(eventId).then(response => {
      this.setState({ eventDetail: response });
    });
  };

  handleCloseEvent = () => {
    const eventObj = {
      showEventModal: false,
    };
    store.dispatch(openEventModal(eventObj));
  };

  // Tabvalue change for Region Data Show
  handleTabChange = (e, value) => {
    this.setState({
      tabValue: value,
    });
  };

  onBreadCrumbChange = item => {
    const { breadcrumbArray } = this.props.heatMapDetails;
    const lastIndex = breadcrumbArray.indexOf(item);
    const foundItem = breadcrumbArray[lastIndex];

    if (foundItem === item) {
      this.handleBreadCrumbs(item);
    }
  };

  render() {
    const { classes, eventModal } = this.props;
    const { whiteLogo, partnerName } = this.props.partnerDetails;
    const {
      breadcrumbArray = [],
      regionDetails = {},
      selectedRegion: { heatmapdatapoints },
      selectedRegion,
    } = this.props.heatMapDetails;
    const {
      locationList = [],
      showModal,
      eventDetail,
      tabValue,
      regionId,
      locationId,
    } = this.state;

    return (
      <>
        {/* <AppBar position="fixed" classes={{ root: classes.rootAppBar }}>
          <Toolbar classes={{ root: classes.rootToolbar }}>
            <div className={classes.logoWrapper}>
              <img
                src={whiteLogo}
                alt={partnerName}
                className={classes.mitkatLogo}
              />
              <h1 className={classes.logoHeader}>Location Risk Review</h1>
            </div>
            <div>
              <Button
                classes={{ root: classes.btnFilter }}
                onClick={this.handleShowModal}
              >
                <i className="fas fa-filter"></i>
              </Button>
              <Button
                classes={{ root: classes.btnLogout }}
                onClick={() => this.props.history.push('/client/dashboard')}
              >
                <i className="fas fa-home"></i>
              </Button>
            </div>
          </Toolbar>
        </AppBar> */}

        <div id="regionData--wrap">
          <h1 className="rootHeader">
            {' '}
            Location Risk Review
            <div className="header--button">
              <Button
                classes={{ root: classes.btnFilter }}
                onClick={this.handleShowModal}
              >
                <i className="fas fa-filter"></i>
              </Button>
              <Button
                classes={{ root: classes.btnLogout }}
                onClick={() => this.props.history.push('/client/dashboard')}
              >
                <i className="fas fa-home"></i>
              </Button>
            </div>{' '}
          </h1>
          {breadcrumbArray && breadcrumbArray.length > 0 && (
            <div className="breadcrumb--box">
              <ol>
                {breadcrumbArray.map(item => (
                  <li onClick={() => this.onBreadCrumbChange(item)}>{item}</li>
                ))}
              </ol>
            </div>
          )}
          <div className="analysis--data">
            <div id="categoriesList">
              {breadcrumbArray && breadcrumbArray.length > 0 && (
                <>
                  <div className="location--box">
                    <div>
                      <p>
                        <span
                          style={{
                            color:
                              RISK_LEVEL_COLORS_NEON[
                                Math.ceil(selectedRegion?.rating)
                              ],
                          }}
                        >
                          {parseFloat(selectedRegion?.rating).toFixed(2)}
                          <span
                            style={{
                              color: '#ddd',
                              padding: '0 5px',
                              fontSize: '20px !important',
                            }}
                          >
                            /5
                          </span>
                        </span>

                        <span>Risk Rating </span>
                      </p>
                      <p>
                        <span>{selectedRegion?.totalEvents}</span>
                        <span>Total Events </span>
                      </p>
                    </div>
                  </div>
                  <Categories showSubRiskData={this.showSubRiskData} />
                </>
              )}
            </div>
            {breadcrumbArray && breadcrumbArray.length > 0 && (
              <div id="regionData">
                {/* <div className="breadcrumb--box">
                <ol>
                  {breadcrumbArray.map(item => (
                    <li onClick={() => this.onBreadCrumbChange(item)}>
                      {item}
                    </li>
                  ))}
                </ol>
              </div> */}
                <div className={classes.rootAdvisories}>
                  <div className={classes.regionBox}>
                    <AppBar
                      position="static"
                      color="default"
                      classes={{ root: classes.rootAppBarTab }}
                    >
                      <Tabs
                        classes={{
                          indicator: classes.indicatorTabs,
                          root: classes.rootTabs,
                        }}
                        value={tabValue}
                        onChange={this.handleTabChange}
                        variant="scrollable"
                        scrollButtons="auto"
                      >
                        {TABS.map(tab => {
                          return (
                            <Tab
                              classes={{
                                root: classes.rootTab,
                                selected: classes.selectedTab,
                              }}
                              icon={<i className={tab.icon}></i>}
                            />
                          );
                        })}
                      </Tabs>
                    </AppBar>
                    <div className="viewBox">
                      {breadcrumbArray?.length && tabValue === 0 && (
                        <HeatMapGraph />
                      )}
                      {breadcrumbArray?.length > 0 && tabValue === 2 && (
                        <MajorRisks />
                      )}
                      {breadcrumbArray?.length > 0 && tabValue === 3 && (
                        <EventList handleOpenEvent={this.handleOpenEvent} />
                      )}
                      {heatmapdatapoints && tabValue === 1 && (
                        <div id="heatMap">
                          <div className="heatMap--wrap">
                            <h1>Heat Map</h1>
                            <HeatMap
                              googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyCYOTLyn6xbysArlvlW4zpxnH9p6H3An08&v=3.exp&libraries=geometry,drawing,places,visualization"
                              loadingElement={
                                <div style={{ height: `100%` }} />
                              }
                              containerElement={
                                <div
                                  style={{
                                    height: `55vh`,
                                    position: 'relative',
                                  }}
                                />
                              }
                              mapElement={<div style={{ height: `100%` }} />}
                            />
                          </div>
                        </div>
                      )}
                      {breadcrumbArray?.length > 0 && tabValue === 4 && (
                        <div id="riskReview">
                          <div className="riskReview--wrap">
                            <h1>Risk Review</h1>
                            Coming Soon!!
                          </div>
                        </div>
                      )}
                      {tabValue === 5 && (
                        <div id="pieEventCount">
                          <div className="riskReview--wrap">
                            <h1>Risk Distribution</h1>
                            <PieChart
                              data={this.state.pieChartObj}
                              options={this.state.options}
                              width={120}
                              height={40}
                            />
                          </div>
                        </div>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            )}
          </div>
        </div>
        <CustomModal
          showModal={showModal}
          closeModal={this.handleCloseModal}
          showCloseOption={true}
        >
          <div className="modal__inner region__filter_modal">
            <div className={classes.selectionWrapper}>
              <FormControl classes={{ root: classes.formControl }}>
                <InputLabel
                  classes={{
                    root: classes.rootLabel,
                  }}
                  htmlFor="select-country"
                >
                  Select Location Type
                </InputLabel>
                <Select
                  value={regionId}
                  className={classes.rootSelect}
                  onChange={this.changeLocationType}
                  inputProps={{
                    shrink: false,
                    name: 'Select Location Type',
                    id: 'select-loctaion-type',
                    classes: {
                      root: classes.rootInput,
                      input: classes.rootInput,
                      select: classes.rootSelect,
                      icon: classes.rootIcon,
                      paper: classes.rootList,
                    },
                  }}
                >
                  {regionList?.length > 0 && this.renderRegionList()}
                </Select>
              </FormControl>
              {locationList.length > 0 && (
                <FormControl classes={{ root: classes.formControl }}>
                  <InputLabel
                    classes={{
                      root: classes.rootLabel,
                    }}
                    htmlFor="select-country"
                  >
                    Select Location
                  </InputLabel>
                  <Select
                    value={locationId}
                    className={classes.rootSelect}
                    onChange={this.handleChangeLocation}
                    inputProps={{
                      shrink: false,
                      name: 'Select Location',
                      id: 'select-loctaion',
                      classes: {
                        root: classes.rootInput,
                        input: classes.rootInput,
                        select: classes.rootSelect,
                        icon: classes.rootIcon,
                        paper: classes.rootList,
                      },
                    }}
                  >
                    {this.renderLocationList()}
                  </Select>
                </FormControl>
              )}
            </div>
            <div className="actionables">
              <Button
                onClick={this.handleCloseModal}
                variant="contained"
                color="default"
              >
                Cancel
              </Button>
              <Button
                onClick={this.getLocationDetails}
                className={classes.applyBtn}
                variant="contained"
                color="secondary"
                disabled={regionId === ''}
              >
                Apply
              </Button>
            </div>
          </div>
        </CustomModal>
        <CustomModal
          showModal={eventModal.showEventModal}
          closeModal={this.handleCloseEvent}
        >
          <div className="modal__inner dark--body">
            {eventDetail && (
              <ModalInner
                eventDetail={eventDetail}
                risklevel={eventDetail?.risklevel}
                riskcategory={eventDetail?.riskcategory}
                closeModal={this.handleCloseEvent}
                isLive={false}
                hasDarkTheme={true}
              />
            )}
          </div>
        </CustomModal>
      </>
    );
  }
}

export default LocationAnalysis;
