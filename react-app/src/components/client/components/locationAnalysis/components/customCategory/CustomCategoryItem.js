import React, { useState } from 'react';
import './CustomCategory.scss';

const CustomCategoryItem = ({ showSubRisk, renderRatings, item }) => {
  const [show, setShow] = useState(false);

  return (
    <>
      <li
        style={{
          borderRadius: 2,
          padding: 4,
          alignItems: 'center',
        }}
        className={`${show && 'highlightHeader'}`}
      >
        <h1
          onClick={() => showSubRisk(item.categoryName)}
          className="riskCatItemHead"
        >
          {item.categoryName}
        </h1>
        <div className="riskrating-box" style={{ flex: 1 }}>
          <span> {renderRatings(item.rating)} &nbsp; </span>
          <p> Risk Rating - {Math.ceil(item.rating)}/5 </p>{' '}
        </div>
        {item?.riskCategories?.length > 0 && (
          <div className="riskrating-box" style={{ flex: 1 }}>
            <span onClick={() => setShow(!show)}>
              <i
                className={`far ${
                  !show ? 'fa-plus-square' : 'fa-minus-square'
                }`}
              ></i>
            </span>
          </div>
        )}
      </li>
      {show && (
        <ul style={{ paddingLeft: 15, margin: 0, userSelect: 'none' }}>
          {item?.riskCategories?.length > 0 &&
            item?.riskCategories.map(item => (
              <li>
                <h1
                  onClick={() => showSubRisk(item.categoryName)}
                  style={{ textDecoration: 'underline' }}
                >
                  {item.categoryName}
                </h1>
                <div className="riskrating-box">
                  <span> {renderRatings(item.rating)} &nbsp; </span>
                  <p> Risk Rating - {Math.ceil(item.rating)}/5 </p>{' '}
                </div>
              </li>
            ))}
        </ul>
      )}
    </>
  );
};

export default CustomCategoryItem;
