import React, { useState } from 'react';
import './CustomCategory.scss';
import CustomCategoryItem from './CustomCategoryItem';

const CustomCategory = ({ riskCategories, showSubRisk, renderRatings }) => {
  return riskCategories?.length > 0
    ? riskCategories.map(item => (
        <CustomCategoryItem
          showSubRisk={showSubRisk}
          renderRatings={renderRatings}
          item={item}
        />
      ))
    : 'No data available';
};

export default CustomCategory;
