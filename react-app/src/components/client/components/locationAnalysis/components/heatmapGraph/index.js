import React, { Component } from 'react';
import { connect } from 'react-redux';
import Highcharts from 'highcharts/highstock';
import HighchartsReact from 'highcharts-react-official';
import './heatMapGraph.scss';

@connect(state => {
  return {
    heatMapDetails: state.heatMapDetails,
  };
})
class HeatMapGraph extends Component {
  state = {
    chartOptions: {
      chart: {
        height: 330,
        type: 'area',
      },
      series: [
        {
          data: [],
        },
      ],
      plotOptions: {
        series: {
          color: '#3a7cbd ',
          fillColor: {
            linearGradient: {
              x1: 0,
              y1: 0,
              x2: 0,
              y2: 1,
            },
            stops: [
              [0, Highcharts.getOptions().colors[0]],
              [
                1,
                Highcharts.Color(Highcharts.getOptions().colors[0])
                  .setOpacity(0)
                  .get('rgba(255, 255, 255, 0.5)'),
              ],
            ],
          },
          point: {
            events: {
              mouseOver: e => this.setHoverData(e),
            },
          },
        },
      },
    },
  };

  componentDidMount() {
    this.initGraphData();
  }

  componentDidUpdate(prevProps) {
    if (
      prevProps.heatMapDetails.selectedRegion !==
      this.props.heatMapDetails.selectedRegion
    ) {
      this.initGraphData();
    }
  }

  setHoverData = e => {
    this.setState({ hoverData: e.target.category });
  };

  initGraphData = () => {
    let updatedRegionData = this.props.heatMapDetails.selectedRegion;
    let updatedSeriesData = [];
    const timestampData = {
      data: [],
    };

    if (updatedRegionData?.graphpoints?.length > 0) {
      updatedRegionData.graphpoints.map(item => {
        const graphPointsData = [];
        graphPointsData.push(item.timestamp, item.value);
        timestampData.data.push(graphPointsData);
      });
    }

    updatedSeriesData = [...timestampData.data];

    this.setState({
      chartOptions: {
        ...this.state.chartOptions,
        ...{ series: [{ data: [...updatedSeriesData] }] },
      },
    });
  };

  render() {
    const { chartOptions } = this.state;
    return (
      <>
        <div id="heatMapGraph">
          <div className="heatMapGraph--wrap">
            <h1>Heat Map Graph</h1>
            <HighchartsReact
              highcharts={Highcharts}
              constructorType={'stockChart'}
              options={chartOptions}
            />
          </div>
        </div>
      </>
    );
  }
}

export default HeatMapGraph;
