import React, { Component } from 'react';
import { connect } from 'react-redux';
import { RISK_LEVEL_COLORS_NEON } from '../../../../../../helpers/constants';
import CustomCategory from '../customCategory/CustomCategory';
import './style.scss';

@connect(state => {
  return {
    heatMapDetails: state.heatMapDetails,
  };
})
class Categories extends Component {
  showSubRisk = name => {
    this.props.showSubRiskData(name);
  };

  renderRatings = rating => {
    let menuItems = [];
    const newRating = Math.ceil(rating);

    for (var i = 0; i < newRating; i++) {
      menuItems.push(
        <span
          style={{
            backgroundColor: RISK_LEVEL_COLORS_NEON[newRating],
          }}
        ></span>,
      );
    }
    return menuItems;
  };

  renderCategoryList = () => {
    const {
      selectedRegion: { riskCategories },
    } = this.props.heatMapDetails;

    return (
      <CustomCategory
        renderRatings={this.renderRatings}
        showSubRisk={this.showSubRisk}
        riskCategories={riskCategories}
      />
    );
  };

  render() {
    let { breadcrumbArray = [] } = this.props.heatMapDetails;

    return (
      <>
        <div id="categories">
          <div className="categories--wrap">
            <h1>
              {breadcrumbArray?.length > 1
                ? 'Sub Risk Categories'
                : 'Risk Categories'}
            </h1>
            <ul>{this.renderCategoryList()}</ul>
          </div>
        </div>
      </>
    );
  }
}

export default Categories;
