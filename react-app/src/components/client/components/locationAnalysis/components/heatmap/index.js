import React, { Component } from 'react';
import { withScriptjs, withGoogleMap, GoogleMap } from 'react-google-maps';
import HeatmapLayer from 'react-google-maps/lib/components/visualization/HeatmapLayer';
import CustomMarker from './CustomMarker';
//helpers
import { withCookies } from 'react-cookie';
import darkStyle from '../../helpers/universalMapStyles.json';
import { connect } from 'react-redux';
import './style.scss';
// assets
import customIconOffice from '../../../../../../assets/solid-pins/officeLocation.png';

const google = window.google;

const defaultOptions = {
  mapTypeControl: false,
  streetViewControl: false,
  zoomControl: false,
  fullscreenControl: false,
  styles: darkStyle,
};

@withCookies
@withScriptjs
@withGoogleMap
@connect(state => {
  return {
    heatMapDetails: state.heatMapDetails,
  };
})
class HeatMap extends Component {
  setLatLng = () => {
    let {
      selectedRegion: { heatmapdatapoints = [] },
    } = this.props.heatMapDetails;

    return heatmapdatapoints.map((i, idx) => ({
      location: new google.maps.LatLng(i.latitude, i.longitude),
      weight: i.intensity,
    }));
  };

  renderOfficeLocations = () => {
    const { officeLocations } = this.props.heatMapDetails;

    return officeLocations.map(location => {
      return (
        <CustomMarker
          lat={location.latitude}
          lng={location.longitude}
          key={location.id}
          address={location.address}
          customIcon={customIconOffice}
        />
      );
    });
  };

  renderHeatMap = () => {
    const {
      selectedRegion: { heatmapdatapoints = [] },
    } = this.props.heatMapDetails;

    return (
      <HeatmapLayer
        data={heatmapdatapoints?.length > 0 ? this.setLatLng() : []}
        options={{ radius: 20 }}
      />
    );
  };

  render() {
    let {
      selectedRegion,
      officeLocations,
      selectedRegion: { heatmapdatapoints },
    } = this.props.heatMapDetails;
    let lat = heatmapdatapoints[0].latitude;
    let lng = heatmapdatapoints[0].longitude;

    return (
      <>
        {selectedRegion?.heatmapdatapoints?.length > 0 ? (
          <GoogleMap
            zoom={5}
            defaultOptions={defaultOptions}
            defaultCenter={{
              lat,
              lng,
            }}
          >
            <>
              {selectedRegion?.heatmapdatapoints?.length > 0 &&
                this.renderHeatMap()}
              {officeLocations?.length > 0 && this.renderOfficeLocations()}
            </>
          </GoogleMap>
        ) : (
          'No data availabe to load on map'
        )}
      </>
    );
  }
}

export default HeatMap;
