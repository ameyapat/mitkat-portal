export default {
  formControl: {
    width: '400px',
    backgroundColor: 'var(--darkGrey) !important',
  },
  rootLabel: {
    color: 'var(--whiteHighEmp) !important',
  },
  rootIcon: {
    color: 'var(--whiteHighEmp) !important',
  },
  rootInput: {
    color: 'var(--whiteHighEmp) !important',
    borderBottom: '2px solid #eee !important',

    '&:hover': {
      borderBottom: '2px solid #eee !important',
      color: 'var(--whiteHighEmp) !important ', // or black
    },
  },
  underline: {
    '&:after': {
      borderBottom: '2px solid #34495e !important',
    },
  },
  notchedOutline: {
    borderColor: 'var(--whiteHighEmp) !important ',
  },
  rootSelect: {
    color: 'var(--whiteHighEmp) !important ',
  },
  rootMenuItem: {
    backgroundColor: 'var(--darkGrey) !important',
    color: 'var(--whiteHighEmp) !important ',
  },
  rootList: {
    backgroundColor: 'var(--darkGrey) !important',
    color: 'var(--whiteHighEmp) !important ',
  },
};
