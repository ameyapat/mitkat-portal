import React, { Component } from 'react';
import { connect } from 'react-redux';
import './style.scss';
import style from './style';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import injectSheet from 'react-jss/lib/injectSheet';

@injectSheet(style)
class Topics extends Component {
  renderList = () => {
    const { topics = [] } = this.props.riskExposure.riskExposureData;
    let { classes } = this.props;

    return topics.map(item => (
      <Accordion
        classes={{
          root: classes.rootAccotdion,
        }}
      >
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
          classes={{
            root: classes.rootAccotdionSummary,
            content: classes.contentSummary,
            expandIcon: classes.icon,
          }}
        >
          <li>
            <p> {item.dataname} </p>
            <span> {item.datavalue} </span>Locations affected
          </li>
        </AccordionSummary>
        <AccordionDetails
          classes={{
            root: classes.rootAccotdionDetails,
          }}
        >
          <ul className="eventList--wrap">
            {item?.dataSet?.map(subitem => (
              <h5>{subitem}</h5>
            ))}
          </ul>
        </AccordionDetails>
      </Accordion>
    ));
  };

  render() {
    const { topics = [] } = this.props.riskExposure.riskExposureData;

    return (
      <div id="topics">
        <div className="topics--wrap">
          <h1>Major Risk events affecting organization</h1>
          <ul>
            {this.renderList()}
          </ul>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    riskExposure: state.riskExposure,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Topics);
