import React, { Component } from 'react';
import { withScriptjs, withGoogleMap, GoogleMap } from 'react-google-maps';
import CustomMarker from './CustomMarker';
import { connect } from 'react-redux';
import customMapStyles from '../../../../../../helpers/universalMapStyles.json';
import customMapStylesLight from '../../../../../../helpers/universalMapStylesLight.json';

const defaultOptions = {
  mapTypeControl: false,
  streetViewControl: false,
  zoomControl: false,
  fullscreenControl: false,
};

@withScriptjs
@withGoogleMap
@connect(state => {
  return {
    riskExposure: state.riskExposure,
    partnerDetails: state.partnerDetails.partnerDetails,
    themeChange: state.themeChange,
  };
})
class MapWithMarker extends Component {
  state = {
    latState: -5.5086192,
    lngState: 103.9100664,
  };

  renderOfficeLocations = () => {
    let { locations, customIcon } = this.props;
    return locations.map(item => {
      return (
        <CustomMarker
          id={item.id}
          key={item.id}
          lat={item.latitude}
          lng={item.longitude}
          locationName={item.locationName}
          customIcon={customIcon[item.riskRating]}
          showLocations={false}
        />
      );
    });
  };

  setCenter = (lat, lng) => this.setState({ latState: lat, lngState: lng });

  render() {
    let {
      mapStyles,
      locations,
      themeChange: { setDarkTheme },
    } = this.props;
    let { latitude, longitude } = this.props.partnerDetails;

    return (
      <GoogleMap
        defaultZoom={1}
        defaultOptions={{ ...defaultOptions }}
        options={{
          ...defaultOptions,
          styles: setDarkTheme ? customMapStyles : customMapStylesLight,
        }}
        defaultCenter={{
          lat: latitude || 16.950209,
          lng: longitude || 93.207995,
        }}
      >
        {locations.length > 0 && this.renderOfficeLocations()}
      </GoogleMap>
    );
  }
}

export default MapWithMarker;
