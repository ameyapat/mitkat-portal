import React from 'react';
import MapWithMarker from './MapWithMarker';

const CustomGoogleMap = ({
  googleMapURL,
  loadingElement,
  containerElement,
  mapElement,
  locations,
  eventLocation,
  mapStyles,
  classes,
  customIcon,
  customLocationIcon,
  onPinClick,
}) => {
  return (
    <MapWithMarker
      googleMapURL={googleMapURL}
      loadingElement={loadingElement}
      containerElement={containerElement}
      mapElement={mapElement}
      locations={locations}
      eventLocation={eventLocation}
      classes={classes}
      customIcon={customIcon}
      customLocationIcon={customLocationIcon}
      onPinClick={onPinClick}
    />
  );
};

export default CustomGoogleMap;
