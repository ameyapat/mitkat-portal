export default {
  formControl: {
    width: '100%',
    backgroundColor: 'var(--darkGrey) !important',
    padding: '15px !important',
    borderRadius: 5,
  },
  rootLabel: {
    color: 'var(--whiteMediumEmp) !important ',
    top: '15px !important',
    left: '10px !important',
    fontSize: '21px !important',
  },
  rootIcon: {
    color: 'var(--whiteHighEmp) !important',
  },
  rootInput: {
    color: 'var(--whiteHighEmp) !important',
    borderBottom: '2px solid rgba(255,255,255, 0) !important',

    '&:hover': {
      borderBottom: '2px solid #eee !important',
      color: 'var(--whiteHighEmp) !important ', // or black
    },
  },
  underline: {
    '&:after': {
      borderBottom: '2px solid rgba(255,255,255, 0) !important',
    },
  },
  notchedOutline: {
    borderColor: 'var(--whiteHighEmp) !important ',
  },
  rootSelect: {
    color: 'var(--whiteHighEmp) !important ',
    marginTop: '0 !important',
    paddingLeft: '15px',
  },
  rootMenuItem: {
    backgroundColor: 'var(--darkGrey) !important',
    color: 'var(--whiteHighEmp) !important ',
  },
  rootList: {
    backgroundColor: 'var(--darkGrey) !important',
    color: 'var(--whiteHighEmp) !important ',
  },
};
