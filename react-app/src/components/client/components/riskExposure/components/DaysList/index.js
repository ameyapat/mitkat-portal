import React, { Component } from 'react';
import injectSheet from 'react-jss';
import styles from './style';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import './style.scss';
import days from '../../days.json';
import { connect } from 'react-redux';

@injectSheet(styles)
@connect(state => {
  return {
    themeChange: state.themeChange,
  };
})
class DaysList extends Component {
  daysXML = () => {
    let daysList = [];
    const {
      classes,
      themeChange: { setDarkTheme },
    } = this.props;

    daysList = days.map(item => (
      <MenuItem
        key={item.id}
        value={item.value}
        classes={{ root: classes.rootMenuItem }}
        className={`${setDarkTheme ? 'dark' : 'light'}`}
      >
        {item.numberOfDays}
      </MenuItem>
    ));

    return daysList;
  };

  render() {
    const { classes, days, handleChange } = this.props;
    return (
      <div id="daysList">
        <FormControl classes={{ root: classes.formControl }}>
          <InputLabel
            classes={{
              root: classes.rootLabel,
            }}
            htmlFor="select-country"
          >
            <i class="fas fa-calendar-day"></i>
          </InputLabel>
          <Select
            value={days}
            className={classes.rootSelect}
            onChange={handleChange}
            inputProps={{
              shrink: false,
              name: 'Select Country',
              id: 'select-country',
              classes: {
                root: classes.rootInput,
                input: classes.rootInput,
                select: classes.rootSelect,
                icon: classes.rootIcon,
                paper: classes.rootList,
              },
            }}
          >
            {this.daysXML()}
          </Select>
        </FormControl>
      </div>
    );
  }
}

export default DaysList;
