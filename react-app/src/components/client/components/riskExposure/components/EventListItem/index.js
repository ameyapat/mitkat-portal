import React, { Component } from 'react';
import './style.scss';
import { connect } from 'react-redux';
import {
  RISK_LEVEL_COLORS_NEON,
  RISK_CATEGORY_NAME,
} from '../../../../../../helpers/constants';

class EventListItem extends Component {
  render() {
    const {
      eventId,
      title,
      risklevel,
      riskcategory,
      handleOpenEvent,
    } = this.props;
    return (
      <li className="eventlist--item" onClick={() => handleOpenEvent(eventId)}>
        <div>
          <h1 className="eventlist--title">{title}</h1>
          <span
            className="eventlist--item__pills"
            style={{ background: RISK_LEVEL_COLORS_NEON[risklevel] }}
          >
            {RISK_CATEGORY_NAME[riskcategory]}
          </span>
        </div>
      </li>
    );
  }
}

const mapStateToProps = state => {
  return {
    riskExposure: state.riskExposure,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EventListItem);
