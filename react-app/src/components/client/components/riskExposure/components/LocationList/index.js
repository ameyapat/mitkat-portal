import React, { Component } from 'react';
import './style.scss';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { connect } from 'react-redux';
import EventListItem from '../EventListItem';
import injectSheet from 'react-jss';
import style from './style';
import { openEventModal } from '../../../../../../actions/EventModalAction';
import store from '../../../../../../store';
import { showEventDetail } from '../../../../../../api/api';
import CustomModal from '../../../../../ui/modal';
import ModalInner from '../../../../../ui/modal/ModalInner';
import { RISK_LEVEL_COLORS_NEON } from '../../../../../../helpers/constants';
import InfoIcon from './infoIcon';
import InfoIconRiskRating from './infoIconRiskRating';
import LocationDistribution from './LocationDistribution';

@injectSheet(style)
@connect(
  state => {
    return {
      themeChange: state.themeChange,
    };
  },
  dispatch => {
    return {
      dispatch,
    };
  },
)
class LocationList extends Component {
  state = {
    eventId: null,
    eventDetail: null,
  };

  componentDidUpdate(prevProps) {
    const prevMapState = prevProps.mapState.eventDetails;
    const currentMapState = this.props.mapState.eventDetails;
    if (prevMapState !== currentMapState) {
      this.setState({ eventList: currentMapState });
    }

    if (
      prevProps.riskExposure.pauseInterval !==
      this.props.riskExposure.pauseInterval
    ) {
      if (this.props.riskExposure.pauseInterval) {
        clearInterval(this.timeout);
      } else {
        this.startTimer();
      }
    }
  }

  handleOpenEvent = eventId => {
    const eventObj = {
      showEventModal: true,
      eventId,
    };

    showEventDetail(eventId).then(response => {
      this.setState({ eventDetail: response });
    });

    store.dispatch(openEventModal(eventObj));
  };

  handleCloseEvent = () => {
    const eventObj = {
      showEventModal: false,
    };
    store.dispatch(openEventModal(eventObj));
  };

  closeModal = () => this.setState({ showEventDetailsModal: false });
  renderRatings = rating => {
    let menuItems = [];
    for (var i = 0; i < rating; i++) {
      menuItems.push(
        <span
          style={{
            backgroundColor: RISK_LEVEL_COLORS_NEON[rating],
          }}
        ></span>,
      );
    }
    return <div>{menuItems}</div>;
  };
  renderLocationList = () => {
    const { locations = [] } = this.props.riskExposure.riskExposureData;
    let { classes } = this.props;

    return locations.map(item => (
      <Accordion
        classes={{
          root: classes.rootAccotdion,
        }}
      >
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
          classes={{
            root: classes.rootAccotdionSummary,
            content: classes.contentSummary,
            expandIcon: classes.icon,
          }}
        >
          <h4>{item.locationName}</h4>
          <div className="totalEvents">
            {item.totalEvents}
            <p>
              <small> Total events </small>
            </p>
          </div>
          <p>
            {this.renderRatings(item.riskRating)} Risk Rating -{item.riskRating}
            /5 <InfoIcon />
          </p>
        </AccordionSummary>
        <AccordionDetails
          classes={{
            root: classes.rootAccotdionDetails,
          }}
        >
          <ul className="eventList--wrap">
            {item?.events?.map(subitem => (
              <>
                <EventListItem
                  key={subitem.id}
                  eventId={subitem.id}
                  title={subitem.title}
                  eventtense={subitem.eventtense}
                  risklevel={subitem.risklevel}
                  riskcategory={subitem.riskcategory}
                  datetime={subitem.datetime}
                  timestamp={subitem.timestamp}
                  distance={subitem.distance}
                  locationName={subitem.locationName}
                  updates={subitem.updates}
                  handleOpenEvent={this.handleOpenEvent}
                  lat={subitem.latitude}
                  lng={subitem.longitude}
                />
              </>
            ))}
          </ul>
        </AccordionDetails>
      </Accordion>
    ));
  };

  render() {
    const { eventDetail } = this.state;
    const { eventModal } = this.props;
    const {
      themeChange: { setDarkTheme },
    } = this.props;
    const {
      riskExposureData: { organizationriskrating },
    } = this.props.riskExposure;

    return (
      <div id="locationList">
        <p className="organizationriskrating">
          Organization Risk Rating
          <InfoIconRiskRating organizationriskrating={organizationriskrating} />
          <div className="riskrating-gauge">
            <div className="riskrating-background">
              <div
                className="riskrating-percentage"
                style={{
                  transform: `rotate(calc(36 * ${parseFloat(
                    organizationriskrating,
                  ).toFixed(0)}deg))`,
                  backgroundColor:
                    RISK_LEVEL_COLORS_NEON[
                      parseFloat(organizationriskrating).toFixed(0)
                    ],
                }}
              ></div>
              <div className="riskrating-mask"></div>
              <span
                className="riskrating-value"
                style={{
                  color:
                    RISK_LEVEL_COLORS_NEON[
                      parseFloat(organizationriskrating).toFixed(0)
                    ],
                }}
              >
                {parseFloat(organizationriskrating).toFixed(2)}
              </span>
            </div>
            <span className="riskrating-min">0 (very low)</span>
            <span className="riskrating-max">5 (very high)</span>
          </div>
        </p>
        <LocationDistribution />
        {this.renderLocationList()}

        <CustomModal
          showModal={eventModal.showEventModal}
          closeModal={this.handleCloseEvent}
        >
          <div
            className={`modal__inner dark--body ${
              setDarkTheme ? 'dark' : 'light'
            }`}
          >
            {eventDetail && (
              <ModalInner
                eventDetail={eventDetail && eventDetail}
                risklevel={eventDetail.risklevel}
                riskcategory={eventDetail.riskcategory}
                closeModal={this.handleCloseEvent}
                isLive={false}
                hasDarkTheme={true}
              />
            )}
          </div>
        </CustomModal>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    mapState: state.mapState,
    riskExposure: state.riskExposure,
    eventModal: state.eventModal,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LocationList);
