export default {
  rootAccotdion: {
    backgroundColor: 'var(--darkGrey) !important',
    borderBottom: '1px solid #314058',
  },
  rootAccotdionSummary: {
    flexDirection: 'row',
    paddingLeft: '100px !important',
    minHeight: '93px !important',
  },
  contentSummary: {
    display: 'block !important',
    color: 'var(--whiteMediumEmp)',

    '& h4': {
      color: 'var(--whiteMediumEmp)',
      marginBottom: '10px !important',
      fontWeight: '100',
    },
    '& .totalEvents': {
      position: 'absolute',
      background: '#111c2f',
      left: '10px',
      top: '10px',
      padding: '10px',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      fontSize: '25px',
      color: '#e0e0e0',
      borderRadius: '5px',

      '& p': {
        fontSize: '50% !important',
        marginTop: '3px',
      },
    },
    '& p': {
      color: '#9a9a9a',
      marginBottom: '5px',
      fontSize: '13px !important',
      '& span': {
        padding: '0 10px',
        color: '#000',
        borderRadius: '5px',
        fontSize: '5px !important',
        marginRight: '3px',
      },
      '& span.infoIcon': {
        color: '#314058',

        '& button': {
          background: 'transparent',
          border: 'none',
          cursor: 'pointer',

          '& i': {
            color: '#4caf50',
          },
        },
      },
    },
  },
  icon: {
    color: '#3b496d',
  },
  rootAccotdionDetails: {},
};
