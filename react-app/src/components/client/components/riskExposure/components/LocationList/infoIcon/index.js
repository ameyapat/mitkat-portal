import React, { useState } from 'react';
import '../style.scss';
import Popover from '@material-ui/core/Popover';
import { RISK_LEVEL_COLORS_NEON } from '../../../../../../../helpers/constants';

const InfoIcon = () => {
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;

  return (
    <span className="infoIcon">
      <button onClick={e => handleClick(e)}>
        <i className="far fa-question-circle"></i>
      </button>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
      >
        <ul className="infoList">
          <li>
            <span
              style={{
                color: RISK_LEVEL_COLORS_NEON[5],
              }}
            >
              5
            </span>
            : Very High Risk: Event(s)/incident(s) may pose significant risk(s)
            for the locations in view of the event/incident or may violate
            government regulations.
          </li>
          <li>
            <span
              style={{
                color: RISK_LEVEL_COLORS_NEON[4],
              }}
            >
              4
            </span>
            : High Risk: The event/incident may pose heightened risks for
            continued operations in view of the security situation or
            applicability of government regulations. Limited operational hours
            for businesses or complete / temporary closure of the location may
            be likely, however issue specific.
          </li>
          <li>
            <span
              style={{
                color: RISK_LEVEL_COLORS_NEON[3],
              }}
            >
              3
            </span>
            : Medium Risk: Increase in security measures may be likely in view
            of an adverse situation, prompting appropriate coordination and
            communication among internal stakeholders to minimise operational
            disruptions.
          </li>
          <li>
            <span
              style={{
                color: RISK_LEVEL_COLORS_NEON[2],
              }}
            >
              2
            </span>
            : Low Risk: Asset location(s) could face minor disruptions
            pertaining to travel / provision of essential services for continued
            operations.
          </li>
          <li>
            <span
              style={{
                color: RISK_LEVEL_COLORS_NEON[1],
              }}
            >
              1
            </span>
            : Very Low Risk: The risk of disruption from an event is minimal and
            unlikely to have any impact on the specific location(s).
            Documentation of incidents may be required.
          </li>
          <li>
            <span
              style={{
                color: RISK_LEVEL_COLORS_NEON[1],
              }}
            >
              0
            </span>
            : No Risk: No risk related incidents have been reported or are
            likely which may impact operations, assets or employees.
          </li>
        </ul>
      </Popover>
    </span>
  );
};

export default InfoIcon;
