import React, { useState } from 'react';
import '../style.scss';
import Popover from '@material-ui/core/Popover';
import { RISK_LEVEL_COLORS_NEON } from '../../../../../../../helpers/constants';

const InfoIconRiskRating = () => {
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;

  return (
    <span className="infoIcon">
      <button onClick={e => handleClick(e)}>
        <i className="far fa-question-circle"></i>
      </button>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
      >
        <ul className="infoList">
          <li>
            {' '}
            <span
              style={{
                color: RISK_LEVEL_COLORS_NEON[5],
              }}
            >
              {' '}
              4-5{' '}
            </span>{' '}
            : Very High Risk: Multiple locations are facing significant threats
            from risks, indicating possibilities towards limits on operational
            hours of office location or even temporary closure of some of them.
          </li>
          <li>
            <span
              style={{
                color: RISK_LEVEL_COLORS_NEON[4],
              }}
            >
              {' '}
              3-4{' '}
            </span>
            : High Risk: Organisation is facing heightened risk from event(s) in
            the vicinity of their location(s) accompanied by heightened security
            measures put in place. Temporary operational disruptions are likely
            in this scenario.
          </li>
          <li>
            <span
              style={{
                color: RISK_LEVEL_COLORS_NEON[3],
              }}
            >
              2-3
            </span>
            : Medium Risk: Moderate risk to the organisation is likely from an
            event or events. Heightened security measures need to be taken at
            some locations in accordance with the on-ground situation. This
            could be risk focused or in order to ensure compliance with
            government regulations.
          </li>
          <li>
            <span
              style={{
                color: RISK_LEVEL_COLORS_NEON[2],
              }}
            >
              1-2
            </span>
            : Low Risk: Few locations are likely to face minor disruptions
            pertaining to travel and/or essential services for continued
            operations.
          </li>
          <li>
            <span
              style={{
                color: RISK_LEVEL_COLORS_NEON[1],
              }}
            >
              0-1
            </span>
            : Very Low Risk: The risk of disruption from any event is minimal
            and unlikely to have any untoward impact on the organisation.
          </li>
        </ul>
      </Popover>
    </span>
  );
};

export default InfoIconRiskRating;
