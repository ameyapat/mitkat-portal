import React, { Component } from 'react';
import { connect } from 'react-redux';
import { RISK_LEVEL_COLORS_NEON } from '../../../../../../../helpers/constants';
import './style.scss';

class LocationDistribution extends Component {
  render() {
    const {
      locationDistribution = [],
    } = this.props.riskExposure.riskExposureData;

    return (
      <div id="locationDistribution">
        <div className="locationDistribution--wrap">
          <ul>
            {locationDistribution.map(item => (
              <li
                style={{
                  borderBottom: `10px solid ${RISK_LEVEL_COLORS_NEON[item.id]}`,
                }}
              >
                <span> {item.datavalue} </span>
                <small> {item.dataname} </small>
                <p> {item.datavalue > 0 ? 'Locations' : 'Location'} </p>
              </li>
            ))}
          </ul>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    riskExposure: state.riskExposure,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LocationDistribution);
