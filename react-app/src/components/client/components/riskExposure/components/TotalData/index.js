import React, { Component } from 'react';
import { connect } from 'react-redux';
import './style.scss';

class TotalData extends Component {
  render() {
    const {
      riskExposureData: { totalEvents, totalLocations },
    } = this.props.riskExposure;

    return (
      <div id="totalData">
        <div className="total__wrap">
          <p>
            <span>{totalEvents} </span>
            <span>Total Events </span>
          </p>
          <p>
            <span>{totalLocations} </span>
            <span>Total Locations </span>
          </p>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    mapState: state.mapState,
    riskExposure: state.riskExposure,
    eventModal: state.eventModal,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TotalData);
