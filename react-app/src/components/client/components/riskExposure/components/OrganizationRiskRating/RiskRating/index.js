import React, { Component } from 'react';
import '../style.scss';
import { Radar } from 'react-chartjs-2';
import { connect } from 'react-redux';

const options = {
  elements: {
    arc: {
      borderWidth: 0,
    },
  },
  legend: {
    position: 'bottom',
    display: false,
    padding: 35,
  },
  scale: {
    ticks: {
      beginAtZero: false,
      fontColor: 'rgba(76, 164, 31, 1)',
      showLabelBackdrop: false,
    },
    reverse: false,
    pointLabels: {
      fontColor: 'var(--whiteHighEmp)',
    },
    gridLines: {
      color: 'rgba(130, 130, 130, 0.38)',
    },
  },
};

const dataSetObj = {
  maxBarThickness: 10,
  backgroundColor: 'rgba(220,220,220,0.2)',
  pointBackgroundColor: 'rgba(76, 164, 31, 1)',
  hoverOffset: 1,
  borderColor: 'rgba(76, 164, 31, 1)',
  borderWidth: 1,
};

class RiskRating extends Component {
  state = {
    metaData: {
      datasets: [],
    },
    width: 100,
    height: 55,
  };

  componentDidMount() {
    this.initriskCategoryRadarData();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.riskExposure !== this.props.riskExposure) {
      this.initriskCategoryRadarData();
    }
  }

  initriskCategoryRadarData = () => {
    const { riskCategoryRadar = [] } = this.props.riskExposure.riskExposureData;
    const metaData = {
      datasets: [],
    };
    const labels = [];
    const data = [];

    riskCategoryRadar.map(item => {
      labels.push(item.dataname);
      data.push(item.datavalue);
    });

    dataSetObj.data = data;
    metaData.labels = labels;
    metaData.datasets.push(dataSetObj);

    this.setState({ metaData });
  };

  render() {
    const {
      title,
      themeChange: { setDarkTheme },
    } = this.props;

    return (
      <div className={`riskRating--wrap ${setDarkTheme ? 'dark' : 'light'}`}>
        <h1>
          {title} <small> (in %) </small>
        </h1>
        <Radar
          data={this.state.metaData}
          options={options}
          width={this.state.width}
          height={this.state.height}
        />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    riskExposure: state.riskExposure,
    themeChange: state.themeChange,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RiskRating);
