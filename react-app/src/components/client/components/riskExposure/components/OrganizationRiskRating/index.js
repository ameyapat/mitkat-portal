import React, { Component } from 'react';
import RiskRating from './RiskRating';
import './style.scss';

class OrganizationRiskRating extends Component {
  render() {
    return (
      <div id="organizationRiskRating">
        <RiskRating title="Distribution of Risk" />
      </div>
    );
  }
}

export default OrganizationRiskRating;
