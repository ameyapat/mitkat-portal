import React, { Component } from 'react';

import './style.scss';

import CustomGoogleMap from './components/customGoogleMap';
import customMapStyles from '../../../../helpers/universalMapStyles.json';
import customMapStylesLight from '../../../../helpers/universalMapStylesLight.json';

import customLocationIcon from '../../../../assets/solid-pins/officelocation-s.png';
import verylowIcon from '../../../../assets/solid-pins/location_verylow.png';
import lowIcon from '../../../../assets/solid-pins/location_low.png';
import mediumIcon from '../../../../assets/solid-pins/location_medium.png';
import highIcon from '../../../../assets/solid-pins/location_high.png';
import veryhighIcon from '../../../../assets/solid-pins/location_veryhigh.png';

import { getRiskExposureData } from '../../../../requestor/riskExposure/requestor';
import { connect } from 'react-redux';

import { setRiskDashboardData } from '../../../../actions/riskExposureActions';

import { withCookies } from 'react-cookie';
import ViewSwitch from '../ViewSwitch/ViewSwitch';
import LocationList from './components/LocationList';
import OrganizationRiskRating from './components/OrganizationRiskRating';
import TopRisks from './components/TopRisks';
import Topics from './components/Topics';
import TotalData from './components/TotalData';
import DaysList from './components/DaysList';
import NavMenu from '../navMenu';

const mappedIcons = {
  0: verylowIcon,
  1: verylowIcon,
  2: lowIcon,
  3: mediumIcon,
  4: highIcon,
  5: veryhighIcon,
};
@withCookies
class RiskExposure extends Component {
  state = {
    days: 1,
  };

  componentDidMount() {
    let { days } = this.state;
    this.initDashboard(days);
  }

  initDashboard(daysValue) {
    getRiskExposureData(daysValue).then(data => {
      this.props.dispatch(setRiskDashboardData(data));
    });
  }

  handleChange = e => {
    let daysValue = e.target.value;
    this.setState({ days: daysValue });
    this.initDashboard(daysValue);
  };
  render() {
    const {
      locations = [],
      topics = [],
      topRisks = [],
      totalEvents,
      totalLocations,
    } = this.props.riskExposure.riskExposureData;
    let { days } = this.state;
    const {
      themeChange: { setDarkTheme },
    } = this.props;
    return (
      <>
        <div
          id="riskExposure--wrap"
          className={`${setDarkTheme ? 'dark' : 'light'}`}
        >
          <div className="riskExposure--wrap">
            {locations && locations.length && <LocationList />}
          </div>
          <div className="riskExposure--wrap">
            <DaysList handleChange={this.handleChange} days={days} />
            {locations && locations.length && <OrganizationRiskRating />}
            <div className="riskExposure--map">
              <CustomGoogleMap
                googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyCYOTLyn6xbysArlvlW4zpxnH9p6H3An08&v=3.exp&libraries=geometry,drawing,places"
                loadingElement={<div style={{ height: `37vh` }} />}
                containerElement={
                  <div style={{ height: `37vh`, position: 'relative' }} />
                }
                mapElement={<div style={{ height: `37vh` }} />}
                locations={
                  (locations && locations.length > 0 && locations) || []
                }
                customIcon={mappedIcons}
                customLocationIcon={customLocationIcon}
              />
            </div>
          </div>
          <div className="riskExposure--wrap">
            {totalEvents && totalLocations && <TotalData />}
            {topics && topics.length && <Topics />}
            {topRisks && topRisks.length && <TopRisks />}
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    riskExposure: state.riskExposure,
    themeChange: state.themeChange,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RiskExposure);
