export function getBarChartData(data) {
  let xData = [];
  let yData = [];
  data.map(item => {
    xData.push(item[0]);
    yData.push(item[1]);
  });
  return { xData, yData };
}