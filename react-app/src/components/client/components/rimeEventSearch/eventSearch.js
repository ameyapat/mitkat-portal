import React, { Component } from 'react';
import {
  downloadRimeEventSearchExcel,
  getRimeEventSearchFeed,
} from '../../../../api/api';
import { connect } from 'react-redux';
import Filters from './components/Filters';
import './eventSearch.scss';
import RiskCategoryChart from './components/RiskCategoryChart';
import PieChart from './components/PieChart';
import HeatMap from './components/HeatMap';
import EventList from './components/EventList';
import Paper from '@material-ui/core/Paper';
import { searchFeed, updateSearchFeed } from '../../../../actions/rimeActions';
import store from '../../../../store';
import EventTimeLine from './components/EventTimeLine';
import EventArchives from '../rimeFilters/EventArchives';
import * as actions from '../../../../actions';
import notFound from '../../../../assets/No-Events-Found.png';
import { Drawer, Tooltip, withStyles } from '@material-ui/core';
import SearchBar from './components/SearchBar';
import downloadIcon from '../../../../assets/download.png';
import { toggleEnableSearch } from '../../../../actions/riskTrackerAction';
import { toggleSearchBar } from '../../../../actions/searchActions';
import LocationChart from './components/LocationChart';
import PersonalityChart from './components/PersonalityChart';
import OrganizationChart from './components/OrganizationChart';
import SubRiskCategoryChart from './components/SubRiskCategoryChart';

const drawerWidth = '100vw';
const drawerHeight = '200px';

const styles = theme => ({
  paperAnchorTop: {
    overflowX: 'hidden',
    overflowY: 'scroll',
    width: drawerWidth,
    height: 'auto',
    minHeight: drawerHeight,
    maxHeight: '92vh',
    marginTop: '8vh',
    backgroundColor: 'var(--filterBackground)',
    '&::-webkit-scrollbar': {
      width: 3,
    },
    '&::-webkit-scrollbar-track': {
      background: 'transparent',
    },
    '&::-webkit-scrollbar-thumb': {
      backgroundColor: '#ccc',
      borderRadius: 5,
    },
  },
  test: {
    background: 'none',
  },
});

@withStyles(styles, { withTheme: true })
@connect(
  state => {
    return {
      rime: state.rime,
      themeChange: state.themeChange,
    };
  },
  dispatch => {
    return {
      dispatch,
    };
  },
)
class NewEventSearch extends Component {
  state = {
    feed: [],
    timeLine: [],
    updatedFeed: [],
    riskCategories: [],
    relevancy: [],
    address: [],
    addressLabel: [],
    operations: 'OR',
    manualTags: '',
    startDate: '',
    endDate: '',
    loaderFlag: true,
    EventLoaded: 21,
    feedForList: '',
    showDrawer: true,
    language: [],
  };

  componentDidMount = () => {
    const { rime } = this.props;
    const {
      eventSearchriskCategories,
      eventSearchrelevancy,
      eventSearchselectAllRiskCat,
      eventSearchselectAllRelevancy,
      eventSearchlocations,
      eventSearchtags,
      eventSearchoperations,
      eventSearchStartDate,
      eventSearchEndDate,
      eventSearchLanguage,
    } = rime;
    this.setState({
      riskCategories: eventSearchriskCategories,
      relevancy: eventSearchrelevancy,
      selectAllRiskCatChecked: eventSearchselectAllRiskCat,
      selectAllRelevancyChecked: eventSearchselectAllRelevancy,
      selectedlocations: eventSearchlocations,
      manualTags: eventSearchtags,
      startDate: eventSearchStartDate,
      endDate: eventSearchEndDate,
      address: eventSearchlocations,
      operations: eventSearchoperations,
      language: eventSearchLanguage,
    });
  };

  componentDidUpdate(prevProps) {
    const { rime } = this.props;
    const {
      eventSearchriskCategories,
      eventSearchrelevancy,
      eventSearchselectAllRiskCat,
      eventSearchselectAllRelevancy,
      eventSearchlocations,
      eventSearchtags,
      eventSearchoperations,
      eventSearchStartDate,
      eventSearchEndDate,
      eventSearchLanguage,
    } = rime;
    if (prevProps.rime !== this.props.rime) {
      this.setState({
        riskCategories: eventSearchriskCategories,
        relevancy: eventSearchrelevancy,
        selectAllRiskCatChecked: eventSearchselectAllRiskCat,
        selectAllRelevancyChecked: eventSearchselectAllRelevancy,
        selectedlocations: eventSearchlocations,
        manualTags: eventSearchtags,
        startDate: eventSearchStartDate,
        endDate: eventSearchEndDate,
        address: eventSearchlocations,
        operations: eventSearchoperations,
        language: eventSearchLanguage,
      });
    }
  }

  handleDownloadOptions = () => {
    const { rime } = this.props;
    const {
      eventSearchriskCategories,
      eventSearchrelevancy,
      eventSearchtags,
      eventSearchoperations,
      eventSearchStartDate,
      eventSearchEndDate,
      eventSearchLanguage,
      eventSearchlocations,
    } = rime;

    let addressLabel = this.getAddress(eventSearchlocations);
    let manualTaglist = this.getManualTags(eventSearchtags);

    let reqObj = {
      riskCategory: eventSearchriskCategories,
      riskLevel: eventSearchrelevancy,
      address: addressLabel,
      tags: manualTaglist,
      operators: eventSearchoperations,
      beginDate: eventSearchStartDate,
      endDate: eventSearchEndDate,
      mode: '',
      language: eventSearchLanguage,
    };
    downloadRimeEventSearchExcel(reqObj);
  };

  handleToggleSearch = show => {
    store.dispatch(toggleEnableSearch(show));
    store.dispatch(toggleSearchBar(show));
  };

  toggleEventArchives = () =>
    this.props.dispatch(actions.toggleRimeEventArchives(false));

  initFetch = () => {
    this.handleRimeEventSearchFeed();
  };

  clearFeed = () => {
    this.setState({ updatedFeed: [], feed: [], loaderFlag: true });
  };

  getAddress = address => {
    let addressLabel = [];
    address.length > 0
      ? address.map(subitem => {
          addressLabel.push(subitem.label);
        })
      : addressLabel.push('');
    return addressLabel;
  };

  getManualTags = manualTags => {
    let tagList = manualTags ? manualTags.split(',') : [''];
    return tagList;
  };

  handleRimeEventSearchFeed = () => {
    const {
      riskCategories,
      relevancy,
      feed,
      operations,
      startDate,
      endDate,
      language,
      manualTags,
      address,
    } = this.state;

    let addressLabel = this.getAddress(address);
    let manualTaglist = this.getManualTags(manualTags);

    let reqObj = {
      riskCategory: riskCategories,
      riskLevel: relevancy,
      address: addressLabel,
      tags: manualTaglist,
      operators: operations,
      beginDate: startDate,
      endDate: endDate,
      mode: 'View',
      language: language,
    };

    getRimeEventSearchFeed(reqObj).then(data => {
      store.dispatch(
        searchFeed(data.events?.length ? [...data.events].reverse() : []),
      );
      store.dispatch(
        updateSearchFeed(data.events?.length ? [...data.events].reverse() : []),
      );
      this.setState({
        feed: data.events?.length ? [...data.events].reverse() : [],
        timeLine: data.timeline,
        loaderFlag: false,
        showDrawer: false,
      });
    });
  };

  handleDrawer = () => {
    this.setState({ showDrawer: true });
  };

  handleDrawerClose = () => {
    this.setState({
      showDrawer: false,
    });
  };

  render() {
    const { feed, timeLine, loaderFlag, showDrawer } = this.state;
    const {
      classes,
      themeChange: { setDarkTheme },
      rime: { showRimeEventArchives },
    } = this.props;
    return (
      <>
        <Drawer
          variant="top"
          classes={{
            root: classes.rootDrawer,
            paperAnchorTop: classes.paperAnchorTop,
            modal: classes.test,
          }}
          anchor="top"
          open={showDrawer}
          className={`${setDarkTheme ? 'dark' : 'light'}`}
        >
          <Filters
            handleRimeFeed={this.handleRimeEventSearchFeed}
            clearFeed={this.clearFeed}
            closeDrawer={() => this.handleDrawerClose()}
          />
        </Drawer>
        <div
          id="rime__eventsearch"
          className={` ${setDarkTheme ? 'dark' : 'light'}`}
        >
          <div className="event__navBar">
            <div className="left__navBar">
              <Tooltip title="Filter">
                <button
                  className="btnEllipsis"
                  onClick={() => {
                    this.handleDrawer();
                  }}
                >
                  <i className="fas fa-filter"></i>
                </button>
              </Tooltip>
              {feed?.length && (
                <h3 className="totalResult">
                  Events Found: &nbsp;
                  {this.props.rime.searchFeed.length
                    ? this.props.rime.searchFeed.length
                    : '0'}
                </h3>
              )}
            </div>
            {feed?.length && (
              <div className="right__navBar">
                <div className="eventsearch_searchbar">
                  <SearchBar
                    placeholder="Search by keyword"
                    onClose={() => this.handleToggleSearch(false)}
                  />
                </div>
                <Tooltip title="Download">
                  <button
                    className="btnEllipsis"
                    onClick={() => {
                      this.handleDownloadOptions();
                    }}
                  >
                    <i class="fa fa-download" aria-hidden="true"></i>
                  </button>
                </Tooltip>
              </div>
            )}
          </div>
          {feed?.length > 0 ? (
            <div className="rime_eventsearchbody">
              <div className="eventSearch_dashboard">
                <div className="eventsearch__body__row">
                  <div className="eventsearch__inner__row2">
                    <div className="eventsearch__title">
                      Relevancy Distribution Chart
                    </div>
                    <div className="eventsearch__piechart">
                      <Paper elevation={0}>
                        <PieChart feed={feed} />
                      </Paper>
                    </div>
                  </div>
                  <div className="eventsearch__inner__row1">
                    <div className="eventsearch__title_heatmap">
                      Event HeatMap
                    </div>
                    <div className="eventsearch__heatmap">
                      <Paper>
                        <HeatMap
                          feed={feed}
                          googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyCYOTLyn6xbysArlvlW4zpxnH9p6H3An08&v=3.exp&libraries=geometry,drawing,places,visualization"
                          loadingElement={<div style={{ height: `100%` }} />}
                          containerElement={
                            <div
                              style={{
                                height: `210px`,
                                position: 'relative',
                              }}
                            />
                          }
                          mapElement={<div style={{ height: `100%` }} />}
                        />
                      </Paper>
                    </div>
                  </div>
                  <div className="eventSearch__paper__spacing__right"></div>
                </div>
                <div className="fullWidthChart">
                  <div className="eventsearch__title">
                    Risk Category Distribution Chart
                  </div>
                  <div className="eventsearch__barchart">
                    <Paper elevation={0} className="eventsearch__paper">
                      <RiskCategoryChart feed={feed} />
                    </Paper>
                  </div>
                </div>
                <div className="fullWidthChart">
                  <div className="eventsearch__title"> Event Timeline</div>
                  <div className="eventsearch__timeline">
                    <Paper elevation={0}>
                      <EventTimeLine timeline={timeLine} />
                    </Paper>
                  </div>
                </div>
                <div className="fullWidthChart">
                  <div className="eventsearch__title">
                    Location Distribution Chart
                  </div>
                  <Paper>
                    <LocationChart feed={feed} />
                  </Paper>
                </div>
                <div className="fullWidthChart">
                  <div className="eventsearch__title">
                   Subrisk Category Distribution Chart
                  </div>
                  <Paper>
                    <SubRiskCategoryChart feed={feed} />
                  </Paper>
                </div>
                <div className="fullWidthChart">
                  <div className="eventsearch__title">
                    Personality Distribution Chart
                  </div>
                  <Paper>
                    <PersonalityChart feed={feed} />
                  </Paper>
                </div>
                <div className="fullWidthChart">
                  <div className="eventsearch__title">
                    Organization Distribution Chart
                  </div>
                  <Paper>
                    <OrganizationChart feed={feed} />
                  </Paper>
                </div>
              </div>
              <div className="eventSearch_right_container">
                <div className="eventsearch__list">
                  <EventList feed={feed} />
                </div>
              </div>
            </div>
          ) : (
            <div>
              <div className="event--search--no-found">
                <img
                  src={notFound}
                  alt="No Event Found"
                  className="event--search--no-found-image"
                />
              </div>
            </div>
          )}
          <Drawer
            variant="top"
            anchor="top"
            classes={{
              root: classes.rootDrawer,
              paperAnchorTop: classes.paperAnchorTop,
              modal: classes.test,
            }}
            open={showRimeEventArchives}
            className={`${setDarkTheme ? 'dark' : 'light'}`}
          >
            <EventArchives toggleEventArchives={this.toggleEventArchives} />
          </Drawer>
        </div>
      </>
    );
  }
}

export default NewEventSearch;
