import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';

import { searchRimeEvents } from '../../../../../../helpers/utils';
import {
  searchFeed,
  updateSearchFeed,
} from '../../../../../../actions/rimeActions';
import './style.scss';
import searchIcon from '../../../../../../assets/svg/searchIcon.svg';

const SearchBar = ({ placeholder = 'type to search', onClose }) => {
  const feed = useSelector(store => store.rime.searchFeed);
  const updateFeed = useSelector(store => store.rime.updateSearchFeed);
  const textInput = React.useRef();
  const history = useHistory();
  const dispatch = useDispatch();

  const handleSearch = e => {
    let value = e.target.value;
    if (value) {
      if (history.location.pathname === '/client/rime/eventSearch') {
        let filteredEventDetails = searchRimeEvents(value, updateFeed);
        dispatch(searchFeed(filteredEventDetails));
      } else {
        let filteredEventDetails = searchRimeEvents(value, feed);
        dispatch(searchFeed(filteredEventDetails));
      }
    } else {
      dispatch(searchFeed(updateFeed));
    }
  };

  const handleOnClose = () => {
    textInput.current.value = '';
    dispatch(searchFeed(updateFeed));
    onClose();
  };

  return (
    <div id="rimeSearchBar">
      <div className="inputWrapper">
        <img className="search_icon" src={searchIcon} />
        <input
          type="text"
          placeholder={placeholder}
          onChange={handleSearch}
          ref={textInput}
        />
        <span className="inputBtnWrapper">
          <button onClick={handleOnClose}>&times;</button>
        </span>
      </div>
    </div>
  );
};

export default SearchBar;
