import React, { Component } from 'react';
import Chart from 'react-google-charts';
import './style.scss';

class PieChart extends Component {
  state = {
    relevancyLow: 0,
    relevancyMedium: 0,
    relevancyHigh: 0,
    totalEvents: 0,
  };

  componentDidMount = () => {
    const { feed } = this.props;
    let { relevancyLow, relevancyMedium, relevancyHigh } = this.state;
    feed.length > 0 &&
      feed.map((item, key) => {
        if (item.eventRiskLevel === 1) {
          relevancyLow++;
        }
        if (item.eventRiskLevel === 2) {
          relevancyMedium++;
        }
        if (item.eventRiskLevel === 3) {
          relevancyHigh++;
        }
      });
    this.setState({
      relevancyLow: relevancyLow,
      relevancyMedium: relevancyMedium,
      relevancyHigh: relevancyHigh,
      totalEvents: relevancyHigh + relevancyLow + relevancyMedium,
    });
  };

  render() {
    let { relevancyLow, relevancyMedium, relevancyHigh } = this.state;

    return (
      <div className="eventSearch__piechart">
        {
          <Chart
            className="piechart-wrapper"
            chartType="PieChart"
            loader={<div>Loading Relevancy Data Pie Chart</div>}
            data={[
              ['Relevancy', 'Events'],
              ['High Risk', relevancyHigh],
              ['Medium Risk', relevancyMedium],
              ['Low Risk', relevancyLow],
            ]}
            options={{
              labels: ['High Risk', 'Medium Risk', 'Low Risk'],
              colors: ['#E35A5A', '#E3A35A', '#F3D66E'],
              is3D: true,
              legend: {
                position: 'bottom',
                textStyle: { color: '#969A9C' },
              },
            }}
          />
        }
      </div>
    );
  }
}

export default PieChart;
