import React, { useEffect, useState } from 'react';
import './style.scss';
import moment from 'moment';
import {
  categories,
  subRiskCategories,
} from '../../../rimeDashboard/helpers/constants';
// common style
import '../../../../sass/style.scss';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { useSelector } from 'react-redux';
import CustomModal from '../../../../../ui/modal';
import ArticleModal from '../../../rimeEventCard/articleModal';
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye';
import { lastUpdateDate, eDate } from '../../../rimeDashboard/helpers/utils';
import tagsIcon from '../../../../../../assets/svg/tagsIcon.svg';

const pinMappingRgba = {
  1: 'low',
  2: 'medium',
  3: 'high',
};

const EventCard = props => {
  const {
    title,
    categoryId,
    secondaryCatergoryId,
    subRiskCategoryId,
    link,
    eventDate,
    relevancy,
    description,
    sourceLinks,
    location,
    noOfArticles,
    lastUpdatedDate,
    imgURL,
    tags,
    language,
  } = props;

  const [isNew, setIsNew] = useState(true);
  const [category, setCategory] = useState('');
  const [secondaryCategory, setSecondaryCategory] = useState('');
  const [subRiskCategory, setSubRiskCategory] = useState('');
  const [showModal, setShowModal] = useState(false);
  const [showTags, setShowTags] = useState(false);
  const setTheme = useSelector(state => state.themeChange.setDarkTheme);
  const refreshDuration = useSelector(state => state.rime.refreshDuration);
  const feed = useSelector(state => state.rime.searchFeed);

  const TIMEOUT = refreshDuration * 60000;

  const mapRiskCategory = () => {
    categories.map(subitem => {
      if (categoryId === subitem.id) {
        setCategory(subitem.label);
      }
    });
    subRiskCategories.map(item => {
      if (subRiskCategoryId === item.id) {
        setSubRiskCategory(item.label);
      }
    });
  };

  const handleModal = () => {
    setShowModal(false);
  };

  const showArticleModal = () => {
    setShowModal(true);
  };
  const showTagsDiv = () => {
    setShowTags(prevCheck => !prevCheck);
  };
  useEffect(() => {
    mapRiskCategory();
    setTimeout(() => {
      setIsNew(false);
    }, TIMEOUT);
  });

  let tag = tags && tags.split(',');
  let allLinks = sourceLinks && sourceLinks.split(',');

  const utcLastUpdateDate = lastUpdateDate(lastUpdatedDate);
  const utcDate = eDate(eventDate);

  return (
    <div id="eventsearch__card">
      <Card className={`rime_eventCardList ${pinMappingRgba[relevancy]}`}>
        <div className="eventsearch__header">
          <div
            className={`eventsearch__pill ${relevancy &&
              pinMappingRgba[relevancy]}`}
          >
            <Typography component="span" className="eventsearch__cat__pill">
              {category ? category : 'Not Available'}
              {subRiskCategoryId !== 0 && subRiskCategoryId !== null && <span className="eventsearch__cat__pill__space">,</span>}
              {subRiskCategoryId !== 0 && subRiskCategory}
            </Typography>
          </div>
          <div className="eventsearch__card__innercolumn">
            <div>
              <Typography component="span" className="rime_event_date">
                &nbsp;{moment(utcDate).format('Do MMM YYYY')}
              </Typography>
              <Typography
                variant="subtitle1"
                color="text.secondary"
                component="span"
                className="rimeupdate__date"
              >
                Updated:&nbsp;
              </Typography>
              <Typography component="span" className="rimeupdate__date">
                {moment(utcLastUpdateDate).format('Do MMM YYYY, h:mm:ss a')}
              </Typography>
            </div>
          </div>
        </div>
        <div className="eventsearch__card__row1">
          <div className="eventsearch__card__innercolumn2">
            <Box sx={{ display: 'flex', flexDirection: 'column' }}>
              <CardContent sx={{ flex: '1 0 auto' }}>
                <Typography
                  component="div"
                  className="eventsearch__eventtitle__wrap"
                  style={{ color: 'black' }}
                >
                  {title.substring(0, 79)}
                  {title.length > 79 ? '...' : ''}
                </Typography>
                <div className="eventsearch__description">
                  <Typography
                    component="div"
                    className="eventsearch__eventbody__wrap"
                  >
                    {description
                      ? description.substring(0, 110)
                      : 'Not Available'}
                    <span className="eventSearch--link">
                      {description && (
                        <a className="anchor__text" href={link} target="_blank">
                          ...read more
                        </a>
                      )}
                    </span>
                  </Typography>
                </div>
                <div className="eventcard_row_2">
                  <div className="language_section">
                    <Typography
                      variant="subtitle1"
                      color="text.secondary"
                      component="span"
                      className="eventsearch__eventbody__subHead"
                    >
                      Location:
                    </Typography>
                    <Typography
                      component="span"
                      className="eventsearch__eventbody__location_wrap"
                    >
                      &nbsp;{location ? location : 'Not Available'}
                    </Typography>
                  </div>
                  <div>
                    <Typography
                      variant="subtitle1"
                      color="text.secondary"
                      component="span"
                      className="eventsearch__eventbody__subHead"
                    >
                      Language:
                    </Typography>
                    <Typography
                      component="span"
                      className="eventsearch__eventbody__wrap"
                    >
                      &nbsp;{language ? language : 'Not Available'}
                    </Typography>
                  </div>
                </div>
              </CardContent>
            </Box>
          </div>
        </div>
        <div className="eventsearch__card__row2">
          <Box sx={{ display: 'flex', flexDirection: 'column' }}>
            <CardContent sx={{ flex: '1 0 auto' }}>
              <div className="rime__eventSearch__spacing"></div>
              <div className="card_footer">
                <div
                  className="article__content__pill"
                  onClick={() => showArticleModal()}
                >
                  <Typography
                    variant="subtitle1"
                    color="text.secondary"
                    component="span"
                    className="eventsearch__eventbody__Head"
                  >
                    <span className="title">No of Articles:</span>
                  </Typography>
                  <Typography
                    component="span"
                    className="eventsearch__eventbody__Head"
                  >
                    &nbsp;
                    {noOfArticles ? noOfArticles : 'Not Available'}
                  </Typography>
                  <span>&nbsp;</span>
                  <RemoveRedEyeIcon className="eyeIcon" />
                </div>
                <div
                  className="article__content__pill"
                  onClick={() => showTagsDiv()}
                >
                  <Typography
                    variant="subtitle1"
                    color="text.secondary"
                    component="span"
                    className="eventsearch__eventbody__Head"
                  >
                    <span className="title">Tags</span>
                  </Typography>
                  <span>&nbsp; &nbsp;</span>
                  <img className="tagsIcon" src={tagsIcon} />
                </div>
              </div>
              <div className="eventsearch__rime__tags">
                {showTags &&
                  tag?.length > 0 &&
                  tag.map(subitem => {
                    return <span>#{subitem}</span>;
                  })}
              </div>
            </CardContent>
          </Box>
        </div>
      </Card>
      <CustomModal showModal={showModal}>
        <div
          id="rimeEventArchivesModalCont"
          className={`modal__inner ${setTheme ? 'dark' : 'light'}`}
        >
          <ArticleModal
            alllinks={allLinks}
            closeEventModal={() => handleModal()}
          />
        </div>
      </CustomModal>
    </div>
  );
};

export default EventCard;
