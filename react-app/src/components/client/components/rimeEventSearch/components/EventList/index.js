import React, { useEffect } from 'react';
import EventCard from './eventCard';
import { useSelector } from 'react-redux';
import RotateRightIcon from '@mui/icons-material/RotateRight';
import { Button } from '@material-ui/core';

export default function EventList() {
  const [feedList, setFeedList] = React.useState('');
  const [feedListToShow, setFeedListToShow] = React.useState('');
  const [hideLoadMore, setHideLoadMore] = React.useState(false);
  const feed = useSelector(store => store.rime.searchFeed);

  useEffect(() => {
    setFeedList(feed);
    setFeedListToShow(feed.slice(0, 9));
  }, [feed]);

  const loadmore = () => {
    const visibleItemsCount = feedListToShow.length;
    const totalItems = feed.length;

    const datatoLoad = [
      ...feedListToShow,
      ...feed.slice(visibleItemsCount, visibleItemsCount + 21),
    ];

    const isAllItemsLoaded = datatoLoad.length === totalItems;

    setFeedListToShow(datatoLoad);
    setHideLoadMore(isAllItemsLoaded);
  };

  return (
    <>
      <div className="eventsearch__container">
        {feedListToShow?.length > 0 &&
          feedListToShow.map(item => {
            return (
              <EventCard
                key={item.eventId}
                title={item.eventTitle}
                description={item.eventDescription}
                sourceLinks={item.eventAllNewsLinks}
                location={item.eventLoc}
                noOfArticles={item.count}
                categoryId={item.eventPrimaryRiskCategory}
                secondaryCatergoryId={item.eventSecondaryRiskCategory}
                subRiskCategoryId={item.subRiskCategory}
                link={item.eventLink}
                hasClose={false}
                eventDate={item.eventDate}
                lastUpdatedDate={item.eventLatestUpdate}
                relevancy={item.eventRiskLevel}
                imgURL={item.eventImageUrl}
                tags={item.eventTags}
                language={item.language}
              />
            );
          })}
      </div>
      <div className="eventsearch__loadmore">
        {!hideLoadMore && feedList?.length > 21 && (
          <Button
            variant="contained"
            onClick={loadmore}
            startIcon={<RotateRightIcon />}
          >
            Load More
          </Button>
        )}
      </div>
    </>
  );
}
