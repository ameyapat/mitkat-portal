import React, { Component } from 'react';
import { connect } from 'react-redux';
import CustomBarChart from '../../../../../ui/customBarChart';
import { getBarChartData } from '../../helpers/utils';

@connect(
  state => {
    return {
      rime: state.rime,
      themeChange: state.themeChange,
    };
  },
  dispatch => {
    return {
      dispatch,
    };
  },
)
class PersonalityChart extends Component {
  state = {
    personalities: [],
    filteredPersonalities: [],
    xAxisData: [],
    yAxisData: [],
  };

  componentDidMount() {
    this.getPersonalities();
  }

  getPersonalities = () => {
    let { feed } = this.props;
    let { personalities } = this.state;
    feed.map(item => {
      let loc = item.personality && item.personality.split(' , ');
      loc?.length > 0 &&
        loc.map(subitem => {
          personalities.push(subitem);
        });
    });

    const result = personalities.reduce(
      (a, c) => a.set(c, (a.get(c) || 0) + 1),
      new Map(),
    );
    const numDescending = [...result].sort((a, b) => b[1] - a[1]);
    const unfilteredPersonalities = numDescending.slice(0, 15);
    this.setState({
      personalities,
      filteredPersonalities: unfilteredPersonalities,
    });
  };

  getAxisData = () => {
    const axisData = getBarChartData(this.state.filteredPersonalities);
    this.setState({
      yAxisData: axisData.yData,
      xAxisData: axisData.xData,
    });
  };

  render() {
    let { xAxisData, yAxisData, filteredPersonalities } = this.state;
    return (
      <div>
        {filteredPersonalities.length > 0 &&
          xAxisData.length === 0 &&
          yAxisData.length === 0 &&
          this.getAxisData()}
        <CustomBarChart
          chartLabel={'No of Events for Each Personality'}
          chartData={yAxisData}
          chartLabels={xAxisData}
        />
      </div>
    );
  }
}

export default PersonalityChart;
