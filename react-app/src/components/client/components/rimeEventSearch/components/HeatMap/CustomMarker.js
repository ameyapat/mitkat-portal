import React, { Component } from 'react';
import { Marker, InfoWindow } from 'react-google-maps';
import { withCookies } from 'react-cookie';

@withCookies
class CustomMarker extends Component {
  state = {
    coronaDetails: [],
    showInfoBox: false,
  };

  markerRef = React.createRef();

  handleShowInfoBox = show => {
    this.setState({ showInfoBox: show });
  };

  render() {
    let { lat, lng, address, customIcon } = this.props;
    const { showInfoBox } = this.state;

    return (
      <Marker
        position={{ lat, lng }}
        onMouseOver={() => this.handleShowInfoBox(true)}
        onMouseOut={() => this.handleShowInfoBox(false)}
        icon={customIcon}
        ref={this.markerRef}
        optimized={true}
      >
        {showInfoBox && (
          <React.Fragment>
            <InfoWindow onCloseClick={() => this.handleShowInfoBox(false)}>
              <div>
                <p className="alert--title">{address}</p>
              </div>
            </InfoWindow>
          </React.Fragment>
        )}
      </Marker>
    );
  }
}

export default CustomMarker;
