import React, { Component } from 'react';
import { withScriptjs, withGoogleMap, GoogleMap } from 'react-google-maps';
import HeatmapLayer from 'react-google-maps/lib/components/visualization/HeatmapLayer';
//helpers
import { withCookies } from 'react-cookie';
import { connect } from 'react-redux';
import customMapStyles from '../../helpers/universalMapStyles.json';
import customMapStylesLight from '../../helpers/universalMapStylesLight.json';

const google = window.google;

@withCookies
@withScriptjs
@withGoogleMap
@connect(state => {
  return {
    rime: state.rime,
    themeChange: state.themeChange,
  };
})
class HeatMap extends Component {
  state = {
    defaultOptions: {
      mapTypeControl: false,
      streetViewControl: false,
      zoomControl: false,
      fullscreenControl: false,
      draggable: true,
    },
  };

  setLatLng = () => {
    return this.props.feed.map((i, idx) => ({
      location: new google.maps.LatLng(i.event_lat, i.event_lon),
    }));
  };

  renderHeatMap = () => {
    const { feed } = this.props;
    return (
      <HeatmapLayer
        data={feed?.length > 0 ? this.setLatLng() : []}
        options={{ radius: 20 }}
      />
    );
  };

  render() {
    let { feed } = this.props;
    const { defaultOptions } = this.state;
    const {
      themeChange: { setDarkTheme },
    } = this.props;
    let lat = feed[0].event_lat;
    let lng = feed[0].event_lon;
    return (
      <>
        <GoogleMap
          zoom={3}
          options={{
            ...defaultOptions,
            styles: setDarkTheme ? customMapStyles : customMapStylesLight,
          }}
          defaultCenter={{
            lat,
            lng,
          }}
        >
          <>{this.renderHeatMap()}</>
        </GoogleMap>
      </>
    );
  }
}

export default HeatMap;
