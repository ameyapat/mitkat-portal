import React, { Component } from 'react';
import { connect } from 'react-redux';
import CustomBarChart from '../../../../../ui/customBarChart';
import { getBarChartData } from '../../helpers/utils';

@connect(
  state => {
    return {
      rime: state.rime,
      themeChange: state.themeChange,
    };
  },
  dispatch => {
    return {
      dispatch,
    };
  },
)
class OrganizationChart extends Component {
  state = {
    organizations: [],
    filteredOrganizations: [],
    xAxisData: [],
    yAxisData: [],
  };

  componentDidMount() {
    this.getOrganizations();
  }

  getOrganizations = () => {
    let { feed } = this.props;
    let { organizations } = this.state;
    feed.map(item => {
      let loc = item.organization && item.organization.split(' , ');
      loc?.length > 0 &&
        loc.map(subitem => {
          organizations.push(subitem);
        });
    });

    const result = organizations.reduce(
      (a, c) => a.set(c, (a.get(c) || 0) + 1),
      new Map(),
    );
    const numDescending = [...result].sort((a, b) => b[1] - a[1]);
    const unfilteredOrganizations = numDescending.slice(0, 15);
    this.setState({
      organizations,
      filteredOrganizations: unfilteredOrganizations,
    });
  };

  getAxisData = () => {
    const axisData = getBarChartData(this.state.filteredOrganizations);
    this.setState({
      yAxisData: axisData.yData,
      xAxisData: axisData.xData,
    });
  };

  render() {
    let { xAxisData, yAxisData, filteredOrganizations } = this.state;
    return (
      <div>
        {filteredOrganizations.length > 0 &&
          xAxisData.length === 0 &&
          yAxisData.length === 0 &&
          this.getAxisData()}
        <CustomBarChart
          chartLabel={'No of Events for Each Organization'}
          chartData={yAxisData}
          chartLabels={xAxisData}
        />
      </div>
    );
  }
}

export default OrganizationChart;
