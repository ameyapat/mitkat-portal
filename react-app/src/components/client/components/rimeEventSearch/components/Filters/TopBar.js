import React, { Component } from 'react';
import MultiSelect from '../../../../../ui/multiSelect';
import { multipleAddress } from '../../../rimeDashboard/helpers/multipleAddress';
import { withStyles, TextField, FormControl, Button } from '@material-ui/core';
import styles from './FiltersStyles';
import CustomCheckboxUi from '../../../../../ui/customCheckboxUi';
import CustomRelevancyCheckbox from '../../../../../ui/customCheckboxUi/CustomRelevancyCheckbox';
import {
  categories,
  relevancyConstants,
  languageConstants,
} from '../../../rimeDashboard/helpers/constants';
import store from '../../../../../../store';
import { toggleIsFetch } from '../../../../../../actions/';
import {
  updateStartDate,
  updateEndDate,
  updateEventSearchSelectedCategories,
  updateEventSearchSelectedRelevancy,
  updateEventSearchSelectedLocations,
  updateEventSearchOperations,
  updateEventSearchTags,
  updateEventSearchLanguage,
  handleEventSearchSelectAllRiskCat,
  handleEventSearchSelectAllRelevancy,
  handleEventSearchSelectAllLanguage,
  updateEventSearchStartDate,
  updateEventSearchEndDate,
} from '../../../../../../actions/rimeActions';
import { connect } from 'react-redux';
import { withCookies } from 'react-cookie';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import * as actions from '../../../../../../actions';
import './style.scss';
import { DatePicker } from 'material-ui-pickers';
import moment from 'moment';
import CancelIcon from '@mui/icons-material/Cancel';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import { operationalValue } from '../../../rimeSubscriptions/helpers/utils';

@withStyles(styles)
@connect(
  state => {
    return { rime: state.rime, themeChange: state.themeChange };
  },
  dispatch => {
    return { dispatch: dispatch };
  },
)
@withCookies
class TopBar extends Component {
  state = {
    selectedAddress: '',
    address: '',
    location: {},
    riskCategories: [],
    totalHours: 1,
    relevancy: [],
    isCatSelected: true,
    selectAllRiskCatChecked: true,
    selectAllRelevancyChecked: true,
    selectAllLanguageChecked: true,
    showRiskCategories: false,
    showRelevancy: false,
    showLanguages: false,
    selectedlocations: [],
    allLocations: { multipleAddress },
    operations: 'OR',
    manualTags: '',
    startDate: '',
    endDate: '',
    catArray: [],
    relArray: [],
    language: [],
  };

  categoriesRef = React.createRef();

  componentDidMount() {
    const { rime } = this.props;
    let cArray = [];
    let rArray = [];
    categories.map(item => {
      cArray.push(item.id);
    });
    relevancyConstants.map(item => {
      rArray.push(item.id);
    });
    let operatorValue = operationalValue(rime.eventSearchoperations);
    this.setState({
      riskCategories: rime.eventSearchriskCategories,
      relevancy: rime.eventSearchrelevancy,
      selectAllRiskCatChecked: rime.eventSearchSelectAllRiskCat,
      selectAllRelevancyChecked: rime.eventSearchSelectAllRelevancy,
      selectAllLanguageChecked: rime.eventSearchSelectAllLanguage,
      selectedlocations: rime.eventSearchlocations,
      manualTags: rime.eventSearchtags,
      language: rime.eventSearchLanguage,
      selectedAddress: rime.eventSearchlocations,
      startDate: rime.eventSearchStartDate,
      endDate: rime.eventSearchEndDate,
      operations: operatorValue,
      catArray: cArray,
      relArray: rArray,
    });
  }

  componentDidUpdate(prevProps) {
    const { rime } = this.props;
    let operatorValue = operationalValue(rime.eventSearchoperations);
    if (this.props.rime !== prevProps.rime) {
      this.setState({
        riskCategories: rime.eventSearchriskCategories,
        relevancy: rime.eventSearchrelevancy,
        selectAllRiskCatChecked: rime.eventSearchSelectAllRiskCat,
        selectAllRelevancyChecked: rime.eventSearchSelectAllRelevancy,
        selectAllLanguageChecked: rime.eventSearchSelectAllLanguage,
        selectedlocations: rime.eventSearchlocations,
        language: rime.eventSearchLanguage,
        manualTags: rime.eventSearchtags,
        startDate: rime.eventSearchStartDate,
        endDate: rime.eventSearchEndDate,
        operations: operatorValue,
      });
    }
  }

  handleApplyFilters = () => {
    this.props.handleRimeFeed();
  };

  toggleFilters = () => this.props.dispatch(actions.toggleRimeFilters(false));

  updateCategories = id => {
    const { riskCategories, catArray } = this.state;
    if (riskCategories.includes(id)) {
      let index = riskCategories.indexOf(id);
      riskCategories.splice(index, 1);
    } else {
      riskCategories.push(id);
    }

    if (riskCategories.length === Math.max(...catArray) + 1) {
      this.setState({ selectAllRiskCatChecked: true }, () => {
        store.dispatch(
          handleEventSearchSelectAllRiskCat(this.state.selectAllRiskCatChecked),
        );
      });
    } else {
      this.setState({ selectAllRiskCatChecked: false }, () => {
        store.dispatch(
          handleEventSearchSelectAllRiskCat(this.state.selectAllRiskCatChecked),
        );
      });
    }
    this.props.dispatch(updateEventSearchSelectedCategories(riskCategories));
  };

  updateRelevancy = id => {
    const { relevancy, relArray } = this.state;
    if (relevancy.includes(id)) {
      let index = relevancy.indexOf(id);
      relevancy.splice(index, 1);
    } else {
      relevancy.push(id);
    }

    if (relevancy.length === Math.max(...relArray)) {
      this.setState({ selectAllRelevancyChecked: true }, () => {
        store.dispatch(
          handleEventSearchSelectAllRelevancy(
            this.state.selectAllRelevancyChecked,
          ),
        );
      });
    } else {
      this.setState({ selectAllRelevancyChecked: false }, () => {
        store.dispatch(
          handleEventSearchSelectAllRelevancy(
            this.state.selectAllRelevancyChecked,
          ),
        );
      });
    }
    this.props.dispatch(updateEventSearchSelectedRelevancy(relevancy));
  };

  updateLanguage = id => {
    const { language } = this.state;
    if (language.includes(id)) {
      let index = language.indexOf(id);
      language.splice(index, 1);
    } else {
      language.push(id);
    }

    if (language.length === 11) {
      this.setState({ selectAllLanguageChecked: true }, () => {
        store.dispatch(
          handleEventSearchSelectAllLanguage(
            this.state.selectAllLanguageChecked,
          ),
        );
      });
    } else {
      this.setState({ selectAllLanguageChecked: false }, () => {
        store.dispatch(
          handleEventSearchSelectAllLanguage(
            this.state.selectAllLanguageChecked,
          ),
        );
      });
    }

    this.props.dispatch(updateEventSearchLanguage(language));
  };

  handleRiskCatSelectAll = e => {
    const { rime } = this.props;

    for (let key in rime.eventSearchriskCategories) {
      rime.eventSearchriskCategories[key] = e.target.checked;
    }

    this.setState({ selectAllRiskCatChecked: e.target.checked }, () => {
      store.dispatch(
        handleEventSearchSelectAllRiskCat(this.state.selectAllRiskCatChecked),
      );
      store.dispatch(
        updateEventSearchSelectedCategories(
          this.state.selectAllRiskCatChecked
            ? [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
            : [],
        ),
      );
    });
  };

  handleRelevancySelectAll = e => {
    const { rime } = this.props;

    for (let key in rime.eventSearchrelevancy) {
      rime.eventSearchrelevancy[key] = e.target.checked;
    }

    this.setState({ selectAllRelevancyChecked: e.target.checked }, () => {
      store.dispatch(
        handleEventSearchSelectAllRelevancy(
          this.state.selectAllRelevancyChecked,
        ),
      );
      store.dispatch(
        updateEventSearchSelectedRelevancy(
          this.state.selectAllRelevancyChecked ? [1, 2, 3] : [],
        ),
      );
    });
  };

  handleLanguageSelectAll = e => {
    const { rime } = this.props;

    for (let key in rime.eventSearchLanguage) {
      rime.eventSearchLanguage[key] = e.target.checked;
    }

    this.setState({ selectAllLanguageChecked: e.target.checked }, () => {
      store.dispatch(
        handleEventSearchSelectAllLanguage(this.state.selectAllLanguageChecked),
      );
      store.dispatch(
        updateEventSearchLanguage(
          this.state.selectAllLanguageChecked
            ? [
                'en',
                'hi',
                'zh',
                'ar',
                'es',
                'ur',
                'mr',
                'fr',
                'bn',
                'pt',
                'id',
                'ml',
                'ta',
                'kn',
                'ms',
                'th',
                'vi',
              ]
            : [],
        ),
      );
    });
  };

  handleMultiSelect = value => {
    this.setState({ selectedAddress: value }, () => {
      store.dispatch(
        updateEventSearchSelectedLocations(
          value.length > 0 && this.state.selectedAddress,
        ),
      );
    });
    store.dispatch(
      updateEventSearchSelectedLocations(
        value.length === 0 && [this.state.allLocations],
      ),
    );
  };

  handleOperationsChange = (event, newAlignment) => {
    this.setState({ operations: newAlignment ? newAlignment : 'OR' }, () => {
      store.dispatch(
        updateEventSearchOperations(newAlignment ? newAlignment : 'OR'),
      );
    });
  };

  handleUpdateTags = event => {
    this.setState({ manualTags: event }, () => {
      store.dispatch(updateEventSearchTags(event ? event : ''));
    });
  };

  handleApplyFilters = () => {
    this.props.dispatch(toggleIsFetch(true));
    this.props.handleRimeFeed();
    this.props.clearFeed();
    this.toggleFilters();
  };

  handleDateChange = (type, value) => {
    this.setState({ [type]: value }, () => {
      type == 'startDate'
        ? store.dispatch(
            updateEventSearchStartDate(moment(value).format('MM/DD/YYYY')),
          )
        : store.dispatch(
            updateEventSearchEndDate(moment(value).format('MM/DD/YYYY')),
          );
    });
  };

  render() {
    const {
      classes,
      themeChange: { setDarkTheme },
    } = this.props;
    const {
      riskCategories,
      relevancy,
      showRiskCategories,
      showRelevancy,
      operations,
      manualTags,
      startDate,
      endDate,
      selectedAddress,
      selectAllRiskCatChecked,
      showLanguages,
      language,
    } = this.state;

    return (
      <div className="eventSearch__filterDrawer">
        <div className="srchBtn">
          <Button
            variant="contained"
            endIcon={<CancelIcon />}
            className="eventSearch__CancelBtn"
            onClick={() => this.props.closeDrawer()}
          >
            Cancel
          </Button>
          <Button
            variant="contained"
            endIcon={<ArrowForwardIcon />}
            className="eventSearchStyle"
            onClick={() => this.handleApplyFilters()}
          >
            Search Events
          </Button>
        </div>
        <div className="eventSearchFilter__row">
          <div className="filter__mLocations">
            <div className="eventSearch__multipleLocationsParent">
              <div className="eventSearch__multipleLocations">
                <MultiSelect
                  value={selectedAddress}
                  placeholder={'Multiple Locations'}
                  parentEventHandler={value => this.handleMultiSelect(value)}
                  options={multipleAddress}
                  allowSelectAll={false}
                  className="multiple_EventLocations"
                />
              </div>
            </div>
          </div>
          <div className="filter__otherFilters">
            <div className="filter__subRow1">
              <div className="eventSearch__toggleOperations">
                <ToggleButtonGroup
                  color="primary"
                  value={operations}
                  exclusive
                  onChange={this.handleOperationsChange}
                >
                  <ToggleButton value="OR">OR</ToggleButton>
                  <ToggleButton value="AND">AND</ToggleButton>
                </ToggleButtonGroup>
              </div>
              <div className="eventSearchFilter__tagsDuration">
                <div className="rime__tags">
                  <TextField
                    id="standard-number"
                    label="Tags (Separate with Comma)"
                    variant="standard"
                    type="text"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    value={manualTags}
                    onChange={e => this.handleUpdateTags(e.target.value)}
                  />
                </div>
                <div className="rimeFilter__Language">
                  <div
                    className={`${showLanguages &&
                      'selectAllEventCatsWhiteRisk'}`}
                  >
                    <div className="selectAllEventCats">
                      {showLanguages ? (
                        <label
                          className={classes.detailsTitleWhite}
                          onClick={() =>
                            this.setState({
                              showLanguages: !showLanguages,
                            })
                          }
                        >
                          <span>
                            <i
                              className="fa fa-globe"
                              style={{
                                color: 'var(--whiteDisabled)',
                                paddingRight: '5px',
                              }}
                            ></i>
                          </span>
                          Language
                        </label>
                      ) : (
                        <label
                          className={classes.detailsTitle}
                          onClick={() =>
                            this.setState({
                              showLanguages: !showLanguages,
                            })
                          }
                        >
                          <span>
                            <i
                              className="fa fa-globe"
                              style={{
                                color: 'var(--whiteDisabled)',
                                paddingRight: '5px',
                              }}
                            ></i>
                          </span>
                          Language
                        </label>
                      )}
                      <span
                        className="filtersWrapBox__arrow"
                        onClick={() =>
                          this.setState({
                            showLanguages: !showLanguages,
                          })
                        }
                      >
                        {showLanguages ? (
                          <i
                            className="fas fa-chevron-up"
                            style={{ color: 'var(--whiteDisabled)' }}
                          ></i>
                        ) : (
                          <i
                            className="fas fa-chevron-down"
                            style={{ color: 'var(--whiteDisabled)' }}
                          ></i>
                        )}
                      </span>
                    </div>
                    {showLanguages && (
                      <>
                        <div className="rimeFilter__languageChild">
                          <span style={{ margin: '0 10px' }}>
                            <FormControlLabel
                              classes={{
                                label: classes.label,
                              }}
                              control={
                                <Checkbox
                                  className={classes.size}
                                  checked={this.state.selectAllLanguageChecked}
                                  onClick={this.handleLanguageSelectAll}
                                  value={this.state.selectAllLanguageChecked}
                                  icon={
                                    <CheckBoxOutlineBlankIcon
                                      className={classes.sizeIcon}
                                    />
                                  }
                                  checkedIcon={
                                    <CheckBoxIcon
                                      className={classes.sizeIcon}
                                    />
                                  }
                                  style={{ color: 'black' }}
                                />
                              }
                              label={'Select All'}
                            />
                          </span>
                          {languageConstants.map(item => (
                            <CustomCheckboxUi
                              key={item.id}
                              id={item.id}
                              label={item.label}
                              updateCategories={this.updateLanguage}
                              isSelected={language.includes(item.id)}
                              name={item.name}
                              ref={this.categoriesRef}
                            />
                          ))}
                        </div>
                      </>
                    )}
                  </div>
                </div>
              </div>
            </div>
            <div className="filter__subRow2">
              <div className="eventSearchFilter__RiskCategory">
                <div
                  className={`${showRiskCategories &&
                    'selectAllEventCatsWhiteRisk'}`}
                >
                  <div className="selectAllEventCats">
                    {showRiskCategories ? (
                      <label
                        className={classes.detailsTitleWhite}
                        onClick={() =>
                          this.setState({
                            showRiskCategories: !showRiskCategories,
                          })
                        }
                      >
                        <span>
                          <i className="icon-uniE90E" aria-hidden="true"></i>
                        </span>
                        Risk Categories
                      </label>
                    ) : (
                      <label
                        className={classes.detailsTitle}
                        onClick={() =>
                          this.setState({
                            showRiskCategories: !showRiskCategories,
                          })
                        }
                      >
                        <span>
                          <i
                            className="icon-uniE90E"
                            aria-hidden="true"
                            style={{ color: 'var(--whiteDisabled)' }}
                          ></i>
                        </span>
                        Risk Categories
                      </label>
                    )}
                    <span
                      className="filtersWrapBox__arrow"
                      onClick={() =>
                        this.setState({
                          showRiskCategories: !showRiskCategories,
                        })
                      }
                    >
                      {showRiskCategories ? (
                        <i className="fas fa-chevron-up"></i>
                      ) : (
                        <i className="fas fa-chevron-down"></i>
                      )}
                    </span>
                  </div>
                  {showRiskCategories && (
                    <>
                      <div className="eventSearchFilter__RiskCategorychild">
                        <span style={{ margin: '0 10px' }}>
                          {/*  Inline style need to be removed in UX upgrade */}
                          <FormControlLabel
                            classes={{
                              label: classes.label,
                            }}
                            control={
                              <Checkbox
                                className={classes.size}
                                checked={selectAllRiskCatChecked}
                                onClick={this.handleRiskCatSelectAll}
                                value={selectAllRiskCatChecked}
                                icon={
                                  <CheckBoxOutlineBlankIcon
                                    className={classes.sizeIcon}
                                  />
                                }
                                checkedIcon={
                                  <CheckBoxIcon className={classes.sizeIcon} />
                                }
                                style={{ color: 'black' }}
                              />
                            }
                            label={'Select All'}
                          />
                        </span>
                        {categories.map(item => (
                          <CustomCheckboxUi
                            key={item.id}
                            id={item.id}
                            label={item.label}
                            updateCategories={this.updateCategories}
                            isSelected={riskCategories.includes(item.id)}
                            name={item.name}
                            ref={this.categoriesRef}
                          />
                        ))}
                      </div>
                    </>
                  )}
                </div>
                {/* </div> */}
              </div>
              <div className="eventSearchFilter__Relevancy">
                <div className="eventSearchFilter__RelevancyChild">
                  <div
                    className={`${showRelevancy &&
                      'selectAllEventCatsWhiteRisk'}`}
                  >
                    <div className="selectAllEventCats">
                      {showRelevancy ? (
                        <label
                          className={classes.detailsTitleWhite}
                          onClick={() =>
                            this.setState({ showRelevancy: !showRelevancy })
                          }
                        >
                          <span>
                            <i className="icon-uniE95E" aria-hidden="true"></i>
                          </span>
                          Relevancy
                        </label>
                      ) : (
                        <label
                          className={classes.detailsTitle}
                          onClick={() =>
                            this.setState({ showRelevancy: !showRelevancy })
                          }
                        >
                          <span>
                            <i className="icon-uniE95E" aria-hidden="true"></i>
                          </span>
                          Relevancy
                        </label>
                      )}
                      <span
                        className="filtersWrapBox__arrow"
                        onClick={() =>
                          this.setState({ showRelevancy: !showRelevancy })
                        }
                      >
                        {showRelevancy ? (
                          <i className="fas fa-chevron-up"></i>
                        ) : (
                          <i className="fas fa-chevron-down"></i>
                        )}
                      </span>
                    </div>
                    {showRelevancy && (
                      <>
                        <span style={{ margin: '0 10px' }}>
                          <FormControlLabel
                            classes={{
                              label: classes.label,
                            }}
                            control={
                              <Checkbox
                                className={classes.size}
                                checked={this.state.selectAllRelevancyChecked}
                                onClick={this.handleRelevancySelectAll}
                                value={this.state.selectAllRelevancyChecked}
                                icon={
                                  <CheckBoxOutlineBlankIcon
                                    className={classes.sizeIcon}
                                  />
                                }
                                checkedIcon={
                                  <CheckBoxIcon className={classes.sizeIcon} />
                                }
                              />
                            }
                            label={'Select All'}
                          />
                        </span>
                        {relevancyConstants.map(item => (
                          <CustomRelevancyCheckbox
                            key={item.id}
                            id={item.id}
                            label={item.label}
                            updateRelevancy={this.updateRelevancy}
                            isSelected={relevancy.includes(item.id)}
                            name={item.name}
                          />
                        ))}
                      </>
                    )}
                  </div>
                </div>
              </div>
              <div className="Event_RefreshDuration">
                <div className="date">
                  <DatePicker
                    //views={['day', 'month', 'year']}
                    label="Start Date"
                    //minDate={new Date('2017-01-01')}
                    value={startDate}
                    disableFuture={true}
                    onChange={value =>
                      this.handleDateChange('startDate', value)
                    }
                    renderInput={params => (
                      <TextField {...params} helperText={null} />
                    )}
                  />
                </div>
                <div className="date">
                  <DatePicker
                    //views={['day', 'month', 'year']}
                    label="End Date"
                    //minDate={new Date('2017-01-01')}
                    value={endDate}
                    disableFuture={true}
                    onChange={value => this.handleDateChange('endDate', value)}
                    renderInput={params => (
                      <TextField {...params} helperText={null} />
                    )}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default TopBar;
