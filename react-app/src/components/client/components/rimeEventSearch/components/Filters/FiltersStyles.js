import * as globalCSS from '../../../../../../sass/globalVariables.scss';

export default {
  inputLabelFocused: {
    color: 'green !important',
  },
  addtionalWrap: {},
  totalHoursText: {},
  rootFilters: {},
  rootTextInput: {
    padding: '5px',
    color: 'var(--whiteMediumEmp)',
  },
  rootInput: {
    padding: '10px',
    color: 'var(--whiteMediumEmp)',
  },
  notchedOutline: {
    borderWidth: '1px',
    borderColor: 'var(--whiteHighEmp) !important ',
  },
  formControl: {
    width: '100%',
    padding: '15px 0',
  },
  inputFormControl: {
    padding: 0,
  },
  inputLabelRoot: {
    color: 'var(--whiteHighEmp) !important ',
  },
  filterBtn: {
    textTransform: 'capitalize !important',
    border: '1px solid var(--darkActive)',
    background: 'transparent',
    color: 'var(--darkActive)',
    padding: '12px',
    cursor: 'pointer',
    borderRadius: '2px',
    fontWeight: '100',
    fontSize: '13px',
    width: '120px',

    '&:hover': {
      background: 'var(--darkActive)',
      color: 'var(--darkGrey)',
    },
  },
  detailsTitle: {
    color: `(--whiteDisabled) !important`,
    fontWeight: 400,
    padding: '5px 0',
    display: 'block',
    fontSize: 12,
    textTransform: 'capitalize',
    width: '100%',

    '& span': {
      fontSize: 18,
    },
  },
  detailsTitleWhite: {
    color: `var(--highlightText) !important`,
    fontWeight: 400,
    padding: '5px 0',
    display: 'block',
    fontSize: 12,
    textTransform: 'capitalize',
    width: '100%',

    '& span': {
      fontSize: 18,
    },
  },
  locationTextfield: {
    color: 'var(--whiteMediumEmp)',
  },
  searchIcon: {
    color: 'var(--whiteMediumEmp)',
  },
  suggestionsWrap: {},
  searchWrapper: {
    padding: 0,
  },
  inputSearchWrap: {
    // borderRadius: 3,
    position: 'relative',
    '& .location-search-input': {
      padding: '15px',
      borderRadius: '3px',
      border: 'none',
      width: '100%',
      fontSize: 14,
      color: '#333',
      outline: 'none',
    },
  },
  autoCompleteDropdown: {
    borderRadius: 5,
    position: 'absolute',
    top: '50px',
    left: 0,
    width: '100%',
    zIndex: 999,
  },
  activeListItem: {
    padding: 10,
    borderBottom: '1px solid #f1f1f1',
    fontSize: 14,
    '&. list--item': {
      color: '#333',
    },
  },
  suggestionItem: {
    padding: 10,
    borderBottom: '1px solid #f1f1f1',
    fontSize: 14,
    '&. list--item': {
      color: '#333',
    },
  },
  label: {
    color: 'var(--whiteMediumEmp)',
    fontSize: '0.98em',
  },
  size: {
    width: 40,
    height: 30,
  },
  sizeIcon: {
    fontSize: 20,
    color: 'rgba(171, 193, 66, 1)',
  },
  underline: {
    color: 'var(--lightText)',
    '&::hover': {
      '&::before': {
        borderBottom: '1px solid var(--lightText)',
      },
    },
    '&::before': {
      borderBottom: '1px solid var(--lightText)',
    },
    '&::after': {
      borderBottom: '1px solid var(--lightText)',
    },
  },
};