import React, { Component } from 'react';
import './style.scss';
import TopBar from './TopBar';

class Filters extends Component {
  render() {
    return (
      <>
        <TopBar
          handleRimeFeed={this.props.handleRimeFeed}
          clearFeed={this.props.clearFeed}
          closeDrawer={() => this.props.closeDrawer()}
        />
      </>
    );
  }
}

export default Filters;
