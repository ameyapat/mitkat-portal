import React, { Component } from 'react';
import Highcharts from 'highcharts/highstock';
import HighchartsReact from 'highcharts-react-official';
import './style.scss';

class EventTimeLine extends Component {
  state = {
    chartOptions: {
      chart: {
        type: 'area',
      },
      series: [
        {
          tooltip: {
            valueDecimals: 2,
          },
          data: [],
        },
      ],
      plotOptions: {
        series: {
          color: '#3a7cbd ',
          fillColor: {
            linearGradient: {
              x1: 0,
              y1: 0,
              x2: 0,
              y2: 1,
            },
            stops: [
              [0, Highcharts.getOptions().colors[0]],
              [
                1,
                Highcharts.Color(Highcharts.getOptions().colors[0])
                  .setOpacity(0)
                  .get('rgba(255, 255, 255, 0.5)'),
              ],
            ],
          },
        },
      },
    },
    hoverData: null,
  };

  componentDidMount() {
    this.initGraphData();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.timeline !== this.props.timeline) {
      this.initGraphData();
    }
  }

  setHoverData = e => {
    this.setState({ hoverData: e.target.category });
  };

  initGraphData = () => {
    let updatedRegionData = this.props.timeline;
    let updatedSeriesData = [];
    const timestampData = {
      data: [],
    };

    if (updatedRegionData.length > 0) {
      updatedRegionData.map(item => {
        const graphPointsData = [];
        graphPointsData.push(item.timestamp, item.eventsNumber);
        timestampData.data.push(graphPointsData);
      });
    }

    updatedSeriesData = [...timestampData.data];

    this.setState({
      chartOptions: {
        ...this.state.chartOptions,
        ...{ series: [{ data: [...updatedSeriesData] }] },
      },
    });
  };

  render() {
    const { chartOptions } = this.state;
    return (
      <div id="eventsearch__heatMapGraph">
        <div>
          <HighchartsReact
            highcharts={Highcharts}
            constructorType={'stockChart'}
            options={chartOptions}
          />
        </div>
      </div>
    );
  }
}

export default EventTimeLine;
