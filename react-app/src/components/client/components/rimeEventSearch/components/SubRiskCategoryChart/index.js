import React, { Component } from 'react';
import { connect } from 'react-redux';
import { subRiskCategories } from '../../../rimeDashboard/helpers/constants';
import CustomBarChart from '../../../../../ui/customBarChart';
import { getBarChartData } from '../../helpers/utils';

@connect(
  state => {
    return {
      rime: state.rime,
      themeChange: state.themeChange,
    };
  },
  dispatch => {
    return {
      dispatch,
    };
  },
)
class SubRiskCategoryChart extends Component {
  state = {
    personalities: [],
    filteredFeed: [],
    filteredSubRiskCat: [],
    subRiskChartData: [],
    filteredPersonalities: [],
    xAxisData: [],
    yAxisData: [],
  };

  componentDidMount() {
    this.getSubriskCategories();
  }

  getSubriskCategories = () => {
    let { feed } = this.props;
    let filterArray = [null, 1, 8, 16, 29, 37, 50, 57, 71, 73, 74.79, 85];
    this.setState(
      {
        filteredFeed: feed.filter(
          item => !filterArray.includes(item.subRiskCategory),
        ),
      },
      () => {
        this.processFeedData();
      },
    );
  };

  processFeedData = () => {
    const updateSubRiskCat = this.state.filteredSubRiskCat;
    this.state.filteredFeed.length > 0 &&
      this.state.filteredFeed.map(item => {
        subRiskCategories.map(subitem => {
          if (item.subRiskCategory === subitem.id) {
            updateSubRiskCat.push(subitem.label);
            this.setState({ filteredSubRiskCat: updateSubRiskCat });
          }
        });
      });
    const result = updateSubRiskCat.reduce(
      (a, c) => a.set(c, (a.get(c) || 0) + 1),
      new Map(),
    );
    const numDescending = [...result].sort((a, b) => b[1] - a[1]);
    const filteredSubCategories = numDescending.slice(1, 15);
    this.setState({
      subRiskChartData: filteredSubCategories,
    });
  };

  getAxisData = () => {
    const axisData = getBarChartData(this.state.subRiskChartData);
    this.setState({
      yAxisData: axisData.yData,
      xAxisData: axisData.xData,
    });
  };

  render() {
    let { xAxisData, yAxisData, subRiskChartData } = this.state;
    return (
      <div>
        {subRiskChartData.length > 0 &&
          xAxisData.length === 0 &&
          yAxisData.length === 0 &&
          this.getAxisData()}
        <CustomBarChart
          chartLabel={'No of events for each Subrisk category'}
          chartData={yAxisData}
          chartLabels={xAxisData}
        />
      </div>
    );
  }
}

export default SubRiskCategoryChart;
