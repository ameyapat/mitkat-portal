import React, { Component } from 'react';
import CustomBarChart from '../../../../../ui/customBarChart';

class RiskCategoryChart extends Component {
  getChartData() {
    let riskCategoryData = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    let { feed } = this.props;
    feed?.length &&
      feed.map(item => {
        riskCategoryData[item.eventPrimaryRiskCategory] += 1;
      });
    riskCategoryData.splice(0, 1);
    return riskCategoryData;
  }

  render() {
    const labels = [
      ['Cyber', 'Technology'],
      'Extremism',
      ['Civil', 'Disturbance'],
      'Environment',
      'Health',
      ['Critical', 'Infrastructure'],
      'Crime',
      ['Travel', 'Risks'],
      ['Natural', 'Disasters'],
      ['External', 'Threats'],
      'Political',
      'Regulatory',
    ];

    return (
      <div>
        <CustomBarChart
          chartLabel={'No of Events for Each Risk Category'}
          chartData={this.getChartData()}
          chartLabels={labels}
        />
      </div>
    );
  }
}

export default RiskCategoryChart;
