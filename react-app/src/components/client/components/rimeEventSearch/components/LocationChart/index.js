import React, { Component } from 'react';
import { connect } from 'react-redux';
import './style.scss';
import CustomBarChart from '../../../../../ui/customBarChart';
import { getBarChartData } from '../../helpers/utils';

@connect(
  state => {
    return {
      rime: state.rime,
      themeChange: state.themeChange,
    };
  },
  dispatch => {
    return {
      dispatch,
    };
  },
)
class LocationChart extends Component {
  state = {
    locationsWithCountries: [],
    filteredLocations: [],
    locationsInsideTags:
      this.props.rime.eventSearchlocations.length > 0
        ? this.props.rime.eventSearchlocations
        : [{ label: 'United States', value: 'United States' }],
    xAxisData: [],
    yAxisData: [],
  };

  componentDidMount() {
    this.getLocations();
  }

  getLocations = () => {
    let { feed } = this.props;
    let { locationsWithCountries, locationsInsideTags } = this.state;
    feed.map(item => {
      let loc = item.eventLoc && item.eventLoc.split(' , ');
      loc?.length > 0 &&
        loc.map(subitem => {
          locationsWithCountries.push(subitem);
        });
    });
    for (var i = 0; i < locationsWithCountries.length; i++) {
      locationsInsideTags.map(loctag => {
        if (locationsWithCountries[i] === loctag.value) {
          locationsWithCountries.splice(i, 1);
          i--;
        }
      });
    }

    const result = locationsWithCountries.reduce(
      (a, c) => a.set(c, (a.get(c) || 0) + 1),
      new Map(),
    );
    const numDescending = [...result].sort((a, b) => b[1] - a[1]);
    const unfilteredLocations = numDescending.slice(0, 15);
    this.setState({
      locationsWithCountries,
      filteredLocations: unfilteredLocations,
    });
  };

  getAxisData = () => {
    const axisData = getBarChartData(this.state.filteredLocations);
    this.setState({
      yAxisData: axisData.yData,
      xAxisData: axisData.xData,
    });
  };

  render() {
    let { xAxisData, yAxisData, filteredLocations } = this.state;
    return (
      <div>
        {filteredLocations.length > 0 &&
          xAxisData.length === 0 &&
          yAxisData.length === 0 &&
          this.getAxisData()}
        <CustomBarChart
          chartLabel={'No of Events on Each Locations'}
          chartData={yAxisData}
          chartLabels={xAxisData}
        />
      </div>
    );
  }
}

export default LocationChart;
