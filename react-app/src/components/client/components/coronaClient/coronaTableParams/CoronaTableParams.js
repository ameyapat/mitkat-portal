import React, { Component } from 'react';
import TableListRow from '../../../../ui/tablelist/TableListRow';
import TableListCell from '../../../../ui/tablelist/TableListCell';
import { withStyles, Button } from '@material-ui/core';
import TableList from '../../../../ui/tablelist';
import './CoronaTableParams.scss';
import { API_ROUTES } from '../../../../../helpers/http/apiRoutes';
import { fetchApi } from '../../../../../helpers/http/fetch';
import { withCookies } from 'react-cookie';
import { Line, Bar } from 'react-chartjs-2';
import Comments from '../comments/Comments';
import CustomLabel from '../../../../ui/customLabel';
import clsx from 'clsx';
import './CoronaTableParams.scss';

const styles = () => ({
  rootRiskWatch: {
    background: '#F0F0F1',
    padding: 20,
  },
  innerRiskWatch: {
    '& .dmd__wrap': {
      margin: 0,
    },
  },
  rootThead: {
    fontWeight: 600,
    padding: '15px 3px',
    cursor: 'pointer',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
    borderRadius: 4,
    margin: '0 1px 0 0',
    '& p': {
      textTransform: 'uppercase',
    },
    '&:hover': {
      backgroundColor: 'rgba(0, 0, 0, 0.8)',
    },
  },
  tRowWrap: {
    //   borderTop: '1px solid #f1f1f1',
  },
  rootTRowWrap: {
    padding: '0',
    borderBottom: 0,
    //   backgroundColor: 'rgba(108, 117, 125, 0.063)',
    //   backgroundColor: 'rgba(0, 0, 0, 0.7)',
    borderRadius: 3,

    '& p': {
      color: 'var(--whiteMediumEmp)',
    },
  },
  rowWrap: {
    backgroundColor: '#fff',
    margin: '3px 0',
    padding: '5px 0',
    // boxShadow: '1px 2px 3px 1px rgba(0,0,0,0.3)',
    borderRadius: '3px',
    '&:nth-child(even)': {
      backgroundColor: '#f1f1f1',

      '& p': {
        color: '#6c757d',
      },
    },
  },
  closeTimes: {
    position: 'absolute',
    color: '#000',
    fontSize: 30,
    right: 10,
    top: 10,
    cursor: 'pointer',
  },
  rootBtnGraph: {
    backgroundColor: 'rgba(26, 115, 232, 0.8)',
    fontSize: '13px',
    minWidth: 32,
    '&:hover': {
      backgroundColor: 'rgba(26, 115, 232, 0.7)',
    },

    '& i': {
      margin: 0,
    },
  },
  rootTCellNew: {
    justifyContent: 'space-around',
    '& p': {
      color: 'rgb(108, 117, 125)',
      fontWeight: 'bold',
      textAlign: 'center',
    },
  },
  boxWrapper: {
    flex: 1,
  },
  boxWrapperRight: {
    display: 'flex',
  },
  boxOne: {
    flex: 1,
    overflowX: 'hidden',
    overflowY: 'hidden',
  },
  boxMiddle: {
    flex: 0.6,
    // overflowX: 'hidden',
    // overflowY: 'hidden',
  },
  boxTwo: {
    flex: 0.7,
  },
  metaWrap: {
    margin: '0 10px',
    padding: 10,
    backgroundColor: '#fff',
    // boxShadow: '1px 1px 1px 1px rgba(0,0,0,0.2)',
    borderRadius: 3,
    height: '100vh',
    overflowX: 'hidden',
    overflowY: 'scroll',
  },
  graphWrappers: {
    margin: '5px 0',
  },
});

@withCookies
@withStyles(styles)
class CoronaTableParams extends Component {
  state = {
    eventDetails: {},
    dynamicHeight: 0,
    riskGraphObj: {
      labels: [],
      datasets: [],
    },
    deathGraphObj: {
      labels: [],
      datasets: [],
    },
    growthGraphObj: {
      labels: [],
      datasets: [],
    },
  };

  componentDidMount() {
    const { defaultStateId } = this.props;
    let { dynamicHeight } = this.state;
    const metaInfo = document.getElementById('metaInfoData');
    dynamicHeight = metaInfo.getBoundingClientRect().top;

    this.setState({ dynamicHeight });

    if (defaultStateId) {
      this.getStateWiseReport(defaultStateId);
    }
  }

  parseCaseGraph = () => {
    const { casegraph } = this.state.eventDetails;
    const { riskGraphObj } = this.state;
    const labels = [];
    const dsObjOne = {
      label: 'Total Cases',
      data: [],
      backgroundColor: 'rgba(186, 39, 37, 0.7)',
      borderColor: 'rgba(186, 39, 37, 1)',
      borderWidth: 2,
      pointBackgroundColor: 'rgba(255,255,255,0)',
      pointBorderColor: 'rgba(255,255,255,0)',
      order: 2,
    };
    const dsObjTwo = {
      label: 'Active Cases',
      data: [],
      backgroundColor: 'rgba(66, 75, 84, 0.9)',
      borderColor: 'rgba(66, 75, 84, 1)',
      borderWidth: 2,
      pointBackgroundColor: 'rgba(255,255,255,0)',
      pointBorderColor: 'rgba(255,255,255,0)',
      order: 1,
    };

    casegraph.map(i => {
      labels.push(i.x_point);
      dsObjOne.data.push(i.y_point);
      dsObjTwo.data.push(i.y_point_new);
    });

    riskGraphObj['labels'] = labels;
    riskGraphObj.datasets = [{ ...dsObjOne }, { ...dsObjTwo }];
    this.setState({ riskGraphObj });
  };

  parseGrowthGraph = () => {
    const { growthgraph } = this.state.eventDetails;
    const { growthGraphObj } = this.state;
    const labels = [];
    const dsObjOne = {
      label: 'Additional Cases',
      data: [],
      backgroundColor: 'rgba(6, 144, 165, 0.7)',
      borderColor: 'rgba(6, 144, 165, 1)',
      borderWidth: 2,
      pointBackgroundColor: 'rgba(255,255,255,0)',
      pointBorderColor: 'rgba(255,255,255,0)',
    };
    const dsObjTwo = {
      label: 'Additional Deaths',
      data: [],
      backgroundColor: 'rgba(66, 75, 84, 1)',
      borderColor: 'rgba(66, 75, 84, 1)',
      borderWidth: 2,
      pointBackgroundColor: 'rgba(255,255,255,0)',
      pointBorderColor: 'rgba(255,255,255,0)',
    };

    growthgraph.map(i => {
      labels.push(i.x_point);
      dsObjOne.data.push(i.y_point);
      dsObjTwo.data.push(i.y_point_new);
    });

    growthGraphObj['labels'] = labels;
    growthGraphObj.datasets = [{ ...dsObjOne }, { ...dsObjTwo }];
    this.setState({ growthGraphObj });
  };

  parseDeathGraph = () => {
    const { deathgraph } = this.state.eventDetails;
    const { deathGraphObj } = this.state;
    const labels = [];
    const dsObjOne = {
      label: 'Deaths',
      data: [],
      backgroundColor: 'rgba(186, 39, 37, 0)',
      borderColor: 'rgba(186, 39, 37, 1)',
      borderWidth: 2,
      pointBackgroundColor: 'rgba(255,255,255,0)',
      pointBorderColor: 'rgba(255,255,255,0)',
      yAxisID: 'A',
    };
    const dsObjTwo = {
      label: 'Deaths Growth rate',
      data: [],
      backgroundColor: 'rgba(66, 75, 84, 0)',
      borderColor: 'rgba(66, 75, 84, 1)',
      borderWidth: 2,
      pointBackgroundColor: 'rgba(255,255,255,0)',
      pointBorderColor: 'rgba(255,255,255,0)',
      yAxisID: 'B',
    };

    deathgraph.map(i => {
      labels.push(i.x_point);
      dsObjOne.data.push(i.y_point);
      dsObjTwo.data.push(i.y_point_new);
    });

    deathGraphObj['labels'] = labels;
    deathGraphObj.datasets = [{ ...dsObjOne }, { ...dsObjTwo }];
    this.setState({ deathGraphObj });
  };

  getStateWiseReport = id => {
    try {
      const { cookies } = this.props;
      const reqObj = {
        url: `${API_ROUTES.stateWiseDashboard}/${id}`,
        isAuth: true,
        authToken: cookies.get('authToken'),
        method: 'GET',
        showToggle: true,
      };

      fetchApi(reqObj)
        .then(data => {
          this.setState({ eventDetails: data }, () => {
            this.parseCaseGraph();
            this.parseGrowthGraph();
            this.parseDeathGraph();
          });
        })
        .catch(e => console.log(e));
    } catch (e) {}
  };

  renderTheadTitles = (value, meta) => {
    return (
      <span className="thead--titles--wrapper">
        <p>{value.title}</p>
        <span>
          <i className="fas fa-sort"></i>
        </span>
      </span>
    );
  };

  handleSort = (type, sortBy) => {
    this.props.handleSort(type, sortBy);
  };

  renderTHeader = () => {
    const { classes, data } = this.props;
    return (
      <div className={classes.tRowWrap}>
        <TableListRow classes={{ root: classes.rootTRowWrap }}>
          <TableListCell
            styles={{ flex: 2 }}
            value={{
              title: 'State / Ut',
            }}
            type={'state'}
            renderProp={(value, meta) => this.renderTheadTitles(value, meta)}
            eventHandler={(type, sortBy) => this.handleSort(type, sortBy)}
            classes={{ root: clsx(classes.rootThead, 'msite-t-head') }}
          />
          <TableListCell
            styles={{ flex: 2 }}
            classes={{ root: clsx(classes.rootThead, 'msite-t-head') }}
            eventHandler={(type, sortBy) => this.handleSort(type, sortBy)}
            renderProp={(value, meta) => this.renderTheadTitles(value, meta)}
            value={{
              title: 'Confirmed',
            }}
            meta={{
              color: '#F2042C',
            }}
            type={'confirmed'}
          />
          <TableListCell
            styles={{ flex: 2 }}
            value={{
              title: 'Death',
            }}
            classes={{ root: clsx(classes.rootThead, 'msite-t-head') }}
            eventHandler={(type, sortBy) => this.handleSort(type, sortBy)}
            renderProp={(value, meta) => this.renderTheadTitles(value, meta)}
            type={'death'}
          />
          <TableListCell
            value={{
              title: 'Recovered',
            }}
            classes={{ root: clsx(classes.rootThead, 'msite-t-head') }}
            eventHandler={(type, sortBy) => this.handleSort(type, sortBy)}
            renderProp={(value, meta) => this.renderTheadTitles(value, meta)}
            type={'recovered'}
          />
          <TableListCell
            value={'#'}
            classes={{
              root: clsx(
                classes.rootThead,
                'msite-t-head',
                'hide-me',
                'flx-xs',
              ),
            }}
          />
        </TableListRow>
      </div>
    );
  };

  renderTBody = () => {
    return <TableList>{this.renderEventList()}</TableList>;
  };

  renderCases = (value, meta) => {
    return (
      <div className="highlight-wrapper">
        {value.total > 0 ? (
          <>
            <span className="new-cases">
              {value.new > 0 && (
                <>
                  <span className="arrow-up">
                    <i
                      className="fas fa-angle-double-up"
                      style={{ color: meta.color }}
                    ></i>
                  </span>
                  <span
                    className="new-cases-value"
                    style={{ color: meta.color }}
                  >
                    {value.new}
                  </span>
                </>
              )}
              {value.percent > 0 && (
                <span className="case-percentage">({value.percent}%)</span>
              )}
            </span>
            <span className="t-value">{value.total}</span>
          </>
        ) : (
          <span className="t-value">-</span>
        )}

        {/* <span className={`highlight-icon ${value && value >= 1 && 'pulse--effect'}`}>
                    <i class="fas fa-circle" style={{color: meta.color}}></i>
                </span>  */}
      </div>
    );
  };

  renderEventList = () => {
    const { classes, data } = this.props;
    return data.map(event => (
      <TableListRow
        key={event + '_' + event.statename}
        classes={{ root: classes.rowWrap }}
      >
        <TableListCell
          styles={{ flex: 2 }}
          value={event.statename}
          classes={{ root: clsx(classes.rootTCellNew, 'msite-t-head') }}
          // onClick={() => this.getStateWiseReport(event.stateid)}
        />
        <TableListCell
          styles={{ flex: 2 }}
          renderProp={(value, meta) => this.renderCases(value, meta)}
          value={{
            new: event.newcases,
            total: event.totalinfected,
            percent: event.growthratecases,
          }}
          meta={{
            color: '#F2042C',
          }}
          classes={{ root: clsx(classes.rootTCellNew, 'msite-t-head') }}
        />
        <TableListCell
          styles={{ flex: 2 }}
          renderProp={(value, meta) => this.renderCases(value, meta)}
          value={{
            new: event.newdeaths,
            total: event.totaldeaths,
            percent: event.growthratedeaths,
          }}
          meta={{
            color: '#F2042C',
          }}
          classes={{ root: clsx(classes.rootTCellNew, 'msite-t-head') }}
        />
        <TableListCell
          value={
            event.totalrecoverd !== 0 ? (
              `${event.totalrecoverd || 0} (${event.recoverypercent || 0}%)`
            ) : (
              <span className="t-value">-</span>
            )
          }
          classes={{ root: clsx(classes.rootTCellNew, 'msite-t-head') }}
        />
        <TableListCell classes={{ root: 'msite-t-head' }}>
          <Button
            variant="contained"
            color="secondary"
            onClick={() => this.getStateWiseReport(event.stateid)}
            classes={{
              root: clsx(classes.rootBtnGraph, 'cta-g'),
            }}
          >
            <span className="btnViewComments">
              {/* <i class="fas fa-chart-bar"></i> */}
              <i className="fas fa-chart-line"></i>
            </span>
          </Button>
        </TableListCell>
      </TableListRow>
    ));
  };

  render() {
    const { classes } = this.props;
    const {
      dynamicHeight,
      riskGraphObj,
      eventDetails,
      growthGraphObj,
      deathGraphObj,
    } = this.state;

    return (
      <div className="coronaTableParams">
        <div className={clsx(classes.boxOne, 'box-one-coronaTable')}>
          {this.renderTHeader()}
          {this.renderTBody()}
        </div>
        <div className={clsx(classes.boxWrapper, 'box--wrapper')}>
          <CustomLabel
            title={eventDetails.statename}
            classes={clsx('select--label', 'comment-label')}
          />
          <div className={clsx(classes.boxWrapperRight, 'boxWrapperRight')}>
            <div className={classes.boxMiddle}>
              <div
                className="stateMeta--info"
                style={{ height: `calc(100vh - ${dynamicHeight}px)` }}
              >
                <div className="boxes--internal">
                  {eventDetails && eventDetails.implementedMeasures && (
                    <CustomLabel
                      title="Implemented Measures"
                      classes={clsx('select--label', 'comment-label', 'no-pd')}
                    />
                  )}
                  <div
                    className="implementedMeasures"
                    dangerouslySetInnerHTML={{
                      __html: eventDetails.implementedMeasures,
                    }}
                  ></div>
                </div>
                <div className="boxes--internal">
                  {eventDetails && eventDetails.resources && (
                    <CustomLabel
                      title="Resources"
                      classes={clsx('select--label', 'comment-label')}
                    />
                  )}
                  <a className="resources" href={eventDetails.resources}>
                    {eventDetails.resources}
                  </a>
                </div>
                <div className="boxes--internal">
                  {eventDetails &&
                    eventDetails.comments &&
                    eventDetails.comments.length > 0 && (
                      <CustomLabel
                        title="Updates"
                        classes={clsx('select--label', 'comment-label')}
                      />
                    )}
                  <Comments data={eventDetails.comments || ['NA']} />
                </div>
              </div>
            </div>
            <div className={classes.boxTwo}>
              <div
                id="metaInfoData"
                className={classes.metaWrap}
                style={{ height: `calc(100vh - ${dynamicHeight}px)` }}
              >
                <div className={clsx(classes.graphWrappers, 'graph-wrappers')}>
                  <Line
                    data={riskGraphObj}
                    height={190}
                    options={{
                      scales: {
                        xAxes: [
                          {
                            gridLines: {
                              display: true,
                            },
                          },
                        ],
                        yAxes: [
                          {
                            gridLines: {
                              display: true,
                            },
                          },
                        ],
                      },
                    }}
                  />
                </div>
                <div className={clsx(classes.graphWrappers, 'graph-wrappers')}>
                  <Bar
                    data={growthGraphObj}
                    height={190}
                    options={{
                      scales: {
                        xAxes: [
                          {
                            gridLines: {
                              display: true,
                            },
                          },
                        ],
                        yAxes: [
                          {
                            gridLines: {
                              display: true,
                            },
                          },
                        ],
                      },
                    }}
                  />
                </div>
                <div className={clsx(classes.graphWrappers, 'graph-wrappers')}>
                  <Line
                    data={deathGraphObj}
                    height={190}
                    options={{
                      scales: {
                        xAxes: [
                          {
                            gridLines: {
                              display: true,
                            },
                          },
                        ],
                        yAxes: [
                          {
                            gridLines: {
                              display: false,
                            },
                            id: 'A',
                            type: 'linear',
                            position: 'left',
                          },
                          {
                            id: 'B',
                            type: 'linear',
                            position: 'right',
                            ticks: {
                              max: 100,
                              min: 0,
                            },
                          },
                        ],
                      },
                    }}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CoronaTableParams;
