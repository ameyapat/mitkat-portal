import React, { Component } from 'react';
import Empty from '../../../../ui/empty';
import moment from 'moment';
import './style.scss';

class List extends Component{

    renderList = (advisories) => {
       return advisories.map(a => <a href={a.advisoryurl} target="_blank" rel="noopener noreferrer">
            <li>
                <div className="flexWrap">
                    <div>
                        <span><i class="fas fa-shield-virus"></i></span>
                    </div>
                    <div>
                        <h4>{a.title}</h4>                
                    </div>
                </div>
                {/* <p>{moment(a.uploadtime).format("YYYY-MM-DD")}</p> */}
            </li>
        </a>)
    }

    render(){
        const { advisories } = this.props;

        return(
            <ul className="corona-advisory--list">
                {
                    advisories.length > 0
                    ?
                    this.renderList(advisories)
                    :
                    <Empty title="No advisories available at this moment" />
                }
            </ul>
        )
    }
}

export default List;