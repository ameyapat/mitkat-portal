import React, { Component } from 'react';
import CustomModal from '../../../../ui/modal';
import './style.scss';
import List from './List';

import { fetchApi } from "../../../../../helpers/http/fetch";
import { API_ROUTES } from "../../../../../helpers/http/apiRoutes";
import { withCookies } from 'react-cookie';

@withCookies
class Advisories extends Component{

    state = {
        showAdvisoriesModal: false,
        advisories: []
    }

    handleModal = (show) => this.setState({ showAdvisoriesModal: show })

    handleGetAdvisories = () => {
        const { cookies } = this.props;
        const reqObj = {
            url: API_ROUTES.getCoronaAdvisory,
            isAuth: false,
            authToken: cookies.get("authToken"),
            method: "GET",
            showToggle: true,
        };

        fetchApi(reqObj)
        .then(advisories => {
            this.setState({ advisories }, () => {
                this.handleModal(true);
            })
        })
        .catch(e => console.log(e));
    }

    render(){
        return(
            <div id="coronaAdvisories">
                <div className="corona-adv-btn" onClick={() => this.handleGetAdvisories()}>
                    <span className="notification--3">3</span>
                    <div className="box-one">
                        <span className="advisoryIcon">
                            <i className="fas fa-shield-virus"></i>
                        </span>
                    </div>
                    <div className="box-two">
                        <h4>Advisories (COVID-19)</h4>
                        <p>Click to get the latest advisories regarding covid-19</p>
                    </div>
                </div>
                {
                    //modal goes here
                }
                <CustomModal
                    id={'coronaAdvisoryModal'}
                    disableBackdropClick={false}
                    showCloseOption={true}
                    showModal={this.state.showAdvisoriesModal} 
                    closeModal={() => this.handleModal(false)}
                >
                    <div className="modal__inner corona-advisory-list">
                        <div className="list-title-corona">
                            <h4>Advisories (Covid-19)</h4>
                        </div>
                        <List advisories={this.state.advisories} />
                    </div>
                </CustomModal>
            </div>
        )
    }
}

export default Advisories