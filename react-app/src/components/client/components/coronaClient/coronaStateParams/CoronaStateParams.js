import React, { Component } from 'react';
import TableListRow from '../../../../ui/tablelist/TableListRow';
import TableListCell from '../../../../ui/tablelist/TableListCell';
import { withStyles } from '@material-ui/core';
import TableList from '../../../../ui/tablelist';

const styles = () => ({
  rootRiskWatch: {
    background: '#F0F0F1',
    padding: 20,
  },
  innerRiskWatch: {
    '& .dmd__wrap': {
      margin: 0,
    },
  },
  rootThead: {
    fontWeight: 600,
  },
  tRowWrap: {
    //   borderTop: '1px solid #f1f1f1',
  },
  rootTRowWrap: {
    padding: '15px 0',
    borderBottom: 0,
    backgroundColor: '#556683',
    borderRadius: 3,
    '& p': {
      color: 'var(--whiteMediumEmp)',
    },
  },
  rowWrap: {
    backgroundColor: '#fff',
    '&:nth-child(even)': {
      backgroundColor: '#E8826E',

      '& p': {
        color: 'var(--whiteMediumEmp)',
      },
    },
  },
  closeTimes: {
    position: 'absolute',
    color: '#000',
    fontSize: 30,
    right: 10,
    top: 10,
    cursor: 'pointer',
  },
});

@withStyles(styles)
class CoronaStateParams extends Component {
  renderTHeader = () => {
    const { classes } = this.props;
    return (
      <div className={classes.tRowWrap}>
        <TableListRow classes={{ root: classes.rootTRowWrap }}>
          <TableListCell
            value={'Total Cases'}
            classes={{ root: classes.rootThead }}
          />
          <TableListCell
            value={'New Cases'}
            classes={{ root: classes.rootThead }}
          />
          <TableListCell
            value={'Growth Rate'}
            classes={{ root: classes.rootThead }}
          />
          <TableListCell
            value={'Total Death'}
            classes={{ root: classes.rootThead }}
          />
          <TableListCell
            value={'New Death'}
            classes={{ root: classes.rootThead }}
          />
          <TableListCell
            value={'Growth Rate'}
            classes={{ root: classes.rootThead }}
          />
          <TableListCell
            value={'Total Recovered'}
            classes={{ root: classes.rootThead }}
          />
        </TableListRow>
      </div>
    );
  };

  renderTBody = () => {
    return <TableList>{this.renderEventList()}</TableList>;
  };

  renderEventList = () => {
    const { classes, data } = this.props;
    return data.map(event => (
      <TableListRow
        key={event + '_' + event.statename}
        classes={{ root: classes.rowWrap }}
      >
        <TableListCell value={event.totalinfected} />
        <TableListCell value={event.newcases} />
        <TableListCell value={event.growthratecases} />
        <TableListCell value={event.totaldeaths} />
        <TableListCell value={event.newdeaths} />
        <TableListCell value={event.growthratedeaths} />
        <TableListCell value={event.totalrecoverd} />
      </TableListRow>
    ));
  };

  render() {
    return (
      <>
        {this.renderTHeader()}
        {this.renderTBody()}
      </>
    );
  }
}

export default CoronaStateParams;
