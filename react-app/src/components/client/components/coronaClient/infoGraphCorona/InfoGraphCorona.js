import React, { Component } from 'react';
import './InfoGraphCorona.scss';

class InfoGraphCorona extends Component{

    render(){
        const { title, value, icon, color, meta } = this.props;
        return(
            <div className="infoGraphCorona">
                <span className="iconWrapper">
                    <i className={icon} style={{ color }}></i>
                </span>
                <h1>{title}</h1>
                { meta && <p className="infograph--meta">{meta}</p>}
                <p>{value}</p>
            </div>
        )
    }
}

export default InfoGraphCorona;