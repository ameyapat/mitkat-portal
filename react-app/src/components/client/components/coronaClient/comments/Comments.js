import React from 'react';
import './comments.scss';

const Comments = ({data}) => {
    return <ul className="comment-list">{ data.map((item, idx) => <li key={idx}>{item}</li>) }</ul>
}

export default Comments;