import { Tooltip } from '@material-ui/core';
import React, { Component } from 'react';
import GlobalSearchBar from '../../../ui/GlobalSearchBar/GlobalSearchBar';
import './style.scss';
import { withRouter } from 'react-router-dom';
import { withCookies } from 'react-cookie';
import * as actions from '../../../../actions';
import { connect } from 'react-redux';
import { toggleEnableSearch } from '../../../../actions/riskTrackerAction';
import { toggleSearchBar } from '../../../../actions/searchActions';

class ClientFilterOptions extends Component {
  handleToggleSearch = show => {
    const { dispatch } = this.props;
    dispatch(toggleEnableSearch(show));
    dispatch(toggleSearchBar(show));
  };

  render() {
    const {
      search: { showSearchBar },
    } = this.props;
    return (
      <div id="clientFilterOptions">
        {showSearchBar && (
          <>
            <Tooltip title="Search">
              <button
                className="btnGlobalSearch"
                onClick={() => this.handleToggleSearch(true)}
              >
                <i className="fas fa-search-location"></i>
              </button>
            </Tooltip>
            <Tooltip title="Filter">
              <button
                className="btnEllipsis"
                onClick={() => {
                  this.props.dispatch(actions.toggleRimeFilters(true));
                }}
              >
                <i className="fas fa-filter"></i>
              </button>
            </Tooltip>
          </>
        )}
        {!showSearchBar && (
          <>
            <GlobalSearchBar
              placeholder="Search by keyword"
              onClose={() => this.handleToggleSearch(false)}
              onApply={this.handleApplySearch}
            />

            <Tooltip title="Filter">
              <button
                className="btnEllipsis"
                onClick={() => {
                  this.props.dispatch(actions.toggleRimeFilters(true));
                }}
              >
                <i className="fas fa-filter"></i>
              </button>
            </Tooltip>
          </>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  search: state.search,
});
const mapDispatchToProps = dispatch => ({ dispatch });

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(withCookies(ClientFilterOptions)),
);
