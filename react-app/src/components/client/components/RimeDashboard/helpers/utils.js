export function lastUpdateDate(lastUpdatedDate) {
  const dateLastUpdateString = lastUpdatedDate;
  const userLastUpdateOffset = new Date().getTimezoneOffset() * 60 * 1000;
  const localLastUpdateDate = new Date(dateLastUpdateString);
  const utcLastUpdateDate = new Date(
    localLastUpdateDate.getTime() - userLastUpdateOffset,
  );
  return utcLastUpdateDate;
}

export function eDate(eventDate) {
  const dateString = eventDate;
  const userOffset = new Date().getTimezoneOffset() * 60 * 1000;
  const localDate = new Date(dateString);
  const utcDate = new Date(localDate.getTime() - userOffset);
  return utcDate;
}

export function istDates(eventDate) {
  const dateString = new Date(eventDate);
  const userOffset = new Date().getTimezoneOffset() * 60 * 1000;
  const localDate = new Date(
    dateString
      .toISOString()
      .replace('T', ',')
      .replace('Z', ' '),
  );
  const utcDate = new Date(localDate.getTime() - userOffset);
  return utcDate;
}
