export const routeItems = {
  routeOptions: [
    {
      id: 1,
      exact: true,
      path: 'url',
      components: 'Rime',
      route: '',
    },
    {
      id: 2,
      exact: false,
      path: 'path',
      components: 'KeywordMonitor',
      route: '/keywordMonitor',
    },
    {
      id: 3,
      exact: false,
      path: 'path',
      components: 'NewEventSearch',
      route: '/eventSearch',
    },
    {
      id: 4,
      exact: false,
      path: 'path',
      components: 'RimeSubscriptions',
      route: '/subscriptions',
    },
    {
      id: 5,
      exact: false,
      path: 'path',
      components: 'ComingSoon',
      route: '/comingSoon',
    },
    {
      id: 6,
      exact: false,
      path: 'path',
      components: 'PrivacyPolicy',
      route: '/privacyPolicy',
    },
    {
      id: 7,
      exact: false,
      path: 'path',
      components: 'Awards',
      route: '/awards',
    },
    {
      id: 8,
      exact: false,
      path: 'path',
      components: 'Certificates',
      route: '/certificates',
    },
    {
      id: 9,
      exact: false,
      path: 'path',
      components: 'TermsOfService',
      route: '/termsOfService',
    },
  ],
};

export const categories = [
  {
    id: 0,
    label: 'Other',
    name: 'categories',
  },
  {
    id: 1,
    label: 'Technology',
    name: 'categories',
  },
  {
    id: 2,
    label: 'Extremism',
    name: 'categories',
  },
  {
    id: 3,
    label: 'Civil Disturbances',
    name: 'categories',
  },
  {
    id: 4,
    label: 'Environment',
    name: 'categories',
  },
  {
    id: 5,
    label: 'Health',
    name: 'categories',
  },
  {
    id: 6,
    label: 'Critical Infrastructure',
    name: 'categories',
  },
  {
    id: 7,
    label: 'Crime',
    name: 'categories',
  },
  {
    id: 8,
    label: 'Travel Risks',
    name: 'categories',
  },
  {
    id: 9,
    label: 'Natural Disasters',
    name: 'categories',
  },
  {
    id: 10,
    label: 'External Threats',
    name: 'categories',
  },
  {
    id: 11,
    label: 'Political',
    name: 'categories',
  },
  {
    id: 12,
    label: 'Regulatory',
    name: 'categories',
  },
];

export const subRiskCategories = [
  {
    id: 1,
    label: 'Protest',
    name: 'subRiskCategories',
  },
  {
    id: 2,
    label: 'Rally',
    name: 'subRiskCategories',
  },
  {
    id: 3,
    label: 'Strike',
    name: 'subRiskCategories',
  },
  {
    id: 4,
    label: 'Riot',
    name: 'subRiskCategories',
  },
  {
    id: 5,
    label: 'Gathering',
    name: 'subRiskCategories',
  },
  {
    id: 6,
    label: 'Stampede',
    name: 'subRiskCategories',
  },
  {
    id: 7,
    label: 'Curfew',
    name: 'subRiskCategories',
  },
  {
    id: 8,
    label: 'Other',
    name: 'subRiskCategories',
  },
  {
    id: 9,
    label: 'Harassment',
    name: 'subRiskCategories',
  },
  {
    id: 10,
    label: 'Liquor',
    name: 'subRiskCategories',
  },
  {
    id: 11,
    label: 'Financial Crime',
    name: 'subRiskCategories',
  },
  {
    id: 12,
    label: 'Counterfeit',
    name: 'subRiskCategories',
  },
  {
    id: 13,
    label: 'Law enforcement',
    name: 'subRiskCategories',
  },
  {
    id: 14,
    label: 'Theft',
    name: 'subRiskCategories',
  },
  {
    id: 15,
    label: 'Violent Crime',
    name: 'subRiskCategories',
  },
  {
    id: 16,
    label: 'Other',
    name: 'subRiskCategories',
  },
  {
    id: 17,
    label: 'Criminal networks',
    name: 'subRiskCategories',
  },
  {
    id: 18,
    label: 'Illicit Substance',
    name: 'subRiskCategories',
  },
  {
    id: 19,
    label: 'Bomb & Explosion',
    name: 'subRiskCategories',
  },
  {
    id: 20,
    label: 'Hijacking',
    name: 'subRiskCategories',
  },
  {
    id: 21,
    label: 'Assassination',
    name: 'subRiskCategories',
  },
  {
    id: 22,
    label: 'Mass Shootings',
    name: 'subRiskCategories',
  },
  {
    id: 23,
    label: 'Terror Threats & Attacks',
    name: 'subRiskCategories',
  },
  {
    id: 24,
    label: 'IED Blast',
    name: 'subRiskCategories',
  },
  {
    id: 25,
    label: 'Arson',
    name: 'subRiskCategories',
  },
  {
    id: 26,
    label: 'Maoism / Left Wing Extremism',
    name: 'subRiskCategories',
  },
  {
    id: 27,
    label: 'Abduction',
    name: 'subRiskCategories',
  },
  {
    id: 28,
    label: 'Insurgency',
    name: 'subRiskCategories',
  },
  {
    id: 29,
    label: 'Other',
    name: 'subRiskCategories',
  },
  {
    id: 30,
    label: 'Fire',
    name: 'subRiskCategories',
  },
  {
    id: 31,
    label: 'Power',
    name: 'subRiskCategories',
  },
  {
    id: 32,
    label: 'Building',
    name: 'subRiskCategories',
  },
  {
    id: 33,
    label: 'Bridge',
    name: 'subRiskCategories',
  },
  {
    id: 34,
    label: 'Port & Shipping',
    name: 'subRiskCategories',
  },
  {
    id: 35,
    label: 'Air Transport',
    name: 'subRiskCategories',
  },
  {
    id: 36,
    label: 'Gas & Leaks',
    name: 'subRiskCategories',
  },
  {
    id: 37,
    label: 'Other',
    name: 'subRiskCategories',
  },
  {
    id: 38,
    label: 'Dam',
    name: 'subRiskCategories',
  },
  {
    id: 39,
    label: 'Railway',
    name: 'subRiskCategories',
  },
  {
    id: 40,
    label: 'Road Transport',
    name: 'subRiskCategories',
  },
  {
    id: 41,
    label: 'Mining',
    name: 'subRiskCategories',
  },
  {
    id: 42,
    label: 'External Intervention',
    name: 'subRiskCategories',
  },
  {
    id: 43,
    label: 'Border Conflict',
    name: 'subRiskCategories',
  },
  {
    id: 44,
    label: 'Other',
    name: 'subRiskCategories',
  },
  {
    id: 45,
    label: 'Pandemics',
    name: 'subRiskCategories',
  },
  {
    id: 46,
    label: 'Epidemic',
    name: 'subRiskCategories',
  },
  {
    id: 47,
    label: 'Healthcare Facilities',
    name: 'subRiskCategories',
  },
  {
    id: 48,
    label: 'Communicable Diseases',
    name: 'subRiskCategories',
  },
  {
    id: 49,
    label: 'Vector borne Diseases',
    name: 'subRiskCategories',
  },
  {
    id: 50,
    label: 'Other',
    name: 'subRiskCategories',
  },
  {
    id: 51,
    label: 'Pollution',
    name: 'subRiskCategories',
  },
  {
    id: 52,
    label: 'Rainfall',
    name: 'subRiskCategories',
  },
  {
    id: 53,
    label: 'Weather Forecast',
    name: 'subRiskCategories',
  },
  {
    id: 54,
    label: 'Drought',
    name: 'subRiskCategories',
  },
  {
    id: 55,
    label: 'Heat Wave',
    name: 'subRiskCategories',
  },
  {
    id: 56,
    label: 'Cold Wave',
    name: 'subRiskCategories',
  },
  {
    id: 57,
    label: 'Other',
    name: 'subRiskCategories',
  },
  {
    id: 58,
    label: 'Cyclone',
    name: 'subRiskCategories',
  },
  {
    id: 59,
    label: 'Earthquake',
    name: 'subRiskCategories',
  },
  {
    id: 60,
    label: 'Landslide',
    name: 'subRiskCategories',
  },
  {
    id: 61,
    label: 'Floods',
    name: 'subRiskCategories',
  },
  {
    id: 62,
    label: 'Bushfires / Forest Fires',
    name: 'subRiskCategories',
  },
  {
    id: 63,
    label: 'Avalanche',
    name: 'subRiskCategories',
  },
  {
    id: 64,
    label: 'Volcano',
    name: 'subRiskCategories',
  },
  {
    id: 65,
    label: 'Tsunami',
    name: 'subRiskCategories',
  },
  {
    id: 66,
    label: 'Locust Attack',
    name: 'subRiskCategories',
  },
  {
    id: 67,
    label: 'Storms',
    name: 'subRiskCategories',
  },
  {
    id: 68,
    label: 'Lightning Strikes',
    name: 'subRiskCategories',
  },
  {
    id: 69,
    label: 'Tornado',
    name: 'subRiskCategories',
  },
  {
    id: 70,
    label: 'Typhoon',
    name: 'subRiskCategories',
  },
  {
    id: 71,
    label: 'Other',
    name: 'subRiskCategories',
  },
  {
    id: 72,
    label: 'Election',
    name: 'subRiskCategories',
  },
  {
    id: 73,
    label: 'Other',
    name: 'subRiskCategories',
  },
  {
    id: 74,
    label: 'Other',
    name: 'subRiskCategories',
  },
  {
    id: 75,
    label: 'Cyber Theft',
    name: 'subRiskCategories',
  },
  {
    id: 76,
    label: 'Cyber Attack',
    name: 'subRiskCategories',
  },
  {
    id: 77,
    label: 'Data Breach',
    name: 'subRiskCategories',
  },
  {
    id: 78,
    label: 'Outage',
    name: 'subRiskCategories',
  },
  {
    id: 79,
    label: 'Other',
    name: 'subRiskCategories',
  },
  {
    id: 80,
    label: 'Traffic diversions',
    name: 'subRiskCategories',
  },
  {
    id: 81,
    label: 'Road Blocks & Congestion',
    name: 'subRiskCategories',
  },
  {
    id: 82,
    label: 'Road Accidents',
    name: 'subRiskCategories',
  },
  {
    id: 83,
    label: 'Train',
    name: 'subRiskCategories',
  },
  {
    id: 84,
    label: 'Flight',
    name: 'subRiskCategories',
  },
  {
    id: 85,
    label: 'Other',
    name: 'subRiskCategories',
  },
];
export const relevancyConstants = [
  {
    id: 1,
    label: 'Low',
    name: 'relevancy',
  },
  {
    id: 2,
    label: 'Medium',
    name: 'relevancy',
  },
  {
    id: 3,
    label: 'High',
    name: 'relevancy',
  },
];

export const languageConstants = [
  {
    id: 'en',
    label: 'English',
    name: 'language',
  },
  {
    id: 'hi',
    label: 'Hindi',
    name: 'language',
  },
  {
    id: 'zh',
    label: 'Chinese',
    name: 'language',
  },
  {
    id: 'ar',
    label: 'Arabic',
    name: 'language',
  },
  {
    id: 'es',
    label: 'Spanish',
    name: 'language',
  },
  {
    id: 'ur',
    label: 'Urdu',
    name: 'language',
  },
  {
    id: 'mr',
    label: 'Marathi',
    name: 'language',
  },
  {
    id: 'fr',
    label: 'French',
    name: 'language',
  },
  {
    id: 'bn',
    label: 'Bengali',
    name: 'language',
  },
  {
    id: 'pt',
    label: 'Portuguese',
    name: 'language',
  },
  {
    id: 'id',
    label: 'Indonesian',
    name: 'language',
  },
  {
    id: 'ml',
    label: 'Malayalam',
    name: 'language',
  },
  {
    id: 'ta',
    label: 'Tamil',
    name: 'language',
  },
  {
    id: 'kn',
    label: 'Kannada',
    name: 'language',
  },
  {
    id: 'ms',
    label: 'Malay',
    name: 'language',
  },
  {
    id: 'th',
    label: 'Thai',
    name: 'language',
  },
  {
    id: 'vi',
    label: 'Vietnamese',
    name: 'language',
  },
];

export const eventTypeConstants = [
  {
    id: 1,
    value: 1,
    label: 'Notification',
    name: 'eventType',
  },
  {
    id: 2,
    value: 2,
    label: 'Warning',
    name: 'eventType',
  },
  {
    id: 3,
    value: 3,
    label: 'Crucial',
    name: 'eventType',
  },
];

export const riskLevels = [
  {
    id: 1,
    label: 'Very Low',
    name: 'riskLevel',
  },
  {
    id: 2,
    label: 'Low',
    name: 'riskLevel',
  },
  {
    id: 3,
    label: 'Medium',
    name: 'riskLevel',
  },
  {
    id: 4,
    label: 'High',
    name: 'riskLevel',
  },
  {
    id: 5,
    label: 'Very High',
    name: 'riskLevel',
  },
];
