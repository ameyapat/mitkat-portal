import React, { Component } from 'react';
import { Switch, Route, withRouter } from 'react-router-dom';
import Rime from '../../pages/Rime';
import ComingSoon from '../comingSoon';
import { routeItems } from './helpers/constants.js';
import PrivacyPolicy from '../privacyPolicy';
import Awards from '../mitkatAwards';
import Certificates from '../mitkatCertificates';
import TermsOfService from '../termsOfService';
import KeywordMonitor from '../keywordMonitor';
import NewEventSearch from '../rimeEventSearch/eventSearch';
import RimeSubscriptions from '../rimeSubscriptions';
import NavMenuHorizontal from '../navMenuHorizontal';

const componentRegistry = {
  Rime,
  ComingSoon,
  PrivacyPolicy,
  Awards,
  Certificates,
  TermsOfService,
  NewEventSearch,
  KeywordMonitor,
  RimeSubscriptions,
};

@withRouter
class RimeDashboard extends Component {
  getRouteList = () => {
    let routetXML = [];
    let { routeOptions } = routeItems;
    routetXML = routeOptions.map(item => (
      <Route
        key={item.id}
        exact={item.exact}
        path={`${this.props.match[item.path]}${item.route}`}
        component={componentRegistry[item.components]}
      />
    ));
    return routetXML;
  };

  render() {
    return (
      <>
        <NavMenuHorizontal />
        <Switch>{this.getRouteList()}</Switch>
      </>
    );
  }
}

export default RimeDashboard;
