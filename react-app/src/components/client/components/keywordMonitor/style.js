export default {
  singleSelect: {
    width: '100%',
  },
  rootText: {
    margin: '15px 0',
    color: 'var(--whiteHighEmp) !important',
  },
  listOfItems: {
    margin: 0,
    borderBottom: '1px solid #f1f1f1',
    paddingBottom: 10,
    fontSize: 12,
    color: '#555',
    fontWeight: 'normal',
  },
  size: {
    width: 40,
    height: 30,
  },
  sizeIcon: {
    fontSize: 20,
    color: 'var(--whiteMediumEmp)',
  },
  rootLabel: {
    color: 'var(--whiteHighEmp) !important ',
  },
  rootInput: {
    color: 'var(--whiteHighEmp) !important',
    width: '100%',
  },
  rootInputKeywords: {
    color: 'var(--whiteHighEmp) !important',
    width: '100%',
    paddingTop: 15,
  },
  underline: {
    color: 'var(--whiteHighEmp)',
    '&::hover': {
      '&::before': {
        borderBottom: '2px solid var(--whiteHighEmp)',
      },
    },
    '&::before': {
      borderBottom: '2px solid var(--whiteHighEmp)',
    },
    '&::after': {
      borderBottom: '2px solid var(--whiteHighEmp)',
    },
  },
  notchedOutline: {
    borderColor: 'var(--whiteHighEmp) !important ',
  },
  radioLabel: {
    color: 'var(--whiteHighEmp) !important ',
  },
  label: {
    color: 'var(--whiteHighEmp) !important ',
  },

  rootPagination: {
    '& .MuiButtonBase-root': {
      backgroundColor: 'transparent',
      color: 'var(--whiteHighEmp) !important ',
    },

    '& .Mui-selected': {
      fontWeight: 500,
      backgroundColor: 'rgba(130, 130, 130, 0.4) !important',
      color: 'var(--whiteHighEmp) !important ',
    },
  },
};
