import React, { useState } from 'react';
import Pagination from '@mui/material/Pagination';

import style from './style';
import withStyles from 'react-jss';

const PaginationFile = ({ totalResults, getQueryNewsData, classes }) => {
  const pageNumbers = [];

  for (let i = 1; i <= Math.ceil(totalResults / 100); i++) {
    pageNumbers.push(i);
  }

  const handleChange = (event, value) => {
    getQueryNewsData(value);
  };

  return (
    <Pagination
      count={pageNumbers.length}
      onChange={handleChange}
      showFirstButton
      showLastButton
      className={classes.rootPagination}
      InputProps={{
        classes: {
          root: classes.rootPagination,
        },
      }}
    />
  );
};

export default withStyles(style)(PaginationFile);
