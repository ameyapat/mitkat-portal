import React, { Component } from 'react';
import { connect } from 'react-redux';
import './style.scss';
import style from './style';
import injectSheet from 'react-jss/lib/injectSheet';
import { Button, TextField } from '@material-ui/core';
import { DatePicker } from 'material-ui-pickers';
import { fetchApi } from '../../../../helpers/http/fetch';
import { keywordMonitor } from '../../../../helpers/http/apiRoutes';
import { withCookies } from 'react-cookie';
import { setQueryNewsData } from '../../../../actions/keywordMonitorAction';
import store from '../../../../store';
import moment from 'moment';
import PaginationFile from './Pagination';
import noImg from '../../../../assets/noImg.png';
import { toggleIsFetch } from '../../../../actions';
import Loader from '../../../ui/loader';
import EventArchives from '../rimeFilters/EventArchives';
import * as actions from '../../../../actions';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import CancelIcon from '@mui/icons-material/Cancel';
import notFound from '../../../../assets/No-Events-Found.png';
import { withStyles, Tooltip, Drawer } from '@material-ui/core';

const drawerArchiveWidth = '100vw';
const drawerArchiveHeight = '200px';
const drawerWidth = '100vw';
const drawerHeight = '60px';

const styles = theme => ({
  paperAnchorArchiveTop: {
    zIndex: 996,
    position: 'absolute',
    overflowX: 'hidden',
    overflowY: 'hidden',
    width: drawerArchiveWidth,
    height: drawerArchiveHeight,
    marginTop: '8vh',
    backgroundColor: 'var(--filterBackground)',

    '&:hover': {
      overflowY: 'scroll',
    },
    '&::-webkit-scrollbar': {
      width: 3,
    },
    '&::-webkit-scrollbar-track': {
      background: 'transparent',
    },
    '&::-webkit-scrollbar-thumb': {
      backgroundColor: '#ccc',
      borderRadius: 5,
    },
  },
  iconOptions: {
    display: 'flex',
    justifyContent: 'flex-end',
    padding: 3,
    minHeight: 40,
  },
  closed: {
    justifyContent: 'space-around',
  },
  chevIcons: {
    color: 'var(--whiteMediumEmp)',
  },
  menuIcon: {
    color: 'var(--whiteMediumEmp)',
  },
  paperAnchorTop: {
    overflowX: 'hidden',
    overflowY: 'scroll',
    width: drawerWidth,
    height: 'auto',
    minHeight: drawerHeight,
    maxHeight: '92vh',
    marginTop: '8vh',
    backgroundColor: 'var(--filterBackground)',

    '&::-webkit-scrollbar': {
      width: 3,
    },
    '&::-webkit-scrollbar-track': {
      background: 'transparent',
    },
    '&::-webkit-scrollbar-thumb': {
      backgroundColor: '#ccc',
      borderRadius: 5,
    },
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 36,
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing.unit * 7 + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing.unit * 9 + 1,
    },
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
  },
  menuItemBtn: {
    padding: 6,
  },
  test: {
    background: 'none',
  },
  searchStyle: {
    backgroundColor: '#02385a',
    color: 'white',
    height: 42,
    width: 170,
    marginLeft: 10,
    '&:hover': {
      background: '#A0B63C',
    },
  },
  cancelBtnStyle: {
    backgroundColor: '#111826',
    color: 'white',
    height: 42,
    width: 115,
    paddingRight: '20px',
    '&:hover': {
      background: '#E35A5A',
    },
  },
});

let date = new Date();
@withCookies
@withStyles(styles, { withTheme: true })
@injectSheet(style)
@connect(
  state => {
    return {
      themeChange: state.themeChange,
      keywordMonitor: state.keywordMonitor,
      appState: state.appState,
      rime: state.rime,
    };
  },
  dispatch => {
    return { dispatch };
  },
)
class KeywordMonitor extends Component {
  state = {
    startDate: new Date().getTime() - 24 * 60 * 60 * 1000,
    endDate: date.setDate(date.getDate() + 1),
    keywords: '',
    pageNum: 1,
    queryNewsData: {},
    limit: 500,
    showDrawer: true,
  };

  handleInputChange = (e, type) => {
    this.setState({ [type]: e.target.value });
  };
  handleDateChange = (type, value) => this.setState({ [type]: value });

  handleSubmit = e => {
    this.setState({ showDrawer: false });
    const { pageNum } = this.state;
    this.getQueryNewsData(1);
  };
  getQueryNewsData = pageNum => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let { startDate, endDate, keywords } = this.state;

    let keywordArr = keywords.split(/[, ]+/);
    this.setState({ pageNum: pageNum });

    store.dispatch(toggleIsFetch(true));
    let reqData = {
      from: moment(startDate).format('MM/DD/YYYY'),
      to: moment(endDate).format('MM/DD/YYYY'),
      str: keywordArr,
      keyValue: '2f6ab741-ec55-4e96-8f4f-51db49dac186',
    };

    let reqObj = {
      url: `${keywordMonitor.queryingNews}/${pageNum}`,
      data: reqData,
      method: 'POST',
      isAuth: true,
      authToken,
    };

    fetchApi(reqObj)
      .then(data => {
        store.dispatch(setQueryNewsData(data));
        this.setState({ queryNewsData: data });
        store.dispatch(toggleIsFetch(false));
      })
      .catch(e => console.log(e));
  };

  toggleEventArchives = () =>
    this.props.dispatch(actions.toggleRimeEventArchives(false));

  handleDrawer = () => {
    this.setState({ showDrawer: true });
  };

  handleDrawerClose = () => {
    this.setState({
      showDrawer: false,
    });
  };

  render() {
    const {
      classes,
      themeChange: { setDarkTheme },
      rime: { showRimeEventArchives },
    } = this.props;
    const {
      queryNewsData: { totalResults, articles, pageNumber },
    } = this.props.keywordMonitor;
    let { isFetching } = this.props.appState;
    let { pageNum, limit, showDrawer } = this.state;

    return (
      <>
        <Drawer
          variant="top"
          classes={{
            root: classes.rootDrawer,
            paperAnchorTop: classes.paperAnchorTop,
            modal: classes.test,
          }}
          anchor="top"
          open={showDrawer}
          onClose={this.handleDrawerClose}
          className={`${setDarkTheme ? 'dark' : 'light'}`}
        >
          <div className="eventMonitor__filter">
            <div className="eventMonitor__filter__search">
              <div className="rime__KeywordMonitor__search__Tags">
                <div className="rime__KeywordMonitor__Event_ManualTags">
                  <TextField
                    id="standard-number"
                    label="Type Keywords (Separation with Commas)"
                    variant="standard"
                    type="text"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    value={this.state.keywords}
                    onChange={e => this.handleInputChange(e, 'keywords')}
                  />
                </div>
              </div>

              <div className="rime__KeywordMonitor__Event_datePicker">
                <div className="keyword__Monitor__startDate">
                  <DatePicker
                    label="Start Date"
                    disableFuture={true}
                    value={this.state.startDate}
                    onChange={value =>
                      this.handleDateChange('startDate', value)
                    }
                    renderInput={params => (
                      <TextField {...params} helperText={null} />
                    )}
                  />
                </div>
                <>&nbsp;</>
                <div className="keyword__Monitor__endDate">
                  <DatePicker
                    label="End Date"
                    disableFuture={false}
                    value={this.state.endDate}
                    onChange={value => this.handleDateChange('endDate', value)}
                    renderInput={params => (
                      <TextField {...params} helperText={null} />
                    )}
                  />
                </div>
              </div>
            </div>
            <div className="keywordMonitorsrch__Btns">
              <Button
                classes={{
                  root: classes.cancelBtnStyle,
                }}
                variant="contained"
                endIcon={<CancelIcon />}
                onClick={() => this.handleDrawerClose()}
              >
                Cancel
              </Button>
              <Button
                variant="contained"
                endIcon={<ArrowForwardIcon />}
                classes={{
                  root: classes.searchStyle,
                }}
                onClick={e => this.handleSubmit(e)}
              >
                Search Events
              </Button>
            </div>
          </div>
        </Drawer>
        <div>
          {isFetching && <Loader />}
          <div
            id="keywordMonitor"
            className={`${setDarkTheme ? 'dark' : 'light'}`}
          >
            <div className="keywordMonitor__wrap">
              <div className="keywordMonitor__data">
                <div className="keywordMonitor__filterRow">
                  <div>
                    <Tooltip title="Filter">
                      <button
                        className="btnEllipsis"
                        onClick={() => {
                          this.handleDrawer();
                        }}
                      >
                        <i className="fas fa-filter"></i>
                      </button>
                    </Tooltip>
                  </div>
                  <h3 className="totalResult">
                    Total Events: {totalResults ? totalResults : '0'}
                  </h3>
                </div>
                {totalResults ? (
                  <div className="keywordMonitor__result">
                    {articles?.length > 0 && (
                      <>
                        {articles.map(item => (
                          <div className="articles-box">
                            <div className="articles-data">
                              <h2 className="articles-title">{item.title}</h2>
                              <p className="articles-description">
                                {item.description?.substr(0, limit)}
                                {item.description?.length > limit && (
                                  <span className="limit--dots">
                                    ... &nbsp;
                                    <a href={item.newsLink} target="_blank">
                                      Read more
                                    </a>
                                  </span>
                                )}
                              </p>
                              <p>
                                <a
                                  className="articles-newslink"
                                  href={item.newsLink}
                                  target="_blank"
                                >
                                  {item.newsLink}
                                </a>
                              </p>
                              <p className="articles-dateTime">
                                <i className="far fa-calendar-alt"></i> &nbsp;
                                {moment(item.dateTime).format(
                                  'Do MMMM YYYY, h:mm:ss a',
                                )}
                              </p>
                            </div>
                            <div className="articles-img">
                              <img
                                className="img-inside"
                                height="200px"
                                width="200px"
                                src={item?.image ? item.image : noImg}
                              />
                            </div>
                          </div>
                        ))}
                      </>
                    )}
                  </div>
                ) : (
                  <div className="KeywordMonitor--no-found">
                    <img
                      src={notFound}
                      alt="No Event Found"
                      className="KeywordMonitor--no-found-image"
                    />
                  </div>
                )}
                <div className="pagination">
                  {totalResults && (
                    <PaginationFile
                      totalResults={totalResults}
                      pageNum={pageNum}
                      getQueryNewsData={this.getQueryNewsData}
                    />
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
        <Drawer
          variant="top"
          anchor="top"
          classes={{
            root: classes.rootDrawer,
            paperAnchorTop: classes.paperAnchorArchiveTop,
            modal: classes.test,
          }}
          open={showRimeEventArchives}
          className={`${setDarkTheme ? 'dark' : 'light'}`}
        >
          <EventArchives toggleEventArchives={this.toggleEventArchives} />
        </Drawer>
      </>
    );
  }
}

export default KeywordMonitor;
