import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
// import Badge from '@material-ui/core/Badge';
// import MenuIcon from '@material-ui/icons/Menu';
// import AccountCircle from '@material-ui/icons/AccountCircle';
// import MailIcon from '@material-ui/icons/Mail';
// import NotificationsIcon from '@material-ui/icons/Notifications';
// import MoreIcon from '@material-ui/icons/MoreVert';
import injectSheet from 'react-jss';
import logo from '../../../../assets/mitkat-logo-filled.png'
import './style.scss';

const styles = {
    rootAppBar: {
        // background: '#fff',
        boxShadow: 'none',
    },
    rootToolbar: {
        display: 'flex',
        justifyContent: 'space-between',
        background: '#fff',
    },
    logoWrap: {
        '& img': {
            width: 128,
        }
    },
    rootIcon: {
        color: '#777'
    }
}

const RimeHeader = (props) => {
    const { classes } = props;
    return(
        <div className={classes.grow}>
        <AppBar position="static" classes={{ root: classes.rootAppBar }}>
            <Toolbar classes={{ root: classes.rootToolbar }}>
                <div className={classes.logoWrap}>
                    {/* <img src={logo} alt="Mitkat" /> */}
                </div>   
                <div className={classes.sectionDesktop}>
                    {/* <IconButton aria-label="show 4 new mails" color="inherit">
                    <Badge badgeContent={4} color="secondary">
                        <MailIcon classes={{ root: classes.rootIcon }} />
                    </Badge>
                    </IconButton>
                    <IconButton aria-label="show 17 new notifications" color="inherit">
                    <Badge badgeContent={17} color="secondary">
                        <NotificationsIcon classes={{ root: classes.rootIcon }} />
                    </Badge>
                    </IconButton> */}
                    <IconButton
                        edge="end"
                        aria-label="account of current user"
                        // aria-controls={menuId}
                        aria-haspopup="true"
                        // onClick={handleProfileMenuOpen}
                        color="inherit"
                    >
                        <div className="userAvatar"></div>
                    </IconButton>
                </div>
            </Toolbar>
        </AppBar>
    </div>
    )
}

export default injectSheet(styles)(RimeHeader);