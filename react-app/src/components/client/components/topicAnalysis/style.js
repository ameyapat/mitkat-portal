export default {
  formControl: {
    width: '400px',
    backgroundColor: 'var(--darkGrey) !important',
  },
  rootLabel: {
    color: 'var(--whiteHighEmp) !important',
  },
  rootIcon: {
    color: 'var(--whiteHighEmp) !important',
  },
  rootInput: {
    color: 'var(--whiteHighEmp) !important',
    borderBottom: '2px solid #eee !important',

    '&:hover': {
      borderBottom: '2px solid #eee !important',
      color: 'var(--whiteHighEmp) !important ', // or black
    },
  },
  underline: {
    '&:after': {
      borderBottom: '2px solid #34495e !important',
    },
  },
  notchedOutline: {
    borderColor: 'var(--whiteHighEmp) !important ',
  },
  rootSelect: {
    color: 'var(--whiteHighEmp) !important ',
  },
  rootMenuItem: {
    backgroundColor: 'var(--darkGrey) !important',
    color: 'var(--whiteHighEmp) !important ',
  },
  rootList: {
    backgroundColor: 'var(--darkGrey) !important',
    color: 'var(--whiteHighEmp) !important ',
  },
  rootAdvisories: {
    background: 'var(--darkGrey)',
  },
  indicator: {
    display: 'none',
  },
  resourcesBox: {
    display: 'flex',
    flexDirection: 'column',
  },
  rootHeader: {
    padding: '13px 30px',
    color: 'var(--lightText)',
    background: 'var(--darkSolid)',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  rootTabs: {
    '& .MuiTabs-scroller': {
      display: 'flex',
      justifyContent: 'center',
    },
  },
  rootTab: {
    color: 'var(--whiteMediumEmp)',
    opacity: 1,
    textAlign: 'left',
    fontSize: '1em !important',
    minHeight: '35px !important',
  },
  rootAppBar: {
    background: 'var(--darkGrey)',
    boxShadow: 'none',
  },
  selectedTab: {
    borderBottom: '3px solid var(--darkActive)',
  },

  regionBox: {
    display: 'flex',
    flexDirection: 'column',
  },

  rootAppBarTab: {
    backgroundColor: 'transparent !important',
    boxShadow: 'none !important',
    flex: '100%',
    '& div': {
      '& div': {
        '& div': {
          justifyContent: 'space-around !important',
        },
      },
    },
  },

  viewBox: {
    flex: '100%',
    maxHeigth: '50vh',
    overflowY: 'scroll',
  },
  emptyWrap: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  rootSearchBar: {
    padding: 10,
    borderRadius: 25,
    display: 'flex',
    alignItems: 'center',
    width: '50%',
    margin: '0 auto 5px',

    '& input': {
      width: '100%',
      background: 'transparent',
      border: 0,
      padding: 5,
      color: 'var(--whiteMediumEmp)',
      outline: 'none',
    },
    '& span i': {
      color: 'var(--whiteMediumEmp)',
    },
  },
};
