import React, { Component } from 'react';
import { setTopicAnalysisData } from '../../../../actions/topicAnalysisActions';
import { getTopicAnalysis } from '../../../../requestor/topicAnalysis/requestor';
import { connect } from 'react-redux';
import store from '../../../../store';
import {
  AppBar,
  Button,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  Tab,
  Tabs,
  Toolbar,
} from '@material-ui/core';
import injectSheet from 'react-jss';
import styles from './style';

import { getLiveTopics } from '../../../../api/api';
import './style.scss';

import ImpactLevelGraph from './components/impactLevelGraph';
import ClientlocationGraph from './components/clientlocationGraph';
import Timeline from './components/timeline';
import Timelapse from './components/timelapse';
import LocationRatingList from './components/locationRatingList';
import Quadrant from './components/quadrant';
import Resources from './components/resources';

@injectSheet(styles)
@connect(state => {
  return {
    topicAnalysis: state.topicAnalysis.topicAnalysisData,
    partnerDetails: state.partnerDetails.partnerDetails,
  };
})
class TopicAnalysis extends Component {
  state = {
    allLiveTopics: [],
    timelineData: [],
    tabValue: 0,
  };

  componentDidMount() {
    this.getAllLiveTopics();
  }

  initDashboard(topicId) {
    getTopicAnalysis(topicId).then(data => {
      console.log('data', data);
      store.dispatch(setTopicAnalysisData(data));
      // this.setTimelineData(data);
    });
  }

  getAllLiveTopics = () => {
    getLiveTopics()
      .then(data => {
        let allLiveTopics = [];
        allLiveTopics = data.map(t => {
          let allLiveTopicsObj = {};
          allLiveTopicsObj['value'] = t.id;
          allLiveTopicsObj['label'] = t.topicName;
          return allLiveTopicsObj;
        });
        this.setState({ allLiveTopics });
      })
      .catch(e => console.log('Error fetching live topics', e));
  };

  topicListXML = () => {
    let topicListArr = [];
    const { classes } = this.props;
    let { allLiveTopics } = this.state;

    topicListArr = allLiveTopics.map(item => (
      <MenuItem
        key={item.value}
        value={item.value}
        classes={{ root: classes.rootMenuItem }}
      >
        {item.label}
      </MenuItem>
    ));

    return topicListArr;
  };

  handleChange = e => {
    let topicId = e.target.value;
    this.setState({ selectedTopicId: topicId });
    this.initDashboard(topicId);
  };
  // Tabvalue change for Region Data Show
  handleTabChange = (e, value) => {
    this.setState({
      tabValue: value,
    });
  };

  render() {
    const { classes } = this.props;
    let { whiteLogo, partnerName } = this.props.partnerDetails;
    let { allLiveTopics, tabValue } = this.state;

    const { curIdx, prevIdx } = this.state;
    const {
      timeline,
      impactLevelGraph,
      clientlocationGraph,
      locationRatingList,
      timelapse,
      resources,
    } = this.props.topicAnalysis;
    return (
      <div id="topicAnalysis">
        <h1 className={classes.rootHeader}>
          Topic Analysis{' '}
          <div className="topic-select">
            <FormControl classes={{ root: classes.formControl }}>
              <InputLabel
                classes={{
                  root: classes.rootLabel,
                }}
                htmlFor="select-topic"
              >
                Select Topic
              </InputLabel>
              <Select
                value={this.state.selectedTopic}
                className={classes.rootSelect}
                onChange={this.handleChange}
                inputProps={{
                  shrink: false,
                  name: 'Select Topic',
                  id: 'select-topic',
                  classes: {
                    root: classes.rootInput,
                    input: classes.rootInput,
                    select: classes.rootSelect,
                    icon: classes.rootIcon,
                    paper: classes.rootList,
                  },
                }}
              >
                {allLiveTopics.length > 0 && this.topicListXML()}
              </Select>
            </FormControl>
          </div>
        </h1>

        {/* <AppBar position="fixed" classes={{ root: classes.rootAppBar }}>
          <Toolbar classes={{ root: classes.rootToolbar }}>
            <div className={classes.logoWrapper}>
              <img
                src={whiteLogo}
                alt={partnerName}
                className={classes.mitkatLogo}
              />
              <h1 className={classes.logoHeader}>Topic Analysis</h1>
            </div>
            
            <Button
              classes={{ root: classes.btnLogout }}
              onClick={() => this.props.history.push('/client/dashboard')}
            >
              <i className="fas fa-home"></i>
            </Button>
          </Toolbar>
        </AppBar> */}
        {timeline?.length > 0 && (
          <div className="topicAnalysis--wrap">
            <div className={classes.rootAdvisories}>
              <div className={classes.regionBox}>
                <AppBar
                  position="static"
                  color="default"
                  classes={{ root: classes.rootAppBarTab }}
                >
                  <Tabs
                    classes={{
                      indicator: classes.indicator,
                      root: classes.rootTabs,
                    }}
                    value={tabValue}
                    onChange={this.handleTabChange}
                    variant="scrollable"
                    scrollButtons="auto"
                    centered
                  >
                    <Tab
                      classes={{
                        root: classes.rootTab,
                        selected: classes.selectedTab,
                      }}
                      label="Timeline"
                    />
                    <Tab
                      classes={{
                        root: classes.rootTab,
                        selected: classes.selectedTab,
                      }}
                      label="Impact Level Graph"
                    />
                    <Tab
                      classes={{
                        root: classes.rootTab,
                        selected: classes.selectedTab,
                      }}
                      label="Client Location Graph"
                    />
                    <Tab
                      classes={{
                        root: classes.rootTab,
                        selected: classes.selectedTab,
                      }}
                      label="Location Rating List"
                    />
                    <Tab
                      classes={{
                        root: classes.rootTab,
                        selected: classes.selectedTab,
                      }}
                      label="Timelapse"
                    />
                    {/* Note: Currently Not needed! commented for future use -- satej*/}
                    {/* <Tab
                      classes={{
                        root: classes.rootTab,
                        selected: classes.selectedTab,
                      }}
                      label="Quadrant"
                    /> */}
                    <Tab
                      classes={{
                        root: classes.rootTab,
                        selected: classes.selectedTab,
                      }}
                      label="Resources"
                    />
                    <Tab
                      classes={{
                        root: classes.rootTab,
                        selected: classes.selectedTab,
                      }}
                      label="Summary"
                    />
                  </Tabs>
                </AppBar>
                <div className="viewBox">
                  {timeline?.length > 0 && tabValue === 0 && <Timeline />}
                  {clientlocationGraph?.length > 0 && tabValue === 2 && (
                    <ClientlocationGraph />
                  )}
                  {locationRatingList?.length > 0 && tabValue === 3 && (
                    <LocationRatingList />
                  )}
                  {impactLevelGraph?.length > 0 && tabValue === 1 && (
                    <ImpactLevelGraph />
                  )}
                  {timelapse?.length > 0 && tabValue === 4 && (
                    <Timelapse timelapse={timelapse} />
                  )}
                  {/* Note: Currently Not needed! commented for future use -- satej*/}
                  {/* {timelapse?.length > 0 && tabValue === 5 && <Quadrant />} */}
                  {resources?.length > 0 && tabValue === 5 && <Resources />}
                  {tabValue === 6 && <p>Coming Soon!</p>}
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default TopicAnalysis;
