import React, { useState } from 'react';
import '../style.scss';
import Popover from '@material-ui/core/Popover';
import { RISK_LEVEL_COLORS_NEON } from '../../../../../../../helpers/constants';

const InfoIcon = () => {
  return (
    <div id="infoIcon">
      <ul className="infoList">
        <li>
          <span
            style={{
              color: RISK_LEVEL_COLORS_NEON[5],
            }}
          >
            80-100
          </span>
          : Very High Risk: Event(s)/incident(s) may pose significant risk(s)
          for the locations in view of the event/incident or may violate
          government regulations.
        </li>
        <li>
          <span
            style={{
              color: RISK_LEVEL_COLORS_NEON[4],
            }}
          >
            60-80
          </span>
          : High Risk: The event/incident may pose heightened risks for
          continued operations in view of the security situation or
          applicability of government regulations. Limited operational hours for
          businesses or complete / temporary closure of the location may be
          likely, however issue specific.
        </li>
        <li>
          <span
            style={{
              color: RISK_LEVEL_COLORS_NEON[3],
            }}
          >
            40-60
          </span>
          : Medium Risk: Increase in security measures may be likely in view of
          an adverse situation, prompting appropriate coordination and
          communication among internal stakeholders to minimise operational
          disruptions.
        </li>
        <li>
          <span
            style={{
              color: RISK_LEVEL_COLORS_NEON[2],
            }}
          >
            20-40
          </span>
          : Low Risk: Asset location(s) could face minor disruptions pertaining
          to travel / provision of essential services for continued operations.
        </li>
        <li>
          <span
            style={{
              color: RISK_LEVEL_COLORS_NEON[1],
            }}
          >
            0-20
          </span>
          : Very Low Risk: The risk of disruption from an event is minimal and
          unlikely to have any impact on the specific location(s). Documentation
          of incidents may be required.
        </li>
        <li>
          <span
            style={{
              color: RISK_LEVEL_COLORS_NEON[1],
            }}
          >
            0
          </span>
          : No Risk: No risk related incidents have been reported or are likely
          which may impact operations, assets or employees.
        </li>
      </ul>
    </div>
  );
};

export default InfoIcon;
