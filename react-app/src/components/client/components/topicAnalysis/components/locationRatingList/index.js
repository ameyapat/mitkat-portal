import React, { Component } from 'react';
import { connect } from 'react-redux';
import { RISK_LEVEL_COLORS_NEON } from '../../../../../../helpers/constants';
import InfoIcon from './infoIcon';
import './style.scss';

@connect(state => {
  return {
    topicAnalysis: state.topicAnalysis.topicAnalysisData,
  };
})
class LocationRatingList extends Component {
  renderRatings = rating => {
    let newRating = Math.floor(rating / 20);
    let menuItems = [];

    for (var i = 0; i <= newRating; i++) {
      menuItems.push(
        <span
          style={{
            backgroundColor: RISK_LEVEL_COLORS_NEON[newRating],
          }}
        ></span>,
      );
    }

    return <div>{menuItems}</div>;
  };
  render() {
    const { locationRatingList } = this.props.topicAnalysis;
    return (
      <div id="locationRatingList">
        <div className="locationRatingList--wrap">
          <h1>Location Rating List</h1>
          <div className="locationRatingList--data">
            <ul className="location--list">
              {locationRatingList.map(item => (
                <li>
                  <h4>{item.locationName} </h4>
                  <p>
                    {this.renderRatings(Math.floor(item.locationRating))} Risk
                    Rating -{Math.floor(item.locationRating)}
                    /100
                  </p>
                </li>
              ))}
            </ul>
            <InfoIcon />
          </div>
        </div>
      </div>
    );
  }
}

export default LocationRatingList;
