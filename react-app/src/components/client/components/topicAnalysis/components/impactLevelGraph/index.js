import React, { Component } from 'react';
import { Line } from 'react-chartjs-2';
import { connect } from 'react-redux';
import './style.scss';

const options = {
  maintainAspectRatio: true,
  legend: {
    display: false,
    position: 'bottom',
  },
};

const dataSetObj = {
  maxBarThickness: 10,
  label: 'Impact',
  backgroundColor: 'rgba(255, 164, 100, 0.15)',
  hoverOffset: 4,
  borderColor: 'rgba(255, 164, 100, 1)',
  borderWidth: 1,
};

@connect(state => {
  return {
    topicAnalysis: state.topicAnalysis.topicAnalysisData,
  };
})
class ImpactLevelGraph extends Component {
  state = {
    metaData: {
      datasets: [],
    },
    width: 100,
    height: 37,
  };

  componentDidMount() {
    this.initImpactLevelGraphData();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.topicAnalysis !== this.props.topicAnalysis) {
      this.initImpactLevelGraphData();
    }
  }

  initImpactLevelGraphData = () => {
    const { impactLevelGraph = [] } = this.props.topicAnalysis;
    const metaData = {
      datasets: [],
    };
    const labels = [];
    const data = [];

    impactLevelGraph.map(item => {
      labels.push(item.dateString);
      data.push(item.impactpoint);
    });

    dataSetObj.data = data;
    metaData.labels = labels;
    metaData.datasets.push(dataSetObj);

    this.setState({ metaData });
  };

  render() {
    return (
      <div id="impactLevelGraph">
        <div className="impactLevelGraph--wrap">
          <h1>Impact Level Graph</h1>
          <Line
            data={this.state.metaData}
            options={options}
            width={this.state.width}
            height={this.state.height}
          />
        </div>
      </div>
    );
  }
}

export default ImpactLevelGraph;
