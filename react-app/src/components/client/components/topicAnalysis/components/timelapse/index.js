import React, { useState } from 'react';
import CustomGoogleMap from '../customGoogleMap/CustomGoogleMap';
import customMapStyle from '../../../../../../helpers/universalMapStyles.json';
import useTimer from './helpers/useTimer';
import './style.scss';

const Timelapse = ({ timelapse }) => {
  const [frequency, setFrequency] = useState(24);

  const { time, play, stop } = useTimer({
    startTime: new Date(timelapse[0].dateString),
    endTime: new Date(timelapse[timelapse.length - 1].dateString),
    step: 1000 * 60 * 60 * 24,
    frequency,
  });

  return (
    <div id="timelapse">
      <div className="timelapse--wrap">
        <h1>Current date :{time.toLocaleDateString()}</h1>
        <button className="play" onClick={play}>
          Play
        </button>
        <button className="stop" onClick={stop}>
          Stop
        </button>

        <CustomGoogleMap
          googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyCYOTLyn6xbysArlvlW4zpxnH9p6H3An08&v=3.exp&libraries=geometry,drawing,places"
          loadingElement={<div style={{ height: `70vh` }} />}
          containerElement={
            <div style={{ height: `70vh`, position: 'relative' }} />
          }
          mapElement={<div style={{ height: `70vh` }} />}
          mapStyles={customMapStyle}
          timelapse={timelapse}
          time={time}
        />
      </div>
    </div>
  );
};

export default Timelapse;
