import React, { Component } from 'react';
import { connect } from 'react-redux';
import Plot from 'react-plotly.js';
import './style.scss';

import { setSelectedItem } from '../../../../../../actions/topicAnalysisActions';

@connect(state => {
  return {
    topicAnalysis: state.topicAnalysis.topicAnalysisData,
    selectedItem: state.topicAnalysis.selectedItem,
  };
})
class Quadrant extends Component {
  componentDidMount() {
    this.setInitialItem();
  }

  arrMonthName = () => {
    let monthnameListArr = [];
    const { quadrant } = this.props.topicAnalysis;
    quadrant.map(item => monthnameListArr.push(item.monthName));

    return monthnameListArr;
  };

  arrLikelihood = () => {
    let likelihoodListArr = [];
    const { quadrant } = this.props.topicAnalysis;
    quadrant.map(item => likelihoodListArr.push(item.likelihood));

    return likelihoodListArr;
  };

  arrImpact = () => {
    let impactListArr = [];
    const { quadrant } = this.props.topicAnalysis;
    quadrant.map(item => impactListArr.push(item.impact));

    return impactListArr;
  };

  handleOnClick = data => {
    const { points = [] } = data;
    const {
      topicAnalysis: { quadrant = [] },
    } = this.props;
    const selectedDate = points?.[0]?.x;

    this.setSelectedItem(quadrant, selectedDate);
  };

  setInitialItem = () => {
    const { quadrant } = this.props.topicAnalysis;
    this.props.dispatch(setSelectedItem(quadrant[0]));
  };

  setSelectedItem = (quadrant, selectedDate) => {
    const selectedItem = quadrant.filter(
      item => item.monthName === selectedDate,
    );

    this.props.dispatch(setSelectedItem(selectedItem[0]));
  };

  render() {
    const {
      topicAnalysis: { quadrant },
      selectedItem,
    } = this.props;

    return (
      <div id="quadrant">
        <div className="quadrant--wrap">
          <div className="quadrant--box">
            <div className="quadrant-data">
              {quadrant?.length > 0 && (
                <div className="quadrant-data__box">
                  <h1> {selectedItem?.monthName}</h1>
                  <p>
                    <label>Development </label> : {selectedItem?.development}
                  </p>

                  <p>
                    <label>Forecast </label> : {selectedItem?.forecast}
                  </p>

                  <p>
                    <label>Interplay</label> : {selectedItem?.interplay}
                  </p>
                </div>
              )}
            </div>
            <div className="quadrant-graph">
              <Plot
                data={[
                  {
                    x: this.arrMonthName(),
                    y: this.arrLikelihood(),
                    z: this.arrImpact(),
                    mode: 'markers',
                    type: 'scatter3d',
                    marker: { size: 16, color: '#00b1ea' },
                  },
                ]}
                layout={{
                  height: 500,
                  width: 900,
                  margin: {
                    l: 25,
                    r: 50,
                    b: 80,
                    t: 10,
                    pad: 4,
                  },

                  scene1: {
                    domain: {
                      x: [0.0, 0.99],
                      y: [0.5, 1],
                    },
                    camera: {
                      center: { x: 0, y: 0, z: 0 },
                      eye: { x: 2, y: 2, z: 0.1 },
                      up: { x: 0, y: 0, z: 1 },
                    },
                  },

                  scene: {
                    xaxis: {
                      title: 'Month ',
                      titlefont: {
                        family: 'Courier New, monospace',
                        size: 12,
                        color: 'red',
                      },
                    },
                    yaxis: {
                      title: 'Likelihood ',
                      titlefont: {
                        family: 'Courier New, monospace',
                        size: 12,
                        color: 'yellow',
                      },
                    },
                    zaxis: {
                      title: 'Impact ',
                      titlefont: {
                        family: 'Courier New, monospace',
                        size: 12,
                        color: 'green',
                      },
                    },
                  },
                }}
                onClick={data => this.handleOnClick(data)}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Quadrant;
