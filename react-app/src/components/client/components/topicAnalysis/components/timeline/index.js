import React, { Component } from 'react';
import { connect } from 'react-redux';
import './style.scss';
import HorizontalTimeline from 'react-horizontal-timeline';
import { RISK_LEVEL_COLORS_NEON } from '../../../../../../helpers/constants';

@connect(state => {
  return {
    topicAnalysis: state.topicAnalysis.topicAnalysisData,
  };
})
class Timeline extends Component {
  state = {
    curIdx: 0,
    prevIdx: -1,
  };
  componentDidMount() {}

  render() {
    const { curIdx, prevIdx } = this.state;
    const { timeline } = this.props.topicAnalysis;
    return (
      <div id="timeline">
        <div
          className="timeline--wrap"
          style={{
            width: '100%',
            height: '100px',
            margin: '0 auto',
            marginTop: '30px',
            fontSize: '17px',
          }}
        >
          <HorizontalTimeline
            styles={{
              background: 'var(--backgroundSolid)',
              foreground: 'var(--darkActive)',
              outline: '#7c8086',
              fontSize: '13px',
            }}
            index={this.state.curIdx}
            indexClick={index => {
              const curIdx = this.state.curIdx;
              this.setState({ curIdx: index, prevIdx: curIdx });
            }}
            values={timeline.map(x => x.date)}
          />
        </div>
        <div className="timeline--data">
          {timeline[curIdx].events.map(items => (
            <div className="event--list">
              <div>
                <h1> {items.title}</h1>
                <p> {items.description}</p>
                <span
                  className="pills"
                  style={{
                    backgroundColor: RISK_LEVEL_COLORS_NEON[items.riskLevelid],
                  }}
                >
                  {' '}
                  {items.riskCategoryName}
                </span>
              </div>
            </div>
          ))}
        </div>
      </div>
    );
  }
}

export default Timeline;
