import React from 'react';
import MapWithMarker from './MapWithMarker';

const CustomGoogleMap = ({
  googleMapURL,
  loadingElement,
  containerElement,
  mapElement,
  mapStyles,
  classes,
  impactRadius,
  time,
  timelapse,
}) => {
  return (
    <MapWithMarker
      googleMapURL={googleMapURL}
      loadingElement={loadingElement}
      containerElement={containerElement}
      mapElement={mapElement}
      mapStyles={mapStyles}
      classes={classes}
      impactRadius={impactRadius}
      timelapse={timelapse}
      time={time}
    />
  );
};

export default CustomGoogleMap;
