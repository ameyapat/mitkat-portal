import React, { Component } from 'react';
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Circle,
} from 'react-google-maps';
import CustomMarker from './CustomMarker';
import { connect } from 'react-redux';
import officesIcon from '../../../../../../assets/solid-pins/officeLocation.png';

const defaultOptions = {
  mapTypeControl: false,
  streetViewControl: false,
  zoomControl: false,
  fullscreenControl: false,
};
const circleOptions = {
  options: {
    strokeColor: '#334FC9',
    strokeOpacity: 0.8,
    strokeWeight: 1,
    fillColor: '#334FC9',
    fillOpacity: 0.15,
  },
};
@withScriptjs
@withGoogleMap
@connect(state => {
  return {
    topicAnalysis: state.topicAnalysis.topicAnalysisData,
  };
})
class MapWithMarker extends Component {
  state = {
    latState: 47.444244,
    lngState: 77.001999,
  };

  renderCoordinates = () => {
    let { timelapse } = this.props.topicAnalysis;
    let { time } = this.props;
    let newDate = new Date(time);
    let finalDate =
      (newDate.getDate() > 9 ? newDate.getDate() : '0' + newDate.getDate()) +
      ' ' +
      newDate.toLocaleString('default', { month: 'short' }) +
      ' ' +
      newDate.getFullYear();

    let index = timelapse.findIndex(p => p.dateString == finalDate);
    return timelapse[index].coordinates.map(item => {
      let lat = parseFloat(item.latitude);
      let lng = parseFloat(item.longitude);
      let impRad = item.impactRadius * 10000;

      return (
        <>
          <Circle
            defaultCenter={{
              lat,
              lng,
            }}
            radius={impRad}
            options={circleOptions.options}
          />
        </>
      );
    });
  };

  renderLocation = () => {
    let { timelapse } = this.props.topicAnalysis;
    let { time } = this.props;
    let newDate = new Date(time);
    let finalDate =
      (newDate.getDate() > 9 ? newDate.getDate() : '0' + newDate.getDate()) +
      ' ' +
      newDate.toLocaleString('default', { month: 'short' }) +
      ' ' +
      newDate.getFullYear();

    let index = timelapse.findIndex(p => p.dateString == finalDate);
    return timelapse[index].locations.map(item => {
      let lat = parseFloat(item.latitude);
      let lng = parseFloat(item.longitude);

      return (
        <>
          <CustomMarker
            id={item.id}
            key={item.id}
            lat={lat}
            lng={lng}
            customIcon={officesIcon}
          />
        </>
      );
    });
  };

  render() {
    let { mapStyles, time } = this.props;
    let { latState, lngState } = this.state;

    return (
      <>
        <GoogleMap
          defaultZoom={2.5}
          defaultOptions={{ ...defaultOptions, styles: mapStyles }}
          defaultCenter={{
            lat: latState,
            lng: lngState,
          }}
        >
          {this.renderCoordinates()}
          {this.renderLocation()}
        </GoogleMap>
      </>
    );
  }
}

export default MapWithMarker;
