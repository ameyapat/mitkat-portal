import React, { Component } from 'react';
import { connect } from 'react-redux';
import './style.scss';

@connect(state => {
  return {
    topicAnalysis: state.topicAnalysis.topicAnalysisData,
  };
})
class Resources extends Component {
  state = {
    showLink: '',
  };
  componentDidMount() {
    const { resources } = this.props.topicAnalysis;
    this.setState({ showLink: resources[0].advisoryurl });
  }
  setResources = link => {
    this.setState({ showLink: link });
  };

  render() {
    const { resources } = this.props.topicAnalysis;
    const { showLink } = this.state;
    return (
      <div id="resources">
        <div className="resources--wrap">
          <div className="advisory--row">
            <div className="advisory--row__list">
              <div>
                {resources.map(item => (
                  <div>
                    <h1>{item.title}</h1>
                    <div className="infoCard-meta-wrap">
                      <div>
                        <p>{item.uploadtime} </p>
                      </div>
                      <div className="infoCard-meta-wrap-buttons">
                        <div>
                          <a
                            target="_blank"
                            title="View"
                            onClick={() => {
                              this.setResources(item.advisoryurl);
                            }}
                          >
                            <i className="far fa-eye"></i>
                          </a>
                        </div>
                        <div>
                          <a
                            target="_blank"
                            title="Expand"
                            href={item.advisoryurl}
                          >
                            <i className="fas fa-expand"></i>
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            </div>
            <div className="embed-responsive embed-responsive-4by3 px-5">
              <iframe
                width="800"
                height="400"
                src={showLink}
                frameborder="0"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen
              ></iframe>
            </div>
          </div>

          {/* <ul>
            {resources.map(item => (
              <li>
                <h1>{item.title}</h1>
                <p>
                  {item.uploadtime}
                  
                  <a target="_blank" onClick={() => {
                                  this.setState( item.advisoryurl,
                                  );
                                }} >
                    view
                  </a>
                </p>
              </li>
            ))}
          </ul> */}
        </div>
      </div>
    );
  }
}

export default Resources;
