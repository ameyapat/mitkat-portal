import React, { Component } from 'react';
import NavMenu from '../navMenu';
import ViewSwitch from '../ViewSwitch/ViewSwitch';
class ClientSideNav extends Component {
  render() {
    return (
      <div className="clientSideNav">
        <div className="fixed--drawer">
          <ViewSwitch />
        </div>
        <div className="scrollable--drawer">
          <NavMenu />
        </div>
      </div>
    );
  }
}

export default ClientSideNav;
