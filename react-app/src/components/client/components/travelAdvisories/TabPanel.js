import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core';
import styles from './style';

const TabPanel = ({ value, children, index, classes }) => {
  return (
    <>
      {value === index && (
        <div
          role="tabpanel"
          id={`vertical-tabpanel-${index}`}
          className={classes.tabPanelRoot}
        >
          {children}
        </div>
      )}
    </>
  );
};

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

export default withStyles(styles)(TabPanel);
