export default {
  rootAdvisories: {
    background: 'transparent',
    maxWidth: '100%',
    height: '80%',
    display: 'flex',
    justifyContent: 'space-around',
    display: 'flex',
    flexDirection: 'column',
  },
  rootHeader: {
    padding: '13px 30px',
    color: 'var(--lightText)',
    background: 'var(--darkSolid)',
  },
  rootData: {
    display: 'flex',
    flexDirection: 'row',
    background: 'var(--darkGrey)',
    padding: 15,
  },
  rootMap: {
    // width: '40%',
    flex: 1,
  },
  indicator: {
    display: 'none',
  },
  rootTabs: {
    flexBasis: '200px',
    overflowY: 'hidden',
    height: `calc(100vh - 160px) !important`,
    margin: '0',
    padding: '10px',
    fontSize: 16,
    color: 'rgba(255, 255, 255, 0.87)',

    '&:hover': {
      overflowY: 'auto',
    },
    '&::-webkit-scrollbar': {
      width: 3,
    },
    '&::-webkit-scrollbar-track': {
      background: 'transparent',
    },
    '&::-webkit-scrollbar-thumb': {
      backgroundColor: '#ccc',
      borderRadius: 5,
    },
  },
  rootTabPanel: {
    flexBasis: 'calc(100vw / 2)',
    '& iframe': {
      border: 'none',
      overflowX: 'hidden',
    },
  },
  flexContainer: {
    flexDirection: 'column',
  },
  rootTab: {
    color: 'rgba(255,255,255,0.67)',
    fontSize: '1em !important',
    opacity: 1,
    '&:hover': {
      background: 'rgba(255, 255, 255, 0.9)',
      borderRadius: '5px',
      color: '#000',
    },
    '& span': {
      fontSize: '0.9em !important',
    },
  },
  rootAppBar: {
    background: 'transparent',
    boxShadow: 'none',
  },
  selectedTab: {
    background: '#fff',
    borderRadius: 5,
    color: '#333',
  },
  rootInfoCardList: {
    background: 'transparent',
    padding: '15px 0',
  },
  rootSearchBar: {
    padding: 10,
    border: '1px solid #fff',
    borderRadius: 25,
    display: 'flex',
    alignItems: 'center',
    width: '50%',
    margin: '0 auto 35px',

    '& input': {
      width: '100%',
      background: 'transparent',
      border: 0,
      padding: 5,
      color: 'var(--whiteMediumEmp)',
      outline: 'none',
    },
    '& span i': {
      color: 'var(--whiteMediumEmp)',
    },
  },
  rootClientInfo: {
    background: `linear-gradient(rgba(0, 0, 0, 0.4) 5%, rgba(0, 0, 0, 0.8) 100%, transparent)`,
  },
  tabPanelRoot: {
    height: 'calc(100vh - 100px) !important',
    overflowY: 'hidden',
    '& h1': {
      color: 'rgba(255,255,255, 0.87)',
      marginBottom: '15px',
      textAlign: 'center',
      textTransform: 'uppercase',
      fontWeight: 'normal',
    },
  },
  locationTitle: {
    fontSize: '17px',
    textAlign: 'center',
    textTransform: 'uppercase',
    marginBottom: '10px',
    color: 'rgba(255,255,255,0.8)',
  },
  rootSearchLocation: {
    marginBottom: '15px',
  },
  inputSearchLocation: {},
  iconWrap: {},
  emptyWrap: {
    color: 'rgba(255, 255, 255, 0.8)',
  },
  emptyBox: {
    justifyContent: 'center',
    height: 'calc(100vh / 2)',

    '& p': {
      color: 'rgba(255, 255, 255, 0.8)',
    },
  },
  formControl: {
    width: '100%',
    marginBottom: 15,
  },
  rootLabel: {
    color: 'var(--whiteHighEmp) !important',
  },
  rootIcon: {
    color: 'var(--whiteHighEmp) !important',
  },
  rootInput: {
    color: 'var(--whiteHighEmp) !important',
    borderBottom: '2px solid #eee !important',

    '&:hover': {
      borderBottom: '2px solid #eee !important',
      color: 'var(--whiteHighEmp) !important ', // or black
    },
  },
  underline: {
    '&:after': {
      borderBottom: '2px solid #34495e !important',
    },
  },
  notchedOutline: {
    borderColor: 'var(--whiteHighEmp) !important ',
  },
  rootSelect: {
    color: 'var(--whiteHighEmp) !important ',
  },
  rootInputWrapper: {
    display: 'flex',
  },
  inputWrap: {
    flex: 1,
    padding: 5,
  },
  rootMenuItem: {
    backgroundColor: 'var(--darkGrey) !important',
    color: 'var(--whiteHighEmp) !important ',
  },
};
