import React, { Component } from 'react';
import Tab from '@material-ui/core/Tab';
import Empty from '../../../ui/empty';
import pdfWhiteIcon from '../../../../assets/pdf-white.png';
import injectSheet from 'react-jss';
import styles from './style';
import { withCookies } from 'react-cookie';
import { fetchApi } from '../../../../helpers/http/fetch';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import MapWithAMarker from './MapWithAMarker';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';

import { getTravelCountryList } from '../../../../requestor/mitkatWeb/requestor';
import { groupBy, byCountries } from './utils';
import './style.scss';
import { connect } from 'react-redux';
@withCookies
@injectSheet(styles)
@connect(state => {
  return {
    themeChange: state.themeChange,
  };
})
class TravelAdvisories extends Component {
  state = {
    tabValue: 0,
    list: [],
    defaultList: [],
    latState: null,
    lngState: null,
    selectedCity: null,
    selectedCountry: null,
    selectedStates: [],
    allCountriesObj: {},
    allCountriesArr: [],
    citiesArr: [],
  };

  componentDidMount() {
    this.fetchList();
    getTravelCountryList().then(countries => {
      let allCountriesObj = groupBy(
        countries,
        'countryName',
        'travelAdvisories',
      );
      let allCountriesArr = byCountries(allCountriesObj);

      this.setState({
        allCountriesObj,
        allCountriesArr,
      });
    });
  }

  fetchList = () => {
    const { cookies } = this.props;
    const authToken = cookies.get('authToken');

    let reqObj = {
      url: API_ROUTES.getTravelAdvisories,
      method: 'POST',
      isAuth: true,
      authToken,
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        if (data.success) {
          this.setState(
            {
              list: data.output,
              defaultList: data.output,
            },
            () =>
              this.setState({
                latState: data.output[0].latitude,
                lngState: data.output[0].longitude,
              }),
          );
        }
      })
      .catch(e => {
        console.log(e);
      });
  };

  getCountries = () => {
    const {
      classes,
      themeChange: { setDarkTheme },
    } = this.props;
    let { allCountriesArr } = this.state;
    return allCountriesArr.map(i => {
      return (
        <MenuItem
          className={`${setDarkTheme ? 'dark' : 'light'}`}
          classes={{
            root: classes.rootMenuItem,
          }}
          value={i.value}
        >
          {i.label}
        </MenuItem>
      );
    });
  };

  getCities = () => {
    let { allCountriesObj, selectedCountry } = this.state;

    for (let city in allCountriesObj) {
      let citiesArr = allCountriesObj[selectedCountry];
      this.setState({ citiesArr });
    }
  };

  renderCities = () => {
    const {
      classes,
      themeChange: { setDarkTheme },
    } = this.props;
    const { citiesArr } = this.state;
    return citiesArr.map(i => {
      return (
        <MenuItem
          className={`${setDarkTheme ? 'dark' : 'light'}`}
          classes={{
            root: classes.rootMenuItem,
          }}
          value={i.id}
        >
          {i.locationName}
        </MenuItem>
      );
    });
  };

  handleCityChange = e => {
    let { citiesArr } = this.state;
    const id = e.target.value;
    const cityId = citiesArr.findIndex(x => x.id === e.target.value);
    this.setState({ tabValue: cityId, selectedCity: id }, () =>
      this.getLatLng(id, citiesArr),
    );
  };

  handleChangeCountry = e => {
    this.setState({ selectedCountry: e.target.value }, () => {
      this.getCities();
    });
  };

  setCenter = (lat, lng) => this.setState({ latState: lat, lngState: lng });

  getLatLng = (id, list) => {
    const cityId = list.findIndex(x => x.id === id);
    const lat = list[cityId].latitude;
    const lng = list[cityId].longitude;
    this.setCenter(lat, lng);
  };

  a11yProps = index => {
    return {
      id: `vertical-tab-${index}`,
      'aria-controls': `vertical-tabpanel-${index}`,
    };
  };

  renderTabs = list => {
    const { classes } = this.props;
    return list.map((item, idx) => (
      <Tab
        key={`${item.locationName}_${idx}`}
        classes={{ root: classes.rootTab, selected: classes.selectedTab }}
        label={item.locationName}
        {...this.a11yProps(idx)}
      />
    ));
  };

  renderTabPanel = list => {
    const { selectedCity } = this.state;
    return list
      .filter(item => item.id === selectedCity)
      .map(item => {
        return (
          <div className="pdfViewer">
            <h1>{item.locationName}</h1>
            <iframe
              src={item.url}
              title={item.locationName}
              width="100%"
              height="100%"
            />
          </div>
        );
      });
  };

  render() {
    let { classes } = this.props;
    let {
      tabValue,
      list,
      latState,
      lngState,
      hasZoom,
      allCountriesArr,
      citiesArr,
      selectedCity,
    } = this.state;
    return (
      <div className={classes.rootAdvisories}>
        <h1 className={classes.rootHeader}>City Brief</h1>
        <div className={classes.rootData}>
          <div className={classes.rootMap}>
            <div className={classes.rootInputWrapper}>
              <div className={classes.inputWrap}>
                {allCountriesArr.length > 0 && (
                  <FormControl classes={{ root: classes.formControl }}>
                    <InputLabel
                      classes={{
                        root: classes.rootLabel,
                      }}
                      htmlFor="select-country"
                    >
                      Select Country
                    </InputLabel>
                    <Select
                      value={this.state.selectedCountry}
                      className={classes.rootSelect}
                      onChange={this.handleChangeCountry}
                      inputProps={{
                        shrink: false,
                        name: 'Select Country',
                        id: 'select-country',
                        classes: {
                          root: classes.rootInput,
                          input: classes.rootInput,
                          select: classes.rootSelect,
                          icon: classes.rootIcon,
                          paper: classes.rootList,
                        },
                      }}
                    >
                      {allCountriesArr.length && this.getCountries()}
                    </Select>
                  </FormControl>
                )}
              </div>
              <div className={classes.inputWrap}>
                {citiesArr.length > 0 && (
                  <FormControl classes={{ root: classes.formControl }}>
                    <InputLabel
                      classes={{
                        root: classes.rootLabel,
                      }}
                      htmlFor="select-city"
                    >
                      Select City
                    </InputLabel>
                    <Select
                      value={this.state.selectedCity}
                      className={classes.rootSelect}
                      onChange={this.handleCityChange}
                      inputProps={{
                        shrink: false,
                        name: 'Select City',
                        id: 'select-city',
                        classes: {
                          root: classes.rootInput,
                          input: classes.rootInput,
                          select: classes.rootSelect,
                          icon: classes.rootIcon,
                          paper: classes.rootList,
                        },
                      }}
                    >
                      {this.renderCities()}
                    </Select>
                  </FormControl>
                )}
              </div>
            </div>
            {list.length > 0 ? (
              <MapWithAMarker
                googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyCYOTLyn6xbysArlvlW4zpxnH9p6H3An08&v=3.exp&libraries=geometry,drawing,places"
                loadingElement={<div style={{ height: `100%` }} />}
                containerElement={
                  <div
                    style={{
                      height: `calc(100vh - 182px)`,
                      position: 'relative',
                    }}
                  />
                }
                mapElement={<div style={{ height: `100%` }} />}
                list={list}
                latState={latState}
                lngState={lngState}
              />
            ) : (
              <p className={classes.emptyWrap}>No items available!</p>
            )}
          </div>
          <div className="boxes pdfWrapCity">
            {selectedCity ? (
              this.renderTabPanel(list)
            ) : (
              <div className={classes.emptyWrap}>
                <Empty
                  title="No Items Available"
                  imgSrc={pdfWhiteIcon}
                  classes={{ box: classes.emptyBox }}
                />
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default TravelAdvisories;
