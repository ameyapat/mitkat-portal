import React, { Component } from 'react';
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
} from 'react-google-maps';
import { connect } from 'react-redux';
import customIconCountry from '../../../../assets/icons/pin.png';
import customMapStyles from '../../../../helpers/universalMapStyles.json';
import customMapStylesLight from '../../../../helpers/universalMapStylesLight.json';

@withScriptjs
@withGoogleMap
@connect(state => {
  return {
    themeChange: state.themeChange,
  };
})
class MapWithAMarker extends Component {
  state = {
    showMarkers: false,
    showLocation: true,
    defaultOptions: {
      mapTypeControl: true,
      streetViewControl: false,
      zoomControl: false,
      fullscreenControl: false,
      draggable: true,
    },
  };
  markerRef = React.createRef();

  componentDidMount() {}

  render() {
    const { defaultOptions } = this.state;
    const {
      latState,
      lngState,
      themeChange: { setDarkTheme },
    } = this.props;
    return (
      <GoogleMap
        defaultOptions={defaultOptions}
        options={{
          ...defaultOptions,

          styles: setDarkTheme ? customMapStyles : customMapStylesLight,
        }}
        //mapTypeId = 'satellite'
        defaultZoom={9.5}
        zoom={9.5}
        defaultCenter={{ lat: latState, lng: lngState }}
        center={{ lat: latState, lng: lngState }}
        onTilesLoaded={() => this.setState({ showMarkers: true })}
      >
        <Marker
          position={{ lat: latState, lng: lngState }}
          icon={customIconCountry}
          ref={this.markerRef}
          optimized={false}
        />
      </GoogleMap>
    );
  }
}

export default MapWithAMarker;
