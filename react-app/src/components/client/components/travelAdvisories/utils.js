export function groupBy(arr, obj, by) {
  return arr.reduce((acc, currValue) => {
    let key = currValue[obj];
    if (!acc[key]) {
      acc[key] = [];
    }

    acc[key] = currValue[by];

    return acc;
  }, {});
}

export function byCountries(normalizedObj) {
  let countriesArr = [];
  for (let i in normalizedObj) {
    let country = {};
    country.label = i;
    country.value = i;
    countriesArr.push(country);
  }

  return countriesArr;
}
