import React, { useState, useEffect } from 'react';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import ExpandableImageCard from './ExpandableImageCard';
import godrejLogo from '../../../assets/godrej-logo.png';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { withStyles } from '@material-ui/core';
import { RISK_LEVEL_COLORS } from '../../../helpers/constants';
import CustomModal from '../../ui/modal';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import { fetchApi } from '../../../helpers/http/fetch';
import { API_ROUTES } from '../../../helpers/http/apiRoutes';
import { withCookies } from 'react-cookie';
import clsx from 'clsx';

const styles = () => ({
  card: {
    // width: "calc(50% - 30px)",
    width: '100%',
    minHeight: '100%',
    maxHeight: 'calc(33.33% - 30px)',
    borderRadius: 0,
  },
  rootCardHead: {
    backgroundColor: '#3da0d6',
    borderBottom: '1px solid #d3d3d3',
    padding: '10px',
    borderRadius: 0,
  },
  cardTitle: {
    color: 'var(--whiteMediumEmp)',
    fontSize: '1em',
    fontWeight: 600,
    textTransform: 'uppercase',
  },
  cardContentRoot: {
    padding: 0,
  },
  listItemBtn: {
    padding: '10px !important',
    '&:nth-child(even)': {
      backgroundColor: '#f5f5f5',
    },
  },
  rootIcon: {
    margin: '0 8px',
  },
  icon: {
    fontSize: 14,
    opacity: 1,
    borderRadius: '50%',
  },
  itemTxt: {
    padding: 0,
  },
  primaryText: {
    fontSize: '0.9rem',
  },
  rootListWrapper: {
    overflow: 'hidden',
    height: 'auto',
    padding: 0,
    '&:hover': {
      overflow: 'auto',
    },
    '&::-webkit-scrollbar-thumb': {
      backgroundColor: '#C5005E',
      borderRadius: 25,
    },
  },
  paperRoot: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    outline: 'none',
    minWidth: '1024px',
    minHeight: '600px',
    height: 'auto',
    maxHeight: '600px',
    overflowY: 'auto',
  },
  closeIconWrap: {
    display: 'flex',
    justifyContent: 'space-between',
    padding: '5px 15px',
    alignItems: 'center',
    borderBottom: '1px solid #f1f1f1',
  },
  closeIcon: {
    color: '#666',
  },
  rootPiskLevel: {
    display: 'flex',
    alignItems: 'center',
  },
  paperBody: {
    padding: '15px',
  },
  pillText: {
    display: 'inline-block',
    padding: '6px',
    marginTop: '8px',
    borderRadius: '3px',
  },
  rootLabels: {
    fontSize: '1em',
    textTransform: 'capitalize',
    color: '#73767a',
    fontWeight: 500,
  },
  blockWrap: {
    margin: '15px 0',
  },
  typographyText: {
    color: '#676767',
    margin: '5px 0',
  },
  bulletList: {
    listStyle: 'none',
    padding: 0,
    margin: 0,
  },
  bulletListItem: {
    display: 'flex',
    // alignItems: 'center',
    padding: '5px 0',
    '& i': {
      color: '#adadad',
      marginRight: '10px',
    },
    '& p': {
      color: '#73767a',
    },
  },
  rootTitle: {
    color: '#303B47',
    fontSize: '1.3em',
    fontWeight: 500,
  },
  pestelCategory: {
    backgroundColor: '#C5005E',
    color: 'var(--whiteMediumEmp)',
    opacity: 0.9,
    fontSize: '0.7em',
    fontWeight: 600,
    padding: '5px',
    borderRadius: '5px',
    marginLeft: '8px',
    textTransform: 'uppercase',
  },
  riskLevelText: {
    textTransform: 'uppercase',
    fontSize: '0.7em',
    color: '#464951',
    marginTop: 0,
  },
  anchorTag: {
    color: '#27939e',
    paddingTop: '10px',
    display: 'inline-block',
  },
  godrejLogo: {
    '& img': {
      width: '64px',
    },
  },
  imagesWrapper: {
    display: 'flex',
    justifyContent: 'space-evenly',
    marginTop: 10,
  },
});

function GodrejSingleEvent({ classes, title, eventItems, cookies }) {
  const [showModal, toggleModal] = useState(false);
  const [dynamicHeight, setDynamicHeight] = useState(null);
  const [eventDetails, setEventDetails] = useState({});
  const cardHeight = document.querySelector('#singleEventCard');

  useEffect(() => {
    if (cardHeight) {
      setDynamicHeight(
        window.innerHeight - cardHeight.getBoundingClientRect().top,
      );
    }
  }, [cardHeight]);

  function generateBulletPoints(list) {
    return list.map(item => (
      <li className={classes.bulletListItem}>
        <span className="b-list--icon">
          <i className="fas fa-circle"></i>
        </span>
        {/* <p>{item.bulletpoint}</p> */}
        <p
          dangerouslySetInnerHTML={{
            __html: item.bulletpoint.replace(/(?:\r\n|\r|\n)/g, '<br />'),
          }}
        ></p>
      </li>
    ));
  }

  function getEventDetails(id) {
    const reqObj = {
      isAuth: true,
      authToken: cookies.get('authToken'),
      url: `${API_ROUTES.viewGodrejEvent}/${id}`,
      method: 'post',
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        setEventDetails(data.output);
        toggleModal(true);
      })
      .catch(e => console.log(e));
  }

  function renderItems() {
    return eventItems.map((item, idx) => (
      <ListItem
        key={item.id}
        button
        classes={{ root: classes.listItemBtn }}
        disableGutters={true}
        onClick={() => getEventDetails(item.id)}
      >
        <div
          style={{
            display: 'flex',
            flex: 1,
          }}
        >
          <ListItemText
            classes={{ root: classes.itemTxt, primary: classes.primaryText }}
            primary={item.title}
          />
        </div>
        <div>
          <Typography
            component="span"
            style={{ backgroundColor: RISK_LEVEL_COLORS[item.risklevelid] }}
            classes={{
              root: clsx(classes.pillText, classes.riskLevelText),
            }}
          >
            {item.risklevelstring}
          </Typography>
        </div>
      </ListItem>
    ));
  }

  return (
    <Card id="singleEventCard" className={clsx(classes.card, 'card')}>
      <CardHeader
        classes={{ root: classes.rootCardHead, title: classes.cardTitle }}
        title={title}
      />
      <CardContent
        classes={{ root: classes.cardContentRoot }}
        // style={{height: `${dynamicHeight}px`}}
        // style={{height: '300px'}}
      >
        <List
          classes={{ root: classes.rootListWrapper }}
          style={{
            maxHeight: `${dynamicHeight}px`,
          }}
        >
          {eventItems.length > 0 ? (
            renderItems()
          ) : (
            <ListItem
              button
              classes={{
                root: classes.listItemBtn,
              }}
              disableGutters={false}
            >
              <ListItemText
                classes={{ root: classes.itemTxt }}
                primary={'No items available!'}
              />
            </ListItem>
          )}
        </List>
      </CardContent>
      {showModal && (
        <CustomModal
          showModal={showModal}
          closeModal={() => toggleModal(false)}
        >
          <Paper classes={{ root: classes.paperRoot }}>
            <div className={classes.closeIconWrap}>
              <span className={classes.godrejLogo}>
                <img src={godrejLogo} alt="godrej-logo" />
              </span>
              <IconButton
                color="inherit"
                aria-label="close modal"
                edge="start"
                onClick={() => toggleModal(false)}
                className={classes.closeIcon}
              >
                <CloseIcon />
              </IconButton>
            </div>
            <div className={classes.paperBody}>
              <Typography
                variant="h5"
                component="h3"
                classes={{ root: classes.rootTitle }}
              >
                {eventDetails.title}
                <span className={classes.pestelCategory}>
                  {eventDetails.pestelCategory.category}
                </span>
              </Typography>
              <div className={classes.blockWrap}>
                <Typography
                  component="span"
                  classes={{ root: classes.rootLabels }}
                >
                  Risk Type
                </Typography>
                <Typography
                  component="span"
                  classes={{
                    root: clsx(classes.pillText, classes.typographyText),
                  }}
                  style={{
                    backgroundColor: `${
                      RISK_LEVEL_COLORS[eventDetails.risklevel.id]
                    }`,
                  }}
                >
                  {eventDetails.risklevel.riskLevel}
                </Typography>
              </div>
              <div className={classes.blockWrap}>
                <Typography
                  component="span"
                  classes={{ root: classes.rootLabels }}
                >
                  Current Update
                </Typography>
                <Typography
                  component="p"
                  classes={{ root: classes.typographyText }}
                >
                  {eventDetails.recommendation}
                </Typography>
                <Typography component="div">
                  <ul className={classes.bulletList}>
                    {eventDetails.recommendationbullets &&
                      generateBulletPoints(eventDetails.recommendationbullets)}
                  </ul>
                </Typography>
              </div>
              <div className={classes.blockWrap}>
                <Typography
                  component="span"
                  classes={{ root: classes.rootLabels }}
                >
                  Details
                </Typography>
                <Typography
                  component="p"
                  classes={{ root: classes.typographyText }}
                >
                  {eventDetails.description}
                </Typography>
                <Typography component="div">
                  <ul className={classes.bulletList}>
                    {eventDetails.descriptionbullets &&
                      generateBulletPoints(eventDetails.descriptionbullets)}
                  </ul>
                </Typography>
              </div>
              <div className={classes.blockWrap}>
                <Typography
                  component="span"
                  classes={{ root: classes.rootLabels }}
                >
                  Impact
                </Typography>
                <Typography
                  component="p"
                  classes={{ root: classes.typographyText }}
                >
                  {eventDetails.impact}
                </Typography>
                <Typography component="div">
                  <ul className={classes.bulletList}>
                    {eventDetails.impactbullets &&
                      generateBulletPoints(eventDetails.impactbullets)}
                  </ul>
                </Typography>
              </div>
              <div className={classes.imagesWrapper}>
                {eventDetails.image1 && (
                  <ExpandableImageCard
                    imgSrc={eventDetails.image1}
                    imgAlt={'godrej-event'}
                  />
                )}
                {eventDetails.image2 && (
                  <ExpandableImageCard
                    imgSrc={eventDetails.image2}
                    imgAlt={'godrej-event'}
                  />
                )}
                {eventDetails.image3 && (
                  <ExpandableImageCard
                    imgSrc={eventDetails.image3}
                    imgAlt={'godrej-event'}
                  />
                )}
              </div>
              <div className={classes.blockWrap}>
                <Typography
                  component="span"
                  classes={{ root: classes.rootLabels }}
                >
                  Further Readings
                </Typography>
                <a
                  className={classes.anchorTag}
                  href={eventDetails.links}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  {eventDetails.links}
                </a>
              </div>
            </div>
          </Paper>
        </CustomModal>
      )}
    </Card>
  );
}

export default withStyles(styles)(withCookies(GodrejSingleEvent));
