import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import injectSheet from 'react-jss';
import styles from './style';
import { getEventCountries } from '../../helpers/utils';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import CountryEventData from './CountryEventData';
import { getCountryEventDetails } from '../../helpers/utils';

import { withCookies } from 'react-cookie';
import { connect } from 'react-redux';

@injectSheet(styles)
@withRouter
@withCookies
@connect(state => {
  return {
    partnerDetails: state.partnerDetails.partnerDetails,
    themeChange: state.themeChange,
  };
})
class EventCalendarDash extends Component {
  state = {
    countryEventDetails: [],
    countryEventDetailsMapped: [],
    countries: [],
    selectedCountry: 9,
  };

  componentDidMount() {
    const cookies = this.props.cookies;
    const authToken = cookies.get('authToken');
    this.getCountries(authToken);
    this.getCountryEventDetailsData(authToken, this.state.selectedCountry);
  }

  getCountries = () => {
    const cookies = this.props.cookies;
    const authToken = cookies.get('authToken');
    getEventCountries(authToken).then(countries => {
      this.setState({ countries });
    });
  };

  handleChange = e => {
    let countryId = e.target.value;
    const cookies = this.props.cookies;
    const authToken = cookies.get('authToken');
    this.setState({ selectedCountry: countryId }, () => {
      this.getCountries();
    });
    this.getCountryEventDetailsData(authToken, countryId);
  };

  getCountryXML = () => {
    const {
      classes,
      themeChange: { setDarkTheme },
    } = this.props;
    let { countries } = this.state;
    let countriesXML = [];
    countriesXML = countries.map(i => (
      <MenuItem
        classes={{
          root: classes.rootMenuItem,
        }}
        className={`${setDarkTheme ? 'dark' : 'light'}`}
        value={i.id}
      >
        {i.countryname}
      </MenuItem>
    ));
    return countriesXML;
  };

  getCountryEventDetailsData = (authToken, countryId) => {
    const countryDetailsArr = [];

    getCountryEventDetails(authToken, countryId).then(countryEventDetails => {
      countryEventDetails.map(item => {
        countryDetailsArr[item.monthName] = item.events;
      });

      this.setState({
        countryEventDetails,
        countryEventDetailsMapped: countryDetailsArr,
      });
    });
  };

  render() {
    const {
      classes,
      themeChange: { setDarkTheme },
    } = this.props;
    let {
      countries,
      countryEventDetails,
      countryEventDetailsMapped,
      selectedCountry,
    } = this.state;
    let { whiteLogo, partnerName } = this.props.partnerDetails;

    return (
      <div id="eventCalendar" className={`${setDarkTheme ? 'dark' : 'light'}`}>
        <h1 className={classes.rootHeader}>
          Event Calendar{' '}
          <FormControl classes={{ root: classes.formControl }}>
            <InputLabel
              classes={{
                root: classes.rootLabel,
              }}
              htmlFor="select-country"
            >
              Select Country
            </InputLabel>
            <Select
              value={selectedCountry}
              className={classes.rootSelect}
              defaultValue={selectedCountry}
              onChange={this.handleChange}
              inputProps={{
                shrink: false,
                name: 'Select Country',
                id: 'select-country',
                classes: {
                  root: classes.rootInput,
                  input: classes.rootInput,
                  select: classes.rootSelect,
                  icon: classes.rootIcon,
                },
              }}
            >
              {countries.length > 0 && this.getCountryXML()}
            </Select>
          </FormControl>
        </h1>
        {/* <AppBar position="fixed" classes={{ root: classes.rootAppBar }}>
          <Toolbar classes={{ root: classes.rootToolbar }}>
            <div className={classes.logoWrapper}>
              <img
                src={whiteLogo}
                alt={partnerName}
                className={classes.mitkatLogo}
              />
              <h1 className={classes.logoHeader}>Event Calendar</h1>
            </div>
            
            <Button
              classes={{ root: classes.btnLogout }}
              onClick={() => this.props.history.push('/client/dashboard')}
            >
              <i class="fas fa-home"></i>
            </Button>
          </Toolbar>
        </AppBar> */}
        {countryEventDetailsMapped && (
          <CountryEventData
            countryEventDetails={countryEventDetails}
            countryEventDetailsMapped={countryEventDetailsMapped}
          />
        )}
      </div>
    );
  }
}

export default EventCalendarDash;
