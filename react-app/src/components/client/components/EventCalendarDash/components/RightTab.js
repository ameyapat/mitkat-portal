import React from 'react';
import '../style.scss';
import EventBox from './EventBox';
import bgImg from '../assets/weekly-calendar-page-symbol.png';

const RightTab = ({ countryEventDetailsMapped }) => {
  const renderCalendar = () => {
    const monthsXML = [];
    for (let key in countryEventDetailsMapped) {
      monthsXML.push(
        <div className="monthListData" id={key}>
          <img src={bgImg} />
          <h1>
            <span>{key}</span>
          </h1>
          {countryEventDetailsMapped[key].map(month => {
            return (
              <EventBox
                key={key}
                riskLevel={month.riskLevel}
                eventdatestring={month.eventdatestring}
                validitydatestring={month.validitydatestring}
                eventName={month.eventName}
                eventType={month.eventType}
                overview={month.overview}
                risk={month.risk}
                impactedArea={month.impactedArea}
              />
            );
          })}
        </div>,
      );
    }
    return monthsXML;
  };

  return <div id="monthListData">{renderCalendar()}</div>;
};

export default RightTab;
