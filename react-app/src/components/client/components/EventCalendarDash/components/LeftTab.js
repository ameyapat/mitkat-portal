import React from 'react';
import '../style.scss';
import { Link } from 'react-scroll';
import bgImg from '../assets/calender.png';

const LeftTab = ({ countryEventDetailsMapped }) => {

    const renderMonths = () => {
        const monthsXML = [];
        for(let key in countryEventDetailsMapped){
            monthsXML.push( 
                <li> 
                    <Link 
                        activeClass="active" 
                        to={key} 
                        spy={true} 
                        smooth={true} 
                        offset={-75} 
                        duration={500} >
                        {key}
                    </Link>
                </li> 
            )
        }
        return monthsXML;
    }

    return(
        <div id="monthList" >
            <div className="monthList"> 
                <ul> 
                    <img src={bgImg} />
                    { renderMonths() }
                </ul>
            </div>
        </div>       
    )
}

export default LeftTab;