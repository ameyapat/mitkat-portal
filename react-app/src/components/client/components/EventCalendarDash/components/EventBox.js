import React from 'react';
import { RISK_LEVEL_COLORS_NEON } from '../../../../../helpers/constants';

const EventBox = ({
  riskLevel,
  eventdatestring,
  validitydatestring,
  eventName,
  eventType,
  overview,
  risk,
  impactedArea,
}) => {
  return (
    <div className="eventBox">
      <h2 className="event-header">
        <span
          style={{
            backgroundColor: `${RISK_LEVEL_COLORS_NEON[riskLevel]}`,
            border: 'none',
          }}
        />
        &nbsp; {riskLevel} {overview}
        {eventdatestring}{' '}
        {eventdatestring === validitydatestring
          ? ''
          : '-' + ' ' + validitydatestring}
        : {eventName} ({eventType})
      </h2>
      <p className="event-overview">
        <strong
          style={{
            color: `${RISK_LEVEL_COLORS_NEON[riskLevel]}`,
            border: 'none',
          }}
        >
          Overview :
        </strong>
        <span> {overview} </span>
      </p>
      <p className="event-riskdata">
        <strong
          style={{
            color: `${RISK_LEVEL_COLORS_NEON[riskLevel]}`,
            border: 'none',
          }}
        >
          Risk :
        </strong>
        <span> {risk} </span>
      </p>
      <p className="event-impactArea">
        <strong
          style={{
            color: `${RISK_LEVEL_COLORS_NEON[riskLevel]}`,
            border: 'none',
          }}
        >
          Areas likely to be impacted :
        </strong>
        <span> {impactedArea} </span>
      </p>
    </div>
  );
};

export default EventBox;
