export default {
  rootHeader: {
    padding: '13px 30px',
    color: 'var(--lightText)',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    position: 'fixed',
    width: 'calc(100% - 119px)',
    zIndex: '9',
    background: 'var(--darkSolid)',
  },

  rootTabs: {
    '& .MuiTabs-scroller': {
      display: 'flex',
      justifyContent: 'center',
    },
  },
  indicator: {
    display: 'none',
  },
  rootTab: {
    color: 'var(--whiteMediumEmp)',
    opacity: 1,
    textAlign: 'left',
    fontSize: '1em !important',
    minHeight: '35px !important',
    minWidth: '105px !important',
  },
  rootAppBar: {
    background: 'var(--darkGrey)',
    boxShadow: 'none',
    flex: '25%',
  },
  selectedTab: {
    borderBottom: '3px solid var(--darkActive)',
  },
  formControl: {
    width: '400px',
    backgroundColor: 'var(--darkSolid) !important',
  },
  rootLabel: {
    color: 'var(--lightText) !important',
  },
  rootIcon: {
    color: 'var(--lightText) !important',
  },
  rootInput: {
    color: 'var(--lightText) !important',
    borderBottom: '2px solid var(--lightText)!important',

    '&:hover': {
      borderBottom: '2px solid var(--lightText) !important',
      color: 'var(--lightText)!important ', // or black
    },
  },
  underline: {
    '&:after': {
      borderBottom: '2px solid #34495e !important',
    },
  },
  notchedOutline: {
    borderColor: 'var(--lightText) !important ',
  },
  rootSelect: {
    color: 'var(--lightText) !important ',
  },
  rootMenuItem: {
    backgroundColor: 'var(--darkGrey) !important',
    color: 'var(--whiteMediumEmp) !important ',
  },
};
