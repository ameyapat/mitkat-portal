import React, { Component } from 'react';
import LeftTab from './components/LeftTab';
import RightTab from './components/RightTab';
class CountryEventData extends Component {
 
  render() { 
    
    const { countryEventDetails, countryEventDetailsMapped } = this.props;

    return (
        <div id="countryEventDataBox"> 
            <LeftTab 
              countryEventDetails={countryEventDetails} 
              countryEventDetailsMapped={countryEventDetailsMapped}
            />
            <RightTab 
              countryEventDetails={countryEventDetails} 
              countryEventDetailsMapped={countryEventDetailsMapped}
            />
        </div>
      )
  }
}

export default CountryEventData;
