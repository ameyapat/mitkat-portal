import React, { Component } from 'react';
import MultiSelect from '../../ui/multiSelect';
import Select from 'react-select';
import Button from '@material-ui/core/Button';
import InputLabel from '@material-ui/core/InputLabel';
import { withStyles } from '@material-ui/core';

const styles = () => ({
  filterWrapper: {
    padding: '20px',
    marginBottom: '10px',
  },
  select: {
    marginBottom: '5px',
  },
  btnBlue: {
    backgroundColor: '#3079C2',
    color: 'var(--whiteMediumEmp)',
    '&:hover': {
      backgroundColor: 'rgba(48, 121, 194, 0.8)',
    },
  },
  btnWrap: {
    padding: '10px 0',
  },
  label: {
    display: 'block',
    padding: '10px 0',
  },
});

@withStyles(styles, { name: 'GodrejFilters' })
class GodrejFilters extends Component {
  checkedIsDisabled = () => {
    const {
      selectedBusinesses,
      selectedRiskLevels,
      selectedCountries,
    } = this.props;
    if (
      selectedCountries.length &&
      selectedBusinesses.length &&
      selectedRiskLevels.length
    ) {
      return false;
    } else {
      return true;
    }
  };

  render() {
    const {
      classes,
      selectedMonth,
      months,
      selectedCountries,
      countries,
      selectedBusinesses,
      businesses,
      selectedRiskLevels,
      riskLevels,
    } = this.props;
    return (
      <div className={classes.filterWrapper}>
        <InputLabel classes={{ root: classes.label }}>Select Month</InputLabel>
        <Select
          defaultValue={selectedMonth}
          className="select--options"
          value={selectedMonth}
          placeholder={'Select Month'}
          onChange={value => this.props.handleSelect('selectedMonth', value)}
          options={months || []}
        />
        {/* <MultiSelect 
                    classes={{ root: classes.select }}
                    defaultValue={selectedMonths}
                    options={months}
                    placeholder={'Select Month'}
                    parentEventHandler={value => this.props.handleSelect('selectedMonths', value)}
                /> */}
        <InputLabel classes={{ root: classes.label }}>
          Select Country
        </InputLabel>
        <MultiSelect
          classes={{ root: classes.select }}
          defaultValue={selectedCountries}
          options={countries}
          placeholder={'Select Country'}
          parentEventHandler={value =>
            this.props.handleSelect('selectedCountries', value)
          }
        />
        <InputLabel classes={{ root: classes.label }}>Select Entity</InputLabel>
        <MultiSelect
          classes={{ root: classes.select }}
          defaultValue={selectedBusinesses}
          options={businesses}
          placeholder={'Select Entity'}
          parentEventHandler={value =>
            this.props.handleSelect('selectedBusinesses', value)
          }
        />
        <InputLabel classes={{ root: classes.label }}>
          Select Risk Level
        </InputLabel>
        <MultiSelect
          classes={{ root: classes.select }}
          defaultValue={selectedRiskLevels}
          options={riskLevels}
          placeholder={'Select Risk Level'}
          parentEventHandler={value =>
            this.props.handleSelect('selectedRiskLevels', value)
          }
        />
        <div className={classes.btnWrap}>
          <Button
            variant="contained"
            onClick={() => {
              this.props.getEmergingRisks();
              this.props.getEvents();
            }}
            className={classes.btnBlue}
            disabled={this.checkedIsDisabled()}
          >
            Apply Filters
          </Button>
        </div>
      </div>
    );
  }
}

export default GodrejFilters;
