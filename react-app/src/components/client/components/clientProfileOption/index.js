import React, { Component } from 'react';
//scss
import './style.scss';
import { withCookies } from 'react-cookie';
import { withRouter } from 'react-router-dom';
import { toggleIsFetch } from '../../../../actions';
import store from '../../../../store';

@withCookies
@withRouter
class ClientProfileOption extends Component{

    handleLogout = () => {
        let { cookies } = this.props;
        store.dispatch(toggleIsFetch(true));
        cookies.remove('authToken');
        cookies.remove('userRole');
        setTimeout(() => {
            this.props.history.push('/');
            store.dispatch(toggleIsFetch(false));
        }, 3000);
      }

    render(){

        return(
            <div id="profileOptions" onClick={() => this.handleLogout()}>
                <div className="userProfileWrap">
                    <div className="user--options" title="logout">
                        <i className="fas fa-sign-out-alt"></i>
                    </div>
                </div>
            </div>
        )
    }
}

export default ClientProfileOption;