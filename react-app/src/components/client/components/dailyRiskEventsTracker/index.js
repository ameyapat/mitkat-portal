import React, { Component } from 'react';
//component
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
//helpers
import injectSheet from 'react-jss';
//style
import styles from './style';
import clsx from 'clsx';
import { withCookies } from 'react-cookie';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import { fetchApi } from '../../../../helpers/http/fetch';
import CustomModal from '../../../ui/modal';
import ModalInner from '../../../ui/modal/ModalInner';
import moment from 'moment';
import './styles.scss';
import { connect } from 'react-redux';
import GMapModal from '../GMap/GMapModal';

@withCookies
@connect(state => {
  return {
    themeChange: state.themeChange,
  };
})
class DailyRiskEventsTracker extends Component {
  state = {
    trackingId: '',
    showEachEventModal: false,
    eventDetails: [],
    dailyRiskList: [],
    defaultList: [],
    showAddEventModal: false,
    showAlert: false,
    showUploadForm: false,
  };

  handleCloseModal = () => {
    this.props.closeModal();
  };

  showEachEventModal = show => this.setState({ showEachEventModal: show });

  handleViewEvent = eventId => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');

    let reqObj = {
      method: 'POST',
      isAuth: true,
      authToken,
      url: `${API_ROUTES.getEventDetails}/${eventId}`,
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(eventDetails => {
        this.setState({ eventDetails }, () => {
          this.showEachEventModal(true);
        });
      })
      .catch(e => console.log(e));
  };

  render() {
    let { eventDetails, showEachEventModal } = this.state;
    const { items, classes, showClose = true, hasDarkTheme } = this.props;
    const {
      themeChange: { setDarkTheme },
    } = this.props;
    return (
      <div className={`model--inner ${setDarkTheme ? 'dark' : 'light'}`}>
        {showClose && (
          <div className="daily--risk--model--header">
            <h1 className="title">
              Events found on Tracking Id : {this.props.trackingId}
            </h1>
            <span className="daily--risk--btn--times" onClick={this.handleCloseModal}>
              &times;
            </span>
          </div>
        )}
        {items?.length ? (
          <div className={classes.rootDailyRisks}>
            <TableContainer className="modal--table" component={Paper}>
              <Table aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell className="table--data">Sr. No.</TableCell>
                    <TableCell align="left" className="table--data">
                      Title
                    </TableCell>
                    <TableCell align="left" className="table--data">
                      Event Date
                    </TableCell>
                    <TableCell align="left" className="table--data">
                      Actions
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {items.map((item, srNo) => (
                    <TableRow
                      key={item.id}
                      sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                    >
                      <TableCell
                        component="th"
                        scope="row"
                        className="table--data"
                      >
                        {srNo + 1}
                      </TableCell>
                      <TableCell align="left" className="table--data">
                        {item.title}
                      </TableCell>
                      <TableCell align="left" className="table--data">
                        {moment(item.datetime).format('YYYY-MM-DD HH:mm:ss')}
                      </TableCell>
                      <TableCell align="left" className="table--data">
                        <button
                          className={clsx(classes.btnView, classes.btn)}
                          onClick={() => this.handleViewEvent(item.id)}
                        >
                          <i className="fa fa-eye"></i>
                        </button>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </div>
        ) : (
          <h1 className="daily--risk--noData--model">No Data Found</h1>
        )}
        <CustomModal showModal={showEachEventModal}>
          <div className={`new__modal__inner dark--body ${'light'}`}>
            {eventDetails && (
              <GMapModal
                eventDetail={eventDetails}
                risklevel={eventDetails.risklevel}
                riskcategory={eventDetails.riskcategory}
                closeModal={() => this.showEachEventModal(false)}
                isLive={false}
                hasDarkTheme={true}
              />
            )}
          </div>
        </CustomModal>
      </div>
    );
  }
}

export default injectSheet(styles)(DailyRiskEventsTracker);