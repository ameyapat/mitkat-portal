export default {
    rootDailyRisk: {
      background: 'var(--darkGreyModal)',
      padding: 20,
    },
    rootLabel: {
      color: 'rgba(255,255,255, 0.67) !important',
    },
    riskWrapOutter: {
      '& img': {
        width: 256,
      },
    },
    rootDailyRisks: {
      padding: 20,
    },
    btnView: {
      color: '#2980b9',
      background: 'transparent',
      border: '1px solid #2980b9',
      '& .fa.fa-eye': {
        padding: '0 3px',
        color: '#2980b9',
      },
    },
    btn: {
      borderRadius: 3,
      padding: 5,
      margin: '0 2px',
      cursor: 'pointer',
    },
    txtField: {
      color: 'var(--whiteHighEmp) !important ',
      fontSize: 14,
    }
  };