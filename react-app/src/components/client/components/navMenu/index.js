import React, { Component } from 'react';
import './style.scss';
import RiskAlerts from '../riskAlerts';
import { Drawer } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import { connect } from 'react-redux';
import Copyright from '../../../ui/copyright';

const drawerWidth = 250;

const drawerstyles = theme => ({
  menuButton: {
    padding: '13px',
    position: 'absolute',
    left: '113px',
    zIndex: '9999',
    top: '8px',
    background: '#111826',
    borderRadius: '30px',
    color: 'var(--whiteMediumEmp)',

    '&:hover': {
      background: '#111826',
      borderRadius: '30px',
      color: 'var(--whiteMediumEmp)',
    },
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    background: 'var(--darkGreySolid)',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
    left: '70px !important',
    overflow: 'hidden',
  },
  drawerClose: {
    background: 'var(--darkGreySolid)',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflow: 'hidden',
    width: theme.spacing(2) + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(6) + 1,
    },
    left: '70px !important',
  },

  logoImg: {
    width: '95px',
    paddingLeft: 7,
  },

  listtext: {
    color: 'var(--whiteMediumEmp)',
  },

  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  drawerRoot: {
    backgroundColor: '#111826 !important',
    color: 'var(--whiteMediumEmp)',
    zIndex: '99999 !important',
  },
  drawerHeaderWrap: {
    width: '100%',
    padding: '25px 14px',
    height: '65px',
    '& h1': {
      color: 'var(--whiteMediumEmp)',
      whiteSpace: 'break-spaces',
    },
  },
  drawerLogoWrap: {
    width: '100%',
    padding: 15,
    position: 'absolute',
    bottom: 25,
  },

  logo: {
    width: '100px',
    margin: 'auto',
  },
  dBlock: { display: 'block' },
  dNone: { display: 'none' },
  drawerBtn: {
    textAlign: 'right',
    background: 'transparent',
    border: 'none',
    width: '98%',
    '& i': {
      top: '15px',
      color: 'var(--darkGrey)',
      width: '24px',
      cursor: 'pointer',
      position: 'fixed',
      fontSize: '10px',
      background: 'var(--darkActive)',
      borderRadius: '50%',
      height: '24px',
      padding: '8px',
    },
  },
});

@withStyles(drawerstyles, { withTheme: true })
class NavMenu extends Component {
  state = {
    showDrawer: false,
  };

  componentDidMount() {
    this.handleDrawer();
  }

  handleDrawer = () => {
    this.setState({ showDrawer: false });
  };

  render() {
    const { classes } = this.props;
    const { showDrawer } = this.state;
    let { whiteLogo, darkLogo, partnerName } = this.props.partnerDetails;
    const {
      themeChange: { setDarkTheme },
    } = this.props;
    return (
      <div id="navMenu" className={`${setDarkTheme ? 'dark' : 'light'}`}>
        <div className="navMenu">
          <Drawer
            variant="permanent"
            className={clsx(classes.drawer, {
              [classes.drawerOpen]: showDrawer,
              [classes.drawerClose]: !showDrawer,
            })}
            classes={{
              paper: clsx({
                [classes.drawerOpen]: showDrawer,
                [classes.drawerClose]: !showDrawer,
              }),
            }}
          >
            <div className={classes.drawerHeaderWrap}>
              <h1
                className={clsx(classes.header, {
                  [classes.dBlock]: showDrawer,
                  [classes.dNone]: !showDrawer,
                })}
              >
                {this.props.currentHeaderTitle}
              </h1>
            </div>
            <div className={classes.drawerLogoWrap}>
              <img
                className={clsx(classes.logo, {
                  [classes.dBlock]: showDrawer,
                  [classes.dNone]: !showDrawer,
                })}
                src={setDarkTheme ? whiteLogo : darkLogo}
                alt={partnerName}
              />
            </div>

            <button
              className={classes.drawerBtn}
              onClick={() => this.setState({ showDrawer: !showDrawer })}
            >
              {showDrawer ? (
                <i className="fas fa-chevron-left"></i>
              ) : (
                <i className="fas fa-chevron-right"></i>
              )}
            </button>
            <RiskAlerts handleDrawer={this.handleDrawer} />
            <div
              className={clsx({
                [classes.dBlock]: showDrawer,
                [classes.dNone]: !showDrawer,
              })}
            >
              <Copyright />
            </div>
          </Drawer>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    partnerDetails: state.partnerDetails.partnerDetails,
    currentHeaderTitle: state.appState.currentHeaderTitle,
    themeChange: state.themeChange,
  };
};

const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(NavMenu);
