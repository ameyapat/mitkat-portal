import React, { Component, Fragment } from 'react';
import NavListItem from './NavListItem';
import { withCookies } from 'react-cookie';
import { withRouter } from 'react-router-dom';
import './style.scss';
import {
  riskTrackerItems,
  rimeItems,
  threatIntelligencePlatformItems,
  covidDashboardItems,
} from './helpers/constants';
import { connect } from 'react-redux';
import { restrictAccess } from '../../../../actions';

@withCookies
@withRouter
class NavList extends Component {
  state = {
    createEvent: false,
    userRole: null,
  };

  componentDidMount() {
    let { cookies } = this.props;
    let userRole = cookies.get('userRole');
    this.setState({ userRole, windowHeight: window.innerHeight });
  }

  getRiskALertList = () => {
    let riskAlertXML = [];
    let {
      selectedItem,
      appState: { appPermissions },
    } = this.props;
    let pathname = this.props.match.path;
    let riskAlertOptions;

    switch (pathname) {
      case '/client/dashboard':
        riskAlertOptions = riskTrackerItems.riskAlertOptions;
        break;
      case '/client/rime':
        riskAlertOptions = rimeItems.riskAlertOptions;
        break;
      case '/client/riskExposure':
        riskAlertOptions = threatIntelligencePlatformItems.riskAlertOptions;
        break;
      case '/covid-19':
        riskAlertOptions = covidDashboardItems.riskAlertOptions;
        break;
      default:
        break;
    }

    riskAlertXML = riskAlertOptions.map((item, index) => {
      if (item.isRoute === true) {
        return (
          <NavListItem
            key={index}
            title={item.title}
            isSelected={item.type === selectedItem ? true : false}
            icon={item.icon}
            type={item.type}
            userRole={this.state.userRole}
            parentHandler={() =>
              appPermissions[item.accessName] === 1
                ? this.props.handleRoute(item.to, item.type)
                : this.props.dispatch(restrictAccess(true))
            }
            path={pathname}
            meta={{
              metaData: appPermissions[item.accessName],
              item: item,
            }}
          />
        );
      } else {
        return (
          <NavListItem
            key={index}
            title={item.title}
            icon={item.icon}
            type={item.type}
            userRole={this.state.userRole}
            parentHandler={this.handleOpenModal}
            path={pathname}
          />
        );
      }
    });
    return riskAlertXML;
  };

  handleOpenModal = type => {
    this.props.handleOpenModal(type);
  };

  render() {
    return <ul className="riskAlert--list--Horizontal">{this.getRiskALertList()}</ul>;
  }
}

const mapStateToProps = state => {
  return {
    currentHeaderTitle: state.appState.currentHeaderTitle,
    appState: state.appState,
  };
};

const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(NavList);
