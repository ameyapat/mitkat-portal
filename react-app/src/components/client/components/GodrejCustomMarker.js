import React, { Component } from 'react';
import {
    Marker,
    InfoWindow,
  } from "react-google-maps";

class GodrejCustomMarker extends Component {

    state = {
        showInfoBox : false,
    }

    markerRef = React.createRef();

    // handleShowInfoBox = (show) => {
    //     this.setState({showInfoBox: show})
    // }

  render() {

    let { id, title, lat, lng, customIcon } = this.props;
    let { showInfoBox } = this.state;

    return (
        <Marker
            // onMouseOver={() => this.handleShowInfoBox(true)}
            // onMouseOut={() => this.handleShowInfoBox(false)}
            position={{ lat, lng }}
            // onClick={() => this.handleShowInfoBox(true)}
            // onClick={isClickable ? () => this.showEventDetail(id) : () => console.log('not allowed')}
            icon={customIcon}
            ref={this.markerRef}
            optimized={false}
        >
            {
                // showInfoBox &&
                // <React.Fragment>
                //     <InfoWindow
                //         onCloseClick={() => this.handleShowInfoBox(false)}
                //     >
                //         <div className={classes.rootAlertListWrap} onClick={() => this.showEventDetail(id)}>
                //             {
                //                 showIcon &&  
                //                 <span
                //                     className="s--icon infoWindow"
                //                     style={{backgroundColor: RISK_LEVEL_COLORS[risklevel]}}
                //                 >
                //                     <i className={RISK_CATEGORY_ICONS[riskcategory]}></i>
                //                 </span>
                //             }
                //             <p className="alert--title">{ title }</p>
                //         </div>
                //     </InfoWindow>                    
                // </React.Fragment>
            }
        </Marker>
    )
  }
}

export default GodrejCustomMarker;
