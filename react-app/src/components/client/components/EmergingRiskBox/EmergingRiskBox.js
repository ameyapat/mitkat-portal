import React from 'react';
import Typography from "@material-ui/core/Typography";
import './EmergingRiskBox.scss';
import injectSheet from 'react-jss'
import GodrejSingleEvent from '../GodrejSingleEvent';

const styles = {
    rootTitle: {
        color: '#303B47',
        fontSize: '1.1em',
        fontWeight: 500,
        borderBottom: '1px solid #f1f1f1',
        borderTop: '1px solid #f1f1f1',
        padding: 10,
    }
}

const EmergingRiskBox = (props) => {
    const { emergingRiskList: { listoftitles, boxtitle } } = props;
    
    return(
        <GodrejSingleEvent
            title={boxtitle ? boxtitle : "Loading..."}
            eventItems={listoftitles ? listoftitles : []}
        />
    )
}

export default injectSheet(styles)(EmergingRiskBox)