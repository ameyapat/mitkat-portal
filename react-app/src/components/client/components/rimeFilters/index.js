import React, { Component } from 'react';
import { withCookies } from 'react-cookie';
import { withRouter } from 'react-router-dom';
import CustomModal from '../../../ui/modal';
import { connect } from 'react-redux';
import * as actions from '../../../../actions';
import './rimeFilters.scss';
import EventArchives from './EventArchives';
import TopBarFilters from './TopBarFilters';
import {
  withStyles,
  FormControl,
  Tooltip,
  Drawer,
  Divider,
} from '@material-ui/core';
const drawerWidth = '100vw';
const drawerHeight = '200px';
const styles = theme => ({
  paperAnchorTop: {
    zIndex: 996,
    position: 'absolute',
    overflowX: 'hidden',
    overflowY: 'hidden',
    width: drawerWidth,
    height: drawerHeight,
    marginTop: '8vh',
    backgroundColor: 'var(--filterBackground)',

    '&:hover': {
      overflowY: 'scroll',
    },
    '&::-webkit-scrollbar': {
      width: 3,
    },
    '&::-webkit-scrollbar-track': {
      background: 'transparent',
    },
    '&::-webkit-scrollbar-thumb': {
      backgroundColor: '#ccc',
      borderRadius: 5,
    },
  },
  test: {
    background: 'none',
  },
});

@withStyles(styles, { withTheme: true })
@withCookies
@withRouter
@connect(
  state => {
    return {
      rime: state.rime,
      themeChange: state.themeChange,
    };
  },
  dispatch => {
    return {
      dispatch: dispatch,
    };
  },
)
class RimeFilters extends Component {
  toggleEventArchives = () =>
    this.props.dispatch(actions.toggleRimeEventArchives(false));

  render() {
    const {
      classes,
      rime: { showRimeEventArchives },
      themeChange: { setDarkTheme },
    } = this.props;
    return (
      <>
        <TopBarFilters
          handleRimeFeed={this.props.handleRimeFeed}
          clearFeed={this.props.clearFeed}
        />
        <Drawer
          variant="top"
          anchor="top"
          classes={{
            root: classes.rootDrawer,
            paperAnchorTop: classes.paperAnchorTop,
            modal: classes.test,
          }}
          open={showRimeEventArchives}
          className={`${setDarkTheme ? 'dark' : 'light'}`}
        >
          <EventArchives toggleEventArchives={this.toggleEventArchives} />
        </Drawer>
      </>
    );
  }
}

export default RimeFilters;
