import React, { Component } from 'react';
import { withStyles, TextField, FormControl, Button } from '@material-ui/core';
import styles from './RimeFiltersStyles';
import { downloadRimeExcel } from '../../../admin/helpers/utils';
import { connect } from 'react-redux';
import clsx from 'clsx';
import { withCookies } from 'react-cookie';
import CustomLabel from '../../../ui/customLabel';
import { multipleAddress } from '../rimeDashboard/helpers/multipleAddress';
import MultiSelect from '../../../ui/multiSelect';
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import CustomCheckboxUi from '../../../ui/customCheckboxUi';
import CustomRelevancyCheckbox from '../../../ui/customCheckboxUi/CustomRelevancyCheckbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import {
  categories,
  relevancyConstants,
  languageConstants,
} from '../rimeDashboard/helpers/constants';
import store from '../../../../store';
import {
  updateSelectedCategories,
  updateSelectedRelevancy,
  handleSelectAllRelevancy,
  handleSelectAllRiskCat,
  handleSelectAllLanguage,
  updateSelectedLanguage,
} from '../../../../actions/rimeActions';
import CancelIcon from '@mui/icons-material/Cancel';
import DownloadIcon from '@mui/icons-material/Download';
import { operationalValue } from '../rimeSubscriptions/helpers/utils';

@withStyles(styles)
@connect(
  state => {
    return { rime: state.rime, themeChange: state.themeChange };
  },
  dispatch => {
    return { dispatch: dispatch };
  },
)
@withCookies
class EventArchives extends Component {
  state = {
    selectedAddress: '',
    address: '',
    location: {},
    riskCategories: [],
    relevancy: [],
    isCatSelected: true,
    selectAllRiskCatChecked: true,
    selectAllRelevancyChecked: true,
    selectAllLanguageChecked: true,
    showRiskCategories: false,
    showRelevancy: false,
    showLanguages: false,
    selectedlocations: [],
    refreshDurations: 60,
    allLocations: { multipleAddress },
    operations: 'OR',
    manualTags: '',
    totalHours: 1,
    relArray: [],
    catArray: [],
    language: [],
  };

  componentDidMount() {
    const { rime } = this.props;
    let cArray = [];
    let rArray = [];
    categories.map(item => {
      cArray.push(item.id);
    });
    relevancyConstants.map(item => {
      rArray.push(item.id);
    });
    let operatorValue = operationalValue(rime.operations);
    this.setState({
      riskCategories: rime.riskCategories,
      relevancy: rime.relevancy,
      refreshDurations: rime.refreshDuration,
      selectedlocations: rime.locations,
      selectedAddress: rime.locations,
      operations: operatorValue,
      manualTags: rime.tags,
      language: rime.language,
      selectAllRiskCatChecked: rime.selectAllRiskCat,
      selectAllRelevancyChecked: rime.selectAllRelevancy,
      selectAllLanguageChecked: rime.selectAllLanguage,
      catArray: cArray,
      relArray: rArray,
    });
  }

  componentDidUpdate(prevProps) {
    const { rime } = this.props;
    let operatorValue = operationalValue(rime.operations);
    if (this.props.rime !== prevProps.rime) {
      this.setState({
        riskCategories: rime.riskCategories,
        relevancy: rime.relevancy,
        selectAllRiskCatChecked: rime.selectAllRiskCat,
        selectAllRelevancyChecked: rime.selectAllRelevancy,
        selectAllLanguageChecked: rime.selectAllLanguage,
        selectedlocations: rime.locations,
        refreshDurations: rime.refreshDuration,
        manualTags: rime.tags,
        operations: operatorValue,
        selectedAddress: rime.locations,
        language: rime.language,
      });
    }
  }

  getAddress = () => {
    const { selectedAddress } = this.state;
    let addressLabel = [];
    selectedAddress.length > 0
      ? selectedAddress.map(subitem => {
          addressLabel.push(subitem.label);
        })
      : addressLabel.push('');
    return addressLabel;
  };

  getManualTags = () => {
    const { manualTags } = this.state;
    let tagList = manualTags ? manualTags.split(',') : [''];
    return tagList;
  };

  handleDownloadOptions = () => {
    const {
      riskCategories,
      relevancy,
      refreshDurations,
      operations,
      language,
    } = this.state;

    let addressLabel = this.getAddress();
    let manualTaglist = this.getManualTags();

    let reqObj = {
      riskCategory: riskCategories,
      timeRange: JSON.parse(refreshDurations),
      riskLevel: relevancy,
      address: addressLabel,
      tags: manualTaglist,
      operators: operations,
      mode: '',
      language: language,
    };

    const authToken = this.props.cookies.get('authToken');
    downloadRimeExcel(authToken, reqObj);
    this.props.toggleEventArchives();
  };

  updateCategories = id => {
    const { riskCategories, catArray } = this.state;
    if (riskCategories.includes(id)) {
      let index = riskCategories.indexOf(id);
      riskCategories.splice(index, 1);
    } else {
      riskCategories.push(id);
    }

    if (riskCategories.length === Math.max(...catArray)) {
      this.setState({ selectAllRiskCatChecked: true }, () => {
        store.dispatch(
          handleSelectAllRiskCat(this.state.selectAllRiskCatChecked),
        );
      });
    } else {
      this.setState({ selectAllRiskCatChecked: false }, () => {
        store.dispatch(
          handleSelectAllRiskCat(this.state.selectAllRiskCatChecked),
        );
      });
    }
  };

  updateRelevancy = id => {
    const { relevancy, relArray } = this.state;
    if (relevancy.includes(id)) {
      let index = relevancy.indexOf(id);
      relevancy.splice(index, 1);
    } else {
      relevancy.push(id);
    }

    if (relevancy.length === Math.max(...relArray)) {
      this.setState({ selectAllRelevancyChecked: true }, () => {
        store.dispatch(
          handleSelectAllRelevancy(this.state.selectAllRelevancyChecked),
        );
      });
    } else {
      this.setState({ selectAllRelevancyChecked: false }, () => {
        store.dispatch(
          handleSelectAllRelevancy(this.state.selectAllRelevancyChecked),
        );
      });
    }

    this.props.dispatch(updateSelectedRelevancy(relevancy));
  };

  updateLanguage = id => {
    const { language } = this.state;
    if (language.includes(id)) {
      let index = language.indexOf(id);
      language.splice(index, 1);
    } else {
      language.push(id);
    }

    if (language.length === 11) {
      this.setState({ selectAllLanguageChecked: true }, () => {
        store.dispatch(
          handleSelectAllLanguage(this.state.selectAllLanguageChecked),
        );
      });
    } else {
      this.setState({ selectAllLanguageChecked: false }, () => {
        store.dispatch(
          handleSelectAllLanguage(this.state.selectAllLanguageChecked),
        );
      });
    }

    this.props.dispatch(updateSelectedLanguage(language));
  };

  handleRiskCatSelectAll = e => {
    const { rime } = this.props;

    for (let key in rime.riskCategories) {
      rime.riskCategories[key] = e.target.checked;
    }

    this.setState({ selectAllRiskCatChecked: e.target.checked }, () => {
      store.dispatch(
        handleSelectAllRiskCat(this.state.selectAllRiskCatChecked),
      );
      store.dispatch(
        updateSelectedCategories(
          this.state.selectAllRiskCatChecked
            ? [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
            : [],
        ),
      );
    });
  };

  handleRelevancySelectAll = e => {
    const { rime } = this.props;

    for (let key in rime.relevancy) {
      rime.relevancy[key] = e.target.checked;
    }

    this.setState({ selectAllRelevancyChecked: e.target.checked }, () => {
      store.dispatch(
        handleSelectAllRelevancy(this.state.selectAllRelevancyChecked),
      );
      store.dispatch(
        updateSelectedRelevancy(
          this.state.selectAllRelevancyChecked ? [1, 2, 3] : [],
        ),
      );
    });
  };

  handleLanguageSelectAll = e => {
    const { rime } = this.props;

    for (let key in rime.language) {
      rime.language[key] = e.target.checked;
    }

    this.setState({ selectAllLanguageChecked: e.target.checked }, () => {
      store.dispatch(
        handleSelectAllLanguage(this.state.selectAllLanguageChecked),
      );
      store.dispatch(
        updateSelectedLanguage(
          this.state.selectAllLanguageChecked
            ? [
                'en',
                'hi',
                'zh',
                'ar',
                'es',
                'ur',
                'mr',
                'fr',
                'bn',
                'pt',
                'id',
                'ml',
                'ta',
                'kn',
                'ms',
                'th',
                'vi',
              ]
            : [],
        ),
      );
    });
  };

  handleMultiSelect = value => {
    this.setState({
      selectedAddress:
        value.length === 0
          ? false
          : value.length > 0
          ? value
          : this.state.allLocations,
    });
  };

  handleOperationsChange = (event, newAlignment) => {
    this.setState({ operations: newAlignment ? newAlignment : 'OR' });
  };

  handleUpdateTags = event => {
    this.setState({ manualTags: event ? event : '' });
  };

  render() {
    const {
      classes,
      themeChange: { setDarkTheme },
    } = this.props;
    const {
      riskCategories,
      relevancy,
      showRiskCategories,
      showRelevancy,
      refreshDurations,
      operations,
      manualTags,
      showLanguages,
      language,
    } = this.state;

    return (
      <div className="rime__filterDrawer">
        <div className="srchBtn">
          <Button
            variant="contained"
            endIcon={<CancelIcon />}
            className="eventFilter__CancelBtn"
            onClick={() => this.props.toggleEventArchives()}
          >
            Cancel
          </Button>
          <Button
            variant="contained"
            endIcon={<DownloadIcon />}
            className="searchStyle"
            onClick={() => this.handleDownloadOptions()}
          >
            Download
          </Button>
        </div>
        <div className="rimeFilter__row">
          <div className="filter__mLocations">
            <div className="rime__multipleLocationsParent">
              <div className="rime__multipleLocations">
                <MultiSelect
                  value={this.state.selectedAddress}
                  placeholder={'Multiple Locations'}
                  parentEventHandler={value => this.handleMultiSelect(value)}
                  options={multipleAddress}
                  allowSelectAll={false}
                  className="multiple_EventLocations"
                />
              </div>
            </div>
          </div>
          <div className="filter__otherFilters">
            <div className="filter__subRow1">
              <div className="rime__toggleOperations">
                <ToggleButtonGroup
                  color="primary"
                  value={operations}
                  exclusive
                  onChange={this.handleOperationsChange}
                >
                  <ToggleButton value="OR">OR</ToggleButton>
                  <ToggleButton value="AND">AND</ToggleButton>
                </ToggleButtonGroup>
              </div>
              <div className="rimeFilter__tags">
                <div className="rime__tags">
                  <TextField
                    id="standard-number"
                    label="Tags (Separate with Comma)"
                    variant="standard"
                    type="text"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    value={manualTags}
                    onChange={e => this.handleUpdateTags(e.target.value)}
                  />
                </div>
                <div className="rimeFilter__Language">
                  {/* <div className="rimeFilter__RiskCategorychild"> */}
                  <div
                    className={`${showLanguages &&
                      'selectAllEventCatsWhiteRisk'}`}
                  >
                    <div className="selectAllEventCats">
                      {showLanguages ? (
                        <label
                          className={classes.detailsTitleWhite}
                          onClick={() =>
                            this.setState({
                              showLanguages: !showLanguages,
                            })
                          }
                        >
                          <span>
                            <i
                              className="fa fa-globe"
                              style={{
                                color: 'var(--whiteDisabled)',
                                paddingRight: '5px',
                              }}
                            ></i>
                          </span>
                          Language
                        </label>
                      ) : (
                        <label
                          className={classes.detailsTitle}
                          onClick={() =>
                            this.setState({
                              showLanguages: !showLanguages,
                            })
                          }
                        >
                          <span>
                            <i
                              className="fa fa-globe"
                              style={{
                                color: 'var(--whiteDisabled)',
                                paddingRight: '5px',
                              }}
                            ></i>
                          </span>
                          Language
                        </label>
                      )}
                      <span
                        className="filtersWrapBox__arrow"
                        onClick={() =>
                          this.setState({
                            showLanguages: !showLanguages,
                          })
                        }
                      >
                        {showLanguages ? (
                          <i
                            className="fas fa-chevron-up"
                            style={{ color: 'var(--whiteDisabled)' }}
                          ></i>
                        ) : (
                          <i
                            className="fas fa-chevron-down"
                            style={{ color: 'var(--whiteDisabled)' }}
                          ></i>
                        )}
                      </span>
                    </div>
                    {showLanguages && (
                      <>
                        <div className="rimeFilter__languageChild">
                          <span style={{ margin: '0 10px' }}>
                            <FormControlLabel
                              classes={{
                                label: classes.label,
                              }}
                              control={
                                <Checkbox
                                  className={classes.size}
                                  checked={this.state.selectAllLanguageChecked}
                                  onClick={this.handleLanguageSelectAll}
                                  value={this.state.selectAllLanguageChecked}
                                  icon={
                                    <CheckBoxOutlineBlankIcon
                                      className={classes.sizeIcon}
                                    />
                                  }
                                  checkedIcon={
                                    <CheckBoxIcon
                                      className={classes.sizeIcon}
                                    />
                                  }
                                  style={{ color: 'black' }}
                                />
                              }
                              label={'Select All'}
                            />
                          </span>
                          {languageConstants.map(item => (
                            <CustomCheckboxUi
                              key={item.id}
                              id={item.id}
                              label={item.label}
                              updateCategories={this.updateLanguage}
                              isSelected={language.includes(item.id)}
                              name={item.name}
                              ref={this.categoriesRef}
                            />
                          ))}
                        </div>
                      </>
                    )}
                  </div>
                  {/* </div> */}
                </div>
              </div>
            </div>
            <div className="filter__subRow2">
              <div className="rimeFilter__RiskCategory">
                {/* <div className="rimeFilter__RiskCategorychild"> */}
                <div
                  className={`${showRiskCategories &&
                    'selectAllEventCatsWhiteRisk'}`}
                >
                  <div className="selectAllEventCats">
                    {showRiskCategories ? (
                      <label
                        className={classes.detailsTitleWhite}
                        onClick={() =>
                          this.setState({
                            showRiskCategories: !showRiskCategories,
                          })
                        }
                      >
                        <span>
                          <i className="icon-uniE90E" aria-hidden="true"></i>
                        </span>
                        Risk Categories
                      </label>
                    ) : (
                      <label
                        className={classes.detailsTitle}
                        onClick={() =>
                          this.setState({
                            showRiskCategories: !showRiskCategories,
                          })
                        }
                      >
                        <span>
                          <i
                            className="icon-uniE90E"
                            aria-hidden="true"
                            style={{ color: 'var(--whiteDisabled)' }}
                          ></i>
                        </span>
                        Risk Categories
                      </label>
                    )}
                    <span
                      className="filtersWrapBox__arrow"
                      onClick={() =>
                        this.setState({
                          showRiskCategories: !showRiskCategories,
                        })
                      }
                    >
                      {showRiskCategories ? (
                        <i className="fas fa-chevron-up"></i>
                      ) : (
                        <i className="fas fa-chevron-down"></i>
                      )}
                    </span>
                  </div>
                  {showRiskCategories && (
                    <>
                      <div className="rimeFilter__RiskCategorychild">
                        <span style={{ margin: '0 10px' }}>
                          <FormControlLabel
                            classes={{
                              label: classes.label,
                            }}
                            control={
                              <Checkbox
                                className={classes.size}
                                checked={this.state.selectAllRiskCatChecked}
                                onClick={this.handleRiskCatSelectAll}
                                value={this.state.selectAllRiskCatChecked}
                                icon={
                                  <CheckBoxOutlineBlankIcon
                                    className={classes.sizeIcon}
                                  />
                                }
                                checkedIcon={
                                  <CheckBoxIcon className={classes.sizeIcon} />
                                }
                                style={{ color: 'black' }}
                              />
                            }
                            label={'Select All'}
                          />
                        </span>
                        {categories.map(item => (
                          <CustomCheckboxUi
                            key={item.id}
                            id={item.id}
                            label={item.label}
                            updateCategories={this.updateCategories}
                            isSelected={riskCategories.includes(item.id)}
                            name={item.name}
                            ref={this.categoriesRef}
                          />
                        ))}
                      </div>
                    </>
                  )}
                </div>
                {/* </div> */}
              </div>
              <div className="rimeFilter__Relevancy">
                <div className="rimeFilter__RelevancyChild">
                  <div
                    className={`${showRelevancy &&
                      'selectAllEventCatsWhiteRisk'}`}
                  >
                    <div className="selectAllEventCats">
                      {showRelevancy ? (
                        <label
                          className={classes.detailsTitleWhite}
                          onClick={() =>
                            this.setState({ showRelevancy: !showRelevancy })
                          }
                        >
                          <span>
                            <i className="icon-uniE95E" aria-hidden="true"></i>
                          </span>
                          Relevancy
                        </label>
                      ) : (
                        <label
                          className={classes.detailsTitle}
                          onClick={() =>
                            this.setState({ showRelevancy: !showRelevancy })
                          }
                        >
                          <span>
                            <i className="icon-uniE95E" aria-hidden="true"></i>
                          </span>
                          Relevancy
                        </label>
                      )}
                      <span
                        className="filtersWrapBox__arrow"
                        onClick={() =>
                          this.setState({ showRelevancy: !showRelevancy })
                        }
                      >
                        {showRelevancy ? (
                          <i className="fas fa-chevron-up"></i>
                        ) : (
                          <i className="fas fa-chevron-down"></i>
                        )}
                      </span>
                    </div>
                    {showRelevancy && (
                      <>
                        <span style={{ margin: '0 10px' }}>
                          <FormControlLabel
                            classes={{
                              label: classes.label,
                            }}
                            control={
                              <Checkbox
                                className={classes.size}
                                checked={this.state.selectAllRelevancyChecked}
                                onClick={this.handleRelevancySelectAll}
                                value={this.state.selectAllRelevancyChecked}
                                icon={
                                  <CheckBoxOutlineBlankIcon
                                    className={classes.sizeIcon}
                                  />
                                }
                                checkedIcon={
                                  <CheckBoxIcon className={classes.sizeIcon} />
                                }
                              />
                            }
                            label={'Select All'}
                          />
                        </span>
                        {relevancyConstants.map(item => (
                          <CustomRelevancyCheckbox
                            key={item.id}
                            id={item.id}
                            label={item.label}
                            updateRelevancy={this.updateRelevancy}
                            isSelected={relevancy.includes(item.id)}
                            name={item.name}
                          />
                        ))}
                      </>
                    )}
                  </div>
                </div>
              </div>
              <div className="Event_RefreshDuration">
                <TextField
                  id="totalHours"
                  label="Total Minutes"
                  variant="standard"
                  type="number"
                  max
                  InputLabelProps={{
                    shrink: true,
                  }}
                  onInput={e => {
                    e.target.value = Math.max(0, parseInt(e.target.value))
                      .toString()
                      .slice(0, 4);
                  }}
                  value={refreshDurations}
                  onChange={e =>
                    this.setState({ refreshDurations: e.target.value })
                  }
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default EventArchives;
