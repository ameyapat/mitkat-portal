import React, { Component } from 'react';
import { withStyles, TextField, FormControl } from '@material-ui/core';
import styles from './RimeFiltersStyles';
import CustomCheckboxUi from '../../../ui/customCheckboxUi';
import CustomRelevancyCheckbox from '../../../ui/customCheckboxUi/CustomRelevancyCheckbox';
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';
import { categories, relevancyConstants } from '../rimeDashboard/helpers/constants';
import store from '../../../../store';
import { toggleToast, toggleIsFetch } from '../../../../actions/';
import {
  updateSelectedCategories,
  updateSelectedRelevancy,
  handleSelectAllRelevancy,
  handleSelectAllRiskCat,
} from '../../../../actions/rimeActions';
import {
  setBoundsAction,
  setManualBoundsAction,
} from '../../../../actions/rimeActions';
import { downloadRimeExcel } from '../../../admin/helpers/utils';
import { connect } from 'react-redux';
import clsx from 'clsx';
import { withCookies } from 'react-cookie';
import CustomLabel from '../../../ui/customLabel';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';

@withStyles(styles)
@connect(
  state => {
    return { rime: state.rime };
  },
  dispatch => {
    return { dispatch: dispatch };
  },
)
@withCookies
class Filters extends Component {
  state = {
    address: '',
    placeBounds: {},
    location: {},
    riskCategories: [],
    totalHours: 1,
    relevancy: [],
    isCatSelected: true,
    selectAllRiskCatChecked: true,
    selectAllRelevancyChecked: true,
    minLat: '',
    minLng: '',
    maxLat: '',
    maxLng: '',
    showRiskCategories: false,
    showRelevancy: false,
  };

  categoriesRef = React.createRef();

  componentDidMount() {
    const { rime } = this.props;
    this.setState({
      riskCategories: rime.riskCategories,
      relevancy: rime.relevancy,
      selectAllRiskCatChecked: rime.selectAllRiskCat,
      selectAllRelevancyChecked: rime.selectAllRelevancy,
    });
  }

  componentDidUpdate(prevProps) {
    const { rime } = this.props;
    if (this.props.rime !== prevProps.rime) {
      this.setState({
        riskCategories: rime.riskCategories,
        relevancy: rime.relevancy,
        selectAllRiskCatChecked: rime.selectAllRiskCat,
        selectAllRelevancyChecked: rime.selectAllRelevancy,
      });
    }
  }

  handleApplyFilters = () => {
    this.props.dispatch(toggleIsFetch(true));
    this.props.handleRimeFeed();
    this.props.clearFeed();
    this.props.toggleFilters();
  };

  getPlacesBounds = address => {
    let reqObj = {
      url: `https://maps.googleapis.com/maps/api/geocode/json?address=${address}&key=AIzaSyCYOTLyn6xbysArlvlW4zpxnH9p6H3An08&v=3.exp`,
      method: 'GET',
      isAuth: false,
    };

    fetch(reqObj.url)
      .then(data => data.json())
      .then(res => {
        let placeBounds = res.results[0].geometry.viewport;
        this.setState({ placeBounds }, () => {
          store.dispatch(setBoundsAction(this.state.placeBounds));
        });
      })
      .catch(e => console.log(e));
  };

  handleAddressChange = address => {
    this.setState({ address });
  };

  handleSelect = address => {
    geocodeByAddress(address)
      .then(results => getLatLng(results[0]))
      .then(latLng => {
        let { location } = this.state;
        location['lat'] = latLng.lat;
        location['lng'] = latLng.lng;
        this.getPlacesBounds(address);
        this.setState({ location, address });
      })
      .catch(error => {
        let toastObj = {
          showToast: true,
          toastObj: 'Error getting location, try again!',
        };
        store.dispatch(toggleToast(toastObj));
      });
  };

  updateCategories = id => {
    const { riskCategories } = this.state;
    if (riskCategories.includes(id)) {
      let index = riskCategories.indexOf(id);
      riskCategories.splice(index, 1);
    } else {
      riskCategories.push(id);
    }

    if (riskCategories.length === 8) {
      this.setState({ selectAllRiskCatChecked: true }, () => {
        store.dispatch(
          handleSelectAllRiskCat(this.state.selectAllRiskCatChecked),
        );
      });
    } else {
      this.setState({ selectAllRiskCatChecked: false }, () => {
        store.dispatch(
          handleSelectAllRiskCat(this.state.selectAllRiskCatChecked),
        );
      });
    }

    this.props.dispatch(updateSelectedCategories(riskCategories));
  };

  handleDownloadOptions = () => {
    const authToken = this.props.cookies.get('authToken');
    downloadRimeExcel(authToken, this.state.totalHours);
    this.props.toggleFilters();
  };

  updateRelevancy = id => {
    const { relevancy } = this.state;
    if (relevancy.includes(id)) {
      let index = relevancy.indexOf(id);
      relevancy.splice(index, 1);
    } else {
      relevancy.push(id);
    }

    if (relevancy.length === 3) {
      this.setState({ selectAllRelevancyChecked: true }, () => {
        store.dispatch(
          handleSelectAllRelevancy(this.state.selectAllRelevancyChecked),
        );
      });
    } else {
      this.setState({ selectAllRelevancyChecked: false }, () => {
        store.dispatch(
          handleSelectAllRelevancy(this.state.selectAllRelevancyChecked),
        );
      });
    }

    this.props.dispatch(updateSelectedRelevancy(relevancy));
  };

  handleRiskCatSelectAll = e => {
    const { rime } = this.props;

    for (let key in rime.riskCategories) {
      rime.riskCategories[key] = e.target.checked;
    }

    this.setState({ selectAllRiskCatChecked: e.target.checked }, () => {
      store.dispatch(
        handleSelectAllRiskCat(this.state.selectAllRiskCatChecked),
      );
      store.dispatch(
        updateSelectedCategories(
          this.state.selectAllRiskCatChecked ? [0, 1, 2, 3, 4, 5, 6, 7] : [],
        ),
      );
    });
  };

  handleRelevancySelectAll = e => {
    const { rime } = this.props;

    for (let key in rime.relevancy) {
      rime.relevancy[key] = e.target.checked;
    }

    this.setState({ selectAllRelevancyChecked: e.target.checked }, () => {
      store.dispatch(
        handleSelectAllRelevancy(this.state.selectAllRelevancyChecked),
      );
      store.dispatch(
        updateSelectedRelevancy(
          this.state.selectAllRelevancyChecked ? [1, 2, 3] : [],
        ),
      );
    });
  };

  handleManualBounds = (type, value) => {
    const {
      dispatch,
      rime: { manualPlaceBounds },
    } = this.props;

    manualPlaceBounds[type] = value;

    dispatch(setManualBoundsAction(manualPlaceBounds));
  };

  render() {
    const {
      classes,
      rime: {
        manualPlaceBounds: { latMin, latMax, lngMin, lngMax },
      },
    } = this.props;
    const {
      riskCategories,
      relevancy,
      showRiskCategories,
      showRelevancy,
    } = this.state;

    return (
      <div className={classes.rootFilters}>
        <div className="rootFilters__body">
          <div className="searchCont">
            <div className="placesBoundWrap">
              <div className="placesBound">
                <FormControl
                  variant="outlined"
                  classes={{
                    root: classes.formControl,
                  }}
                >
                  <TextField
                    id="latMin"
                    label="Min Latitude"
                    type="text"
                    InputLabelProps={{
                      shrink: true,
                      classes: {
                        root: classes.inputLabelRoot,
                        focused: classes.inputLabelFocused,
                      },
                    }}
                    variant="outlined"
                    className={classes.totalHoursText}
                    fullWidth
                    InputProps={{
                      classes: {
                        root: classes.rootTextInput,
                        input: classes.rootInput,
                        formControl: classes.inputFormControl,
                        notchedOutline: classes.notchedOutline,
                      },
                    }}
                    value={latMin}
                    onChange={e =>
                      this.handleManualBounds('latMin', e.target.value)
                    }
                  />
                </FormControl>
              </div>

              <div className="placesBound">
                <FormControl
                  variant="outlined"
                  classes={{
                    root: classes.formControl,
                  }}
                >
                  <TextField
                    id="latMax"
                    label="Max Latitude"
                    type="text"
                    InputLabelProps={{
                      shrink: true,
                      classes: {
                        root: classes.inputLabelRoot,
                        focused: classes.inputLabelFocused,
                      },
                    }}
                    variant="outlined"
                    className={classes.totalHoursText}
                    fullWidth
                    InputProps={{
                      classes: {
                        root: classes.rootTextInput,
                        input: classes.rootInput,
                        formControl: classes.inputFormControl,
                        notchedOutline: classes.notchedOutline,
                      },
                    }}
                    value={latMax}
                    onChange={e =>
                      this.handleManualBounds('latMax', e.target.value)
                    }
                  />
                </FormControl>
              </div>
              <div className="placesBound">
                <FormControl
                  variant="outlined"
                  classes={{
                    root: classes.formControl,
                  }}
                >
                  <TextField
                    id="lngMin"
                    label="Min Longitude"
                    type="text"
                    InputLabelProps={{
                      shrink: true,
                      classes: {
                        root: classes.inputLabelRoot,
                        focused: classes.inputLabelFocused,
                      },
                    }}
                    variant="outlined"
                    className={classes.totalHoursText}
                    fullWidth
                    InputProps={{
                      classes: {
                        root: classes.rootTextInput,
                        input: classes.rootInput,
                        formControl: classes.inputFormControl,
                        notchedOutline: classes.notchedOutline,
                      },
                    }}
                    value={lngMin}
                    onChange={e =>
                      this.handleManualBounds('lngMin', e.target.value)
                    }
                  />
                </FormControl>
              </div>
              <div className="placesBound">
                <FormControl
                  variant="outlined"
                  classes={{
                    root: classes.formControl,
                  }}
                >
                  <TextField
                    id="lngMax"
                    label="Max Longitude"
                    type="text"
                    InputLabelProps={{
                      shrink: true,
                      classes: {
                        root: classes.inputLabelRoot,
                        focused: classes.inputLabelFocused,
                      },
                    }}
                    variant="outlined"
                    className={classes.totalHoursText}
                    fullWidth
                    InputProps={{
                      classes: {
                        root: classes.rootTextInput,
                        input: classes.rootInput,
                        formControl: classes.inputFormControl,
                        notchedOutline: classes.notchedOutline,
                      },
                    }}
                    value={lngMax}
                    onChange={e =>
                      this.handleManualBounds('lngMax', e.target.value)
                    }
                  />
                </FormControl>
              </div>
            </div>
          </div>
          <div className="rootFilters__filtersCont">
            <div className="filtersCont__boxOne filtersCont__boxes">
              <div
                className={clsx(
                  classes.rootCategories,
                  'filtersCont__boxes__inner',
                )}
              >
                <div className="selectAllCats">
                  <label className={classes.detailsTitle}>
                    <span>
                      <i className="icon-uniE90E" aria-hidden="true"></i>
                    </span>{' '}
                    Risk Categories
                  </label>
                  <span
                    className="filtersWrapBox__arrow"
                    onClick={() =>
                      this.setState({ showRiskCategories: !showRiskCategories })
                    }
                  >
                    {showRiskCategories && (
                      <i className="fas fa-chevron-up"></i>
                    )}
                    {!showRiskCategories && (
                      <i className="fas fa-chevron-down"></i>
                    )}
                  </span>
                </div>
                {showRiskCategories && (
                  <>
                    <span style={{ margin: '0 10px' }}>
                      <FormControlLabel
                        classes={{
                          label: classes.label,
                        }}
                        control={
                          <Checkbox
                            className={classes.size}
                            checked={this.state.selectAllRiskCatChecked}
                            onClick={this.handleRiskCatSelectAll}
                            value={this.state.selectAllRiskCatChecked}
                            icon={
                              <CheckBoxOutlineBlankIcon
                                className={classes.sizeIcon}
                              />
                            }
                            checkedIcon={
                              <CheckBoxIcon className={classes.sizeIcon} />
                            }
                          />
                        }
                        label={'Select All'}
                      />
                    </span>
                    {categories.map(item => (
                      <CustomCheckboxUi
                        key={item.id}
                        id={item.id}
                        label={item.label}
                        updateCategories={this.updateCategories}
                        isSelected={riskCategories.includes(item.id)}
                        name={item.name}
                        ref={this.categoriesRef}
                      />
                    ))}
                  </>
                )}
              </div>
            </div>
            <div className="filtersCont__boxTwo filtersCont__boxes">
              <div
                className={clsx(
                  classes.rootCategories,
                  'filtersCont__boxes__inner',
                )}
              >
                <div className="selectAllCats">
                  <label className={classes.detailsTitle}>
                    <span>
                      <i className="icon-uniE95E" aria-hidden="true"></i>
                    </span>
                    Relevancy
                  </label>
                  <span
                    className="filtersWrapBox__arrow"
                    onClick={() =>
                      this.setState({ showRelevancy: !showRelevancy })
                    }
                  >
                    {showRelevancy && <i className="fas fa-chevron-up"></i>}
                    {!showRelevancy && <i className="fas fa-chevron-down"></i>}
                  </span>
                </div>
                {showRelevancy && (
                  <>
                    {' '}
                    <span style={{ margin: '0 10px' }}>
                      <FormControlLabel
                        classes={{
                          label: classes.label,
                        }}
                        control={
                          <Checkbox
                            className={classes.size}
                            checked={this.state.selectAllRelevancyChecked}
                            onClick={this.handleRelevancySelectAll}
                            value={this.state.selectAllRelevancyChecked}
                            icon={
                              <CheckBoxOutlineBlankIcon
                                className={classes.sizeIcon}
                              />
                            }
                            checkedIcon={
                              <CheckBoxIcon className={classes.sizeIcon} />
                            }
                          />
                        }
                        label={'Select All'}
                      />
                    </span>
                    {relevancyConstants.map(item => (
                      <CustomRelevancyCheckbox
                        key={item.id}
                        id={item.id}
                        label={item.label}
                        updateRelevancy={this.updateRelevancy}
                        isSelected={relevancy.includes(item.id)}
                        name={item.name}
                      />
                    ))}
                  </>
                )}
              </div>
            </div>
          </div>
        </div>
        <div className="rootFilters__footer">
          <button
            className={classes.filterBtn}
            onClick={() => this.handleApplyFilters()}
          >
            Apply Filters
          </button>
        </div>
      </div>
    );
  }
}

export default Filters;
