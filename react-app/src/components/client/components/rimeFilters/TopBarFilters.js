import React, { Component } from 'react';
import MultiSelect from '../../../ui/multiSelect';
import { multipleAddress } from '../rimeDashboard/helpers/multipleAddress';
import {
  withStyles,
  TextField,
  FormControl,
  Tooltip,
  Drawer,
  Divider,
  Button,
} from '@material-ui/core';
import style from './RimeFiltersStyles';
import CustomCheckboxUi from '../../../ui/customCheckboxUi';
import CustomRelevancyCheckbox from '../../../ui/customCheckboxUi/CustomRelevancyCheckbox';
import {
  categories,
  relevancyConstants,
  languageConstants,
} from '../rimeDashboard/helpers/constants';
import store from '../../../../store';
import { toggleToast, toggleIsFetch } from '../../../../actions/';
import {
  updateSelectedCategories,
  updateSelectedRelevancy,
  handleSelectAllRelevancy,
  handleSelectAllRiskCat,
  updateSelectedLocations,
  updateRefreshDuration,
  updateOperations,
  updateTags,
  handleSelectAllLanguage,
  updateSelectedLanguage,
  showApiHitTime,
} from '../../../../actions/rimeActions';
import { connect } from 'react-redux';
import { withCookies } from 'react-cookie';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import * as actions from '../../../../actions';
import injectSheet from 'react-jss/lib/injectSheet';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import CancelIcon from '@mui/icons-material/Cancel';
import { operationalValue } from '../rimeSubscriptions/helpers/utils';

const drawerWidth = '100vw';
const drawerHeight = '200px';
const min = 1;

const styles = theme => ({
  iconOptions: {
    display: 'flex',
    justifyContent: 'flex-end',
    padding: 3,
    minHeight: 40,
  },
  closed: {
    justifyContent: 'space-around',
  },
  chevIcons: {
    color: 'var(--whiteMediumEmp)',
  },
  menuIcon: {
    color: 'var(--whiteMediumEmp)',
  },
  paperAnchorTop: {
    overflowX: 'hidden',
    overflowY: 'scroll',
    width: drawerWidth,
    height: 'auto',
    minHeight: drawerHeight,
    maxHeight: '92vh',
    marginTop: '8vh',
    backgroundColor: 'var(--filterBackground)',
    '&::-webkit-scrollbar': {
      width: 3,
    },
    '&::-webkit-scrollbar-track': {
      background: 'transparent',
    },
    '&::-webkit-scrollbar-thumb': {
      backgroundColor: '#ccc',
      borderRadius: 5,
    },
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 36,
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing.unit * 7 + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing.unit * 9 + 1,
    },
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
  },
  menuItemBtn: {
    padding: 6,
  },
  test: {
    background: 'none',
  },
  searchStyle: {
    backgroundColor: '#02385a',
    color: 'white',
    height: 42,
    width: 165,
    marginLeft: 10,
    '&:hover': {
      background: '#A0B63C',
    },
  },
  cancelBtnStyle: {
    backgroundColor: '#111826',
    color: 'white',
    height: 42,
    width: 115,
    paddingRight: '20px',
    '&:hover': {
      background: '#E35A5A',
    },
  },
  multipleLocation: {
    height: 100,
  },
});
@withStyles(styles, { withTheme: true })
@injectSheet(style)
@connect(
  state => {
    return { rime: state.rime, themeChange: state.themeChange };
  },
  dispatch => {
    return { dispatch: dispatch };
  },
)
@withCookies
class TopBarFilters extends Component {
  state = {
    selectedAddress: '',
    address: '',
    location: {},
    riskCategories: [],
    relevancy: [],
    isCatSelected: true,
    selectAllRiskCatChecked: true,
    selectAllRelevancyChecked: true,
    selectAllLanguageChecked: true,
    showRiskCategories: false,
    showRelevancy: false,
    showLanguages: false,
    selectedlocations: [],
    refreshDurations: 60,
    allLocations: { multipleAddress },
    operations: 'OR',
    manualTags: '',
    catArray: [],
    relArray: [],
    showDrawer: false,
    language: [],
  };

  categoriesRef = React.createRef();

  componentDidMount() {
    const { rime } = this.props;
    let cArray = [];
    let rArray = [];
    categories.map(item => {
      cArray.push(item.id);
    });
    relevancyConstants.map(item => {
      rArray.push(item.id);
    });
    let operatorValue = operationalValue(rime.operations);
    this.setState({
      riskCategories: rime.riskCategories,
      relevancy: rime.relevancy,
      selectAllRiskCatChecked: rime.selectAllRiskCat,
      selectAllRelevancyChecked: rime.selectAllRelevancy,
      selectAllLanguageChecked: rime.selectAllLanguage,
      selectedlocations: rime.locations,
      refreshDurations: rime.refreshDuration,
      manualTags: rime.tags,
      operations: operatorValue,
      language: rime.language,
      catArray: cArray,
      relArray: rArray,
    });
  }

  componentDidUpdate(prevProps) {
    const { rime } = this.props;
    let operatorValue = operationalValue(rime.operations);
    if (rime !== prevProps.rime) {
      this.setState({
        riskCategories: rime.riskCategories,
        relevancy: rime.relevancy,
        selectAllRiskCatChecked: rime.selectAllRiskCat,
        selectAllRelevancyChecked: rime.selectAllRelevancy,
        selectAllLanguageChecked: rime.selectAllLanguage,
        selectedlocations: rime.locations,
        refreshDurations: rime.refreshDuration,
        manualTags: rime.tags,
        operations: operatorValue,
        language: rime.language,
      });
    }
  }

  toggleFilters = () => this.props.dispatch(actions.toggleRimeFilters(false));

  updateCategories = id => {
    const { riskCategories, catArray } = this.state;
    if (riskCategories.includes(id)) {
      let index = riskCategories.indexOf(id);
      riskCategories.splice(index, 1);
    } else {
      riskCategories.push(id);
    }

    if (riskCategories.length === Math.max(...catArray) + 1) {
      this.setState({ selectAllRiskCatChecked: true }, () => {
        store.dispatch(
          handleSelectAllRiskCat(this.state.selectAllRiskCatChecked),
        );
      });
    } else {
      this.setState({ selectAllRiskCatChecked: false }, () => {
        store.dispatch(
          handleSelectAllRiskCat(this.state.selectAllRiskCatChecked),
        );
      });
    }
    this.props.dispatch(updateSelectedCategories(riskCategories));
  };

  updateRelevancy = id => {
    const { relevancy, relArray } = this.state;
    if (relevancy.includes(id)) {
      let index = relevancy.indexOf(id);
      relevancy.splice(index, 1);
    } else {
      relevancy.push(id);
    }

    if (relevancy.length === Math.max(...relArray)) {
      this.setState({ selectAllRelevancyChecked: true }, () => {
        store.dispatch(
          handleSelectAllRelevancy(this.state.selectAllRelevancyChecked),
        );
      });
    } else {
      this.setState({ selectAllRelevancyChecked: false }, () => {
        store.dispatch(
          handleSelectAllRelevancy(this.state.selectAllRelevancyChecked),
        );
      });
    }
    this.props.dispatch(updateSelectedRelevancy(relevancy));
  };

  updateLanguage = id => {
    const { language } = this.state;
    if (language.includes(id)) {
      let index = language.indexOf(id);
      language.splice(index, 1);
    } else {
      language.push(id);
    }

    if (language.length === 11) {
      this.setState({ selectAllLanguageChecked: true }, () => {
        store.dispatch(
          handleSelectAllLanguage(this.state.selectAllLanguageChecked),
        );
      });
    } else {
      this.setState({ selectAllLanguageChecked: false }, () => {
        store.dispatch(
          handleSelectAllLanguage(this.state.selectAllLanguageChecked),
        );
      });
    }

    this.props.dispatch(updateSelectedLanguage(language));
  };

  handleRiskCatSelectAll = e => {
    const { rime } = this.props;

    for (let key in rime.riskCategories) {
      rime.riskCategories[key] = e.target.checked;
    }

    this.setState({ selectAllRiskCatChecked: e.target.checked }, () => {
      store.dispatch(
        handleSelectAllRiskCat(this.state.selectAllRiskCatChecked),
      );
      store.dispatch(
        updateSelectedCategories(
          this.state.selectAllRiskCatChecked
            ? [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
            : [],
        ),
      );
    });
  };

  handleRelevancySelectAll = e => {
    const { rime } = this.props;

    for (let key in rime.relevancy) {
      rime.relevancy[key] = e.target.checked;
    }

    this.setState({ selectAllRelevancyChecked: e.target.checked }, () => {
      store.dispatch(
        handleSelectAllRelevancy(this.state.selectAllRelevancyChecked),
      );
      store.dispatch(
        updateSelectedRelevancy(
          this.state.selectAllRelevancyChecked ? [1, 2, 3] : [],
        ),
      );
    });
  };

  handleLanguageSelectAll = e => {
    const { rime } = this.props;

    for (let key in rime.language) {
      rime.language[key] = e.target.checked;
    }

    this.setState({ selectAllLanguageChecked: e.target.checked }, () => {
      store.dispatch(
        handleSelectAllLanguage(this.state.selectAllLanguageChecked),
      );
      store.dispatch(
        updateSelectedLanguage(
          this.state.selectAllLanguageChecked
            ? [
                'en',
                'hi',
                'zh',
                'ar',
                'es',
                'ur',
                'mr',
                'fr',
                'bn',
                'pt',
                'id',
                'ml',
                'ta',
                'kn',
                'ms',
                'th',
                'vi',
              ]
            : [],
        ),
      );
    });
  };

  handleMultiSelect = value => {
    this.setState({ selectedAddress: value }, () => {
      store.dispatch(
        updateSelectedLocations(value.length > 0 && this.state.selectedAddress),
      );
    });
    store.dispatch(
      updateSelectedLocations(value.length === 0 && [this.state.allLocations]),
    );
  };

  handleRefreshDuration = e => {
    if (e < min) e = min;
    this.setState({ refreshDurations: e || 60 }, () => {
      store.dispatch(updateRefreshDuration(e || 60));
    });
  };

  handleOperationsChange = (event, newAlignment) => {
    this.setState({ operations: newAlignment || 'OR' }, () => {
      store.dispatch(updateOperations(newAlignment || 'OR'));
    });
  };

  handleUpdateTags = event => {
    this.setState({ manualTags: event }, () => {
      store.dispatch(updateTags(event || ''));
    });
  };

  handleApplyFilters = () => {
    store.dispatch(showApiHitTime(true));
    this.handleDrawerClose();
    this.props.dispatch(toggleIsFetch(true));
    this.props.handleRimeFeed();
    this.props.clearFeed();
    this.toggleFilters();
  };

  handleDrawer = () => {
    this.setState({ showDrawer: true });
  };

  handleDrawerClose = () => {
    this.setState({
      showDrawer: false,
      showRiskCategories: false,
      showRelevancy: false,
    });
  };

  render() {
    const {
      classes,
      themeChange: { setDarkTheme },
    } = this.props;
    const {
      riskCategories,
      relevancy,
      showRiskCategories,
      showRelevancy,
      refreshDurations,
      operations,
      manualTags,
      showDrawer,
      showLanguages,
      language,
    } = this.state;

    return (
      <>
        <div id="rimeFilterOptions">
          <Tooltip title="Filter">
            <button
              className="btnEllipsis"
              onClick={() => {
                this.handleDrawer();
              }}
            >
              <i className="fas fa-filter"></i>
            </button>
          </Tooltip>
        </div>
        <Drawer
          variant="top"
          classes={{
            root: classes.rootDrawer,
            paperAnchorTop: classes.paperAnchorTop,
            modal: classes.test,
          }}
          anchor="top"
          open={showDrawer}
          className={`${setDarkTheme ? 'dark' : 'light'}`}
        >
          <div className="rime__filterDrawer">
            <div className="srchBtn">
              <Button
                classes={{
                  root: classes.cancelBtnStyle,
                }}
                variant="contained"
                endIcon={<CancelIcon />}
                onClick={() => this.handleDrawerClose()}
              >
                Cancel
              </Button>
              <Button
                variant="contained"
                endIcon={<ArrowForwardIcon />}
                classes={{
                  root: classes.searchStyle,
                }}
                onClick={() => this.handleApplyFilters()}
              >
                Search Events
              </Button>
            </div>
            <div className="rimeFilter__row">
              <div className="filter__mLocations">
                <div className="rime__multipleLocationsParent">
                  <div className="rime__multipleLocations">
                    <MultiSelect
                      value={this.state.selectedlocations}
                      placeholder={'Multiple Locations'}
                      parentEventHandler={value =>
                        this.handleMultiSelect(value)
                      }
                      options={multipleAddress}
                      allowSelectAll={false}
                      className="multiple_EventLocations"
                    />
                  </div>
                </div>
              </div>
              <div className="filter__otherFilters">
                <div className="filter__subRow1">
                  <div className="rime__toggleOperations">
                    <ToggleButtonGroup
                      color="primary"
                      value={operations}
                      exclusive
                      onChange={this.handleOperationsChange}
                    >
                      <ToggleButton value="OR">OR</ToggleButton>
                      <ToggleButton value="AND">AND</ToggleButton>
                    </ToggleButtonGroup>
                  </div>
                  <div className="rimeFilter__tags">
                    <div className="rime__tags">
                      <TextField
                        id="standard-number"
                        label="Tags (Separate with Comma)"
                        variant="standard"
                        type="text"
                        InputLabelProps={{
                          shrink: true,
                        }}
                        value={manualTags}
                        onChange={e => this.handleUpdateTags(e.target.value)}
                        fullWidth
                      />
                    </div>
                    <div className="rimeFilter__Language">
                      <div
                        className={`${showLanguages &&
                          'selectAllEventCatsWhiteRisk'}`}
                      >
                        <div className="selectAllEventCats">
                          <label
                            className={
                              showLanguages
                                ? classes.detailsTitleWhite
                                : classes.detailsTitle
                            }
                            onClick={() =>
                              this.setState({
                                showLanguages: !showLanguages,
                              })
                            }
                          >
                            <span>
                              <i
                                className="fa fa-globe"
                                style={{
                                  color: 'var(--whiteDisabled)',
                                  paddingRight: '5px',
                                }}
                              ></i>
                            </span>
                            Language
                          </label>
                          <span
                            className="filtersWrapBox__arrow"
                            onClick={() =>
                              this.setState({
                                showLanguages: !showLanguages,
                              })
                            }
                          >
                            <i
                              className={
                                showLanguages
                                  ? 'fas fa-chevron-up'
                                  : 'fas fa-chevron-down'
                              }
                              style={{ color: 'var(--whiteDisabled)' }}
                            ></i>
                          </span>
                        </div>
                        {showLanguages && (
                          <>
                            <div className="rimeFilter__languageChild">
                              <span style={{ margin: '0 10px' }}>
                                <FormControlLabel
                                  classes={{
                                    label: classes.label,
                                  }}
                                  control={
                                    <Checkbox
                                      className={classes.size}
                                      checked={
                                        this.state.selectAllLanguageChecked
                                      }
                                      onClick={this.handleLanguageSelectAll}
                                      value={
                                        this.state.selectAllLanguageChecked
                                      }
                                      icon={
                                        <CheckBoxOutlineBlankIcon
                                          className={classes.sizeIcon}
                                        />
                                      }
                                      checkedIcon={
                                        <CheckBoxIcon
                                          className={classes.sizeIcon}
                                        />
                                      }
                                      style={{ color: 'black' }}
                                    />
                                  }
                                  label={'Select All'}
                                />
                              </span>
                              {languageConstants.map(item => (
                                <CustomCheckboxUi
                                  key={item.id}
                                  id={item.id}
                                  label={item.label}
                                  updateCategories={this.updateLanguage}
                                  isSelected={language.includes(item.id)}
                                  name={item.name}
                                  ref={this.categoriesRef}
                                />
                              ))}
                            </div>
                          </>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
                <div className="filter__subRow2">
                  <div className="rimeFilter__RiskCategory">
                    <div
                      className={`${showRiskCategories &&
                        'selectAllEventCatsWhiteRisk'}`}
                    >
                      <div className="selectAllEventCats">
                        {showRiskCategories ? (
                          <label
                            className={classes.detailsTitleWhite}
                            onClick={() =>
                              this.setState({
                                showRiskCategories: !showRiskCategories,
                              })
                            }
                          >
                            <span>
                              <i
                                className="icon-uniE90E"
                                aria-hidden="true"
                              ></i>
                            </span>
                            Risk Categories
                          </label>
                        ) : (
                          <label
                            className={classes.detailsTitle}
                            onClick={() =>
                              this.setState({
                                showRiskCategories: !showRiskCategories,
                              })
                            }
                          >
                            <span>
                              <i
                                className="icon-uniE90E"
                                aria-hidden="true"
                                style={{ color: 'var(--whiteDisabled)' }}
                              ></i>
                            </span>
                            Risk Categories
                          </label>
                        )}
                        <span
                          className="filtersWrapBox__arrow"
                          onClick={() =>
                            this.setState({
                              showRiskCategories: !showRiskCategories,
                            })
                          }
                        >
                          <i
                            className={
                              showRiskCategories
                                ? 'fas fa-chevron-up'
                                : 'fas fa-chevron-down'
                            }
                            style={{ color: 'var(--whiteDisabled)' }}
                          ></i>
                        </span>
                      </div>
                      {showRiskCategories && (
                        <div className="rimeFilter__RiskCategorychild">
                          <span style={{ margin: '0 10px' }}>
                            <FormControlLabel
                              classes={{
                                label: classes.label,
                              }}
                              control={
                                <Checkbox
                                  className={classes.size}
                                  checked={this.state.selectAllRiskCatChecked}
                                  onClick={this.handleRiskCatSelectAll}
                                  value={this.state.selectAllRiskCatChecked}
                                  icon={
                                    <CheckBoxOutlineBlankIcon
                                      className={classes.sizeIcon}
                                    />
                                  }
                                  checkedIcon={
                                    <CheckBoxIcon
                                      className={classes.sizeIcon}
                                    />
                                  }
                                  style={{ color: 'black' }}
                                />
                              }
                              label={'Select All'}
                            />
                          </span>
                          {categories.map(item => (
                            <CustomCheckboxUi
                              key={item.id}
                              id={item.id}
                              label={item.label}
                              updateCategories={this.updateCategories}
                              isSelected={riskCategories.includes(item.id)}
                              name={item.name}
                              ref={this.categoriesRef}
                            />
                          ))}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="rimeFilter__Relevancy">
                    <div className="rimeFilter__RelevancyChild">
                      <div
                        className={`${showRelevancy &&
                          'selectAllEventCatsWhiteRisk'}`}
                      >
                        <div className="selectAllEventCats">
                          <label
                            className={
                              showRelevancy
                                ? classes.detailsTitleWhite
                                : classes.detailsTitle
                            }
                            onClick={() =>
                              this.setState({ showRelevancy: !showRelevancy })
                            }
                          >
                            <span>
                              <i
                                className="icon-uniE95E"
                                aria-hidden="true"
                              ></i>
                            </span>
                            Relevancy
                          </label>
                          <span
                            className="filtersWrapBox__arrow"
                            onClick={() =>
                              this.setState({ showRelevancy: !showRelevancy })
                            }
                          >
                            <i
                              className={
                                showRelevancy
                                  ? 'fas fa-chevron-up'
                                  : 'fas fa-chevron-down'
                              }
                              style={{ color: 'var(--whiteDisabled)' }}
                            ></i>
                          </span>
                        </div>
                        {showRelevancy && (
                          <>
                            <span style={{ margin: '0 10px' }}>
                              <FormControlLabel
                                classes={{
                                  label: classes.label,
                                }}
                                control={
                                  <Checkbox
                                    className={classes.size}
                                    checked={
                                      this.state.selectAllRelevancyChecked
                                    }
                                    onClick={this.handleRelevancySelectAll}
                                    value={this.state.selectAllRelevancyChecked}
                                    icon={
                                      <CheckBoxOutlineBlankIcon
                                        className={classes.sizeIcon}
                                      />
                                    }
                                    checkedIcon={
                                      <CheckBoxIcon
                                        className={classes.sizeIcon}
                                      />
                                    }
                                  />
                                }
                                label={'Select All'}
                              />
                            </span>
                            {relevancyConstants.map(item => (
                              <CustomRelevancyCheckbox
                                key={item.id}
                                id={item.id}
                                label={item.label}
                                updateRelevancy={this.updateRelevancy}
                                isSelected={relevancy.includes(item.id)}
                                name={item.name}
                              />
                            ))}
                          </>
                        )}
                      </div>
                    </div>
                  </div>
                  <div className="Event_RefreshDuration">
                    <TextField
                      id="standard-number"
                      label="Refresh Duration (In Minutes)"
                      variant="standard"
                      type="number"
                      defaultValue="60"
                      InputLabelProps={{
                        shrink: true,
                      }}
                      onInput={e => {
                        e.target.value = Math.max(0, parseInt(e.target.value))
                          .toString()
                          .slice(0, 4);
                      }}
                      value={refreshDurations}
                      onChange={e => this.handleRefreshDuration(e.target.value)}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Drawer>
      </>
    );
  }
}

export default TopBarFilters;
