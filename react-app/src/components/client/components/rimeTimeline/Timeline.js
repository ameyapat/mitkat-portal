import React, { Component } from 'react';
import RimeCard from '../rimeCard';
import { Transition } from 'react-spring/renderprops';

class Timeline extends Component {
  render() {
    const { randomFeeds } = this.props;

    return (
      <div className="timelineWrap">
        <ul>
          <Transition
            items={randomFeeds}
            keys={item => item.id}
            from={{ transform: 'translate3d(0, -40px, 0)', opacity: 0 }}
            enter={{ transform: 'translate3d(0,0,0)', opacity: 1 }}
            update={{ transform: 'translate3d(0,0,0)', opacity: 1 }}
            reset={true}
            unique={true}
          >
            {item => props => {
              return (
                <>
                  {randomFeeds.includes(item) ? (
                    <li style={props}>
                      <RimeCard
                        title={item.title}
                        categoryId={item.riskCategoryId}
                        category={item.riskCategory}
                        link={item.link}
                        isTimeline={false}
                        date={item.publishDateString}
                        relevancy={item.relevency}
                      />
                    </li>
                  ) : null}
                </>
              );
            }}
          </Transition>
        </ul>
      </div>
    );
  }
}

export default Timeline;
