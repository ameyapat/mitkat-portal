import React, { Component } from 'react';
import './RimeTimeline.scss';
import Timeline from './Timeline';

class RimeTimeline extends Component {
  state = {};

  componentDidMount() {
    const timelineWrap = document
      .querySelector('.rimeTimeline')
      .getBoundingClientRect().top;

    this.setState({
      dynamicHeight: window.innerHeight - timelineWrap,
    });
  }

  render() {
    return (
      <div className="rimeTimeline">
        <Timeline randomFeeds={this.props.randomFeeds} />
      </div>
    );
  }
}

export default RimeTimeline;
