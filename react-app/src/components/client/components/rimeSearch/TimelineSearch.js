import React, { Component } from 'react';
import './RimeSearch.scss';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import DirectionsIcon from '@material-ui/icons/Directions';
import EventIcon from '@material-ui/icons/Event';
import injectSheet from 'react-jss';

const useStyles = {
    root: {
      padding: '2px 4px',
      display: 'flex',
      alignItems: 'center',
    //   width: 300,
    },
    input: {
    //   marginLeft: theme.spacing(1),
      flex: 1,
    },
    iconButton: {
      padding: 10,
    },
    divider: {
      height: 28,
      margin: 4,
    },
};

class TimelineSearch extends Component{

    render(){
        const { classes } = this.props;
        return(
            <div className="rimeSearch">
                <Paper component="form" className={classes.root}>
                    <IconButton className={classes.iconButton} aria-label="menu">
                        <EventIcon />
                    </IconButton>
                    <InputBase
                        className={classes.input}
                        placeholder="Search..."
                        inputProps={{ 'aria-label': 'search events' }}
                    />
                    <IconButton type="submit" className={classes.iconButton} aria-label="search">
                        <SearchIcon />
                    </IconButton>
                    <Divider className={classes.divider} orientation="vertical" />
                </Paper>
            </div>
        )
    }
}

export default injectSheet(useStyles, { withTheme: true })(TimelineSearch);