import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { liveMonitorSearchFeed } from '../../../../actions/rimeActions';
import { searchRimeEvents } from '../../../../helpers/utils';
import './RimeSearch.scss';
import searchIcon from '../../../../assets/svg/searchIcon.svg';
import EventAvailableIcon from '@material-ui/icons/EventAvailable';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import injectSheet from 'react-jss';

const useStyles = {
  root: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: '100%',
    backgroundColor: 'var(--cardBackground) !important',
    color: 'var(--highlightText) !important',
  },
  input: {
    flex: 1,
    border: 'none',
    height: '6vh',
    outline: 'none',
    backgroundColor: 'var(--cardBackground) !important',
    color: 'var(--highlightText) !important',
    '&:focus': {
      outline: 'none',
    },
  },
  iconButton: {
    padding: 10,
    backgroundColor: 'var(--cardBackground) !important',
    color: 'var(--highlightText) !important',
  },
  divider: {
    height: 28,
    margin: 4,
  },
};

const RimeSearch = ({ placeholder = 'type to search', onClose, classes }) => {
  const feed = useSelector(store => store.rime.liveMonitorSearchFeed);
  const updateFeed = useSelector(
    store => store.rime.updatedLiveMonitorSearchFeed,
  );
  const textInput = React.useRef();

  const history = useHistory();
  const dispatch = useDispatch();

  const handleSearch = e => {
    let value = e.target.value;
    if (value) {
      if (history.location.pathname === '/client/rime') {
        let filteredEventDetails = searchRimeEvents(value, updateFeed);
        dispatch(liveMonitorSearchFeed(filteredEventDetails));
      } else {
        let filteredEventDetails = searchRimeEvents(value, updateFeed);
      }
    } else {
      dispatch(liveMonitorSearchFeed(updateFeed));
    }
  };

  const handleOnClose = e => {
    textInput.current.value = '';
    dispatch(liveMonitorSearchFeed(updateFeed));
    onClose();
    e.preventDefault();
  };

  return (
    <div className="rimeSearch">
      <Paper component="form" className={classes.root}>
        <IconButton className={classes.iconButton} aria-label="menu">
          <EventAvailableIcon />
        </IconButton>
        <input
          className={classes.input}
          type="text"
          placeholder={placeholder}
          inputProps={{ 'aria-label': 'search events' }}
          onChange={handleSearch}
          ref={textInput}
        />
        <span className="inputBtnWrapper">
          <button className="cancelButton" onClick={handleOnClose}>
            &times;
          </button>
        </span>
        <Divider className={classes.divider} orientation="vertical" />
      </Paper>
    </div>
  );
};

export default injectSheet(useStyles, { withTheme: true })(RimeSearch);