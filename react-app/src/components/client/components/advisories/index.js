import React, { Component } from 'react';
//components
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import InfoCardList from '../../../ui/infoCardList';
import Empty from '../../../ui/empty';
//assets
import listIcon from './list.png';
//
import injectSheet from 'react-jss';
import styles from './style';
import moment from 'moment';
import { withCookies } from 'react-cookie';
import { fetchApi } from '../../../../helpers/http/fetch';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import { searchByTitle } from '../../../../helpers/utils';
import InfoList from '../../../ui/infoList';
import clsx from 'clsx';
import './style.scss';

@withCookies
@injectSheet(styles)
class Advisories extends Component {
  state = {
    tabValue: 0,
    list: [],
    filteredList: [],
    searchedValue: '',
    methodType: 'GET',
    showLink: '',
  };

  componentDidMount() {
    this.fetchList();
  }

  fetchList = () => {
    let { tabValue } = this.state;
    let apiUrl = '';
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');

    switch (tabValue) {
      case 0:
        apiUrl = API_ROUTES.getAdvisoryList;
        break;
      case 1:
        apiUrl = API_ROUTES.getReports;
        break;
      case 2:
        apiUrl = API_ROUTES.getSpecialReports;
        break;
      case 3:
        apiUrl = API_ROUTES.getRiskReviews;
        break;
      case 4:
        apiUrl = API_ROUTES.getBlogs;
        break;
      case 5:
        apiUrl = API_ROUTES.getPodcast;
        break;
      default:
        apiUrl = API_ROUTES.getAdvisoryList;
        break;
    }
    this.setState({ apiUrl });
    let reqObj = {
      url: apiUrl,
      method: this.state.methodType,
      isAuth: true,
      authToken,
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        const { tabValue } = this.state;
        if (data) {
          this.setState(
            {
              list: tabValue !== 5 ? data.output : data,
              filteredList: tabValue !== 5 ? data.output : data,
            },
            () => {
              const defaultLink =
                this.state.tabValue === 5
                  ? this.state.filteredList?.[0]?.linkURL
                  : this.state.filteredList?.[0]?.document || '';
              this.setVideo(defaultLink);
            },
          );
        }
      })
      .catch(e => {
        console.log(e);
      });
  };

  handleTabChange = (e, value) => {
    this.setState(
      {
        tabValue: value,
      },
      () => {
        this.fetchList();
      },
    );
  };

  handleSearch = value => {
    this.setState({ searchedValue: value }, () => this.searchTitle());
  };

  searchTitle = () => {
    let { list, filteredList, searchedValue } = this.state;
    if (searchedValue.length > 0) {
      filteredList = searchByTitle(searchedValue, list);
      this.setState({ filteredList });
    } else {
      this.setState({ filteredList: list });
    }
  };

  setVideo = link => {
    this.setState({ showLink: link });
  };

  render() {
    let { classes } = this.props;
    let { tabValue, filteredList, searchedValue, showLink } = this.state;

    return (
      <div className={classes.rootAdvisories}>
        <div className={classes.resourcesBox}>
          <h1 className={classes.rootHeader}>Resources </h1>
          <AppBar
            position="static"
            color="default"
            classes={{ root: classes.rootAppBar }}
          >
            <Tabs
              classes={{ indicator: classes.indicator, root: classes.rootTabs }}
              value={tabValue}
              onChange={this.handleTabChange}
              centered
              // orientation="vertical"
            >
              <Tab
                classes={{
                  root: classes.rootTab,
                  selected: classes.selectedTab,
                }}
                label="Advisories"
              />
              <Tab
                classes={{
                  root: classes.rootTab,
                  selected: classes.selectedTab,
                }}
                label="Periodic Updates"
              />
              <Tab
                classes={{
                  root: classes.rootTab,
                  selected: classes.selectedTab,
                }}
                label="Special Reports"
              />
              <Tab
                classes={{
                  root: classes.rootTab,
                  selected: classes.selectedTab,
                }}
                label="Risk Reviews"
              />

              <Tab
                classes={{
                  root: classes.rootTab,
                  selected: classes.selectedTab,
                }}
                label="Blogs"
              />
              <Tab
                classes={{
                  root: classes.rootTab,
                  selected: classes.selectedTab,
                }}
                label="Podcast"
              />
            </Tabs>
          </AppBar>
          <div className={classes.viewBox}>
            <InfoCardList
              isClient={true}
              classNames={{ root: classes.rootInfoCardList }}
            >
              {filteredList?.length > 0 ? (
                <>
                  <div className="advisory--row">
                    <div className="embed-responsive embed-responsive-4by3 px-5">
                      <iframe
                        width="100%"
                        height="100%"
                        src={showLink}
                        frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen
                      ></iframe>
                    </div>
                    <div className={classes.rootInfoCard}>
                      <div className={classes.rootSearchBar}>
                        <span>
                          <i className="fa fa-search"></i>
                        </span>
                        <input
                          value={searchedValue}
                          placeholder="Search by title"
                          onChange={e => this.handleSearch(e.target.value)}
                        />
                      </div>
                      <div className={clsx(classes.rootInfoWrap)}>
                        {filteredList.map(item => (
                          <div
                            className={clsx(
                              classes.rootInfoTxt,

                              tabValue === 5
                                ? item.linkURL == showLink
                                  ? 'active'
                                  : 'inActive'
                                : item.document == showLink
                                ? 'active'
                                : 'inActive',
                            )}
                            onClick={() => {
                              this.setVideo(
                                tabValue === 5 ? item.linkURL : item.document,
                              );
                            }}
                          >
                            <h1>{item.title}</h1>
                            <div className="infoCard-meta-wrap">
                              <div>
                                <p>{item.date}</p>
                              </div>
                              {/* <div className="infoCard-meta-wrap-buttons">
                                <div>
                                  <a
                                    target="_blank"
                                    title="View"
                                    onClick={() => {
                                      this.setVideo(
                                        tabValue === 5
                                          ? item.linkURL
                                          : item.document,
                                      );
                                    }}
                                  >
                                    <i className="far fa-eye"></i>
                                  </a>
                                </div>
                                <div>
                                  <a
                                    target="_blank"
                                    title="Expand"
                                    href={
                                      tabValue === 5
                                        ? item.linkURL
                                        : item.document
                                    }
                                  >
                                    <i className="fas fa-expand"></i>
                                  </a>
                                </div>
                              </div> */}
                            </div>
                          </div>
                        ))}
                      </div>
                    </div>
                  </div>
                </>
              ) : (
                <div className={classes.emptyWrap}>
                  <Empty title="No Items Available" imgSrc={listIcon} />
                </div>
              )}
            </InfoCardList>
          </div>
        </div>
      </div>
    );
  }
}

export default Advisories;
