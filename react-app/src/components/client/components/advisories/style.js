import { setAutoFreeze } from 'immer';

export default {
  rootAdvisories: {
    background: 'transparent',
  },
  indicator: {
    display: 'none',
  },
  resourcesBox: {
    display: 'flex',
    flexDirection: 'column',
  },
  rootHeader: {
    padding: '13px 30px',
    color: 'var(--lightText)',
    background: 'var(--darkSolid)',
  },
  rootTabs: {
    '& .MuiTabs-scroller': {
      display: 'flex',
      justifyContent: 'center',
    },
  },
  rootTab: {
    color: 'var(--whiteMediumEmp)',
    opacity: 1,
    textAlign: 'left',
    fontSize: '1em !important',
    minHeight: '35px !important',
  },
  rootAppBar: {
    background: 'var(--darkGrey)',
    boxShadow: 'none',
    flex: '25%',
  },
  selectedTab: {
    borderBottom: '3px solid var(--darkActive)',
  },
  rootInfoCardList: {
    background: 'transparent',
    padding: '0 0',
    maxHeight: '82vh',
  },
  viewBox: {
    flex: '65%',
  },
  emptyWrap: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  rootSearchBar: {
    padding: 10,
    borderRadius: 25,
    display: 'flex',
    alignItems: 'center',
    width: '96%',
    margin: '10px auto',
    background: 'var(--darkGrey)',

    '& input': {
      width: '100%',
      background: 'transparent',
      border: 0,
      padding: 5,
      color: 'var(--whiteMediumEmp)',
      outline: 'none',
    },
    '& span i': {
      color: 'var(--whiteMediumEmp)',
    },
  },
  rootBtn: {
    // "& button":{
    //     background: 'transparent',
    //     border: '1px solid #fff',
    //     color: 'var(--whiteMediumEmp)',
    //     "&:hover":{
    //         background: 'transparent',
    //         color: '#fff'
    //     }
    // }
  },
  rootClientInfo: {
    background: `linear-gradient(rgba(0, 0, 0, 0.4) 5%, rgba(0, 0, 0, 0.8) 100%, transparent)`,
  },
  rootInfoCard: {
    flex: '35%',
    background: 'var(--backgroundSolid)',
  },
  rootInfoWrap: {
    width: '100%',
    color: 'var(--whiteMediumEmp)',
    minHeight: 100,
    padding: 5,
    overflowY: 'scroll',
    maxHeight: '72vh',
  },
  rootInfoTxt: {
    padding: '15px',
    background: 'var(--darkGrey)',

    marginBottom: '7px',
    cursor: 'pointer',

    '&.active': {
      background: 'var(--darkGrey)!important',
      borderBottom: '5px solid var(--highlightBackground)',

      '& h1': {
        color: 'var(--whiteHighEmp) !important',
      },
      '& p': {
        color: 'var(--whiteHighEmp) !important',
      },
    },
    '&:hover': {
      background: 'rgba(255, 255, 255, 0.05)',

      '& h1': {
        color: 'var(--whiteMediumEmp) !important',
      },
      '& p': {
        color: 'var(--whiteMediumEmp) !important',
      },
    },

    '& h1': {
      textTransform: 'capitalize',
      paddingBottom: 10,
      fontSize: 15,
      fontWeight: 100,
      color: 'var(--whiteHighEmp)',
    },
    '& p': {
      fontSize: 12,
      paddingBottom: 15,
      color: 'var(--whiteHighEmp)',

      '& a': {
        color: 'var(--darkActive)',
        float: 'right',
        border: '1px solid var(--darkActive)',
        borderRadius: 3,
        padding: 5,
        cursor: 'pointer',
        '&:hover': {
          transform: 'translate3d(0px, -1px , 0px)',
        },
      },
    },
  },
  rootActions: {
    '& button, & a': {
      margin: '0 8px 0 0',
      background: 'transparent',
      border: '1px solid #fff',
      color: 'var(--whiteMediumEmp)',
      borderRadius: 3,
      padding: 5,
      cursor: 'pointer',
      '&:hover': {
        // background: '#f1f1f1',
        transform: 'translate3d(0px, -2px , 0px)',
        // boxShadow: '0px 0px 7px 0 #d3d3d3'
      },
    },

    '& a': {
      fontSize: 13,
      color: '#000',
      '&:hover': {
        transform: 'translate3d(0px, -2px , 0px)',
        // boxShadow: '0px 0px 7px 0 #d3d3d3'
      },
    },
  },
};
