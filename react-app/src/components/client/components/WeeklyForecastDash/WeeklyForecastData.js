import React, { Component } from 'react';
import WeeklyForecastList from './components/WeeklyForecastList';
import WeeklyForecastDataBox from './components/WeeklyForecastDataBox';

class WeeklyForecastData extends Component {
  flattenArr = weeklyDataMapped => {
    let flat = [];
    for (let key in weeklyDataMapped) {
      flat.push(...weeklyDataMapped[key]);
    }
    return flat;
  };

  render() {
    const {
      eventId,
      weeklyDetails,
      weeklyDataMapped,
      weeklyEventDetails,
      getWeeklyEventDetailsData,
      centerValues,
    } = this.props;

    return (
      <div id="weeklyForecastDataBox">
        <WeeklyForecastList
          eventId={eventId}
          weeklyDetails={weeklyDetails}
          weeklyDataMapped={weeklyDataMapped}
          weeklyDataMappedFlatten={this.flattenArr(weeklyDataMapped)}
          getWeeklyEventDetailsData={getWeeklyEventDetailsData}
        />
        <WeeklyForecastDataBox
          weeklyEventDetails={weeklyEventDetails}
          centerValues={centerValues}
        />
      </div>
    );
  }
}

export default WeeklyForecastData;
