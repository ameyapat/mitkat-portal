import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import injectSheet from 'react-jss';
import styles from './style';
import { getWeeklyDetails, getWeeklyEventDetails } from '../../helpers/utils';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import WeeklyForecastData from './WeeklyForecastData';
import Empty from '../../../ui/empty';
import regionList from './regionList.json';
import { connect } from 'react-redux';

@withRouter
@injectSheet(styles)
@connect(state => {
  return {
    partnerDetails: state.partnerDetails.partnerDetails,
  };
})
class WeeklyForecastDash extends Component {
  state = {
    weeklyDetails: [],
    weeklyEventDetails: [],
    selectedCountry: 2,
    weeklyDataMapped: [],
    eventId: null,
    latState: 16.950209,
    lngState: 93.207995,
  };

  componentDidMount() {
    let { selectedCountry, eventId } = this.state;
    this.getWeeklyData(selectedCountry + 1);
  }

  regionListXML = () => {
    let regionListArr = [];
    const { classes } = this.props;
    regionListArr = regionList.map(item => (
      <Tab
        key={item.id}
        classes={{
          root: classes.rootTab,
          selected: classes.selectedTab,
        }}
        label={item.region}
      />
    ));

    return regionListArr;
  };

  handleChange = (e, value) => {
    let regionId = value + 1;
    this.setState({ selectedCountry: value });
    this.getWeeklyData(regionId);
  };

  getWeeklyData = regionId => {
    const weeklyDetailsArr = [];
    let firstIds = [];
    getWeeklyDetails(regionId).then(weeklyDetails => {
      if (weeklyDetails.length > 0) {
        weeklyDetails.map(item => {
          weeklyDetailsArr[item.calenderdate] = item.eventlist;
          firstIds.push(item.eventlist[0].id);
        });

        this.setState(
          {
            weeklyDetails,
            weeklyDataMapped: weeklyDetailsArr,
            eventId: firstIds[0],
          },
          () => {
            this.getWeeklyEventDetailsData(this.state.eventId);
          },
        );
      } else {
        this.setState({ weeklyDetails: [], weeklyDataMapped: [] });
      }
    });
  };

  setCenter = (lat, lng) => this.setState({ latState: lat, lngState: lng });

  getWeeklyEventDetailsData = eventId => {
    getWeeklyEventDetails(eventId).then(weeklyEventDetails => {
      this.setState({ weeklyEventDetails }, () =>
        this.getLatLng(eventId, this.state.weeklyEventDetails),
      );
    });
  };

  getLatLng = (eventId, weeklyEventDetails) => {
    if (eventId === weeklyEventDetails.id) {
      const lat = weeklyEventDetails.reportlocations[0].latitude;
      const lng = weeklyEventDetails.reportlocations[0].longitude;
      this.setCenter(lat, lng);
    }
  };

  render() {
    const { classes } = this.props;
    let {
      eventId,
      weeklyDetails,
      weeklyDataMapped,
      weeklyEventDetails,
      latState,
      lngState,
      selectedCountry,
    } = this.state;
    let { whiteLogo, partnerName } = this.props.partnerDetails;
    return (
      <div id="weeklyDashboard">
        <h1 className={classes.rootHeader}>Weekly Forecast </h1>
        <AppBar
          position="static"
          color="default"
          classes={{ root: classes.rootAppBar }}
        >
          <Tabs
            classes={{ indicator: classes.indicator, root: classes.rootTabs }}
            value={selectedCountry}
            onChange={this.handleChange}
            centered
          >
            {regionList.length > 0 && this.regionListXML()}
          </Tabs>
        </AppBar>
        {weeklyDetails && weeklyDetails.length > 0 ? (
          <WeeklyForecastData
            eventId={eventId}
            weeklyDetails={weeklyDetails}
            weeklyDataMapped={weeklyDataMapped}
            weeklyEventDetails={weeklyEventDetails}
            getWeeklyEventDetailsData={this.getWeeklyEventDetailsData}
            centerValues={{ lat: latState, lng: lngState }}
          />
        ) : (
          <div className="emptyBox">
            <div className="">
              <Empty title="No Data Found" />
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default WeeklyForecastDash;
