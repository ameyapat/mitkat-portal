import React from 'react';
import '../style.scss';
import { RISK_LEVEL_COLORS_NEON } from '../../../../../helpers/constants';
import MapView from './MapView';

const WeeklyForecastDataBox = ({ weeklyEventDetails, centerValues }) => (
  <div id="weeklyListData">
    <div className="weeklyListData">
      <div className="weeklyListData__Header">
        <h1>{weeklyEventDetails.title}</h1>
        <p className="weeklyListData__Date">
          {weeklyEventDetails.eventDate}{' '}
          {weeklyEventDetails.eventDate === weeklyEventDetails.validityDate
            ? ''
            : '-' + ' ' + weeklyEventDetails.validityDate}
          <span
            style={{
              color: `${
                RISK_LEVEL_COLORS_NEON[weeklyEventDetails.risklevelid]
              }`,
              border: 'none',
              fontWeight: '400'
            }}
          >
            {weeklyEventDetails.riskCategory}

            {weeklyEventDetails?.risksubcategory && (
              <>
                <span className="separator"> - </span>{' '}
                {weeklyEventDetails?.risksubcategory}
              </>
            )}
          </span>
        </p>

        <p>
          <strong>Area Impacted : </strong>
          {weeklyEventDetails.areasimpacted}
        </p>
      </div>
      <div className="weeklyListData__forecast">
        <p>{weeklyEventDetails.forecast}</p>
      </div>
      <div className="weeklyListData__map">
        <MapView
          weeklyEventDetails={weeklyEventDetails}
          centerValues={centerValues}
        />
      </div>
    </div>
  </div>
);

export default WeeklyForecastDataBox;
