import React, { Component } from 'react';
import { Marker } from 'react-google-maps';

class CustomMarker extends Component {

  markerRef = React.createRef();
  render() {
    let { lat, lng, customIcon } = this.props;
    
    return (
            <Marker
              position={{ lat, lng }}
              icon={customIcon}
              ref={this.markerRef}
            />
    );
  }
}

export default React.forwardRef((props, ref) => (
  <CustomMarker innerRef={ref} {...props} />
));
