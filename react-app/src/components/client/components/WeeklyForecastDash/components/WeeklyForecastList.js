import React, { useEffect, useState } from 'react';
import '../style.scss';
import {
  RISK_LEVEL_COLORS_NEON,
  RISK_CATEGORY_NAME,
} from '../../../../../helpers/constants';
import SearchBox from '../../../../ui/searchBox';

const WeeklyForecastList = ({
  eventId,
  weeklyDataMapped,
  weeklyDataMappedFlatten,
  getWeeklyEventDetailsData,
}) => {
  const [select, setSelect] = useState(eventId);
  const [searchedValue, setSearchedValue] = useState('');

  function getEventId(eventId) {
    getWeeklyEventDetailsData(eventId);
    setSelect(eventId);
  }

  const renderEvents = () => {
    return weeklyDataMappedFlatten
      .filter(item =>
        searchedValue
          ? item.title.match(new RegExp(searchedValue, 'gi'))
          : item,
      )
      .map(event => (
        <li>
          <div
            className={
              event.id === select ? 'eventDetails active' : 'eventDetails'
            }
            onClick={() => getEventId(event.id)}
          >
            <h4 className="eventTitle">{event.title}</h4>
            <p>
              {/* <span className="date"> {key} </span> */}
              <span
                style={{
                  color: `${RISK_LEVEL_COLORS_NEON[event.risklevel]}`,
                  border: 'none',
                  fontWeight: '400'
                }}
              >
                {RISK_CATEGORY_NAME[event.riskcategory]}
              </span>
            </p>
            <img src={event.riskCategoryLogo} />
          </div>
        </li>
      ));
  };

  return (
    <div id="weeklyList">
      <div className="weeklyList">
        <SearchBox
          classes="fa fa-search"
          placeholder="Search by keyword"
          parentHandler={e => setSearchedValue(e.target.value)}
          value={searchedValue}
        />
        <ul>{renderEvents()}</ul>
      </div>
    </div>
  );
};

export default WeeklyForecastList;
