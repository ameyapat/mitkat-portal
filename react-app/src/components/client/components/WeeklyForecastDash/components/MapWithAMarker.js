import React, { Component } from 'react';
import { withScriptjs, withGoogleMap, GoogleMap } from 'react-google-maps';
import CustomMarker from './CustomMarker';
import customIconCountry from '../../../../../assets/icons/reportLocation.png';
import customMapStyles from '../../../../../helpers/universalMapStyles.json';
import customMapStylesLight from '../../../../../helpers/universalMapStylesLight.json';
import { connect } from 'react-redux';

@withScriptjs
@withGoogleMap
@connect(state => {
  return {
    themeChange: state.themeChange,
  };
})
class MapWithAMarker extends Component {
  state = {
    showMarkers: false,
    showLocation: true,
    latState: 16.950209,
    lngState: 93.207995,
    defaultOptions: {
      mapTypeControl: false,
      streetViewControl: false,
      zoomControl: false,
      fullscreenControl: false,
      draggable: true,
    },
  };

  setCenter = (lat, lng) => this.setState({ latState: lat, lngState: lng });

  renderCustomMarkers = () => {
    const { weeklyEventDetails } = this.props;

    return weeklyEventDetails.reportlocations.map((data, index) => {
      let lat = parseFloat(data.latitude);
      let lng = parseFloat(data.longitude);

      return (
        <CustomMarker
          key={index}
          lat={lat}
          lng={lng}
          customIcon={customIconCountry}
          setCenter={this.setCenter}
        />
      );
    });
  };

  render() {
    const { lat, lng } = this.props.centerValues;
    const { defaultOptions, showMarkers, latState, lngState } = this.state;
    const {
      themeChange: { setDarkTheme },
    } = this.props;
    return (
      <GoogleMap
        defaultOptions={defaultOptions}
        options={{
          ...defaultOptions,
          styles: setDarkTheme ? customMapStyles : customMapStylesLight,
        }}
        defaultZoom={3.5}
        defaultCenter={{ lat: latState, lng: lngState }}
        center={{ lat, lng }}
        onTilesLoaded={() => this.setState({ showMarkers: true })}
      >
        {showMarkers && this.renderCustomMarkers()}
      </GoogleMap>
    );
  }
}

export default MapWithAMarker;
