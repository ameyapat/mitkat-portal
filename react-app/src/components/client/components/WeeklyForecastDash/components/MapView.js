import React, { Component } from 'react';
import MapWithAMarker from './MapWithAMarker';
import '../style.scss';

class MapView extends Component {
  render() {
    const { weeklyEventDetails, centerValues } = this.props;
    return (
      <MapWithAMarker
        googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyCYOTLyn6xbysArlvlW4zpxnH9p6H3An08&v=3.exp&libraries=geometry,drawing,places"
        loadingElement={<div style={{ height: `100%` }} />}
        containerElement={
          <div style={{ height: `53vh`, position: 'relative' }} />
        }
        mapElement={<div style={{ height: `100%` }} />}
        weeklyEventDetails={weeklyEventDetails}
        centerValues={centerValues}
      />
    );
  }
}

export default MapView;
