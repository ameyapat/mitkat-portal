export function searchByKeyword(value, list) {
    return list.filter(val => {
      let regex = new RegExp(value, 'gi');
      return (
        val.title.match(regex) ||
        val.riskcategory.match(regex)
      );
    });
  }