export default {
  rootHeader: {
    padding: '13px 30px',
    color: 'var(--lightText)',
    background: 'var(--darkSolid)',
  },
  rootTabs: {
    '& .MuiTabs-scroller': {
      display: 'flex',
      justifyContent: 'center',
    },
  },
  indicator: {
    display: 'none',
  },
  rootTab: {
    color: 'var(--whiteMediumEmp)',
    opacity: 1,
    textAlign: 'left',
    fontSize: '1em !important',
    minHeight: '35px !important',
  },
  rootAppBar: {
    background: 'var(--darkGrey)',
    boxShadow: 'none',
    flex: '25%',
  },
  selectedTab: {
    borderBottom: '3px solid var(--darkActive)',
  },
};
