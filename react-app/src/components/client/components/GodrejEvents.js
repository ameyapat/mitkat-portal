import React, { Component } from "react";
import GodrejEventItem from "./GodrejEventItem";
import { withStyles } from "@material-ui/core";

const styles = () => ({
  eventListWrapper: {
    display: "flex",
    padding: "0",
    flexWrap: "wrap",
    justifyContent: "space-between",
    overflow: "hidden",
    flexGrow: 1
  }
});

@withStyles(styles)
class GodrejEvents extends Component {
  state = {
    dynamicHeight: 0
  };

  componentDidMount() {
    const dynamicHeight = document
      .querySelector("#godrej-event-list")
      .getBoundingClientRect().top;
    this.setState({ dynamicHeight });
  }

  renderEventItems = () => {
    let { eventList } = this.props;
    return eventList.map((item, idx) => (
      <GodrejEventItem
        key={item.pestelcategoryid}
        title={item.pestelcategorystring}
        eventItems={item.categorywisedistribution}
      />
    ));
  };

  render() {
    const { classes } = this.props;
    return (
      <div
        id="godrej-event-list"
        style={{ height: `calc(100vh - ${this.state.dynamicHeight}px)` }}
        className={classes.eventListWrapper}
      >
        {this.renderEventItems()}
      </div>
    );
  }
}

export default GodrejEvents;
