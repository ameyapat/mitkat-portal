export const TABS = [
  {
    id: 0,
    icon: 'fas fa-chart-area',
  },
  {
    id: 1,
    icon: 'fas fa-globe-asia',
  },
  {
    id: 2,
    icon: 'fas fa-list',
  },
  {
    id: 3,
    icon: 'fas fa-calendar-day',
  },
  {
    id: 4,
    icon: 'fas fa-exclamation-circle',
  },
  {
    id: 5,
    icon: 'fas fa-chart-pie',
  },
];
