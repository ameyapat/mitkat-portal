import React, { Component } from 'react';
import { connect } from 'react-redux';
import './style.scss';
@connect(state => {
  return {
    heatMapDetails: state.heatMapDetails,
  };
})
class MajorRisks extends Component {
  renderMajorRiskList = () => {
    let {
      breadcrumbArray = [],
      regionDetails = {},
    } = this.props.heatMapDetails;

    let updatedRegionData =
      regionDetails[breadcrumbArray[breadcrumbArray.length - 1]][0];

    let majorRiskListArr = [];

    if (
      'majorRisks' in updatedRegionData === true &&
      updatedRegionData.majorRisks != null
    ) {
      majorRiskListArr = updatedRegionData.majorRisks.map(item => (
        <li>
          <h1> {item.topicName} </h1>
        </li>
      ));
    } else if (updatedRegionData.majorRisks == null) {
      majorRiskListArr = 'No Major Risk Available';
    }

    return majorRiskListArr;
  };

  render() {
    return (
      <>
        <div id="majorRisk">
          <div className="majorRisk--wrap">
            <h1>Major Risks </h1>
            <ul>{this.renderMajorRiskList()}</ul>
          </div>
        </div>
      </>
    );
  }
}

export default MajorRisks;
