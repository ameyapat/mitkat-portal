import React, { Component } from 'react';
import { connect } from 'react-redux';
import './style.scss';
@connect(state => {
  return {
    heatMapDetails: state.heatMapDetails,
  };
})
class EventList extends Component {
  renderEventList = () => {
    let {
      breadcrumbArray = [],
      regionDetails = {},
    } = this.props.heatMapDetails;

    let updatedRegionData =
      regionDetails[breadcrumbArray[breadcrumbArray.length - 1]][0];

    let eventListArr = [];

    if (
      'eventlist' in updatedRegionData === true &&
      updatedRegionData.eventlist != null
    ) {
      eventListArr = updatedRegionData.eventlist.map(item => (
        <li onClick={() => this.props.handleOpenEvent(item.id)}>
          <h1>{item.eventtitle}</h1>
          <p> {item.dateString}</p>
        </li>
      ));
    } else if (updatedRegionData.eventlist == null) {
      eventListArr = 'No Events Available';
    }

    return eventListArr;
  };

  render() {
    return (
      <>
        <div id="eventListregion">
          <div className="eventList--wrap">
            <h1>Event List </h1>
            <ul>{this.renderEventList()}</ul>
          </div>
        </div>
      </>
    );
  }
}

export default EventList;
