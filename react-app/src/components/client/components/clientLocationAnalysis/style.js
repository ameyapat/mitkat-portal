export default {
  applyBtn: {
    marginLeft: '20px !important',
    backgroundColor: '#2980b9 !important',
  },
  rootAppBar: {
    backgroundColor: 'var(--darkGrey) !important',
  },
  rootToolbar: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  logoWrapper: {
    display: 'flex',
    alignItems: 'center',
  },
  logoHeader: {
    fontSize: '17px !important',
    paddingLeft: '15px',
    color: '#d3d3d3',
    lineHeight: '65px',
    marginLeft: '15px',
    borderLeft: '1px solid #404b5d',
  },
  mitkatLogo: {
    width: '100px',
    maxWidth: '100%',
  },
  btnLogout: {
    backgroundColor: '#3b4554 !important',
    padding: '15px 10px !important',
    color: '#eee !important',
    '&:hover': {
      backgroundColor: 'rgba(9, 15, 25 , 0.7) !important',
    },
  },
  btnFilter: {
    backgroundColor: '#3b4554 !important',
    padding: '14px 10px !important',
    color: '#eee !important',
    marginRight: '15px !important',
    '&:hover': {
      backgroundColor: 'rgba(9, 15, 25 , 0.7) !important',
    },
  },
  formControl: {
    width: '100%',
    backgroundColor: 'var(--backgroundSolid) !important',
    marginBottom: '25px !important',
  },
  rootLabel: {
    color: 'var(--whiteHighEmp) !important',
  },
  rootIcon: {
    color: 'var(--whiteHighEmp) !important',
  },
  rootInput: {
    color: 'var(--whiteHighEmp) !important',
    borderBottom: '2px solid #eee !important',

    '&:hover': {
      borderBottom: '2px solid #eee !important',
      color: 'var(--whiteHighEmp) !important ', // or black
    },
  },
  underline: {
    '&:after': {
      borderBottom: '2px solid #34495e !important',
    },
  },
  notchedOutline: {
    borderColor: 'var(--whiteHighEmp) !important ',
  },
  rootSelect: {
    color: 'var(--whiteHighEmp) !important ',
  },
  rootMenuItem: {
    backgroundColor: 'var(--darkGrey) !important',
    color: 'var(--whiteHighEmp) !important ',
  },
  rootList: {
    backgroundColor: 'var(--darkGrey) !important',
    color: 'var(--whiteHighEmp) !important ',
  },
  rootAdvisories: {
    background: 'transparent',
  },
  indicatorTabs: {
    display: 'none',
  },
  regionBox: {
    display: 'flex',
    flexDirection: 'column',
  },
  rootTabs: {},
  rootTab: {
    color: 'var(--whiteHighEmp) !important ',
    opacity: 1,
    textAlign: 'left',
    fontSize: '1.15em !important',
    minHeight: '50px !important',
    maxWidth: '264px!important',
    minWidth: '72px!important',

    '& span': {
      fontSize: '0.9em !important',
      alignItems: 'center',
    },
  },
  rootAppBarTab: {
    backgroundColor: 'transparent !important',
    boxShadow: 'none !important',
    flex: '100%',
    padding: 15,
  },
  selectedTab: {
    backgroundColor: '#07101c !important',
    borderRadius: '5px !important',
    color: '#cfcfcf !important',
    '& span': {
      alignItems: 'center !important',
    },
  },

  viewBox: {
    flex: '100%',
    maxHeigth: '50vh',
    overflowY: 'scroll',
  },
  emptyWrap: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  rootSearchBar: {
    padding: 10,
    borderRadius: 25,
    display: 'flex',
    alignItems: 'center',
    width: '50%',
    margin: '0 auto 5px',

    '& input': {
      width: '100%',
      background: 'transparent',
      border: 0,
      padding: 5,
      color: 'var(--whiteMediumEmp)',
      outline: 'none',
    },
    '& span i': {
      color: 'var(--whiteMediumEmp)',
    },
  },
};
