import React, { Component } from 'react';
import './style.scss';

class ComingSoon extends Component {
  render() {
    return (
      <div id="comingsoon">
        <div className="comingsoon">
          <div className="comingsoon-animation">
            <i className=" fas fa-cog comingsoon-big"></i>
            <i className=" fas fa-cog comingsoon-small"></i>
          </div>
          <h1 className="comingsoon-text"> Coming Sooon!</h1>
        </div>
      </div>
    );
  }
}

export default ComingSoon;
