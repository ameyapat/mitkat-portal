import React, { Component } from 'react';
import MapWithAMarker from './MapWithAMarker';
import { withCookies } from 'react-cookie';

@withCookies
class MapApi extends Component {
  render() {
    return (
      <MapWithAMarker
        googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyCYOTLyn6xbysArlvlW4zpxnH9p6H3An08&v=3.exp&libraries=geometry,drawing,places"
        loadingElement={<div style={{ height: `94vh` }} />}
        containerElement={
          <div style={{ height: `50vh`, position: 'relative' }} />
        }
        mapElement={<div style={{ height: `50vh` }} />}
        feed={this.props.feed}
        clientLocations={this.props.clientLocations}
      />
    );
  }
}

export default MapApi;
