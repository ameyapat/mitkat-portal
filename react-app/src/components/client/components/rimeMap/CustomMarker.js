import React, { Component } from 'react';
import { Marker, InfoWindow } from 'react-google-maps';
import injectSheet from 'react-jss';
import style from './style';
import {
  RISK_LEVEL_COLORS,
  RISK_CATEGORY_ICONS,
} from '../../../../helpers/constants';
import { riskCategoryUrls } from '../../../../context/mapping';
import { withCookies } from 'react-cookie';
import { connect } from 'react-redux';
import './style.scss';
import { categories } from '../rimeDashboard/helpers/constants';

const pinMappingHex = {
  0: '#EEA672',
  1: '#23CD98',
  2: '#4BB2E7',
  3: '#B07AFB',
  4: '#7FFBFF',
  5: '#FD87CB',
  6: '#FFDC34',
  7: '#EE72A9',
};

const RISK_CATEGORY_URLS = riskCategoryUrls;

@withCookies
@injectSheet(style)
@connect(state => {
  return {
    rime: state.rime,
  };
})
class CustomMarker extends Component {
  state = {
    showInfoBox: false,
    showModal: false,
    eventDetail: '',
    showInfoBoxOnHover: false,
    mappedCategory: '',
  };

  markerRef = React.createRef();

  componentDidMount() {
    this.mapRiskCategories();
    const { lat, lng } = this.props;

    this.props.rime.lockScreen && this.props.setCenter(lat, lng);

    this.setState({ showInfoBox: false }, () => {
      setTimeout(() => {
        this.setState({ showInfoBox: false });
      }, 5000);
    });
  }

  mapRiskCategories = () => {
    categories.map(subitem => {
      if (this.props.riskCategoryId === subitem.id) {
        this.setState({ mappedCategory: subitem.label });
      }
    });
  };

  componentDidUpdate(prevProps) {
    if (
      this.props.rime.liveListItemDetails !== prevProps.rime.liveListItemDetails
    ) {
      const { liveListItemDetails } = this.props.rime;
      let lat =
        (liveListItemDetails &&
          liveListItemDetails.coordinates &&
          liveListItemDetails.coordinates.lat) ||
        '';
      let lng =
        (liveListItemDetails &&
          liveListItemDetails.coordinates &&
          liveListItemDetails.coordinates.lng) ||
        '';

      this.props.rime.lockScreen &&
        this.props.setCenter(parseFloat(lat), parseFloat(lng));

      if (parseFloat(lat) === this.props.lat) {
        this.setState({ showInfoBox: true });
      } else {
        this.setState({ showInfoBox: false });
      }
    }
  }

  handleShowInfoBox = show => {
    this.setState({ showInfoBox: show });
  };

  openLink = link => {
    const newWindow = window.open(link, '_blank');
    newWindow.focus();
  };

  renderIcon = () => {
    const { relevency, riskCategoryId } = this.props;
    if (relevency && riskCategoryId) {
      let url =
        RISK_CATEGORY_URLS[riskCategoryId - 1] &&
        RISK_CATEGORY_URLS[riskCategoryId - 1].riskLevels &&
        RISK_CATEGORY_URLS[riskCategoryId - 1].riskLevels[relevency] &&
        RISK_CATEGORY_URLS[riskCategoryId - 1].riskLevels[relevency].url;
      return url;
    }
  };

  render() {
    let {
      title,
      lat,
      lng,
      customIcon,
      classes,
      riskcategory,
      risklevel,
      showIcon = false,
      isClickable = true,
      showPill = false,
      riskId,
      locationName,
      distance,
      riskCategoryId,
      relevency,
      defaultTitle,
      showLocations = true,
    } = this.props;
    let { showInfoBox } = this.state;
    return (
      <Marker
        onMouseOver={() => this.handleShowInfoBox(true)}
        onMouseOut={() => this.handleShowInfoBox(false)}
        position={{ lat, lng }}
        onClick={
          isClickable
            ? () => this.openLink(this.props.link)
            : () => console.log('not allowed')
        }
        icon={customIcon}
        ref={this.markerRef}
        optimized={false}
        defaultTitle={defaultTitle}
        label={''}
      >
        {showInfoBox && (
          <React.Fragment>
            <InfoWindow onCloseClick={() => this.handleShowInfoBox(false)}>
              <div className="riskTracker__infoCont">
                <div className="riskTracker__boxes box__one">
                  {showIcon && (
                    <span className="s--icon infoWindow">
                      <img
                        // src={(RISK_CATEGORY_URLS[riskCategoryId] && RISK_CATEGORY_URLS[riskCategoryId].riskLevels[relevency].url) || ''}
                        src={this.renderIcon()}
                        alt="logo"
                      />
                    </span>
                  )}
                </div>
                <div className="riskTracker__boxes box__two">
                  <div className={classes.rootAlertListWrap}>
                    <p className="alert--title">{title}</p>
                    {showLocations && (
                      <p>Event Location: {locationName ? locationName : '-'}</p>
                    )}
                    {showPill && (
                      <span
                        className={'cat__pill'}
                        style={{
                          background: pinMappingHex[riskCategoryId],
                          fontWeight: 400,
                          border: `1px solid ${pinMappingHex[riskCategoryId]}`,
                        }}
                      >
                        {this.state.mappedCategory}
                      </span>
                    )}
                  </div>
                </div>
              </div>
            </InfoWindow>
          </React.Fragment>
        )}
      </Marker>
    );
  }
}

export default CustomMarker;
