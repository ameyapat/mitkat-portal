import React, { Component } from 'react';
import { withScriptjs, withGoogleMap, GoogleMap } from 'react-google-maps';
import CustomMarker from './CustomMarker';
import { connect } from 'react-redux';
import { withCookies } from 'react-cookie';
import low from './assets/img/Low.svg';
import medium from './assets/img/Medium.svg';
import high from './assets/img/High.svg';
// common style
import '../../sass/style.scss';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import { fetchApi } from '../../../../helpers/http/fetch';
// map style
import customMapStyles from '../../../../helpers/universalMapStyles.json';
import customMapStylesLight from '../../../../helpers/universalMapStylesLight.json';
//import customClientIcon from '../../../../assets/icons/pin.png'; // office location
import customClientIcon from './assets/clientLocations.svg';
import customClientIconDark from './assets/clientLocationsDark.svg';

const pinMapping = {
  1: low,
  2: medium,
  3: high,
};

@withCookies
@connect(state => {
  return {
    appState: state.appState,
    mapState: state.mapState,
    rime: state.rime,
    partnerDetails: state.partnerDetails.partnerDetails,
    themeChange: state.themeChange,
  };
})
@withScriptjs
@withGoogleMap
class MapWithAMarker extends Component {
  state = {
    latState: 20.5937,
    lngState: 80.9629,
    showLocation: true,
    defaultOptions: {
      mapTypeControl: false,
      streetViewControl: false,
      zoomControl: false,
      fullscreenControl: false,
      draggable: true,
    },
    mappedCategory: '',
  };

  componentDidMount() {
    this.setState({
      showLocation: this.props.appState.appPermissions.locationPinSetting,
    });
  }

  componentDidUpdate(prevProps) {
    if (prevProps.rime !== this.props.rime) {
      this.setState({
        showLocation: this.props.appState.appPermissions.locationPinSetting,
        defaultOptions: {
          ...this.state.defaultOptions,
          draggable: this.props.rime.lockScreen,
        },
      });
    }
  }

  renderPinsOnMap() {
    let { feed } = this.props;
    let reversedFeed = [...feed].reverse();
    return reversedFeed.map(item => {
      let lat = parseFloat(item.event_lat);
      let lng = parseFloat(item.event_lon);
      if (item.eventRiskLevel === 3) {
        return (
          <CustomMarker
            id={item.id}
            key={item.id}
            lat={lat}
            lng={lng}
            customIcon={pinMapping[item.eventRiskLevel]}
            title={item.eventTitle}
            riskcategory={item.riskCategory}
            riskCategoryId={item.eventPrimaryRiskCategory}
            riskId={item.eventPrimaryRiskCategory}
            isLive={item.ispresent}
            showIcon={true}
            link={item.eventLink}
            setCenter={this.setCenter}
            feed={feed}
            distance={item.distance}
            locationName={item.eventLoc}
            relevency={item.eventRiskLevel}
            defaultTitle="highRisk"
          />
        );
      } else {
        return (
          <CustomMarker
            id={item.id}
            key={item.id}
            lat={lat}
            lng={lng}
            customIcon={pinMapping[item.eventRiskLevel]}
            title={item.eventTitle}
            riskcategory={item.riskCategory}
            riskCategoryId={item.eventPrimaryRiskCategory}
            riskId={item.eventPrimaryRiskCategory}
            isLive={item.ispresent}
            showIcon={true}
            link={item.eventLink}
            setCenter={this.setCenter}
            feed={feed}
            distance={item.distance}
            locationName={item.eventLoc}
            relevency={item.eventRiskLevel}
            defaultTitle="notHighRisk"
          />
        );
      }
    });
  }

  renderClientPins() {
    let { clientLocations } = this.props;
    const {
      themeChange: { setDarkTheme },
    } = this.props;

    return clientLocations.map(item => {
      let lat = parseFloat(item.latitude);
      let lng = parseFloat(item.longitude);

      return (
        <CustomMarker
          id={item.id}
          key={item.id}
          lat={lat}
          lng={lng}
          customIcon={setDarkTheme ? customClientIconDark : customClientIcon}
          isClickable={false}
          setCenter={this.setCenter}
          title={item.title}
          showLocations={
            clientLocations && clientLocations.length > 0 ? false : true
          }
        />
      );
    });
  }

  setCenter = (lat, lng) => this.setState({ latState: lat, lngState: lng });

  render() {
    const { latState, lngState, showLocation, defaultOptions } = this.state;
    let { latitude, longitude, focus } = this.props.partnerDetails;
    const {
      themeChange: { setDarkTheme },
      clientLocations,
    } = this.props;
    return (
      <GoogleMap
        defaultZoom={focus}
        defaultOptions={defaultOptions}
        options={{
          ...defaultOptions,
          styles: setDarkTheme ? customMapStyles : customMapStylesLight,
        }}
        defaultCenter={{ lat: 35, lng: 30 }}
        //center={{ lat: latState, lng: lngState }}
      >
        {this.props.feed.length > 0 && this.renderPinsOnMap()}
        {clientLocations &&
          clientLocations.length > 0 &&
          showLocation &&
          this.renderClientPins()}
      </GoogleMap>
    );
  }
}

export default MapWithAMarker;
