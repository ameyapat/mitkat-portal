import React, { Component } from 'react';
// styles
import './style.scss';
// components
import MapApi from './MapApi';
// hoc
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({});

class RimeMap extends Component {

  state = {
    dynamicHeight: 0
  }

  componentDidMount(){
      let { dynamicHeight } = this.state;
      dynamicHeight = document.querySelector('.mapCont').getBoundingClientRect().top;
      this.setState({ dynamicHeight: window.innerHeight - dynamicHeight - 100 });
  }

  render() {
    
    return (        
        <div className="rimeMap">
            <div className="search--toolbar"></div>
            <div className="mapCont" style={{ height: `${this.state.dynamicHeight}px` }}>
                <MapApi />
            </div>
        </div>
    )
  }
}

export default withStyles(styles, { withTheme: true })(RimeMap);
