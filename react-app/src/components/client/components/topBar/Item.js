import React from 'react';
import './style.scss';
//
import { RISK_CATEGORY_ICONS } from '../../../../helpers/constants';

const Item = ({ catId, category, numberOfEvents }) => (
    <div className="metricsItem">
        <div className="metrics-icon--title" title={category}>
            <span><i className={RISK_CATEGORY_ICONS[catId]}></i></span>
        </div>
        <div>
            <p>{ numberOfEvents }</p>
        </div>
    </div>
)

export default Item;
