import React, { Component } from 'react';
import './style.scss';
//
import Item from './Item';
import { withCookies } from 'react-cookie';
import { connect } from 'react-redux';
import { withRouter } from "react-router-dom";
//
import { fetchApi } from '../../../../helpers/http/fetch';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';

@withCookies
@withRouter
@connect(state => {
    return{
        mapState: state.mapState
    }
})
class TopBar extends Component {

    state = {
        categories: [],
        filters: {}
    }

    componentDidMount(){
        this.setState({filters: this.props.mapState.filters}, () => this.fetchMetrics())
    }

    fetchMetrics = () => {
        let { cookies } = this.props;
        let { filters } = this.state;
        let authToken = cookies.get('authToken');
        let reqObj = {
            method: 'POST',
            isAuth: true,
            authToken,
            url: API_ROUTES.clientMetricsBar,
            data: filters,
            showToggle: true,
        }

        fetchApi(reqObj)
        .then(data => {
            this.setState({categories: data});
        })

    }

    getMetrics = () => {
        let { categories } = this.state;
        let metricsXML = [];

        metricsXML = categories.length > 0 && categories.map((item, index) => {
            return <Item 
                    catId={item.riskcategoryid} 
                    category={item.riskCategory} 
                    numberOfEvents={item.numberofevents} 
                />
        })

        return metricsXML;
    }

  render() {

    // let { categories } = this.state;
    return (
      <div id="topBar">
        {/* {categories.length > 0 ? this.getMetrics() : 'No Updates Availables!'} */}
        <div
          className="bar--items"
          onClick={() => this.props.history.push('/covid-19')}
        >
          <i className="fas fa-viruses"></i>
          <label>Covid-19 Dashboard</label>
        </div>
        <div
          className="bar--items"
          onClick={() => this.props.history.push('/covid-19-map')}
        >
          <i className="fas fa-virus"></i>
          <label>Covid-19 Map</label>
        </div>
      </div>
    )
  }
}

export default TopBar;
