import React, { Component } from 'react';
//
import './style.scss';

class Filters extends Component{
    render(){
        return(
            <div id="filter" className="filterBox">
                <div className="filterBox__title">
                    <h2 className="transparent-title">Sort By</h2>
                </div>
                <div className="filterBox__body">
                    filter options goes here
                </div>
            </div>
        )
    }
}

export default Filters