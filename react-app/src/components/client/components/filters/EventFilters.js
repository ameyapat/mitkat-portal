import React, { Component } from 'react';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
// import Slider from '@material-ui/lab/Slider';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import Checkbox from '@material-ui/core/Checkbox';
import { TOR, LOR } from './constants';
import injectSheet from 'react-jss';
import style from './style.js';
import { withCookies } from 'react-cookie';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import { fetchApi } from '../../../../helpers/http/fetch';
import MultiSelect from '../../../ui/multiSelect';
import TextField from '@material-ui/core/TextField';
// import { filterArrObjects, hasData } from '../../../../helpers/utils';
import { saveAs } from "file-saver";
import { toggleToast } from "../../../../actions";
import { connect } from 'react-redux';
import store from '../../../../store';
import { fetchCities, fetchStates, fetchCountries, filterArrObjects } from '../../../../helpers/utils';
import { applyArchiveFilters, resetPageNo,
     handlePageNo, resetEventFilters, setTorFilters, setLorFilters, handleSelectAllArchivesTOR, handleSelectAllArchivesLOR } from '../../../../actions';  

@withCookies
@injectSheet(style)
@connect(state => {
    return {
        eventArchiveState: state.eventArchiveState
    }
})
class EventFilters extends Component {

    state = {
        countries: [],
        states: [],
        cities: [],
        selectedCountries: [],
        selectedStates: [],
        selectedCities: [],
        selectedStartDate: "",
        selectedEndDate: "",
        typeOfRisk: {
            civil: true,
            cyber: true,
            crime: true,
            infrastructure: true,
            health: true,
            environment: true,
            insurgency: true,
            travelRisks: true,
            naturalDisasters: true,
            externalThreats: true,
            political: true,
            regulatory: true,
        },
        levelOfRisk: {
            verylow: true,
            low: true,
            medium: true,
            high: true,
            veryhigh: true
        },
        archiveFilters: {
            risklevel: [],
            riskcategory: [],
            countries: null,
            states: null,
            cities: null,
            startDate: "",
            endDate:""
        },
        pageNo: 0,
        showTOR: false,
        showLOR: true,
        isTORChecked: true,
        isLORChecked: true
    }

    componentDidMount(){
        let { cookies, eventArchiveState: { typeOfRisk, levelOfRisk, pageNo, selectAllTOR, selectAllLOR, archiveFilters } } = this.props;
        let authToken = cookies.get('authToken');
        
        this.setState({ pageNo, typeOfRisk, levelOfRisk, isTORChecked: selectAllTOR, isLORChecked: selectAllLOR, archiveFilters });

        fetchCountries(authToken).then(countries => this.setState({countries}));
        fetchStates(authToken).then(states => this.setState({states}));
        fetchCities(authToken).then(cities => this.setState({cities}));
    }

    componentDidUpdate(prevProps, prevState){
        let prevPageNo = prevProps.eventArchiveState.pageNo;
        let currPageNo = this.props.eventArchiveState.pageNo;
        let prevTypeOfRisk = prevProps.eventArchiveState.typeOfRisk;
        let currTypeOfRisk = this.props.eventArchiveState.typeOfRisk;
        //
        let prevLor = prevProps.eventArchiveState.levelOfRisk;
        let currLor = this.props.eventArchiveState.levelOfRisk;
        //page no
        if(prevPageNo !== currPageNo){
            this.setState({pageNo: currPageNo});
        }
        //tor
        if(prevTypeOfRisk !== currTypeOfRisk){
            this.setState({typeOfRisk: currTypeOfRisk})
        }
        //lor
        if(prevLor !== currLor){
            this.setState({levelOfRisk: currLor})
        }
        
        if(this.props.eventArchiveState.selectAllTOR !== prevProps.eventArchiveState.selectAllTOR){
            this.setState({ isTORChecked: this.props.eventArchiveState.selectAllTOR });
        }
        
        if(this.props.eventArchiveState.selectAllLOR !== prevProps.eventArchiveState.selectAllLOR){
            this.setState({ isLORChecked: this.props.eventArchiveState.selectAllLOR });
        }

        const prevArchives = prevProps.eventArchiveState.archiveFilters;
        const currArchives = this.props.eventArchiveState.archiveFilters;

        if(prevArchives !== currArchives){
            this.setState({archiveFilters: currArchives})
        }

    }

    resetFilters = async () => {
        try{
           await store.dispatch(resetEventFilters());
           await store.dispatch(resetPageNo());
           this.applyFilters();
        }catch(e) {
            console.log(e);
        }
    }

    applyFilters = () => {
        // let { cookies, eventArchiveState: { archiveFilters } } = this.props;
        let { cookies } = this.props;
        let { pageNo, archiveFilters } = this.state;        
        let authToken = cookies.get('authToken');
        let reqData = {
            "risklevel": archiveFilters.risklevel,
            "riskcategory": archiveFilters.riskcategory,
            "countries": archiveFilters.selectedCountries ? filterArrObjects(archiveFilters.selectedCountries) : null,
            "states": archiveFilters.selectedStates ? filterArrObjects(archiveFilters.selectedStates) : null,
            "cities":archiveFilters.selectedCities ? filterArrObjects(archiveFilters.selectedCities) : null,
            "startdate": archiveFilters.startDate,
            "enddate": archiveFilters.endDate 
        };
        
        let reqObj = {
            method: 'POST',
            isAuth: true,
            authToken: authToken,
            data: reqData,
            url: `${API_ROUTES.getEventArchives}/${pageNo}`,
            showToggle: true,
        }

        fetchApi(reqObj)
        .then(data => { 
            let { pageNo } = this.state;
            let newPageNo = pageNo <= data.totalPages ? pageNo + 1 : pageNo; 
            store.dispatch(handlePageNo(newPageNo))
            this.props.handleFilteredData(data); 
        })
        .then(() => {
            this.downloadFilters()
        })
    }

    downloadFilters = () => {
        // let { cookies, eventArchiveState: { archiveFilters } } = this.props;
        let { cookies } = this.props;
        const { archiveFilters } = this.state;
        let authToken = cookies.get("authToken");

        let reqData = {
            "risklevel": archiveFilters.risklevel,
            "riskcategory": archiveFilters.riskcategory,
            "countries": archiveFilters.selectedCountries ? filterArrObjects(archiveFilters.selectedCountries) : null,
            "states": archiveFilters.selectedStates ? filterArrObjects(archiveFilters.selectedStates) : null,
            "cities":archiveFilters.selectedCities ? filterArrObjects(archiveFilters.selectedCities) : null,
            "startdate": archiveFilters.startDate,
            "enddate": archiveFilters.endDate 
        };

        let reqObj = {
          url: `${API_ROUTES.downloadEventArchives}`,
          isAuth: true,
          authToken,
          method: "POST",
          isBlob: true,
          data: reqData,
          showToggle: true,
        };        
    
        fetchApi(reqObj)
          .then(blobObj => {
            let objUrl = new Blob([blobObj], {
              type:
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
            });
            saveAs(objUrl, "event-details.xlsx");
    
            let toastObj = {
              toastMsg: "Downloading file...",
              showToast: true
            };
            store.dispatch(toggleToast(toastObj));
          })
          .catch(e => {
            let toastObj = {
              toastMsg: "Error downloading excel sheet",
              showToast: true
            };
            store.dispatch(toggleToast(toastObj));
          });
      };

    handleTORChange = type => e => {
        let { typeOfRisk, archiveFilters, archiveFilters: { riskcategory } } = this.state;
        typeOfRisk[type] = e.target.checked;
        let checkedVal = parseInt(e.target.value);
        
        if(riskcategory.includes(checkedVal)){
            let index = riskcategory.indexOf(checkedVal);
            riskcategory = riskcategory.splice(index, 1);
        }else{
            riskcategory.push(checkedVal);
        }
       
        for(let key in typeOfRisk){
            if(!typeOfRisk[key]) {
              this.setState({isTORChecked: false}, () => {
                store.dispatch(handleSelectAllArchivesTOR(this.state.isTORChecked));
              });
              return;
            }
            this.setState({isTORChecked: true}, () => {
              store.dispatch(handleSelectAllArchivesTOR(this.state.isTORChecked));
            });
          }

          //SET_TYPE_OF_RISK
          store.dispatch(setTorFilters(typeOfRisk))
          this.setState({typeOfRisk, archiveFilters}, () => {
              store.dispatch(resetPageNo());
              store.dispatch(applyArchiveFilters(archiveFilters));
        });
    }

    handleLORChange = type => e => {
        let { levelOfRisk, archiveFilters, archiveFilters: { risklevel } } = this.state;
        levelOfRisk[type] = e.target.checked;
        let checkedVal = parseInt(e.target.value);
        
        if(risklevel.includes(checkedVal)){
            let index = risklevel.indexOf(checkedVal);
            risklevel = risklevel.splice(index, 1);
        }else{
            risklevel.push(checkedVal)
        }

        for(let key in levelOfRisk){
            if(!levelOfRisk[key]) {
              this.setState({isLORChecked: false}, () => {
                store.dispatch(handleSelectAllArchivesLOR(this.state.isLORChecked));
              });
              return;
            }
            this.setState({isLORChecked: true}, () => {
              store.dispatch(handleSelectAllArchivesLOR(this.state.isLORChecked));
            });
          }

        //SET_LEVEL_OF_RISK
        store.dispatch(setLorFilters(levelOfRisk));

        this.setState({levelOfRisk, archiveFilters}, () => {
            store.dispatch(resetPageNo())
            store.dispatch(applyArchiveFilters(archiveFilters));
        })
    }

    getTorXML = () => {
        let { classes } = this.props;
        let { typeOfRisk } = this.state;
        let TORXML = [];

         return TORXML = TOR.map(item => {
            return <div key={item.id} className={classes.checkBoxWrapEvent}>
            <FormControlLabel 
                classes={{
                    label: classes.labelEvent
                }}
                control={
                    <Checkbox
                        className={classes.sizeEvent}
                        checked={typeOfRisk[item.type]}
                        onChange={this.handleTORChange(item.type)}
                        value={item.value}
                        icon={<CheckBoxOutlineBlankIcon className={classes.sizeIcon} />}
                        checkedIcon={<CheckBoxIcon className={classes.sizeIcon} />}
                    />
                } 
                label={item.label}
            />
            {/* <span className={classes.iconWrap}><i class={item.icon} aria-hidden="true"></i></span> */}
        </div>
        })
    }

    getLorXML = () => {
        let { classes } = this.props;
        let { levelOfRisk } = this.state;
        let LORXML = [];

        return LORXML = LOR.map(item => {
            return <div key={item.id} className={classes.checkBoxWrap}>
            <FormControlLabel 
                classes={{
                    label: classes.labelEvent
                }}
                control={
                    <Checkbox
                        className={classes.sizeEvent}
                        checked={levelOfRisk[item.type]}
                        onChange={this.handleLORChange(item.type)}
                        value={item.value}
                        icon={<CheckBoxOutlineBlankIcon className={classes.sizeIcon} />}
                        checkedIcon={<CheckBoxIcon className={classes.sizeIcon} />}
                    />
                } 
                label={item.label}
            />
            {/* <span className={classes.colorCode} style={{backgroundColor: item.colorCode}}></span> */}
        </div>
        })
    }

    handleMultiSelect = (value, type) => {
        let { archiveFilters } = this.state;
        let values = this.state[type];
        if(values.includes(value)){
          let index = values.indexOf(value);
          values.splice(index, 1);
        }else{
          values = [...value];
        }

        archiveFilters[type] = values;
        this.setState({[type]: values, archiveFilters}, () => {
            store.dispatch(resetPageNo());
            store.dispatch(applyArchiveFilters(archiveFilters));
        });
      }

    handleDateChange = (e, type) => {
        let { archiveFilters } = this.state;
        archiveFilters[type] = e.target.value;
        this.setState({archiveFilters}, () => {
            store.dispatch(resetPageNo());
            store.dispatch(applyArchiveFilters(archiveFilters))
        })
    }

    toggleFilter = (type) => { this.setState({[type]: !this.state[type]}) }

    handleSelectAllTORChange = (e) => {
        let { typeOfRisk, archiveFilters } = this.state;

        for(let key in typeOfRisk){
            typeOfRisk[key] = e.target.checked;
        }
        
        let archiveFiltersAll = { ...archiveFilters, riskcategory: [1,2,3,4,5,6,7,8,9,10,11,12] };
        let archiveFiltersNone = { ...archiveFilters, riskcategory: [] };

        this.setState({isTORChecked: e.target.checked}, () => {
            store.dispatch(handleSelectAllArchivesTOR(this.state.isTORChecked));
            //SET_TYPE_OF_RISK
            store.dispatch(setTorFilters(typeOfRisk))
            store.dispatch(applyArchiveFilters(this.state.isTORChecked ? archiveFiltersAll : archiveFiltersNone));
        })
    }

    handleSelectAllLORChange = (e) => {
        let { levelOfRisk, archiveFilters } = this.state;

        for(let key in levelOfRisk){
            levelOfRisk[key] = e.target.checked;
        }

        let archiveFiltersAll = { ...archiveFilters, risklevel: [1,2,3,4,5,6,7] };
        let archiveFiltersNone = { ...archiveFilters, risklevel: [] };

        this.setState({isLORChecked: e.target.checked}, () => {
            store.dispatch(handleSelectAllArchivesLOR(this.state.isLORChecked));
            //SET_LEVEL_OF_RISK
            store.dispatch(setLorFilters(levelOfRisk));
            store.dispatch(applyArchiveFilters(this.state.isTORChecked ? archiveFiltersAll : archiveFiltersNone));
        })
    }

  render() {
      let { classes } = this.props;
      let { states, countries, cities, selectedCountries, selectedStates, 
        selectedCities, showLOR, showTOR } = this.state;

    return (
      <React.Fragment>
          <div>
                <div className="filterWrapper__filters">
                    <div className={"filtersWrapBox"}>
                        <div className="filtersWrapBox__boxOne">  
                            <div className="filtersWrapBox__boxOne__typeOfRisk">
                                <div className="typeOfRiskFilter">
                                    <h3 className="filter--by--label" onClick={() => this.toggleFilter('showTOR')}>
                                        Types Of Risk Categories 
                                        <span className="typeOfRiskFilter_caret"><i className={`fas ${showTOR ? 'fa-caret-up' : 'fa-caret-down'}`}></i></span>
                                    </h3>
                                    { showTOR && <>
                                        <FormControlLabel 
                                            classes={{
                                                label: classes.labelEvent
                                            }}
                                            control={
                                                <Checkbox
                                                    className={classes.sizeEvent}
                                                    checked={this.state.isTORChecked}
                                                    onChange={this.handleSelectAllTORChange}
                                                    value={this.state.isTORChecked}
                                                    icon={<CheckBoxOutlineBlankIcon className={classes.sizeIcon} />}
                                                    checkedIcon={<CheckBoxIcon className={classes.sizeIcon} />}
                                                />
                                            } 
                                            label={"Select All"}
                                        />
                                    {this.getTorXML()}
                                    </> 
                                    }
                                </div>
                                <div className="levelOfRisk">
                                    <h3 className="filter--by--label" onClick={() => this.toggleFilter('showLOR')}>
                                        Level Of Risk
                                        <span className="typeOfRiskFilter_caret"><i className={`fas ${showLOR ? 'fa-caret-up' : 'fa-caret-down'}`}></i></span>
                                    </h3>
                                    { showLOR && 
                                    <>
                                    <FormControlLabel 
                                            classes={{
                                                label: classes.labelEvent
                                            }}
                                            control={
                                                <Checkbox
                                                    className={classes.sizeEvent}
                                                    checked={this.state.isLORChecked}
                                                    onChange={this.handleSelectAllLORChange}
                                                    value={this.state.isLORChecked}
                                                    icon={<CheckBoxOutlineBlankIcon className={classes.sizeIcon} />}
                                                    checkedIcon={<CheckBoxIcon className={classes.sizeIcon} />}
                                                />
                                            } 
                                            label={"Select All"}
                                        />
                                    {this.getLorXML()}
                                    </> }
                                </div>
                            </div>                                                                             
                        </div>
                        <div className="filtersWrapBox__boxTwo">
                            <div className="dateWrapper__filters">
                                <div className="startDate">
                                    <TextField
                                        id="startDate"
                                        label="Start Date"
                                        type="date"
                                        defaultValue="2019-01-01"
                                        className={classes.textField}
                                        onChange={(e) => this.handleDateChange(e, 'startDate')}
                                        InputLabelProps={{
                                            shrink: true,
                                            classes: {
                                               root: classes.rootLabel
                                            }
                                        }}
                                        InputProps={{
                                            classes: {
                                                root: classes.rootInput,
                                                input: classes.rootInput,
                                                notchedOutline: classes.notchedOutline,
                                            },
                                            endAdornment: null
                                        }}
                                        variant='outlined'
                                    />
                                </div>
                                <div className="endDate">
                                    <TextField
                                        id="endDate"
                                        label="End Date"
                                        type="date"
                                        defaultValue="2019-12-31"
                                        onChange={(e) => this.handleDateChange(e, 'endDate')}
                                        InputLabelProps={{
                                            shrink: true,
                                            classes: {
                                               root: classes.rootLabel
                                            }
                                        }}
                                        InputProps={{
                                            classes: {
                                                root: classes.rootInput,
                                                input: classes.rootInput,
                                                // underline: classes.underline,
                                                notchedOutline: classes.notchedOutline,
                                            }
                                        }}
                                        variant='outlined'
                                    />
                                </div>
                            </div>
                            <div>
                                <div className="countries">
                                    <h3 className="filter--by--label">Countries</h3>
                                    <MultiSelect
                                        selectedValues={selectedCountries}
                                        placeholder={'Select Countries'}
                                        parentEventHandler={(value) => this.handleMultiSelect(value, 'selectedCountries')}
                                        options={countries}

                                        
                                        
                                    />
                                </div>
                                <div className="states">
                                    <h3 className="filter--by--label">States</h3>
                                    <MultiSelect
                                        selectedValues={selectedStates}
                                        placeholder={'Select States'}
                                        parentEventHandler={(value) => this.handleMultiSelect(value, 'selectedStates')}
                                        options={states}
                                    />
                                </div>
                                <div className="cities">
                                    <h3 className="filter--by--label">Cities</h3>
                                    <MultiSelect
                                        selectedValues={selectedCities}
                                        placeholder={'Select Cities'}
                                        parentEventHandler={(value) => this.handleMultiSelect(value, 'selectedCities')}
                                        options={cities}
                                    />
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div className="filterWrapper__filters__footer">                          
                        <div className="btnWrapper__filters">
                            <div className="apply__filters">
                                <button 
                                className="btn btn--apply" 
                                onClick={() => this.setState({pageNo: 0}, () => this.applyFilters())}
                                >
                                Download
                                </button>                        
                                {/* <button className="btn btn--reset" onClick={() => this.resetFilters()}>Reset</button> */}
                            </div>
                        </div>
                        {/* <div className="downloadBtnWrap">
                            <button 
                                className="btn btn--download" 
                                onClick={() => this.downloadFilters()}
                            >
                                Download
                            </button>
                        </div> */}
                    </div>
                </div>                
          </div>


          
        {/* 
        
        
         */}
      </React.Fragment>
    )
  }
}

export default EventFilters;
