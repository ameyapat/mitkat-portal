import React, { Component } from 'react';
import style from './style';
import './style.scss';
import injectSheet from 'react-jss';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';

import { withStyles } from '@material-ui/core/styles';
import CustomCheckboxUi from '../../../ui/customCheckboxUi';
import { TOR, LOR, TOE } from './constants';
import store from '../../../../store';
//actions
import {
  setFilters,
  setTor,
  setLor,
  setToe,
  setEventDetails,
  setDays,
  handleSelectAllTOE,
  handleSelectAllTOR,
  handleSelectAllLOR,
  setRegions,
  setSelectAllRegions,
} from '../../../../actions';
import { fetchRiskTracker } from '../../../../actions/riskTrackerAction';
//helpers
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import { fetchApi } from '../../../../helpers/http/fetch';
import { connect } from 'react-redux';
import { withCookies } from 'react-cookie';

import { getRegions } from '../../../../requestor/quadrants/requestor';
import { Slider } from '@material-ui/core';

const styles = {
  root: {
    '&$checked': {
      color: 'var(--whiteMediumEmp)',
    },
  },
  size: {
    width: 40,
    height: 30,
  },
  sizeIcon: {
    fontSize: 20,
    //   color: 'var(--whiteMediumEmp)',
    color: 'rgba(171, 193, 66, 0.87)',
  },
};

@withCookies
@withStyles(styles)
@injectSheet(style)
@connect(state => {
  return {
    appState: state.appState,
    mapState: state.mapState,
    rime: state.rime,
    riskTracker: state.riskTracker,
  };
})
class Filters extends Component {
  state = {
    showTorFilters: true,
    showLorFilters: true,
    showToeFilters: true,
    isTOEChecked: true,
    isTORChecked: true,
    isLORChecked: true,
    regions: [],
    isSelectAllRegion: true,
    isSingleSelect: false,
    showTypeOfEvent: false,
    showTypeOfRisk: false,
    showLevelOfRisk: false,
    showRegion: false,
  };

  UNSAFE_componentWillMount() {
    let {
      mapState: { filters, typeOfRisk, levelOfRisk, typeOfEvent, hourValue },
    } = this.props;
    this.setState({ filters, typeOfRisk, levelOfRisk, typeOfEvent, hourValue });
  }

  componentDidMount() {
    const {
      rime,
      mapState: {
        selectAllTOE,
        selectAllTOR,
        selectAllLOR,
        filters,
        typeOfRegions,
      },
      dispatch,
    } = this.props;
    this.setState({
      isTOEChecked: selectAllTOE,
      isTORChecked: selectAllTOR,
      isLORChecked: selectAllLOR,
    });

    if (!typeOfRegions.length > 0) {
      getRegions()
        .then(regions => {
          const regionIds = [];
          regions = regions.map(item => {
            item.isSelected = true;
            regionIds.push(item.id);
            return item;
          });

          // dispatch(setFilters({ ...filters, region: regionIds }));
          dispatch(setRegions(regions));
        })
        .catch(e => {
          console.log(e);
        });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    let {
      mapState: {
        filters,
        levelOfRisk,
        typeOfRisk,
        typeOfEvent,
        hourValue,
        selectAllTOE,
        selectAllRegions,
      },
    } = this.props;
    let prevFilters = prevProps.mapState.filters;
    let currFilters = filters;

    if (prevFilters !== currFilters) {
      this.setState({
        filters: currFilters,
        levelOfRisk,
        typeOfRisk,
        typeOfEvent,
        hourValue,
      });
    }

    if (
      !this.state.isSingleSelect &&
      selectAllRegions !== prevProps.mapState.selectAllRegions
    ) {
      this.handleSelectAllRegion();
    }

    if (this.props.mapState.selectAllTOE !== prevProps.mapState.selectAllTOE) {
      this.setState({
        isTOEChecked: this.props.mapState.selectAllTOE,
      });
    }

    if (this.props.mapState.selectAllTOR !== prevProps.mapState.selectAllTOR) {
      this.setState({
        isTORChecked: this.props.mapState.selectAllTOR,
      });
    }

    if (this.props.mapState.selectAllLOR !== prevProps.mapState.selectAllLOR) {
      this.setState({
        isLORChecked: this.props.mapState.selectAllLOR,
      });
    }
  }

  toggleAccordion = type => {
    this.setState({ [type]: !this.state[type] });
  };

  handleTORChange = type => e => {
    let {
      typeOfRisk,
      filters: { riskcategory },
    } = this.state;
    typeOfRisk[type] = e.target.checked;
    let checkedValue = parseInt(e.target.value);

    if (riskcategory.includes(checkedValue)) {
      let index = riskcategory.indexOf(checkedValue);
      riskcategory.splice(index, 1);
    } else {
      riskcategory.push(checkedValue); //push selected types value eg: 1, 2 in the array
    }

    for (let key in typeOfRisk) {
      if (!typeOfRisk[key]) {
        this.setState({ isTORChecked: false }, () => {
          store.dispatch(handleSelectAllTOR(this.state.isTORChecked));
        });
        return;
      }
      this.setState({ isTORChecked: true }, () => {
        store.dispatch(handleSelectAllTOR(this.state.isTORChecked));
      });
    }

    this.setState(
      {
        typeOfRisk,
        riskcategory,
      },
      () => {
        store.dispatch(setTor(this.state.typeOfRisk)); //sets tor in redux
        store.dispatch(setFilters(this.state.filters)); //sets new filter values in redux
        // this.applyFilters();
      },
    );
  };

  handleLORChange = type => e => {
    let {
      levelOfRisk,
      filters: { risklevel },
    } = this.state;
    levelOfRisk[type] = e.target.checked;
    let checkedValue = parseInt(e.target.value);

    if (risklevel.includes(checkedValue)) {
      let index = risklevel.indexOf(checkedValue);
      risklevel.splice(index, 1);
    } else {
      risklevel.push(checkedValue);
    }

    for (let key in levelOfRisk) {
      if (!levelOfRisk[key]) {
        this.setState({ isLORChecked: false }, () => {
          store.dispatch(handleSelectAllLOR(this.state.isLORChecked));
        });
        return;
      }
      this.setState({ isLORChecked: true }, () => {
        store.dispatch(handleSelectAllLOR(this.state.isLORChecked));
      });
    }

    this.setState(
      {
        levelOfRisk,
        risklevel,
      },
      () => {
        store.dispatch(setLor(this.state.levelOfRisk)); //sets tor in redux
        store.dispatch(setFilters(this.state.filters)); //sets new filter values in redux
        // this.applyFilters();
      },
    );
  };

  handleTOEChange = type => e => {
    let {
      typeOfEvent,
      filters: { eventtense },
    } = this.state;
    typeOfEvent[type] = e.target.checked;
    let checkedValue = parseInt(e.target.value);

    if (eventtense.includes(checkedValue)) {
      let index = eventtense.indexOf(checkedValue);
      eventtense.splice(index, 1);
    } else {
      eventtense.push(checkedValue);
    }

    for (let key in typeOfEvent) {
      if (!typeOfEvent[key]) {
        this.setState({ isTOEChecked: false }, () => {
          store.dispatch(handleSelectAllTOE(this.state.isTOEChecked));
        });
        return;
      }
      this.setState({ isTOEChecked: true }, () => {
        store.dispatch(handleSelectAllTOE(this.state.isTOEChecked));
      });
    }

    this.setState(
      {
        typeOfEvent,
        eventtense,
      },
      () => {
        store.dispatch(setToe(this.state.typeOfEvent)); //sets toe in redux
        store.dispatch(setFilters(this.state.filters)); //sets new filter values in redux
        // this.applyFilters();
      },
    );
  };

  resetFilters = () => {
    this.handleHourChange(1, 1);
    let {
      mapState: { typeOfEvent, typeOfRisk, levelOfRisk, filters },
    } = this.props;

    for (let key in typeOfEvent) {
      typeOfEvent[key] = true;
    }
    for (let key in typeOfRisk) {
      typeOfRisk[key] = true;
    }
    for (let key in levelOfRisk) {
      levelOfRisk[key] = true;
    }
    this.setState(
      {
        isTOEChecked: true,
        isTORChecked: true,
        isLORChecked: true,
        isSingleSelect: false,
      },
      () => {
        store.dispatch(handleSelectAllTOE(this.state.isTOEChecked));
        store.dispatch(handleSelectAllTOR(this.state.isTORChecked));
        store.dispatch(handleSelectAllLOR(this.state.isLORChecked));
        store.dispatch(
          setFilters({
            ...filters,
            eventtense: [0, 1, 2],
            riskcategory: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
            risklevel: [1, 2, 3, 4, 5],
          }),
        );
        store.dispatch(setSelectAllRegions(true));
      },
    );
  };

  applyFilters = () => {
    let { filters } = this.state;
    // let { authToken } = this.props.appState;
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let reqData = filters;

    let reqObj = {
      method: 'POST',
      isAuth: true,
      authToken: authToken,
      data: reqData,
      url: API_ROUTES.getEventListPortal,
      showToggle: true,
    };

    // fetchApi(reqObj).then(data => {
    //   store.dispatch(setEventDetails(data)); //setEventDetails action is dispatched to store new fetched details
    //   this.props.handleDrawerClose();
    // });

    store.dispatch(fetchRiskTracker(reqObj));
    this.props.handleDrawerClose();
  };

  handleHourChange = (e, value) => {
    let { filters } = this.state;
    let hourValue = Math.round(value);
    filters['days'] = hourValue;
    this.setState({ hourValue, filters }, () => {
      store.dispatch(setDays(this.state.hourValue)); //sets toe in redux
      store.dispatch(setFilters(this.state.filters)); //sets new filter values in redux
      // this.applyFilters();
    });
  };

  handleSelectAll = e => {
    let {
      mapState: { typeOfEvent, filters },
    } = this.props;

    for (let key in typeOfEvent) {
      typeOfEvent[key] = e.target.checked;
    }

    this.setState({ isTOEChecked: e.target.checked }, () => {
      store.dispatch(handleSelectAllTOE(this.state.isTOEChecked));
      store.dispatch(setToe(typeOfEvent));
      store.dispatch(
        setFilters(
          this.state.isTOEChecked
            ? { ...filters, eventtense: [0, 1, 2] }
            : { ...filters, eventtense: [] },
        ),
      );
    });
  };

  selectAllTOR = e => {
    let {
      mapState: { typeOfRisk, filters },
    } = this.props;

    for (let key in typeOfRisk) {
      typeOfRisk[key] = e.target.checked;
    }

    this.setState({ isTORChecked: e.target.checked }, () => {
      store.dispatch(handleSelectAllTOR(this.state.isTORChecked));
      store.dispatch(setTor(typeOfRisk));
      store.dispatch(
        setFilters(
          this.state.isTORChecked
            ? {
                ...filters,
                riskcategory: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
              }
            : { ...filters, riskcategory: [] },
        ),
      );
    });
  };

  selectAllLOR = e => {
    let {
      mapState: { levelOfRisk, filters },
    } = this.props;

    for (let key in levelOfRisk) {
      levelOfRisk[key] = e.target.checked;
    }

    this.setState({ isLORChecked: e.target.checked }, () => {
      store.dispatch(handleSelectAllLOR(this.state.isLORChecked));
      store.dispatch(setLor(levelOfRisk));
      store.dispatch(
        setFilters(
          this.state.isLORChecked
            ? { ...filters, risklevel: [1, 2, 3, 4, 5] }
            : { ...filters, risklevel: [] },
        ),
      );
    });
  };

  getRegionsXML = () => {
    const {
      classes,
      mapState: { typeOfRegions },
    } = this.props;
    return typeOfRegions.map(item => {
      return (
        <div key={item.id} className={classes.checkBoxWrap}>
          <FormControlLabel
            classes={{
              label: classes.label,
            }}
            control={
              <Checkbox
                className={classes.size}
                checked={item.isSelected}
                onChange={() =>
                  this.handleRegionChange(item.id, !item.isSelected)
                }
                value={item.id}
                icon={<CheckBoxOutlineBlankIcon className={classes.sizeIcon} />}
                checkedIcon={<CheckBoxIcon className={classes.sizeIcon} />}
              />
            }
            label={item.region_name}
          />
        </div>
      );
    });
  };

  handleRegionChange = (id, isSelected) => {
    let {
      mapState: {
        selectAllRegions,
        typeOfRegions,
        filters: { region },
        filters,
      },
      dispatch,
    } = this.props;

    this.setState({ isSingleSelect: true }, () => {
      typeOfRegions.map(item => {
        if (item.id === id) {
          item.isSelected = isSelected;
        }
      });

      if (region.includes(id)) {
        const index = region.indexOf(id);
        region.splice(index, 1);
      } else {
        region.push(id);
      }

      dispatch(setFilters({ ...filters, region }));
      dispatch(setRegions(typeOfRegions));
      this.checkIsSelectAll();
    });
  };

  checkIsSelectAll = () => {
    let {
      mapState: { typeOfRegions, selectAllRegions },
      dispatch,
    } = this.props;

    this.setState({ isSingleSelect: true }, () => {
      const hasSelectedEveryRegion = typeOfRegions.every(item => {
        return item.isSelected === true;
      });

      dispatch(setSelectAllRegions(hasSelectedEveryRegion));
    });
  };

  toggleRegionSelectAll = () => {
    const {
      mapState: { selectAllRegions },
      dispatch,
    } = this.props;

    this.setState({ isSingleSelect: false }, () => {
      dispatch(setSelectAllRegions(!selectAllRegions));
    });
  };

  handleSelectAllRegion = () => {
    let {
      mapState: {
        selectAllRegions,
        typeOfRegions,
        filters: { region },
        filters,
      },
      dispatch,
    } = this.props;

    typeOfRegions = typeOfRegions.map(item => {
      if (!selectAllRegions) {
        item.isSelected = false;
        region = [];
      } else {
        item.isSelected = true;
        if (!region.includes(item.id)) {
          region.push(item.id);
        }
      }
      return item;
    });

    dispatch(setRegions(typeOfRegions));
    dispatch(setFilters({ ...filters, region }));
  };

  render() {
    let {
      classes,
      mapState: { selectAllRegions },
    } = this.props;
    let {
      typeOfRisk,
      levelOfRisk,
      hourValue,
      showLorFilters,
      showTorFilters,
      showToeFilters,
      typeOfEvent,
      showTypeOfEvent,
      showTypeOfRisk,
      showLevelOfRisk,
      showRegion,
    } = this.state;

    let TORXML = TOR.map(item => {
      return (
        <div key={item.id} className={classes.checkBoxWrap}>
          <FormControlLabel
            classes={{
              label: classes.label,
            }}
            control={
              <Checkbox
                className={classes.size}
                checked={typeOfRisk[item.type]}
                onChange={this.handleTORChange(item.type)}
                value={item.value}
                icon={<CheckBoxOutlineBlankIcon className={classes.sizeIcon} />}
                checkedIcon={<CheckBoxIcon className={classes.sizeIcon} />}
              />
            }
            label={item.label}
          />
          {/* <span className={classes.iconWrap}><i class={item.icon} aria-hidden="true"></i></span> */}
        </div>
      );
    });

    let LORXML = LOR.map(item => {
      return (
        <div key={item.id} className={classes.checkBoxWrap}>
          <FormControlLabel
            classes={{
              label: classes.label,
            }}
            control={
              <Checkbox
                className={classes.size}
                checked={levelOfRisk[item.type]}
                onChange={this.handleLORChange(item.type)}
                value={item.value}
                icon={<CheckBoxOutlineBlankIcon className={classes.sizeIcon} />}
                checkedIcon={<CheckBoxIcon className={classes.sizeIcon} />}
              />
            }
            label={item.label}
          />
          {/* <span className={classes.colorCode} style={{backgroundColor: item.colorCode}}></span> */}
        </div>
      );
    });

    let TOEXML = TOE.map(item => {
      return (
        <div key={item.id} className={classes.checkBoxWrap}>
          <FormControlLabel
            classes={{
              label: classes.label,
            }}
            control={
              <Checkbox
                className={classes.size}
                checked={typeOfEvent[item.type]}
                onChange={this.handleTOEChange(item.type)}
                value={item.value}
                icon={<CheckBoxOutlineBlankIcon className={classes.sizeIcon} />}
                checkedIcon={<CheckBoxIcon className={classes.sizeIcon} />}
              />
            }
            label={item.label}
          />
          {/* <span className={classes.colorCode} style={{backgroundColor: item.colorCode}}></span> */}
        </div>
      );
    });

    return (
      <div className={classes.filterRoot}>
        <div className={classes.sortByFilter}>
          <div className="risk-tracker-wrap">
            <div className={classes.hourLabel}>
              <label>
                <span style={{ marginRight: 10, display: 'inline-block' }}>
                  <i className="fa fa-clock-o" aria-hidden="true"></i>
                </span>
                Filter by {hourValue === 1 ? 'day' : 'days'}
              </label>
              <p>{hourValue}</p>
            </div>
            <div className={classes.hours}>
              <Slider
                value={hourValue}
                aria-labelledby="label"
                onChange={this.handleHourChange}
                max={5}
                min={1}
                classes={{
                  trackBefore: classes.sliderBefore,
                  track: classes.sliderTrack,
                  thumb: classes.thumbStyle,
                }}
              />
            </div>
          </div>
        </div>
        <div className="filterModal__filtersWrapBox">
          <div className="filtersWrapBox__box">
            <div className="risk-tracker-wrap">
              <div className="filter__title__wrap">
                <span>
                  <label>
                    <span>
                      <i className="icon-uniE989" aria-hidden="true"></i>
                    </span>{' '}
                    Type of event
                  </label>
                </span>
                <span
                  className="filtersWrapBox__arrow"
                  onClick={() =>
                    this.setState({ showTypeOfEvent: !showTypeOfEvent })
                  }
                >
                  {showTypeOfEvent && <i className="fas fa-chevron-up"></i>}
                  {!showTypeOfEvent && <i className="fas fa-chevron-down"></i>}
                </span>
              </div>
              {showTypeOfEvent && (
                <div className={classes.typeOfRisk}>
                  <FormControlLabel
                    classes={{
                      label: classes.label,
                    }}
                    control={
                      <Checkbox
                        className={classes.size}
                        checked={this.state.isTOEChecked}
                        onClick={this.handleSelectAll}
                        value={this.state.isTOEChecked}
                        icon={
                          <CheckBoxOutlineBlankIcon
                            className={classes.sizeIcon}
                          />
                        }
                        checkedIcon={
                          <CheckBoxIcon className={classes.sizeIcon} />
                        }
                      />
                    }
                    label={'Select All'}
                  />
                  {TOEXML}
                </div>
              )}
            </div>
          </div>

          <div className="filtersWrapBox__box">
            <div className="risk-tracker-wrap">
              <div className="filter__title__wrap">
                <span>
                  <label>
                    <span>
                      <i className="icon-uniE90E" aria-hidden="true"></i>
                    </span>{' '}
                    Type of risk
                  </label>
                </span>
                <span
                  className="filtersWrapBox__arrow"
                  onClick={() =>
                    this.setState({ showTypeOfRisk: !showTypeOfRisk })
                  }
                >
                  {showTypeOfRisk && <i className="fas fa-chevron-up"></i>}
                  {!showTypeOfRisk && <i className="fas fa-chevron-down"></i>}
                </span>
              </div>
              {showTypeOfRisk && (
                <div className={classes.typeOfRisk}>
                  <FormControlLabel
                    classes={{
                      label: classes.label,
                    }}
                    control={
                      <Checkbox
                        className={classes.size}
                        checked={this.state.isTORChecked}
                        onClick={this.selectAllTOR}
                        value={this.state.isTORChecked}
                        icon={
                          <CheckBoxOutlineBlankIcon
                            className={classes.sizeIcon}
                          />
                        }
                        checkedIcon={
                          <CheckBoxIcon className={classes.sizeIcon} />
                        }
                      />
                    }
                    label={'Select All'}
                  />
                  {TORXML}
                </div>
              )}
            </div>
          </div>

          <div className="filtersWrapBox__box">
            <div className="risk-tracker-wrap">
              <div
                className="filter__title__wrap"
                onClick={() => this.toggleAccordion('showLorFilters')}
              >
                <span>
                  <label>
                    <span>
                      <i className="icon-uniE95E" aria-hidden="true"></i>
                    </span>{' '}
                    Level of risk
                  </label>
                </span>
                <span
                  className="filtersWrapBox__arrow"
                  onClick={() =>
                    this.setState({ showLevelOfRisk: !showLevelOfRisk })
                  }
                >
                  {showLevelOfRisk && <i className="fas fa-chevron-up"></i>}
                  {!showLevelOfRisk && <i className="fas fa-chevron-down"></i>}
                </span>
              </div>
              {showLevelOfRisk && (
                <div className={classes.typeOfRisk}>
                  <FormControlLabel
                    classes={{
                      label: classes.label,
                    }}
                    control={
                      <Checkbox
                        className={classes.size}
                        checked={this.state.isLORChecked}
                        onClick={this.selectAllLOR}
                        value={this.state.isLORChecked}
                        icon={
                          <CheckBoxOutlineBlankIcon
                            className={classes.sizeIcon}
                          />
                        }
                        checkedIcon={
                          <CheckBoxIcon className={classes.sizeIcon} />
                        }
                      />
                    }
                    label={'Select All'}
                  />
                  {LORXML}
                </div>
              )}
            </div>
          </div>
          <div className="filtersWrapBox__box">
            <div className="risk-tracker-wrap">
              <div className="filter__title__wrap">
                <span>
                  <label>
                    <span>
                      <i className="icon-uniE95E" aria-hidden="true"></i>
                    </span>{' '}
                    Region
                  </label>
                </span>
                <span
                  className="filtersWrapBox__arrow"
                  onClick={() => this.setState({ showRegion: !showRegion })}
                >
                  {showRegion && <i className="fas fa-chevron-up"></i>}
                  {!showRegion && <i className="fas fa-chevron-down"></i>}
                </span>
              </div>
              {showRegion && (
                <div className={classes.typeOfRisk}>
                  <FormControlLabel
                    classes={{
                      label: classes.label,
                    }}
                    control={
                      <Checkbox
                        className={classes.size}
                        checked={selectAllRegions}
                        onClick={() => this.toggleRegionSelectAll()}
                        value={selectAllRegions}
                        icon={
                          <CheckBoxOutlineBlankIcon
                            className={classes.sizeIcon}
                          />
                        }
                        checkedIcon={
                          <CheckBoxIcon className={classes.sizeIcon} />
                        }
                      />
                    }
                    label={'Select All'}
                  />
                  {this.getRegionsXML()}
                </div>
              )}
            </div>
          </div>
        </div>
        <div className="filter__btnWraper">
          <button
            className="btn btn--apply"
            onClick={() => this.resetFilters()}
          >
            Reset
          </button>
          <button
            className="btn btn--apply"
            onClick={() => this.applyFilters()}
          >
            Apply
          </button>
        </div>
      </div>
    );
  }
}

export default Filters;
