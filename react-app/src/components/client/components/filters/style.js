export default {
  filterRoot: {
    '& .risk-tracker-wrap': {
      // padding: '10px 20px',
      // borderTop: '1px solid rgba(255, 255, 255, 0)',
      '& .fa': {
        fontSize: '17px',
        // paddingRight: 10
      },
      '& i': {
        fontSize: 18,
      },
      '&:nth-child(1)': {
        paddingBottom: 20,
      },
    },
    '& label': {
      fontSize: '0.8em',
      fontWeight: '600',
      color: 'var(--whiteMediumEmp)',
    },
    '& .filter-title': {
      padding: '15px 20px',
      borderTop: '1px solid rgba(255,255,255,0.1)',
      position: 'relative',
      cursor: 'pointer',
      '&:hover': {
        // transition: 'all 0.3s ease-in',
        // background: '#c0392b'
      },
      '& label': {
        // paddingLeft: 10
      },
      '& .sort-by-icon': {
        position: 'absolute',
        right: 30,
        top: '50%',
        transform: 'translateY(-50%)',
      },
    },
    '& .filter-btn': {
      margin: '0px 28px 10px',
      display: 'flex',
      justifyContent: 'space-around',
      '& button': {
        background: '#e67e22',
        color: 'var(--whiteMediumEmp)',
        padding: '8px 12px',
        border: 0,
        cursor: 'pointer',
        // width: '100%',
        fontWeight: '600',
      },
      '& .clear-filter-btn': {
        background: '#7f8c8d',
        color: 'var(--whiteMediumEmp)',
        padding: '8px 12px',
        border: 0,
        cursor: 'pointer',
        // width: '100%',
        fontWeight: '600',
      },
    },
    '& .close-btn': {
      display: 'flex',
      justifyContent: 'flex-end',
      cursor: 'pointer',
      '& span': {
        fontSize: '3em',
        color: 'var(--whiteMediumEmp)',
        paddingRight: 20,
      },
    },
  },
  label: {
    color: 'var(--whiteMediumEmp)',
    fontSize: '0.98em',
  },
  labelEvent: {
    color: 'rgba(255,255,255, 0.67)',
    fontSize: '0.70em',
    paddingRight: 5,
  },
  typeOfRisk: {
    paddingTop: 10,
  },
  sortByFilter: {
    transition: 'all 0.4s ease-in',
    padding: 4,
    // maxWidth: '240px',
    display: 'flex',
    justifyContent: 'space-between',
  },
  checkBoxWrap: {
    position: 'relative',
  },
  checkBoxWrapEvent: {
    position: 'relative',
    flexBasis: 186,
  },
  iconWrap: {
    position: 'absolute',
    right: 0,
    top: '50%',
    transform: 'translateY(-50%)',
    '& i': {
      color: 'var(--whiteMediumEmp)',
    },
  },
  colorCode: {
    border: '2px solid #fff',
    height: 15,
    width: 15,
    position: 'absolute',
    top: '50%',
    right: 0,
    transform: 'translateY(-50%)',
    borderRadius: '50%',
  },
  hours: {
    width: 180,
    padding: '15px 5px',
  },
  sliderTrack: {
    background: '#fff',
  },
  sliderTrackEvent: {
    background: 'var(--darkActive)',
  },
  sizeEvent: {
    width: 40,
    height: 35,
  },
  sizeIcon: {
    color: 'rgba(171, 193, 66, 0.87)',
  },
  sliderBefore: {
    background: 'var(--darkActive)',
  },
  sliderBeforeEvent: {
    background: 'var(--darkActive)',
  },
  thumbStyle: {
    background: 'var(--darkActive)',
  },
  thumbStyleEvent: {
    background: 'var(--darkActive)',
  },
  hourLabel: {
    position: 'relative',
    paddingBottom: 10,
    '& p': {
      fontSize: 14,
      margin: 0,
      position: 'absolute',
      top: '50%',
      right: 10,
      transform: 'translateY(-50%)',
      color: 'var(--darkActive)',
      fontWeight: 600,
    },
    '& .fa': {
      fontSize: '1.1em',
    },
    '& span': {
      // paddingRight: 10
    },
  },
  rootLabel: {
    color: 'rgba(255,255,255, 0.67) !important',
  },
  rootInput: {
    color: 'var(--whiteHighEmp) !important',
  },
  underline: {
    '&:after': {
      borderBottom: '2px solid #34495e !important',
    },
  },
  notchedOutline: {
    borderColor: 'var(--whiteHighEmp) !important ',
  },
  // textField: {
  //     color: 'yellow'
  // }
};
