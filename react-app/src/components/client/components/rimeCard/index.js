import React, { useEffect, useState } from 'react';
import './style.scss';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import clsx from 'clsx';
import moment from 'moment';

// latest assets pins on rime map

import otherIcon from '../rimeMap/assets/img/0-other.png';
import crimeIcon from '../rimeMap/assets/img/1-crime.png';
import infrastructureIcon from '../rimeMap/assets/img/2-infrastructure.png';
import healthIcon from '../rimeMap/assets/img/3-health.png';
import environmentIcon from '../rimeMap/assets/img/4-enviornment.png';
import civilIcon from '../rimeMap/assets/img/5-civil.png';
import cyberIcon from '../rimeMap/assets/img/6-cyber.png';
import terrorismIcon from '../rimeMap/assets/img/7-terrorism.png';
import travelRiskIcon from '../rimeMap/assets/img/8-travel-risks.png';
import naturalDisasterIcon from '../rimeMap/assets/img/9-natural-disaster.png';
import externalThreatsIcon from '../rimeMap/assets/img/10-external-threats.png';
import politicalIcon from '../rimeMap/assets/img/11-political.png';
import regulatoryIcon from '../rimeMap/assets/img/12-Regulatory.png';

// common style
import '../../sass/style.scss';

const pinMapping = {
  0: otherIcon,
  1: crimeIcon,
  2: infrastructureIcon,
  3: healthIcon,
  4: environmentIcon,
  5: civilIcon,
  6: cyberIcon,
  7: terrorismIcon,
  8: travelRiskIcon,
  9: naturalDisasterIcon,
  10: externalThreatsIcon,
  11: politicalIcon,
  12: regulatoryIcon,
};

const pinMappingRgba = {
  1: 'low',
  2: 'medium',
  3: 'high',
};

const TIMEOUT = 120000;

const RimeCard = props => {
  const {
    title,
    categoryId,
    category,
    link,
    isTimeline,
    date,
    hasClose = false,
    handleClose,
    relevancy,
  } = props;

  const [isNew, setIsNew] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setIsNew(false);
    }, TIMEOUT);
  });

  return (
    <a
      href={link}
      target="_blank"
      rel="noopener noreferrer"
      className={clsx(isTimeline && 'timeline--card')}
    >
      <Card className={clsx('rime__card')}>
        <CardContent className="rime__card__content">
          <div className="title__wrap">
            {hasClose && (
              <span onClick={() => handleClose()} className="close--times">
                &times;
              </span>
            )}
            <h3>
              {title.substring(0, 100)}
              {title.length > 99 ? '...' : ''}
            </h3>
            <div className={'pillWrapper'}>
              {isTimeline && (
                <span
                  className={`cat__pill ${relevancy &&
                    pinMappingRgba[relevancy]}`}
                  style={{
                    fontWeight: 400,
                  }}
                >
                  {category}
                </span>
              )}
            </div>

            {!isTimeline && (
              <span
                className={`cat__pill ${relevancy &&
                  pinMappingRgba[relevancy]}`}
                style={{
                  fontWeight: 400,
                }}
              >
                {category}
              </span>
            )}
            {date && (
              <div className="date__footerWrap">
                <label
                  className="date--time"
                  style={{ display: 'block', padding: '5px 0 0' }}
                >
                  {moment(date).format('MMMM Do YYYY, h:mm:ss a')}
                </label>
                {isNew && (
                  <span
                    className={`cat__pill ${relevancy &&
                      pinMappingRgba[relevancy]} flash`}
                    style={{
                      fontWeight: 400,
                      // color: 'rgba(18,18,18,1)',
                    }}
                  >
                    New
                  </span>
                )}
              </div>
            )}
          </div>
        </CardContent>
      </Card>
      {/* {
                isTimeline && <span className="pointer"><img src={pinMapping[categoryId]} alt={category} /></span>
            } */}
    </a>
  );
};

export default RimeCard;
