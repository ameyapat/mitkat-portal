import React, { Component } from 'react';
import './RimeLiveList.scss';
import RimeSearch from '../rimeSearch';
import RimeLiveListItems from '../rimeLiveListItems';
import { withShimmer } from '../../components/hoc/Shimmer';
import RimeEventCard from '../rimeEventCard';
import Empty from '../../../ui/empty';
class RimeLiveList extends Component {
  state = {
    dynamicHeight: 0,
  };

  componentDidMount() {
    const rimeLiveList = document
      .querySelector('.rimeLiveListCont')
      .getBoundingClientRect().top;

    this.setState({
      dynamicHeight: window.innerHeight - rimeLiveList,
    });
  }

  renderEventList = () => {
    const { feed } = this.props;
    return feed.map(item => (
      <RimeEventCard
        title={item.eventTitle}
        description={item.eventDescription}
        sourceLinks={item.eventAllNewsLinks}
        location={item.eventLoc}
        noOfArticles={item.count}
        categoryId={item.eventPrimaryRiskCategory}
        secondaryCategoryId={item.eventSecondaryRiskCategory}
        subRiskCategoryId={item.subRiskCategory}
        link={item.eventLink}
        hasClose={false}
        eventDate={item.eventDate}
        lastUpdatedDate={item.eventLatestUpdate}
        relevancy={item.eventRiskLevel}
        imgURL={item.eventImageUrl}
        tags={item.eventTags}
        language={item.language}
      />
    ));
  };

  renderNoEventList = () => {
    return (
      <RimeEventCard
        key={'no-events'}
        title={'No Events Available, Please refresh after sometime.'}
        category={'No New Events'}
      />
    );
  };

  render() {
    const { feed } = this.props;
    return (
      <div className="rimeLiveListCont" style={{ height: '90vh' }}>
        {feed?.length ? this.renderEventList() : this.renderNoEventList()}
      </div>
    );
  }
}

export default RimeLiveList;
