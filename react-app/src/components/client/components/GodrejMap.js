import React from 'react';
import GodrejMapMarker from './GodrejMapMarker';

const GodrejMap = ({ eventList }) => {
    return(
        <GodrejMapMarker
            googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyCYOTLyn6xbysArlvlW4zpxnH9p6H3An08&v=3.exp&libraries=geometry,drawing,places"
            loadingElement={<div style={{ height: `100%` }} />}
            containerElement={<div style={{ height: `100%`, position: 'relative' }} />}
            mapElement={<div style={{ height: `100%` }} />}
            eventList={eventList}
        />   
    );
} 

export default GodrejMap;