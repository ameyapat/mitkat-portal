import React, { Component } from 'react';
import certificateList from './constant.json';
import certificateImg from '../../../../assets/awards/Mitkat Cert 2021_page-0001.jpg';
import VerticleTimeline from '../../../ui/verticleTimeline';
import TimelineList from '../../../ui/verticleTimeline/timelineList';

class Certificates extends Component {
  render() {
    return (
      <>
        <VerticleTimeline header="Certificates" />
        <ul className="timeline">
          {certificateList.map(item => (
            <TimelineList
              dataDate={item.dataDate}
              title={item.title}
              img={item.img}
              description={item.description}
            />
          ))}
        </ul>
      </>
    );
  }
}

export default Certificates;
