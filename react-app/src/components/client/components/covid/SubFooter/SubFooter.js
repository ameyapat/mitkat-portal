import React from 'react';
import './SubFooter.scss';

const SubFooter = (props) => {
    return(
        <div id="subFooter">
            <p className="subFooter--title">{props.title}</p>
            <p className="subFooter--subtitle">{props.subTitle}</p>
        </div>
    )
}

export default SubFooter;