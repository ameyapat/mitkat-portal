import React from 'react';
import './MapButton.scss';

const MapButton = () => {
    return(
        <div className="fixed-element-1 blinking" id="india_map_btn"> 
            <span className="new-banner"> New </span>
            <a className="map-box" href="https://mitkatrisktracker.com/covid-19/vaccination" target="_blank" rel="noopener noreferrer"> 
                <i class="fas fa-syringe"></i>
                Covid-19 Vaccination 
            </a>
        </div>
    )
}

export default MapButton;