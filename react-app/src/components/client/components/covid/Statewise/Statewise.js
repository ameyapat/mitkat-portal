import React, { useState } from 'react';
import './Statewise.scss';
import clsx from 'clsx';

import { Button, withStyles } from '@material-ui/core';
import { Line, Bar } from 'react-chartjs-2';

import { downloadDistrictReport } from '../../../../admin/helpers/utils';
import { withCookies } from 'react-cookie';
import StatewiseTable from '../StatewiseTable/StatewiseTable';
import CovidTabs from '../CovidTabs/CovidTabs';
import Comments from '../../coronaClient/comments/Comments';

const tabs = [
    {
        id: 0,
        label: "Graph"
    },
    // {
    //     id: 1,
    //     label: "Updates"
    // },
    {
        id: 1,
        label: "Implemented Measures"
    },
]

const styles = () => ({
    metaWrap:{
    },
    graphWrappers:{
        margin: '5px 0',
    }
})

const Statewise = ({
    stateTable, 
    defaultstateid, 
    cookies, 
    handleSort, 
    getStateWiseReport, 
    eventDetails, 
    classes,
    riskGraphObj,
    growthGraphObj,
    deathGraphObj,
}) => {
    const authToken = cookies.get('authToken');
    const [value, setValue] = useState(0);

    function handleTabChange(event, value){
        setValue(value);
    }

    return(
        <div id="covidStatewise">
            <h2 className="statewise-title">State Statistics</h2>
            <div className="covidStatewise">
                <div id="statewiseTableCont">
                    <StatewiseTable 
                        data={stateTable} 
                        defaultstateid={defaultstateid} 
                        handleSort={handleSort}
                        getStateWiseReport={getStateWiseReport}
                    />
                </div>
                <div id="statewiseInfoCont">
                    <CovidTabs 
                        tabTitle={eventDetails.statename} 
                        handleTabChange={handleTabChange}
                        tabs={tabs}
                        centered={true}
                        value={value}
                    >
                        <div hidden={value !== 0}>
                            <div id="metaInfoData" className={classes.metaWrap}>
                                <div className={clsx(classes.graphWrappers, 'graph-wrappers')}>
                                    <Line 
                                        data={riskGraphObj} 
                                        height={190} 
                                        options={{
                                            legend: {
                                                labels: {
                                                    fontColor: "#bdbdbd",
                                                }
                                            },
                                            scales: {
                                            xAxes: [{
                                                gridLines: {
                                                    display: true
                                                },
                                                ticks: {
                                                  fontColor: "#bdbdbd",
                                                }
                                            }],
                                            yAxes: [{
                                                gridLines: {
                                                    display: true
                                                },
                                                ticks: {
                                                  fontColor: "#bdbdbd",
                                                }   
                                            }]
                                            }
                                        }} 
                                    />
                                </div>
                                <div className={clsx(classes.graphWrappers, 'graph-wrappers')}>
                                    <Bar 
                                        data={growthGraphObj} 
                                        height={190} 
                                        options={{
                                            legend: {
                                                labels: {
                                                    fontColor: "#bdbdbd",
                                                }
                                            },
                                            scales: {
                                            xAxes: [{
                                                gridLines: {
                                                    display: true
                                                },
                                                ticks: {
                                                  fontColor: "#bdbdbd",
                                                }
                                            }],
                                            yAxes: [{
                                                gridLines: {
                                                    display: true
                                                },
                                                ticks: {
                                                  fontColor: "#bdbdbd",
                                                }   
                                            }]
                                            }
                                        }} 
                                    />
                                </div>
                                <div className={clsx(classes.graphWrappers, 'graph-wrappers')}>
                                    <Line 
                                        data={deathGraphObj} 
                                        height={190} 
                                        options={{
                                            legend: {
                                                labels: {
                                                    fontColor: "#bdbdbd",
                                                }
                                            },
                                            scales: {
                                            xAxes: [{
                                                gridLines: {
                                                    display: true
                                                },
                                                ticks: {
                                                  fontColor: "#bdbdbd",
                                                }
                                            }],
                                            yAxes: [{
                                                gridLines: {
                                                    display: false
                                                },
                                                ticks: {
                                                  fontColor: "#bdbdbd",
                                                },
                                                id: 'A',
                                                type: 'linear',
                                                position: 'left',   
                                                },{
                                                id: 'B',
                                                type: 'linear',
                                                position: 'right',
                                                ticks: {
                                                    max: 100,
                                                    min: 0
                                                }    
                                                }
                                            ]
                                            }
                                        }}
                                    />
                                </div>
                            </div>
                        </div>
                        {/* <div hidden={value !== 1}>
                            <Comments data={eventDetails.comments || ['NA']} />
                        </div> */}
                        <div hidden={value !== 1}>
                            <div className="implementedMeasures" dangerouslySetInnerHTML={{__html: eventDetails.implementedMeasures}}></div>
                        </div>
                    </CovidTabs>
                </div>
            </div>
            <div className="stateReportBtnCont">
                <Button
                    //size="small"
                    variant='outlined'
                    color='secondary'                    
                    onClick={() => downloadDistrictReport(authToken)}
                    classes={{
                        root: clsx('rootBtnState', 'downloadBtns')
                    }}
                    >
                    <i className="far fa-file-pdf"></i> <span className="c-mg-10">Download State Report</span>              
                </Button>  
            </div>
        </div>
    )

}

export default withCookies(withStyles(styles)(Statewise));