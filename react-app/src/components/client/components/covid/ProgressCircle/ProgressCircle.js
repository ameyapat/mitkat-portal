import React from 'react';
import './ProgressCircle.scss';
// import covid19Icon from '../../../../../assets/icons/covid-19-icon.png';
// style={{ backgroundImage: `url(${covid19Icon})` }}

const renderCircle = (value) => {
    if(value > 0 && value < 26){
        return "prg-25";
    }else if(value > 26 && value < 51){
        return "prg-50";
    }else if(value > 51 && value < 76){
        return "prg-75";
    }else{
        return "prg-100";
    }
}

const ProgressCircle = ({value, classes, color, icon = "fas fa-viruses"}) => {
    return(
        <div className={`progressCircle ${value && renderCircle(value)} ${classes.color}`}>
            <div className={`icon-cont ${classes && classes.rootIcon}`}>
                <span><i className={icon}></i></span>
                <span className="progress-percent" style={{color}}>{value} %</span>
            </div>
        </div>
    )
}

export default ProgressCircle;