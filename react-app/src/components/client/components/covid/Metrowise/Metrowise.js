import React, { useState } from 'react';
import './Metrowise.scss';
import clsx from 'clsx';

import { Button, withStyles } from '@material-ui/core';
import { Line, Bar } from 'react-chartjs-2';

import { downloadMetroReport } from '../../../../admin/helpers/utils';
import { withCookies } from 'react-cookie';
import MetroTable from '../MetroTable/MetroTable';
import CovidTabs from '../CovidTabs/CovidTabs';
import Comments from '../../coronaClient/comments/Comments';

const tabs = [
  {
    id: 0,
    label: 'Graph',
  },
  {
    id: 1,
    label: 'Implemented Measures',
  },
];

const styles = () => ({
  metaWrap: {},
  graphWrappers: {
    margin: '5px 0',
  },
});

const Metrowise = ({
  metroTable,
  cookies,
  handleSort,
  eventDetails,
  getCityWiseReport,
  riskGraphObjCity,
  growthGraphObjCity,
  deathGraphObjCity,
  classes,
}) => {
  const authToken = cookies.get('authToken');
  const [value, setValue] = useState(0);

  function handleTabChange(event, value) {
    setValue(value);
  }

  return (
    <div id="covidMetrowise">
      <h2 className="metrowise-title">Metro City Statistics</h2>
      <div className="covidMetrowise">
        <div id="metroTableCont">
          <MetroTable
            data={metroTable}
            handleSort={handleSort}
            getCityWiseReport={getCityWiseReport}
          />
        </div>
        <div id="metroInfoCont">
          <CovidTabs
            tabTitle={eventDetails.cityname}
            handleTabChange={handleTabChange}
            tabs={tabs}
            centered={true}
            value={value}
          >
            <div hidden={value !== 0}>
              <div id="metaInfoDataCity" className={classes.metaWrap}>
                <div className={clsx(classes.graphWrappers, 'graph-wrappers')}>
                  <Line
                    data={riskGraphObjCity}
                    height={190}
                    options={{
                      legend: {
                        labels: {
                          fontColor: '#bdbdbd',
                        },
                      },
                      scales: {
                        xAxes: [
                          {
                            gridLines: {
                              display: true,
                            },
                            ticks: {
                              fontColor: '#bdbdbd',
                            },
                          },
                        ],
                        yAxes: [
                          {
                            gridLines: {
                              display: true,
                            },
                            ticks: {
                              fontColor: '#bdbdbd',
                            },
                          },
                        ],
                      },
                    }}
                  />
                </div>
                <div className={clsx(classes.graphWrappers, 'graph-wrappers')}>
                  <Bar
                    data={growthGraphObjCity}
                    height={190}
                    options={{
                      legend: {
                        labels: {
                          fontColor: '#bdbdbd',
                        },
                      },
                      scales: {
                        xAxes: [
                          {
                            gridLines: {
                              display: true,
                            },
                            ticks: {
                              fontColor: '#bdbdbd',
                            },
                          },
                        ],
                        yAxes: [
                          {
                            gridLines: {
                              display: true,
                            },
                            ticks: {
                              fontColor: '#bdbdbd',
                            },
                          },
                        ],
                      },
                    }}
                  />
                </div>
                <div className={clsx(classes.graphWrappers, 'graph-wrappers')}>
                  <Line
                    data={deathGraphObjCity}
                    height={190}
                    options={{
                      legend: {
                        labels: {
                          fontColor: '#bdbdbd',
                        },
                      },
                      scales: {
                        xAxes: [
                          {
                            gridLines: {
                              display: true,
                            },
                            ticks: {
                              fontColor: '#bdbdbd',
                            },
                          },
                        ],
                        yAxes: [
                          {
                            gridLines: {
                              display: false,
                            },
                            ticks: {
                              fontColor: '#bdbdbd',
                            },
                            id: 'A',
                            type: 'linear',
                            position: 'left',
                          },
                          {
                            id: 'B',
                            type: 'linear',
                            position: 'right',
                            ticks: {
                              max: 100,
                              min: 0,
                            },
                          },
                        ],
                      },
                    }}
                  />
                </div>
              </div>
            </div>
            {/* <div hidden={value !== 1}>
                            <Comments data={eventDetails.comments || ['NA']} />
                        </div> */}
            <div hidden={value !== 1}>
              <div
                className="implementedMeasures"
                dangerouslySetInnerHTML={{
                  __html: eventDetails.implementedMeasures,
                }}
              ></div>
            </div>
          </CovidTabs>
        </div>
      </div>
      <div className="metroReportBtnCont">
        <Button
          //size="small"
          variant="outlined"
          color="secondary"
          onClick={() => downloadMetroReport(authToken)}
          classes={{
            root: clsx('rootBtnState', 'downloadBtns'),
          }}
        >
          <i className="far fa-file-pdf"></i>{' '}
          <span className="c-mg-10">Download Metro Report</span>
        </Button>
      </div>
    </div>
  );
};

export default withCookies(withStyles(styles)(Metrowise));
