import React from 'react';
import Slider from "react-slick";

import './CovidSlider.scss';

const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 10000,
  };

const CovidSlider = ({ children }) => {
    return(
        <div id="covidSlider">
            {/* <div className="infoPills">
                <span className="info-pills btn-active"><i class="fas fa-check-circle"></i> Active</span>
                <span className="info-pills btn-recovered"><i class="fas fa-check-circle"></i> Recovered</span>
                <span className="info-pills btn-death"><i class="fas fa-check-circle"></i> Death</span>
            </div> */}
            <Slider {...settings}>
                { children }
            </Slider>
        </div>
    )
}

export default CovidSlider;