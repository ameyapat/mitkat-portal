import React from 'react';
import './MetroTable.scss';
import TableListCell from '../../../../ui/tablelist/TableListCell';
import TableListRow from '../../../../ui/tablelist/TableListRow';

import { withStyles, Button } from '@material-ui/core';

import clsx from 'clsx';
import TableList from '../../../../ui/tablelist';

const styles = () => ({
  rootRiskWatch: {
    background: '#F0F0F1',
    padding: 20,
  },
  innerRiskWatch: {
    '& .dmd__wrap': {
      margin: 0,
    },
  },
  rootThead: {
    fontWeight: 600,
    padding: '15px 3px',
    cursor: 'pointer',
    backgroundColor: 'var(--backgroundSolid)',
    '& p': {
      textTransform: 'uppercase',
    },
  },
  tRowWrap: {},
  rootTRowWrap: {
    padding: '0',
    borderBottom: 0,
    background: 'var(--backgroundSolid)',
    borderRadius: 5,
    '& p': {
      color: 'var(--whiteMediumEmp)',
    },
  },
  rowWrap: {
    backgroundColor: 'var(--backgroundSolid) !important',
    padding: '5px 0',
    borderRadius: '3px',
    color: '#f2f2f2',
    '&:nth-child(even)': {
      backgroundColor: '#101b2d',

      '& p': {
        color: 'var(--whiteMediumEmp)',
      },
    },
  },
  closeTimes: {
    position: 'absolute',
    color: '#000',
    fontSize: 30,
    right: 10,
    top: 10,
    cursor: 'pointer',
  },
  rootBtnGraph: {
    border: '1px solid var(--darkActive)',
    backgroundColor: 'transparent',
    color: 'var(--darkActive)',
    fontSize: '13px',
    minWidth: 32,
    '&:hover': {
      backgroundColor: 'var(--darkActive)',
      color: 'var(--whiteMediumEmp)',
    },

    '& i': {
      margin: 0,
    },
  },
  rootTCellNew: {
    justifyContent: 'space-around',
    '& p': {
      color: 'rgb(255, 255, 255) !important',
      fontWeight: 'bold',
      textAlign: 'center',
    },
  },
  metaWrap: {
    margin: '0 10px',
    padding: 10,
    backgroundColor: '#fff',
    borderRadius: 3,
    height: '100vh',
    overflowX: 'hidden',
    overflowY: 'scroll',
  },
});

const MetroTable = props => {
  function renderTheadTitles(value, meta) {
    return (
      <span className="thead--titles--wrapper">
        <p>{value.title}</p>
        <span>
          <i className="fas fa-sort"></i>
        </span>
      </span>
    );
  }

  function handleSort(type, sortBy) {
    props.handleSort(type, sortBy);
  }

  function renderTHeader() {
    const { classes } = props;
    return (
      <div className={classes.tRowWrap}>
        <TableListRow classes={{ root: classes.rootTRowWrap }}>
          <TableListCell
            styles={{
              flex: 2,
              borderRadius: '5px 0 0 0',
            }}
            value={{
              title: 'Cities',
            }}
            type={'city'}
            renderProp={(value, meta) => renderTheadTitles(value, meta)}
            eventHandler={(type, sortBy) => handleSort(type, sortBy)}
            classes={{ root: clsx(classes.rootThead, 'msite-t-head') }}
          />
          <TableListCell
            styles={{ flex: 2 }}
            classes={{ root: clsx(classes.rootThead, 'msite-t-head') }}
            eventHandler={(type, sortBy) => handleSort(type, sortBy)}
            renderProp={(value, meta) => renderTheadTitles(value, meta)}
            value={{
              title: 'Total',
            }}
            meta={{
              color: '#F2042C',
            }}
            type={'confirmed'}
          />
          <TableListCell
            styles={{ flex: 2 }}
            value={{
              title: 'Deaths',
            }}
            classes={{ root: clsx(classes.rootThead, 'msite-t-head') }}
            eventHandler={(type, sortBy) => handleSort(type, sortBy)}
            renderProp={(value, meta) => renderTheadTitles(value, meta)}
            type={'death'}
          />
          <TableListCell
            value={{
              title: 'Recovered',
            }}
            classes={{ root: clsx(classes.rootThead, 'msite-t-head') }}
            eventHandler={(type, sortBy) => handleSort(type, sortBy)}
            renderProp={(value, meta) => renderTheadTitles(value, meta)}
            type={'recovered'}
          />
          <TableListCell
            styles={{
              borderRadius: '0 5px 0 0',
            }}
            value={'#'}
            classes={{
              root: clsx(
                classes.rootThead,
                'msite-t-head',
                'hide-me',
                'flx-xs',
              ),
            }}
          />
        </TableListRow>
      </div>
    );
  }

  function renderTBody() {
    return (
      <TableList id="metroTable" classes={{ root: 'root' }} key="metroTable">
        {renderEventList()}
      </TableList>
    );
  }

  function renderCases(value, meta) {
    return (
      <div className="highlight-wrapper">
        {value.total > 0 ? (
          <>
            <span className="new-cases">
              {value.new > 0 && (
                <>
                  <span className="arrow-up">
                    <i
                      className="fas fa-caret-up"
                      style={{ color: meta.color }}
                    ></i>
                  </span>
                  <span
                    className="new-cases-value"
                    style={{ color: meta.color }}
                  >
                    {value.new}
                  </span>
                </>
              )}
              {
                // value.percent > 0 &&
                // <span className="case-percentage">({value.percent}%)</span>
              }
            </span>
            <span className="t-value">{value.total}</span>
          </>
        ) : (
          <span className="t-value">-</span>
        )}
      </div>
    );
  }

  function renderEventList() {
    const { classes, data } = props;
    return data.map((event, idx) => (
      <TableListRow
        key={idx + '_' + event.statename}
        classes={{ root: classes.rowWrap }}
      >
        <TableListCell
          styles={{ flex: 2 }}
          value={event.cityname}
          classes={{ root: clsx(classes.rootTCellNew, 'msite-t-head') }}
        />
        <TableListCell
          styles={{ flex: 2 }}
          renderProp={(value, meta) => renderCases(value, meta)}
          value={{
            new: event.newcases,
            total: event.totalinfected,
            percent: event.growthratecases,
          }}
          meta={{
            color: '#F2042C',
          }}
          classes={{ root: clsx(classes.rootTCellNew, 'msite-t-head') }}
        />
        <TableListCell
          styles={{ flex: 2 }}
          renderProp={(value, meta) => renderCases(value, meta)}
          value={{
            new: event.newdeaths,
            total: event.totaldeaths,
            percent: event.growthratedeaths,
          }}
          meta={{
            color: '#F2042C',
          }}
          classes={{ root: clsx(classes.rootTCellNew, 'msite-t-head') }}
        />
        <TableListCell
          value={
            event.totalrecoverd !== 0 ? (
              `${event.totalrecoverd || 0} (${event.recoverypercent || 0}%)`
            ) : (
              <span className="t-value">-</span>
            )
          }
          classes={{ root: clsx(classes.rootTCellNew, 'msite-t-head') }}
        />
        <TableListCell classes={{ root: 'msite-t-head' }}>
          <Button
            variant="outlined"
            color="primary"
            onClick={() => props.getCityWiseReport(event.cityid)}
            classes={{
              root: clsx(classes.rootBtnGraph, 'cta-g'),
            }}
          >
            <span className="btnViewComments">
              <i className="fas fa-chart-bar"></i>
            </span>
          </Button>
        </TableListCell>
      </TableListRow>
    ));
  }

  return (
    <>
      {renderTHeader()}
      <div className="metro-t-wrapper">{renderTBody()}</div>
    </>
  );
};

export default withStyles(styles)(MetroTable);
