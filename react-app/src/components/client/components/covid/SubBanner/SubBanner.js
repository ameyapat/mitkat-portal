import React from 'react';
import './SubBanner.scss';
import { Button } from '@material-ui/core';
import clsx from 'clsx';

import { downloadExcel, downloadSummary } from '../../../../admin/helpers/utils';
import { withCookies } from 'react-cookie';
import Metrics from '../Metrics/Metrics';
import ProgressCircle from '../ProgressCircle/ProgressCircle';

const SubBanner = (props) => {
    const authToken = props.cookies.get('authToken');
    const { 
        totalactive, 
        newactive, 
        growthrateactive, 
        activepercent, 
        growthratedeaths, 
        totaldeaths, 
        newdeaths, 
        totalrecoverd, 
        newrecovered,
        growthraterecovered,
        deathpercent,
        recoverypercent,
    } = props.coronaDashboard;

    return(
        <div id="covidSubBanner">
            <div id="metricsCont">
                <Metrics 
                    styles={{color: '#00b1ea'}} 
                    title={"Active Cases"} 
                    total={totalactive} 
                    current={newactive} 
                    currentPer={growthrateactive}
                >
                    <ProgressCircle 
                        classes={{color: 'blue'}} 
                        value={activepercent} 
                        color={"rgba(0, 177, 234, 1)"} 
                        icon="fas fa-viruses"
                    />
                </Metrics>
                <Metrics 
                    styles={{color: '#e01059'}} 
                    title={"Total Deaths"} 
                    total={totaldeaths} 
                    current={newdeaths} 
                    currentPer={growthratedeaths}
                >
                    <ProgressCircle 
                        classes={{color: 'red'}} 
                        value={deathpercent} 
                        color={"rgba(224, 16, 90, 1)"} 
                        icon="fas fa-biohazard"
                    />
                </Metrics>
                <Metrics 
                    styles={{color: '#63bd23'}} 
                    title={"Total Recovered"} 
                    total={totalrecoverd} 
                    current={newrecovered} 
                    currentPer={growthraterecovered}
                >
                    <ProgressCircle 
                        classes={{color: 'green'}} 
                        value={recoverypercent} 
                        color="rgba(99, 189, 35, 1)"
                        icon="fas fa-shield-virus"
                    />
                </Metrics>
            </div>
            <div id="reportsBtnCont">
                <div className="reports-box">
                    <Button
                        //size="small"
                        variant='outlined'
                        color='secondary'
                        onClick={() => downloadSummary(authToken)}
                        classes={{
                            root: clsx('rootBtnSummary', 'downloadBtns')
                        }}
                        >
                        <i className="far fa-file-pdf"></i> <span className="c-mg-10">Download Summary Report</span>             
                    </Button> 
                </div>
                {/* <div className="reports-box">
                    <Button
                        size="small"
                        variant='outlined'
                        color='secondary'                    
                        onClick={() => downloadExcel(authToken)}
                        classes={{
                            root: clsx('rootBtnExcel', 'downloadBtns')
                        }}
                        >
                        <i className="far fa-file-excel"></i> <span className="c-mg-10">Download Excel Report</span>              
                    </Button>  
                </div> */}
            </div>
        </div>
    )
}

export default withCookies(SubBanner);


                    