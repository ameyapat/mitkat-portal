import React from 'react';
import './Disclaimer.scss';

const Disclaimer = (props) => {
    return(
        <div id="covidDisclaimer">
            <p className="disclaimer--title">{props.title}</p>
            <p className="disclaimer--subtitle">{props.subTitle}</p>
        </div>
    )
}

export default Disclaimer;