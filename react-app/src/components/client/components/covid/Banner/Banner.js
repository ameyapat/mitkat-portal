import React from 'react';
import './Banner.scss';

import IndiaIcon from './assets/india.png'

const Banner = (props) => {
    return(
        <div id="covidBanner">
            <div className="bannerTitleCont">
                <h1>
                    <span>
                        COVID-19 TRACKER INDIA  
                    </span>
                    <img src={IndiaIcon} alt="IndiaIcon" /> 
                </h1> 
                <p>{props.coronaDashboard.subjectTime}</p>
            </div>
            <div className="totalCasesCont"> 
                <h1 className="total-cases-title">Total Cases</h1> 
                <p className="total-cases-counter">{props.coronaDashboard.totalinfected}</p>
				<p className="total-cases-per">
                    <i className="fas fa-long-arrow-alt-up"></i> 
                    {props.coronaDashboard.newcases} ({Math.floor(props.coronaDashboard.growthratecases)} %)</p>  
			</div> 
            <div className="ratingCont">
                <div className="rating-inner-cont">
                    <h2 className="text-white">Tested</h2>
                    <p>{props.coronaDashboard.tested} <span>({Math.floor(props.coronaDashboard.testedpercent)}%)</span></p>
                </div>
                <div className="rating-inner-cont">
                    <h2 className="text-white">Doubling Rate</h2>
                    <p>{props.coronaDashboard.doublingrate} Days</p>
                </div>
            </div>
        </div>
    )
}

export default Banner;