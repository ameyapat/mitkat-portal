import React from 'react';
import './Header.scss';

import mitkatLogo from '../../../../../assets/MitKat_new_logo.png';
import { withCookies } from 'react-cookie';
import { withRouter } from 'react-router-dom';

const Header = (props) => {
    return(
        <nav id="headerCovid" className="navbar navbar-expand-lg navbar-light">
            <div>
                <img 
                    src={mitkatLogo}
                    alt="mitkat-logo-white"
                />
            </div>
            {/* <div>
                {
                    props.cookies.get('authUserId') 
                    ?
                    <button onClick={() => props.handleLogout()}> 
                        <i className="fas fa-sign-in-alt"></i>  Logout
                    </button>
                    :
                    <button onClick={() => props.history.push('/')}>
                        <i className="fas fa-sign-in-alt"></i>  Login
                    </button>
                }
            </div> */}
	    </nav> 
    )
}

export default withRouter(withCookies(Header));