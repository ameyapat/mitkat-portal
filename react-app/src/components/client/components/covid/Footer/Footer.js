import React from 'react';
import './Footer.scss';
import SubFooter from '../SubFooter/SubFooter';
import './assets/corona-bg-green.svg';
import moment from 'moment';

const title = `DISCLAIMER`;
const subTitle = `MitKat dashboard is updated as and when official announcements are made by State and Union Health Ministries, therefore total number of cases will always reflect a more updated figure than other official sources. The total count includes confirmed, recovered and fatal cases.`

const Footer = () => {
    return(
    <footer id="covidFooter"> 
		<SubFooter title={title} subTitle={subTitle} />
		<div className="footerWrapper">
			{/* <div className="iconsWrapper">
				<span className="social--icons"><i className="fab fa-facebook-square"></i></span> 
				<span className="social--icons"><i className="fab fa-google-plus-square"></i></span>
				<span className="social--icons"><i className="fab fa-linkedin"></i></span>
			</div> */}
			<div className="copyrightWrapper">
				<span>© All Right Reserved</span>
				<span className="copyright--year">{ moment(new Date()).format('YYYY') }</span>
			</div>
		</div>
	</footer>
)}

export default Footer;