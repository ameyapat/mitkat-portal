import React from 'react';
import './Metrics.scss';

const Metrics = (props) => {
    return(
        <div className="metrics-box">
            <div className="metrics-box-inner" style={{...props.styles}}>
                <h3 className="metrics--title">{props.title}</h3>
                <p className="metrics--total">{props.total}</p>
                <p className="metrics--per"><i className="fas fa-long-arrow-alt-up"></i> {props.current} ({props.currentPer} %)</p>
            </div>
            <div className="metrics-box-inner">
                { props.children }
            </div>
        </div>
    )
}

export default Metrics;