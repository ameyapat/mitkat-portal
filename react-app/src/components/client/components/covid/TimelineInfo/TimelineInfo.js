import React from 'react';
import './TimelineInfo.scss';
import clsx from 'clsx';

const TimelineInfo = (props) => {
    return(
        <div className={clsx("timeline--item", props.classes && props.classes.root )}>
            <span className="highlight"></span>
            <div className={clsx("timeline--info", props.classes && props.classes.root )}> { /* content */ }
                <span className="timeline--link--title">{props.title}</span>
                <span><i className="fas fa-external-link-alt small"></i></span>                    
            </div>
        </div>
    )
}

export default TimelineInfo;