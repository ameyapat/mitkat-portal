import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { withStyles } from '@material-ui/core';

import './CovidTabs.scss';

const styles = () => ({
  root: {},
  indicator: {
    background: 'var(--backgroundSolid)',
  },
  rootTab: {
    color: '#bbb',
  },
  selectedTab: {
    color: 'var(--whiteMediumEmp)',
    background: '#202c40',
    fontWeight: 'bold',
  },
});

class CovidTabs extends Component {
  a11yProps = index => {
    return {
      id: `tab-${index}`,
    };
  };

  render() {
    const { tabs, centered, children, value, classes } = this.props;
    return (
      <div className="tab-panel-cont">
        <div className="tab-header">
          <h2>{this.props.tabTitle}</h2>
        </div>
        <div className="tab-body">
          <Tabs
            onChange={this.props.handleTabChange}
            centered={centered}
            value={value}
            classes={{
              root: classes.root,
              indicator: classes.indicator,
            }}
          >
            {tabs.map(tab => (
              <Tab
                key={tab.id}
                classes={{
                  root: classes.rootTab,
                  selected: classes.selectedTab,
                }}
                label={tab.label}
                {...this.a11yProps(tab.id)}
              />
            ))}
          </Tabs>
          <div className="tab-body-cont">{children}</div>
        </div>
      </div>
    );
  }
}

CovidTabs.propTypes = {
  tabTitle: PropTypes.any.isRequired,
};

export default withStyles(styles)(CovidTabs);
