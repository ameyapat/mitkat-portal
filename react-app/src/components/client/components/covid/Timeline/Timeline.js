import React from 'react';
import './Timeline.scss';

import TimelineInfo from '../TimelineInfo/TimelineInfo';

import clsx from 'clsx';

const Timeline = (props) => {

	function renderList(){
		return props.advisories.map((a, idx) => (
		<a 
		key={a.id}
		className={clsx("timeline--list",idx % 2 === 0 ? "left" : "right")}
		href={a.advisoryurl} 
		target="_blank" 
		rel="noopener noreferrer"
		>
			<TimelineInfo classes={{root: idx % 2 === 0 ? "left" : "right"}} title={a.title} />
		</a>)
		)
	}

    return(
        <div id="covidTimeline">
            <p className="timeline--title">Advisories (Covid-19)</p>
			<div className="timeline--wrapper">
				<div id="timelineAdvisory">
					{ renderList() }
				</div>
			</div>
        </div>
    )
}

export default Timeline;