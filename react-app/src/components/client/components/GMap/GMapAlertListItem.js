import React, { Component } from 'react'
import style from './style'
import injectSheet from 'react-jss'

@injectSheet(style)
class GMapAlertListItem extends Component {
  
  render() {

    let { classes, singleAlertItem } = this.props;

    return (
      <div className={classes.rootGMapListItem}>  
      {
        singleAlertItem.header &&
        <div className="item-header">
          <h1>{singleAlertItem.header ? singleAlertItem.header : ""}</h1>
          {/* <small className="item-date">{singleAlertItem.sentDate}</small> */}
        </div>
      }    
      {
        singleAlertItem.subHeader &&
        <div className="item-sub-header">
          <label>SubHeader</label>
          <h2>{singleAlertItem.subHeader ? singleAlertItem.subHeader : ""}</h2>          
        </div>
      } 
      {
        singleAlertItem.details &&
        <div className="item-details">
          <label>Details</label>
          <p>{singleAlertItem.details ? singleAlertItem.details : ""}</p>          
        </div>
      } 
      {
        singleAlertItem.analysis &&
        <div className="item-details">
          <label>Analysis</label>
          <p>{singleAlertItem.analysis ? singleAlertItem.analysis : ""}</p>          
        </div>
      }
      {
        singleAlertItem.impactAnalysis &&
        <div className="item-details">
          <label>Impact Analysis</label>
          <p>{singleAlertItem.impactAnalysis ? singleAlertItem.impactAnalysis : ""}</p>          
        </div>
      }
      {
        singleAlertItem.recommendations &&
        <div className="item-details">
          <label>Recommendations</label>
          <p>{singleAlertItem.recommendations ? singleAlertItem.recommendations : ""}</p>          
        </div>
      }
      </div>
    )
  }
}

export default GMapAlertListItem;
