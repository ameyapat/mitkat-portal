import React, { Component } from 'react'
import injectSheet from 'react-jss'
import style from './style'


const AlertItem = (props) => {
  return(
    <div className="alert-item" onClick={() => props.handleFetchSingleAlert(props.alertId)}>
      <div className="alert-item-date"><small>{props.time}</small></div>
      <div className="alert-item-title"><h2>{props.header}</h2></div>
      <div className="alert-item-desc"><p>{props.subHeader}</p></div>
    </div>
  ) 
}

@injectSheet(style)
class GMapAlertList extends Component {

  render() {

    let { classes, allAlerts, handleFetchSingleAlert } = this.props;
    let allAlertsXML = [];

    if(allAlerts.length > 0){
      allAlertsXML = allAlerts.map(item => {
        return <AlertItem 
          key={item.id}
          alertId={item.id}
          header={item.header}
          subHeader={item.subHeader}
          details={item.details}
          time={item.sentDate}
          handleFetchSingleAlert={handleFetchSingleAlert}
        />
      })
    }

    return (
      <div className={classes.rootGMapAlertList}>
        {
          allAlertsXML && allAlertsXML.length > 0
          ?
          allAlertsXML
          :
          <div style={{padding: 10}}>No latest alerts</div>
        }
      </div>
    )
  }
}

export default GMapAlertList;
