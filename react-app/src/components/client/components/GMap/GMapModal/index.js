import React, { Component, Fragment } from 'react';
//components
import BulletList from '../../../../ui/bulletList';
//helpers
import {
  RISK_CATEGORY_NAME,
  RISK_LEVEL_COLORS_NEON,
  RISK_LEVEL_NAME,
} from '../../../../../helpers/constants';
import Moment from 'react-moment';
import { API_ROUTES } from '../../../../../helpers/http/apiRoutes';
import { fetchApi } from '../../../../../helpers/http/fetch';
import mitkatLogoBlue from '../../../../../assets/svg/MitKat-Logo-Blue.svg';
import blackPearlLogo from '../../../../../assets/partners/BlackPearlLogo.png';
import sanctuaryRiskLogo from '../../../../../assets/partners/SanctuaryRisk.png';
//
import clsx from 'clsx';
import styles from './style';
import injectSheet from 'react-jss';

import { withCookies } from 'react-cookie';
import { NavLink, useRouteMatch } from 'react-router-dom';
import store from '../../../../../store';
// import { setImpactRadius } from '../../../actions/EventModalAction';
import { setImpactRadius } from '../../../../../actions/EventModalAction';
import { Divider } from '@material-ui/core';
import './style.scss';
import CustomModal from '../../../../ui/modal';
import EmployeeTracking from '../../employeeTracking';
import { connect } from 'react-redux';
import { localDateTime } from '../../../../../helpers/utils';

@withCookies
@injectSheet(styles)
@connect(state => {
  return {
    themeChange: state.themeChange,
    eventModal: state.eventModal,
    partnerDetails: state.partnerDetails.partnerDetails,
  };
})
class GMapModal extends Component {
  state = {
    showEventDetails: true,
    showUpdates: false,
    showRelatedEvents: false,
    relatedEventList: [],
    showAffectedEntities: false,
  };

  componentDidMount() {
    this.handleToggleTabs(true, this.state.showEventDetails);
    this.props.eventModal.eventId && this.getRelatedEvents();
    store.dispatch(
      setImpactRadius(this.props.eventDetail?.impactRadius || 1500),
    );
  }

  getUpdateList = () => {
    let {
      eventDetail: { updates },
    } = this.props;
    let updatesXML = [];
    if (updates) {
      updatesXML = updates.map(item => {
        return (
          <li key={item.key} className="update__item">
            <h2 className="update--title">{item.title}</h2>
            <p className="update--desc">{item.description}</p>
            <p className="update--time">{item.timeupdate}</p>
          </li>
        );
      });
    }

    return updatesXML;
  };

  downloadPdf = () => {
    let {
      eventDetail: { id },
      cookies,
    } = this.props;
    let authToken = cookies.get('authToken');

    let reqObj = {
      url: `${API_ROUTES.downloadEventPdf}/${id}`,
      isAuth: true,
      authToken,
      isBlob: true,
      method: 'POST',
      showToggle: true,
    };

    fetchApi(reqObj).then(blobObj => {
      var a = document.createElement('a');
      document.body.appendChild(a);
      let objUrl = new Blob([blobObj], { type: 'application/pdf' });
      let url = window.URL.createObjectURL(objUrl);
      a.href = url;
      a.download = 'mitkat-report';
      a.click();
      window.URL.revokeObjectURL(url);
    });
  };

  getRelatedEvents = () => {
    let {
      eventDetail: { id },
      cookies,
      eventModal: { eventId },
    } = this.props;
    let authToken = cookies.get('authToken');

    let reqObj = {
      url: `${API_ROUTES.getRelatedEvents}/${eventId}`,
      isAuth: true,
      authToken,
      method: 'GET',
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        this.setState({ relatedEventList: data });
      })
      .catch(e => console.log(e));
  };

  handleCloseModal = () => {
    this.props.closeModal();
  };

  generateBulletPoints = (type, list) => {
    return <BulletList list={list} type={type} />;
  };

  handleToggleTabs = (tabvalue, tabtype) => {
    this.setState({ [tabtype]: tabvalue }, () => {
      if (tabtype === 'showEventDetails') {
        this.setState({ showUpdates: false, showRelatedEvents: false });
      } else if (tabtype === 'showUpdates') {
        this.setState({ showEventDetails: false, showRelatedEvents: false });
      } else if (tabtype === 'showRelatedEvents') {
        this.setState({ showEventDetails: false, showUpdates: false });
      }
    });
  };

  showEvents = eventId => {
    this.props.showDetails(eventId);
    this.setState({
      showUpdates: false,
      showRelatedEvents: false,
      showEventDetails: true,
    });
  };

  render() {
    let {
      eventDetail,
      risklevel,
      riskcategory,
      showClose = true,
      classes,
      hasDarkTheme = false,
    } = this.props;
    let { partnerName } = this.props.partnerDetails;

    const {
      showEventDetails,
      showUpdates,
      showRelatedEvents,
      relatedEventList,
      showAffectedEntities,
    } = this.state;
    let eventId = '';
    if (eventDetail && eventDetail.id) eventId = eventDetail.id;
    const {
      themeChange: { setDarkTheme },
    } = this.props;

    return (
      <Fragment>
        {showClose && (
          <div className="new__modal__header">
            <div className="modal--header-layer1">
              <h2 className="modal--heading">Risk Alert</h2>
            </div>
            <div className="modal--header-layer2"></div>
            <div className="modal--header-layer3"></div>
            <div className="modal--header-layer4">
              <div className={classes.logoWrapper}>
                <img
                  src={
                    partnerName === 'BlackPearl'
                      ? blackPearlLogo
                      : partnerName === 'Sanctuary Risk'
                      ? sanctuaryRiskLogo
                      : mitkatLogoBlue
                  }
                  alt="mitkat-logo"
                  className={classes.mitkatLogo}
                />
              </div>
              <span className="btn-times" onClick={this.handleCloseModal}>
                &times;
              </span>
            </div>
          </div>
        )}
        <div
          className={`new--modal--body ${hasDarkTheme ? 'dark--body' : null}`}
        >
          <div className="new__event__detail">
            <div className="eventTabs">
              <div className="eventTab">
                <button
                  className={`eventTab__btn ${showEventDetails &&
                    'tabIsActive'}`}
                  onClick={() =>
                    this.handleToggleTabs(true, 'showEventDetails')
                  }
                >
                  Event Details
                </button>
              </div>
              <div className="eventTab">
                <button
                  className={`eventTab__btn ${showUpdates && 'tabIsActive'}`}
                  onClick={() => this.handleToggleTabs(true, 'showUpdates')}
                >
                  Current Updates
                </button>
              </div>
              <div className="eventTab">
                <button
                  className={`eventTab__btn ${showRelatedEvents &&
                    'tabIsActive'}`}
                  onClick={() =>
                    this.handleToggleTabs(true, 'showRelatedEvents')
                  }
                >
                  Related Events
                </button>
              </div>
            </div>
            <div className="title_wrap">
              <h2>{eventDetail?.title || ''}</h2>
            </div>
            <div className="event--detail">
              <div className="event">
                {eventDetail?.reportingTimeInstant && (
                  <>
                    <div className="date_wrapper">
                      <span className="icon-wrap">
                        <i class="fas fa-calendar-alt"></i>
                      </span>
                      <div className="date">
                        <Moment format="Do MMM YYYY, h:mm:ss a">
                          {localDateTime(eventDetail?.reportingTimeInstant)}
                        </Moment>
                      </div>
                    </div>
                  </>
                )}
              </div>
            </div>
            <div className="eventDetails_row_1">
              <div className="eventDetails_wrap">
                <div className="eventDetails_heading">Locations Affected</div>
                {eventDetail?.locationName && (
                  <div className="eventDetails_content">
                    {eventDetail?.locationName}
                  </div>
                )}
              </div>
              <div className="tracking_id_wrap">
                <div className="tracking_id_heading">Tracking Id</div>
                {eventDetail?.trackingId && (
                  <div className="tracking_id_content">
                    {eventDetail?.trackingId}
                  </div>
                )}
              </div>
            </div>
            <div className="eventDetails_row_2">
              <div className="riskLevel_wrap">
                <div className="riskLevel_heading">Risk Level</div>
                <div
                  className="riskLevel_content"
                  style={{
                    backgroundColor: `${RISK_LEVEL_COLORS_NEON[risklevel]}`,
                    border: 'none',
                    // color: 'rgba(18,18,18,1)',
                  }}
                >
                  {RISK_LEVEL_NAME[risklevel]}
                </div>
              </div>
              <div className="riskcategory_wrap">
                <div className="riskcategory_heading">Risk Category</div>
                <div className="riskcategory_content">
                  {RISK_CATEGORY_NAME[riskcategory]}-{' '}
                  {eventDetail?.riskSubCategoryName}
                </div>
              </div>
            </div>
            {showEventDetails && (
              <>
                <Divider className="divider-pd-15" />
                {/** description */}
                {(eventDetail?.description ||
                  eventDetail?.description2 ||
                  eventDetail?.descriptionbullets?.length > 0) && (
                  <div className="event--detail">
                    <div className="event">
                      <label>Description</label>
                    </div>
                  </div>
                )}
                {eventDetail?.description && (
                  <div className="event--detail">
                    <div className="event">
                      <p>{eventDetail?.description}</p>
                    </div>
                  </div>
                )}
                {eventDetail?.description2 && (
                  <div className="event--detail">
                    <div className="event">
                      <p>{eventDetail?.description2}</p>
                    </div>
                  </div>
                )}
                {eventDetail?.descriptionbullets?.length > 0 && (
                  <div className="event--detail">
                    {eventDetail?.descriptionbullets &&
                      this.generateBulletPoints(
                        'description',
                        eventDetail.descriptionbullets,
                      )}
                  </div>
                )}
                {/** background */}
                {(eventDetail?.background ||
                  eventDetail?.background2 ||
                  eventDetail?.backgroundbullets?.length > 0) && (
                  <div className="event--detail">
                    <div className="event">
                      <label>Background</label>
                    </div>
                  </div>
                )}
                {eventDetail?.background && (
                  <div className="event--detail">
                    <div className="event">
                      <p>{eventDetail?.background}</p>
                    </div>
                  </div>
                )}
                {eventDetail?.background2 && (
                  <div className="event--detail">
                    <div className="event">
                      <p>{eventDetail?.background2}</p>
                    </div>
                  </div>
                )}
                {eventDetail?.backgroundbullets?.length > 0 && (
                  <div className="event--detail">
                    {this.generateBulletPoints(
                      'background',
                      eventDetail?.backgroundbullets,
                    )}
                  </div>
                )}
                {/** background ends */}
                {/** impact begins */}
                {(eventDetail?.impact ||
                  eventDetail?.impact2 ||
                  eventDetail?.impactbullets?.length > 0) && (
                  <div className="event--detail">
                    <div className="event">
                      <label>Impact</label>
                    </div>
                  </div>
                )}
                {eventDetail?.impact && (
                  <div className="event--detail">
                    <div className="event">
                      <p>{eventDetail?.impact}</p>
                    </div>
                  </div>
                )}

                {eventDetail?.impact2 && (
                  <div className="event--detail">
                    <div className="event">
                      <p>{eventDetail?.impact2}</p>
                    </div>
                  </div>
                )}
                {eventDetail?.impactbullets?.length > 0 && (
                  <div className="event--detail">
                    {this.generateBulletPoints(
                      'impact',
                      eventDetail.impactbullets,
                    )}
                  </div>
                )}
                {/** impact ends */}
                {/** Recommendation begins */}
                {(eventDetail?.recommendation ||
                  eventDetail?.recommendation2 ||
                  eventDetail?.recommendationbullets?.length > 0) && (
                  <div className="event--detail">
                    <div className="event">
                      <label>Recommendation</label>
                    </div>
                  </div>
                )}
                {eventDetail?.recommendation && (
                  <div className="event--detail">
                    <div className="event">
                      <p>{eventDetail?.recommendation}</p>
                    </div>
                  </div>
                )}
                {eventDetail?.recommendation2 && (
                  <div className="event--detail">
                    <div className="event">
                      <p>{eventDetail?.recommendation2}</p>
                    </div>
                  </div>
                )}
                {eventDetail?.recommendationbullets?.length > 0 && (
                  <div className="event--detail">
                    {this.generateBulletPoints(
                      'recommendation',
                      eventDetail?.recommendationbullets,
                    )}
                  </div>
                )}
                {/** recommendation ends */}
                {/** Web portal image */}
                {eventDetail?.imageURLwebportal && (
                  <div className="event--detail">
                    <div className="event">
                      <label>Image</label>

                      {eventDetail?.imageURLwebportal && (
                        <img
                          src={eventDetail?.imageURLwebportal}
                          alt="webportal"
                          style={{
                            width: '700px',
                            maxWidth: '700px',
                            marginTop: '15px',
                          }}
                        />
                      )}
                    </div>
                  </div>
                )}
                {eventDetail?.sourceList?.length > 0 && (
                  <div className="event--detail">
                    <div className="event">
                      <label>Sources</label>
                    </div>
                  </div>
                )}
                {eventDetail?.sourceList?.length > 0 && (
                  <div className="event--detail">
                    {this.generateBulletPoints(
                      'sourceList',
                      eventDetail?.sourceList,
                    )}
                  </div>
                )}
              </>
            )}

            {showUpdates && (
              <>
                <Divider className="divider-pd-15" />
                {eventDetail?.updates.map(update => (
                  <div className="liveUpdatesWrap">
                    <div className="liveUpdates--title">
                      {update?.title || ''}
                    </div>
                    <div className="liveUpdates--timeupdate">
                      {update?.timeupdate || ''}
                    </div>
                  </div>
                ))}
              </>
            )}

            {showRelatedEvents && (
              <>
                <Divider className="divider-pd-15" />
                {relatedEventList && (
                  <>
                    <ul className="related--list">
                      {relatedEventList.map(item => (
                        <li onClick={() => this.showEvents(item.eventId)}>
                          <h3>Title: {item.title} </h3>
                          <h4>Description: {item.update}</h4>
                          <p> {item.timestamp}</p>
                        </li>
                      ))}
                    </ul>
                  </>
                )}
              </>
            )}
          </div>
        </div>
        <div
          className={
            partnerName !== 'BlackPearl'
              ? 'new--modal--footer'
              : 'new--modal--footer--blackpearl'
          }
        >
          <div className="pdf__btn">
            <button onClick={this.downloadPdf} className="pdf--btn">
              Download Pdf
            </button>
          </div>

          {/* <NavLink
            to={`/client/dashboard/${eventId}`}
            target="_blank"
            className="entity--btn"
          >
            Affected Entities
          </NavLink> */}
          <div className="pdf__btn">
            <button
              onClick={() => this.setState({ showAffectedEntities: true })}
              className="pdf--btn"
            >
              Affected Entities
            </button>
          </div>

          {false && (
            <div>
              <button
                onClick={() => this.props.trackLiveEvents(eventId)}
                className={clsx('pdf--btn', classes.btnLiveTracker)}
              >
                <i className="fas fa-sync"></i> Track Live Updates
              </button>
            </div>
          )}
        </div>

        <CustomModal
          showModal={showAffectedEntities}
          closeModal={() => this.setState({ showAffectedEntities: false })}
        >
          <div
            className={`affectedEntities__inner ${
              setDarkTheme ? 'dark' : 'light'
            }`}
          >
            <div className="affectedEntities__header">
              <span className="affectedEntities--heading">
                Affected Entities
              </span>
              <span
                className="affectedEntities--close"
                onClick={() => this.setState({ showAffectedEntities: false })}
              >
                &times;
              </span>
            </div>
            <div className="affectedEntities__body">
              <EmployeeTracking eventId={eventId} eventDetail={eventDetail} />
            </div>
          </div>
        </CustomModal>
      </Fragment>
    );
  }
}
export default GMapModal;
