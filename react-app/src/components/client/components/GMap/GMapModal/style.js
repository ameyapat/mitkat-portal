export default {
  rootPill: {
    height: 'auto',
    margin: '0 12px !important',
    // padding: '5px !important',
    lineHeight: 0,
    display: 'inline-flex !important',
    outline: 'none !important',
    borderRadius: '3px !important',
    border: 'none !important',

    '& span': {
      fontSize: '12px !important',
    },

    '& i': {
      fontSize: '16px !important',
    },
  },
  btnLiveTracker: {
    background: 'transparent',
    border: '1px solid #589167',
    color: '#589167',
    '& i': {
      fontSize: 11,
    },
  },
  modalInner: {
    // position: 'relative',
  },
  closeTimes: {
    position: 'absolute',
    color: '#000',
    fontSize: 30,
    right: 10,
    top: 10,
    cursor: 'pointer',
  },
  logoWrapper: {
    display: 'flex',
    alignItems: 'center',
    paddingTop: '5px',
    background: '#f2f2f2',
  },
  mitkatLogo: {
    width: '125px',
    maxWidth: '100%',
    paddingLeft: '15px',
    marginRight: '15px',
  },
};
