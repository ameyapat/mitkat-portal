import React, { Component } from 'react'
import GMapAlertList from './GMapAlertList'
import GMapAlertListItem from './GMapAlertListItem'
import style from './style'
import injectSheet from 'react-jss'
import Title from '../ui/Title';
import { fetchGetData } from '../../api/fetchApi'
import apiRoutes from '../../api/routesApi';

@injectSheet(style)
class GMapAlertComponent extends Component {

  state = {
    allAlerts: [],
    singleAlertItem: {}
  }

  componentDidMount(){

    let { regionId } = this.props;
    this.fetchAlertMessages(regionId);

  }

  fetchAlertMessages = (regionid) => {
    
    fetchGetData(apiRoutes.regionalAlertMessage + '/' + regionid, 'GET').then(
      response => {
        let allAlerts = response ? response : [];

        this.setState({ allAlerts }, () => {
          if (this.state.allAlerts.length != 0) {
            if (this.state.allAlerts) {
              let alertid = allAlerts[0].id;
              this.handleFetchSingleAlert(alertid);
            }
          }
        });
      },
    );
  }

  handleFetchSingleAlert = (alertid) => {
    
    fetchGetData(apiRoutes.alertDetails + "/" + alertid, "GET")
    .then(response => {
      if(response.message){
        let singleAlertItem = response.message;
        this.setState({singleAlertItem})
      }
    })
  }

  handleCloseAlert = () => {
    this.props.closeMapAlerts();
  }

  render() {

    let { classes } = this.props;
    let { allAlerts, singleAlertItem } = this.state;

    return (
      <div>
        {/* <Title 
          title={"Map Alerts"} 
          fontType={"fa-map-o"} 
          hasClose={true} 
          handleCloseAlert={this.handleCloseAlert} 
        /> */}
        <div className={classes.rootGMapAlertCompRoot}>
          <div className="alertListWrap">
              <GMapAlertList
                allAlerts={allAlerts}
                handleFetchSingleAlert={this.handleFetchSingleAlert}
              />
          </div>
          <div className="alertListItemWrap">
            {
              singleAlertItem && 
               <GMapAlertListItem
                singleAlertItem={singleAlertItem}
              />
            }
          </div>
        </div>
      </div>
    )
  }
}

export default GMapAlertComponent;
