import React, { Component } from 'react';
import { withScriptjs, withGoogleMap, GoogleMap } from 'react-google-maps';
import CustomMarker from './CustomMarker';
import { connect } from 'react-redux';
import { withCookies } from 'react-cookie';
//helpers
import { fetchApi } from '../../../../helpers/http/fetch';
import { fetchRiskTracker } from '../../../../actions/riskTrackerAction';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';

import veryLow from '../../../../assets/solid-pins/verylow-s.png'; // #99D100 - very low march
import low from '../../../../assets/solid-pins/low-s.png'; // - low
import medium from '../../../../assets/solid-pins/medium-s.png'; // #FF7F00 - medium march
import high from '../../../../assets/solid-pins/high-s.png'; //  - high
import veryHigh from '../../../../assets/solid-pins/veryhigh-s.png'; //  - very high
import customMapStyles from '../../../../helpers/universalMapStyles.json';
import customMapStylesLight from '../../../../helpers/universalMapStylesLight.json';
import customClientIcon from '../../../../assets/icons/pin.png'; // office location

const pinMapping = {
  1: veryLow,
  2: low,
  3: medium,
  4: high,
  5: veryHigh,
};

const getPixelPositionOffset = (width, height) => ({
  x: width,
  y: height - 6,
});

const google = window.google;
@withCookies
@connect(
  state => {
    return {
      appState: state.appState,
      mapState: state.mapState,
      rime: state.rime,
      riskTracker: state.riskTracker,
      partnerDetails: state.partnerDetails.partnerDetails,
      themeChange: state.themeChange,
    };
  },
  dispatch => {
    return {
      dispatch,
    };
  },
)
@withScriptjs
@withGoogleMap
class MapWithAMarker extends Component {
  state = {
    eventDetails: [],
    clientPinDetails: [],
    showMarkers: false,
    showLocation: true,
    latState: 16.950209,
    lngState: 93.207995,
    defaultOptions: {
      mapTypeControl: true,
      streetViewControl: false,
      zoomControl: false,
      fullscreenControl: false,
      draggable: true,
      minZoom: 2,
      zoomControlOptions: {
        position: google.maps.ControlPosition.LEFT_BOTTOM,
      },
      //mapTypeId: 'satellite'
    },
  };

  componentDidMount() {
    this.getPins();
    this.setState({
      showLocation: this.props.appState.appPermissions.locationPinSetting,
    });
  }

  componentDidUpdate(prevProps, prevState) {
    let {
      mapState: { eventDetails },
    } = this.props;
    let { clientPinDetails } = this.state;
    let prevEventDetails = prevProps.mapState.eventDetails;

    if (prevEventDetails !== eventDetails) {
      this.setState({ eventDetails: [...eventDetails, ...clientPinDetails] });
    }

    if (prevProps.rime !== this.props.rime) {
      this.setState({
        showLocation: this.props.appState.appPermissions.locationPinSetting,
        defaultOptions: {
          ...this.state.defaultOptions,
          draggable: this.props.rime.lockScreen,
        },
      });
    }
  }

  getPins = () => {
    let {
      mapState: { filters },
      cookies,
      dispatch,
    } = this.props;
    let authToken = cookies.get('authToken');

    let reqObj = {
      method: 'POST',
      isAuth: true,
      authToken,
      url: API_ROUTES.getEventListPortal,
      data: filters,
      showToggle: true,
    };

    dispatch(fetchRiskTracker(reqObj));
    this.getClientPins();
  };

  getClientPins = () => {
    let { cookies } = this.props;
    let { eventDetails } = this.state;
    let authToken = cookies.get('authToken');

    let reqObj = {
      method: 'POST',
      isAuth: true,
      authToken,
      url: API_ROUTES.getClientLocations,
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        this.setState({
          eventDetails: [...eventDetails, ...data],
          clientPinDetails: data,
        });
      })
      .catch(e => console.log(e));
  };

  setCenter = (lat, lng) => this.setState({ latState: lat, lngState: lng });

  render() {
    const { eventInfo, riskTracker } = this.props;
    let {
      eventDetails,
      showLocation,
      latState,
      lngState,
      defaultOptions,
    } = this.state;
    let CustomMarkerXML = [];
    let { latitude, longitude, focus } = this.props.partnerDetails;
    const {
      themeChange: { setDarkTheme },
    } = this.props;

    if (eventDetails) {
      CustomMarkerXML =
        eventDetails.length > 0 &&
        eventDetails.map((list, index) => {
          let lat = parseFloat(list.latitude);
          let lng = parseFloat(list.longitude);

          if (list.eventtense === 1) {
            return (
              <CustomMarker
                id={list.id}
                key={index}
                lat={lat}
                lng={lng}
                customIcon={pinMapping[list.risklevel]}
                title={list.title}
                risklevel={list.risklevel}
                riskcategory={list.riskcategory}
                isLive={true}
                trackLiveEvents={this.props.trackLiveEvents}
                eventInfo={eventInfo}
                distance={list.distance}
                locationName={list.locationName}
                setCenter={this.setCenter}
                defaultTitle="isLive"
              />
            );
          } else if (list.eventtense === 2 || list.eventtense === 0) {
            return (
              <CustomMarker
                id={list.id}
                key={index}
                lat={lat}
                lng={lng}
                customIcon={pinMapping[list.risklevel]}
                title={list.title}
                risklevel={list.risklevel}
                riskcategory={list.riskcategory}
                eventInfo={eventInfo}
                distance={list.distance}
                locationName={list.locationName}
                setCenter={this.setCenter}
                defaultTitle="isNotLive"
              />
            );
          } else if (!list.eventtense && showLocation) {
            return (
              <CustomMarker
                id={list.id}
                key={index + '_client'}
                lat={lat}
                lng={lng}
                customIcon={customClientIcon}
                title={list.title}
                showIcon={false}
                isClickable={false}
                eventInfo={eventInfo}
                distance={list.distance}
                locationName={list.locationName}
                setCenter={this.setCenter}
                showLocation={showLocation}
              />
            );
          }
        });
    }

    const enableCenter = riskTracker && riskTracker.riskTrackerDetails.show;
    const centerOne = {
      lat: latState,
      lng: lngState,
    };

    return (
      <GoogleMap
        defaultZoom={focus}
        defaultOptions={{
          ...defaultOptions,
        }}
        options={{
          ...defaultOptions,
          styles: setDarkTheme ? customMapStyles : customMapStylesLight,
        }}
        defaultCenter={{
          // lat: latitude || 16.950209,
          // lng: longitude || 93.207995,
          lat: latitude || 54.27,
          lng: longitude || -0.37,
        }}
        onTilesLoaded={() => this.setState({ showMarkers: true })}
        {...(enableCenter && { center: centerOne })}
      >
        {this.state.showMarkers && CustomMarkerXML}
      </GoogleMap>
    );
  }
}

export default MapWithAMarker;
