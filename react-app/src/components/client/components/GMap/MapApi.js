import React, { Component } from 'react';
import NavMenu from '../navMenu';
import MapWithAMarker from './MapWithAMarker';
import injectSheet from 'react-jss';
import style from './style';
import { fetchApi } from '../../../../helpers/http/fetch';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import { normalizeWatchList } from '../../../client/helpers/utils';
import { withCookies } from 'react-cookie';
import { connect } from 'react-redux';
import UpdateList from '../updateList';
import ControlOptions from '../../../ui/controlOptions';
import CustomList from '../../../ui/customList';
import { openEventModal } from '../../../../actions/EventModalAction';
import {
  toggleRiskTrackerBox,
  updateWatchList,
  toggleToast,
} from '../../../../actions';
import store from '../../../../store';
import { showEventDetail } from '../../../../api/api';
import CustomModal from '../../../ui/modal';
import ModalInner from '../../../ui/modal/ModalInner';
import { fetchRiskTracker } from '../../../../actions/riskTrackerAction';
import ClientFilterOptions from '../clientFilterOptions';
import {
  getWatchList,
  addToWatchList,
  removeFromWatchList,
} from '../../../../requestor/mitkatWeb/requestor';
import './sass/mapApi.scss';
import GMapModal from './GMapModal/index';

const TIMEOUT = 600000;
@withCookies
@injectSheet(style)
@connect(
  state => {
    return {
      mapState: state.mapState,
      eventModal: state.eventModal,
      riskTracker: state.riskTracker,
      themeChange: state.themeChange,
      watchList: state.mapState.watchList,
    };
  },
  dispatch => {
    return {
      dispatch,
    };
  },
)
class MapApi extends Component {
  state = {
    sliderData: [],
    shwNatEvtModal: false,
    eventList: [],
    liveUpdateList: [],
    eventDetail: null,
    eventInfo: null,
    watchListArr: [],
    isFirstTime: true,
  };

  componentDidMount() {
    // this.getEventList();
    this.getLiveUpdates();
    this.startTimer();
    this.fetchWatchList();
  }

  componentDidUpdate(prevProps) {
    const prevMapState = prevProps.mapState.eventDetails;
    const currentMapState = this.props.mapState.eventDetails;
    if (prevMapState !== currentMapState) {
      this.setState({ eventList: currentMapState });
    }

    if (
      prevProps.riskTracker.pauseInterval !==
      this.props.riskTracker.pauseInterval
    ) {
      if (this.props.riskTracker.pauseInterval) {
        clearInterval(this.timeout);
      } else {
        this.startTimer();
      }
    }

    if (this.props.watchList !== prevProps.watchList) {
      this.setState({ watchListArr: Object.values(this.props.watchList) });
    }
  }

  startTimer = () => {
    this.timeout = setInterval(() => {
      this.getEventList();
      this.getLiveUpdates();
      this.fetchWatchList();
      this.setState({ isFirstTime: false });
    }, TIMEOUT);
  };

  fetchWatchList = () => {
    const { dispatch } = this.props;
    getWatchList().then(async response => {
      if (response.length > 0) {
        const normalizedWatchlist = await normalizeWatchList(response);
        dispatch(updateWatchList(normalizedWatchlist));
      } else {
        dispatch(updateWatchList({}));
      }
    });
  };

  addWatchList = eventId => {
    addToWatchList(eventId).then(response => {
      this.showToast(response);
    });
  };

  removeWatchList = eventId => {
    removeFromWatchList(eventId).then(response => {
      this.showToast(response);
    });
  };

  showToast = data => {
    const { dispatch } = this.props;
    this.getEventList();
    this.fetchWatchList();
    dispatch(toggleToast({ showToast: data.success, toastMsg: data.message }));
  };

  getEventList = () => {
    let {
      mapState: { filters },
      cookies,
      dispatch,
    } = this.props;
    let authToken = cookies.get('authToken');

    let reqObj = {
      method: 'POST',
      isAuth: true,
      authToken,
      url: API_ROUTES.getEventListPortal,
      data: filters,
      showToggle: false,
    };

    dispatch(fetchRiskTracker(reqObj));
  };

  getLiveUpdates = () => {
    let {
      mapState: { filters },
      cookies,
    } = this.props;
    let authToken = cookies.get('authToken');

    let reqObj = {
      method: 'POST',
      isAuth: true,
      authToken,
      url: API_ROUTES.getLiveUpdatePortal,
      data: filters,
      showToggle: false,
    };

    fetchApi(reqObj)
      .then(liveUpdateList => {
        this.setState({ liveUpdateList });
      })
      .catch(e => console.log(e));
  };

  getNationalEvents = () => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');

    let reqObj = {
      method: 'POST',
      isAuth: true,
      authToken,
      url: API_ROUTES.getNationalEvents,
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        this.setState({ sliderData: data });
      })
      .catch(e => {
        console.log(e);
      });
  };

  handleOpenEvent = eventId => {
    const eventObj = {
      showEventModal: true,
      eventId,
    };

    showEventDetail(eventId).then(response => {
      this.setState({ eventDetail: response });
    });

    store.dispatch(openEventModal(eventObj));
  };

  showDetails = eventId => {
    showEventDetail(eventId).then(response => {
      this.setState({ eventDetail: response });
    });
  };

  handleCloseEvent = () => {
    const eventObj = {
      showEventModal: false,
    };
    store.dispatch(openEventModal(eventObj));
  };

  onHighlight = event => {
    store.dispatch(toggleRiskTrackerBox({ riskTrackerDetails: event }));
    this.setState({ eventInfo: event });
  };

  toggleEventModal = (show, eventDetail) => {
    this.setState({ eventDetail, showEventModal: show });
  };

  render() {
    const { open, eventModal } = this.props;
    const {
      eventList,
      liveUpdateList,
      eventDetail,
      eventInfo,
      watchListArr,
      isFirstTime,
    } = this.state;
    const {
      themeChange: { setDarkTheme },
    } = this.props;
    return (
      <div className="mapOutterWrap">
        <div id="mapWrapper" className="mapInnerWrap">
          <ControlOptions />
          <div id="liveWatchList">
            {watchListArr.length > 0 && (
              <CustomList
                watchList={watchListArr}
                isWatchList={true}
                onHighlight={this.onHighlight}
                parentHandler={this.handleOpenEvent}
                removeWatchList={this.removeWatchList}
              />
            )}
          </div>
          <div id="liveListPortal">
            {
              <CustomList
                feed={eventList || []}
                parentHandler={this.handleOpenEvent}
                onHighlight={this.onHighlight}
                addWatchList={this.addWatchList}
                removeWatchList={this.removeWatchList}
                watchList={watchListArr}
                isFirstTime={isFirstTime}
              />
            }
          </div>
          {liveUpdateList && liveUpdateList.length > 0 && (
            <div id="liveUpdatePortal">
              <UpdateList
                feed={liveUpdateList.length > 0 ? liveUpdateList : []}
                parentHandler={eventId => this.handleOpenEvent(eventId)}
              />
            </div>
          )}
          <ClientFilterOptions />

          <MapWithAMarker
            googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyCYOTLyn6xbysArlvlW4zpxnH9p6H3An08&v=3.exp&libraries=geometry,drawing,places"
            loadingElement={<div style={{ height: `100%` }} />}
            containerElement={
              <div style={{ height: `100vh`, position: 'relative' }} />
            }
            mapElement={<div style={{ height: `100%` }} />}
            handleHideMap={this.props.handleHideMap}
            trackLiveEvents={this.props.trackLiveEvents}
            eventInfo={eventInfo}
          />
          <NavMenu />
          <CustomModal
            showModal={eventModal.showEventModal}
            closeModal={this.handleCloseEvent}
          >
            <div className={`new__modal__inner dark--body ${'light'}`}>
              {eventDetail && (
                <GMapModal
                  eventDetail={eventDetail && eventDetail}
                  risklevel={eventDetail.risklevel}
                  riskcategory={eventDetail.riskcategory}
                  closeModal={this.handleCloseEvent}
                  showDetails={this.showDetails}
                  isLive={false}
                  hasDarkTheme={true}
                />
              )}
            </div>
          </CustomModal>
        </div>
      </div>
    );
  }
}

export default MapApi;
