import React, { Component } from 'react';
import { Marker, InfoWindow, OverlayView } from 'react-google-maps';
import { connect } from 'react-redux';
import MarkerWithLabel from 'react-google-maps/lib/components/addons/MarkerWithLabel';
import injectSheet from 'react-jss';
import style from './style';
import { list } from 'postcss';
import CustomModal from '../../../ui/modal';
import ModalInner from '../../../ui/modal/ModalInner';
//
import {
  RISK_LEVEL_COLORS,
  RISK_CATEGORY_ICONS,
} from '../../../../helpers/constants';
import { fetchApi } from '../../../../helpers/http/fetch';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import { riskCategoryUrls } from '../../../../context/mapping';
//
import { withCookies } from 'react-cookie';
//
import './sass/style.scss';
import { openEventModal } from '../../../../actions/EventModalAction';
import store from '../../../../store';
import GMapModal from './GMapModal';

const google = window.google;

const RISK_CATEGORY_URLS = riskCategoryUrls;

@withCookies
@injectSheet(style)
@connect(state => {
  return {
    riskTracker: state.riskTracker,
    rime: state.rime,
    themeChange: state.themeChange,
  };
})
class CustomMarker extends Component {
  state = {
    showInfoBox: false,
    showModal: false,
    eventDetail: '',
  };

  componentDidUpdate(prevProps) {
    const { riskTracker, setCenter } = this.props;
    if (
      riskTracker &&
      riskTracker.riskTrackerDetails.show !==
        prevProps.riskTracker.riskTrackerDetails.show
    ) {
      const { latitude, longitude, show } = riskTracker.riskTrackerDetails;
      const { lat, lng } = this.markerRef.current.props.position;
      if (latitude === lat && longitude === lng) {
        this.setState({ showInfoBox: show });
        this.props.rime.lockScreen && setCenter(lat, lng);
      } else {
        this.setState({ showInfoBox: false });
      }
    }
  }

  markerRef = React.createRef();

  handleShowInfoBox = show => {
    this.setState({ showInfoBox: show });
  };

  showEventDetail = eventId => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    const eventObj = {
      showEventModal: true,
      eventId,
    };
    store.dispatch(openEventModal(eventObj));

    let reqObj = {
      method: 'POST',
      url: `${API_ROUTES.getEventDetails}/${eventId}`,
      isAuth: true,
      authToken: authToken,
      showToggle: true,
    };

    fetchApi(reqObj).then(data => {
      this.setState({
        showModal: true,
        showInfoBox: false,
        eventDetail: data,
      });
    });
  };

  handleCloseModal = () => {
    const eventObj = {
      showEventModal: false,
    };
    store.dispatch(openEventModal(eventObj));
    this.setState({ showModal: false });
  };

  trackLiveEvents = id => {
    this.setState({ showModal: false });
    this.props.trackLiveEvents(id);
  };

  renderIcon = () => {
    const { riskcategory, risklevel, setCenter } = this.props;

    if (riskcategory && risklevel) {
      let url =
        RISK_CATEGORY_URLS[riskcategory - 1] &&
        RISK_CATEGORY_URLS[riskcategory - 1].riskLevels &&
        RISK_CATEGORY_URLS[riskcategory - 1].riskLevels[risklevel] &&
        RISK_CATEGORY_URLS[riskcategory - 1].riskLevels[risklevel].url;
      return url;
    }
  };

  render() {
    let {
      id,
      title,
      lat,
      lng,
      showLocation,
      customIcon,
      classes,
      riskcategory,
      risklevel,
      showIcon = true,
      isClickable = true,
      isLive = false,
      distance,
      locationName,
      defaultTitle,
    } = this.props;
    let { showInfoBox, showModal, eventDetail } = this.state;
    const {
      themeChange: { setDarkTheme },
    } = this.props;

    return (
      <Marker
        onMouseOver={() => this.handleShowInfoBox(true)}
        onMouseOut={() => this.handleShowInfoBox(false)}
        position={{ lat, lng }}
        onClick={
          isClickable
            ? () => this.showEventDetail(id)
            : () => console.log('not allowed')
        }
        icon={customIcon}
        ref={this.markerRef}
        defaultTitle={defaultTitle}
        //@info => do not remove this label prop, as this works for live pulse effect on markers
        label={''}
      >
        {showInfoBox && (
          <React.Fragment>
            <InfoWindow
              className="riskTracker__infoCont_infoWindow"
              onCloseClick={() => this.handleShowInfoBox(false)}
            >
              <div className="riskTracker__infoCont">
                <div className="riskTracker__boxes box__one">
                  {showIcon && (
                    <span className="s--icon infoWindow">
                      <img src={this.renderIcon()} alt="logo" />
                    </span>
                  )}
                </div>
                <div className="riskTracker__boxes box__two">
                  <div
                    className={classes.rootAlertListWrap}
                    onClick={() => this.showEventDetail(id)}
                  >
                    <p className="alert--title">{title}</p>
                    {!showLocation && (
                      <>
                        <p>
                          Distance:{' '}
                          {distance
                            ? `${Math.floor(distance)} kms`
                            : 'Not Applicable'}
                        </p>
                        <p>
                          Nearest Client Location:{' '}
                          {locationName ? locationName : '-'}
                        </p>
                      </>
                    )}
                  </div>
                </div>
              </div>
            </InfoWindow>
          </React.Fragment>
        )}
        <CustomModal showModal={showModal} closeModal={this.handleCloseModal}>
          <div className={`new__modal__inner dark--body ${'light'}`}>
            <GMapModal
              {...this.props}
              eventDetail={eventDetail && eventDetail}
              risklevel={risklevel}
              riskcategory={riskcategory}
              closeModal={this.handleCloseModal}
              isLive={isLive}
              trackLiveEvents={this.trackLiveEvents}
            />
          </div>
        </CustomModal>
      </Marker>
    );
  }
}

export default React.forwardRef((props, ref) => (
  <CustomMarker innerRef={ref} {...props} />
));
