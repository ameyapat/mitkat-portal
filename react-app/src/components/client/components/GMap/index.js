import React, { Component } from 'react';
//components
import { connect } from 'react-redux';
import MapApi from './MapApi';
//hoc
import { withStyles } from '@material-ui/core/styles';
//components
import LeftSideNav from '../leftSideNav';
import LiveEventsDrawer from '../liveEventsDrawer';
import CustomModal from '../../../ui/modal';

import { withCookies } from 'react-cookie';
import { withRouter } from 'react-router-dom';
import * as actions from '../../../../actions';

import './sass/style.scss';
import { Drawer } from '@material-ui/core';

const drawerWidth = 300;

const styles = theme => ({
  iconOptions: {
    display: 'flex',
    justifyContent: 'flex-end',
    padding: 3,
    minHeight: 40,
  },
  closed: {
    justifyContent: 'space-around',
  },
  chevIcons: {
    color: 'var(--whiteMediumEmp)',
  },
  menuIcon: {
    color: 'var(--whiteMediumEmp)',
  },
  paperAnchorRight: {
    zIndex: 996,
    position: 'absolute',
    overflowX: 'hidden',
    overflowY: 'hidden',
    width: drawerWidth,
    backgroundColor: 'var(--backgroundSolid)',
    '&:hover': {
      overflowY: 'scroll',
    },
    '&::-webkit-scrollbar': {
      width: 3,
    },
    '&::-webkit-scrollbar-track': {
      background: 'transparent',
    },
    '&::-webkit-scrollbar-thumb': {
      backgroundColor: '#ccc',
      borderRadius: 5,
    },
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 36,
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing.unit * 7 + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing.unit * 9 + 1,
    },
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
  },
  menuItemBtn: {
    padding: 6,
  },
  test: {
    background: 'none',
  },
});

@withCookies
@withRouter
@connect(state => {
  return {
    rime: state.rime,
    themeChange: state.themeChange,
  };
})
class GMap extends Component {
  state = {
    open: false,
    openLiveTracking: false,
    trackingId: '',
  };

  handleDrawerOpen = () => {
    this.setState({ open: true });
  };

  handleDrawerClose = () => {
    this.setState({ open: false });
    this.props.dispatch(actions.toggleRimeFilters(false));
  };

  trackLiveEvents = trackingId => {
    this.setState({ openLiveTracking: true, trackingId });
  };

  toggleLiveEventDrawer = show => {
    this.setState({ openLiveTracking: show });
  };

  render() {
    let {
      classes,
      themeChange: { setDarkTheme },
    } = this.props;
    let { open, openLiveTracking, trackingId } = this.state;
    let { showRimeFilter, showSettings, lockScreen } = this.props.rime;

    return (
      <div className={`${setDarkTheme ? 'dark' : 'light'}`}>
        <Drawer
          classes={{
            root: classes.rootDrawer,
            paperAnchorRight: classes.paperAnchorRight,
            modal: classes.test,
          }}
          anchor="right"
          open={showRimeFilter}
          onClose={this.handleDrawerClose}
          className={`${setDarkTheme ? 'dark' : 'light'}`}
        >
          <div className="drawer_header">
            <h1>Apply Filter</h1>
            <a
              className="close_drawer--btn"
              onClick={() => this.handleDrawerClose()}
            >
              <i className="fas fa-chevron-right"></i>
            </a>
          </div>
          <LeftSideNav
            showRimeFilter={this.showRimeFilter}
            handleDrawerClose={this.handleDrawerClose}
          />
        </Drawer>

        {/* <CustomModal showModal={showRimeFilter}>
          <div id="filterModal" className="modal__inner">
            <div className="filterModal__header">
              <h1>Apply Filters</h1>
              <button
                className="btn--close"
                onClick={() => this.handleDrawerClose()}
              >
                &times;
              </button>
            </div>
            <div className="filterModal__body">
              <LeftSideNav handleDrawerClose={this.handleDrawerClose} />
            </div>
            <div className="filterModal__footer"></div>
          </div>
        </CustomModal> */}

        {/* <CustomModal showModal={showSettings}>
            <div id="rimeUserSettings">
              <div className="userSettings__header">
                <h1>Settings</h1>
                <button className="btn--close" onClick={this.closeSettings}>&times;</button>
              </div>
              <div className="userSettings__body">
                <div className="userSettings_toggles">
                  <span className="userSettings__label">Lock Map Screen</span>                
                  <FormControlLabel
                    control={
                      <Switch 
                        checked={!lockScreen} 
                        onChange={this.handleLockScreen} 
                        name="lockScreen" 
                      />
                    }
                    classes={{
                      root: classes.rootControlLabel,
                      label: classes.rootLabelControlLabel,
                      switchBase: classes.switchBase,
                      thumb: classes.switchThumb,
                      track: classes.switchTrack,
                      checked: classes.switchChecked,
                    }}
                  />
                </div>
                <div className="userSettings_toggles">
                  <span className="userSettings__label">Show Locations</span>                
                    <FormControlLabel
                      control={
                        <Switch 
                          checked={showLocationPin} 
                          onChange={this.handleShowLocations} 
                          name="showLocationPin" 
                        />
                      }
                      classes={{
                        root: classes.rootControlLabel,
                        label: classes.rootLabelControlLabel
                      }}
                    />
                </div>
              </div>
              <div className="userSettings__footer">
                <button className="btnLogout" onClick={this.handleLogout}>Logout</button>
              </div>
            </div>
          </CustomModal> */}
        {openLiveTracking && (
          <LiveEventsDrawer
            open={openLiveTracking}
            handleDrawerClose={this.toggleLiveEventDrawer}
            trackingId={trackingId}
          />
        )}
        <div className="map-wrapper">
          <MapApi
            open={open}
            handleDrawerOpen={this.handleDrawerOpen}
            trackLiveEvents={this.trackLiveEvents}
          />
        </div>
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(GMap);
