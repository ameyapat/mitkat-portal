export default {
  rootGMapAlertCompRoot: {
    display: 'flex',
    minHeight: '100vh',
    '& .alertListWrap': {
      flexBasis: '320px',
      borderRight: '1px solid #d3d3d3',
      overflowY: 'scroll',
      minHeight: '100vh',
      // backgroundColor: 'yellow'
    },
    '& .alertListItemWrap': {
      flex: 1,
      // backgroundColor: 'grey'
    },
  },
  rootGMapAlertList: {
    '& .alert-item': {
      padding: 10,
      background: '#fff',
      borderBottom: '1px solid #d3d3d3',
      cursor: 'pointer',
      '& .alert-item-title': {
        paddingBottom: 8,
        '& h2': {
          textTransform: 'capitalize',
          fontWeight: 600,
          color: '#444',
          fontSize: '1em',
          margin: 0,
        },
      },
      '& .alert-item-desc': {
        paddingBottom: 8,
        '& p': {
          fontSize: '0.9em',
          color: '#444',
          margin: 0,
        },
      },
      '& .alert-item-date': {
        paddingBottom: 10,
        fontWeight: 600,
      },
      '&:hover': {
        backgroundColor: '#f3f3f3',
      },
    },
  },
  rootGMapListItem: {
    padding: '10px 15px',
    '& label': {
      fontWeight: 700,
      color: '#333',
    },
    '& .item-header': {
      '& h1': {
        textTransform: 'capitalize',
        fontSize: '1.6em',
        color: '#444',
      },
      '& .item-date': {
        color: '#444',
        fontSize: '0.9em',
      },
    },
    '& p': {
      color: '#444',
      fontSize: '1em',
    },
    '& .item-sub-header': {
      '& h2': {
        fontSize: '1.2em',
        color: '#444',
        fontWeight: 500,
        textTransform: 'capitalize',
      },
    },
    '& .item-details': {
      marginBottom: 30,
      '& p': {
        fontSize: '1em',
        color: '#444',
      },
    },
  },
  mapAlertSolid: {
    '& .list-title-wrap': {
      display: 'flex',
      padding: '20px 35px',
      background: '#fff',
      borderBottom: '1px solid #d3d3d3',
      alignItems: 'center',
      justifyContent: 'center',
      '& .trend-icon': {
        paddingRight: 15,
        '& .fa': {
          fontSize: '1.3em',
          color: 'var(--backgroundSolid)',
        },
      },
      '& h1': {
        margin: 0,
        color: 'var(--whiteMediumEmp)',
        fontSize: '1.4em',
      },
    },
  },
  infoBox: {
    display: 'flex',
    alignItems: 'center',
    cursor: 'pointer',
    backgroundColor: 'rgba(255, 255, 255, 0.6)',
    color: '#444',
    padding: '6px 0',
    borderBottom: '1px solid #f1f1f1',
  },
  boxOne: {},
  boxTwo: {},
  mapOuterWrapper: {
    '& .hamburger-wrapper': {
      position: 'absolute',
      zIndex: '999',
      bottom: '60px',
      left: '1%',
    },
    '& .fa': {
      fontSize: '1.1em',
      color: 'var(--whiteMediumEmp)',
      position: 'absolute',
      top: '50%',
      left: '50%',
      transform: 'translate(-50%,-50%)',
    },
    '& .hamburger-icon': {
      backgroundColor: '#2c3e50',
      borderRadius: '50%',
      height: '32px',
      width: '32px',
      position: 'relative',
      padding: 4,
      display: 'block',
      cursor: 'pointer',
      boxShadow: 'rgba(0, 0, 0, 0.3) 0px 1px 4px -1px',
    },
    '& .main-switches': {
      position: 'absolute',
      top: 0,
      left: '50%',
      transform: 'translateX(-50%)',
      background: 'rgba(52, 73, 94, 0.8)',
      margin: 10,
      padding: '10px 20px',
      borderRadius: 1,
      color: 'var(--whiteMediumEmp)',
    },
  },
  paperAnchorWrap: {
    background: 'transparent',
  },
  rootSideMenu: {
    // position: 'absolute',
    backgroundColor: 'rgba(52, 73, 94, 0.8)',
    minHeight: '100vh',
    zIndex: 100,
    width: 250,
    overflowY: 'auto',
    top: 0,
    bottom: 0,
    // padding: '10px',
    color: 'rgba(255,255,255,0.7)',
  },
  rootAlertListWrap: {
    // backgroundColor: 'red',
    display: 'flex',
    alignItems: 'center',
    maxHeight: 128,
    maxWidth: 256,
    overflowY: 'auto',
    overflowX: 'hidden',
    flexDirection: 'column',
    alignItems: 'flex-start',

    '&::-webkit-scrollbar': {
      width: 5,
    },
    '&::-webkit-scrollbar-track': {
      background: '#f1f1f1',
    },
    '&::-webkit-scrollbar-thumb': {
      background: '#e74c3c',
    },
    '&::-webkit-scrollbar-thumb:hover': {
      background: '#c0392b',
    },
    '& .s--icon.infoWindow': {
      width: '32px !important',
      height: '32px !important',
      lineHeight: '32px',
      textAlign: 'center',
      '& i': {
        fontSize: 20,
      },
    },
  },
  iconWrap: {
    padding: 8,
    '& span': {
      display: 'block',
      position: 'relative',
    },
    '& i': {
      position: 'absolute',
      left: '50%',
      top: '50%',
      transform: 'translate(-50%,-50%)',
      fontSize: '1em',
    },
  },
  titleWrap: {
    paddingLeft: 10,
    '& p': {
      margin: 0,
      fontWeight: 500,
      textTransform: 'capitalize',
      lineHeight: '1.3em',
    },
  },
  menuIcon: {
    color: 'var(--whiteMediumEmp)',
    fontSize: 22,
  },
  iconBtn: {
    padding: 0,
    position: 'absolute !important',
    top: '10px !important',
    background: 'rgba(20, 33, 54, 0.8) !important',
    width: '50px',
    height: '50px',
    right: '1%',
    boxShadow: '1px 2px 8px 1px rgba(0, 0, 0, 0.3)',
    borderRadius: '12px !important',
    '&:hover': {
      // background: 'rgba(0, 0, 0, 0.7)'
    },
    '& i': {
      color: 'rgba(255, 255, 255, 0.87)',
      fontSize: 18,
    },
  },
  menuItemBtn: {
    padding: 6,
  },
  infoWindowStyles: {
    backgroundColor: 'red',
  },
  rootControlLabel: {
    color: 'red',
  },
  rootLabelControlLabel: {
    color: 'red !important',
  },
  switchBase: {
    color: 'red',
    '&$checked': {
      color: 'yellow',
    },
    '&$checked + $track': {
      backgroundColor: 'green',
    },
  },
  thumb: {},
  track: {},
  checked: {},
};
