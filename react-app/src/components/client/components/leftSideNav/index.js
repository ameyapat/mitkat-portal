import React, { Component } from 'react';
import './style.scss';
import Filters from '../filters';

class LeftSideNav extends Component {
  render() {
    return (
      <div id="leftSideNav">
        <Filters handleDrawerClose={this.props.handleDrawerClose} />
      </div>
    );
  }
}

export default LeftSideNav;
