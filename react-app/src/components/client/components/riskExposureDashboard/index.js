import React, { Component } from 'react';
import { Switch, Route, withRouter } from 'react-router-dom';
import './style.scss';
import RiskExposure from '../riskExposure';
import LocationAnalysis from '../locationAnalysis';
import ClientLocationAnalysis from '../clientLocationAnalysis';
import TopicAnalysis from '../topicAnalysis';
import ComingSoon from '../comingSoon';
import { routeItems } from './helpers/constant';
import ViewSwitch from '../ViewSwitch/ViewSwitch';
import PrivacyPolicy from '../privacyPolicy';
import Awards from '../mitkatAwards';
import Certificates from '../mitkatCertificates';
import TermsOfService from '../termsOfService';
import NavMenu from '../navMenu';

const componentRegistry = {
  RiskExposure,
  LocationAnalysis,
  ClientLocationAnalysis,
  ComingSoon,
  PrivacyPolicy,
  Awards,
  Certificates,
  TermsOfService,
  TopicAnalysis,
};

@withRouter
class RiskExposureDashboard extends Component {
  getRouteList = () => {
    let routetXML = [];
    let { routeOptions } = routeItems;
    routetXML = routeOptions.map(item => (
      <Route
        exact={item.exact}
        path={`${this.props.match[item.path]}${item.route}`}
        component={componentRegistry[item.components]}
      />
    ));
    return routetXML;
  };

  render() {
    return (
      <>
        <ViewSwitch />
        <NavMenu />
        <div id="riskExposureDashboard">
          <Switch>{this.getRouteList()}</Switch>
        </div>
      </>
    );
  }
}

export default RiskExposureDashboard;
