import React, { Component } from 'react';
import MapApi from '../rimeMap/MapApi';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox, { checkboxClasses } from '@mui/material/Checkbox';
import './style.scss';

class RimeOnScreenFilters extends Component {
  state = {
    filterFeed: [],
  };

  componentDidMount = () => {
    this.setState({ filterFeed: this.props.feed });
  };

  componentDidUpdate(prevProps) {
    if (prevProps.feed !== this.props.feed) {
      this.setState({ filterFeed: this.props.feed });
    }
  }

  filterRiskEvents = (event, value) => {
    let { filterFeed } = this.state;
    event.target.checked
      ? this.setState({
          filterFeed: [
            ...filterFeed,
            ...this.props.feed.filter(item => item.eventRiskLevel === value),
          ],
        })
      : this.setState({
          filterFeed: filterFeed.filter(item => item.eventRiskLevel !== value),
        });
  };

  render() {
    return (
      <div>
        <MapApi
          feed={this.state.filterFeed}
          clientLocations={this.props.clientLocations}
        />
        <div className="onScreenFilters">
          <FormGroup
            style={{
              display: 'flex',
              flexDirection: 'row',
              alignItems: 'center',
            }}
          >
            <FormControlLabel
              control={
                <Checkbox
                  defaultChecked
                  sx={{
                    [`&, &.${checkboxClasses.checked}`]: {
                      color: 'rgba(0,145,255,1)',
                    },
                  }}
                  onClick={e => this.filterRiskEvents(e, 3)}
                />
              }
              label="High Relevency"
            />
            <FormControlLabel
              control={
                <Checkbox
                  defaultChecked
                  sx={{
                    [`&, &.${checkboxClasses.checked}`]: {
                      color: 'rgba(0,145,255,1)',
                    },
                  }}
                  onClick={e => this.filterRiskEvents(e, 2)}
                />
              }
              label="Medium Relevency"
            />
            <FormControlLabel
              control={
                <Checkbox
                  defaultChecked
                  sx={{
                    [`&, &.${checkboxClasses.checked}`]: {
                      color: 'rgba(0,145,255,1)',
                    },
                  }}
                  onClick={e => this.filterRiskEvents(e, 1)}
                />
              }
              label="Low Relevency"
            />
          </FormGroup>
        </div>
      </div>
    );
  }
}

export default RimeOnScreenFilters;