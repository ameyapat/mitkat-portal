export default {
  rootAccotdion: {
    backgroundColor: 'var(--cardBackground) !important',
    borderBottom: '1px solid #314058',
  },
  rootAccotdionSummary: {
    flexDirection: 'row',
    paddingLeft: '0px',
    minHeight: '90px !important',
  },
  contentSummary: {
    display: 'block',
    color: 'var(--whiteMediumEmp)',
    '& h4': {
      color: 'var(--whiteMediumEmp)',
      marginBottom: '10px',
      fontWeight: '100',
    },
    '& .totalEvents': {
      position: 'absolute',
      left: '0px',
      top: '0px',
      padding: '10px',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'left',
      justifyContent: 'left',
      fontSize: 'calc(7px + 1vmin)',
      color: 'var(--highlightText)',
      borderRadius: '5px',
      '& p': {
        fontSize: '90% !important',
        marginTop: '3px',
        maxHeight: '30px',
        overflowY: 'scroll',
        color: 'var(--highlightText)'
      },
      '& .locationIcon': {
        height: 'calc(3px + 1vmin)',
        color: '#acc243',
      },
    },
    '& p': {
      color: '#9a9a9a',
      marginBottom: '5px',
      fontSize: '13px !important',
      '& span': {
        padding: '0 10px',
        color: '#000',
        borderRadius: '5px',
        fontSize: '5px !important',
        marginRight: '3px',
      },
      '& span.infoIcon': {
        color: '#314058',
        '& button': {
          background: 'transparent',
          border: 'none',
          cursor: 'pointer',
          '& i': {
            color: '#4caf50',
          },
        },
      },
    },
  },
  icon: {
    color: '#3b496d',
  },
};