import React, { Component } from 'react';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import PlaceIcon from '@mui/icons-material/Place';
import style from './style';
import './style.scss';
import injectSheet from 'react-jss';
import TopEventsCard from './topEventsCard';

@injectSheet(style)
class RimeTopEvents extends Component {
  renderTopEventList = () => {
    const { topEventsFeed } = this.props;
    let { classes } = this.props;

    return topEventsFeed.map(item => (
      <Accordion
        classes={{
          root: classes.rootAccotdion,
        }}
      >
        <AccordionSummary
          aria-controls="panel1a-content"
          id="panel1a-header"
          classes={{
            root: classes.rootAccotdionSummary,
            content: classes.contentSummary,
            expandIcon: classes.icon,
          }}
        >
          <div className="totalEvents">
            <p>
              <small>
                <PlaceIcon className="locationIcon" />
                {item.eventLoc}
              </small>
            </p>
            {item.eventTitle}
          </div>
        </AccordionSummary>
        <AccordionDetails
          classes={{
            root: classes.rootAccotdionDetails,
          }}
        >
          <TopEventsCard
            title={item.eventTitle}
            description={item.eventDescription}
            sourceLinks={item.eventAllNewsLinks}
            location={item.eventLoc}
            noOfArticles={item.count}
            categoryId={item.eventPrimaryRiskCategory}
            secondaryCatergoryId={item.eventSecondaryRiskCategory}
            subRiskCategoryId={item.subRiskCategory}
            link={item.eventLink}
            hasClose={false}
            eventDate={item.eventDate}
            lastUpdatedDate={item.eventLatestUpdate}
            relevancy={item.eventRiskLevel}
            imgURL={item.eventImageUrl}
            tags={item.eventTags}
            language={item.language}
          />
        </AccordionDetails>
      </Accordion>
    ));
  };
  render() {
    return <div className="topList">{this.renderTopEventList()}</div>;
  }
}

export default RimeTopEvents;
