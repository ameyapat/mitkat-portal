import React, { useEffect, useState } from 'react';
import './style.scss';
import moment from 'moment';
import {
  categories,
  subRiskCategories,
} from '../rimeDashboard/helpers/constants';
import '../../sass/style.scss';
import ArticleModal from '../rimeEventCard/articleModal';
//Card
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import Typography from '@mui/material/Typography';
import { useSelector } from 'react-redux';
import CustomModal from '../../../ui/modal';
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye';
import TagsIcon from '../../../../assets/svg/tagsIcon.svg';
import { eDate, lastUpdateDate } from '../rimeDashboard/helpers/utils';

const pinMappingRgba = {
  1: 'low',
  2: 'medium',
  3: 'high',
};

const TopEventsCard = props => {
  const {
    title,
    categoryId,
    secondaryCatergoryId,
    subRiskCategoryId,
    link,
    eventDate,
    relevancy,
    description,
    sourceLinks,
    noOfArticles,
    lastUpdatedDate,
    imgURL,
    tags,
    language,
  } = props;

  const [isNew, setIsNew] = useState(true);
  const [category, setCategory] = useState('');
  const [secondaryCategory, setSecondaryCategory] = useState('');
  const [subRiskCategory, setSubRiskCategory] = useState('');
  const setTheme = useSelector(state => state.themeChange.setDarkTheme);
  const refreshDuration = useSelector(state => state.rime.refreshDuration);
  const [showTags, setShowTags] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const TIMEOUT = refreshDuration * 60000;

  const mapRiskCategory = () => {
    categories.map(item => {
      if (categoryId === item.id) {
        setCategory(item.label);
      }
    });
    subRiskCategories.map(item => {
      if (subRiskCategoryId === item.id) {
        setSubRiskCategory(item.label);
      }
    });
  };

  const handleModal = () => {
    setShowModal(false);
  };

  const showArticleModal = () => {
    setShowModal(true);
  };

  const showTagsDiv = () => {
    setShowTags(prevCheck => !prevCheck);
  };

  useEffect(() => {
    mapRiskCategory();
  });

  let tag = tags && tags.split(',');
  let allLinks = sourceLinks && sourceLinks.split(',');

  // To convert UTC Time to Local Time as per Computer's TimeZone
  const utcLastUpdateDate = lastUpdateDate(lastUpdatedDate);
  const utcDate = eDate(eventDate);

  return (
    <div id="rime__topEvents__eventCard">
      <Card
        className={`rime__topEvents__eventCardList ${pinMappingRgba[relevancy]}`}
      >
        <div className="topEvents__description">
          <Typography component="div" className="topEvents__eventbody__wrap">
            Description:&nbsp;
            {description ? description.substring(0, 200) : 'Not Available'}
            <span style={{ color: 'var(--link)' }}>
              {description && (
                <a className="anchor__text" href={link} target="_blank">
                  ...read more
                </a>
              )}
            </span>
          </Typography>
        </div>
        <div className="topEvents_row_2">
          <div>
            <Typography
              variant="subtitle1"
              color="text.secondary"
              component="span"
              className="topEvents__eventbody__subHead"
            >
              Language:
            </Typography>
            <Typography component="span" className="topEvents__eventbody__wrap">
              &nbsp;{language ? language : 'Not Available'}
            </Typography>
          </div>
        </div>
        <div className="topEvents__header">
          <div
            className={`topEvents__pill ${relevancy &&
              pinMappingRgba[relevancy]}`}
          >
            <Typography
              component="span"
              className="topEvents__cat__pill"
              style={{
                fontWeight: 500,
              }}
            >
              {category ? category : 'Not Available'}
              {subRiskCategoryId !== 0 && subRiskCategoryId !== null && (
                <span className="topEvents__cat__pill__space">,</span>
              )}
              {subRiskCategoryId !== 0 && subRiskCategory}
            </Typography>
          </div>
          <div
            className="topEvents__article__content__pill"
            onClick={() => showArticleModal()}
          >
            <Typography
              variant="subtitle1"
              color="text.secondary"
              component="span"
              className="topEvents__eventbody__Head"
              style={{ marginTop: '-1px' }}
            >
              No of Articles:
            </Typography>
            <Typography component="span" className="topEvents__eventbody__Head">
              &nbsp;
              {noOfArticles ? noOfArticles : 'Not Available'}
            </Typography>
            <span>&nbsp;</span>
            <RemoveRedEyeIcon className="eyeIcon" />
          </div>
        </div>
        <div className="topEvents__card__row2">
          <Box sx={{ display: 'flex', flexDirection: 'column' }}>
            <div className="topEvents__card__innercolumn">
              <div>
                <Typography
                  component="span"
                  className="rime__topEvents__eventDate"
                >
                  &nbsp;
                  {moment(utcDate).format('Do MMM YYYY')}
                </Typography>
                <Typography
                  variant="subtitle1"
                  color="text.secondary"
                  component="span"
                  className="rimeupdate__topEvents__date"
                >
                  Updated:&nbsp;
                </Typography>
                <Typography
                  component="span"
                  className="rimeupdate__topEvents__date"
                >
                  {moment(utcLastUpdateDate).format('Do MMM YYYY, h:mm:ss a')}
                </Typography>
              </div>
              <div
                className="topEvents__article__content__pill"
                onClick={() => showTagsDiv()}
              >
                <Typography
                  variant="subtitle1"
                  color="text.secondary"
                  component="span"
                  className="topEvents__eventbody__Head"
                  style={{ marginTop: '-1px' }}
                >
                  Tags
                </Typography>
                <span>&nbsp;&nbsp;</span>
                <img className="tagsIcon" src={TagsIcon} />
              </div>
            </div>
            <div className="topEvents__rime__tags">
              {showTags &&
                tags &&
                tag.length > 0 &&
                tag.map(subitem => {
                  return <span>#{subitem}</span>;
                })}
            </div>
          </Box>
        </div>
      </Card>
      <CustomModal showModal={showModal}>
        <div
          id="rimeEventArchivesModalCont"
          className={`modal__inner ${setTheme ? 'dark' : 'light'}`}
        >
          <ArticleModal
            alllinks={allLinks}
            closeEventModal={() => handleModal()}
          />
        </div>
      </CustomModal>
    </div>
  );
};

export default TopEventsCard;
