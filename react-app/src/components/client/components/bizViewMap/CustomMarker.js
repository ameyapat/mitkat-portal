import React, { Component } from 'react'
import {
    Marker,
    InfoWindow,
    OverlayView
  } from "react-google-maps";
import MarkerWithLabel from "react-google-maps/lib/components/addons/MarkerWithLabel";
import injectSheet from 'react-jss'
import style from './style'
import { list } from 'postcss';
import CustomModal from '../../../ui/modal';
import ModalInner from '../../../ui/modal/ModalInner';
//
import { RISK_LEVEL_COLORS, RISK_CATEGORY_ICONS } from '../../../../helpers/constants';
import { fetchApi } from '../../../../helpers/http/fetch';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
//
import { withCookies } from 'react-cookie';

const google =  window.google

@withCookies
@injectSheet(style)
class CustomMarker extends Component {

    state = {
        showInfoBox : false,
        showModal : false,
        eventDetail: "",
        riskcategory: '',
        risklevel: ''
    }

    markerRef = React.createRef();

    handleShowInfoBox = (show) => {
        this.setState({showInfoBox: show})
    }

    showEventDetail = (eventId) => {

        let { cookies } = this.props;
        let authToken = cookies.get('authToken');

        let reqObj = {
            method: 'POST',
            url: `${API_ROUTES.getEventDetails}/${eventId}`,
            isAuth: true,
            authToken: authToken,
            showToggle: true,
        }

        fetchApi(reqObj)
        .then(data => {
            this.setState({
                showModal: true,
                showInfoBox: false,
                eventDetail: data,
                riskcategory: data.riskcategory,
                risklevel: data.risklevel
            });
        })
    }

    handleCloseModal = () => {
        this.setState({showModal: false});
    }  

  render() {

    let { id, title, lat, lng, customIcon, classes } = this.props;
    let { showInfoBox, eventDetail, showModal, riskcategory, risklevel } = this.state;

    return (
        <Marker
            onMouseOver={() => this.handleShowInfoBox(true)}
            onMouseOut={() => this.handleShowInfoBox(false)}
            position={{ lat, lng }}
            onClick={() => this.showEventDetail(id)}
            icon={customIcon}
            ref={this.markerRef}
            optimized={false}
        >
            {
                showInfoBox &&
                <React.Fragment>
                    <InfoWindow
                        // onCloseClick={() => this.handleShowInfoBox(false)}
                    >
                        {/* <div className={classes.rootAlertListWrap} onClick={() => this.showEventDetail(id)}>
                            {
                                showIcon &&  
                                <span
                                    className="s--icon infoWindow"
                                    style={{backgroundColor: RISK_LEVEL_COLORS[risklevel]}}
                                >
                                    <i className={RISK_CATEGORY_ICONS[riskcategory]}></i>
                                </span>
                            }
                            <p className="alert--title">{ title }</p>
                        </div> */}
                        <p className="alert--title">{ title }</p>
                    </InfoWindow>                    
                </React.Fragment>
            }
            <CustomModal showModal={showModal} closeModal={this.handleCloseModal}>
                <div className="modal__inner">
                <ModalInner 
                    {...this.props}
                    eventDetail={eventDetail && eventDetail}
                    risklevel={risklevel} 
                    riskcategory={riskcategory} 
                    closeModal={this.handleCloseModal}
                />
                </div>
            </CustomModal> 
        </Marker>
    )
  }
}

export default CustomMarker;
