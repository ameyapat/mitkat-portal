export default {
  rootBizViewMap: {
    position: 'relative',
    '& .icon': {
      position: 'absolute',
      cursor: 'pointer',
      borderRadius: '50%',
      background: '#34495e',

      '& i': {
        color: 'var(--whiteMediumEmp)',
        fontSize: 16,
      },

      '& span': {
        width: 40,
        height: 40,
        display: 'block',
        textAlign: 'center',
        lineHeight: '40px',
      },

      '&.home-icon': {
        top: 20,
        right: 32,
      },
      '&.profile-icon': {
        top: 20,
        right: 32,
        // background: '#1098E0',
      },
      '&.queryIcon': {
        bottom: 32,
        right: 32,
        // background: '#1098E0',
      },
    },
  },
};
