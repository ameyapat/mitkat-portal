import React, { Component } from 'react';
//components
import { DatePicker } from 'material-ui-pickers';
import MultiSelect from '../../../ui/multiSelect';
import CustomModal from '../../../ui/modal';
import MapWithAMarker from './MapWithAMarker';
import Drawer from '@material-ui/core/Drawer';
//style
import style from './style';
import './style.scss';
//helpers
import moment from 'moment';
import injectSheet from 'react-jss';
import { fetchApi } from '../../../../helpers/http/fetch';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { withCookies } from 'react-cookie';
import { withStyles } from '@material-ui/core/styles';
import { dataArrOfObjects } from './utils';
import { Bar } from 'react-chartjs-2';
import MapToggle from '../../../ui/MapToggle/MapToggle';

const drawerWidth = 250;

const GRAPH_DATA = {
  labels: [],
  datasets: [
    {
      data: [],
      label: '# of Industry',
    },
  ],
};

const styles = theme => ({
  iconOptions: {
    display: 'flex',
    justifyContent: 'flex-end',
    padding: 3,
    minHeight: 40,
  },
  closed: {
    justifyContent: 'space-around',
  },
  chevIcons: {
    color: 'var(--whiteMediumEmp)',
  },
  menuIcon: {
    color: 'var(--whiteMediumEmp)',
  },
  rootDrawer: {},
  paperAnchorLeft: {
    zIndex: 996,
    position: 'absolute',
    // top: 44,
    overflowX: 'hidden',
    overflowY: 'hidden',
    // background: 'rgba(52, 73, 94, 0.7)'
    backgroundColor: 'var(--backgroundSolid)',
    '&:hover': {
      overflowY: 'scroll',
    },
    '&::-webkit-scrollbar': {
      width: 3,
    },
    '&::-webkit-scrollbar-track': {
      background: 'transparent',
    },
    '&::-webkit-scrollbar-thumb': {
      backgroundColor: '#ccc',
      borderRadius: 5,
    },
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 36,
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing.unit * 7 + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing.unit * 9 + 1,
    },
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
  },
  menuItemBtn: {
    padding: 6,
  },
  test: {
    background: 'none',
  },
});

@withStyles(styles, { withTheme: true })
@injectSheet(style)
@withCookies
@withRouter
@connect(state => {
  return {
    appState: state.appState,
  };
})
class BizViewMap extends Component {
  state = {
    industryDetails: [],
    clientStates: [],
    startDate: '2019-04-01',
    endDate: moment(new Date()).format('YYYY-MM-DD'),
    selectedClientStates: [],
    stateList: [],
    graphList: [],
    showDrawer: true,
    industryGraphData: [],
    showStats: false,
  };

  componentDidMount() {
    this.getClientStates();
    this.getIndustrySpecificPins();
    this.getStateList();
    this.getIndustryGraph();
  }

  getIndustrySpecificPins = () => {
    let { cookies } = this.props;
    let { startDate, endDate, selectedClientStates } = this.state;
    let authToken = cookies.get('authToken');

    let filterObj = {
      startdate: startDate,
      enddate: endDate,
      stateid:
        selectedClientStates.length > 0
          ? dataArrOfObjects(selectedClientStates)
          : [0],
    };
    let reqObj = {
      method: 'POST',
      isAuth: true,
      authToken,
      url: API_ROUTES.getIndustrySpecificEvents,
      data: filterObj,
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        this.setState({ industryDetails: data });
      })
      .catch(e => console.log(e));
  };

  getClientStates = () => {
    let { cookies } = this.props;

    let authToken = cookies.get('authToken');
    let reqObj = {
      url: `${API_ROUTES.getClientStates}`,
      method: 'POST',
      isAuth: true,
      authToken,
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        let { clientStates } = this.state;

        data.map(s => {
          let clientStateObj = {};
          clientStateObj['label'] = s.statename;
          clientStateObj['value'] = s.id;
          clientStates.push(clientStateObj);
        });

        this.setState({ clientStates }, () => console.log(this.state));
      })
      .catch(e => {
        console.log(e);
      });
  };

  getStateList = () => {
    let { cookies } = this.props;
    let { startDate, endDate, selectedClientStates } = this.state;
    let authToken = cookies.get('authToken');

    let filterObj = {
      startdate: startDate,
      enddate: endDate,
      stateid:
        selectedClientStates.length > 0
          ? dataArrOfObjects(selectedClientStates)
          : [0],
    };

    let reqObj = {
      url: `${API_ROUTES.getIndustryStateList}`,
      method: 'POST',
      isAuth: true,
      authToken,
      data: filterObj,
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        this.setState({ stateList: data });
      })
      .catch(e => {
        console.log(e);
      });
  };

  getIndustryGraph = () => {
    let { cookies } = this.props;
    let { startDate, endDate, selectedClientStates } = this.state;
    let authToken = cookies.get('authToken');

    let filterObj = {
      startdate: startDate,
      enddate: endDate,
      stateid:
        selectedClientStates.length > 0
          ? dataArrOfObjects(selectedClientStates)
          : [0],
    };

    let reqObj = {
      url: `${API_ROUTES.getIndustryGraph}`,
      method: 'POST',
      isAuth: true,
      authToken,
      data: filterObj,
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        //industryGraphData
        let graphNumbers = [];
        let tempNumbers = [];
        let tempLabels = [];

        data.map(g => {
          tempLabels.push(g.date);
          tempNumbers.push(g.totalnumber);
        });

        GRAPH_DATA['labels'] = [...tempLabels];
        graphNumbers = [...graphNumbers, ...tempNumbers];

        GRAPH_DATA['datasets'] = [
          {
            data: graphNumbers,
            label: '# of Industry',
            borderColor: 'rgba(201, 83, 46, 0.9)',
            backgroundColor: 'rgba(201, 83, 46, 0.6)',
          },
        ];
        // console.log(GRAPH_DATA, 'graph data');
        this.setState({ industryGraphData: GRAPH_DATA }, () =>
          console.log(this.state),
        );
      })
      .catch(e => {
        console.log(e);
      });
  };

  handleMultiSelect = (type, value) =>
    this.setState({ selectedClientStates: value });

  handleDateChange = (type, value) =>
    this.setState({ [type]: moment(value).format('YYYY-MM-DD') });

  gotoDashboard = () => this.props.history.push('/client/dashboard');

  applyFilter = () => {
    this.getIndustrySpecificPins();
    this.getStateList();
    this.getIndustryGraph();
  };

  getStateListXML = () => {
    let { stateList } = this.state;
    let stateListXML = [];

    return (stateListXML = stateList.map(s => {
      return (
        <div className="state-list-item">
          <div className="state--title">
            <p>{s.statename}</p>
          </div>
          <div className="state--number">
            <p>{s.number}</p>
          </div>
        </div>
      );
    }));
  };

  render() {
    let { classes } = this.props;
    let {
      startDate,
      endDate,
      selectedClientStates,
      industryDetails,
      clientStates,
      showDrawer,
      stateList,
      graphList,
      industryGraphData,
      showStats,
    } = this.state;

    return (
      <div>
        <MapToggle />
        <Drawer
          variant="permanent"
          classes={{
            root: classes.rootDrawer,
            paperAnchorLeft: classes.paperAnchorLeft,
            modal: classes.test,
          }}
          open={showDrawer}
          onClose={this.handleDrawerClose}
        >
          <div className="biz-actions">
            <div className="bizFiltersWrap">
              <div className="biz--filters">
                <div className="filters__header">
                  <h4>Filter By:</h4>
                  <button onClick={this.applyFilter}>Apply</button>
                </div>
                <DatePicker
                  label="Start Date"
                  value={startDate}
                  onChange={value => this.handleDateChange('startDate', value)}
                  animateYearScrolling
                  className="start--date"
                  fullWidth={true}
                />
                <DatePicker
                  label="End Date"
                  value={endDate}
                  onChange={value => this.handleDateChange('endDate', value)}
                  animateYearScrolling
                  fullWidth={true}
                  className="end--date"
                />
                <div className="statesWrap">
                  <label>Select States</label>
                  {clientStates.length > 0 && (
                    <MultiSelect
                      value={selectedClientStates}
                      placeholder={'States'}
                      parentEventHandler={value =>
                        this.handleMultiSelect('selectedClientStates', value)
                      }
                      options={clientStates}
                    />
                  )}
                </div>
              </div>
              <h5 className="biz--title">List Of States</h5>
              <div className="biz--states">
                {stateList.length > 0 && this.getStateListXML()}
              </div>
            </div>
          </div>
        </Drawer>
        <div className={classes.rootBizViewMap}>
          <MapWithAMarker
            googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyCYOTLyn6xbysArlvlW4zpxnH9p6H3An08&v=3.exp&libraries=geometry,drawing,places"
            loadingElement={<div style={{ height: `100%` }} />}
            containerElement={
              <div style={{ height: `100vh`, position: 'relative' }} />
            }
            mapElement={<div style={{ height: `100%` }} />}
            startDate={startDate}
            endDate={endDate}
            selectedClientStates={selectedClientStates}
            industryDetails={industryDetails}
          />

          {/* <div className="icon profile-icon" title="User Profile">
                        <span>
                            <i className="fas fa-user"></i>                        
                        </span>
                    </div> */}
          <div
            className="icon home-icon"
            title="Home"
            onClick={() => this.gotoDashboard()}
          >
            <span>
              <i className="fas fa-home"></i>
            </span>
          </div>
          {/* <div className="icon queryIcon" title="Ask Question">
                        <span>
                            <i className="fas fa-comment-alt"></i>
                        </span>
                    </div> */}
          <div className="biz--graph--wrap">
            <div className="statsWrap">
              <h5 className="biz--title stats">Industry Specific Statistics</h5>
              <button onClick={() => this.setState({ showStats: true })}>
                Expand
              </button>
            </div>
            {industryGraphData && (
              <Bar
                data={industryGraphData}
                width={100}
                height={50}
                options={{ maintainAspectRatio: true }}
              />
            )}
          </div>
        </div>
        <CustomModal
          showModal={showStats}
          // closeModal={this.handleCloseModal}
        >
          <div className="modal--inner--stats">
            <div className="biz--graph">
              {industryGraphData && (
                <Bar
                  data={industryGraphData}
                  width={100}
                  height={50}
                  options={{ maintainAspectRatio: true }}
                />
              )}
            </div>
            <span className="stats-close-button">
              <button onClick={() => this.setState({ showStats: false })}>
                &times;
              </button>
            </span>
          </div>
        </CustomModal>
      </div>
    );
  }
}

export default BizViewMap;
