import React, { Component } from 'react'
//components
import {
    withScriptjs,
    withGoogleMap,
    GoogleMap
  } from "react-google-maps";
import CustomMarker from './CustomMarker';
//helpers
import { connect } from 'react-redux';
import { withCookies } from 'react-cookie';
//assets
import whiteMarker from '../../../../assets/solid-pins/medium-march.png'
//data
import customMapStyles from '../../../../helpers/universalMapStyles.json';

@withCookies
@connect(state => {
    return {
        appState: state.appState,
        mapState: state.mapState
    }
})  
@withScriptjs
@withGoogleMap
class MapWithAMarker extends Component{  

    render(){

        let defaultOptions = {
            mapTypeControl: false,
            streetViewControl: false,
            zoomControl: false,
            fullscreenControl: false,
            styles: customMapStyles
        }

        let { industryDetails } = this.props;
        let CustomMarkerXML= [];

        if(industryDetails){
            CustomMarkerXML = industryDetails.length > 0 && industryDetails.map((list, index) => {
                
                let lat = parseFloat(list.latitude);
                let lng = parseFloat(list.longitude);

                return <CustomMarker
                        id={list.id}
                        key={index}
                        lat={lat}
                        lng={lng}
                        customIcon={whiteMarker}
                        title={list.title}
                />
            })

        }

        return(
            <GoogleMap
                defaultZoom={4.7}
                defaultOptions={defaultOptions}
                defaultCenter={{ lat: 20.5937, lng: 80.9629 }}
            >
                { CustomMarkerXML }                
            </GoogleMap>
        )
    }
}

export default MapWithAMarker;