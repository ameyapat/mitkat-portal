export default {
  rootRightDrawer: {},
  rootLabelLight: {
    color: 'var(--whiteHighEmp) !important',
  },
  rootInputLight: {
    color: 'var(--whiteHighEmp) !important',
  },
  notchedOutlineLight: {
    bordercolor: 'var(--whiteHighEmp) !important',
  },
  underline: {
    color: '#f5f5f5',
    '&::hover': {
      '&::before': {
        borderBottom: '2px solid #f5f5f5',
      },
    },
    '&::before': {
      borderBottom: '2px solid #f5f5f5',
    },
    '&::after': {
      borderBottom: '2px solid #f5f5f5',
    },
  },
  btnSubmit: {
    margin: 0,
    marginRight: 15,
    border: '1px solid var(--darkActive)',
    background: 'transparent',
    textTransform: 'uppercase',
    padding: 8,
    color: 'var(--darkActive)',
    cursor: 'pointer',

    '&:disabled': {
      opacity: '0.4',
      cursor: 'not-allowed',
    },
  },
  rightDrawPaper: {
    minWidth: 240,
    maxWidth: 320,
    background: 'rgba(0, 0, 0, 0.5)',
    color: 'var(--whiteMediumEmp)',
  },
  rootSnackBar: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  anchorOriginBottomLeft: {
    // bottom: 200
  },
  rootAdvisory: {
    backgroundColor: 'var(--backgroundSolid)',
    height: '100vh',
    // position: 'relative',
    overflow: 'auto',
    '& .close-times': {
      cursor: 'pointer',
      color: 'var(--whiteMediumEmp)',
      position: 'absolute',
      top: 20,
      right: 30,
      fontSize: '24px !important',
      borderRadius: 3,
      padding: '5px 10px',

      '&:hover': {
        background: 'rgba(31, 31, 31, 1)',
      },
    },
  },
  riskMapImg: {
    padding: 50,
    width: '100%',
    height: '100%',
    maxWidth: '100%',
    objectFit: 'contain',
  },
  rootBackdrop: {
    backgroundColor: 'rgba(0,0,0,0.8) !important',
  },
  dateWrap: {
    display: 'flex',
  },
};
