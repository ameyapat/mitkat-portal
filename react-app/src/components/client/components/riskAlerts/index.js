import React, { Component, Fragment } from 'react';
import './style.scss';
import style from './style';
//components
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import TextField from '@material-ui/core/TextField';
import CustomModal from '../../../ui/modal';
import IsEmpty from '../../../ui/isEmpty';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import ModalInnerSms from '../../../ui/modal/ModalInnerSms';
import ListItem from '../../../ui/listItem';
import CustomCalendar from '../../../ui/calendar';
import SubscribeBox from '../../../ui/subscribeBox';
import QuerySystem from '../querySystem';
import Advisories from '../advisories';
import TravelAdvisories from '../travelAdvisories';
import CustomDatePicker from '../../../ui/customDatePicker';
//helpers
import injectSheet from 'react-jss';
import { Spring } from 'react-spring/renderprops';
import moment from 'moment';
import { hasData, hasNewObj } from '../../../../helpers/utils';
import { saveAs } from 'file-saver';
import {
  RISK_CATEGORY_ICONS,
  RISK_LEVEL_COLORS,
} from '../../../../helpers/constants';
import { getCalendarObj } from './utils';
import { Link } from 'react-router-dom';
//api
import { fetchApi } from '../../../../helpers/http/fetch';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import { toggleToast } from '../../../../actions';
import { connect } from 'react-redux';
import { withCookies } from 'react-cookie';
import { withRouter } from 'react-router-dom';
import ModalInner from '../../../ui/modal/ModalInner';
import EventFilters from '../filters/EventFilters';
import store from '../../../../store';
import {
  resetEventFilters,
  resetPageNo,
  handlePageNo,
} from '../../../../actions';
import {
  getClientTrackingId,
  downloadArchives,
} from '../../../../requestor/mitkatWeb/requestor';
import notifyBellIcon from '../../../../assets/icons/notification-bell.png';

import { showEventDetail } from '../../../../api/api';
import NavList from './NavList';
import * as actions from '../../../../actions';
import Empty from '../../../ui/empty';
import DailyRiskEventsTracker from '../dailyRiskEventsTracker';

@withCookies
@injectSheet(style)
@withRouter
@connect(state => {
  return {
    appState: state.appState,
    eventArchiveState: state.eventArchiveState,
    appPermissions: state.appState.appPermissions,
    themeChange: state.themeChange,
  };
})
class RiskAlerts extends Component {
  state = {
    showSmsLabel: true,
    showNotifyLabel: true,
    right: false,
    notifyRight: false,
    smsAlertsDetails: [],
    notificationAlertDetails: [],
    hasNewAlert: false,
    hasNewNotify: false,
    eventDetail: null,
    eventDetails: null,
    openEventModal: false,
    eventArchives: [],
    showArchiveEvent: false,
    showNotifyModal: false,
    pageNo: 0,
    totalEvents: null,
    isLast: false,
    showCalendarModal: false,
    calendarEvents: [],
    showCalEventDetails: false,
    showSubscribtionBox: false,
    showQueryBox: false,
    showAlertBox: false,
    showRiskMap: false,
    showTravelAdvisories: false,
    travelAdvisoriesSubscribed: false,
    totalEventsNo: 0,
    showEventArchives: true,
    trackingId: '',
    month: moment(new Date()).format('M'),
    year: moment(new Date()).format('YYYY'),
    selectedItem: 'home',
    dailyRiskList: [],
  };

  componentDidMount() {
    this.setState({ pageNo: this.props.eventArchiveState.pageNo });
    // this.getTotalEvents();
    // this.getSmsAlerts();
    // this.getNotificationAlerts();
    // this.getCalendarEvents();
    // this.checkTravelSub();
    setTimeout(
      () => this.setState({ showSmsLabel: false, showNotifyLabel: false }),
      8000,
    );
  }

  componentDidUpdate(prevProps, prevState) {
    let prevPageNo = prevProps.eventArchiveState.pageNo;
    let currPageNo = this.props.eventArchiveState.pageNo;
    if (prevPageNo !== currPageNo) {
      this.setState({ pageNo: currPageNo });
      this.getSelectedValue();
    }
  }

  getSelectedValue = () => {
    this.setState({
      selectedItem: localStorage.getItem('selectedOption') || 'home',
    });
  };

  checkTravelSub = () => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');

    let reqObj = {
      url: API_ROUTES.checkTravelAdSub,
      method: 'POST',
      isAuth: true,
      authToken,
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        if (data.access) {
          this.setState({ travelAdvisoriesSubscribed: true });
        }
      })
      .catch(e => {
        console.log(e);
      });
  };

  toggleDrawer = (direction, show) => {
    this.setState({ [direction]: show });
  };

  getCalendarEvents = () => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');

    let reqObj = {
      url: API_ROUTES.getCalendarEvents,
      method: 'POST',
      isAuth: true,
      authToken,
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        let { calendarEvents } = this.state;
        if (data) {
          calendarEvents = getCalendarObj(data);
          this.setState({ calendarEvents }, () => console.log(this.state));
        }
      })
      .catch(e => {
        console.log(e);
      });
  };

  getSmsAlerts = () => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');

    let reqObj = {
      url: API_ROUTES.getSmsAlerts,
      method: 'POST',
      isAuth: true,
      authToken,
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        let { smsAlertsDetails } = this.state;
        if (hasData(smsAlertsDetails)) {
          let hasNew = hasNewObj(data, smsAlertsDetails);
          if (hasNew) {
            this.setState({ hasNewAlert: true });
            return data;
          } else {
            return smsAlertsDetails;
          }
        } else {
          return data;
        }
      })
      .then(data => {
        this.setState({ smsAlertsDetails: data });
      });
  };

  openAlertModal = (id, riskcategory, risklevel, type) => {
    this.setState(
      { right: false, notifyRight: false, riskcategory, risklevel },
      () => {
        if (type === 'notification') {
          this.getNotificationDetails(id);
        } else {
          this.getEventDetails(id);
        }
      },
    );
  };

  getEventDetails = eventId => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');

    let reqObj = {
      url: `${API_ROUTES.showRiskAlertDetails}/${eventId}`,
      method: 'POST',
      isAuth: true,
      authToken,
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        this.setState({ eventDetail: data, showAlertModal: true });
      })
      .catch(e => {
        console.log(e);
      });
  };

  getNotificationAlerts = () => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');

    let reqObj = {
      url: API_ROUTES.getNotificationAlerts,
      method: 'POST',
      isAuth: true,
      authToken,
      showToggle: true,
    };

    fetchApi(reqObj).then(data => {
      this.setState({ notificationAlertDetails: data });
    });
  };

  getNotificationDetails = eventId => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');

    let reqObj = {
      url: `${API_ROUTES.getEventDetails}/${eventId}`,
      method: 'POST',
      isAuth: true,
      authToken,
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        this.setState({ eventDetail: data, showNotifyModal: true });
      })
      .catch(e => {
        console.log(e);
      });
  };

  getCalEventDetails = eventId => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');

    let reqObj = {
      url: `${API_ROUTES.getEventDetails}/${eventId}`,
      method: 'POST',
      isAuth: true,
      authToken,
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        this.setState({ eventDetail: data });
      })
      .catch(e => {
        console.log(e);
      });
  };

  getAlertXML = () => {
    let { smsAlertsDetails } = this.state;
    let smsAlertXML = [];

    smsAlertXML =
      smsAlertsDetails &&
      smsAlertsDetails.map((item, index) => {
        return (
          <li
            key={index}
            onClick={() =>
              this.openAlertModal(item.id, item.riskcategory, item.risklevel)
            }
          >
            <div>
              <h2>{item.title}</h2>
              <p>{item.timestamp}</p>
            </div>
          </li>
        );
      });

    return smsAlertXML;
  };

  getNotificationXML = () => {
    let { notificationAlertDetails } = this.state;
    let notifyAlertXML = [];

    notifyAlertXML =
      notificationAlertDetails &&
      notificationAlertDetails.map((item, index) => {
        return (
          <li
            key={index}
            onClick={() =>
              this.openAlertModal(
                item.id,
                item.riskcategory,
                item.risklevel,
                'notification',
              )
            }
          >
            <div>
              <h2>{item.title}</h2>
              <p>{item.timestamp}</p>
            </div>
          </li>
        );
      });

    return notifyAlertXML;
  };

  handleCloseModal = () => {
    this.setState({ showAlertModal: false });
  };

  handleCloseModals = (type, show) => {
    this.setState({ [type]: show, showCalEventDetails: false });
  };

  toggleSubscribtions = show =>
    this.setState({ showSubscribtionBox1: show, showAlertBox: show });

  closeEventModal = () => {
    this.setState(
      {
        openEventModal: false,
        eventArchives: [],
        isLast: false,
      },
      () => {
        store.dispatch(resetPageNo());
        store.dispatch(resetEventFilters());
      },
    );
  };

  handleHideAlert = () => {
    this.setState({ hasNewAlert: false });
  };

  //get all event details.
  loadEventArchivesModal = () => {
    let {
      cookies,
      eventArchiveState: { archiveFilters },
    } = this.props;
    let { pageNo } = this.state;
    let authToken = cookies.get('authToken');

    let dataObj = {
      risklevel: archiveFilters.risklevel,
      riskcategory: archiveFilters.riskcategory,
      countries: archiveFilters.countries || null,
      states: archiveFilters.states || null,
      cities: archiveFilters.cities || null,
      startdate: archiveFilters.startDate,
      enddate: archiveFilters.endDate,
    };

    let reqObj = {
      isAuth: true,
      authToken,
      method: 'POST',
      url: `${API_ROUTES.getEventArchives}/${pageNo}`,
      data: dataObj,
      showToggle: true,
    };

    fetchApi(reqObj).then(data => {
      let { pageNo } = this.state;
      let newPageNo = pageNo <= data.totalPages ? pageNo + 1 : pageNo;
      this.setState(
        {
          openEventModal: true,
          eventArchives: [...this.state.eventArchives, ...data.content],
          totalEvents: data.totalElements,
          totalPages: data.totalPages,
          isLast: data.last,
        },
        () => {
          store.dispatch(handlePageNo(newPageNo));
        },
      );
    });
  };

  viewArchiveDetails = (eventId, riskcategory, risklevel) => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let reqObj = {
      url: `${API_ROUTES.getEventDetails}/${eventId}`,
      isAuth: true,
      authToken,
      method: 'POST',
      showToggle: true,
    };

    fetchApi(reqObj).then(data => {
      this.setState({
        showArchiveEvent: true,
        riskcategory: riskcategory,
        risklevel: risklevel,
        eventDetail: data,
      });
    });
  };

  hideArchiveDetails = () => {
    this.setState({ showArchiveEvent: false });
  };

  fetchEventArchives = () => {
    let { eventArchives } = this.state;
    let eventArchivesXML = [];

    eventArchivesXML = eventArchives.map((event, index) => {
      let icon = RISK_CATEGORY_ICONS[event.riskcategory];
      let iconColor = RISK_LEVEL_COLORS[event.risklevel];

      return (
        <ListItem
          key={index}
          id={event.id}
          icon={icon}
          riskcategory={event.riskcategory}
          risklevel={event.risklevel}
          iconColor={iconColor}
          title={event.title}
          parentHandler={this.viewArchiveDetails}
        />
      );
    });

    return eventArchivesXML;
  };

  handleFilteredData = filteredEventArchives => {
    this.setState({
      eventArchives: [...filteredEventArchives.content],
      totalEvents: filteredEventArchives.totalElements,
      totalPages: filteredEventArchives.totalPages,
      isLast: filteredEventArchives.last,
    });
  };

  handleLoadMore = () => {
    this.loadEventArchivesModal();
  };

  loadCalendar = () => {
    this.setState({ showCalendarModal: true });
  };

  handleShowEvent = eventId => {
    this.getCalEventDetails(eventId);
    this.setState({ showCalEventDetails: true });
  };

  handleBizView = () => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');

    let reqObj = {
      url: API_ROUTES.getIndustrySpecificView,
      method: 'POST',
      isAuth: true,
      authToken,
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        if (data.access) {
          this.props.history.push('/client/businessView');
        } else {
          this.setState({ showSubscribtionBox: true });
        }
      })
      .catch(e => console.log(e));
  };

  handleQueries = () => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');

    let reqObj = {
      url: API_ROUTES.validateUserSubscription,
      method: 'POST',
      isAuth: true,
      authToken,
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        if (data.access) {
          this.setState({
            showQueryBox: true,
            showAlertBox: false,
            showSubscribtionBox1: true,
          });
        } else {
          this.setState({
            showQueryBox: false,
            showAlertBox: true,
            showSubscribtionBox1: true,
          });
        }
      })
      .catch(e => console.log(e));
  };

  getTotalEvents = () => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    const reqObj = {
      method: 'GET',
      isAuth: true,
      authToken: authToken,
      url: `${API_ROUTES.getTotalEvents}`,
      showToggle: true,
    };

    fetchApi(reqObj).then(totalEventsNo => {
      this.setState({ totalEventsNo });
    });
  };

  toggleEventArchives = show => {
    this.setState({ openEventArchives: show });
  };

  handleInputChange = e => {
    this.setState({
      trackingId: e.target.value,
    });
  };

  editByTrackingId = () => {
    getClientTrackingId(this.state.trackingId)
      .then(data => {
        if (data && data.status === 502) {
          let toastObj = {
            toastMsg: data.message,
            showToast: true,
          };
          store.dispatch(toggleToast(toastObj));
          return;
        }
        this.setState(
          {
            dailyRiskList: data,
          },
          () => {
            this.handleToggleEventModal(true);
          },
        );
      })
      .catch(e => {
        let toastObj = {
          toastMsg: 'Something Went Wrong!',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      });
  };

  handleDownloadArchives = () => {
    const { month, year } = this.state;

    const reqData = {
      month: moment(month).format('M'),
      year: moment(year).format('YYYY'),
    };

    downloadArchives(reqData)
      .then(blobObj => {
        let objUrl = new Blob([blobObj], {
          type:
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8',
        });
        saveAs(objUrl, 'event-details.xlsx');

        let toastObj = {
          toastMsg: 'Downloading file...',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      })
      .catch(e => {
        let toastObj = {
          toastMsg: 'Error downloading excel sheet',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      });
  };

  setYear = currentYear => {
    this.setState({
      year: currentYear,
    });
  };

  setMonth = currentMonth => {
    this.setState({
      month: currentMonth,
    });
  };

  handleToggleEventModal = showEventModal => {
    this.setState({ showEventModal });
  };

  showDetails = eventId => {
    showEventDetail(eventId).then(response => {
      this.setState({ eventDetail: response });
    });
  };

  handleOpenModal = type => {
    this.props.handleDrawer();
    if (type === 'loadEventArchivesModal') {
      this.toggleEventArchives(true);
    }
    if (type === 'upcomingEvents') {
      this.loadCalendar();
    }
    if (type === 'resources') {
      this.setState({ showAdvisories: true });
    }
    if (type === 'cityBrief') {
      this.setState({ showTravelAdvisories: true });
    }
    if (type === 'askQuestion') {
      this.handleQueries();
    }
    if (type === 'rimeEventArchivesModal') {
      this.props.dispatch(actions.toggleRimeEventArchives(true));
    }
  };

  handleRoute = (to, type) => {
    this.props.handleDrawer();
    let path = this.props.match.url;
    this.setState({ selectedItem: type });
    localStorage.setItem('selectedOption', type);
    this.props.history.push(`${path}${to}`);
  };

  render() {
    let {
      classes,
      themeChange: { setDarkTheme },
    } = this.props;

    let {
      showSmsLabel,
      smsAlertsDetails,
      showAlertModal,
      hasNewAlert,
      eventDetail,
      eventDetails,
      risklevel,
      riskcategory,
      eventArchives,
      openEventModal,
      showNotifyLabel,
      notificationAlertDetails,
      showArchiveEvent,
      hasNewNotify,
      showNotifyModal,
      totalEvents,
      showCalendarModal,
      calendarEvents,
      showCalEventDetails,
      showSubscribtionBox,
      showAlertBox,
      showQueryBox,
      totalEventsNo,
      showEventArchives,
      trackingId,
      month,
      year,
      showEventModal,
      dailyRiskList,
    } = this.state;

    return (
      <div className={`riskAlerts ${setDarkTheme ? 'dark' : 'light'}`}>
        <NavList
          handleOpenModal={this.handleOpenModal}
          handleRoute={this.handleRoute}
          selectedItem={this.state.selectedItem}
        />
        {/* Handle Modal */}
        {/* Event Archives */}
        {false && (
          <CustomModal
            showModal={openEventModal}
            closeModal={this.closeEventModal}
            BackdropProps={{
              className: classes.rootBackdrop,
            }}
          >
            <div
              id="eventArchivesModal"
              className={`modal__inner ${setDarkTheme ? 'dark' : 'light'}`}
            >
              <div className="eventArchivesModal_header">
                <h1>
                  Total Number of Events Covered{' '}
                  <span className="eventArchivesModal_Noe">
                    ({totalEventsNo})
                  </span>
                </h1>
                <button
                  className="btn--close"
                  onClick={() => this.closeEventModal()}
                >
                  &times;
                </button>
              </div>
              {!showArchiveEvent && (
                <div className="eventArchiveFilters">
                  <EventFilters
                    pageNo={this.state.pageNo}
                    resetPageNo={this.resetPageNo}
                    handleFilteredData={this.handleFilteredData}
                    closeEventModal={this.closeEventModal}
                  />
                </div>
              )}
            </div>
          </CustomModal>
        )}

        {showEventArchives && (
          <div className="event__archives">
            <CustomModal
              showModal={this.state.openEventArchives}
              closeModal={this.closeEventModal}
              BackdropProps={{
                className: classes.rootBackdrop,
              }}
            >
              <div
                id="eventArchivesModal"
                className={`modal__inner ${setDarkTheme ? 'dark' : 'light'}`}
              >
                <div className="eventArchivesModal_header">
                  <h1>Event Archives</h1>
                  <button
                    className="btn--close"
                    onClick={() => this.toggleEventArchives(false)}
                  >
                    &times;
                  </button>
                </div>
                <div className="eventArchivesModal_body">
                  <div className={classes.dateWrap}>
                    <div className="alertWrap">
                      <h2>
                        <i className="fas fa-search" /> Search by Tracking Id
                      </h2>
                      <div className={classes.datesWrapper}>
                        <TextField
                          // error={validation[input.id].isInvalid}
                          id={'trackingId'}
                          label={'Tracking Id'}
                          fullWidth
                          type={'text'}
                          InputLabelProps={{
                            shrink: true,
                            classes: {
                              root: classes.rootLabelLight,
                            },
                          }}
                          InputProps={{
                            classes: {
                              root: classes.rootText,
                              input: classes.rootInputLight,
                              notchedOutline: classes.notchedOutline,
                              underline: classes.underline,
                            },
                            endAdornment: null,
                          }}
                          value={trackingId}
                          onChange={this.handleInputChange}
                          margin="normal"
                        />
                      </div>
                      <div className="downloadBtnWrap">
                        <button
                          disabled={!trackingId}
                          className={classes.btnSubmit}
                          onClick={() => this.editByTrackingId()}
                        >
                          Search
                        </button>
                      </div>
                    </div>
                    <div className="dividerVerticle">
                      <span> OR </span>{' '}
                    </div>
                    <div className="dateWrap">
                      <h2>
                        <i className="fas fa-download" /> Download events by
                        month
                      </h2>
                      <div>
                        <div className="dateWrapper">
                          <CustomDatePicker
                            label={'Enter Month'}
                            value={month}
                            defaultValue={month}
                            type={'month'}
                            handleDateChange={(type, value) =>
                              this.setMonth(value)
                            }
                            views={['month']}
                            inputLabelClasses={{
                              root: classes.rootLabelLight,
                            }}
                            inputPropsClasses={{
                              root: classes.rootInputLight,
                              input: classes.rootInputLight,
                              notchedOutline: classes.notchedOutlineLight,
                              underline: classes.underline,
                            }}
                            format="MMMM"
                          />
                        </div>
                        <div className="dateWrapper">
                          <CustomDatePicker
                            label={'Enter Year'}
                            value={year}
                            type={'year'}
                            handleDateChange={(type, value) =>
                              this.setYear(value)
                            }
                            views={['year']}
                            inputLabelClasses={{
                              root: classes.rootLabelLight,
                            }}
                            inputPropsClasses={{
                              root: classes.rootInputLight,
                              input: classes.rootInputLight,
                              notchedOutline: classes.notchedOutlineLight,
                              underline: classes.underline,
                            }}
                          />
                        </div>
                      </div>
                      <div className="downloadBtnWrap">
                        <button
                          disabled={!month && !year}
                          className={classes.btnSubmit}
                          onClick={() => this.handleDownloadArchives()}
                        >
                          Download
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="eventArchivesModal_footer">
                  <p>
                    Note: Events applicable as per subscription will be
                    downloaded.
                  </p>
                </div>
              </div>
            </CustomModal>
          </div>
        )}
        {/* Event Archives end */}

        {/* Upcoming Events */}
        <CustomModal
          showModal={showCalendarModal}
          closeModal={() => this.handleCloseModals('showCalendarModal', false)}
        >
          <div className={`calendar__inner ${setDarkTheme ? 'dark' : 'light'}`}>
            <div className="calendar__header">
              <span className="cal--heading">Upcoming Events</span>
              {showCalEventDetails && (
                <button
                  className="calendar--back"
                  onClick={() => this.setState({ showCalEventDetails: false })}
                >
                  Back to Calendar
                </button>
              )}
              <span
                className="calendar--close"
                onClick={() =>
                  this.handleCloseModals('showCalendarModal', false)
                }
              >
                &times;
              </span>
            </div>
            <div className="calendar__body">
              {!showCalEventDetails ? (
                <CustomCalendar
                  events={calendarEvents.length > 0 ? calendarEvents : []}
                  getCalendarEvents={this.getCalendarEvents}
                  parentEventHandler={e => {
                    this.handleShowEvent(e.event.id);
                  }}
                />
              ) : (
                <Fragment>
                  {eventDetail && Object.values(eventDetail).length > 0 && (
                    <ModalInner
                      {...this.props}
                      meta={this.state}
                      eventDetail={eventDetail}
                      riskcategory={eventDetail && eventDetail.riskcategory}
                      risklevel={eventDetail && eventDetail.risklevel}
                      showDetails={this.showDetails}
                      showClose={false}
                      closeModal={() =>
                        this.handleCloseModals('showNotifyModal', false)
                      }
                    />
                  )}
                </Fragment>
              )}
            </div>
          </div>
        </CustomModal>

        {/* Upcoming Events End */}

        {/* Resources */}
        <div className="querySystemWrap">
          <CustomModal
            showModal={this.state.showAdvisories}
            BackdropProps={{
              className: classes.rootBackdrop,
            }}
            closeModal={() => this.handleCloseModals('showAdvisories', false)}
          >
            {/* <div className={classes.rootAdvisory}>
              <span
                className="close-times"
                onClick={() => this.handleCloseModals('showAdvisories', false)}
              >
                &times;
              </span>
              <Advisories />
            </div>  */}
            <div className="advisory__inner">
              <div className="calendar__header">
                <span className="cal--heading">Resources</span>
                <span
                  className="calendar--close"
                  onClick={() =>
                    this.handleCloseModals('showAdvisories', false)
                  }
                >
                  &times;
                </span>
              </div>
              <div className="calendar__body">
                <Advisories />
              </div>
            </div>
          </CustomModal>
        </div>
        {/* Resources End */}

        {/* cityBrief */}
        <CustomModal
          showModal={this.state.showTravelAdvisories}
          BackdropProps={{
            className: classes.rootBackdrop,
          }}
          closeModal={() =>
            this.handleCloseModals('showTravelAdvisories', false)
          }
        >
          <div className={classes.rootAdvisory}>
            <span
              className="close-times"
              onClick={() =>
                this.handleCloseModals('showTravelAdvisories', false)
              }
            >
              &times;
            </span>
            <TravelAdvisories />
          </div>
        </CustomModal>
        {/* cityBrief End */}

        {/* askQuestion  */}
        <CustomModal
          showModal={this.state.showSubscribtionBox1}
          BackdropProps={{
            className: classes.rootBackdrop,
          }}
          closeModal={() =>
            this.handleCloseModals('showSubscribtionBox1', false)
          }
        >
          <Fragment>
            {showAlertBox && (
              <div className="subscribeBoxWrap">
                <SubscribeBox
                  handleCloseModals={() =>
                    this.handleCloseModals('showSubscribtionBox1', false)
                  }
                />
              </div>
            )}
            {showQueryBox && (
              <QuerySystem
                handleCloseModals={() =>
                  this.handleCloseModals('showSubscribtionBox1', false)
                }
              />
            )}
          </Fragment>
        </CustomModal>
        {/* askQuestion End */}

        <SwipeableDrawer
          anchor="right"
          open={this.state.notifyRight}
          onOpen={() => {}}
          classes={{
            root: classes.rootRightDrawer,
            paper: classes.rightDrawPaper,
          }}
          onClose={() => this.toggleDrawer('notifyRight', false)}
        >
          <div>
            <div className="alert__title__wrap">
              <h2>
                <span className="a--icon">
                  <img
                    src={notifyBellIcon}
                    alt="notification-bell"
                    className="icons--riskAlerts"
                  />
                </span>
                Notifications
              </h2>
            </div>
            <div className="alert__listing">
              <ul>
                {notificationAlertDetails &&
                  notificationAlertDetails.length > 0 &&
                  this.getNotificationXML()}
              </ul>
            </div>
          </div>
        </SwipeableDrawer>
        <SwipeableDrawer
          anchor="right"
          open={this.state.right}
          onOpen={() => {}}
          classes={{
            root: classes.rootRightDrawer,
            paper: classes.rightDrawPaper,
          }}
          onClose={() => this.toggleDrawer('right', false)}
        >
          <div>
            <div className="alert__title__wrap">
              <h2>
                <span className="a--icon">
                  <i className="icon-uniE972" />
                </span>
                Sms Alerts
              </h2>
            </div>
            <div className="alert__listing">
              <ul>
                {smsAlertsDetails &&
                  smsAlertsDetails.length > 0 &&
                  this.getAlertXML()}
              </ul>
            </div>
          </div>
        </SwipeableDrawer>
        <CustomModal
          showModal={showAlertModal}
          BackdropProps={{
            className: classes.rootBackdrop,
          }}
          closeModal={this.handleCloseModal}
        >
          <div className={`modal__inner ${setDarkTheme ? 'dark' : 'light'}`}>
            <ModalInnerSms
              {...this.props}
              eventDetail={eventDetail}
              closeModal={this.handleCloseModal}
            />
          </div>
        </CustomModal>
        {/** notification details modal below */}
        <CustomModal
          showModal={showNotifyModal}
          BackdropProps={{
            className: classes.rootBackdrop,
          }}
          closeModal={() => this.handleCloseModals('showNotifyModal', false)}
        >
          <div className={`modal__inner ${setDarkTheme ? 'dark' : 'light'}`}>
            <ModalInner
              {...this.props}
              eventDetail={eventDetail}
              riskcategory={riskcategory}
              risklevel={risklevel}
              closeModal={() =>
                this.handleCloseModals('showNotifyModal', false)
              }
            />
          </div>
        </CustomModal>
        <CustomModal
          showModal={showEventModal}
          closeModal={this.handleToggleEventModal}
        >
          <div className={`modal__inner ${setDarkTheme ? 'dark' : 'light'}`}>
            <DailyRiskEventsTracker
              items={dailyRiskList}
              trackingId={this.state.trackingId}
              closeModal={this.handleToggleEventModal}
            />
          </div>
        </CustomModal>
        <Snackbar
          style={{ bottom: 120 }}
          classes={{
            root: classes.rootSnackBar,
            anchorOriginBottomLeft: classes.anchorOriginBottomLeft,
          }}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={this.state.hasNewAlert}
          onClose={this.handleHideAlert}
          message={'New Alerts Available!'}
          action={[
            <IconButton
              key="close"
              aria-label="Close"
              onClick={this.handleHideAlert}
            >
              <CloseIcon style={{ color: '#fff' }} />
            </IconButton>,
          ]}
        />
      </div>
    );
  }
}

export default RiskAlerts;
