import { RISK_LEVEL_COLORS } from '../../../../helpers/constants';

export const getCalendarObj = (data) => {
    let calendarArr = [];
    
    data.map(c => {   
        let date = c.calenderdate;
        c.eventlist.map(e => {
            let calendarObj = {};
            calendarObj['date'] = date;
            calendarObj['title'] = e.title;
            calendarObj['id'] = e.id;
            calendarObj['backgroundColor'] = RISK_LEVEL_COLORS[e.risklevel];
            calendarArr.push(calendarObj);
        })
    })

    return calendarArr;
}