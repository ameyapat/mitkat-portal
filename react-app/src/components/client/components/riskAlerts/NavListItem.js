import { Tooltip } from '@material-ui/core';
import React from 'react';
import './style.scss';

const NavListItem = ({ title, icon, type, parentHandler, isSelected }) => {
  return (
    <Tooltip title={title}>
      <li
        className={`${isSelected ? 'active' : null}`}
        onClick={e => {
          e.stopPropagation();
          parentHandler(type);
        }}
      >
        <p>
          <span className={`${isSelected ? 'list__iconActive' : 'list__icon'}`}>
            <i className={icon}></i>
          </span>
          {title}
        </p>
      </li>
    </Tooltip>
  );
};

export default NavListItem;
