import React from 'react';
//scss
import './style.scss';

const TopEventItem = ({eventId = "", topEventHandler}) => (
    <div className="eventItem" onClick={() => topEventHandler(eventId)}>
        <div className="eventItem__details">
            <div className="img--wrap">img</div>
            <div className="info--wrap">
                <p>some event text</p>
            </div>
        </div>
        <div className="eventItem__timeStamp">
            <label>time stamp</label>
            <span>READ</span>
        </div>
    </div>
)

export default TopEventItem;