// export default UserManagement;
import React, { Component } from 'react';
//components @material-ui
import Modal from '@material-ui/core/Modal';
import { connect } from 'react-redux';
import { withCookies } from 'react-cookie';
import './style.scss';
import { getClientLocationList } from '../../../../requestor/mitkatWeb/requestor';
import ClientLocationTable from '../../../ui/tableLocation';
import AddClientLocation from '../../../ui/addClientLocation';

@withCookies
@connect(state => {
  return {
    appState: state.appState,
  };
})
class LocationManagement extends Component {
  state = {
    showCreateUserModal: false,
    showUserDetails: false,
    locationList: [],
    calcHeight: '100%',
  };

  componentDidMount = () => {
    this.getLocationList();
  };

  getLocationList = () => {
    getClientLocationList().then(data => {
      this.setState({ locationList: data });
    });
  };

  handleModal = () => {
    this.setState({ showCreateUserModal: !this.state.showCreateUserModal });
  };

  render() {
    let { locationList } = this.state;

    return (
      <>
        <h1 className="rootHeader">Location Management</h1>
        <div className="userLocationManagement">
          <div className="more--options">
            <span>
              <i className="fas fa-users"></i>
              <label>Location List</label>
            </span>
            <button className="btn" onClick={this.handleModal}>
              <span className="icon-wrap">
                <i className="fas fa-user-plus" style={{ color: 'white' }}></i>
              </span>
              <span className="btn--create">
                Add Location
              </span>
            </button>
          </div>
          <div
            className="user--list-wrap"
            style={{ height: this.state.calcHeight }}
          >
            {
              <ClientLocationTable
                tableData={locationList}
                showViewDetails={false}
                getUserList={this.getLocationList}
              />
            }
          </div>
          <Modal
            open={this.state.showCreateUserModal}
          >
            <div className="modal-paper">
              <AddClientLocation
                getLocationList={this.getLocationList}
                handleCloseModal={this.handleModal}
              />
            </div>
          </Modal>
        </div>
      </>
    );
  }
}

export default LocationManagement;
