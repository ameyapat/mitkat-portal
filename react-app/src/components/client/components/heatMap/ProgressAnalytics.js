import React, { Component } from 'react';
import Item from './Item';
//helpers
import injectSheet from 'react-jss';
//style
import styles from './styles';

@injectSheet(styles)
class ProgressAnalytics extends Component{

    getProgressXML = () => {
        let { riskCatData } = this.props;
        let riskCatDataXML = [];

        for(let k in riskCatData){
            riskCatDataXML.push(<Item key={k} type={k} value={riskCatData[k]} />)
        }

        return riskCatDataXML;
    }

    render(){
        let { classes, riskCatData } = this.props;
        return(
            <div className={classes.rootProgressAnalytics}>
                <label className="label">[ Risk Category ]</label>
                <div className={classes.progressItemsWrap}>
                    {
                        riskCatData && this.getProgressXML()
                    }
                </div>
            </div>
        )
    }
}

export default ProgressAnalytics;