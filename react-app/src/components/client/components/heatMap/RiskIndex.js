import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
//helpers
import injectSheet from 'react-jss';
//style
import styles from './styles';

const colorConst = {
    riskIndexColor: 'rgba(255, 220, 52, 0.8)',
}

const RiskIndex = ({classes, value, type}) => {    

    return(
        <div className={classes.riskIndex}>        
            <label>[ Risk Index ]</label>
            <p className={classes.riskValue}>{value}%</p>
            <span className={classes.pBarOuter}>
                <span 
                    style={{
                        background: colorConst.riskIndexColor, 
                        width: `${value}%`, 
                        boxShadow: `0 0 5px 0 ${colorConst.riskIndexColor}` 
                    }} 
                    className={classes.pBarInner}
                >
                </span>
            </span>
            {/* <CircularProgress 
                className={classes.progress} 
                variant="indeterminate" 
                value={value}
                classes={{
                    circle: classes.circleColor
                }}
                // color="red"
             /> */}
        </div>
    )
}

export default injectSheet(styles)(RiskIndex);
