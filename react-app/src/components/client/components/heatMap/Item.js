import React from 'react';
//helpers
import injectSheet from 'react-jss';
//style
import styles from './styles';

const colorConst = {
    technology: 'rgba(35, 205, 152, 0.8)',
    terrorism: 'rgba(75, 178, 231, 0.8)',
    civilDisturbances: 'rgba(235, 135, 203, 0.8)',
    environment: 'rgba(127, 251, 255, 0.8)',
    health: 'rgba(176, 122, 251, 0.8)',
    crime: 'rgba(176, 122, 251, 0.8)',                    
    infrastructure: 'rgba(255, 220, 52, 0.8)'
}

const Item = ({classes, value, type}) => {

    let riskCat = '';
    let bgColor = '';

    switch(type){
        case 'technology':
            riskCat = 'Technology';
            bgColor = colorConst.technology;
        break;
        case 'terrorism':
            riskCat = 'Terrorism';
            bgColor = colorConst.terrorism;
        break;
        case 'civilDisturbances':
            riskCat = 'Civil';
            bgColor = colorConst.civilDisturbances;
        break;
        case 'environment':
            riskCat = 'Environment';
            bgColor = colorConst.environment;
        break;
        case 'health':
            riskCat = 'Health';
            bgColor = colorConst.health;
        break;
        case 'critInfra':
            riskCat = 'Infrastructure';
            bgColor = colorConst.infrastructure;
        break;
        case 'crime':
            riskCat = 'Crime';
            bgColor = colorConst.crime;
        break;
        default:
            riskCat = '';
        break;
    }

    return(
        <div className={classes.rootBar}>
            <label className={classes.pBarLabel}>
                <span className={classes.labelType}>{riskCat}</span>
                <span className={classes.labelValue}>{value}</span>
            </label>
            <span className={classes.pBarOuter}>
                <span 
                    style={{
                        background: bgColor, 
                        width: `${value}%`, 
                        boxShadow: `0 0 5px 0 ${bgColor}` 
                    }} 
                    className={classes.pBarInner}
                >
                </span>
            </span>
        </div>
    )
}

export default injectSheet(styles)(Item);
