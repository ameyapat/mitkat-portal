import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
//helpers
import injectSheet from 'react-jss';
//style
import styles from './styles';

const colorConst = {
    veryHigh: 'rgba(181, 27, 51, 0.8)',
    high: 'rgba(250, 159, 101, 0.8)',
    medium: 'rgba(253, 225, 61, 0.8)',    
    veryLow: 'rgba(14, 127, 64, 0.8)',
    low: 'rgba(185, 211, 123, 0.8)',
}

const SpeedoItem = ({classes, value, type}) => {
    let riskLevel = '';
    let bgColor = '';

    switch(type){
        case 'veryHighRiskPercent':
            riskLevel = 'Very High';
            bgColor = colorConst.veryHigh;
        break;
        case 'highRiskPercent':
            riskLevel = 'High';
            bgColor = colorConst.high;
        break;
        case 'lowRiskPercentl':
            riskLevel = 'Low';
            bgColor = colorConst.low;
        break;
        case 'veryLowRiskPercent':
            riskLevel = 'Very Low';
            bgColor = colorConst.veryLow;
        break;
        case 'mediumRiskPercent':
            riskLevel = 'Medium';
            bgColor = colorConst.medium;
        break;        
        default:
            riskLevel = '';
        break;
    }

    return(
        <div className={classes.rootCircle}>            
            {/* <span 
                style={{
                    background: bgColor, 
                    width: `${value}%`,     
                    boxShadow: `0 0 5px 0 ${bgColor}` 
                }} 
                className={classes.pCircleInner}
            >
                
            </span>    */}
            <label className={classes.rootCircleLabel}>
                    <span className={classes.labelRiskValue}>{value}</span>
                    <span className={classes.labelRiskLevel}>{riskLevel}</span>
            </label>          
        </div>
    )
}

export default injectSheet(styles)(SpeedoItem);
