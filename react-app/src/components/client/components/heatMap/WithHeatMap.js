import React, { Component } from 'react';
import {
    withScriptjs,
    withGoogleMap,
    GoogleMap,
} from "react-google-maps";
import HeatmapLayer from "react-google-maps/lib/components/visualization/HeatmapLayer";
//helpers
import { withCookies } from 'react-cookie';
import darkStyle from '../../../../helpers/universalMapStyles.json';

const google = window.google;

@withCookies 
@withScriptjs
@withGoogleMap
class WithHeatMap extends Component{

    state = {
        heatMapData: {}
    }

    componentDidUpdate(prevProps){
        if(prevProps.analytics !== this.props.analytics){
            this.setState({heatMapData: this.props.analytics.heatmapobject});
        }
    }

    getLatLng = () => {
        let { heatmapdatapoints } = this.state.heatMapData;
        return heatmapdatapoints.map((i, idx) => ({ location: new google.maps.LatLng(i.latitude, i.longitude), weight: i.intensity }));
    }

    render(){

        let { focusLocation, heatmapdatapoints } = this.state.heatMapData;

        let defaultOptions = {
            mapTypeControl: false,
            streetViewControl: false,
            zoomControl: false,
            fullscreenControl: false,
            styles: darkStyle
        }

        return(
            <GoogleMap
                zoom={5.7}
                defaultOptions={defaultOptions}                
                center={focusLocation || { lat: 20.5937, lng: 80.9629 }}
            >
                <HeatmapLayer 
                    data={heatmapdatapoints ? this.getLatLng() : []}
                    options={{radius: 20}}
                />
            </GoogleMap>
        )
    }
}

export default WithHeatMap;