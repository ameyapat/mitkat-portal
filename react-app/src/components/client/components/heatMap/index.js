import React, { Component } from 'react';
import WithHeatMap from './WithHeatMap';
import SearchBox from './SearchBox';
import ProgressAnalytics from './ProgressAnalytics';
import RiskLevelChart from './RiskLevelChart';
import RiskIndex from './RiskIndex';
//helpers
import injectSheet from 'react-jss';
// import { connect } from 'react-redux';
import { withCookies } from 'react-cookie';
import AnalyticsBox from './AnalyticsBox';
//helpers
import moment from 'moment';
import { fetchApi } from '../../../../helpers/http/fetch';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import store from '../../../../store';
import { toggleToast } from '../../../../actions';
//style
import styles from './styles';
import MapToggle from '../../../ui/MapToggle/MapToggle';

const doughnutObj = {
    labels: [],            
    datasets: [{
        label: 'Risk Index',
        data: [],
        backgroundColor: [
            'rgba(35, 205, 152, 0.8)',//green
            'rgba(75, 178, 231, 0.8)',//blue
            'rgba(235, 135, 203, 0.8)',//
            'rgba(127, 251, 255, 0.8)',//turquise
            'rgba(120, 139, 254, 0.8)',//
            'rgba(176, 122, 251, 0.8)',//                                      
            'rgba(255, 220, 52, 0.8)',                   
        ],
        borderColor: [
            'rgba(35, 205, 152, 1)',//green
            'rgba(75, 178, 231, 1)',//blue                    
            'rgba(235, 135, 203, 1)',//
            'rgba(127, 251, 255, 1)',//turqoise
            'rgba(120, 139, 254, 1)',//
            'rgba(176, 122, 251, 1)',//                    
            'rgba(255, 220, 52, 1)'//
        ],
        borderWidth: 1
    }]
}


@withCookies
@injectSheet(styles)
class HeatMap extends Component{

    state = {
        // startDate: moment(new Date()).format('YYYY-MM-DD'),
        startDate: '2019-04-04',
        // endDate: moment(new Date()).format('YYYY-MM-DD'),
        endDate: '2019-12-31',
        analytics: {},
        searchLocation: '',
        searchParameter: '',        
        riskCatObj: doughnutObj
    }

    componentDidMount(){
        this.handleUpdateEvent();
    }

    handleDateChange = (type, value) => this.setState({[type]: value});

    handleInputChange = (type, value) => this.setState({[type]: value});

    handleUpdateEvent = () => {
        let { cookies } = this.props;        
        let { searchLocation, searchParameter, startDate, endDate } = this.state;
        let authToken = cookies.get('authToken');
        let reqData = {
            "locationInput": searchLocation,
            "searchParameters": searchParameter,
            "startdate": moment(startDate).format('YYYY-MM-DD'),
            "enddate": moment(endDate).format('YYYY-MM-DD')                
        }

        let reqObj = {
            url: API_ROUTES.getAnalytics,
            data: reqData,
            isAuth: true,
            authToken,
            method: 'POST',
            showToggle: true,
          };
    
          fetchApi(reqObj).then(data => {
            if(data.success){
                this.setState({analytics: data.output});
            }else{
                let toastObj = {
                        toastMsg: 'Something went wrong, Please try again',
                        showToast: true
                      };
                      store.dispatch(toggleToast(toastObj));
                }            
          });
    }

    parseRiskLevel = () => {
        let { doughnutData } = this.props;
        let { doughnutDataObj } = this.state;        
        const labels = [];
        const data = [];        
        
        for(let k in doughnutData){
            if(k !== 'riskIndex' && doughnutData[k]){                
                labels.push(k);
                data.push(doughnutData[k]);
            }
        }

        doughnutDataObj['labels'] = labels;           
        doughnutDataObj.datasets[0] = {...doughnutDataObj.datasets[0], data};

        this.setState({ doughnutDataObj });
    }

    parseRiskCategory = () => {
        let { doughnutData } = this.props;
        let { doughnutDataObj } = this.state;        
        const labels = [];
        const data = [];        
        
        for(let k in doughnutData){
            if(k !== 'riskIndex' && doughnutData[k]){                
                labels.push(k);
                data.push(doughnutData[k]);
            }
        }

        doughnutDataObj['labels'] = labels;           
        doughnutDataObj.datasets[0] = {...doughnutDataObj.datasets[0], data};

        this.setState({ doughnutDataObj });
    }

    render(){
        let { classes } = this.props;
        let { analytics, startDate, endDate, searchLocation, searchParameter } = this.state;

        return(
            <div className={classes.rootHeatMapWrap}>
                <MapToggle />
                <SearchBox  
                    startDate={startDate} 
                    endDate={endDate} 
                    searchLocation={searchLocation}
                    searchParameter={searchParameter}
                    handleDateChange={this.handleDateChange} 
                    handleInputChange={this.handleInputChange}
                    handleUpdateEvent={this.handleUpdateEvent}
                />
                <WithHeatMap
                    googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyCYOTLyn6xbysArlvlW4zpxnH9p6H3An08&v=3.exp&libraries=geometry,drawing,places,visualization"
                    loadingElement={<div style={{ height: `100%` }} />}
                    containerElement={<div style={{ height: `100vh`, position: 'relative' }} />}
                    mapElement={<div style={{ height: `100%` }} />}
                    analytics={analytics}
                /> 
                {
                    analytics.size && 
                    <AnalyticsBox 
                        doughnutData={analytics.riskIndexDoughnutChart} 
                        eventGraphData={analytics.eventDistributionGraph}
                        riskGraphData={analytics.riskDistributionGraph}
                        riskLevelData={analytics.riskLevelPieChart}
                        riskCatData={analytics.riskCategoryPieChart}
                    />           
                }                                
                { analytics.size && <RiskIndex value={analytics.riskIndexDoughnutChart.riskIndex} /> }                
                { analytics.size && <RiskLevelChart doughnutData={analytics.riskLevelPieChart} /> }
                { analytics.size && <ProgressAnalytics riskCatData={analytics.riskCategoryPieChart} /> }
            </div>
        )
    }
}

export default HeatMap;