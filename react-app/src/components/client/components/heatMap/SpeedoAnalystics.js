import React, { Component } from 'react';
import SpeedoItem from './SpeedoItem';
//helpers
import injectSheet from 'react-jss';
//style
import styles from './styles';

@injectSheet(styles)
class SpeedoAnalytics extends Component{

    getProgressXML = () => {
        let { riskLevelData } = this.props;
        let riskLevelDataXML = [];

        for(let k in riskLevelData){
            riskLevelDataXML.push(<SpeedoItem key={k} type={k} value={riskLevelData[k]} />)
        }

        return riskLevelDataXML;
    }

    render(){
        let { classes, riskLevelData } = this.props;
        return(
            <div className={classes.rootCircleProgress}>                
                <div className={classes.progressItemsWrap}>
                    {
                        riskLevelData && this.getProgressXML()
                    }
                </div>
            </div>
        )
    }
}

export default SpeedoAnalytics;