import React, { Component } from 'react';
import { Pie, Doughnut } from 'react-chartjs-2';
//
import injectSheet from 'react-jss';
//
import styles from './styles';

@injectSheet(styles)
class RiskLevelChart extends Component{

    state = {
        doughnutDataObj: {
            labels: [],   
            position: 'right',         
            datasets: [{
                label: 'Risk Index',
                data: [],
                backgroundColor: [
                    'rgba(35, 205, 152, 0.8)',//green
                    'rgba(75, 178, 231, 0.8)',//blue
                    'rgba(235, 135, 203, 0.8)',//
                    'rgba(127, 251, 255, 0.8)',//turquise
                    'rgba(120, 139, 254, 0.8)',//
                    'rgba(176, 122, 251, 0.8)',//                                      
                    'rgba(255, 220, 52, 0.8)',                   
                ],
                borderColor: [
                    'rgba(35, 205, 152, 1)',//green
                    'rgba(75, 178, 231, 1)',//blue                    
                    'rgba(235, 135, 203, 1)',//
                    'rgba(127, 251, 255, 1)',//turqoise
                    'rgba(120, 139, 254, 1)',//
                    'rgba(176, 122, 251, 1)',//                    
                    'rgba(255, 220, 52, 1)'//
                ],
                borderWidth: 1,
                position: 'right'
            }]
        }
    }

    async componentDidMount(){
        await this.parseRiskIndexValues();
    }

    async componentDidUpdate(prevProps){
        if(this.props !== prevProps) await this.parseRiskIndexValues();
    }

    parseRiskIndexValues = () => {        
        let { doughnutData } = this.props;
        let { doughnutDataObj } = this.state;        
        const labels = [];
        const data = [];
        let label = '';        
        
        for(let k in doughnutData){
            if(k !== 'riskIndex' && doughnutData[k]){  
                switch(k){
                    case 'highRiskPercent':
                        label = 'High';
                    break;
                    case 'lowRiskPercentl':
                        label = 'Low';
                    break;
                    case 'mediumRiskPercent':
                        label = 'Medium';
                    break;
                    case 'mediumRiskPercent':
                        label = 'Medium';
                    break;
                    case 'veryHighRiskPercent':
                        label = 'Very High';
                    break;
                    case 'veryLowRiskPercent':
                        label = 'Very Low';
                    break;
                }              
                labels.push(label);
                data.push(doughnutData[k]);
            }
        }

        doughnutDataObj['labels'] = labels;           
        doughnutDataObj.datasets[0] = {...doughnutDataObj.datasets[0], data};
        this.setState({ doughnutDataObj });
    }

    render(){
        let { classes } = this.props;
        let { doughnutDataObj } = this.state;
        return(
            <div className={classes.doughnutDataObj}>
                <label>[ Risk Levels ]</label>
                {
                    doughnutDataObj &&
                    <Doughnut 
                        data={doughnutDataObj}                                        
                        options={{ 
                            maintainAspectRatio: true, 
                            cutoutPercentage: 80, 
                            legend: {
                            display: true,
                            position: 'right'
                        } 
                     }} 
                    />
                }
            </div>
        )
    }
}

export default RiskLevelChart;