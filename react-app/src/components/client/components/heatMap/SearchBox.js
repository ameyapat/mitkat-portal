import React, { Component } from 'react';
//components
import { DatePicker } from "material-ui-pickers";
import Button from '@material-ui/core/Button';
//helpers
import clsx from 'clsx';
import injectSheet from 'react-jss';
import { TextField } from '@material-ui/core';
//styles
import styles from './styles';

@injectSheet(styles)
class SearchBox extends Component{

    render(){
        let { classes, startDate, endDate, handleDateChange, 
            searchLocation, searchParameter, handleInputChange, handleUpdateEvent } = this.props;

        return(
            <div className={clsx(classes.rootSearchBox, classes.outerBox, classes.box)}>
                <div className={clsx(classes.innerBox, classes.box)}></div>
                <DatePicker
                    label="Start Date"                    
                    value={startDate}                    
                    onChange={(value) => handleDateChange('startDate', value)}
                    animateYearScrolling
                    className="start--date"
                    fullWidth={true}
                    classes={{
                        root: classes.rootDatePicker,
                        underline: classes.pickerUnderline,
                        formControl: classes.dateFormcontrol,
                        input: classes.dateInput
                    }}                    
                    InputProps={{
                        classes: {
                            input: classes.dateInput,
                            underline: classes.datepickerUnderline,                            
                        },                                             
                    }}
                    InputLabelProps={{
                        classes: {
                            root: classes.inputLabelRoot,
                            focused: classes.focusedDatePicker,                            
                        }, 
                        disableAnimation: true                                               
                    }}
                /> 
                <DatePicker
                    label="End Date"
                    value={endDate}                    
                    onChange={(value) => handleDateChange('endDate', value)}
                    animateYearScrolling
                    fullWidth={true}
                    className="end--date"
                    classes={{
                        root: classes.rootDatePicker,
                        underline: classes.pickerUnderline,
                        formControl: classes.dateFormcontrol,
                        input: classes.dateInput
                    }}
                    InputProps={{
                        classes: {
                            input: classes.dateInput,
                            underline: classes.datepickerUnderline, 
                        }                        
                    }}
                    InputLabelProps={{
                        classes: {
                            root: classes.inputLabelRoot,
                            focused: classes.focusedDatePicker
                        }
                    }}
                />
                <TextField                  
                  label="Search Location"                  
                  fullWidth                  
                  value={searchLocation}
                  onChange={ (e) => handleInputChange('searchLocation', e.target.value)}                  
                  className={classes.textField}
                  defaultValue="Enter Search Location"
                  margin="normal"                
                  classes={{
                      root: classes.rootTextField,                      
                    }}
                    InputProps={{
                        classes: {
                            input: classes.inputTextField,
                            underline: classes.datepickerUnderline, 
                        }
                    }}
                    InputLabelProps={{
                        classes: {
                            root: classes.inputLabelRoot,
                            focused: classes.focusedTextField
                        }
                    }}
                  />
                <TextField                  
                  label="Search Parameter"   
                  fullWidth                                                   
                  value={searchParameter}
                  onChange={ (e) => handleInputChange('searchParameter', e.target.value)}
                  className={classes.textField}
                  defaultValue="Enter Search Parameter"
                  margin="normal"                
                  classes={{
                    root: classes.rootTextField,                      
                  }}
                  InputProps={{
                      classes: {
                          input: classes.inputTextField,
                          underline: classes.datepickerUnderline, 
                      }
                  }}
                  InputLabelProps={{
                      classes: {
                          root: classes.inputLabelRoot,
                          focused: classes.focusedTextField
                      }
                  }}
                />                
                <Button 
                    disabled={!searchParameter && !searchLocation}
                    onClick={handleUpdateEvent}                     
                    variant="contained"                     
                    className={classes.applyBtn}
                    size="small"
                    classes={{
                        disabled: classes.btnDisabled
                    }}
                >
                    Apply              
                </Button>                
            </div>
        )
    }
}


export default SearchBox;