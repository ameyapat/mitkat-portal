import React from 'react';
import { Doughnut, Pie } from 'react-chartjs-2';

const DoughnutRiskIndex = ({data}) => {    
    return <Pie 
        data={data}
        width={100}
        height={50}
        options={{ 
            maintainAspectRatio: true,
            legend: {
                display: true,
                position: 'right'
            } 
        }} 
     />
}

export default DoughnutRiskIndex;