import React, { Component } from 'react';
import DoughnutRiskIndex from './DoughnutRiskIndex';
import EventGraph from './EventGraph';
import injectSheet from 'react-jss';
//style
import styles from './styles';

@injectSheet(styles)
class AnalyticsBox extends Component {
  state = {
    doughnutDataObj: {
      labels: [],
      datasets: [
        {
          label: 'Risk Index',
          data: [],
          backgroundColor: [
            'rgba(35, 205, 152, 0.8)', //green
            'rgba(75, 178, 231, 0.8)', //blue
            'rgba(235, 135, 203, 0.8)', //
            'rgba(127, 251, 255, 0.8)', //turquise
            'rgba(120, 139, 254, 0.8)', //
            'rgba(176, 122, 251, 0.8)', //
            'rgba(255, 220, 52, 0.8)',
          ],
          borderColor: [
            'rgba(35, 205, 152, 1)', //green
            'rgba(75, 178, 231, 1)', //blue
            'rgba(235, 135, 203, 1)', //
            'rgba(127, 251, 255, 1)', //turqoise
            'rgba(120, 139, 254, 1)', //
            'rgba(176, 122, 251, 1)', //
            'rgba(255, 220, 52, 1)', //
          ],
          borderWidth: 1,
        },
      ],
    },
    eventGraphObj: {
      labels: [],
      datasets: [
        {
          label: 'Event Distribution',
          data: [],
          backgroundColor: 'rgba(75, 178, 231, 0.7)',
          borderColor: 'rgba(75, 178, 231, 1)',
          borderWidth: 2,
        },
      ],
    },
    riskGraphObj: {
      labels: [],
      datasets: [
        {
          label: 'Risk Distribution',
          data: [],
          backgroundColor: 'rgba(176, 122, 251, 0.8)',
          borderColor: 'rgba(176, 122, 251, 1)',
          borderWidth: 2,
        },
      ],
    },
  };

  async componentDidMount() {
    await this.parseRiskIndexValues();
    await this.parseEventValues();
    await this.parseRiskValues();
  }

  async componentDidUpdate(prevProps) {
    // let { doughnutData, eventGraphData, riskGraphData } = this.props;
    if (prevProps !== this.props) {
      await this.parseRiskIndexValues();
      await this.parseEventValues();
      await this.parseRiskValues();
    }
  }

  parseRiskIndexValues = () => {
    let { doughnutData } = this.props;
    let { doughnutDataObj } = this.state;
    const labels = [];
    const data = [];
    let label = '';

    for (let k in doughnutData) {
      if (k !== 'riskIndex' && doughnutData[k]) {
        switch (k) {
          case 'civilDisturbancesweight':
            label = 'Civil Disturbance';
            break;
          case 'crimeweight':
            label = 'Crime';
            break;
          case 'critInfraweight':
            label = 'Infrastructure';
            break;
          case 'environmentweight':
            label = 'Enviornment';
            break;
          case 'healthweight':
            label = 'Health';
            break;
          case 'technologyweight':
            label = 'Technology';
            break;
          case 'terrorismweight':
            label = 'Terrorism';
            break;
        }
        labels.push(label);
        data.push(doughnutData[k]);
      }
    }
  }

    parseRiskIndexValues = () => {        
        let { doughnutData } = this.props;
        let { doughnutDataObj } = this.state;        
        const labels = [];
        const data = [];  
        let label = '';      
        
        for(let k in doughnutData){
            if(k !== 'riskIndex' && doughnutData[k]){   
                switch(k){
                    case 'civilDisturbancesweight':
                        label = 'Civil Disturbance';
                    break;
                    case 'crimeweight':
                        label = 'Crime';
                    break;
                    case 'critInfraweight':
                        label = 'Infrastructure';
                    break;
                    case 'environmentweight':
                        label = 'Environment';
                    break;
                    case 'healthweight':
                        label = 'Health';
                    break;
                    case 'technologyweight':
                        label = 'Technology';
                    break;
                    case 'terrorismweight':
                        label = 'Terrorism';
                    break;
                }              
                labels.push(label);
                data.push(doughnutData[k]);
            }
        }
    doughnutDataObj['labels'] = labels;
    doughnutDataObj.datasets[0] = { ...doughnutDataObj.datasets[0], data };

    this.setState({ doughnutDataObj });
  };

  parseEventValues = () => {
    let { eventGraphData } = this.props;
    let { eventGraphObj } = this.state;
    const labels = [];
    const data = [];

    eventGraphData.map((i, idx) => {
      labels.push(i.xaxisdatapoint);
      data.push(i.yaxisdatapoint);
    });

    eventGraphObj['labels'] = labels;
    eventGraphObj.datasets[0] = { ...eventGraphObj.datasets[0], data };
    this.setState({ eventGraphObj });
  };

  parseRiskValues = () => {
    let { riskGraphData } = this.props;
    let { riskGraphObj } = this.state;
    const labels = [];
    const data = [];

    riskGraphData.map((i, idx) => {
      labels.push(i.xaxisdatapoint);
      data.push(i.yaxisdatapoint);
    });

    riskGraphObj['labels'] = labels;
    riskGraphObj.datasets[0] = { ...riskGraphObj.datasets[0], data };
    this.setState({ riskGraphObj });
  };

  render() {
    let { classes } = this.props;
    let { doughnutDataObj, eventGraphObj, riskGraphObj } = this.state;

    return (
      <div className={classes.rootAnalyticsBox}>
        <div className={classes.doughnutWrap}>
          <label>[ Risk Index ]</label>
          {doughnutDataObj && <DoughnutRiskIndex data={doughnutDataObj} />}
        </div>
        <div className={classes.rootEventGraph}>
          <label>[ Event Distribution ]</label>
          {eventGraphObj && <EventGraph data={eventGraphObj} />}
        </div>
        <div className={classes.rootRiskGraph}>
          <label>[ Risk Distribution ]</label>
          {riskGraphObj && <EventGraph data={riskGraphObj} />}
        </div>
      </div>
    );
  }
}

export default AnalyticsBox;
