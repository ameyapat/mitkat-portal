import React, { Component, Fragment } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import injectSheet from 'react-jss';
import styles from './style';

@injectSheet(styles)
class QueryForm extends Component {
  state = {
    title: '',
    details: '',
  };

  handleChange = (type, value) => this.setState({ [type]: value });

  render() {
    let { handleSubmitQuery, classes } = this.props;
    let { title, details } = this.state;
    return (
      <div className="query--form">
        <p className="query--header">Create New Query </p>
        <TextField
          // error={validation[input.id].isInvalid}
          key={'title'}
          multiline
          rows="3"
          // id={input.id}
          label={'Title'}
          fullWidth
          type={'text'}
          value={title}
          onChange={e => this.handleChange('title', e.target.value)}
          margin="normal"
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
              input: classes.rootInput,
              underline: classes.underline,
            },
          }}
          InputLabelProps={{
            className: classes.rootInput,
          }}
        />
        <TextField
          // error={validation[input.id].isInvalid}
          multiline
          rows="10"
          key={'details'}
          label={'Details'}
          fullWidth
          value={details}
          onChange={e => this.handleChange('details', e.target.value)}
          margin="normal"
          InputProps={{
            classes: {
              notchedOutline: classes.notchedOutline,
              input: classes.rootInput,
              underline: classes.underline,
            },
          }}
          InputLabelProps={{
            className: classes.rootInput,
          }}
        />
        <div className="btnWrap">
          <Button
            variant="outlined"
            className={classes.submitBtn}
            onClick={() => handleSubmitQuery(this.state)}
          >
            Submit
          </Button>
        </div>
      </div>
    );
  }
}

export default QueryForm;
