import { red } from '@material-ui/core/colors';
import * as globalCss from '../../../../sass/globalVariables.scss';

export default {
  rootQuerySystem: {
    '& .close-times': {
      position: 'absolute',
      top: 5,
      right: 12,
      fontSize: '25px !important',
      cursor: 'pointer',
      color: 'var(--whiteMediumEmp)',
      padding: '5px 10px',
      borderRadius: 3,

      '&:hover': {
        background: 'rgba(31,31,31,1)',
      },
    },
    '& .inner--query': {
      '& .query--header': {},
      '& .query--body': {
        background: 'var(--darkGrey)',
        height: '89vh',
        padding: 15,
        display: 'flex',
        '& .query--form': {
          flex: '45%',
          padding: 15,
          boxShadow: '0px 0px 2px 1px var(--backgroundSolid)',

          '& .query--header': {
            color: 'var(--whiteMediumEmp)',
            padding: '15px',
            margin: '-15px',
            background: 'var(--darkGreySolid)',
            marginBottom: 15,
          },
        },
        '& .query--table': {
          flex: '55%',
          padding: 15,
          // boxShadow: '0px 0px 2px 1px var(--backgroundSolid)',
        },
        '& .btnWrap': {
          textAlign: 'center',
        },
      },
      '& .query__details': {
        position: 'relative',
        '& .close--details': {
          position: 'absolute',
          right: 40,
          fontSize: 12,
          color: '#ffffff',
          '&:hover': {
            color: '#17a2b8',
            // textDecoration: 'underline',
            cursor: 'pointer',
          },
        },
        background: 'var(--darkGreySolid)',
        //margin: '6px 6px 8px 0',
        //borderRadius: '5px',
        padding: '8px 8px',
        // borderBottom: '1px solid #f1f1f1',
        //border: '2px solid rgba(255, 255, 255, 0.6)',//rgba(52, 73, 94, 1)
        '& label': {
          margin: '10px 0px',
          display: 'block',
          fontSize: 13,
          color: 'var(--whiteMediumEmp) ',
        },
        '& p': {
          color: 'var(--darkActive)',
          fontSize: 12,
        },
        '& .response--time': {
          margin: '10px 0',
          fontSize: 11,
        },
      },
    },
  },
  rootHeader: {
    padding: '13px 30px',
    color: 'var(--lightText)',
    background: 'var(--darkSolid)',
  },
  submitBtn: {
    marginTop: '15px !important',
    color: 'var(--whiteMediumEmp) !important',
    // border: '1px solid var(--whiteHighEmp) !important ',
    // background: 'var(--darkActive)',

    '&:hover': {
      background: 'var(--darkActive)',
      color: 'var(--lightText) !important',
    },
  },
  tableHead: {
    display: 'flex',
    '& div': {
      flex: 1,
      borderBottom: '1px solid #f1f1f1',
      paddingBottom: 10,
      fontSize: 14,
      color: 'var(--whiteMediumEmp)',
      textAlign: 'center',
    },
    '& div:first-of-type ': {
      flex: '35%',
    },
  },
  rootTableRow: {
    maxHeight: '77vh',

    '& .tablelist__row': {
      '& .tablelist__item:first-of-type ': {
        flex: '35%',

        '& p': {
          maxWidth: '100%',
        },
      },
    },
  },
  textField: {
    marginTop: '0 !important',
  },
  rootActiveRow: {
    background: 'var(--darkGrey)',
    borderBottom: '1px solid var(--darkGrey)',
  },
  rootViewMore: {
    border: 0,

    color: globalCss.lightBlue,

    '&:hover': {
      border: 0,
    },
  },
  rootTCell: {
    '& span': {
      padding: '3px !important',
      borderRadius: '3px !important',
      display: 'block',
    },
    color: '#000',
    fontSize: 11,
    // fontWeight: 600,
    '& .resolved': {
      color: 'rgba(39, 174, 96, 1);',
    },
    '& .pending': {
      color: 'rgba(243, 156, 18, 1)',
    },
  },
  rootInput: {
    color: 'var(--whiteHighEmp) !important ',
  },
  notchedOutline: {
    borderColor: 'var(--whiteHighEmp) !important ',
  },
  underline: {
    color: 'var(--whiteHighEmp)',
    '&::hover': {
      '&::before': {
        borderBottom: '2px solid var(--whiteHighEmp)',
      },
    },
    '&::before': {
      borderBottom: '2px solid var(--whiteHighEmp)',
    },
    '&::after': {
      borderBottom: '2px solid var(--whiteHighEmp)',
    },
  },
};
