import React, { Component, Fragment } from 'react';
//components
import Button from '@material-ui/core/Button';
import TableListRow from '../../../ui/tablelist/TableListRow';
import TableListCell from '../../../ui/tablelist/TableListCell';
import Empty from '../../../ui/empty'; //<Empty title="No alerts available!" />
import TableList from '../../../ui/tablelist';
import QueryListItem from './QueryListItem';
//helpers
import { fetchApi } from '../../../../helpers/http/fetch';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import { withCookies } from 'react-cookie';
import styles from './style';
import injectSheet from 'react-jss';
import { localDateTime } from '../../../../helpers/utils';
import moment from 'moment';

@injectSheet(styles)
@withCookies
class QueryList extends Component {
  state = {
    queryList: [],
    queryDetails: {},
  };

  componentDidMount() {
    this.getQueryList();
  }

  getQueryList = () => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');

    let reqObj = {
      url: API_ROUTES.getQueryList,
      method: 'POST',
      isAuth: true,
      authToken,
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        this.setState({ queryList: data });
      })
      .catch(e => console.log(e));
  };

  handleViewDetails = id => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');

    let reqObj = {
      url: `${API_ROUTES.getQueryDetails}/${id}`,
      method: 'POST',
      isAuth: true,
      authToken,
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        let { queryDetails } = this.state;
        queryDetails['clientName'] = data.clientname;
        queryDetails['details'] = data.details;
        queryDetails['title'] = data.title;
        queryDetails['status'] = data.status;
        queryDetails['queryTime'] = moment(
          localDateTime(data.querytime),
        ).format('Do MMM YYYY, h:mm:ss a');
        queryDetails['response'] = data.response;
        queryDetails['responseTime'] = data.responsetime;
        queryDetails['responseUser'] = data.responseuser;

        this.setState({ queryDetails: data });
      })
      .catch(e => console.log(e));
  };

  generateListXML = () => {
    let { classes } = this.props;
    let { queryList, queryDetails } = this.state;
    let queryListXML = [];

    return (queryListXML = queryList.map(items => {
      return <QueryListItem items={items} />;
    }));
  };

  render() {
    let { classes } = this.props;

    return (
      <div className="query--table">
        <div className={classes.tableHead}>
          <div>Title</div>
          <div>Status</div>
          <div>Query Date</div>
          <div>Action</div>
        </div>
        <TableList classes={{ root: classes.rootTableRow }}>
          {this.generateListXML()}
        </TableList>
      </div>
    );
  }
}

export default QueryList;
