import React, { Component } from 'react';
//components
import Pills from '../../../ui/pills';
import QueryForm from './QueryForm';
import QueryList from './QueryList';
//
import injectSheet from 'react-jss';
import styles from './style';
import { PILL_ITEMS } from './constant';
import { fetchApi } from '../../../../helpers/http/fetch';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import { withCookies } from 'react-cookie';
import store from '../../../../store';
import { toggleToast } from '../../../../actions';

@withCookies
@injectSheet(styles)
class QuerySystem extends Component {
  handleSubmitQuery = data => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');

    let dataObj = {
      title: data.title,
      details: data.details,
    };

    let reqObj = {
      url: API_ROUTES.addNewQuery,
      method: 'POST',
      isAuth: true,
      authToken,
      data: dataObj,
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        let toastObj = {
          showToast: true,
          toastMsg: 'Query submitted successfully!',
        };
        store.dispatch(toggleToast(toastObj));
        // re-renders the component
        this.setState({});
      })
      .catch(e => console.log(e));
  };

  render() {
    let { classes } = this.props;

    return (
      <div className={classes.rootQuerySystem}>
        <h1 className={classes.rootHeader}> Query System </h1>
        <div className="inner--query">
          <div className="query--body">
            <QueryForm handleSubmitQuery={this.handleSubmitQuery} />

            <QueryList />
          </div>
        </div>
      </div>
    );
  }
}

export default QuerySystem;
