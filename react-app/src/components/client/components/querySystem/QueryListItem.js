import React, { Component, Fragment } from 'react';
import Button from '@material-ui/core/Button';
import QueryItemDetails from './QueryItemDetails';
import TableListRow from '../../../ui/tablelist/TableListRow';
import TableListCell from '../../../ui/tablelist/TableListCell';
//helpers
import { fetchApi } from '../../../../helpers/http/fetch';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import { withCookies } from 'react-cookie';
import { withStyles } from '@material-ui/core';
import moment from 'moment';
//
import styles from './style';
import { localDateTime } from '../../../../helpers/utils';

@withStyles(styles)
@withCookies
class QueryListItem extends Component {
  state = {
    showDetails: false,
    queryDetails: {},
    file: '-',
  };

  handleViewDetails = id => {
    let { cookies, items } = this.props;
    let authToken = cookies.get('authToken');

    let reqObj = {
      url: `${API_ROUTES.getQueryDetails}/${id}`,
      method: 'POST',
      isAuth: true,
      authToken,
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        let { queryDetails } = this.state;
        queryDetails['clientName'] = data.clientname;
        queryDetails['details'] = data.details;
        queryDetails['title'] = data.title;
        queryDetails['status'] = data.status;
        queryDetails['queryTime'] = data.querytime;
        queryDetails['response'] = data.response;
        queryDetails['responseTime'] = data.responsetime;
        queryDetails['responseUser'] = data.responseuser;

        this.setState({
          queryDetails: data,
          showDetails: true,
          file: items.file,
        });
      })
      .catch(e => console.log(e));
  };

  handleCloseDetails = () => this.setState({ showDetails: false });

  render() {
    let { items, classes } = this.props;
    let { queryDetails, showDetails } = this.state;

    return (
      <Fragment>
        <TableListRow
          key={items.id}
          classes={showDetails ? { root: classes.rootActiveRow } : ''}
        >
          <TableListCell value={items.title} />
          <TableListCell classes={{ root: classes.rootTCell }}>
            <span className={`${items.status ? 'resolved' : 'pending'}`}>
              {items.status ? 'Resolved' : 'Pending'}
            </span>
          </TableListCell>
          <TableListCell
            value={moment(localDateTime(items.timestamp)).format(
              'Do MMM YYYY, h:mm:ss a',
            )}
          />
          <TableListCell>
            {showDetails ? (
              <Button
                variant="outlined"
                size="small"
                color="primary"
                onClick={() => this.handleCloseDetails()}
                classes={{ root: classes.rootViewMore }}
              >
                <i className="fas fa-chevron-up"></i>
              </Button>
            ) : (
              <Button
                variant="outlined"
                size="small"
                color="primary"
                onClick={() => this.handleViewDetails(items.id)}
                classes={{ root: classes.rootViewMore }}
              >
                <i className="fas fa-chevron-down"></i>
              </Button>
            )}
          </TableListCell>
        </TableListRow>
        {showDetails && (
          <QueryItemDetails
            queryDetails={queryDetails}
            file={this.state.file}
            // handleCloseDetails={this.handleCloseDetails}
          />
        )}
      </Fragment>
    );
  }
}

export default QueryListItem;
