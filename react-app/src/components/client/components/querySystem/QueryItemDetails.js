import React, { Component } from 'react';
import { Spring } from 'react-spring/renderprops';
import moment from 'moment';
import { connect } from 'react-redux';
import './style.scss';

@connect(state => {
  return {
    themeChange: state.themeChange,
  };
})
class QueryItemDetails extends Component {
  render() {
    let { queryDetails } = this.props;
    const {
      themeChange: { setDarkTheme },
      file,
    } = this.props;
    return (
      <Spring from={{ opacity: 0 }} to={{ opacity: 1 }}>
        {props => (
          <div
            style={props}
            className={`query__details ${setDarkTheme ? 'dark' : 'light'}`}
          >
            {/* <span
              className="close--details"
              onClick={() => this.props.handleCloseDetails()}
            >
              <i className="fas fa-chevron-up"></i>
            </span> */}
            <label>Title:</label>
            <p>{queryDetails.title}</p>
            <label>Details:</label>
            <p>{queryDetails.details}</p>
            {/* <small>{queryDetails.queryTime}</small> */}
            <label>Response:</label>
            <p className="response--time">
              {queryDetails.responsetime
                ? moment(queryDetails.responsetime).format(
                    'Do MMM YYYY, h:mm:ss a',
                  )
                : 'Awaiting Response'}
            </p>
            <label>File:</label>
            <p>
              {file ? (
                <a className="view-btn" href={`${file}`} target="_blank">
                  View File
                </a>
              ) : (
                '-'
              )}
            </p>
          </div>
        )}
      </Spring>
    );
  }
}
export default QueryItemDetails;
