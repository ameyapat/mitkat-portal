import React, { Component } from 'react'
//components
import MapApi from './MapApi';

class VaccinationMap extends Component {
  render() {
    return (
      <MapApi setCountryId={this.props.setCountryId} />
    )
  }
}

export default VaccinationMap;
