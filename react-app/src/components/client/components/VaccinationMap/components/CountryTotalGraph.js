import React from 'react'
import '../style.scss'

import VaccinationGraph from './VaccinationGraph'

const CountryTotalGraph = ({ vaccinationGraphObj, coronaDash }) => ( 
    <div className="countryTotalGraphData__row">
        <p>Total Vaccinations </p>
        { vaccinationGraphObj && <VaccinationGraph data={vaccinationGraphObj} /> }
    </div>
)

export default CountryTotalGraph;


