
import React from 'react';
import ShowMoreText from 'react-show-more-text';

const CoronaPhaseData = ({ coronaDetails }) => (
    <div id="countryCoronaPhaseData" >
        <div className="countryCoronaPhaseData__box"> 
        <h1> Timeline of vaccination </h1>
        <p>
            <ShowMoreText
                lines={1}
                more='Show more'
                less='Show less'
                className='content-css'
                anchorClass='my-anchor-css-class'
                expanded={false}
                width={500}
            >
                {coronaDetails.vaccineTimeline}
            </ShowMoreText>
        </p>
        <ul class="timeline">
            <li>
                <h2> Phase 1 </h2>
                <p>
                    <ShowMoreText
                        lines={2}
                        more='Show more'
                        less='Show less'
                        className='content-css'
                        anchorClass='my-anchor-css-class'
                        expanded={false}
                        width={250}
                    >
                    {coronaDetails.phase1}
                    </ShowMoreText>
                </p>
            </li>
            <li>
                <h2> Phase 2 </h2>
                <p>
                    <ShowMoreText
                        lines={2}
                        more='Show more'
                        less='Show less'
                        className='content-css'
                        anchorClass='my-anchor-css-class'
                        expanded={false}
                        width={250}
                    >
                    {coronaDetails.phase2}
                    </ShowMoreText>
                </p>
            </li>
            <li>
                <h2> Phase 3 </h2>
                <p>
                    <ShowMoreText
                        lines={2}
                        more='Show more'
                        less='Show less'
                        className='content-css'
                        anchorClass='my-anchor-css-class'
                        expanded={false}
                        width={250}
                    >
                    {coronaDetails.phase3}
                    </ShowMoreText>
                </p>
            </li>
        </ul>             
        </div>
    </div>
)

export default CoronaPhaseData;