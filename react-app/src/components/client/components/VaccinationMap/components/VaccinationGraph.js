import React from 'react';
import { Line } from 'react-chartjs-2';

const VaccinationGraph = ({ data }) => {    
    return <Line 
        data={data} 
        width={100}
        height={50}
        options={{ 
            maintainAspectRatio: true, 
            legend: {
                display: true,
                position: 'bottom'
            } 
        }} 
    />
}

export default VaccinationGraph;