import React from 'react'
import '../style.scss'

import VaccinationGraph from './VaccinationGraph'

const CountryPerHundGraph = ({ vaccinationGraphObj, coronaDash }) => (
    <div className="countryHundGraphData__row"> 
        <p>% Population Vaccinated</p>
        { vaccinationGraphObj && <VaccinationGraph data={vaccinationGraphObj} /> }
    </div>
)

export default CountryPerHundGraph;


