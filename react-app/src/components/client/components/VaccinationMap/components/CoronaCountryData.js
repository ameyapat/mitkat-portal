import React from 'react';
import '../style.scss';
import columnImg from  '../../../../../assets/vaccine-covid.png';

const CoronaCountryData = ({ coronaDetails, coronaDash }) => (
    <div id="countryCoronaData" >
            <div className="countryCoronaData__box"> 
              <h1 className="countryCoronaData__countryname"> {coronaDetails.countryName} </h1> 
              <p className="countryCoronaData__data"> 
                Current Updates : &nbsp;
                <span>  {coronaDetails.otherParagraph}  </span>
              </p>
              <p className="countryCoronaData__data"> 
                Approved Vaccine  : &nbsp;
                <span> {coronaDetails.approvedVaccine} </span>
              </p> 
              <p className="countryCoronaData__data"> 
                Vaccination Start Date :&nbsp;  
                <span> {coronaDetails.vaccinatinStartDate} </span>
              </p>
              <div className="countryCoronaData__row">
                <div class="countryCoronaData__row-column">                   
                  <p> Total Vaccinations  </p>
                  <span className="nummber"> {coronaDash.totalVaccinations} </span>
                  <img src={columnImg} />
                </div>
                {/* <div class="countryCoronaData__row-column">                   
                  <p> New Vaccinations  </p>
                  <span className="nummber"> {coronaDash.newvaccinations} </span>
                  <img src={columnImg} />
                </div> */}
              {/* </div>
              <div className="countryCoronaData__row"> */}
                {/* <div class="countryCoronaData__row-column">                   
                  <p> Growth Rate </p>
                  <span className="nummber"> {coronaDash.percentincreasevaccination}</span>
                  <img src={columnImg} />
                </div> */}
                <div class="countryCoronaData__row-column">                   
                  <p>% Population Vaccinated </p>
                  <span className="nummber">{coronaDash.totalvaccinationsperhundred}</span> 
                  <img src={columnImg} />                 
                </div>
              </div>
              
              <p className="countryCoronaData__data-link"> 
                Official Link : &nbsp; <a href={coronaDetails.officiallink} target="_blank">{coronaDetails.officiallink}</a>
              </p> 
              <p className="countryCoronaData__data-link"> 
                Vaccine Link : &nbsp; <a href={coronaDetails.vaccinelink} target="_blank">{coronaDetails.vaccinelink}</a>
              </p>
              <p className="countryCoronaData__data-link"> 
                For more info : &nbsp; <a href={coronaDetails.moreinfolink} target="_blank">{coronaDetails.moreinfolink}</a>
              </p> 
              <p className="countryCoronaData__data-link">
                Info. Last Updated On : &nbsp; <span>{coronaDetails.lastupdateddatastring} </span>
              </p> 
              <p className="countryCoronaData__data-link">
                Reading Last Updated On : &nbsp; <span>{coronaDetails.lastupdatedreadingstring} </span>
              </p> 
            </div>           
          </div> 
)

export default CoronaCountryData