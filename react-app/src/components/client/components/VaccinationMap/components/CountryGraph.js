import React from 'react'
import '../style.scss'

import VaccinationGraph from './VaccinationGraph'

const CountryGraph = ({ vaccinationGraphObj, coronaDash }) => (
            <div className="countryCoronaGraphData__row">
                <p>New Vaccinations</p>
                { vaccinationGraphObj && <VaccinationGraph data={vaccinationGraphObj} /> }
            </div>
)

export default CountryGraph;


