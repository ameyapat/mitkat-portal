import React, { Component } from 'react';
import MapWithAMarker from './MapWithAMarker';
import {
  getEditableCountryDetails,
  getCoronaCountryDashboard,
  downloadCountryReport,
} from '../../../admin/helpers/utils';
import './style.scss';
import CoronaCountryData from './components/CoronaCountryData';
import CoronaPhaseData from './components/CoronaPhaseData';
import CountryGraph from './components/CountryGraph';
import CountryPerHundGraph from './components/CountryPerHundGraph';
import CountryTotalGraph from './components/CountryTotalGraph';

class MapApi extends Component {
  state = {
    coronaDetails: [],
    coronaDash: [],
    countryId: '',
    vaccinationGraphObj: {
      labels: [],
      datasets: [
        {
          label: 'Vaccination Distribution',
          data: [],
          backgroundColor: 'rgba(231, 251, 189, 0.7)',
          borderColor: 'rgba(231, 251, 189, 1)',
          borderWidth: 2,
        },
      ],
    },
    vaccinationPerHundGraphObj: {
      labels: [],
      datasets: [
        {
          label: 'Vaccination Distribution',
          data: [],
          backgroundColor: 'rgba(	190, 229, 199, 0.7)',
          borderColor: 'rgba(	190, 229, 199, 1)',
          borderWidth: 2,
        },
      ],
    },
    vaccinationTotalGraphObj: {
      labels: [],
      datasets: [
        {
          label: 'Vaccination Distribution',
          data: [],
          backgroundColor: 'rgba(	138, 208, 206, 0.7)',
          borderColor: 'rgba(	138, 208, 206, 1)',
          borderWidth: 2,
        },
      ],
    },
  };

  componentDidMount() {
    this.vaccinationDetails(1);
  }

  vaccinationDetails = countryId => {
    this.props.setCountryId(countryId);

    getEditableCountryDetails(false, countryId).then(coronaDetails => {
      this.setState({ coronaDetails, countryId: countryId });
    });

    getCoronaCountryDashboard(false, countryId).then(coronaDash => {
      this.setState({ coronaDash }, () => {
        this.parseGraph();
        this.parsePerHundredGraph();
        this.parseTotalGraph();
      });
    });
  };

  parseGraph = () => {
    const { additionalvaccinegraph } = this.state.coronaDash;
    const { vaccinationGraphObj } = this.state;
    const labels = [];
    const dsObjOne = {
      label: 'Total Cases',
      data: [],
      backgroundColor: 'rgba(231, 251, 189, 0.7)',
      borderColor: 'rgba(231, 251, 189, 1)',
      borderWidth: 2,
      pointBackgroundColor: 'rgba(255,255,255,0)',
      pointBorderColor: 'rgba(255,255,255,0)',
      order: 2,
    };

    additionalvaccinegraph.map(i => {
      labels.push(i.x_point);
      dsObjOne.data.push(i.y_point);
    });

    vaccinationGraphObj['labels'] = labels;
    vaccinationGraphObj.datasets = [{ ...dsObjOne }];
    this.setState({ vaccinationGraphObj });
  };

  parsePerHundredGraph = () => {
    const { vaccineperhundredgraph } = this.state.coronaDash;
    const { vaccinationPerHundGraphObj } = this.state;
    const labels = [];
    const dsObjOne = {
      label: 'Total Cases',
      data: [],
      backgroundColor: 'rgba(	190, 229, 199, 0.7)',
      borderColor: 'rgba(	190, 229, 199, 1)',
      borderWidth: 2,
      pointBackgroundColor: 'rgba(255,255,255,0)',
      pointBorderColor: 'rgba(255,255,255,0)',
      order: 2,
    };

    vaccineperhundredgraph.map(i => {
      labels.push(i.x_point);
      dsObjOne.data.push(i.y_point);
    });

    vaccinationPerHundGraphObj['labels'] = labels;
    vaccinationPerHundGraphObj.datasets = [{ ...dsObjOne }];
    this.setState({ vaccinationPerHundGraphObj });
  };

  parseTotalGraph = () => {
    const { vaccinetotalnumbergraph } = this.state.coronaDash;
    const { vaccinationTotalGraphObj } = this.state;
    const labels = [];
    const dsObjOne = {
      label: 'Total Cases',
      data: [],
      backgroundColor: 'rgba(	138, 208, 206, 0.7)',
      borderColor: 'rgba(	138, 208, 206, 1)',
      borderWidth: 2,
      pointBackgroundColor: 'rgba(255,255,255,0)',
      pointBorderColor: 'rgba(255,255,255,0)',
      order: 2,
    };

    vaccinetotalnumbergraph.map(i => {
      labels.push(i.x_point);
      dsObjOne.data.push(i.y_point);
    });

    vaccinationTotalGraphObj['labels'] = labels;
    vaccinationTotalGraphObj.datasets = [{ ...dsObjOne }];
    this.setState({ vaccinationTotalGraphObj });
  };

  render() {
    const {
      coronaDetails,
      coronaDash,
      vaccinationGraphObj,
      vaccinationPerHundGraphObj,
      vaccinationTotalGraphObj,
    } = this.state;

    return (
      <div className="mapOutterWrap">
        <div id="vaccinationMapWrapper" className="vaccinationMapWrap">
          {coronaDetails && (
            <CoronaCountryData
              coronaDetails={coronaDetails}
              coronaDash={coronaDash}
            />
          )}
          {coronaDetails && <CoronaPhaseData coronaDetails={coronaDetails} />}
          <div id="countryCoronaGraphData">
            <div className="countryCoronaGraphData__box">
              {vaccinationTotalGraphObj && (
                <CountryTotalGraph
                  vaccinationGraphObj={vaccinationTotalGraphObj}
                  coronaDash={coronaDash}
                />
              )}
              {vaccinationGraphObj && (
                <CountryGraph
                  vaccinationGraphObj={vaccinationGraphObj}
                  coronaDash={coronaDash}
                />
              )}
              {vaccinationPerHundGraphObj && (
                <CountryPerHundGraph
                  vaccinationGraphObj={vaccinationPerHundGraphObj}
                  coronaDash={coronaDash}
                />
              )}
            </div>
          </div>
          <MapWithAMarker
            googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyCYOTLyn6xbysArlvlW4zpxnH9p6H3An08&v=3.exp&libraries=geometry,drawing,places"
            loadingElement={<div style={{ height: `100%` }} />}
            containerElement={
              <div style={{ height: `100vh`, position: 'relative' }} />
            }
            mapElement={<div style={{ height: `100%` }} />}
            coronaDetails={coronaDetails}
            vaccinationDetails={this.vaccinationDetails}
          />
        </div>
      </div>
    );
  }
}

export default MapApi;
