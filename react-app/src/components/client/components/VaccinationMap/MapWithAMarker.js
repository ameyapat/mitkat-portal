import React, { Component } from 'react';
import { withScriptjs, withGoogleMap, GoogleMap } from 'react-google-maps';
import CustomMarker from './CustomMarker';
import { getCovidCountries } from '../../../admin/helpers/utils';
import customIconCountry from '../../../../assets/icons/virus.png';
import customIconCountry2 from '../../../../assets/icons/virus-active.png';
import { exampleMapStyles } from './mapStyle';

@withScriptjs
@withGoogleMap
class MapWithAMarker extends Component {
  state = {
    countries: [],
    countryId: '',
    showMarkers: false,
    showLocation: true,
    latState: 17.71958,
    lngState: 88.785291,
    defaultOptions: {
      mapTypeControl: false,
      streetViewControl: false,
      zoomControl: false,
      fullscreenControl: false,
      draggable: true,
      styles: exampleMapStyles,
    },
    isActiveCountryId: null,
    hasZoom: null,
  };

  componentDidMount() {
    this.getCountries();
  }

  getCountries = () => {
    getCovidCountries(false).then(countries => {
      this.setState({ countries });
    });
  };

  setCenter = (lat, lng) =>
    this.setState({ latState: lat, lngState: lng, hasZoom: 5.0 });

  renderCustomMarkers = () => {
    const { countries } = this.state;
    return countries && countries.length > 0
      ? countries.map((data, index) => {
          let lat = parseFloat(data.latitude);
          let lng = parseFloat(data.longitude);

          return (
            <CustomMarker
              id={data.id}
              key={index}
              lat={lat}
              lng={lng}
              customIcon={customIconCountry}
              customIcon2={customIconCountry2}
              countryName={data.countryname}
              setCenter={this.setCenter}
              vaccinationDetails={this.props.vaccinationDetails}
              handleIsActive={this.handleIsActive}
              isActive={data.id === this.state.isActiveCountryId}
            />
          );
        })
      : 'No items Available';
  };

  handleIsActive = id => this.setState({ isActiveCountryId: id });

  render() {
    const {
      defaultOptions,
      countries,
      showMarkers,
      latState,
      lngState,
      hasZoom,
    } = this.state;

    return (
      <GoogleMap
        defaultOptions={defaultOptions}
        options={defaultOptions}
        defaultZoom={3.5}
        zoom={hasZoom || 3.5}
        defaultCenter={{ lat: latState, lng: lngState }}
        center={{ lat: latState, lng: lngState }}
        onTilesLoaded={() => this.setState({ showMarkers: true })}
      >
        {showMarkers && countries && this.renderCustomMarkers()}
      </GoogleMap>
    );
  }
}

export default MapWithAMarker;
