import React, { Component } from 'react';
import { Marker, InfoWindow } from 'react-google-maps';
import { withCookies } from 'react-cookie';

@withCookies
class CustomMarker extends Component {

  state = {
    coronaDetails: [],
    showInfoBox: false,
  };

  markerRef = React.createRef();
  
  showCountryDetails = (countryId) => {
      const { lat, lng } = this.props
      this.props.vaccinationDetails(countryId);
      this.props.handleIsActive(countryId)
      this.props.setCenter(lat, lng);
  }

   handleShowInfoBox = (show) => {
      this.setState({showInfoBox: show})
  }

  render() {
    let {
      id,
      lat,
      lng,
      countryName,
      customIcon,
      customIcon2,
      isClickable = true,
      isActive = false
    } = this.props;

    const { showInfoBox } = this.state;
   
    return (
            <Marker
              position={{ lat, lng }}
              onMouseOver={() => this.handleShowInfoBox(true)}
              onMouseOut={() => this.handleShowInfoBox(false)}
              onClick={() => isClickable ?  this.showCountryDetails(id) : console.log('This is not allowed!')}
              icon={ isActive ? customIcon2 : customIcon}
              ref={this.markerRef}
              optimized={false}
            >
              {
                showInfoBox &&
                <React.Fragment>
                    <InfoWindow
                        onCloseClick={() => this.handleShowInfoBox(false)}
                    >
                      <div onClick={() => this.showEventDetail(id)}>
                          <p className="alert--title">{countryName}</p>
                      </div>
                    </InfoWindow>                    
                </React.Fragment>
              }
            </Marker>
    );
  }
}

export default React.forwardRef((props, ref) => (
  <CustomMarker innerRef={ref} {...props} />
));
