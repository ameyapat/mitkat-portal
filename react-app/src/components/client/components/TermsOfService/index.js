import React, { Component } from 'react';
import policyList from './constant.json';
import './style.scss';

class TermsOfService extends Component {
  render() {
    return (
      <div id="termsOfService">
        {policyList.map(item => (
          <>
            <strong className="termsOfService-title">{item.header} </strong>
            <p className="termsOfService-detail">{item.description}</p>
          </>
        ))}
      </div>
    );
  }
}

export default TermsOfService;
