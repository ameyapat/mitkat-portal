import React, { Component } from 'react';
import Shimmer from '../../../ui/shimmer';
import { Spring } from 'react-spring/renderprops';
import RimeEventCard from '../rimeEventCard';

export function withShimmer(WrappedComponent) {
  return class extends Component {
    state = {
      showShimmer: true,
      showCard: true,
      timeoutValue: 5000,
    };

    componentDidMount() {
      setTimeout(() => {
        this.setState({ showShimmer: false });
      }, this.state.timeoutValue);
    }

    handleClose = () => {
      this.setState({ showCard: false });
    };

    render() {
      const { feed } = this.props;
      const { showShimmer, showCard } = this.state;
      return feed.length > 0 ? (
        <WrappedComponent {...this.props} />
      ) : showShimmer ? (
        <Spring
          from={{ opacity: 0.7, background: '#fff' }}
          to={{ opacity: 0.9, background: '#f1f1f1' }}
          reset={true}
          velocity={500}
          tension={280}
          friction={60}
        >
          {props => <Shimmer style={props} rows={8} />}
        </Spring>
      ) : (
        showCard && (
          <RimeEventCard
            key={'no-events'}
            title={'No Events Available, Please refresh after sometime.'}
            category={'No New Events'}
            hasClose={false}
            handleClose={this.handleClose}
          />
        )
      );
    }
  };
}
