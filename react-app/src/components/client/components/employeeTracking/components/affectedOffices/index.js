import React, { Component } from 'react';
import { connect } from 'react-redux';
import './style.scss';

class AffectedOffices extends Component {
  render() {
    const { affectedOffices = [] } = this.props.empTracking.empTrackingData;
    const { downloadExcelAssets } = this.props;
    return (
      <div id="affectedOffices">
        <h1>
          Asset List
          <button className="downloadBtn" onClick={() => downloadExcelAssets()}>
            <i className="fas fa-file-download"></i>
          </button>
          <span>( {affectedOffices.length} affected) </span>
        </h1>
        <div className="affectedOffices--wrap">
          {affectedOffices.length > 0 ? (
            <ul>
              {affectedOffices.map(item => (
                <li>
                  {' '}
                  <i className="fas fa-map-marker-alt"></i> &nbsp;
                  {item.entityName}{' '}
                </li>
              ))}
            </ul>
          ) : (
            <p>No Office location affected </p>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    empTracking: state.empTracking,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AffectedOffices);
