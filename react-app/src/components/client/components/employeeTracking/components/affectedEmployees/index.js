import React, { Component } from 'react';
import { connect } from 'react-redux';
import { API_ROUTES } from '../../../../../../helpers/http/apiRoutes';
import { fetchApi } from '../../../../../../helpers/http/fetch';
import { saveAs } from 'file-saver';
import './style.scss';
import store from '../../../../../../store';
import { toggleToast } from '../../../../../../actions';

class AffectedEmployees extends Component {
  render() {
    const { affectedEmployees = [] } = this.props.empTracking.empTrackingData;
    const { downloadExcelEmp } = this.props;
    return (
      <div id="affectedEmployees">
        <h1>
          Employee List
          <button className="downloadBtn" onClick={() => downloadExcelEmp()}>
            <i className="fas fa-file-download"></i>
          </button>
          <span>( {affectedEmployees.length} affected) </span>
        </h1>

        <div className="affectedEmployees--wrap">
          {affectedEmployees.length > 0 ? (
            <ul>
              {affectedEmployees.map(item => (
                <li>
                  {' '}
                  <i className="fas fa-user"></i> &nbsp;{item.entityName}{' '}
                </li>
              ))}
            </ul>
          ) : (
            <p>No Employee Affected </p>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    empTracking: state.empTracking,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AffectedEmployees);
