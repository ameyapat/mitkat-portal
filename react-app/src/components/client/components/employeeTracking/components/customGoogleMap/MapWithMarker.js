import React, { Component } from 'react';
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Circle,
} from 'react-google-maps';
import CustomMarker from './CustomMarker';
import { connect } from 'react-redux';
import AffectedEntitiesMapStylesLight from './AffectedEntitiesMapStylesLight.json';
import AffectedEntitiesMapStyles from './AffectedEntitiesMapStyles.json';

const defaultOptions = {
  mapTypeControl: true,
  streetViewControl: false,
  zoomControl: false,
  fullscreenControl: false,
};
const circleOptions = {
  options: {
    strokeColor: '#334FC9',
    strokeOpacity: 0.8,
    strokeWeight: 3,
    fillColor: '#334FC9',
    fillOpacity: 0.15,
  },
};
@withScriptjs
@withGoogleMap
@connect(state => {
  return {
    riskExposure: state.riskExposure,
    themeChange: state.themeChange,
  };
})
class MapWithMarker extends Component {
  state = {
    latState: 47.444244,
    lngState: 77.001999,
  };

  renderReportLocations = () => {
    let { reportLocations, reportLocationIcon, impactRadius } = this.props;
    let impRad = impactRadius * 1000;

    return reportLocations.map(item => {
      let lat = parseFloat(item.latitude);
      let lng = parseFloat(item.longitude);

      return (
        <>
          <CustomMarker
            id={item.id}
            key={item.id}
            lat={lat}
            lng={lng}
            entityName={item.entityName}
            customIcon={reportLocationIcon}
            showLocations={false}
          />
          <Circle
            defaultCenter={{
              lat,
              lng,
            }}
            radius={impRad}
            options={circleOptions.options}
          />
        </>
      );
    });
  };
  renderAffectedOffices = () => {
    let { affectedOffices, affectedOfficesIcon } = this.props;
    return affectedOffices.map(item => {
      return (
        <CustomMarker
          id={item.id}
          key={item.id}
          lat={item.latitude}
          lng={item.longitude}
          entityName={item.entityName}
          customIcon={affectedOfficesIcon}
          showLocations={false}
        />
      );
    });
  };
  renderAffectedEmployees = () => {
    let { affectedEmployees, affectedEmployeesIcon } = this.props;
    return affectedEmployees.map(item => {
      return (
        <CustomMarker
          id={item.id}
          key={item.id}
          lat={item.latitude}
          lng={item.longitude}
          entityName={item.entityName}
          customIcon={affectedEmployeesIcon}
          showLocations={false}
        />
      );
    });
  };

  render() {
    let {
      mapStyles,
      reportLocations,
      affectedOffices,
      affectedEmployees,
      themeChange: { setDarkTheme },
    } = this.props;
    let { latState, lngState } = this.state;

    return (
      <GoogleMap
        defaultZoom={1.9}
        defaultOptions={{
          ...defaultOptions,
          //styles: mapStyles
          styles: setDarkTheme
            ? AffectedEntitiesMapStyles
            : AffectedEntitiesMapStylesLight,
        }}
        defaultCenter={{
          lat: latState,
          lng: lngState,
        }}
      >
        {reportLocations.length > 0 && this.renderReportLocations()}
        {affectedOffices.length > 0 && this.renderAffectedOffices()}
        {affectedEmployees.length > 0 && this.renderAffectedEmployees()}
      </GoogleMap>
    );
  }
}

export default MapWithMarker;
