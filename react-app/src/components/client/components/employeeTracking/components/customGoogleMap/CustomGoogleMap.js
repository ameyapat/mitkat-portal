import React from 'react';
import MapWithMarker from './MapWithMarker';

const CustomGoogleMap = ({
  googleMapURL,
  loadingElement,
  containerElement,
  mapElement,
  reportLocations,
  affectedOffices,
  affectedEmployees,
  mapStyles,
  classes,
  customIcon,
  reportLocationIcon,
  affectedEmployeesIcon,
  affectedOfficesIcon,
  onPinClick,
  impactRadius,
}) => {
  return (
    <MapWithMarker
      googleMapURL={googleMapURL}
      loadingElement={loadingElement}
      containerElement={containerElement}
      mapElement={mapElement}
      reportLocations={reportLocations}
      affectedOffices={affectedOffices}
      affectedEmployees={affectedEmployees}
      mapStyles={mapStyles}
      classes={classes}
      customIcon={customIcon}
      reportLocationIcon={reportLocationIcon}
      affectedEmployeesIcon={affectedEmployeesIcon}
      affectedOfficesIcon={affectedOfficesIcon}
      onPinClick={onPinClick}
      impactRadius={impactRadius}
    />
  );
};

export default CustomGoogleMap;
