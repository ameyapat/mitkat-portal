import React, { Component } from 'react';
import { Marker, InfoWindow } from 'react-google-maps';
import './style.scss';

class CustomMarker extends Component {
  state = {
    showInfoBox: false,
  };

  markerRef = React.createRef();

  setShowInfoBox = show => this.setState({ showInfoBox: show });

  render() {
    const {
      id,
      lat,
      lng,
      customIcon,
      entityName = '',
      showLocations = true,
      onPinClick,
    } = this.props;
    const { showInfoBox } = this.state;

    return (
      <Marker
        onMouseOver={() => this.setShowInfoBox(true)}
        onMouseOut={() => this.setShowInfoBox(false)}
        position={{ lat, lng }}
        onClick={() =>
          onPinClick ? onPinClick(id) : console.log('Not Allowed!')
        }
        icon={customIcon}
        ref={this.markerRef}
        optimized={false}
      >
        {showInfoBox && (
          <>
            <InfoWindow onCloseClick={() => this.handleShowInfoBox(false)}>
              <>
                <p className="affected-entity">{entityName}</p>
              </>
            </InfoWindow>
          </>
        )}
      </Marker>
    );
  }
}

export default React.forwardRef((props, ref) => (
  <CustomMarker innerRef={ref} {...props} />
));
