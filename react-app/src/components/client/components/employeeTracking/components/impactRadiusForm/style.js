export default {
  textField: {
    marginBottom: '10px',
    color: 'var(--whiteHighEmp)',
  },
  underline: {
    borderBottom: '1px solid rgba(255,255,255,0.67)',
    '&:after': {
      borderBottom: '2px solid #f1f1f1',
    },
  },
  cssLabel: {
    color: 'var(--whiteHighEmp) !important',
  },
  cssLabelRoot: {
    color: 'var(--whiteHighEmp)',
  },
};
