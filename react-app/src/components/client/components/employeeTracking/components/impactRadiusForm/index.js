import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import style from './style';
import './style.scss';
import { TextField } from '@material-ui/core';

@withStyles(style)
class ImpactRadiusForm extends Component {
  handleInputChange = e => {
    this.props.handleInputChange(e);
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.getAffectedEntities();
  };

  render() {
    let { classes } = this.props;
    let { impactRadius } = this.props;

    return (
      <div id="impactRadiusForm">
        <TextField
          required
          id="impactRadius"
          label="Impact Radius"
          value={impactRadius}
          onChange={e => this.handleInputChange(e)}
          defaultValue="Enter Impact Radius"
          InputLabelProps={{
            classes: {
              focused: classes.cssLabel,
              root: classes.cssLabelRoot,
            },
          }}
          InputProps={{
            classes: {
              root: classes.textField,
              underline: classes.underline,
            },
          }}
          type="number"
        />
        <small>
          <em> (In Kms)</em>
        </small>
        <button className="submitBtn" onClick={this.handleSubmit}>
          Submit
        </button>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    empTracking: state.empTracking,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch,
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(ImpactRadiusForm);
