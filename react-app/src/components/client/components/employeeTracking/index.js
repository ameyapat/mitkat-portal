import React, { Component } from 'react';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import { fetchApi } from '../../../../helpers/http/fetch';
import { withCookies } from 'react-cookie';
import './style.scss';
import AffectedEmployees from './components/affectedEmployees';
import AffectedOffices from './components/affectedOffices';
import { setEmpTrackingData } from '../../../../actions/employeeTrackingActions';
import { connect } from 'react-redux';
import CustomGoogleMap from './components/customGoogleMap';
import customMapStyle from '../../../../helpers/universalMapStyles.json';

import reportLocationIcon from '../../../../assets/solid-pins/reportLocation.png';
import affectedEmployeesIcon from '../../../../assets/solid-pins/empLocation.png';
import affectedOfficesIcon from '../../../../assets/solid-pins/officeLocation.png';
import ImpactRadiusForm from './components/impactRadiusForm';
import { RISK_LEVEL_COLORS_NEON } from '../../../../helpers/constants';
import { saveAs } from 'file-saver';
import { toggleToast } from '../../../../actions';
import store from '../../../../store';
import { hasLength } from '../../../../helpers/utils';

@withCookies
class EmployeeTracking extends Component {
  state = {
    impactRadiusState: 0,
    datalist: [],
    affectedEntitiesData: [],
  };

  componentDidMount() {
    this.setState(
      {
        impactRadiusState: this.props.eventDetail.impactRadius,
      },
      () => {
        this.getAffectedEntities();
      },
    );
  }

  componentDidUpdate(prevProps) {
    if (prevProps.radius !== this.props.radius) {
      const impactRadius = this.props.radius;
      this.setState({ impactRadiusState: impactRadius }, () => {
        this.getAffectedEntities();
      });
    }
  }

  getAffectedEntities = () => {
    const { eventId } = this.props;
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let dispatchData = {
      eventid: eventId,
      impactRadius: this.state.impactRadiusState,
    };
    let reqObj = {
      url: API_ROUTES.getAffectedEntities,
      data: dispatchData,
      isAuth: true,
      authToken,
      method: 'POST',
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        this.setState({ affectedEntitiesData: data }, () => {
          this.props.dispatch(setEmpTrackingData(data));
        });
      })
      .catch(e => console.log(e));
  };

  downloadExcelEmp = () => {
    const { eventId } = this.props;
    const { impactRadius } = this.props.empTracking.empTrackingData;
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let dispatchData = {
      eventid: eventId,
      impactRadius: impactRadius,
    };
    let reqObj = {
      url: `${API_ROUTES.downloadAffectedEmployees}`,
      isAuth: true,
      authToken,
      method: 'POST',
      isBlob: true,
      data: dispatchData,
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(blobObj => {
        let objUrl = new Blob([blobObj], {
          type: 'application/vnd.ms-excel',
        });
        saveAs(objUrl, 'affected-employees.xlsx');

        let toastObj = {
          toastMsg: 'Downloading file...',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      })
      .catch(e => {
        let toastObj = {
          toastMsg: 'Error downloading excel sheet',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      });
  };

  downloadExcelAssets = () => {
    const { eventId } = this.props;
    const { impactRadius } = this.props.empTracking.empTrackingData;
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let dispatchData = {
      eventid: eventId,
      impactRadius: impactRadius,
    };
    let reqObj = {
      url: `${API_ROUTES.downloadAffectedAssets}`,
      isAuth: true,
      authToken,
      method: 'POST',
      isBlob: true,
      data: dispatchData,
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(blobObj => {
        let objUrl = new Blob([blobObj], {
          type: 'application/vnd.ms-excel',
        });
        saveAs(objUrl, 'affected-assets.xlsx');

        let toastObj = {
          toastMsg: 'Downloading file...',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      })
      .catch(e => {
        let toastObj = {
          toastMsg: 'Error downloading excel sheet',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      });
  };

  handleInputChange = e => {
    this.setState({ impactRadiusState: e.target.value });
  };

  callMaps = () => {
    let MapXML = [];
    const {
      affectedEmployees = [],
      affectedOffices = [],
      reportLocations = [],
      impactRadius,
    } = this.props.empTracking.empTrackingData;
    if (
      this.state.affectedEntitiesData === this.props.empTracking.empTrackingData
    ) {
      MapXML = (
        <CustomGoogleMap
          googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyCYOTLyn6xbysArlvlW4zpxnH9p6H3An08&v=3.exp&libraries=geometry,drawing,places"
          loadingElement={<div style={{ height: `85vh` }} />}
          containerElement={
            <div style={{ height: `85vh`, position: 'relative' }} />
          }
          mapElement={<div style={{ height: `85vh` }} />}
          reportLocations={hasLength(reportLocations)}
          affectedOffices={hasLength(affectedOffices)}
          affectedEmployees={hasLength(affectedEmployees)}
          mapStyles={customMapStyle}
          reportLocationIcon={reportLocationIcon}
          affectedEmployeesIcon={affectedEmployeesIcon}
          affectedOfficesIcon={affectedOfficesIcon}
          impactRadius={impactRadius}
        />
      );
    }
    return MapXML;
  };

  render() {
    const {
      title,
      risklevel,
      riskCategoryString,
      impactRadius,
    } = this.props.empTracking.empTrackingData;

    return (
      <>
        <div id="employeeTrackingMap">{this.callMaps()}</div>
        <div id="employeeTrackingData">
          <div className="employeeTracking">
            <div className="affectedOffices--wrap">
              <h1>
                <i className="fas fa-location-arrow"></i> &nbsp; {title}
              </h1>

              <span
                style={{
                  color: RISK_LEVEL_COLORS_NEON[risklevel],
                }}
              >
                {riskCategoryString}
              </span>

              <ImpactRadiusForm
                impactRadius={this.state.impactRadiusState}
                handleInputChange={this.handleInputChange}
                getAffectedEntities={this.getAffectedEntities}
              />
            </div>
          </div>
          <div className="employeeTrackingList">
            <AffectedEmployees downloadExcelEmp={this.downloadExcelEmp} />
            <AffectedOffices downloadExcelAssets={this.downloadExcelAssets} />
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    empTracking: state.empTracking,
    radius: state.empTracking.impactRadius,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EmployeeTracking);
