import React, { useEffect, useState } from 'react';
import './style.scss';
import moment from 'moment';
import {
  categories,
  subRiskCategories,
} from '../rimeDashboard/helpers/constants';
import ArticleModal from './articleModal';
// common style
import '../../sass/style.scss';
//Card
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { useSelector } from 'react-redux';
import CustomModal from '../../../ui/modal';
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye';
import TagsIcon from '../../../../assets/svg/tagsIcon.svg';
import { lastUpdateDate, eDate } from '../rimeDashboard/helpers/utils';

const pinMappingRgba = {
  1: 'low',
  2: 'medium',
  3: 'high',
};

const RimeEventCard = props => {
  const {
    title,
    categoryId,
    secondaryCategoryId,
    subRiskCategoryId,
    link,
    eventDate,
    relevancy,
    description,
    sourceLinks,
    location,
    noOfArticles,
    lastUpdatedDate,
    tags,
    language,
  } = props;

  const [isNew, setIsNew] = useState(true);
  const [category, setCategory] = useState('');
  const [secondaryCategory, setSecondaryCategory] = useState('');
  const [subRiskCategory, setSubRiskCategory] = useState('');
  const setTheme = useSelector(state => state.themeChange.setDarkTheme);
  const refreshDuration = useSelector(state => state.rime.refreshDuration);
  const [showTags, setShowTags] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const TIMEOUT = refreshDuration * 60000;

  const mapRiskCategory = () => {
    categories.map(item => {
      if (categoryId === item.id) {
        setCategory(item.label);
      }
    });
    subRiskCategories.map(item => {
      if (subRiskCategoryId === item.id) {
        setSubRiskCategory(item.label);
      }
    });
  };

  const handleModal = () => {
    setShowModal(false);
  };

  const showArticleModal = () => {
    setShowModal(true);
  };

  const showTagsDiv = () => {
    setShowTags(prevCheck => !prevCheck);
  };

  useEffect(() => {
    mapRiskCategory();
    setTimeout(() => {
      setIsNew(false);
    }, TIMEOUT);
  });

  let tag = tags && tags.split(',');
  let allLinks = sourceLinks && sourceLinks.split(',');

  const utcLastUpdateDate = lastUpdateDate(lastUpdatedDate);
  const utcDate = eDate(eventDate);

  return (
    <div id="rime__liveMonitor__eventCard">
      <Card
        className={`rime__liveMonitor__eventCardList ${pinMappingRgba[relevancy]}`}
      >
        <div className="liveMonitor__header">
          <div
            className={`liveMonitor__pill ${relevancy &&
              pinMappingRgba[relevancy]}`}
          >
            <Typography
              component="span"
              className="liveMonitor__cat__pill"
              style={{
                fontWeight: 500,
              }}
            >
              {category ? category : 'Not Available'}
              {subRiskCategoryId !== 0 && subRiskCategoryId !== null && (
                <span className="liveMonitor__cat__pill_space">,</span>
              )}
              {subRiskCategoryId !== 0 && subRiskCategory}
            </Typography>
          </div>
          <div className="liveMonitor__card__innercolumn">
            <div>
              <Typography
                component="span"
                className="rime__liveMonitor__eventDate"
              >
                &nbsp;
                {moment(utcDate).format('Do MMM YYYY')}
              </Typography>
              <Typography
                variant="subtitle1"
                color="text.secondary"
                component="span"
                className="rimeupdate__liveMonitor__date"
              >
                Updated:&nbsp;
              </Typography>
              <Typography
                component="span"
                className="rimeupdate__liveMonitor__date"
              >
                {moment(utcLastUpdateDate).format('Do MMM YYYY, h:mm:ss a')}
              </Typography>
            </div>
          </div>
        </div>
        <div className="liveMonitor__card__row1">
          <div className="liveMonitor__card__innercolumn2">
            <Box sx={{ display: 'flex', flexDirection: 'column' }}>
              <CardContent sx={{ flex: '1 0 auto' }}>
                <Typography
                  component="div"
                  className="liveMonitor__eventtitle__wrap"
                >
                  {title.substring(0, 79)}
                  {title.length > 79 ? '...' : ''}
                </Typography>
                <div className="liveMonitor__description">
                  <Typography
                    component="div"
                    className="liveMonitor__eventbody__wrap"
                  >
                    {description
                      ? description.substring(0, 110)
                      : 'Not Available'}
                    <span style={{ color: 'var(--link)' }}>
                      {description && (
                        <a className="anchor__text" href={link} target="_blank">
                          ...read more
                        </a>
                      )}
                    </span>
                  </Typography>
                </div>
                <div className="liveMonitor_row_2">
                  <div className="language_section">
                    <Typography
                      variant="subtitle1"
                      color="text.secondary"
                      component="span"
                      className="liveMonitor__eventbody__subHead"
                    >
                      Location:
                    </Typography>
                    <Typography
                      component="span"
                      className="liveMonitor__eventbody__location_wrap"
                    >
                      &nbsp;{location ? location : 'Not Available'}
                    </Typography>
                  </div>
                  <div>
                    <Typography
                      variant="subtitle1"
                      color="text.secondary"
                      component="span"
                      className="liveMonitor__eventbody__subHead"
                    >
                      Language:
                    </Typography>
                    <Typography
                      component="span"
                      className="liveMonitor__eventbody__wrap"
                    >
                      &nbsp;{language ? language : 'Not Available'}
                    </Typography>
                  </div>
                </div>
              </CardContent>
            </Box>
          </div>
        </div>
        <div className="liveMonitor__card__row2">
          <Box sx={{ display: 'flex', flexDirection: 'column' }}>
            <CardContent sx={{ flex: '1 0 auto' }}>
              <div className="liveMonitor__card_footer">
                <div
                  className="liveMonitor__article__content__pill"
                  onClick={() => showArticleModal()}
                >
                  <Typography
                    variant="subtitle1"
                    color="text.secondary"
                    component="span"
                    className="liveMonitor__eventbody__Head"
                    style={{ marginTop: '-1px' }}
                  >
                    No of Articles:
                  </Typography>
                  <Typography
                    component="span"
                    className="liveMonitor__eventbody__Head"
                  >
                    &nbsp;
                    {noOfArticles ? noOfArticles : 'Not Available'}
                  </Typography>
                  <span>&nbsp;</span>
                  <RemoveRedEyeIcon className="eyeIcon" />
                </div>
                <div
                  className="liveMonitor__article__content__pill"
                  onClick={() => showTagsDiv()}
                >
                  <Typography
                    variant="subtitle1"
                    color="text.secondary"
                    component="span"
                    className="liveMonitor__eventbody__Head"
                    style={{ marginTop: '-1px' }}
                  >
                    Tags
                  </Typography>
                  <span>&nbsp;&nbsp;</span>
                  <img className="tagsIcon" src={TagsIcon} />
                </div>
              </div>
              <div className="liveMonitor__rime__tags">
                {showTags &&
                  tags &&
                  tag.length > 0 &&
                  tag.map(subitem => {
                    return <span>#{subitem}</span>;
                  })}
              </div>
            </CardContent>
          </Box>
        </div>
      </Card>
      <CustomModal showModal={showModal}>
        <div
          id="rimeEventArchivesModalCont"
          className={`modal__inner ${setTheme ? 'dark' : 'light'}`}
        >
          <ArticleModal
            alllinks={allLinks}
            closeEventModal={() => handleModal()}
          />
        </div>
      </CustomModal>
    </div>
  );
};

export default RimeEventCard;
