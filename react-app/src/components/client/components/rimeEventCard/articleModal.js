import React, { Component } from 'react';

class ArticleModal extends Component {
  render() {
    const { alllinks, closeEventModal } = this.props;
    return (
      <div>
        <div className="rootFilters__header">
          <h1>List of Articles </h1>
          <span classname="btn--close" onClick={() => closeEventModal()}>
            &times;
          </span>
        </div>
        <div className="rootFilters__body">
          {alllinks?.length > 0 &&
            alllinks.map((subitem, srno) => {
              return (
                <div className="rimeModal_links">
                  <a className="anchor__text" href={subitem} target="_blank">
                    {srno + 1}).&nbsp;{subitem}
                  </a>
                </div>
              );
            })}
        </div>
      </div>
    );
  }
}

export default ArticleModal;
