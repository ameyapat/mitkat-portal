import React, { Component } from 'react';
import './RimeLiveListItems.scss';
import { Transition } from 'react-spring/renderprops';
import { connect } from 'react-redux';
import { setLiveListItemDetails } from '../../../../actions/rimeActions';
import RimeEventCard from '../rimeEventCard';

class RimeLiveListItems extends Component {
  handleShowLiveEvent = item => {
    const { dispatch } = this.props;
    dispatch(setLiveListItemDetails({ ...item, showMarker: true }));
  };

  handleHideLiveEvent = () => {
    const { dispatch } = this.props;
    dispatch(setLiveListItemDetails({ showMarker: false }));
  };

  render() {
    const { dynamicHeight, feed, animate = true } = this.props;
    return (
      <div className="liveListItems" style={{ height: dynamicHeight }}>
        <ul id="liveList">
          <Transition
            items={feed}
            keys={item => item.eventId}
            from={{ transform: 'translate3d(0, -40px, 0)', opacity: 0 }}
            enter={{ transform: 'translate3d(0,0,0)', opacity: 1 }}
            update={{ transform: 'translate3d(0,0,0)', opacity: 1 }}
            reset={true}
            unique={true}
            initial={animate === true ? animate : null}
          >
            {item => props => {
              return (
                <>
                  {feed.includes(item) ? (
                    <li
                      style={props}
                      onMouseEnter={() =>
                        this.props.rime.lockScreen &&
                        this.handleShowLiveEvent(item)
                      }
                      onMouseLeave={() => this.handleHideLiveEvent()}
                    >
                      <RimeEventCard
                        title={item.eventTitle}
                        description={item.eventDescription}
                        sourceLinks={item.eventAllNewsLinks}
                        location={item.eventLoc}
                        noOfArticles={item.count}
                        categoryId={item.eventPrimaryRiskCategory}
                        secondaryCategoryId={item.eventSecondaryRiskCategory}
                        link={item.eventLink}
                        hasClose={false}
                        eventDate={item.eventDate}
                        lastUpdatedDate={item.eventLatestUpdate}
                        relevancy={item.eventRiskLevel}
                        imgURL={item.eventImageUrl}
                        tags={item.eventTags}
                        language={item.language}
                      />
                    </li>
                  ) : null}
                </>
              );
            }}
          </Transition>
        </ul>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatch,
  };
};

const mapStateToProps = state => {
  return {
    rime: state.rime,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RimeLiveListItems);
