import React, { Component } from 'react';
import moment from 'moment';
import './style.scss';

class RimeStatistics extends Component {
  render() {
    return (
      <div>
        <div className="firstRow">
          <div className="numbers">{this.props.feedLength}</div>
          <div className="header">Total Live Events</div>
        </div>
        <div className="secondrow">
          <div className="date">
            {moment(new Date(this.props.firstHitTime).valueOf()).format('ll')}
          </div>
          <div className="time">
            {moment(new Date(this.props.firstHitTime).valueOf()).format('LT')}
          </div>
          <div className="header">API Start Time</div>
        </div>
        <div className="secondrow">
          <div className="date">
            {moment(new Date(this.props.lastHitTime).valueOf()).format('ll')}
          </div>
          <div className="time">
            {moment(new Date(this.props.lastHitTime).valueOf()).format('LT')}
          </div>
          <div className="header">API Last Hit Time</div>
        </div>
      </div>
    );
  }
}

export default RimeStatistics;