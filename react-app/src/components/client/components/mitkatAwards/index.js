import React, { Component } from 'react';
import './style.scss';
import awardList from './constant.json';
import finestIndiaImg from '../../../../assets/awards/finestIndiaImg.jpeg';
import ospasImg from '../../../../assets/awards/ospasImg.png';
import VerticleTimeline from '../../../ui/verticleTimeline';
import TimelineList from '../../../ui/verticleTimeline/timelineList';

class Awards extends Component {
  render() {
    return (
      <>
        <VerticleTimeline header="Awards" />
        <ul className="timeline">
          {awardList.map(item => (
            <TimelineList
              dataDate={item.dataDate}
              title={item.title}
              img={item.img}
              description={item.description}
            />
          ))}
        </ul>
      </>
    );
  }
}

export default Awards;
