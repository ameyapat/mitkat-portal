import React, { Component } from 'react';
import { Switch, Route, withRouter } from 'react-router-dom';
import './style.scss';
import GMap from '../GMap';
import LocationAnalysis from '../locationAnalysis';
import ClientLocationAnalysis from '../clientLocationAnalysis';
import EventCalendar from '../../pages/EventCalendar/EventCalendar';
import WeeklyForecast from '../../pages/WeeklyForecast/WeeklyForecast';
import KaleidoscopePageClient from '../../pages/KaleidoscopePageClient';
import riskExposure from '../riskExposure';
import Advisories from '../advisories';
import TravelAdvisories from '../travelAdvisories';
import QuerySystem from '../querySystem';
import KEvent from '../../pages/KaleidoscopePageClient/KEvent';
import PrivacyPolicy from '../privacyPolicy';
import Awards from '../mitkatAwards';
import Certificates from '../mitkatCertificates';
import TermsOfService from '../termsOfService';
import UserManagement from '../userManagement';
import LocationManagement from '../locationManagement';
import { routeItems } from './helpers/constant';
import { connect } from 'react-redux';

const componentRegistry = {
  GMap,
  LocationAnalysis,
  ClientLocationAnalysis,
  EventCalendar,
  WeeklyForecast,
  KaleidoscopePageClient,
  riskExposure,
  Advisories,
  TravelAdvisories,
  QuerySystem,
  KEvent,
  PrivacyPolicy,
  Awards,
  Certificates,
  TermsOfService,
  UserManagement,
  LocationManagement,
};

@withRouter
@connect(state => {
  return {
    themeChange: state.themeChange,
  };
})
class ClientDashboard extends Component {
  getRouteList = () => {
    let routetXML = [];
    let { routeOptions } = routeItems;
    routetXML = routeOptions.map(item => (
      <Route
        exact={item.exact}
        path={`${this.props.match[item.path]}${item.route}`}
        component={componentRegistry[item.components]}
      />
    ));
    return routetXML;
  };

  render() {
    const {
      themeChange: { setDarkTheme },
    } = this.props;
    return (
      <div
        id="clientDashboard"
        className={`${setDarkTheme ? 'dark' : 'light'}`}
      >
        <Switch>{this.getRouteList()}</Switch>
      </div>
    );
  }
}

export default ClientDashboard;
