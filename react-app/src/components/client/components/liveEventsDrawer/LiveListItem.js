import React from 'react';
import { withStyles } from '@material-ui/core';

const styles = {
    rootListItem:{
        borderBottom: '1px solid rgba(29, 28, 29, 0.2)',
        padding: 10,

        '& p':{
            fontSize: 14,
            color: '#333'
        },
        '& span':{
            fontSize: 12,
            color: '#666'
        }
    }
}

const LiveListItem = ({title, date, classes}) => (
    <div className={classes.rootListItem}>
        <span>{date}</span>
        <p dangerouslySetInnerHTML={{__html: title}}></p>
    </div>
)

export default withStyles(styles)(LiveListItem);