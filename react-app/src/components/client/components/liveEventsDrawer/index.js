import React, { Component } from 'react';
import Drawer from '@material-ui/core/Drawer';
import { withStyles } from '@material-ui/core/styles';
import LiveEventsList from './LiveEventsList';

const drawerWidth = 420;

const styles = theme => ({    
  drawerPaper: {
    width: drawerWidth
  },
  header: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#F8F8F8',
    borderBottom: '1px solid rgba(29, 28, 29, 0.2)',
    padding: 10,
    '& h1': {
      fontSize: 14,    
      color: '#444'
    }
  },
  close: {
    fontSize: 24,
    color: '#838283',
    cursor: 'pointer'
  }
});

@withStyles(styles, { withTheme: true })
class LiveEventsDrawer extends Component {
  render() {
    let { classes, open, handleDrawerClose, theme, trackingId } = this.props;
    return (
      <Drawer
        // variant="persistent"
        // anchor="left"
        classes={{          
          paper: classes.drawerPaper
        }}
        open={open}
        onClose={() => handleDrawerClose(false)}
      >
        <div className={classes.header}>
          <h1>Live Updates</h1>
          <span
            className={classes.close}
            onClick={() => handleDrawerClose(false)}
          >
            &times;
          </span>
        </div>
        <LiveEventsList trackingId={trackingId} />
      </Drawer>
    );
  }
}

export default LiveEventsDrawer;
