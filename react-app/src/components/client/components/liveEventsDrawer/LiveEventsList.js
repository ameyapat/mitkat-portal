import React, { Component } from 'react';
import LiveListItem from './LiveListItem';
import { withCookies } from 'react-cookie';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import { fetchApi } from '../../../../helpers/http/fetch';
import Empty from '../../../ui/empty';
//assets
import emptyBox from '../../../../assets/empty-box.svg';
import { withStyles } from '@material-ui/core';

const styles = {
    rootLiveEventsWrap:{
        overflowY: 'auto',
    },
    boxWrap:{
        marginTop: 35
    }
}

@withCookies
@withStyles(styles)
class LiveEventsList extends Component{

    state = {
        liveEvents: [],
        dynamicHeight: ''
    }

    componentDidMount(){        
        this.getLiveEvents();
        const rootEl = document.querySelector('#rootLiveEventsWrap');
        const windowHeight = window.innerHeight;
        
        this.setState({ dynamicHeight: windowHeight - rootEl.getBoundingClientRect().top })
    }

    getLiveEvents = () => {
        let { cookies, trackingId } = this.props;
        let authToken = cookies.get("authToken");

        let reqObj = {
            url: `${API_ROUTES.trackLiveEvents}/${trackingId}`,
            method: "POST",
            isAuth: true,
            authToken,
            showToggle: true,
        };

        fetchApi(reqObj)
        .then(data => {
            this.setState({ liveEvents: data })
        })
        .catch(e => {
            console.log(e);
        });
    }

    getEventsXML = () => {
        let { liveEvents } = this.state;
        let liveEventsXML = [];

        return liveEventsXML = liveEvents.map((item, idx) => <LiveListItem key={item.id} title={item.title} date={item.timeupdate} />)
    }

    render(){
        let { classes } = this.props;
        let { liveEvents, dynamicHeight } = this.state;

        return(
            <div id="rootLiveEventsWrap" className={classes.rootLiveEventsWrap} style={{height: dynamicHeight}}>
                {
                    liveEvents.length > 0
                    ?
                    this.getEventsXML()
                    :
                    <Empty
                        alt="empty-box" 
                        imgSrc={emptyBox} 
                        title={"No events available!"} 
                        classes={{
                            box: classes.boxWrap
                        }}
                    />
                }
            </div>
        )
    }
}

export default LiveEventsList;