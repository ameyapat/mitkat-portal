import React, { Component } from 'react';
import { Radar } from 'react-chartjs-2';
import './style.scss';
import { connect } from 'react-redux';

@connect(state => {
  return {
    themeChange: state.themeChange,
  };
})
class RiskCategoryChart extends Component {
  getRadarChartData(props) {
    let riskCategoryData = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    let { chartFeed } = props;
    chartFeed?.length &&
      chartFeed.map(
        item => {
          riskCategoryData[item[0].eventPrimaryRiskCategory] += 1;
        });
    return riskCategoryData;
  }

  render() {
    const {
      themeChange: { setDarkTheme },
    } = this.props;
    const RadarData = {
      labels: [
        'Others',
        'Cyber / Technology',
        'Extremism',
        'Civil Disturbance',
        'Environment',
        'Health',
        'Critical Infrastructure',
        'Crime',
        'Travel Risks',
        'Natural Disasters',
        'External Threats',
        'Political',
        'Regulatory',
      ],
      datasets: [
        {
          backgroundColor: 'rgba(13, 153, 255, .5)',
          borderColor: 'rgba(13, 153, 255, 0.7)',
          borderWidth: 2,
          pointBackgroundColor: 'rgba(13, 153, 255, 1)',
          poingBorderColor: '#fff',
          pointHoverBackgroundColor: '#fff',
          pointHoverBorderColor: 'rgba(13, 153, 255, 1)',
          data: this.getRadarChartData(this.props),
        },
      ],
    };
    const RadarOptions = {
      scale: {
        ticks: {
          min: 0,
          max: Math.max(...this.getRadarChartData(this.props)),
          stepSize: Math.max(...this.getRadarChartData(this.props)) / 5,
          showLabelBackdrop: false,
          display: false,
          backdropColor: setDarkTheme
            ? 'rgba(255, 255, 255, 0.87)'
            : 'rgba(0, 0, 0, 0.75)',
        },
        angleLines: {
          color: setDarkTheme
            ? 'rgba(255, 255, 255, 0.50)'
            : 'rgba(0, 0, 0, 0.50)',
          lineWidth: 1.5,
        },
        gridLines: {
          color: setDarkTheme
            ? 'rgba(255, 255, 255, 0.50)'
            : 'rgba(0, 0, 0, 0.50)',
        },
        pointLabels: {
          fontColor: setDarkTheme
            ? 'rgba(255, 255, 255, 0.87)'
            : 'rgba(0, 0, 0, 0.95)',
        },
      },
      legend: {
        display: false,
      },
    };

    return (
      <div className="radarChart">
        <Radar data={RadarData} options={RadarOptions} height={170} />
      </div>
    );
  }
}

export default RiskCategoryChart;
