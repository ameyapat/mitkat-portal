import React, { Component } from 'react';
import { getLocationValue } from '../../helpers/utils';
import GeoFencingEventCard from './geofencingEventCard';
import GeoFencingNoEventCard from './geofencingNoEventCard';
import RiskCategoryChart from './riskCategoryChart';
import './style.scss';

class GeoFencingData extends Component {
  renderGeoFencingEventList = () => {
    const { geoFencingFeed } = this.props;
    return geoFencingFeed.map(item => (
      <GeoFencingEventCard
        locationDetails={getLocationValue(item[1])}
        title={item[0].eventTitle}
        description={item[0].eventDescription}
        sourceLinks={item[0].eventAllNewsLinks}
        location={item[0].eventLoc}
        noOfArticles={item[0].count}
        categoryId={item[0].eventPrimaryRiskCategory}
        secondaryCatergoryId={item[0].eventSecondaryRiskCategory}
        subRiskCategoryId={item[0].subRiskCategory}
        link={item[0].eventLink}
        hasClose={false}
        eventDate={item[0].eventDate}
        lastUpdatedDate={item[0].eventLatestUpdate}
        relevancy={item[0].eventRiskLevel}
        imgURL={item[0].eventImageUrl}
        tags={item[0].eventTags}
        language={item[0].language}
      />
    ));
  };

  render() {
    const { geoFencingFeed } = this.props;
    return (
      <>
        <RiskCategoryChart
          chartFeed={geoFencingFeed?.length && this.props.geoFencingFeed}
        />
        <div className="eventList">
          <div className="geoFencingHeader">Geo-Fencing Events</div>
          <div className="geoFenceEventCard">
            {geoFencingFeed.length ? (
              this.renderGeoFencingEventList()
            ) : (
              <GeoFencingNoEventCard />
            )}
          </div>
        </div>
      </>
    );
  }
}

export default GeoFencingData;
