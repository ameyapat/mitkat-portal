import React, { useEffect, useState } from 'react';
import './style.scss';
// common style
import '../../sass/style.scss';
//Card
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { useSelector } from 'react-redux';
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye';
import TagsIcon from '../../../../assets/svg/tagsIcon.svg';
import PlaceIcon from '@mui/icons-material/Place';

const pinMappingRgba = {
  1: 'low',
  2: 'medium',
  3: 'high',
};

const GeoFencingNoEventCard = props => {
  const { relevancy } = props;
  const setTheme = useSelector(state => state.themeChange.setDarkTheme);

  return (
    <div id="rime__geoFencing__eventCard">
      <Card
        className={`rime__geoFencing__eventCardList ${pinMappingRgba[relevancy]}`}
      >
        <div className="geoFencing__header">
          <div
            className={`geoFencing__pill ${relevancy &&
              pinMappingRgba[relevancy]}`}
          >
            <Typography
              component="span"
              className="geoFencing__cat__pill"
              style={{
                fontWeight: 500,
              }}
            >
              Not Available
            </Typography>
          </div>
          <div className="geoFencing__card__innercolumn">
            <div>
              <Typography
                component="span"
                className="rime__geoFencing__eventDate"
              >
                &nbsp; Not Available
              </Typography>
              <Typography
                variant="subtitle1"
                color="text.secondary"
                component="span"
                className="rimeupdate__geoFencing__date"
              >
                Updated:&nbsp;
              </Typography>
              <Typography
                component="span"
                className="rimeupdate__geoFencing__date"
              >
                Not Available
              </Typography>
            </div>
          </div>
        </div>
        <div className="geoFencing__card__row1">
          <div className="geoFencing__card__innercolumn2">
            <Box sx={{ display: 'flex', flexDirection: 'column' }}>
              <CardContent sx={{ flex: '1 0 auto' }}>
                <Typography
                  component="div"
                  className="geoFencing__eventtitle__wrap"
                >
                  No events available, Please refresh after some time.
                </Typography>
              </CardContent>
            </Box>
          </div>
        </div>
        <div className="distanceFromOffice">
          <PlaceIcon className="locationIcon" />
          0.0 kms from not available location
        </div>
        <div className="eventsearch__card__row2">
          <Box sx={{ display: 'flex', flexDirection: 'column' }}>
            <CardContent sx={{ flex: '1 0 auto' }}>
              <div className="rime__eventSearch__spacing"></div>
              <div className="geoFencing__card_footer">
                <div className="geoFencing__article__content__pill">
                  <Typography
                    variant="subtitle1"
                    color="text.secondary"
                    component="span"
                    className="geoFencing__eventbody__Head"
                    style={{ marginTop: '-1px' }}
                  >
                    No of Articles:
                  </Typography>
                  <Typography
                    component="span"
                    className="geoFencing__eventbody__Head"
                  >
                    &nbsp; Not Available
                  </Typography>
                  <span>&nbsp;</span>
                  <RemoveRedEyeIcon className="eyeIcon" />
                </div>
                <div className="geoFencing__article__content__pill">
                  <Typography
                    variant="subtitle1"
                    color="text.secondary"
                    component="span"
                    className="geoFencing__eventbody__Head"
                    style={{ marginTop: '-1px' }}
                  >
                    Tags
                  </Typography>
                  <span>&nbsp;&nbsp;</span>
                  <img className="tagsIcon" src={TagsIcon} />
                </div>
              </div>
            </CardContent>
          </Box>
        </div>
      </Card>
    </div>
  );
};

export default GeoFencingNoEventCard;
