import React, { useEffect, useState } from 'react';
import './style.scss';
import moment from 'moment';
import {
  categories,
  subRiskCategories,
} from '../rimeDashboard/helpers/constants';
import ArticleModal from '../rimeEventCard/articleModal';
// common style
import '../../sass/style.scss';
//Card
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { useSelector } from 'react-redux';
import CustomModal from '../../../ui/modal';
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye';
import TagsIcon from '../../../../assets/svg/tagsIcon.svg';
import PlaceIcon from '@mui/icons-material/Place';
import { eDate, lastUpdateDate } from '../rimeDashboard/helpers/utils';

const pinMappingRgba = {
  1: 'low',
  2: 'medium',
  3: 'high',
};

const GeoFencingEventCard = props => {
  const {
    title,
    categoryId,
    secondaryCatergoryId,
    subRiskCategoryId,
    link,
    eventDate,
    relevancy,
    sourceLinks,
    noOfArticles,
    lastUpdatedDate,
    imgURL,
    tags,
    language,
    locationDetails: { distance, name },
  } = props;

  const [isNew, setIsNew] = useState(true);
  const [category, setCategory] = useState('');
  const [secondaryCategory, setSecondaryCategory] = useState('');
  const [subRiskCategory, setSubRiskCategory] = useState('');
  const setTheme = useSelector(state => state.themeChange.setDarkTheme);
  const refreshDuration = useSelector(state => state.rime.refreshDuration);
  const [showTags, setShowTags] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const TIMEOUT = refreshDuration * 60000;

  const mapRiskCategory = () => {
    categories.map(item => {
      if (categoryId === item.id) {
        setCategory(item.label);
      }
    });
    subRiskCategories.map(item => {
      if (subRiskCategoryId === item.id) {
        setSubRiskCategory(item.label);
      }
    });
  };

  const handleModal = () => {
    setShowModal(false);
  };

  const showArticleModal = () => {
    setShowModal(true);
  };

  const showTagsDiv = () => {
    setShowTags(prevCheck => !prevCheck);
  };

  useEffect(() => {
    mapRiskCategory();
  });

  let tag = tags && tags.split(',');
  let allLinks = sourceLinks && sourceLinks.split(',');

  // To convert UTC Time to Local Time as per Computer's TimeZone
  const utcLastUpdateDate = lastUpdateDate(lastUpdatedDate);
  const utcDate = eDate(eventDate);

  return (
    <div id="rime__geoFencing__eventCard">
      <Card
        className={`rime__geoFencing__eventCardList ${pinMappingRgba[relevancy]}`}
      >
        <div className="geoFencing__header">
          <div
            className={`geoFencing__pill ${relevancy &&
              pinMappingRgba[relevancy]}`}
          >
            <Typography
              component="span"
              className="geoFencing__cat__pill"
              style={{
                fontWeight: 500,
              }}
            >
              {category ? category : 'Not Available'}
              {subRiskCategoryId != 0 && subRiskCategoryId != null && <span>,&nbsp;</span>}
              {subRiskCategoryId != 0 && subRiskCategory}
            </Typography>
          </div>
          <div className="geoFencing__card__innercolumn">
            <div>
              <Typography
                component="span"
                className="rime__geoFencing__eventDate"
              >
                &nbsp;
                {moment(utcDate).format('Do MMM YYYY')}
              </Typography>
              <Typography
                variant="subtitle1"
                color="text.secondary"
                component="span"
                className="rimeupdate__geoFencing__date"
              >
                Updated:&nbsp;
              </Typography>
              <Typography
                component="span"
                className="rimeupdate__geoFencing__date"
              >
                {moment(utcLastUpdateDate).format('Do MMM YYYY, h:mm:ss a')}
              </Typography>
            </div>
          </div>
        </div>
        <div className="geoFencing__card__row1">
          <div className="geoFencing__card__innercolumn2">
            <Box sx={{ display: 'flex', flexDirection: 'column' }}>
              <CardContent sx={{ flex: '1 0 auto' }}>
                <Typography
                  component="div"
                  className="geoFencing__eventtitle__wrap"
                >
                  {title.substring(0, 79)}
                  {title.length > 79 ? '...' : ''}
                  <a className="anchor__text" href={link} target="_blank">
                    ...read more
                  </a>
                </Typography>
              </CardContent>
            </Box>
          </div>
        </div>
        <div className="distanceFromOffice">
          <PlaceIcon className="locationIcon" />
          {distance} kms from {name}
        </div>
        <div className="eventsearch__card__row2">
          <Box sx={{ display: 'flex', flexDirection: 'column' }}>
            <CardContent sx={{ flex: '1 0 auto' }}>
              <div className="rime__eventSearch__spacing"></div>
              <div className="geoFencing__card_footer">
                <div
                  className="geoFencing__article__content__pill"
                  onClick={() => showArticleModal()}
                >
                  <Typography
                    variant="subtitle1"
                    color="text.secondary"
                    component="span"
                    className="geoFencing__eventbody__Head"
                    style={{ marginTop: '-1px' }}
                  >
                    No of Articles:
                  </Typography>
                  <Typography
                    component="span"
                    className="geoFencing__eventbody__Head"
                  >
                    &nbsp;
                    {noOfArticles ? noOfArticles : 'Not Available'}
                  </Typography>
                  <span>&nbsp;</span>
                  <RemoveRedEyeIcon className="eyeIcon" />
                </div>
                <div
                  className="geoFencing__article__content__pill"
                  onClick={() => showTagsDiv()}
                >
                  <Typography
                    variant="subtitle1"
                    color="text.secondary"
                    component="span"
                    className="geoFencing__eventbody__Head"
                    style={{ marginTop: '-1px' }}
                  >
                    Tags
                  </Typography>
                  <span>&nbsp;&nbsp;</span>
                  <img className="tagsIcon" src={TagsIcon} />
                </div>
              </div>
              <div className="geoFencing__rime__tags">
                {showTags &&
                  tags &&
                  tag.length > 0 &&
                  tag.map(subitem => {
                    return <span>#{subitem}</span>;
                  })}
              </div>
            </CardContent>
          </Box>
        </div>
      </Card>
      <CustomModal showModal={showModal}>
        <div
          id="rimeEventArchivesModalCont"
          className={`modal__inner ${setTheme ? 'dark' : 'light'}`}
        >
          <ArticleModal
            alllinks={allLinks}
            closeEventModal={() => handleModal()}
          />
        </div>
      </CustomModal>
    </div>
  );
};

export default GeoFencingEventCard;
