import React, { Component } from 'react';
import GeoFencingData from './geofencingData';
import { connect } from 'react-redux';
import { showGeoFenceEventNumbers } from '../../../../actions/rimeActions';

@connect(
  state => {
    return { rime: state.rime, themeChange: state.themeChange };
  },
  dispatch => {
    return { dispatch: dispatch };
  },
)
class RimeGeoFencing extends Component {
  state = {
    geoFencingFeed: [],
    nearestLocation: [],
    mainFeed: [],
  };

  componentDidMount() {
    this.setState({ geoFencingFeed: [] }, () => {
      this.props.feed.length > 0
        ? this.getFeed()
        : this.props.dispatch(showGeoFenceEventNumbers('0'));
    });
  }

  componentDidUpdate(prevProps) {
    if (prevProps.feed !== this.props.feed) {
      this.setState({ geoFencingFeed: [] }, () => {
        this.props.feed.length > 0
          ? this.getFeed()
          : this.props.dispatch(showGeoFenceEventNumbers('0'));
      });
    }
  }

  checkDuplicacy = (array, key) => {
    if (typeof key !== 'function') {
      const property = key;
      key = function(item) {
        return item[property];
      };
    }
    return Array.from(
      array
        .reduce(function(map, item) {
          const k = key(item);
          if (!map.has(k)) map.set(k, item);
          return map;
        }, new Map())
        .values(),
    );
  };

  getFeed = () => {
    this.setState(
      {
        mainFeed: this.checkDuplicacy(this.props.feed, 'eventId'),
      },
      () => this.calDistance(),
    );
  };

  calDistance = () => {
    let { clientLocations } = this.props;
    let { geoFencingFeed, nearestLocation, mainFeed } = this.state;
    mainFeed.map(item => {
      if (item.eventRiskLevel === 3) {
        let lat2 = parseFloat(item.event_lat);
        let lng2 = parseFloat(item.event_lon);
        nearestLocation = [];
        clientLocations.map(subItem => {
          let lat = parseFloat(subItem.latitude);
          let lng = parseFloat(subItem.longitude);
          // Calculate Distance
          let radLat1 = (Math.PI * lat) / 180;
          let radLat2 = (Math.PI * lat2) / 180;
          let theta = lng2 - lng;
          let radTheta = (Math.PI * theta) / 180;
          let dist =
            Math.sin(radLat1) * Math.sin(radLat2) +
            Math.cos(radLat1) * Math.cos(radLat2) * Math.cos(radTheta);
          if (dist > 1) {
            dist = 1;
          }
          dist = Math.acos(dist);
          dist = (dist * 180) / Math.PI;
          dist = dist * 60 * 1.1515;
          dist = dist * 1.609344; // to Convert into Kms
          if (dist < 50) {
            nearestLocation.push({
              name: subItem.title,
              distance: parseFloat(dist).toFixed(2),
            });
          }
        });
        if (nearestLocation?.length) {
          geoFencingFeed.push([item, nearestLocation]);
        }
      }
      this.setState(
        {
          geoFencingFeed: geoFencingFeed,
        },
        () => {
          this.props.dispatch(
            showGeoFenceEventNumbers(
              geoFencingFeed ? geoFencingFeed.length : '0',
            ),
          );
        },
      );
    });
  };

  render() {
    const { geoFencingFeed } = this.state;
    return (
      <>
        <GeoFencingData geoFencingFeed={geoFencingFeed} />
      </>
    );
  }
}

export default RimeGeoFencing;
