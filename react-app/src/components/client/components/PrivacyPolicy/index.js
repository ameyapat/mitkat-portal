import React, { Component } from 'react';
import policyList from './constant.json';
import './style.scss';

class PrivacyPolicy extends Component {
  render() {
    return (
      <div id="privacyPolicy">
        {policyList.map(item => (
          <>
            <strong className="privacyPolicy-title">{item.header} </strong>
            <p className="privacyPolicy-detail">{item.description}</p>
          </>
        ))}
      </div>
    );
  }
}

export default PrivacyPolicy;
