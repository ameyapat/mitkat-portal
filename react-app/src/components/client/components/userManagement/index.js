// export default UserManagement;
import React, { Component } from 'react';
//components @material-ui
import Modal from '@material-ui/core/Modal';
//components
import ClientTable from '../../../ui/tableClient';
import { connect } from 'react-redux';
import { withCookies } from 'react-cookie';
import './style.scss';
import { getClientUserList } from '../../../../requestor/mitkatWeb/requestor';
import CreateClientUser from '../../../ui/createClientUser';

@withCookies
@connect(state => {
  return {
    appState: state.appState,
  };
})
class UserManagement extends Component {
  state = {
    showCreateUserModal: false,
    showUserDetails: false,
    userList: [],
    calcHeight: '100%',
  };

  componentDidMount = () => {
    this.getUserList();
  };

  getUserList = () => {
    getClientUserList().then(data => {
      this.setState({ userList: data });
    });
  };

  handleModal = () => {
    this.setState({ showCreateUserModal: !this.state.showCreateUserModal });
  };

  render() {
    let { userList } = this.state;

    return (
      <>
        <h1 className="rootHeader">User Management</h1>
        <div className="userClientManagement">
          <div className="more--options">
            <span>
              <i className="fas fa-users"></i>
              <label>User List</label>
            </span>
            <button className="btn" onClick={this.handleModal}>
              <span className="icon-wrap">
                <i className="fas fa-user-plus" style={{ color: 'white' }}></i>
              </span>
              <span className="btn--create">Create User</span>
            </button>
          </div>
          <div
            className="user--list-wrap"
            style={{ height: this.state.calcHeight }}
          >
            {
              <ClientTable
                tableData={userList}
                showViewDetails={false}
                getUserList={this.getUserList}
              />
            }
          </div>
          <Modal
            open={this.state.showCreateUserModal}
          >
            <div className="modal-paper">
              <CreateClientUser
                userType="user"
                getUserList={this.getUserList}
                handleCloseModal={this.handleModal}
              />
            </div>
          </Modal>
        </div>
      </>
    );
  }
}

export default UserManagement;
