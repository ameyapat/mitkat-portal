import React, { useState } from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import './ExpandableImageCard.scss';
import CustomModal from '../../../ui/modal';

const ExpandableImageCard = (props) => {
    const { imgSrc, imgAlt } = props;
    const [show, showModal] = useState(false);

    function toggleModal(){
        showModal(!show);
    }

    return(
        <Card className={"expandableImageCard"}>
            <CardContent className="cardContent">
                <img 
                    src={imgSrc} 
                    alt={imgAlt} 
                    className="default-img" 
                    onClick={() => toggleModal()} 
                />
                {
                    <CustomModal
                        showModal={show}
                    >
                        <div className="expand-modal-inner">
                            <span
                                className={"closeTimes"}
                                onClick={() => toggleModal()}
                                >
                                &times;
                            </span>
                            <img 
                                src={imgSrc} 
                                alt={imgAlt} 
                                className="expanded-img" 
                                />
                        </div>
                    </CustomModal>
                }
            </CardContent>
        </Card>
    )
}

export default ExpandableImageCard;