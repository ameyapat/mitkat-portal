import React, { Component } from 'react'
import {
    withScriptjs,
    withGoogleMap,
    GoogleMap,
    Polygon,
    InfoWindow,
  } from "react-google-maps";

import stateJson from './india_state_geo.json';
import customMapStyles from './mapStyle.json';
import { plotPolygon, loopLatLng } from './utils';
import { fetchApi } from '../../../../helpers/http/fetch.js';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes.js';
import CustomPolygon from './CustomPolygon.js';

const colorMap = {
    1: '#3C8C74',
    2: '#E5853A',
    3: '#a72421',
}

const defaultOptions = {
    mapTypeControl: false,
    streetViewControl: false,
    zoomControl: false,
    fullscreenControl: false,
    styles: customMapStyles
}

const coords = [
    { lat: 29.047487, lng: 41.023164 },
    { lat: 29.0459633, lng: 41.0212904 },
    { lat: 29.0449333, lng: 41.0167573 },
    { lat: 29.0393543, lng: 41.0106695 },
    { lat: 29.032917, lng: 41.0049697 },
    { lat: 29.0226173, lng: 41.0061356 },
    { lat: 29.0078545, lng: 41.0039334 },
    { lat: 29.0201283, lng: 40.9765933 },
    { lat: 29.0319729, lng: 40.9657708 },
    { lat: 29.0784073, lng: 40.9536501 },
    { lat: 29.0944576, lng: 40.9493068 },
    { lat: 29.0975475, lng: 40.9514461 },
    { lat: 29.1052294, lng: 40.9647986 },
    { lat: 29.097338, lng: 40.978242 },
    { lat: 29.0931273, lng: 40.9835914 },
    { lat: 29.0858746, lng: 40.987738 },
    { lat: 29.056509, lng: 40.998902 },
    { lat: 29.061456, lng: 41.008443 },
    { lat: 29.0617561, lng: 41.0104752 },
    { lat: 29.0595245, lng: 41.0126772 },
    { lat: 29.052014, lng: 41.018198 },
    { lat: 29.047487, lng: 41.023164 }];

const reversedCoords = coords.map( ll => {
    return { lat: ll.lng, lng: ll.lat }
});

@withScriptjs
@withGoogleMap
class CustomGMap extends Component {
  state = {
    stateObjArr: [],
    polygons: [],
    showMeNow: false,
  };

  componentDidMount() {
                        this.getGeoJson();
                      }

  getGeoJson = () => {
    const reqObj = {
      url: API_ROUTES.geojson,
      isAuth: false,
      // authToken,
      method: 'GET',
      // data,
      showToggle: true,
    };

    fetchApi(reqObj).then(data => {
      this.setState({
        polygons: data.polygonmodel,
      });
    });
  };

  renderPolygon = () => {
    const { polygons } = this.state;

    return polygons.map(p => {
      return (
        <CustomPolygon
          p={p}
          key={p.districtid}
          options={{
            fillColor: colorMap[p.colorid],
            fillOpacity: 0.4,
            strokeColor: colorMap[p.colorid],
            strokeOpacity: 1,
            strokeWeight: 1,
          }}
          latitude={p.coordinatelist[0].latitude}
          longitude={p.coordinatelist[0].longitude}
          districtName={p.districtName}
        />
      );
    });
  };

  render() {
    const { polygons } = this.state;

    return (
      <GoogleMap
        defaultZoom={9}
        defaultOptions={defaultOptions}
        defaultCenter={{ lat: 28.7041, lng: 77.1025 }}
      >
        {polygons.length > 0 && this.renderPolygon()}
      </GoogleMap>
    );
  }
}

export default CustomGMap;