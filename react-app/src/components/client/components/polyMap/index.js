import React, { Component } from 'react';
import CustomGMap from './CustomGMap';

class PolyMap extends Component{
    render(){
        return(
            <CustomGMap
                googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyCYOTLyn6xbysArlvlW4zpxnH9p6H3An08&v=3.exp&libraries=geometry,drawing,places"
                loadingElement={<div style={{ height: `100%` }} />}
                containerElement={<div style={{ height: `100vh`, position: 'relative' }} />}
                mapElement={<div style={{ height: `100%` }} />}
            />
        )
    }
}

export default PolyMap;