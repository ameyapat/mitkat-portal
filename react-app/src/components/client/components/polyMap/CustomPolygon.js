import React, { Component } from 'react';
import {
    Polygon,
    InfoWindow,
  } from "react-google-maps";

class CustomPolygon extends Component{

    state = {
        showInfoWindow: false
    }

    render(){
        const { latitude, longitude, options, districtName, p } = this.props;
        return(
            <>
            {
                this.state.showInfoWindow && (
                    <InfoWindow
                    position={{ lat: latitude, lng: longitude }}
                    >
                    <p>{districtName}</p>
                    </InfoWindow>
                )
            }
                <Polygon
                    // onMouseOver={() => this.setState({ showInfoWindow: true })}
                    // onMouseOut={() => this.setState({ showInfoWindow: false })}
                    path={p.coordinatelist.map(i => ({ lat: i.latitude, lng: i.longitude }))}
                    options={options} 
                /> 
            </>
        )
    }
}

export default CustomPolygon;