export const helpItems = {
  helpOptions: [
    {
      id: 1,
      title: 'Demo',
      icon: 'fas fa-video dropIcons',
      type: 'demo',
      to:
        'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/IMG-8542.MOV.mp4',
      isRoute: true,
    },
    {
      id: 2,
      title: 'Manual',
      icon: 'fas fa-book-open dropIcons',
      type: 'manual',
      to:
        'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/MitKat+Risk+Tracker+Portal+User+Manual.pdf',
      isRoute: true,
    },
    {
      id: 3,
      title: 'Certificates',
      icon: 'fas fa-certificate',
      type: 'certificates',
      isRoute: false,
    },
    {
      id: 4,
      title: 'Awards',
      icon: 'fas fa-award',
      type: 'awards',
      isRoute: false,
    },
    {
      id: 3,
      title: 'Terms of service',
      icon: 'fas fa-file-invoice',
      type: 'termsOfService',
      isRoute: false,
    },
    {
      id: 4,
      title: 'Privacy policy',
      icon: 'fas fa-lock',
      type: 'privacyPolicy',
      isRoute: false,
    },
  ],
};
