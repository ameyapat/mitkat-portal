import React, { Component } from 'react';
import ViewSwitch from './components/ViewSwitch';
import RimeLogo from '../../../../assets/icons/rime-icon-dark.svg';
import MitkatLogoLight from '../../../../assets/svg/MitKat-Logo-White.svg';
import MitkatLogoDark from '../../../../assets/svg/MitKat-Logo-Blue.svg';
import MitKatIconDark from '../../../../assets/svg/MitKat-Logo-Icon-2022-Blue.svg';
import MitKatIconLight from '../../../../assets/svg/MitKat-Logo-Icon-2022-White.svg';
import './style.scss';
import HelpMenu from './components/helpMenu';
import ProfileMenu from './components/profileMenu';
import SettingMenu from './components/settingMenu';
import RiskAlertsHorizontal from '../riskAlertsHorizontal';
import { connect } from 'react-redux';

@connect(state => {
  return {
    themeChange: state.themeChange,
  };
})
class NavMenuHorizontal extends Component {
  render() {
    const {
      themeChange: { setDarkTheme },
    } = this.props;
    return (
      <div className={`navigation__Bar ${setDarkTheme ? 'dark' : 'light'}`}>
        <div className="left__Menus">
          <img
            src={setDarkTheme ? MitkatLogoLight : MitkatLogoDark}
            className="mitkat__logo"
          />
          <RiskAlertsHorizontal />
        </div>
        <div className="right__Menus">
          <ViewSwitch />
          <HelpMenu />
          <SettingMenu />
          <ProfileMenu />
          <h1 className={`Rime__title ${setDarkTheme ? 'dark' : 'light'}`}>
            RIME
          </h1>
        </div>
      </div>
    );
  }
}

export default NavMenuHorizontal;
