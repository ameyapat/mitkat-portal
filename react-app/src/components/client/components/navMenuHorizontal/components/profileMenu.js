import React, { Component } from 'react';
import '../style.scss';
import style from '../Style';
import { withRouter } from 'react-router-dom';
import { withCookies } from 'react-cookie';
import * as actions from '../../../../../actions';
import { connect } from 'react-redux';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { toggleShowSettings } from '../../../../../actions';
import Tooltip from '@material-ui/core/Tooltip';
import { withStyles } from '@material-ui/core';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';

@withCookies
@withRouter
@withStyles(style)
class ProfileMenu extends Component {
  state = {
    anchorEl: null,
    enableLockMapScreen: false,
    enableShowLocations: true,
    enableTheme: true,
    darkMode: true,
  };

  handleOpenSettings = () => {
    const { dispatch } = this.props;
    dispatch(toggleShowSettings(true));
  };

  handleLogout = () => {
    let { cookies } = this.props;
    cookies.remove('authToken');
    cookies.remove('userRole');
    cookies.remove('partnerId');

    this.closeSettings();

    setTimeout(() => {
      this.props.history.push('/');
    }, 2000);
  };

  closeSettings = () => {
    const { dispatch } = this.props;
    dispatch(actions.toggleShowSettings(false));
  };

  handleLockScreen = () => {
    const { dispatch } = this.props;
    const { lockScreen } = this.props.rime;
    dispatch(actions.toggleLockScreen(!lockScreen));
  };

  handleReset = () => this.props.history.push('/reset');

  handleClose = () => this.setState({ anchorEl: null });
  handleOpen = e => {
    this.setState({ anchorEl: e.currentTarget });
  };

  render() {
    const {
      history: {
        location: { pathname },
      },
      classes,
      themeChange: { setDarkTheme },
      dispatch,
    } = this.props;

    const { anchorEl } = this.state;

    return (
      <>
        <div className="rootViewSwitch__Grid">
          <Tooltip title="Profile">
            <button className="btnAvatarMenu" onClick={this.handleOpen}>
              <AccountCircleIcon className="menu-icon" />
            </button>
          </Tooltip>
          <Menu
            id="user-profile-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={this.handleClose}
            classes={{
              paper: classes.paperMenu,
            }}
            className={`${setDarkTheme ? 'dark' : 'light'}`}
          >
            <MenuItem
              onClick={this.handleReset}
              classes={{
                root: classes.rootMenuItem,
              }}
            >
              <i className="fas fa-unlock-alt dropIcons"></i>
              <span className="marginLft">Reset Password</span>
            </MenuItem>
            <MenuItem
              onClick={this.handleLogout}
              classes={{
                root: classes.rootMenuItem,
              }}
            >
              <i className="fas fa-sign-out-alt dropIcons"></i>
              <span className="marginLft">Logout</span>
            </MenuItem>
          </Menu>
        </div>
      </>
    );
  }
}

const mapStateToProps = state => ({
  search: state.search,
  rime: state.rime,
  appState: state.appState,
  themeChange: state.themeChange,
});
const mapDispatchToProps = dispatch => ({ dispatch });

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(withCookies(ProfileMenu)),
);
