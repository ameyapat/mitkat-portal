import React, { Component } from 'react';
import '../style.scss';
import style from '../Style';
import { withRouter } from 'react-router-dom';
import { withCookies } from 'react-cookie';
import * as actions from '../../../../../actions';
import { connect } from 'react-redux';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { toggleShowSettings } from '../../../../../actions';
import {
  updateSettings,
  getSubscriptions,
} from '../../../../../requestor/mitkatWeb/requestor';
import Tooltip from '@material-ui/core/Tooltip';

import CustomModal from '../../../../ui/modal';
import ModalWithPortal from '../../../../ui/modalWithPortal/ModalWithPortal';

import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import { Button, withStyles } from '@material-ui/core';
import { themeChangeToggle } from '../../../../../actions/themeAction';
import SettingsIcon from '@mui/icons-material/Settings';

@withCookies
@withRouter
@withStyles(style)
class SettingMenu extends Component {
  state = {
    anchorEl: null,
    enableLockMapScreen: false,
    enableShowLocations: true,
    enableTheme: true,
    darkMode: true,
  };

  handleOpenSettings = () => {
    const { dispatch } = this.props;
    dispatch(toggleShowSettings(true));
  };

  closeSettings = () => {
    const { dispatch } = this.props;
    dispatch(actions.toggleShowSettings(false));
  };

  handleLockScreen = () => {
    const { dispatch } = this.props;
    const { lockScreen } = this.props.rime;
    dispatch(actions.toggleLockScreen(!lockScreen));
  };

  handleShowLocations = () => {
    const {
      dispatch,
      appState: {
        appPermissions: { locationPinSetting, colorThemeSetting },
      },
    } = this.props;

    const reqObj = {
      locationPinSetting: !locationPinSetting,
      colorThemeSetting,
    };

    const reqData = {
      locationPinSetting: reqObj.locationPinSetting ? 1 : 0,
      colorThemeSetting: reqObj.colorThemeSetting ? 1 : 0,
    };

    updateSettings(reqData).then(data => {
      getSubscriptions().then(data => {
        dispatch(actions.setAppPermissions(data));
      });
    });
  };

  handleToggleTheme = () => {
    const {
      dispatch,
      appState: {
        appPermissions: { locationPinSetting, colorThemeSetting },
      },
    } = this.props;

    const reqObj = {
      locationPinSetting,
      colorThemeSetting: !colorThemeSetting,
    };

    const reqData = {
      locationPinSetting: reqObj.locationPinSetting ? 1 : 0,
      colorThemeSetting: reqObj.colorThemeSetting ? 1 : 0,
    };

    updateSettings(reqData).then(data => {
      getSubscriptions().then(data => {
        dispatch(actions.setAppPermissions(data));
      });
    });
  };

  render() {
    const {
      history: {
        location: { pathname },
      },
      classes,
      appState: {
        appPermissions: { colorThemeSetting, locationPinSetting },
      },
      themeChange: { setDarkTheme },
      dispatch,
    } = this.props;

    let { showSettings, lockScreen, showLocationPin } = this.props.rime;
    const {
      anchorEl,
      enableLockMapScreen,
      enableShowLocations,
      enableTheme,
    } = this.state;

    return (
      <>
        <div className="rootViewSwitch__Grid">
          <Tooltip title="Settings">
            <button className="btnAvatarMenu" onClick={this.handleOpenSettings}>
              <SettingsIcon className="menu-icon" />
            </button>
          </Tooltip>
          <ModalWithPortal>
            <CustomModal showModal={showSettings}>
              <div id="rimeUserSettings">
                <div className="userSettings__header">
                  <h1>Settings</h1>
                  <button className="btn--close" onClick={this.closeSettings}>
                    &times;
                  </button>
                </div>
                <div className="userSettings__body">
                  {enableLockMapScreen && (
                    <div className="userSettings_toggles">
                      <span className="userSettings__label">
                        Lock Map Screen
                      </span>
                      <FormControlLabel
                        control={
                          <Switch
                            checked={!lockScreen}
                            onChange={this.handleLockScreen}
                            name="lockScreen"
                          />
                        }
                        classes={{
                          root: classes.rootControlLabel,
                          label: classes.rootLabelControlLabel,
                          switchBase: classes.switchBase,
                          thumb: classes.switchThumb,
                          track: classes.switchTrack,
                          checked: classes.switchChecked,
                        }}
                      />
                    </div>
                  )}
                  {enableShowLocations && (
                    <div className="userSettings_toggles">
                      <span className="userSettings__label">
                        Show Locations
                      </span>
                      <FormControlLabel
                        control={
                          <Switch
                            checked={locationPinSetting}
                            onChange={this.handleShowLocations}
                            name="showLocationPin"
                          />
                        }
                        classes={{
                          root: classes.rootControlLabel,
                          label: classes.rootLabelControlLabel,
                        }}
                      />
                    </div>
                  )}
                  {enableTheme && (
                    <div className="userSettings_toggles">
                      <span className="userSettings__label">Switch Theme</span>
                      <div>
                        <i
                          className="fas fa-sun"
                          style={{ color: '#f1c749' }}
                        ></i>

                        <FormControlLabel
                          control={
                            <Switch
                              checked={setDarkTheme}
                              onChange={() => {
                                this.handleToggleTheme();
                                dispatch(themeChangeToggle(!setDarkTheme));
                              }}
                              name="setDarkTheme"
                            />
                          }
                          classes={{
                            root: classes.rootControlLabel,
                            label: classes.rootLabelControlLabel,
                            switchBase: classes.switchBase,
                            thumb: classes.switchThumb,
                            track: classes.switchTrack,
                            checked: classes.switchChecked,
                          }}
                        />
                        <i
                          className="fas fa-moon"
                          style={{ color: '#78818f' }}
                        ></i>
                      </div>
                    </div>
                  )}
                </div>
              </div>
            </CustomModal>
          </ModalWithPortal>
        </div>
      </>
    );
  }
}

const mapStateToProps = state => ({
  search: state.search,
  rime: state.rime,
  appState: state.appState,
  themeChange: state.themeChange,
});
const mapDispatchToProps = dispatch => ({ dispatch });

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(withCookies(SettingMenu)),
);
