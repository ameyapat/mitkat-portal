export function refreshDurationLimit(refreshValue) {
  let value = parseInt(refreshValue, 10);
  if (value > '24') value = '24';
  if (value < '1') value = '1';
  return value;
}
export function operationalValue(data) {
  let value =
    data === 'and' ? 'AND' : data === 'or' ? 'OR' : !data ? 'OR' : data;
  return value;
}