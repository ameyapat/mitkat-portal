import * as globalCSS from '../../../../sass/globalVariables.scss';

export default {
  rootProductSubscriptionHeader: {
    padding: '13px 30px',
    color: 'var(--lightText)',
    background: 'var(--darkSolid)',
    display: 'flex',
    justifyContent: 'space-between',
  },
  inputLabelFocused: {
    color: 'green !important',
  },
  addtionalWrap: {},
  totalHoursText: {},
  rootFilters: {},
  rootTextInput: {
    padding: '5px',
    color: 'var(--whiteMediumEmp)',
  },
  rootInput: {
    padding: '10px',
    color: 'var(--whiteMediumEmp)',
  },
  notchedOutline: {
    borderWidth: '1px',
    borderColor: 'var(--whiteHighEmp) !important ',
  },
  formControl: {
    width: '100%',
    padding: '15px 0',
  },
  inputFormControl: {
    padding: 0,
  },
  inputLabelRoot: {
    color: 'var(--whiteHighEmp) !important ',
  },
  filterBtn: {
    textTransform: 'capitalize !important',
    border: '1px solid var(--darkActive)',
    background: 'transparent',
    color: 'var(--darkActive)',
    padding: '12px',
    cursor: 'pointer',
    borderRadius: '2px',
    fontWeight: '100',
    fontSize: '13px',
    width: '120px',
    '&:hover': {
      background: 'var(--darkActive)',
      color: 'var(--darkGrey)',
    },
  },
  detailsTitle: {
    color: `white !important`,
    fontWeight: 400,
    padding: '5px 0',
    display: 'block',
    fontSize: 12,
    textTransform: 'capitalize',
    width: '100%',
    '& span': {
      fontSize: 18,
    },
  },
  detailsTitleWhite: {
    color: `var(--highlightText) !important`,
    fontWeight: 400,
    padding: '5px 0',
    display: 'block',
    fontSize: 12,
    textTransform: 'capitalize',
    width: '100%',
    '& span': {
      fontSize: 18,
    },
  },
  rimeSubscriptionsdetailsTitle: {
    color: `white !important`,
    fontWeight: 400,
    padding: '5px 0',
    display: 'block',
    fontSize: 12,
    textTransform: 'capitalize',
    '& span': {
      fontSize: 18,
    },
  },
  locationTextfield: {
    color: 'var(--whiteMediumEmp)',
  },
  searchIcon: {
    color: 'var(--whiteMediumEmp)',
  },
  suggestionsWrap: {},
  searchWrapper: {
    padding: 0,
  },
  inputSearchWrap: {
    position: 'relative',
    '& .location-search-input': {
      padding: '15px',
      borderRadius: '3px',
      border: 'none',
      width: '100%',
      fontSize: 14,
      color: '#333',
      outline: 'none',
    },
  },
  autoCompleteDropdown: {
    borderRadius: 5,
    position: 'absolute',
    top: '50px',
    left: 0,
    width: '100%',
    zIndex: 999,
  },
  activeListItem: {
    padding: 10,
    borderBottom: '1px solid #f1f1f1',
    fontSize: 14,
    '&. list--item': {
      color: '#333',
    },
  },
  suggestionItem: {
    padding: 10,
    borderBottom: '1px solid #f1f1f1',
    fontSize: 14,
    '&. list--item': {
      color: '#333',
    },
  },
  label: {
    color: 'var(--whiteMediumEmp)',
    fontSize: '0.98em',
  },
  size: {
    width: 40,
    height: 30,
  },
  sizeIcon: {
    fontSize: 20,
    color: 'rgba(171, 193, 66, 1)',
  },
  singleSelect: {
    width: '100%',
  },
  rootText: {
    margin: '15px 0',
    color: 'var(--whiteHighEmp) !important',
  },
  listOfItems: {
    margin: 0,
    borderBottom: '1px solid #f1f1f1',
    paddingBottom: 10,
    fontSize: 12,
    color: '#555',
    fontWeight: 'normal',
  },
  size: {
    width: 40,
    height: 30,
  },
  sizeIcon: {
    fontSize: 20,
    color: 'rgba(0, 119, 182, 0.87)',
  },
  rootLabel: {
    color: 'rgba(255,255,255, 0.67) !important',
  },
  rootInput: {
    color: 'var(--whiteHighEmp) !important',
  },
  underline: {
    '&:after': {
      borderBottom: '2px solid #34495e !important',
    },
  },
  notchedOutline: {
    borderColor: 'var(--whiteHighEmp) !important ',
  },
  label: {
    color: 'var(--whiteHighEmp) !important ',
  },
  radioLabel: {
    color: 'var(--whiteHighEmp) !important ',
  },
};
