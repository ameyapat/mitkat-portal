import React, { Component } from 'react';
import { getRimeSubscriptions } from '../../../../api/api';
import './rimeStyle.scss';
import { withCookies } from 'react-cookie';
import { connect } from 'react-redux';
import ManageRimeSubscriptions from './ManageRimeSubscriptions';
import store from '../../../../store';
import { getRimeSubscriptionData } from '../../../../actions/rimeActions';
import EventArchives from '../rimeFilters/EventArchives';
import * as actions from '../../../../actions';
import { withStyles, Drawer } from '@material-ui/core';

const drawerWidth = '100vw';
const drawerHeight = '200px';
const styles = theme => ({
  paperAnchorTop: {
    zIndex: 996,
    position: 'absolute',
    overflowX: 'hidden',
    overflowY: 'hidden',
    width: drawerWidth,
    height: drawerHeight,
    marginTop: '8vh',
    backgroundColor: 'var(--filterBackground)',
    '&:hover': {
      overflowY: 'scroll',
    },
    '&::-webkit-scrollbar': {
      width: 3,
    },
    '&::-webkit-scrollbar-track': {
      background: 'transparent',
    },
    '&::-webkit-scrollbar-thumb': {
      backgroundColor: '#ccc',
      borderRadius: 5,
    },
  },
  test: {
    background: 'none',
  },
});

@withStyles(styles, { withTheme: true })
@withCookies
@connect(
  state => {
    return {
      themeChange: state.themeChange,
      subscriptionList: state.rime.subscriptionData,
      rime: state.rime,
    };
  },
  dispatch => {
    return { dispatch: dispatch };
  },
)
class RimeSubscriptions extends Component {
  state = {
    subscriptionList: [],
  };

  componentDidMount = () => {
    this.getSubscriptionsData();
  };

  getSubscriptionsData = () => {
    getRimeSubscriptions().then(data => {
      store.dispatch(getRimeSubscriptionData(data));
    });
  };
  toggleEventArchives = () =>
    this.props.dispatch(actions.toggleRimeEventArchives(false));

  render() {
    const {
      classes,
      themeChange: { setDarkTheme },
      rime: { showRimeEventArchives },
    } = this.props;
    return (
      <div
        id="rime__subscriptions"
        className={`${setDarkTheme ? 'dark' : 'light'}`}
      >
        <ManageRimeSubscriptions />
        <Drawer
          variant="top"
          anchor="top"
          classes={{
            root: classes.rootDrawer,
            paperAnchorTop: classes.paperAnchorTop,
            modal: classes.test,
          }}
          open={showRimeEventArchives}
          className={`${setDarkTheme ? 'dark' : 'light'}`}
        >
          <EventArchives toggleEventArchives={this.toggleEventArchives} />
        </Drawer>
      </div>
    );
  }
}

export default RimeSubscriptions;
