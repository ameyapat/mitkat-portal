import React, { Component } from 'react';
import {
  deleteRimeSubscriptions,
  getRimeSubscriptions,
} from '../../../../api/api';
import { withCookies } from 'react-cookie';
import { Button, IconButton } from '@material-ui/core';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import TablePagination from '@material-ui/core/TablePagination';
import { connect } from 'react-redux';
import CustomModal from '../../../ui/modal';
import AlertDialog from '../../../ui/alertDialog';
import { styled } from '@mui/material/styles';
import AddCircleIcon from '@mui/icons-material/AddCircle';
import './rimeStyle.scss';
import CreateRimeSubscription from './CreateSubscriptions';
import EditRimeSubscription from './EditSubscriptions';
import { getRimeSubscriptionData } from '../../../../actions/rimeActions';
import store from '../../../../store';
import { toggleToast } from '../../../../actions';
import deleteIcon from '../../../../assets/svg/deleteIcon.svg';
import editIcon from '../../../../assets/svg/editIcon.svg';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.body}`]: {
    fontSize: 18,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '&:nth-of-type(odd)': {
    backgroundColor: 'var(--evenRow)',
    color: 'var(--highlightText)',
  },
  '&:nth-of-type(even)': {
    backgroundColor: 'var(--tableFooter)',
    color: 'var(--highlightText)',
  },
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));

@withCookies
@connect(state => {
  return {
    themeChange: state.themeChange,
    subscriptionList: state.rime.subscriptionData,
  };
})
class ManageRimeSubscriptions extends Component {
  state = {
    createSubscription: false,
    editSubscription: false,
    showAlert: false,
    checkDelete: ' ',
    checkEdit: ' ',
    page: 0,
    rowsPerPage: 7,
  };

  getSubscriptionsData = () => {
    getRimeSubscriptions().then(data => {
      store.dispatch(getRimeSubscriptionData(data));
    });
  };

  handleChangePage = (event, newPage) => {
    this.setState({ page: +newPage });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: +event.target.value });
    this.setState({ page: 0 });
  };

  handleShowAlert = id => {
    this.setState({ showAlert: true, checkDelete: id });
  };

  handleCloseAlert = () => {
    this.setState({ showAlert: false });
  };

  handleModal = () => {
    let { createSubscription } = this.state;
    this.setState({ createSubscription: !createSubscription });
  };

  handleDeleteSubscription = id => {
    this.handleCloseAlert();
    deleteRimeSubscriptions(id)
      .then(data => {
        if (data.success) {
          let toastObj = {
            toastMsg: data.message,
            showToast: true,
          };
          store.dispatch(toggleToast(toastObj));
          setTimeout(this.getSubscriptionsData, 500);
        }
      })
      .catch(e => {
        let toastObj = {
          toastMsg: 'Something went wrong, please try again',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      });
  };

  handleEditSubscriptionOpenModal = id => {
    this.setState({ editSubscription: true, checkEdit: id });
  };

  handleEditSubscriptionCloseModal = () => {
    this.setState({ editSubscription: false });
  };

  render() {
    const {
      themeChange: { setDarkTheme },
    } = this.props;
    let { subscriptionList } = this.props;
    const { checkDelete, checkEdit, rowsPerPage, page, showAlert } = this.state;
    return (
      <div className={`${setDarkTheme ? 'dark' : 'light'}`}>
        {showAlert && (
          <AlertDialog
            isOpen={showAlert}
            title="Click on 'Yes' to delete subscription"
            description="Once you delete subscription, all subscription details would be deleted !"
            parentEventHandler={e => this.handleDeleteSubscription(checkDelete)}
            handleClose={() => this.handleCloseAlert()}
          />
        )}
        <CustomModal showModal={this.state.createSubscription}>
          <div id="createSubscription" className="modal__inner">
            <CreateRimeSubscription closeEventModal={this.handleModal} />
          </div>
        </CustomModal>
        <CustomModal showModal={this.state.editSubscription}>
          <div id="createSubscription" className="modal__inner">
            <EditRimeSubscription
              closeEventModal={this.handleEditSubscriptionCloseModal}
              id={checkEdit}
            />
          </div>
        </CustomModal>
        <Button
          onClick={() => {
            this.handleModal();
          }}
          variant="outlined"
          className="createRimeSubscription_btn"
          startIcon={<AddCircleIcon style={{ color: 'white' }} />}
        >
          <p className="text">Add Subscription</p>
        </Button>
        <div className="Subscription-table">
          <Paper>
            <TableContainer>
              <Table aria-label="simple table">
                <TableHead
                  className={`supplyChain--table__head ${
                    setDarkTheme ? 'dark' : 'light'
                  }`}
                >
                  <TableRow>
                    <StyledTableCell className="supplyChain--table__rowCell">
                      Sr No
                    </StyledTableCell>
                    <StyledTableCell className="supplyChain--table__rowCell">
                      Subscription Name
                    </StyledTableCell>
                    <StyledTableCell className="supplyChain--table__rowCell"></StyledTableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {subscriptionList?.length > 0 ? (
                    subscriptionList
                      .slice(
                        page * rowsPerPage,
                        page * rowsPerPage + rowsPerPage,
                      )
                      .map((item, srno) => {
                        return (
                          <StyledTableRow key={item.id}>
                            <StyledTableCell>{srno + 1}</StyledTableCell>
                            <StyledTableCell>
                              {item.subscriptionName
                                ? item.subscriptionName
                                : 'N.A'}
                            </StyledTableCell>
                            <StyledTableCell align="right">
                              <IconButton
                                color="primary"
                                aria-label="upload picture"
                                component="label"
                                onClick={() => {
                                  this.handleEditSubscriptionOpenModal(item.id);
                                }}
                              >
                                <img src={editIcon} className="icon__style" />
                              </IconButton>
                              <span>&#160;</span>
                              <IconButton
                                color="primary"
                                aria-label="upload picture"
                                component="label"
                                onClick={() => this.handleShowAlert(item.id)}
                              >
                                <img src={deleteIcon} className="icon__style" />
                              </IconButton>
                            </StyledTableCell>
                          </StyledTableRow>
                        );
                      })
                  ) : (
                    <h1> </h1>
                  )}
                </TableBody>
              </Table>
            </TableContainer>
            <TablePagination
              rowsPerPageOptions={[7, 15, 25]}
              component="div"
              count={subscriptionList?.length}
              rowsPerPage={rowsPerPage}
              page={page}
              onPageChange={this.handleChangePage}
              onRowsPerPageChange={this.handleChangeRowsPerPage}
              className="table--footer"
            />
          </Paper>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatch,
  };
};

export default connect(mapDispatchToProps)(ManageRimeSubscriptions);
