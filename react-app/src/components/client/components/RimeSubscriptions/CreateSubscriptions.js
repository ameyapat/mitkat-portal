import React, { Component } from 'react';
import { withStyles, TextField, FormControl, Button } from '@material-ui/core';
import styles from './style';
import { connect } from 'react-redux';
import { withCookies } from 'react-cookie';
import { multipleAddress } from '../rimeDashboard/helpers/multipleAddress';
import MultiSelect from '../../../ui/multiSelect';
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import CustomCheckboxUi from '../../../ui/customCheckboxUi';
import CustomRelevancyCheckbox from '../../../ui/customCheckboxUi/CustomRelevancyCheckbox';
import {
  categories,
  relevancyConstants,
  languageConstants,
} from '../rimeDashboard/helpers/constants';
import './rimeStyle.scss';
import AlertDialog from '../../../ui/alertDialog';
import {
  createRimeSubscriptions,
  getRimeSubscriptions,
} from '../../../../api/api';
import FormValidator from '../../../../helpers/validation/FormValidator';
import validationRules from './validationRules';
import {
  getRimeSubscriptionData,
  handleSelectAllRelevancy,
  handleSelectAllRiskCat,
  updateSelectedCategories,
  updateSelectedRelevancy,
  updateSelectedLanguage,
} from '../../../../actions/rimeActions';
import store from '../../../../store';
import { toggleToast } from '../../../../actions';
import { refreshDurationLimit } from './helpers/utils';

@withStyles(styles)
@connect(
  state => {
    return { rime: state.rime, themeChange: state.themeChange };
  },
  dispatch => {
    return { dispatch: dispatch };
  },
)
@withCookies
class CreateRimeSubscription extends Component {
  validator = new FormValidator(validationRules);
  submitted = false;

  state = {
    selectedAddress: [],
    address: '',
    location: {},
    riskCategories: [],
    relevancy: [],
    isCatSelected: true,
    selectAllRiskCatChecked: true,
    selectAllRelevancyChecked: true,
    showRiskCategories: false,
    showRelevancy: false,
    showLanguages: false,
    selectedLocations: [],
    refreshDurations: 1,
    allLocations: { multipleAddress },
    operations: 'OR',
    manualTags: '',
    characterLimit: 3,
    totalHours: 1,
    showAlert: false,
    subscriptionName: '',
    subscriptionEmailIds: '',
    validation: this.validator.valid(),
    catArray: [],
    relArray: [],
    language: [],
  };

  componentDidMount() {
    const { rime } = this.props;
    let rArray = [];
    let cArray = [];
    categories.map(item => {
      cArray.push(item.id);
    });
    relevancyConstants.map(item => {
      rArray.push(item.id);
    });
    this.setState({
      selectAllRiskCatChecked: rime.selectAllRiskCat,
      selectAllRelevancyChecked: rime.selectAllRelevancy,
      catArray: cArray,
      relArray: rArray,
    });
  }

  getSubscriptionsData = () => {
    getRimeSubscriptions().then(data => {
      store.dispatch(getRimeSubscriptionData(data));
    });
  };

  handleEventSubmit = e => {
    e.preventDefault();
    const {
      riskCategories,
      relevancy,
      refreshDurations,
      selectedAddress,
      operations,
      manualTags,
      subscriptionName,
      subscriptionEmailIds,
      language,
    } = this.state;
    let addressLabel = [];

    this.validator = new FormValidator(validationRules);
    let validation = this.validator.validate(this.state);
    this.setState({ validation });
    this.submitted = true;

    if (validation.isValid) {
      this.props.closeEventModal();
      addressLabel = [];
      let manualTaglist = [];

      selectedAddress.length > 0 &&
        selectedAddress.map(subitem => {
          addressLabel.push(subitem.label);
        });

      manualTaglist = manualTags ? manualTags.split(',') : [''];
      let tg = manualTaglist?.length > 0 && manualTaglist.join();

      let reqObj = {
        subscriptionName: subscriptionName,
        emailIds: subscriptionEmailIds,
        riskCategory: riskCategories,
        duration: JSON.parse(refreshDurations),
        riskLevel: relevancy,
        address: addressLabel,
        tags: tg,
        operators: operations,
        language: language,
      };

      createRimeSubscriptions(reqObj)
        .then(data => {
          if (data.success) {
            let toastObj = {
              toastMsg: data.message,
              showToast: true,
            };
            store.dispatch(toggleToast(toastObj));
            setTimeout(this.getSubscriptionsData, 500);
          }
        })
        .catch(e => {
          let toastObj = {
            toastMsg: 'Something went wrong, please try again',
            showToast: true,
          };
          store.dispatch(toggleToast(toastObj));
        });
    }
  };

  updateCategories = id => {
    const { riskCategories, catArray } = this.state;
    let riskCategoriesArray = [...riskCategories];
    if (riskCategoriesArray.includes(id)) {
      let index = riskCategoriesArray.indexOf(id);
      riskCategoriesArray.splice(index, 1);
    } else {
      riskCategoriesArray.push(id);
    }
    this.setState({ riskCategories: riskCategoriesArray }, () => {
      if (riskCategories.length === Math.max(...catArray) + 1) {
        this.setState({ selectAllRiskCatChecked: true }, () => {
          store.dispatch(
            handleSelectAllRiskCat(this.state.selectAllRiskCatChecked),
          );
        });
      } else {
        this.setState({ selectAllRiskCatChecked: false }, () => {
          store.dispatch(
            handleSelectAllRiskCat(this.state.selectAllRiskCatChecked),
          );
        });
      }
      this.props.dispatch(updateSelectedCategories(riskCategories));
    });
  };

  updateRelevancy = id => {
    const { relevancy, relArray } = this.state;
    let relevancyArray = [...relevancy];
    if (relevancyArray.includes(id)) {
      let index = relevancyArray.indexOf(id);
      relevancyArray.splice(index, 1);
    } else {
      relevancyArray.push(id);
    }
    this.setState({ relevancy: relevancyArray }, () => {
      if (relevancy.length === Math.max(...relArray)) {
        this.setState({ selectAllRelevancyChecked: true }, () => {
          store.dispatch(
            handleSelectAllRelevancy(this.state.selectAllRelevancyChecked),
          );
        });
      } else {
        this.setState({ selectAllRelevancyChecked: false }, () => {
          store.dispatch(
            handleSelectAllRelevancy(this.state.selectAllRelevancyChecked),
          );
        });
      }
      this.props.dispatch(updateSelectedRelevancy(relevancy));
    });
  };

  updateLanguage = id => {
    const { language } = this.state;
    let languageArray = [...language];
    if (languageArray.includes(id)) {
      let index = languageArray.indexOf(id);
      languageArray.splice(index, 1);
    } else {
      languageArray.push(id);
    }
    this.setState({ language: languageArray }, () => {
      this.props.dispatch(updateSelectedLanguage(language));
    });
  };

  handleRiskCatSelectAll = e => {
    const { rime } = this.props;

    for (let key in rime.riskCategories) {
      rime.riskCategories[key] = e.target.checked;
    }

    this.setState({ selectAllRiskCatChecked: e.target.checked }, () => {
      store.dispatch(
        handleSelectAllRiskCat(this.state.selectAllRiskCatChecked),
      );
      store.dispatch(
        updateSelectedCategories(
          this.state.selectAllRiskCatChecked
            ? [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
            : [],
        ),
      );
    });
  };

  handleRelevancySelectAll = e => {
    const { rime } = this.props;

    for (let key in rime.relevancy) {
      rime.relevancy[key] = e.target.checked;
    }

    this.setState({ selectAllRelevancyChecked: e.target.checked }, () => {
      store.dispatch(
        handleSelectAllRelevancy(this.state.selectAllRelevancyChecked),
      );
      store.dispatch(
        updateSelectedRelevancy(
          this.state.selectAllRelevancyChecked ? [1, 2, 3] : [],
        ),
      );
    });
  };

  handleMultiSelect = value => {
    this.setState({
      selectedAddress: value,
    });
  };

  handleOperationsChange = (event, newAlignment) => {
    this.setState({ operations: newAlignment || 'OR' });
  };

  handleUpdateTags = event => {
    this.setState({ manualTags: event });
  };

  handleInputChange = (e, type) => {
    this.setState({ [type]: e.target.value });
  };

  refreshDurationLimit = e => {
    this.setState({
      refreshDurations: refreshDurationLimit(e.target.value),
    });
  };

  render() {
    const {
      classes,
      themeChange: { setDarkTheme },
    } = this.props;
    const {
      riskCategories,
      relevancy,
      showRiskCategories,
      showRelevancy,
      refreshDurations,
      operations,
      manualTags,
      showAlert,
      subscriptionName,
      subscriptionEmailIds,
      selectedAddress,
      showLanguages,
      language,
    } = this.state;

    let validation = this.submitted
      ? this.validator.validate(this.state)
      : this.state.validation;

    return (
      <div className="createRimeSubscription__inner">
        <h3>Create a new Subscription</h3>
        <div className="field--create--event">
          <section className="rime--subscriptions--modal--section">
            <h2 className="event-head--title">
              <i className="icon-uniE90E" /> Subscription Details
            </h2>
            <div>
              <TextField
                required
                error={validation.subscriptionName.isInvalid}
                id="subscriptionName"
                label="Subscription Name"
                multiline
                fullWidth
                rows="1"
                rowsMax="3"
                value={subscriptionName}
                onChange={e => this.handleInputChange(e, 'subscriptionName')}
                margin="normal"
                variant="outlined"
                InputLabelProps={{
                  shrink: true,
                  classes: {
                    root: classes.rootLabel,
                  },
                }}
                InputProps={{
                  maxLength: 5000,
                  classes: {
                    root: classes.rootInput,
                    input: classes.rootInput,
                    notchedOutline: classes.notchedOutline,
                  },
                  endAdornment: null,
                }}
              />
              {validation.subscriptionName.isInvalid && (
                <span className="error--text">
                  {validation.subscriptionName.message}
                </span>
              )}
            </div>
            <div>
              <TextField
                error={validation.subscriptionEmailIds.isInvalid}
                id="subscriptionEmailIds"
                label="Enter Email Ids (separated by commas)"
                multiline
                required
                fullWidth
                rows="1"
                rowsMax="10"
                value={subscriptionEmailIds}
                onChange={e =>
                  this.handleInputChange(e, 'subscriptionEmailIds')
                }
                margin="normal"
                variant="outlined"
                type="email"
                InputLabelProps={{
                  shrink: true,
                  classes: {
                    root: classes.rootLabel,
                  },
                }}
                InputProps={{
                  maxLength: 5000,
                  classes: {
                    root: classes.rootInput,
                    input: classes.rootInput,
                    notchedOutline: classes.notchedOutline,
                  },
                  endAdornment: null,
                }}
              />
              {validation.subscriptionEmailIds.isInvalid && (
                <span className="error--text">
                  {validation.subscriptionEmailIds.message}
                </span>
              )}
            </div>
          </section>
          <section className="rime--subscriptions--modal--section">
            <h2 className="event-head--title">
              <i className="icon-uniE905" /> Filters
            </h2>
            <div className="multiple--Locations">
              <MultiSelect
                value={selectedAddress}
                placeholder={'Multiple Locations'}
                parentEventHandler={value => this.handleMultiSelect(value)}
                options={multipleAddress}
                allowSelectAll={true}
                className="rime--subscriptions--multiple_EventLocations"
                required
              />
              {validation.selectedAddress.isInvalid && (
                <span className="error--text">
                  {validation.selectedAddress.message}
                </span>
              )}
            </div>
            <div className="rime--subscriptions--toggleOperations">
              <ToggleButtonGroup
                color="primary"
                value={operations}
                exclusive
                onChange={this.handleOperationsChange}
              >
                <ToggleButton value="OR">OR</ToggleButton>
                <ToggleButton value="AND">AND</ToggleButton>
              </ToggleButtonGroup>
            </div>
            <div>
              <TextField
                id="eventTitle"
                label="Enter Tags (Separate with Comma)"
                multiline
                fullWidth
                rows="1"
                rowsMax="3"
                value={manualTags}
                onChange={e => this.handleUpdateTags(e.target.value)}
                margin="normal"
                variant="outlined"
                InputLabelProps={{
                  shrink: true,
                  classes: {
                    root: classes.rootLabel,
                  },
                }}
                InputProps={{
                  maxLength: 5000,
                  classes: {
                    root: classes.rootInput,
                    input: classes.rootInput,
                    notchedOutline: classes.notchedOutline,
                  },
                  endAdornment: null,
                }}
              />
            </div>
            <div className="rime--subscriptions--last--section">
              <div className="rime--subscription--RefreshDuration">
                <TextField
                  id="standard-number"
                  label="Update Duration(Min - 1 hr / Max - 24 hrs)"
                  variant="standard"
                  type="number"
                  InputLabelProps={{
                    shrink: true,
                  }}
                  value={refreshDurations}
                  onChange={e => this.refreshDurationLimit(e)}
                />
              </div>
              <div className="dropdown__list">
                <div
                  className={`${showRiskCategories &&
                    'rime--subscriptions__selectAllEventCatsWhiteRisk'}`}
                >
                  <div className="rime--subscriptions__selectAllEventCats">
                    <label className={classes.rimeSubscriptionsdetailsTitle}>
                      <span>
                        <i className="icon-uniE90E" aria-hidden="true"></i>
                      </span>
                      Risk Categories
                    </label>
                    <span
                      className="filtersWrapBox__arrow"
                      onClick={() =>
                        this.setState({
                          showRiskCategories: !showRiskCategories,
                        })
                      }
                    >
                      {showRiskCategories ? (
                        <i className="fas fa-chevron-up"></i>
                      ) : (
                        <i className="fas fa-chevron-down"></i>
                      )}
                    </span>
                  </div>
                  {showRiskCategories && (
                    <>
                      {categories.map(item => (
                        <CustomCheckboxUi
                          key={item.id}
                          id={item.id}
                          label={item.label}
                          updateCategories={this.updateCategories}
                          isSelected={riskCategories.includes(item.id)}
                          name={item.name}
                          ref={this.categoriesRef}
                        />
                      ))}
                    </>
                  )}
                  {validation.riskCategories.isInvalid && (
                    <span className="error--text">
                      {validation.riskCategories.message}
                    </span>
                  )}
                </div>
                <div
                  className={`${showRelevancy &&
                    'rime--subscriptions__selectAllEventCatsWhiteRisk'}`}
                >
                  <div className="rime--subscriptions__selectAllEventCats">
                    <label className={classes.EventArchievedetailsTitle}>
                      <span>
                        <i className="icon-uniE95E" aria-hidden="true"></i>
                      </span>
                      Relevancy
                    </label>
                    <span
                      className="filtersWrapBox__arrow"
                      onClick={() =>
                        this.setState({ showRelevancy: !showRelevancy })
                      }
                    >
                      {showRelevancy ? (
                        <i className="fas fa-chevron-up"></i>
                      ) : (
                        <i className="fas fa-chevron-down"></i>
                      )}
                    </span>
                  </div>
                  {showRelevancy && (
                    <>
                      {relevancyConstants.map(item => (
                        <CustomRelevancyCheckbox
                          key={item.id}
                          id={item.id}
                          label={item.label}
                          updateRelevancy={this.updateRelevancy}
                          isSelected={relevancy.includes(item.id)}
                          name={item.name}
                        />
                      ))}
                    </>
                  )}
                  {validation.relevancy.isInvalid && (
                    <span className="error--text">
                      {validation.relevancy.message}
                    </span>
                  )}
                </div>
                <div
                  className={`${showLanguages &&
                    'rime--subscriptions__selectAllEventCatsWhiteRisk'}`}
                >
                  <div className="subscription__language">
                    <div className="rime--subscriptions__selectAllEventCats">
                      <label className={classes.rimeSubscriptionsdetailsTitle}>
                        <span>
                          <i
                            className="fa fa-globe"
                            style={{
                              color: 'var(--whiteDisabled)',
                              paddingRight: '5px',
                            }}
                          ></i>
                        </span>
                        Languages
                      </label>
                      <span
                        className="filtersWrapBox__arrow"
                        onClick={() =>
                          this.setState({
                            showLanguages: !showLanguages,
                          })
                        }
                      >
                        {showLanguages ? (
                          <i className="fas fa-chevron-up"></i>
                        ) : (
                          <i className="fas fa-chevron-down"></i>
                        )}
                      </span>
                    </div>
                    {showLanguages && (
                      <>
                        {languageConstants.map(item => (
                          <CustomCheckboxUi
                            key={item.id}
                            id={item.id}
                            label={item.label}
                            updateCategories={this.updateLanguage}
                            isSelected={language.includes(item.id)}
                            name={item.name}
                            ref={this.categoriesRef}
                          />
                        ))}
                      </>
                    )}
                    {validation.language.isInvalid && (
                      <span className="error--text">
                        {validation.language.message}
                      </span>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
        <div className="actionables">
          <Button
            onClick={e => this.props.closeEventModal()}
            variant="contained"
            color="default"
          >
            Cancel
          </Button>
          <Button
            onClick={e => this.handleEventSubmit(e)}
            style={{ marginLeft: 20, backgroundColor: '#2980b9' }}
            variant="contained"
            color="secondary"
          >
            Create Subscription
          </Button>
        </div>
        {showAlert && (
          <AlertDialog
            isOpen={showAlert}
            title="Cancel Changes"
            description="You are about to cancel event changes, you won't be able to get them back, are you sure?"
            parentEventHandler={e => this.handleCloseModal()}
            handleClose={() => this.handleCloseAlert()}
          />
        )}
      </div>
    );
  }
}

export default CreateRimeSubscription;
