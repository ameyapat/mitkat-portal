const validationRules = [
  {
    field: 'subscriptionName',
    method: 'isEmpty',
    validWhen: false,
    message: 'Subscription Name is required',
  },
  {
    field: 'subscriptionEmailIds',
    method: 'isEmpty',
    validWhen: false,
    message: 'Atleast 1 Email Id is required!',
  },
  {
    field: 'riskCategories',
    method: 'isEmpty',
    validWhen: false,
    message: 'Minimum 1 Risk Category is required',
  },
  {
    field: 'relevancy',
    method: 'isEmpty',
    validWhen: false,
    message: 'Minimum 1 Relevency is required',
  },
  {
    field: 'selectedAddress',
    method: 'isEmpty',
    validWhen: false,
    message: 'Minimum 1 Location is required',
  },
  {
    field: 'language',
    method: 'isEmpty',
    validWhen: false,
    message: 'Minimum 1 Language is required',
  }
];
export default validationRules;
