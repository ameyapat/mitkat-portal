import React, { Component } from 'react';
import './style.scss';

class Stats extends Component{
    render(){

        const { 
            totalinfected, 
            totaldeaths, 
            totalrecoverd, 
            newcases, 
            newdeaths, 
            growthratecases, 
            growthratedeaths, 
            statename,
            districts 
        } = this.props.statistics;
        
        return(
            <div id="stats-covid-19">
                <div className="stats-cont">
                    <label>State Name</label>
                    <span className="stats--value">{statename || 'default'}</span>
                </div>
                <div className="stats-cont">
                    <label>Cases</label>
                    <div className="total-stats-cont">
                        <span className="new-cases">                              
                                <span className="arrow-up"><i className="fas fa-angle-double-up" style={{ color: '#F2042C'}}></i></span>
                                <span className="new-cases-value">{newcases}</span>
                                <span className="case-percentage">({growthratecases} %)</span>
                        </span>
                        <span className="t-value"> {totalinfected}</span>
                    </div>
                </div>
                <div className="stats-cont">
                    <label>Deaths</label>
                    <div className="total-stats-cont">
                        <span className="new-cases">                              
                                <span className="arrow-up"><i className="fas fa-angle-double-up" style={{ color: '#F2042C'}}></i></span>
                                <span className="new-cases-value">{newdeaths}</span>
                                <span className="case-percentage">({growthratedeaths} %)</span>
                        </span>
                        <span className="t-value"> {totaldeaths}</span>
                    </div>
                </div>
                <div className="stats-cont">
                    <label>Total Recovered</label>
                    <span className="stats--value" style={{ color: '#A0DD2E'}}>{totalrecoverd || 0}</span>
                </div>
                {
                    districts && districts.length > 0 &&
                    (<div className="stats-cont districtsCont">
                        {/* <label>Districts</label> */}
                        <div className="t--header-district">
                            <span>District Name</span>
                            <span>Count</span>
                        </div>
                        {
                            districts.map(d => 
                                <div className="districts-cont" key={d.id}>
                                    <span className="d--name">{d.districtName}</span>
                                    <span className="d--count">{d.count}</span>
                                </div>
                            )
                        }
                    </div>)
                }
            </div>
        )
    }
}

export default Stats;