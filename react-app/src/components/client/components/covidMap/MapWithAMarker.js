import React, { Component } from 'react';
import {
    withScriptjs,
    withGoogleMap,
    GoogleMap,
    Circle,
  } from "react-google-maps";
import CustomMarker from './CustomMarker';
import { connect } from 'react-redux';
import { withCookies } from 'react-cookie';
import hospitalsIcon from './assets/img/hospital-icon-new.png';
import containmentIcon from './assets/img/containment-solid.png';
import officeIcon from './assets/img/icons8-map-pin-64 (1) 1.png';
import stateIcon from './assets/img/icons8-google-nearby-64.png';
import empIcon from './assets/img/emp-icon-pin.png';
import customMapStyles from './mapStyles.json';
import '../../sass/style.scss';
import CustomPolygon from '../polyMap/CustomPolygon';

const defaultOptions = {
    mapTypeControl: false,
    streetViewControl: false,
    zoomControl: false,
    fullscreenControl: false,
    styles: customMapStyles
}

const colorMap = {
    1: '#3C8C74',
    2: '#E5853A',
    3: '#a72421',
}

const circleOptions = {
    radius: 250,
    options: {
      strokeColor: "#c23709"
    }
  }

@withCookies
@connect(state => {
    return {
        appState: state.appState,
        mapState: state.mapState
    }
})  
@withScriptjs
@withGoogleMap
class MapWithAMarker extends Component{

    state = {
        latState: 20.5937,
        lngState: 80.9629,
    }

    renderClientLocations = () => {
        let { officeLocations } = this.props.feed;
        return officeLocations.map(item => {
            let lat = parseFloat(item.latitude);
            let lng = parseFloat(item.longitude);

            return (
                <CustomMarker
                    id={item.id}
                    key={item.id}
                    lat={lat}
                    lng={lng}
                    feedDetails={item}
                    customIcon={officeIcon}
                    setCenter={this.setCenter}
                    showTitles={true}
                    title={item.address}
                />
            )
        })
    }

    renderContainmentZones = () => {
        let { containmentZones } = this.props.feed;
        return containmentZones.map((item, idx) => {
            let lat = parseFloat(item.latitude);
            let lng = parseFloat(item.longitude);

            return (
                <div  key={item.id + "-" + idx}>
                <CustomMarker
                    id={item.id}
                    key={item.id}
                    lat={lat}
                    lng={lng}
                    feedDetails={item}
                    customIcon={containmentIcon}
                    showTitles={true}
                    title={item.sitename}
                />
                {/* <Circle
                    defaultCenter={{
                    lat,
                    lng
                    }}
                    radius={circleOptions.radius}
                    options={circleOptions.options}
                /> */}
                </div>
            )
        })
    }

    renderInspectionSites = () => {
        let { inspectionSites } = this.props.feed;
        return inspectionSites.map(item => {
            let lat = parseFloat(item.latitude);
            let lng = parseFloat(item.longitude);

            return (
                <CustomMarker
                    id={item.id}
                    key={item.id}
                    lat={lat}
                    lng={lng}
                    feedDetails={item}
                    customIcon={hospitalsIcon}
                    showTitles={true}
                    title={item.sitename}
                />
            )
        })
    }

    renderStateWiseCases = () => {
        let { stateWiseCases } = this.props.feed;
        return stateWiseCases.map(item => {
            let lat = parseFloat(item.latitude);
            let lng = parseFloat(item.longitude);

            return (
                <CustomMarker
                    id={item.id}
                    key={item.id}
                    lat={lat}
                    lng={lng}
                    feedDetails={item}
                    customIcon={stateIcon}
                    showTitles={true}
                    showStatsOnHover={true}
                    updateStats={this.props.updateStats}
                    title={item.statename}
                />
            )
        })
    }

    setCenter = (lat, lng) => this.setState({latState: lat, lngState: lng })
    
    // renderPolygon = () => {
    //     const { polygons } = this.props;
        
    //     return polygons.map(p => {
    //         return(
    //             <CustomPolygon 
    //                 p={p}
    //                 key={p.districtid}
    //                 options={{
    //                     fillColor: colorMap[p.colorid],
    //                     fillOpacity: 0.4,
    //                     strokeColor: colorMap[p.colorid],
    //                     strokeOpacity: 1,
    //                     strokeWeight: 1
    //                 }} 
    //                 latitude={p.defaultCenter.latitude}
    //                 longitude={p.defaultCenter.longitude}
    //                 districtName={p.districtName}
    //             />
    //         )
    //     })
    // }

    renderEmpDetails = () => {
        const { empDetails } = this.props;
        return empDetails.map((item, idx) => {
            let lat = parseFloat(item.latitude);
            let lng = parseFloat(item.longitude);
            return (
                <CustomMarker
                    id={item.id + "-" + idx}
                    key={item.id + "-" + idx}
                    lat={lat}
                    lng={lng}
                    customIcon={empIcon}
                    showTitles={true}
                    title={item.employeeName}
                />
            )
        })
    }

    render(){
        const { officeLocations, containmentZones, inspectionSites, stateWiseCases } = this.props.feed;
        const { empDetails, showEmp } = this.props;

        return(
            <GoogleMap
                defaultZoom={5}
                defaultOptions={defaultOptions}
                defaultCenter={{ lat: 20.5937, lng: 80.9629 }}
            >
                { officeLocations && this.renderClientLocations() } 
                { containmentZones && this.renderContainmentZones() }
                { inspectionSites && this.renderInspectionSites() }
                { stateWiseCases && this.renderStateWiseCases() }
                
                { empDetails.length > 0 && showEmp && this.renderEmpDetails() }
            </GoogleMap>
        )
    }
}

export default MapWithAMarker;

/* { polygons.length > 0 && showHotSpot && this.renderPolygon() } */