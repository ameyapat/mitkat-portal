import React, { Component } from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import injectSheet from 'react-jss';
import './style.scss';
import style from './style';
import stateIcon from './assets/img/icons8-google-nearby-64_2.png';
import officeIcon from './assets/img/icons8-map-pin-64 (1).png';
import hospitalIcon from './assets/img/hospital-icon-new-2.png';
import hotspotIcon from './assets/img/corona-icon-filter.png';
import empIcon from './assets/img/emp-icon-filter.png';
import { Drawer, Button } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { downloadEmpExcel, downloadLocationExcel } from '../../../admin/helpers/utils';
import { withCookies } from 'react-cookie';

const drawerWidth = 450;

const styles = theme => ({
    paperAnchorLeft:{
      zIndex: 996,
      position: 'absolute',
      overflowX: 'hidden',
      overflowY: 'hidden',
      background: 'rgba(0, 0, 0, 0.7)',
      '&:hover':{
        overflowY: 'scroll'
      },
      '&::-webkit-scrollbar':{
        width: 3
      },
      '&::-webkit-scrollbar-track':{
        background: 'transparent'
      },
      '&::-webkit-scrollbar-thumb':{
        backgroundColor: '#ccc',
        borderRadius: 5 
      }
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
      whiteSpace: 'nowrap',
    },
    drawerOpen: {
      width: drawerWidth,
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    drawerClose: {
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      overflowX: 'hidden',
      width: theme.spacing.unit * 7 + 1,
      [theme.breakpoints.up('sm')]: {
        width: theme.spacing.unit * 9 + 1,
      },
    },
    toolbar: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      padding: '0 8px',
      ...theme.mixins.toolbar,
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing.unit * 3,
    },
  });

@withCookies
@withStyles(styles, { withTheme: true })
@injectSheet(style)
class MFilter extends Component{

    state = {
        showDrawer: false,
    }

    handleApplyFilters = () => {
        this.setState({
            showDrawer: false
        })
        this.props.applyFilters();
    }

    handleUpdateHotspot = () => { 
        this.setState({
            showDrawer: false,
        });

        this.props.updateHotSpot(!this.props.showHotSpot);
    }

    handleUpdateEmp = () => { 
        this.setState({
            showDrawer: false,
        });

        this.props.updateShowEmp(!this.props.showEmp);
    }

    render(){
        const { clientlocation, statecaseinfo, containmentzone, inspectionsite } = this.props.filters;
        const { updateFilters, classes, showHotSpot, showEmp, cookies } = this.props;
        const { showDrawer } = this.state;
        const authToken = cookies.get('authToken');
        return(
            <div id="mFilter">
                {
                    !showDrawer &&
                    <div id="covid-19-hambu" onClick={() => this.setState({showDrawer: true})}>
                        <button className="btn-covid-hambu">
                            <i className="fas fa-bars"></i>
                        </button>
                    </div>
                } 
                <Drawer        
                    classes={{
                        root: classes.rootDrawer,
                        paperAnchorLeft: classes.paperAnchorLeft,
                        modal: classes.test
                    }}
                    open={showDrawer}
                    onClose={this.handleDrawerClose}
                    
                >
                   <div className="m--filters">
                       <div className="filter--head-covid">
                           <div className="filter-head-box one">
                               Filter by
                           </div>
                           <div className="filter-head-box">
                                <span
                                        className="close-times"
                                        onClick={() => this.setState({showDrawer: false})}
                                    >
                                        &times;
                                </span>
                           </div>
                       </div>
                       <div className="filter--body-covid">
                            <div className="filter-options">
                                <span className="filter--icon">
                                    <img src={officeIcon} alt="fa-laptop-house" />
                                </span>
                                <span className="filter--label" style={{ display: 'flex', flexDirection: 'column' }}>
                                    <div>Client Locations</div>
                                    <div 
                                        style={{ paddingTop: 10, textDecoration: 'underline', cursor: 'pointer'}}
                                        onClick={() => downloadLocationExcel(authToken)}
                                    >
                                            Download <i style={{ paddingLeft: 5, display: 'inline-block' }} className="far fa-file-excel"></i>
                                    </div>
                                </span>
                                <FormControlLabel
                                    control={
                                        <Switch 
                                            size="small"
                                            color="primary"
                                            checked={clientlocation} 
                                            onChange={() => updateFilters('clientlocation', !clientlocation)} 
                                            name="Client Location"                                         
                                        />
                                    }
                                />
                            </div>
                            <div className="filter-options">
                                <span className="filter--icon">
                                    <i className="fas fa-shield-virus" style={{color: '#EDA900'}}></i>
                                </span>
                                <span className="filter--label">
                                    Containment Zones
                                </span>
                                <FormControlLabel
                                    control={
                                        <Switch 
                                            color="primary"
                                            checked={containmentzone} 
                                            onChange={() => updateFilters('containmentzone', !containmentzone)} 
                                            name="Containment Zones"                                           
                                        />
                                    }
                                />
                            </div>
                            {/* <div className="filter-options">
                                <span className="filter--icon">
                                    <img src={stateIcon} alt="fa-map-marked-alt" style={{ width: 22}} />
                                </span>
                                <span className="filter--label">
                                    State Wise Cases
                                </span>
                                <FormControlLabel
                                    control={
                                        <Switch 
                                            color="primary"
                                            checked={statecaseinfo} 
                                            onChange={() => updateFilters('statecaseinfo', !statecaseinfo)} 
                                            name="State Wise Cases"                                           
                                        />
                                    }
                                />
                            </div> */}
                            <div className="filter-options">
                                <span className="filter--icon">
                                    <img style={{ width: 22}} src={hospitalIcon} alt="fa-hand-holding-medical" />
                                </span>
                                <span className="filter--label">
                                    Hospitals
                                </span>
                                <FormControlLabel
                                    control={
                                        <Switch 
                                            color="primary"
                                            checked={inspectionsite} 
                                            onChange={() => updateFilters('inspectionsite', !inspectionsite)} 
                                            name="Hospitals/Inspection Labs"                                             
                                        />
                                    }
                                />
                            </div>
                            {/* <div className="filter-options">
                                <span className="filter--icon">                            
                                    <img style={{ width: 22}} src={hotspotIcon} alt="fa-hand-holding-medical" />
                                </span>
                                <span className="filter--label">
                                    Hotspots
                                </span>
                                <FormControlLabel
                                    control={
                                        <Switch 
                                            color="primary"
                                            checked={showHotSpot} 
                                            onChange={() => this.handleUpdateHotspot()} 
                                            name="Hotspot" 
                                            classes={{
                                                track: classes.track
                                            }}
                                        />
                                    }
                                />
                            </div> */}
                            <div className="filter-options">
                                <span className="filter--icon">                            
                                    <img style={{ width: 22}} src={empIcon} alt="fa-hand-holding-medical" />
                                </span>
                                <span className="filter--label" style={{ display: 'flex', flexDirection: 'column' }}>
                                    <div>Employee Locations</div>
                                    <div 
                                        style={{ paddingTop: 10, textDecoration: 'underline', cursor: 'pointer'}}
                                        onClick={() => downloadEmpExcel(authToken)}
                                    >
                                            Download <i style={{ paddingLeft: 5, display: 'inline-block' }} className="far fa-file-excel"></i>
                                    </div>
                                </span>
                                <FormControlLabel
                                    control={
                                        <Switch 
                                            color="primary"
                                            checked={showEmp} 
                                            onChange={() => this.handleUpdateEmp()} 
                                            name="Employee Locations" 
                                        />
                                    }
                                />
                            </div>
                       </div>
                       <div className="filter--footer-covid">
                            <Button 
                                variant="contained"
                                color="default"
                                onClick={this.handleApplyFilters}
                            >
                                Apply Filter
                            </Button>
                       </div>
                   </div>
                </Drawer>               
            </div>
        )
    }
}

export default MFilter;