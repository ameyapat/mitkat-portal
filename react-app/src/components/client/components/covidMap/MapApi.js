import React from 'react';
import MapWithAMarker from './MapWithAMarker';
// import { withCookies } from 'react-cookie';

const MapApi = (props) => { 
        return(
            <MapWithAMarker
                googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyCYOTLyn6xbysArlvlW4zpxnH9p6H3An08&v=3.exp&libraries=geometry,drawing,places,visualization"
                loadingElement={<div style={{ height: `100vh`, backgroundColor: '#333' }}>Loading...</div>}
                containerElement={<div style={{ height: `100vh`, position: 'relative' }} />}
                mapElement={<div style={{ height: `100vh`, backgroundColor: '#333' }}>Loading...</div>}
                feed={props.feed}
                updateStats={props.updateStats}
                // polygons={this.props.polygons}
                showHotSpot={props.showHotSpot}
                empDetails={props.empDetails}
                showEmp={props.showEmp}
            />
        )
} 

export default MapApi;