import React, { Component } from 'react';
import {
    Marker,
    InfoWindow
  } from "react-google-maps";
import injectSheet from 'react-jss';
import style from './style';
import { withCookies } from 'react-cookie';

@withCookies
@injectSheet(style)
class CustomMarker extends Component {

    state = {
        showInfoBox : false,
        showModal : false,
        eventDetail: ""
    }

    markerRef = React.createRef();

    handleShowInfoBox = (showInfoBox) => {
        this.setState({showInfoBox});
    }

    handleUpdateStats = () => {
        this.props.updateStats(this.props.feedDetails);
        this.handleShowInfoBox(true);
    }

  render() {

    let { 
        title, 
        lat, 
        lng, 
        customIcon, 
        classes,
        showTitles,
        showStatsOnHover = false,
        feedDetails,
        updateStats
    } = this.props;
    let { showInfoBox } = this.state;

    return (
        <Marker
            onMouseOver={() => showStatsOnHover ? this.handleUpdateStats() : this.handleShowInfoBox(true)}
            onMouseOut={() => this.handleShowInfoBox(false)}
            position={{ lat, lng }}
            onClick={showStatsOnHover ? () => updateStats(feedDetails) : () => console.log('not allowed!!!')}
            icon={customIcon}
            ref={this.markerRef}
            optimized={false}
        >
            {
                showInfoBox && showTitles &&
                <React.Fragment>
                    <InfoWindow
                        onCloseClick={() => this.handleShowInfoBox(false)}
                    >
                        <div className={classes.rootAlertListWrap}>
                            <p className="alert--title">{ title }</p>
                        </div>
                    </InfoWindow>                    
                </React.Fragment>
            }
        </Marker>
    )
  }
}

export default CustomMarker;
