import React, { Component } from 'react';
import './style.scss';

class MStats extends Component{
    render(){

        const { 
            totalinfected, 
            totaldeaths, 
            totalrecoverd, 
            newcases, 
            newdeaths, 
            growthratecases, 
            growthratedeaths, 
            statename,
            districts 
        } = this.props.statistics;
        
        return(
            <div id="mstats-covid-19">
                <div className="bottom--stats">                
                    <div className="stats-cont">
                        <label>State</label>
                        <span className="stats--value">{statename || 'default'}</span>
                    </div>
                    <div className="stats-cont">
                        <label>Cases</label>
                        <div className="total-stats-cont">
                            <span className="new-cases">                              
                                    <span className="new-cases-value">{newcases}</span>
                                    <span className="case-percentage">({growthratecases} %)</span>
                            </span>
                            <span className="t-value"> {totalinfected}</span>
                        </div>
                    </div>
                    <div className="stats-cont">
                        <label>Deaths</label>
                        <div className="total-stats-cont">
                            <span className="new-cases">                                                                  
                                    <span className="new-cases-value">{newdeaths}</span>
                                    <span className="case-percentage">({growthratedeaths} %)</span>
                            </span>
                            <span className="t-value"> {totaldeaths}</span>
                        </div>
                    </div>
                    <div className="stats-cont">
                        <label>Recovered</label>
                        <span className="stats--value" style={{ color: '#A0DD2E'}}>{totalrecoverd || 0}</span>
                    </div>
                </div>
                {
                    districts && districts.length > 0 &&
                    (<div id="district-m" className="stats-cont districtsCont">
                        {/* <label>Districts</label> */}
                        <div className="t--header-district">
                            <span className="d--name">District Name</span>
                            <span>Count</span>
                        </div>
                        {
                            districts.map(d => 
                                <div className="districts-cont" key={d.id}>
                                    <span className="d--name">{d.districtName}</span>
                                    <span className="d--count">{d.count}</span>
                                </div>
                            )
                        }
                    </div>)
                }
            </div>
        )
    }
}

export default MStats;