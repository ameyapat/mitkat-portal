import React, { Component } from 'react';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import injectSheet from 'react-jss';
import './style.scss';
import styles from './style';
import stateIcon from './assets/img/icons8-google-nearby-64_2.png';
import officeIcon from './assets/img/icons8-map-pin-64 (1).png';
import hospitalIcon from './assets/img/hospital-icon-new-2.png';
import hotspotIcon from './assets/img/corona-icon-filter.png';
import empIcon from './assets/img/emp-icon-filter.png';
import { withCookies } from 'react-cookie';
import { downloadEmpExcel, downloadLocationExcel } from '../../../admin/helpers/utils';
@withCookies
@injectSheet(styles)
class Filter extends Component{
    render(){
        const { clientlocation, statecaseinfo, containmentzone, inspectionsite } = this.props.filters;
        const { updateFilters, classes, showHotSpot, updateHotSpot, showEmp, updateShowEmp, cookies } = this.props;
        const authToken = cookies.get('authToken');
        
        return(
            <div id="covid-map-filter">
                <div className="filter-options">
                    <span className="filter--icon">
                        <img src={officeIcon} alt="fa-laptop-house" />
                    </span>
                    <span className="filter--label" style={{ display: 'flex', flexDirection: 'column' }}>
                        <div>Client Locations</div>
                        <div 
                            style={{ paddingTop: 10, textDecoration: 'underline', cursor: 'pointer'}}
                            onClick={() => downloadLocationExcel(authToken)}
                        >
                                Download <i style={{ paddingLeft: 5, display: 'inline-block' }} className="far fa-file-excel"></i>
                        </div>
                    </span>
                    <FormControlLabel
                        control={
                            <Switch 
                                size="small"
                                color="primary"
                                checked={clientlocation} 
                                onChange={() => updateFilters('clientlocation', !clientlocation)} 
                                name="Client Location" 
                            />
                        }
                    />
                </div>
                <div className="filter-options">
                    <span className="filter--icon">
                        <i className="fas fa-shield-virus" style={{color: '#EDA900'}}></i>
                    </span>
                    <span className="filter--label">
                        Containment Zones
                    </span>
                    <FormControlLabel
                        control={
                            <Switch 
                                color="primary"
                                checked={containmentzone} 
                                onChange={() => updateFilters('containmentzone', !containmentzone)} 
                                name="Containment Zones" 
                            />
                        }
                    />
                </div>
                {/* <div className="filter-options">
                    <span className="filter--icon">
                        <img src={stateIcon} alt="fa-map-marked-alt" style={{ width: 22}} />
                    </span>
                    <span className="filter--label">
                        State Wise Cases
                    </span>
                    <FormControlLabel
                        control={
                            <Switch 
                                color="primary"
                                checked={statecaseinfo} 
                                onChange={() => updateFilters('statecaseinfo', !statecaseinfo)} 
                                name="State Wise Cases" 
                            />
                        }
                    />
                </div> */}
                <div className="filter-options">
                    <span className="filter--icon">
                        <img style={{ width: 22}} src={hospitalIcon} alt="fa-hand-holding-medical" />
                    </span>
                    <span className="filter--label">
                        Hospitals
                    </span>
                    <FormControlLabel
                        control={
                            <Switch 
                                color="primary"
                                checked={inspectionsite} 
                                onChange={() => updateFilters('inspectionsite', !inspectionsite)} 
                                name="Hospitals/Inspection Labs" 
                            />
                        }
                    />
                </div>
                {/* <div className="filter-options">
                    <span className="filter--icon">
                        <img style={{ width: 22}} src={hotspotIcon} alt="fa-hand-holding-medical" />
                    </span>
                    <span className="filter--label">
                        Hotspots
                    </span>
                    <FormControlLabel
                        control={
                            <Switch 
                                color="primary"
                                checked={showHotSpot} 
                                onChange={() => updateHotSpot(!showHotSpot)} 
                                name="Hotspot" 
                                classes={{
                                    track: classes.track
                                }}
                            />
                        }
                    />
                </div> */}
                <div className="filter-options">
                    <span className="filter--icon">
                        <img style={{ width: 22}} src={empIcon} alt="employee" />
                    </span>
                    <span className="filter--label" style={{ display: 'flex', flexDirection: 'column' }}>
                        <div>Employee Locations</div>
                        <div 
                            style={{ paddingTop: 10, textDecoration: 'underline', cursor: 'pointer'}}
                            onClick={() => downloadEmpExcel(authToken)}
                        >
                                Download <i style={{ paddingLeft: 5, display: 'inline-block' }} className="far fa-file-excel"></i>
                        </div>
                    </span>
                    <FormControlLabel
                        control={
                            <Switch 
                                color="primary"
                                checked={showEmp} 
                                onChange={() => updateShowEmp(!showEmp)} 
                                name="Employee Locations" 
                            />
                        }
                    />
                </div>
            </div>
        )
    }
}

export default Filter;