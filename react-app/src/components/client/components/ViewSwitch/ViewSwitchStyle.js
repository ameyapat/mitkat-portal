export default {
  paperMenu: {
    background: 'var(--backgroundSolid)',
  },
  rootMenuItem: {
    color: 'var(--whiteHighEmp)',
    '& a': {
      color: 'var(--whiteHighEmp)',
    },
  },
  rootDivider: {
    backgroundColor: 'rgba(255, 255, 255, 0.12) !important',
    margin: '10px 0',
  },
  rootControlLabel: {
    marginLeft: '0  !important',
    marginRight: '0  !important',
  },
};
