import React, { Component } from 'react';
import './ViewSwitch.scss';
import MapToggle from '../../../ui/MapToggle/MapToggle';
import { withRouter } from 'react-router-dom';
import { withCookies } from 'react-cookie';
import { connect } from 'react-redux';
import { setHeader, toggleShowSettings } from '../../../../actions';
import Tooltip from '@material-ui/core/Tooltip';
import { withStyles } from '@material-ui/core/styles';
import { downloadCountryReport } from '../../../admin/helpers/utils';
import logoWhite from '../../../../assets/logo_colorful.png';
import { Drawer } from '@material-ui/core';
import ProfileMenu from './components/profileMenu';
import HelpMenu from './components/helpMenu';
import mitkatLogoLight from '../../../../assets/svg/MitKat-Logo-White.svg';
import mitkatLogoBlue from '../../../../assets/svg/MitKat-Logo-Blue.svg';
import blackPearlLogo from '../../../../assets/partners/BlackPearlLogo.png';
import sanctuaryRiskLogo from '../../../../assets/partners/SanctuaryRisk.png';

const drawerWidth = 300;

const drawerstyles = theme => ({
  paperAnchorLeft: {
    width: drawerWidth,
    zIndex: 996,
    position: 'absolute',
    overflowX: 'hidden',
    overflowY: 'hidden',
    background: 'var(--backgroundSolid)',
    '& ul': {
      color: 'rgba(255, 255, 255, 0.87)',
    },
    '&:hover': {
      overflowY: 'scroll',
    },
    '&::-webkit-scrollbar': {
      width: 3,
    },
    '&::-webkit-scrollbar-track': {
      background: 'transparent',
    },
    '&::-webkit-scrollbar-thumb': {
      backgroundColor: '#ccc',
      borderRadius: 5,
    },
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflow: 'hidden',
    width: theme.spacing.unit * 7 + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing.unit * 9 + 1,
    },
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
  },
});
@withCookies
@withRouter
@connect(state => {
  return {
    rime: state.rime,
    partnerDetails: state.partnerDetails.partnerDetails,
    themeChange: state.themeChange,
  };
})
@withStyles(drawerstyles, { withTheme: true })
class ViewSwitch extends Component {
  state = {
    showDrawer: false,
  };
  componentDidMount() {
    this.headerHandler();
  }

  handleOpenSettings = () => {
    const { dispatch } = this.props;
    dispatch(toggleShowSettings(true));
  };

  handleReset = () => this.props.history.push('/reset');

  vaccinationReportDownload = () => {
    const { countryId } = this.props;
    downloadCountryReport(countryId);
  };

  headerHandler = () => {
    const { dispatch } = this.props;
    let path = this.props.match.url;

    let activeTab = '';
    switch (path) {
      case '/client/dashboard':
        activeTab = 'Risk Tracker';
        break;
      case '/client/rime':
        activeTab = 'RIME';
        break;
      case '/client/riskExposure':
        activeTab = 'Threat Intelligence Platform';
        break;
      default:
        break;
    }
    dispatch(setHeader(activeTab));
  };

  render() {
    const {
      classes,
      themeChange: { setDarkTheme },
    } = this.props;
    const { showDrawer } = this.state;
    let { partnerName } = this.props.partnerDetails;

    return (
      <div className={`rootViewSwitch ${setDarkTheme ? 'dark' : 'light'}`}>
        <div className="topGrid">
          <div className="mitkatTransparentLogo__wrap">
            <img
              src={logoWhite}
              alt="MitKat Logo"
              onClick={() => this.props.history.push('/client/dashboard')}
            />
          </div>
        </div>

        <div className="bottomGrid">
          <div className="rootViewSwitch__Grid">
            <Tooltip title="Switch">
              <button
                className="btnAvatar"
                onClick={() => this.setState({ showDrawer: true })}
              >
                <i className="fas fa-th-large"></i>
              </button>
            </Tooltip>
          </div>

          {/* HELP */}
          <HelpMenu />
          {/* HELP */}

          {/* Profile Menu */}
          <ProfileMenu />
          {/* Profile End */}
        </div>

        <Drawer
          className={`${setDarkTheme ? 'dark' : 'light'}`}
          classes={{
            root: classes.rootDrawer,
            paperAnchorLeft: classes.paperAnchorLeft,
            modal: classes.test,
          }}
          open={showDrawer}
          onClose={this.handleDrawerClose}
        >
          <div className="drawer--header">
            <div>
              {partnerName === 'BlackPearl' ? (
                <img
                  className="blackpearl--Logo--Switch"
                  src={blackPearlLogo}
                />
              ) : partnerName === 'Sanctuary Risk' ? (
                <img
                  className="sanctuary--Logo--Switch"
                  src={sanctuaryRiskLogo}
                />
              ) : (
                <img
                  className="mitKat--Logo--Switch"
                  src={setDarkTheme ? mitkatLogoLight : mitkatLogoBlue}
                />
              )}
            </div>
            <a
              className="close--drawer--btn"
              onClick={() => this.setState({ showDrawer: false })}
            >
              <i className="fas fa-chevron-left"></i>
            </a>
          </div>
          <MapToggle />
        </Drawer>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  search: state.search,
  currentHeaderTitle: state.appState.currentHeaderTitle,
});
const mapDispatchToProps = dispatch => ({ dispatch });

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(withCookies(ViewSwitch)),
);
