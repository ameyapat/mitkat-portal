import React, { Component } from 'react';
import '../ViewSwitch.scss';
import style from '../ViewSwitchStyle';
import { withRouter } from 'react-router-dom';
import { withCookies } from 'react-cookie';
import { connect } from 'react-redux';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Tooltip from '@material-ui/core/Tooltip';
import { Divider, withStyles } from '@material-ui/core';
import { helpItems } from '../helpers/constants';
@withCookies
@withRouter
@withStyles(style)
class HelpMenu extends Component {
  state = {
    anchorEl: null,
  };

  handleClose = () => this.setState({ anchorEl: null });
  handleOpen = e => {
    this.setState({ anchorEl: e.currentTarget });
  };

  handleRoute = to => {
    this.setState({ anchorEl: null });
    let pathname = this.props.match.url;
    this.props.history.push(`${pathname}/${to}`);
  };

  getHelpMenuList = () => {
    const { classes } = this.props;
    let helpXML = [];
    let pathname = this.props.match.url;
    let helpOptions = helpItems.helpOptions;

    helpXML = helpOptions.map(item => {
      if (item.isRoute === true) {
        return (
          <MenuItem
            classes={{
              root: classes.rootMenuItem,
            }}
          >
            <i className={item.icon}></i>
            <a href={item.to} target="_blank">
              <span className="marginLft">{item.title}</span>
            </a>
          </MenuItem>
        );
      } else {
        return (
          <MenuItem
            classes={{
              root: classes.rootMenuItem,
            }}
            onClick={() => this.handleRoute(item.type)}
          >
            <i className={item.icon}></i>

            <span className="marginLft">{item.title}</span>
          </MenuItem>
        );
      }
    });
    return helpXML;
  };
  render() {
    const {
      history: {
        location: { pathname },
      },
      classes,
      themeChange: { setDarkTheme },
    } = this.props;

    const { anchorEl } = this.state;

    return (
      <>
        <div className="rootViewSwitch__Grid">
          <Tooltip title="help">
            <button className="btnAvatar" onClick={this.handleOpen}>
              <i className="fas fa-info-circle"></i>
            </button>
          </Tooltip>
        </div>

        <Menu
          id="user-profile-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={this.handleClose}
          classes={{
            paper: classes.paperMenu,
          }}
          className={`${setDarkTheme ? 'dark' : 'light'}`}
        >
          {this.getHelpMenuList()}

          {/* 
          
          ******** This is already a Commented Code, Don't know why it is commented - Shubham Khandhar *********
          
          <MenuItem
            classes={{
              root: classes.rootMenuItem,
            }}
          >
            <i className="fas fa-video dropIcons"></i>
            <a
              href="https://mitkathelp.s3.aphelpamazonaws.com/IMG-8542.MOV.mp4"
              target="_blank"
            >
              <span className="marginLft">Demo</span>
            </a>
          </MenuItem>
          <MenuItem
            classes={{
              root: classes.rootMenuItem,
            }}
          >
            <i className="fas fa-book-open dropIcons"></i>
            <a
              href="https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/MitKat+Risk+Tracker+Portal+User+Manual.pdf"
              target="_blank"
            >
              <span className="marginLft">Manual</span>
            </a>
          </MenuItem>
          <Divider
            classes={{
              root: classes.rootDivider,
            }}
          />
          <MenuItem
            classes={{
              root: classes.rootMenuItem,
            }}
          >
            <i className="fas fa-file-invoice"></i>
            <a
              href="https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/IMG-8542.MOV.mp4"
              target="_blank"
            >
              <span className="marginLft">Certificates and Awards</span>
            </a>
          </MenuItem>
          <Divider
            classes={{
              root: classes.rootDivider,
            }}
          />
          <MenuItem
            classes={{
              root: classes.rootMenuItem,
            }}
          >
            <i className="fas fa-file-invoice"></i>
            <a
              href="https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/IMG-8542.MOV.mp4"
              target="_blank"
            >
              <span className="marginLft">Terms of service</span>
            </a>
          </MenuItem>
          <MenuItem
            classes={{
              root: classes.rootMenuItem,
            }}
          >
            <i className="fas fa-lock"></i>
            <a
              href="https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/MitKat+Risk+Tracker+Portal+User+Manual.pdf"
              target="_blank"
            >
              <span className="marginLft">Privacy policy</span>
            </a>
          </MenuItem> */}
        </Menu>
      </>
    );
  }
}

const mapStateToProps = state => ({
  search: state.search,
  rime: state.rime,
  themeChange: state.themeChange,
});
const mapDispatchToProps = dispatch => ({ dispatch });

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(withCookies(HelpMenu)),
);
