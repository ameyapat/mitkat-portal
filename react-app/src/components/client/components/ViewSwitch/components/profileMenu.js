import React, { Component } from 'react';
import '../ViewSwitch.scss';
import style from '../ViewSwitchStyle';
import { withRouter } from 'react-router-dom';
import { withCookies } from 'react-cookie';
import * as actions from '../../../../../actions';
import { connect } from 'react-redux';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { toggleShowSettings } from '../../../../../actions';
import {
  updateSettings,
  getSubscriptions,
} from '../../../../../requestor/mitkatWeb/requestor';
import Tooltip from '@material-ui/core/Tooltip';

import CustomModal from '../../../../ui/modal';
import ModalWithPortal from '../../../../ui/modalWithPortal/ModalWithPortal';

import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import { Button, withStyles } from '@material-ui/core';
import { themeChangeToggle } from '../../../../../actions/themeAction';
import { managementItems } from '../helpers/constants';

@withCookies
@withRouter
@withStyles(style)
class ProfileMenu extends Component {
  state = {
    anchorEl: null,
    enableLockMapScreen: false,
    enableShowLocations: true,
    enableTheme: true,
    darkMode: true,
  };

  handleClose = () => this.setState({ anchorEl: null });
  handleOpen = e => {
    this.setState({ anchorEl: e.currentTarget });
  };

  handleOpenSettings = () => {
    const { dispatch } = this.props;
    dispatch(toggleShowSettings(true));
  };

  handleLogout = () => {
    let { cookies } = this.props;
    cookies.remove('authToken');
    cookies.remove('userRole');
    cookies.remove('partnerId');

    this.closeSettings();

    setTimeout(() => {
      this.props.history.push('/');
    }, 2000);
  };

  closeSettings = () => {
    const { dispatch } = this.props;
    dispatch(actions.toggleShowSettings(false));
  };

  handleLockScreen = () => {
    const { dispatch } = this.props;
    const { lockScreen } = this.props.rime;
    dispatch(actions.toggleLockScreen(!lockScreen));
  };

  handleShowLocations = () => {
    const {
      dispatch,
      appState: {
        appPermissions: { locationPinSetting, colorThemeSetting },
      },
    } = this.props;

    const reqObj = {
      locationPinSetting: !locationPinSetting,
      colorThemeSetting,
    };

    const reqData = {
      locationPinSetting: reqObj.locationPinSetting ? 1 : 0,
      colorThemeSetting: reqObj.colorThemeSetting ? 1 : 0,
    };

    updateSettings(reqData).then(data => {
      getSubscriptions().then(data => {
        dispatch(actions.setAppPermissions(data));
      });
    });
  };

  handleToggleTheme = () => {
    const {
      dispatch,
      appState: {
        appPermissions: { locationPinSetting, colorThemeSetting },
      },
    } = this.props;

    const reqObj = {
      locationPinSetting,
      colorThemeSetting: !colorThemeSetting,
    };

    const reqData = {
      locationPinSetting: reqObj.locationPinSetting ? 1 : 0,
      colorThemeSetting: reqObj.colorThemeSetting ? 1 : 0,
    };

    updateSettings(reqData).then(data => {
      getSubscriptions().then(data => {
        dispatch(actions.setAppPermissions(data));
      });
    });
  };

  handleReset = () => this.props.history.push('/reset');

  handleRoute = to => {
    this.setState({ anchorEl: null });
    let pathname = this.props.match.url;
    this.props.history.push(`${pathname}/${to}`);
  };

  getManagementMenuList = () => {
    const { classes } = this.props;
    let managementXML = [];
    let managementOptions = managementItems.managementOptions;

    managementXML = managementOptions.map(item => {
      if (item.isRoute === true) {
        return (
          <MenuItem
            classes={{
              root: classes.rootMenuItem,
            }}
          >
            <i className={item.icon}></i>
            <a href={item.to} target="_blank">
              <span className="marginLft">{item.title}</span>
            </a>
          </MenuItem>
        );
      } else {
        return (
          <MenuItem
            classes={{
              root: classes.rootMenuItem,
            }}
            onClick={() => this.handleRoute(item.type)}
          >
            <i className={item.icon}></i>
            <span className="marginLft">{item.title}</span>
          </MenuItem>
        );
      }
    });
    return managementXML;
  };

  render() {
    const {
      history: {
        location: { pathname },
      },
      classes,
      appState: {
        appPermissions: { colorThemeSetting, locationPinSetting, parentCLient },
      },
      themeChange: { setDarkTheme },
      dispatch,
    } = this.props;

    let { showSettings, lockScreen, showLocationPin } = this.props.rime;
    const {
      anchorEl,
      enableLockMapScreen,
      enableShowLocations,
      enableTheme,
    } = this.state;

    return (
      <>
        <div className="rootViewSwitch__Grid">
          <Tooltip title="Profile">
            <button className="btnAvatar" onClick={this.handleOpen}>
              <i className="far fa-user-circle"></i>
            </button>
          </Tooltip>
          <Menu
            id="user-profile-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={this.handleClose}
            classes={{
              paper: classes.paperMenu,
            }}
            className={`${setDarkTheme ? 'dark' : 'light'}`}
          >
            {parentCLient === 1 && this.getManagementMenuList()}
            <MenuItem
              onClick={this.handleReset}
              classes={{
                root: classes.rootMenuItem,
              }}
            >
              <i className="fas fa-unlock-alt dropIcons"></i>
              <span className="marginLft">Reset Password</span>
            </MenuItem>

            <MenuItem
              classes={{
                root: classes.rootMenuItem,
              }}
              onClick={this.handleOpenSettings}
            >
              <i className="fas fa-cogs dropIcons"></i>
              <span className="marginLft">Settings</span>
            </MenuItem>

            <MenuItem
              onClick={this.handleLogout}
              classes={{
                root: classes.rootMenuItem,
              }}
            >
              <i className="fas fa-sign-out-alt dropIcons"></i>
              <span className="marginLft">Logout</span>
            </MenuItem>
          </Menu>
          <ModalWithPortal>
            <CustomModal showModal={showSettings}>
              <div id="rimeUserSettings">
                <div className="userSettings__header">
                  <h1>Settings</h1>
                  <button className="btn--close" onClick={this.closeSettings}>
                    &times;
                  </button>
                </div>
                <div className="userSettings__body">
                  {enableLockMapScreen && (
                    <div className="userSettings_toggles">
                      <span className="userSettings__label">
                        Lock Map Screen
                      </span>
                      <FormControlLabel
                        control={
                          <Switch
                            checked={!lockScreen}
                            onChange={this.handleLockScreen}
                            name="lockScreen"
                          />
                        }
                        classes={{
                          root: classes.rootControlLabel,
                          label: classes.rootLabelControlLabel,
                          switchBase: classes.switchBase,
                          thumb: classes.switchThumb,
                          track: classes.switchTrack,
                          checked: classes.switchChecked,
                        }}
                      />
                    </div>
                  )}
                  {enableShowLocations && (
                    <div className="userSettings_toggles">
                      <span className="userSettings__label">
                        Show Locations
                      </span>
                      <FormControlLabel
                        control={
                          <Switch
                            checked={locationPinSetting}
                            onChange={this.handleShowLocations}
                            name="showLocationPin"
                          />
                        }
                        classes={{
                          root: classes.rootControlLabel,
                          label: classes.rootLabelControlLabel,
                        }}
                      />
                    </div>
                  )}
                  {/* Unstable code begins*/}
                  {enableTheme && (
                    <div className="userSettings_toggles">
                      <span className="userSettings__label">Switch Theme</span>
                      <div>
                        <i
                          className="fas fa-sun"
                          style={{ color: '#f1c749' }}
                        ></i>

                        <FormControlLabel
                          control={
                            <Switch
                              checked={setDarkTheme}
                              onChange={() => {
                                this.handleToggleTheme();
                                dispatch(themeChangeToggle(!setDarkTheme));
                              }}
                              name="setDarkTheme"
                            />
                          }
                          classes={{
                            root: classes.rootControlLabel,
                            label: classes.rootLabelControlLabel,
                            switchBase: classes.switchBase,
                            thumb: classes.switchThumb,
                            track: classes.switchTrack,
                            checked: classes.switchChecked,
                          }}
                        />
                        <i
                          className="fas fa-moon"
                          style={{ color: '#78818f' }}
                        ></i>
                      </div>
                    </div>
                  )}
                  {/* Unstable code ends*/}
                </div>
              </div>
            </CustomModal>
          </ModalWithPortal>
        </div>
      </>
    );
  }
}

const mapStateToProps = state => ({
  search: state.search,
  rime: state.rime,
  appState: state.appState,
  themeChange: state.themeChange,
});
const mapDispatchToProps = dispatch => ({ dispatch });

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(withCookies(ProfileMenu)),
);
