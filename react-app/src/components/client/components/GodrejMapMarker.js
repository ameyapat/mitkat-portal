import React, { Component } from 'react';
import {
    withScriptjs,
    withGoogleMap,
    GoogleMap,
  } from "react-google-maps";
import GodrejCustomMarker from './GodrejCustomMarker';
import customMarker from './GMap/assets/img/godrej-location-pin.png';
// import customMapStyles from './godrejMapStyles.json';

const defaultOptions = {
    mapTypeControl: false,
    streetViewControl: false,
    zoomControl: false,
    fullscreenControl: false,
    // styles: customMapStyles
}

@withScriptjs
@withGoogleMap
class GodrejMapMarker extends Component {
    renderPins = () => {
        let { eventList } = this.props;
        return eventList.map((items, idx) => {
            return items.categorywisedistribution.map(item => {
                return (
                        <GodrejCustomMarker
                        id={item.id}
                        key={item.id}
                        lat={item.latitude}
                        lng={item.longitude}
                        customIcon={customMarker}
                        // title={list.title}
                        />
                    )
                })
            });
    }

    render(){
        return(
            <GoogleMap
                defaultZoom={1.6}
                defaultOptions={defaultOptions}
                defaultCenter={{ lat: 0, lng: 0 }}
            >
                { this.renderPins() }                
            </GoogleMap>
        );
    }
}

export default GodrejMapMarker;