import React from 'react';
import Barclays from '../../../client/Barclays';

const BarclaysClientDashboard = props => {
  return <Barclays {...props} />;
};

export default BarclaysClientDashboard;
