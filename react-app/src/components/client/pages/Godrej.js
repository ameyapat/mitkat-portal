import React, { Component } from 'react';
import { withCookies } from 'react-cookie';
import { fetchApi } from '../../../helpers/http/fetch';
import { API_ROUTES } from '../../../helpers/http/apiRoutes';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import { getArrFromObj } from '../../../helpers/utils';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { withRouter } from 'react-router-dom';
import store from '../../../store';
import { toggleIsFetch } from '../../../actions';
import GodrejLogo from '../../../assets/godrej-logo.png';
import MitkatLogo from '../../../assets/mitkat-logo-filled.png';
import { withStyles } from '@material-ui/core';
import GodrejFilters from '../components/GodrejFilters';
import GodrejEvents from '../components/GodrejEvents';
import GodrejMap from '../components/GodrejMap';
import FilterListIcon from '@material-ui/icons/FilterList';
import Drawer from '@material-ui/core/Drawer';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import Typography from '@material-ui/core/Typography';
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';
import { getEmergingRisks } from '../../../api/api';
import EmergingRiskBox from '../components/EmergingRiskBox/EmergingRiskBox';

const drawerWidth = 420;

const styles = theme => ({
  rootAppBar: {
    backgroundColor: '#fff',
  },
  rootToolbar: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  btnGreen: {
    backgroundColor: '#53ab3e',
    color: '#fff',
    '&:hover': {
      backgroundColor: 'rgba(83, 171, 62, 0.8)',
    },
  },
  logoWrapper: {
    display: 'flex',
    alignItems: 'center',
  },
  godrejLogo: {
    width: '90px',
  },
  mitkatLogo: {
    width: '128px',
    maxWidth: '100%',
    borderLeft: '1px solid #d3d3d3',
    borderRight: '1px solid #d3d3d3',
    paddingLeft: '15px',
    paddingRight: '15px',
    marginLeft: '15px',
  },
  rootEventWrapper: {
    flex: 3,
    minHeight: '100vh',
    backgroundColor: '#f5f5f5',
  },
  rootContentWrapper: {
    display: 'flex',
  },
  rootMapWrappper: {
    flex: 2,
    // backgroundColor: "yellow",
    borderLeft: '1px solid #d3d3d3',
    minHeight: '100vh',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-evenly',
  },
  bannerWrapper: {
    backgroundColor: 'green',
    minHeight: '128px',
  },
  mapWrap: {
    flex: '1',
  },
  emergingRiskWrap: {
    flex: '1',
  },
  filters: {
    borderTop: '1px solid #f1f1f1',
    padding: '10px 20px',
    display: 'flex',
    justifyContent: 'space-between',
    background: '#fff',
    alignItems: 'center',
  },
  rootDrawer: {},
  paper: {
    width: drawerWidth,
  },
  arrowClose: {
    display: 'flex',
    justifyContent: 'space-between',
    padding: '5px 10px',
    borderBottom: '1px solid #f1f1f1',
    alignItems: 'center',
  },
  rootTypography: {
    display: 'flex',
    alignItems: 'center',
  },
  rootTypographyFilter: {
    display: 'flex',
    alignItems: 'center',
    marginRight: '10px',
  },
  rootTypographyDownload: {
    display: 'flex',
    alignItems: 'center',
    marginLeft: '10px',
    color: 'var(--whiteMediumEmp)',
  },
  drawerFilterIcon: {
    marginRight: '10px',
  },
  typographyRiskWatch: {
    marginLeft: 10,
    fontFamily: `'Montserrat', sans-serif`,
    fontSize: '1.1em',
    color: 'rgba(0, 0, 0, 0.87)',
    textTransform: 'uppercase',
    alignItems: 'center',
    display: 'flex',
  },
  btnDownload: {
    // zIndex: 999,
    // position: 'absolute',
    // bottom: '19%',
    // right: '2%',
    // background: '#2C97D0',
    background: '#C5005E',
    // borderRadius: '50%',
    // height: '40px',
    // width: '40px !important',
    display: 'flex',
    // minWidth: '40px',
    alignItems: 'center',
    padding: '6px 8px',
    color: 'var(--whiteMediumEmp)',
    // lineHeight: '55px',
    boxShadow: '0 3px 5px 0 rgba(0, 0, 0, 0.6)',
    '&:hover': {
      background: 'rgba(197, 0, 94, 0.8)',
    },
  },
  rootGodrejPage: {
    position: 'relative',
    '& h1': {
      fontFamily: 'Helvetica',
    },
    '& h2': {
      fontFamily: 'Helvetica',
    },
    '& h3': {
      fontFamily: 'Helvetica',
    },
    '& h4': {
      fontFamily: 'Helvetica',
    },
    '& p': {
      fontFamily: 'Arial, Helvetica, sans-serif',
    },
    '& span': {
      fontFamily: 'Arial, Helvetica, sans-serif',
    },
  },
  downloadIcon: {
    color: 'var(--whiteMediumEmp)',
    marginLeft: 5,
  },
  morePill: {
    fontSize: '0.7em',
    background: 'rgba(0, 0, 0, 0.3)',
    borderRadius: '25px',
    padding: '5px',
    color: 'var(--whiteMediumEmp)',
    margin: '0 0 0 5px',
  },
  subText: {
    fontSize: '15px',
    textTransform: 'capitalize',
    margin: '0 0 0 5px',
    background: 'rgba(0, 0, 0, 0.08)',
    padding: '5px 8px',
    borderRadius: 4,
    '&:last-child': {
      // marginLeft: 0,
    },
  },
});

@withCookies
@withRouter
@withStyles(styles)
class Godrej extends Component {
  state = {
    eventList: [],
    selectedMonths: [{ value: 0, label: 'All' }],
    selectedMonth: { value: 0, label: 'All' },
    selectedBusinesses: [{ value: 0, label: 'All' }],
    selectedCountries: [{ value: 0, label: 'All' }],
    selectedRiskLevels: [{ value: 0, label: 'All' }],
    openFilters: false,
    emergingRiskList: [],
  };

  async componentDidMount() {
    const {
      selectedMonth,
      selectedCountries,
      selectedBusinesses,
      selectedRiskLevels,
    } = this.state;
    const emgRiskFilObj = {
      countries: getArrFromObj(selectedCountries),
      businesses: getArrFromObj(selectedBusinesses),
      month: selectedMonth.value,
      risklevels: getArrFromObj(selectedRiskLevels),
    };
    // this.getEvents();
    // this.getEmergingRisks();
    await this.getMonths().then(data => this.setLabelValues('months', data));
    await this.getCountries().then(data =>
      this.setLabelValues('countries', data),
    );
    await this.getBizGroups().then(data =>
      this.setLabelValues('businesses', data),
    );
    await this.getRiskLevels().then(data =>
      this.setLabelValues('riskLevels', data),
    );
  }

  setLabelValues = (type, data) => {
    let values = [];
    switch (type) {
      case 'months':
        values = data.map(item => ({ label: item.month, value: item.id }));
        data.filter(item => {
          if (item.islive === true) {
            this.setState(
              {
                selectedMonth: { value: item.id, label: item.month },
              },
              () => {
                this.getEvents();
                this.getEmergingRisks();
              },
            );
          }
        });
        break;
      case 'countries':
        values = data.map(item => ({
          label: item.countryname,
          value: item.id,
        }));
        break;
      case 'businesses':
        values = data.map(item => ({
          label: item.businessName,
          value: item.id,
        }));
        break;
      case 'riskLevels':
        values = data.map(item => ({
          label: item.riskLevel,
          value: item.id,
        }));
        break;
      default:
        values = [];
        break;
    }
    this.setState({ [type]: values });
  };

  getEmergingRisks = async () => {
    const {
      selectedCountries,
      selectedBusinesses,
      selectedMonth,
      selectedRiskLevels,
    } = this.state;

    const emgRiskFilObj = {
      countries: getArrFromObj(selectedCountries),
      businesses: getArrFromObj(selectedBusinesses),
      month: selectedMonth.value,
      risklevels: getArrFromObj(selectedRiskLevels),
    };

    await getEmergingRisks(emgRiskFilObj).then(emergingRiskList => {
      this.setState({ emergingRiskList });
    });
  };

  getEvents = () => {
    const { cookies } = this.props;
    const {
      selectedBusinesses,
      selectedCountries,
      // selectedMonths,
      selectedMonth,
      selectedRiskLevels,
    } = this.state;
    const filters = {
      countries: getArrFromObj(selectedCountries),
      businesses: getArrFromObj(selectedBusinesses),
      // months: getArrFromObj(selectedMonths),
      month: selectedMonth.value,
      risklevels: getArrFromObj(selectedRiskLevels),
    };

    const reqObj = {
      url: API_ROUTES.filterGodrejEvents,
      isAuth: true,
      authToken: cookies.get('authToken'),
      method: 'POST',
      data: filters,
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        if (data.status === '200') {
          this.setState({ eventList: data.output, openFilters: false });
        }
      })
      .catch(e => console.log(e));
  };

  getMonths = () => {
    return new Promise(res => {
      const { cookies } = this.props;
      const reqObj = {
        url: API_ROUTES.getMonthsGodrej,
        isAuth: true,
        authToken: cookies.get('authToken'),
        method: 'POST',
        showToggle: true,
      };

      fetchApi(reqObj)
        .then(data => {
          if (data.status === '200') {
            res(data.output);
          }
        })
        .catch(e => console.log(e));
    });
  };

  getRiskLevels = () => {
    return new Promise(res => {
      const { cookies } = this.props;
      const reqObj = {
        url: API_ROUTES.getRiskLevelGodrej,
        isAuth: true,
        authToken: cookies.get('authToken'),
        method: 'POST',
        showToggle: true,
      };

      fetchApi(reqObj)
        .then(data => {
          if (data.status === '200') {
            res(data.output);
          }
        })
        .catch(e => console.log(e));
    });
  };

  getCountries = () => {
    return new Promise(res => {
      const { cookies } = this.props;
      const reqObj = {
        url: API_ROUTES.getCountriesGodrej,
        isAuth: true,
        authToken: cookies.get('authToken'),
        method: 'POST',
        showToggle: true,
      };

      fetchApi(reqObj)
        .then(data => {
          if (data.status === '200') {
            res(data.output);
          }
        })
        .catch(e => console.log(e));
    });
  };

  getBizGroups = () => {
    return new Promise(res => {
      const { cookies } = this.props;
      const reqObj = {
        url: API_ROUTES.getBusinessGroupsGodrej,
        isAuth: true,
        authToken: cookies.get('authToken'),
        method: 'POST',
        showToggle: true,
      };

      fetchApi(reqObj)
        .then(data => {
          if (data.status === '200') {
            res(data.output);
          }
        })
        .catch(e => console.log(e));
    });
  };

  handleSelect = (type, value) => this.setState({ [type]: value });

  handleLogout = () => {
    const { cookies } = this.props;
    store.dispatch(toggleIsFetch(true));
    setTimeout(() => {
      cookies.remove('authToken');
      cookies.remove('userRole');
      cookies.remove('authUserId');
      this.props.history.push('/');
      store.dispatch(toggleIsFetch(false));
    }, 2000);
  };

  toggleDrawer = show => {
    this.setState({ openFilters: show });
  };

  toggleModal = show => {
    this.setState({ showModal: show });
  };

  handleDownload = () => {
    const { cookies } = this.props;
    const {
      selectedBusinesses,
      selectedCountries,
      // selectedMonths,
      selectedMonth,
      selectedRiskLevels,
    } = this.state;
    const authToken = cookies.get('authToken');
    const filters = {
      countries: getArrFromObj(selectedCountries),
      businesses: getArrFromObj(selectedBusinesses),
      // months: getArrFromObj(selectedMonths),
      month: selectedMonth.value,
      risklevels: getArrFromObj(selectedRiskLevels),
    };
    const reqObj = {
      url: API_ROUTES.downloadGodrejEvents,
      isAuth: true,
      authToken,
      method: 'POST',
      isBlob: true,
      data: filters,
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(blobObj => {
        let a = document.createElement('a');
        document.body.appendChild(a);
        let objUrl = new Blob([blobObj], { type: 'application/pdf' });
        let url = window.URL.createObjectURL(objUrl);
        a.href = url;
        a.download = 'godrej-report-pdf';
        a.click();
        window.URL.revokeObjectURL(url);
      })
      .catch(e => console.log(e));
  };

  render() {
    const { classes, cookies } = this.props;
    const authUserId = parseInt(cookies.get('authUserId'));

    const {
      months,
      countries,
      businesses,
      riskLevels,
      // selectedMonths,
      selectedMonth,
      selectedCountries,
      selectedBusinesses,
      selectedRiskLevels,
      eventList,
      emergingRiskList,
    } = this.state;
    return (
      <div className={classes.rootGodrejPage}>
        <AppBar position="static" classes={{ root: classes.rootAppBar }}>
          <Toolbar classes={{ root: classes.rootToolbar }}>
            <div className={classes.logoWrapper}>
              {authUserId !== 185 && (
                <img
                  src={GodrejLogo}
                  alt="godrej-logo"
                  className={classes.godrejLogo}
                />
              )}
              <img
                src={MitkatLogo}
                alt="mitkat-logo"
                className={classes.mitkatLogo}
                style={authUserId === 185 ? { borderLeft: 'none' } : null}
              />
              <Typography
                component="h1"
                classes={{ root: classes.typographyRiskWatch }}
              >
                <span>Risk Watch (GILAC) - </span>
                {selectedBusinesses && selectedBusinesses.length > 0 && (
                  <span className={classes.subText}>
                    {selectedBusinesses[selectedBusinesses.length - 1].label}
                    {selectedBusinesses.length > 1 && (
                      <span className={classes.morePill}>
                        +{selectedBusinesses.length - 1} More
                      </span>
                    )}
                  </span>
                )}
                {selectedMonth.label && (
                  <span className={classes.subText}>{selectedMonth.label}</span>
                )}
              </Typography>
            </div>
            <Button
              classes={{ root: classes.btnGreen }}
              onClick={() => this.handleLogout()}
            >
              Logout
            </Button>
          </Toolbar>
        </AppBar>
        <div className={classes.rootContentWrapper}>
          <div className={classes.rootEventWrapper}>
            <GodrejEvents eventList={eventList} />
          </div>
          <div className={classes.rootMapWrappper}>
            {/* <AppBar position="static" classes={{ root: classes.rootAppBar }}>
              <Toolbar classes={{ root: classes.rootToolbar }}>
                <div className={classes.logoWrapper}>
                  <img
                    src={GodrejLogo}
                    alt="godrej-logo"
                    className={classes.godrejLogo}
                  />
                  <img
                    src={MitkatLogo}
                    alt="mitkat-logo"
                    className={classes.mitkatLogo}
                  />
                </div>
                <Button
                  classes={{ root: classes.btnGreen }}
                  onClick={() => this.handleLogout()}
                >
                  Logout
                </Button>
              </Toolbar>
            </AppBar> */}
            <div className={classes.filters}>
              {/* <Typography
                  component="h1"
                  classes={{ root: classes.typographyRiskWatch }}
                >
                Godrej Risk Watch
              </Typography>               */}
              <Button
                // classes={{ root: classes.btnGreen }}
                onClick={() => this.toggleDrawer(true)}
              >
                <Typography
                  component="h1"
                  classes={{ root: classes.rootTypographyFilter }}
                >
                  Filter By
                </Typography>
                <FilterListIcon />
              </Button>
              <Button
                classes={{ root: classes.btnDownload }}
                onClick={() => this.handleDownload()}
              >
                <Typography
                  component="h1"
                  classes={{ root: classes.rootTypographyDownload }}
                >
                  Download
                </Typography>
                <CloudDownloadIcon classes={{ root: classes.downloadIcon }} />
              </Button>
            </div>
            <div className={classes.emergingRiskWrap}>
              <EmergingRiskBox emergingRiskList={emergingRiskList} />
            </div>
            <div className={classes.mapWrap}>
              <GodrejMap eventList={eventList} />
            </div>
          </div>
          {/* <Button
              classes={{ root: classes.btnDownload }}
              onClick={() => this.handleDownload()}
          >            
            <CloudDownloadIcon
              classes={{ root: classes.downloadIcon }}
            />
          </Button> */}
        </div>
        <Drawer
          classes={{
            root: classes.rootDrawer,
            paper: classes.paper,
          }}
          anchor="right"
          open={this.state.openFilters}
          onClose={() => this.toggleDrawer(false)}
        >
          <div className={classes.arrowClose}>
            <Typography
              component="h1"
              classes={{ root: classes.rootTypography }}
            >
              <FilterListIcon classes={{ root: classes.drawerFilterIcon }} />
              Filter By
            </Typography>
            <IconButton
              color="inherit"
              aria-label="open drawer"
              edge="start"
              onClick={() => this.toggleDrawer(false)}
              className={classes.filterButton}
            >
              <ArrowForwardIcon />
            </IconButton>
          </div>
          <GodrejFilters
            months={months}
            countries={countries}
            businesses={businesses}
            riskLevels={riskLevels}
            // selectedMonths={selectedMonths}
            selectedMonth={selectedMonth}
            selectedBusinesses={selectedBusinesses}
            selectedCountries={selectedCountries}
            selectedRiskLevels={selectedRiskLevels}
            getEvents={this.getEvents}
            handleSelect={this.handleSelect}
            getEmergingRisks={this.getEmergingRisks}
          />
        </Drawer>
      </div>
    );
  }
}

export default Godrej;
