import React from 'react';
import './style.scss';

import { withCookies } from 'react-cookie';
import { withRouter } from 'react-router-dom';

const Session = (props) => {

    const handleLogout = () => {
        let { cookies, history } = props;
        cookies.remove('authToken');
        cookies.remove('userRole');
        setTimeout(() => {
            history.push('/');
        }, 2000);
    }

    return (
        <div id="sessionPage">
            <div className="session__popup">
                <h1 className="session__heading">Session Timeout</h1>
                <p className="session__title">Your session has expired due to inactivity</p>
                <button className="session__btn" onClick={handleLogout}>Login</button>
            </div>
        </div>
    )
}

export default withCookies(Session);