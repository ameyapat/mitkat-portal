import React, { Component } from 'react';
import { API_ROUTES } from '../../../helpers/http/apiRoutes';
import { fetchApi } from '../../../helpers/http/fetch';
import { withCookies } from 'react-cookie';
import MapApi from '../components/covidMap/MapApi';
import Filter from '../components/covidMap/Filter';
import Stats from '../components/covidMap/Stats';
import MStats from '../components/covidMap/MStats';
import MFilter from '../components/covidMap/MFilter';

@withCookies
class CovidMap extends Component{

    state = {
        clientlocation: true,
        statecaseinfo: false,
        inspectionsite: false,
        containmentzone: false,
        showHotSpot: true,
        showEmp: true,
        mapDetails: null,
        // polygons: [],
        statistics: {},
        empDetails: [],
    }

    componentDidMount(){
        this.getMapDetails();
        // this.getGeoJson();
        this.setHotSpot();
        this.setShowEmp();
        this.getEmpDetails();
    }

    getEmpDetails = () => {
        const { cookies } = this.props;
        const isAuth = cookies.get('authUserId');
        const reqObj = {
            url: API_ROUTES.getEmpDetails,
            isAuth: isAuth ? true : false,
            ...isAuth && { authToken: cookies.get("authToken") },
            method: "GET",
            showToggle: false,
        }
            
        fetchApi(reqObj)
        .then(empDetails => {
            this.setState({empDetails})
        })
    }

    setHotSpot = () => {
        const { cookies } = this.props;
        if(cookies.get('showHotSpot')){
            if(cookies.get('showHotSpot') === 'true'){
                this.setState({ showHotSpot: true })
            }else if(cookies.get('showHotSpot') === 'false'){
                this.setState({ showHotSpot: false })
            }
        }else{
            this.setCookie(true);
        }
    }

    setShowEmp = () => {
        const { cookies } = this.props;
        if(cookies.get('showEmp')){
            if(cookies.get('showEmp') === 'true'){
                this.setState({ showEmp: true })
            }else if(cookies.get('showEmp') === 'false'){
                this.setState({ showEmp: false })
            }
        }else{
            this.setShowEmpCookie(true);
        }
    }

    setCookie = (showHotSpot) => {
        const { cookies } = this.props;
        const d = new Date();
        d.setTime(d.getTime() + (24*60*60*1000)); 

        cookies.set('showHotSpot', showHotSpot, {
            path: '/',
            expires: d
        })
    }

    setShowEmpCookie = (showEmp) => {
        const { cookies } = this.props;
        const d = new Date();
        d.setTime(d.getTime() + (24*60*60*1000)); 

        cookies.set('showEmp', showEmp, {
            path: '/',
            expires: d
        })
    }

    getMapDetails(){
        const { cookies } = this.props;
        const {
            clientlocation,
            statecaseinfo,
            inspectionsite,
            containmentzone,
            statistics,
        } = this.state;

        const filters = {
            clientlocation,
            statecaseinfo,
            inspectionsite,
            containmentzone,
        };

        const reqObj = {
            url: API_ROUTES.getCoronaMapDetails,
            isAuth: true,
            authToken: cookies.get("authToken"),
            method: "POST",
            data: filters,
            showToggle: true,
        };

        fetchApi(reqObj)
        .then(mapDetails => {
            this.setState({ mapDetails });
            statistics['totalinfected'] = mapDetails.stateWiseCases[0].totalinfected;
            statistics['totaldeaths'] = mapDetails.stateWiseCases[0].totaldeaths;
            statistics['totalrecoverd'] = mapDetails.stateWiseCases[0].totalrecoverd;
            statistics['newcases'] = mapDetails.stateWiseCases[0].newcases;
            statistics['newdeaths'] = mapDetails.stateWiseCases[0].newdeaths;
            statistics['growthratecases'] = mapDetails.stateWiseCases[0].growthratecases;
            statistics['growthratedeaths'] = mapDetails.stateWiseCases[0].growthratedeaths;
            statistics['statename'] = mapDetails.stateWiseCases[0].statename;
            statistics['districts'] = mapDetails.stateWiseCases[0].districts;
            this.setState({
                statistics
            })
        })
        .catch(e => console.log(e));
    }

    applyFilters = () => {
        this.getMapDetails();
    }

    updateFilters = (type, value) => {
        this.setState({[type]: value}, () => {
            this.applyFilters();
        });
    }

    updateMFilters = (type, value) => {
        this.setState({[type]: value});
    }

    updateStats = (feedDetails) => {
        const { statistics } = this.state;
        statistics['totalinfected'] = feedDetails.totalinfected;
        statistics['totaldeaths'] = feedDetails.totaldeaths;
        statistics['totalrecoverd'] = feedDetails.totalrecoverd;
        statistics['newcases'] = feedDetails.newcases;
        statistics['newdeaths'] = feedDetails.newdeaths;
        statistics['growthratecases'] = feedDetails.growthratecases;
        statistics['growthratedeaths'] = feedDetails.growthratedeaths;
        statistics['statename'] = feedDetails.statename;
        statistics['districts'] = feedDetails.districts;
        this.setState({statistics});
    }

    // getGeoJson = () => {
        
    //     const reqObj = {
    //         url: API_ROUTES.geojson,
    //         isAuth: false,
    //         method: "GET",
    //         showToggle: true,
    //       };
          
    //       fetchApi(reqObj)
    //       .then(data => {
    //           this.setState({
    //               polygons: data.polygonmodel
    //           })
    //       })

    // }

    updateHotSpot = (showHotSpot) => this.setState({ showHotSpot }, () => this.setCookie(this.state.showHotSpot));

    updateShowEmp = (showEmp) => this.setState({ showEmp }, () => this.setShowEmpCookie(this.state.showEmp));

    render(){
        const { cookies } = this.props;
        const { mapDetails, statistics, showHotSpot, empDetails, showEmp } = this.state;
        const isMobile = cookies.get('isMobile');
        
        return(
            <div id="covid-19-map-cont">
                {
                    (isMobile === 'true')
                    ?
                    <MFilter 
                        filters={this.state} 
                        updateFilters={this.updateMFilters}
                        applyFilters={this.applyFilters}
                        showHotSpot={showHotSpot}
                        showEmp={showEmp}
                        updateHotSpot={this.updateHotSpot}
                        updateShowEmp={this.updateShowEmp}
                    />
                    :
                    <Filter 
                        filters={this.state} 
                        updateFilters={this.updateFilters}
                        showHotSpot={showHotSpot}
                        showEmp={showEmp}
                        updateHotSpot={this.updateHotSpot}
                        updateShowEmp={this.updateShowEmp}
                    />
                }
                {
                    // (isMobile === 'true')
                    // ?
                    // <MStats 
                    //     statistics={statistics}
                    // />
                    // :
                    // <Stats statistics={statistics} />
                }
                {
                    // mapDetails && polygons &&
                    mapDetails &&
                    <MapApi 
                        feed={mapDetails} 
                        updateStats={this.updateStats}
                        // polygons={polygons}
                        showHotSpot={showHotSpot}
                        showEmp={showEmp}
                        empDetails={empDetails}
                    />
                }
            </div>
        )
    }
}

export default CovidMap;