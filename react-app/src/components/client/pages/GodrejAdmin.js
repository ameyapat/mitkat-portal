import React, { Component } from "react";
import { withStyles, Button } from "@material-ui/core";
import GodrejLogo from "../../../assets/godrej-logo.png";
import MitkatLogo from "../../../assets/mitkat-logo-filled.png";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import CustomModal from '../../ui/modal';
import { searchGodrejEvents } from "../../../helpers/utils";
import { withCookies } from "react-cookie";
import { fetchApi } from "../../../helpers/http/fetch";
import { API_ROUTES } from "../../../helpers/http/apiRoutes";
import TableListRow from "../../ui/tablelist/TableListRow";
import TableListCell from "../../ui/tablelist/TableListCell";
import TableList from '../../ui/tablelist';
import SearchBox from "../../ui/searchBox";
import store from "../../../store";
import { toggleToast } from "../../../actions";
import EditRiskWatch from '../../admin/components/createRiskWatch/EditRiskWatch';
import CustomStepper from "../../ui/stepper";
import { toggleIsFetch } from "../../../actions";
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

const styles = () => ({
    rootGodrejPage: {
        position: 'relative',
        '& h1':{
            fontFamily: 'Helvetica',
        },
        '& h2':{
            fontFamily: 'Helvetica',
        },
        '& h3':{
            fontFamily: 'Helvetica',
        },
        '& h4':{
            fontFamily: 'Helvetica',
        },
        '& p':{
            fontFamily: 'Arial, Helvetica, sans-serif',
        },
        '& span':{
            fontFamily: 'Arial, Helvetica, sans-serif',
        }
    },
  rootRiskWatch: {
    background: "#F0F0F1",
    padding: 20
  },
  innerRiskWatch: {
    '& .dmd__wrap':{
      margin: 0,
    }
  },
  rootThead: {
      fontWeight: 600,
  },
  tRowWrap: {
    borderTop: '1px solid #f1f1f1',
  },
  rootTRowWrap: {
    padding: '15px 0',
    borderBottom: 0,
    backgroundColor: '#f1f1f1',
  },
  rowWrap: {
    '&:nth-child(even)':{
      backgroundColor: '#f5f5f5',
    }
  },
  closeTimes: {
    position: 'absolute',
    color: '#000',
    fontSize: 30,
    right: 10,
    top: 10,
    cursor: 'pointer',
  },
  rootAppBar: {
    backgroundColor: "#fff"
  },
  rootToolbar: {
    display: "flex",
    justifyContent: "space-between"
  },
  logoWrapper: {
    display: 'flex',
    alignItems: 'center',
  },
  godrejLogo: {
    width: "90px"
  },
  mitkatLogo: {
    width: "128px",
    maxWidth: '100%',
    borderLeft: '1px solid #d3d3d3',
    paddingLeft: '15px',
    marginLeft: '15px',
  },
  btnGreen: {
    backgroundColor: "#53ab3e",
    color: "#fff",
    "&:hover": {
      backgroundColor: "rgba(83, 171, 62, 0.8)"
    }
  },
});

@withCookies
@withStyles(styles)
class GodrejAdmin extends Component {
  state = {
    riskWatchList: [],
    eventDetails: {},
    defaultList: [],
    showEventModal: false,
    showImageModal: false,
    eventId: null,
  };

  componentDidMount() {
    let { cookies } = this.props;
    let authToken = cookies.get("authToken");
    this.getRiskWatch(authToken)
    .then(data => {
        // if(data.status === '200'){
        //     this.setState({ 
        //         riskWatchList: data.output,
        //          defaultList: data.output 
        //     });
        // }
    })
    .catch(e => console.log(e))
  }

  getRiskWatch = (authToken) => {
    return new Promise((res, rej) => {
        const reqObj = {
            url: API_ROUTES.godrejEventListAdmin,
            method: 'POST',
            isAuth: true,
            authToken,
            showToggle: true,
        }

        fetchApi(reqObj)
        .then(data => { 
          if(data.status === '200'){
            this.setState({ 
                riskWatchList: data.output,
                defaultList: data.output 
            });
          }
            res(data)
         })
        .catch(e => rej(e));
    })
  }

  renderEventList = () => {
    const { classes } = this.props;
    let { riskWatchList } = this.state;
    return riskWatchList.map(event => {
      return (
        <TableListRow
          key={event.eventid}
          classes={{ root: classes.rowWrap }}
        >
          <TableListCell value={event.month} />
          <TableListCell value={event.title} />
          <TableListCell value={event.businessGroup} />
          <TableListCell>
              <span className={`approve--status ${event.live ? 'approved' : 'rejected'}`}></span>
              <p>{event.live ? 'Yes' : 'No'}</p>
          </TableListCell>
          <TableListCell value={event.country} />
          <TableListCell value={event.pestelCategory} />
          <TableListCell>
              <Button
                variant='contained'
                color='secondary'
                onClick={() => this.handleEditEvent(event.eventid)}
              >
                  Edit
              </Button>
              <Button
                variant='outlined'
                color='primary'
                style={{ padding: 10, marginLeft: 8 }}
                // onClick={() => this.handleModal(true, 'showImageModal', event.eventid)}
                onClick={() => this.handleUploadImages(event.eventid)}
              >
                  <i className="fas fa-images"></i>
              </Button>
          </TableListCell>
        </TableListRow>
      );
    });
  };

  handleUploadImages = (eventId) => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    //use fetch here
    let reqObj = {
        url: `${API_ROUTES.viewGodrejEventAdmin}/${eventId}`,
        isAuth: true,
        authToken,
        method: 'POST',
        showToggle: true,
    }

    fetchApi(reqObj)
    .then(data => {
        this.setState({eventDetails: data.output}, () => {
            this.handleModal(true, 'showImageModal', eventId);
        });
    })
    .catch(e => {
        let toastObj = {
            toastMsg: 'Error fetching details',
            showToast: true
        }
        store.dispatch(toggleToast(toastObj))
    })
  }

  handleModal = (show, type, id) => this.setState({[type]: show, eventId: id});

  handleEditEvent = (id) => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    //use fetch here
    let reqObj = {
        url: `${API_ROUTES.viewGodrejEventAdmin}/${id}`,
        isAuth: true,
        authToken,
        method: 'POST',
        showToggle: true,
    }

    fetchApi(reqObj)
    .then(data => {
        this.setState({eventDetails: data.output}, () => {
            this.handleModal(true, 'showEventModal');
        });
    })
    .catch(e => {
        let toastObj = {
            toastMsg: 'Error fetching details',
            showToast: true
        }
        store.dispatch(toggleToast(toastObj))
    })
}

  submitTracker = () => {
    let { cookies } = this.props;
    let {
      attachLogo,
      attachSignature,
      selectedProduct,
      selectedTrackers
    } = this.state;
    let authToken = cookies.get("authToken");
    let reqData = {
      logo: attachLogo,
      signature: attachSignature,
      clientid: selectedProduct,
      eventid: selectedTrackers
    };
    let reqObj = {
      url: API_ROUTES.dispatchMail,
      method: "POST",
      isAuth: true,
      authToken,
      data: reqData,
      showToggle: true,
    };
    fetchApi(reqObj)
      .then(data => {
        let toastObj = {
          toastMsg: `Total failed emails: ${data.totalFailedEmails}`,
          showToast: true
        };
        store.dispatch(toggleToast(toastObj));
      })
      .catch(e => console.log(e));
  };

  handleSearch = e => {
    let value = e.target.value;
    let { riskWatchList, defaultList } = this.state;
    riskWatchList = searchGodrejEvents(value, riskWatchList);
    if (riskWatchList.length > 0 && value) {
      this.setState({ riskWatchList });
    } else {
      this.setState({ riskWatchList: defaultList });
    }
  };

  handleLogout = () => {
    const { cookies } = this.props;
    store.dispatch(toggleIsFetch(true));
    setTimeout(() => {
      cookies.remove("authToken");
      cookies.remove("userRole");
      cookies.remove("authUserId");
      this.props.history.push('/');
      store.dispatch(toggleIsFetch(false));
    }, 2000);
  };

  render() {
    const { classes } = this.props;
    const { riskWatchList, eventDetails, eventId } = this.state;
    
    return (
        <>
        <div className={classes.rootGodrejPage}>
          <AppBar position="static" classes={{ root: classes.rootAppBar }}>
            <Toolbar classes={{ root: classes.rootToolbar }}>
              <div className={classes.logoWrapper}>
                <img
                  src={GodrejLogo}
                  alt="godrej-logo"
                  className={classes.godrejLogo}
                />
                <img
                  src={MitkatLogo}
                  alt="mitkat-logo"
                  className={classes.mitkatLogo}
                />
              </div>
              <Button
                classes={{ root: classes.btnGreen }}
                onClick={() => this.handleLogout()}
              >
                Logout
              </Button>
            </Toolbar>
          </AppBar>
      </div>
      <div className={classes.rootRiskWatch}>
        <div className={classes.innerRiskWatch}>
          <div className="dmd__wrap">
            <div className="product__header">
              <div className="product__title">
                <h3>Risk Watch</h3>
              </div>
              {riskWatchList.length > 0 && (
                <div className="product__search">
                  <SearchBox
                    classes="fa fa-search"
                    placeholder="Search by keyword"
                    parentHandler={this.handleSearch}
                  />
                </div>
              )}
            </div>
            <div className={classes.tRowWrap}>
              <TableListRow classes={{ root: classes.rootTRowWrap }}>
                <TableListCell value={"Month"} classes={{ root: classes.rootThead }} />
                <TableListCell value={"Title"} classes={{ root: classes.rootThead }} />
                <TableListCell value={"Business Group"} classes={{ root: classes.rootThead }} />
                <TableListCell value={"Is Live"} classes={{ root: classes.rootThead }} />
                <TableListCell value={"Country"} classes={{ root: classes.rootThead }} />
                <TableListCell value={"Category"} classes={{ root: classes.rootThead }} />
                <TableListCell value={"Actions"} classes={{ root: classes.rootThead }} />
              </TableListRow>
              <TableList>{this.renderEventList()}</TableList>
            </div>
          </div>
        </div>
        <CustomModal 
            showModal={this.state.showEventModal} 
            closeModal={() => this.handleModal(false, 'showEventModal')}
        >
            <div id="editRiskWatch" className="modal__inner">
            { eventDetails && <EditRiskWatch 
              eventDetails={eventDetails} 
              handleModal={this.handleModal} 
              getRiskWatch={this.getRiskWatch}
              refreshEvents1={true}
              /> }
            </div>
        </CustomModal>
        <CustomModal 
            showModal={this.state.showImageModal} 
            closeModal={() => this.handleModal(false, 'showImageModal')}
            // showCloseOption
        >
          <div id="imageRiskWatch" className="modal__inner">
              <span
                className={classes.closeTimes}
                onClick={() => this.handleModal(false, 'showImageModal')}
              >
                  &times;
              </span>
              <CustomStepper 
                eventId={eventId} 
                eventDetails={eventDetails}
                getRiskWatch={this.getRiskWatch}
              />
          </div>
        </CustomModal>
      </div>
      </>
    );
  }
}

const mapStateToProps = (state) => ({ uploadImage: state.uploadImage });
const mapDispatchToProps = (dispatch) => ({ dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(GodrejAdmin));

