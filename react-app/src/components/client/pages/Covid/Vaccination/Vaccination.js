import React, { Component, Fragment } from 'react';
import './Vaccination.scss';
import {
  getCoronaCountryDashboard,
  getCovidCountries,
  getEditableCountryDetails,
} from '../../../../admin/helpers/utils';
import VaccinationMap from '../../../components/VaccinationMap';
import ViewSwitch from '../../../../client/components/ViewSwitch/ViewSwitch';

class Vaccination extends Component {
  state = {
    countries: [],
    coronaDetails: [],
    coronaDash: [],
    countryId: '',
  };

  componentDidMount() {
    getCovidCountries(false).then(countries => {
      this.setState({ countries, countryId: countries[0].id }, () => {
        this.initCountryId();
      });
    });
  }

  initCountryId = () => {
    const { countryId } = this.state;
    getCoronaCountryDashboard(false, countryId).then(coronaDash => {
      this.setState({ coronaDash });
    });
    getEditableCountryDetails(false, countryId).then(coronaDetails => {
      this.setState({ coronaDetails });
    });
  };

  setCountryId = countryId => this.setState({ countryId });

  render() {
    const { countryId } = this.state;
    return (
      <div>
        <ViewSwitch countryId={countryId} />
        <VaccinationMap setCountryId={this.setCountryId} />
      </div>
    );
  }
}

export default Vaccination;
