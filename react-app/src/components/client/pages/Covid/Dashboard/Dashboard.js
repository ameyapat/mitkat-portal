import React, { Component } from 'react';
import './Dashboard.scss';
import Footer from '../../../components/covid/Footer/Footer';
import Timeline from '../../../components/covid/Timeline/Timeline';
import Disclaimer from '../../../components/covid/Disclaimer/Disclaimer';
import Header from '../../../components/covid/Header/Header';

import { API_ROUTES } from '../../../../../helpers/http/apiRoutes';
import { fetchApi } from '../../../../../helpers/http/fetch';
import { withCookies } from 'react-cookie';
import { withRouter } from 'react-router-dom';
import store from '../../../../../store';
import { toggleIsFetch, toggleToast } from '../../../../../actions';
import Banner from '../../../components/covid/Banner/Banner';
import MapButton from '../../../components/covid/MapButton/MapButton';
import SubBanner from '../../../components/covid/SubBanner/SubBanner';
import Statewise from '../../../components/covid/Statewise/Statewise';
import Metrowise from '../../../components/covid/Metrowise/Metrowise';

import { sortBy } from '../../../../../helpers/utils';
import CovidSlider from '../../../components/covid/CovidSlider/CovidSlider';
import { Line, Bar } from 'react-chartjs-2';

import { connect } from 'react-redux';
import { setIsLoaded, setIsCovidDashAuth } from '../../../../../actions';

import { authWebUser } from '../../../../../requestor/mitkatWeb/requestor';
import CustomModal from '../../../../ui/modal';
import LoginBox from '../../../../login/LoginBox';
import style from './style';
import injectSheet from 'react-jss';

const disclaimerTitle = `Compiled from State Govt. numbers`;
const disclaimerSubtitle = `District zones as published by MoHFW`;

@withCookies
@withRouter
@connect(
  state => {
    return {
      appState: state.appState,
    };
  },
  {
    setIsLoaded,
  },
)
@injectSheet(style)
class Dashboard extends Component {
  state = {
    advisories: null,
    coronaDashboard: null,
    eventDetails: null,
    cityDetails: null,
    riskGraphObj: {
      labels: [],
      datasets: [],
    },
    deathGraphObj: {
      labels: [],
      datasets: [],
    },
    growthGraphObj: {
      labels: [],
      datasets: [],
    },
    riskGraphObjCity: {
      labels: [],
      datasets: [],
    },
    deathGraphObjCity: {
      labels: [],
      datasets: [],
    },
    growthGraphObjCity: {
      labels: [],
      datasets: [],
    },
    riskGraphObjCountry: {
      labels: [],
      datasets: [],
    },
    deathGraphObjCountry: {
      labels: [],
      datasets: [],
    },
    growthGraphObjCountry: {
      labels: [],
      datasets: [],
    },
  };

  componentDidMount() {
    this.checkIsCovidDashAuth();
    this.props.setIsLoaded(false);
    this.handleGetAdvisories();
    this.getCoronaDashboard()
      .then(coronaDashboard => {
        this.setState({ coronaDashboard }, () => {
          this.getStateWiseReport(coronaDashboard.defaultstateid);
          this.getCityWiseReport(1);
          this.parseCaseGraphCountry();
          this.parseDeathGraphCountry();
          this.parseGrowthGraphCountry();
          this.props.setIsLoaded(true);
        });
      })
      .catch(e => console.log('errr!!!'));
  }

  checkIsCovidDashAuth = () => {
    const { cookies } = this.props;
    const isCovidDashAuth = cookies.get('isCovidDashAuth');
    if (isCovidDashAuth) {
      store.dispatch(setIsCovidDashAuth(JSON.parse(isCovidDashAuth)));
    }
  };

  getUserAuth = reqData => {
    const newReqObj = {
      username: reqData.usernameOrEmail,
      password: reqData.password,
    };
    authWebUser(newReqObj).then(data => {
      const toastObj = {
        toastMsg: data.message,
        showToast: true,
      };
      store.dispatch(toggleToast(toastObj));
      store.dispatch(setIsCovidDashAuth(JSON.parse(data.success)));
    });
  };

  getCoronaDashboard = () => {
    return new Promise((resolve, reject) => {
      const { cookies } = this.props;
      const reqObj = {
        url: API_ROUTES.coronaDashboard,
        isAuth: true,
        authToken: cookies.get('authToken'),
        method: 'GET',
        showToggle: true,
      };

      fetchApi(reqObj)
        .then(data => {
          resolve(data);
        })
        .catch(e => reject(e));
    });
  };

  handleLogout = () => {
    const { cookies } = this.props;
    store.dispatch(toggleIsFetch(true));
    setTimeout(() => {
      cookies.remove('authToken');
      cookies.remove('userRole');
      cookies.remove('authUserId');
      this.props.history.push('/');
      store.dispatch(toggleIsFetch(false));
    }, 2000);
  };

  handleGetAdvisories = () => {
    const { cookies } = this.props;
    const reqObj = {
      url: API_ROUTES.getCoronaAdvisory,
      isAuth: false,
      authToken: cookies.get('authToken'),
      method: 'GET',
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(advisories => {
        this.setState({ advisories });
      })
      .catch(e => console.log(e));
  };

  handleSort = (type, sort) => {
    const { stateTable } = this.state.coronaDashboard;
    let sortedStateTable = sortBy(stateTable, type, sort);
    let coronaDashboardUpdated = this.state.coronaDashboard;
    coronaDashboardUpdated['stateTable'] = sortedStateTable;
    this.setState({ coronaDashboard: coronaDashboardUpdated });
  };

  handleSortCity = (type, sort) => {
    const { cityTable } = this.state.coronaDashboard;
    let sortedCityTable = sortBy(cityTable, type, sort);
    let coronaDashboardUpdated = this.state.coronaDashboard;
    coronaDashboardUpdated['cityTable'] = sortedCityTable;
    this.setState({ coronaDashboard: coronaDashboardUpdated });
  };

  getStateWiseReport = id => {
    try {
      const { cookies } = this.props;
      const reqObj = {
        url: `${API_ROUTES.stateWiseDashboard}/${id}`,
        isAuth: true,
        authToken: cookies.get('authToken'),
        method: 'GET',
        showToggle: true,
      };

      fetchApi(reqObj)
        .then(data => {
          this.setState({ eventDetails: data }, () => {
            this.parseCaseGraph();
            this.parseGrowthGraph();
            this.parseDeathGraph();
          });
        })
        .catch(e => console.log(e));
    } catch (e) {
      console.log(e);
    }
  };

  getCityWiseReport = id => {
    try {
      const { cookies } = this.props;
      const reqObj = {
        url: `${API_ROUTES.cityWiseDashboard}/${id}`,
        isAuth: true,
        authToken: cookies.get('authToken'),
        method: 'GET',
        showToggle: true,
      };

      fetchApi(reqObj)
        .then(data => {
          this.setState({ cityDetails: data }, () => {
            this.parseCaseGraphCity();
            this.parseGrowthGraphCity();
            this.parseDeathGraphCity();
          });
        })
        .catch(e => console.log(e));
    } catch (e) {
      console.log(e);
    }
  };

  parseCaseGraph = () => {
    const { casegraph } = this.state.eventDetails;
    const { riskGraphObj } = this.state;
    const labels = [];
    const dsObjOne = {
      label: 'Total Cases',
      data: [],
      backgroundColor: 'rgba(224, 16, 89, 0)',
      borderColor: 'rgba(224, 16, 89, 1)',
      borderWidth: 1.75,
      pointBackgroundColor: 'rgba(255,255,255,0)',
      pointBorderColor: 'rgba(255,255,255,0)',
      order: 2,
    };
    const dsObjTwo = {
      label: 'Active Cases',
      data: [],
      backgroundColor: 'rgba(0, 147, 213, 0)',
      borderColor: 'rgba(0, 147, 213, 1)',
      borderWidth: 1.75,
      pointBackgroundColor: 'rgba(255,255,255,0)',
      pointBorderColor: 'rgba(255,255,255,0)',
      order: 1,
    };

    casegraph.map(i => {
      labels.push(i.x_point);
      dsObjOne.data.push(i.y_point);
      dsObjTwo.data.push(i.y_point_new);
    });

    riskGraphObj['labels'] = labels;
    riskGraphObj.datasets = [{ ...dsObjOne }, { ...dsObjTwo }];
    this.setState({ riskGraphObj });
  };

  parseCaseGraphCity = () => {
    const { casegraph } = this.state.cityDetails;
    const { riskGraphObjCity } = this.state;
    const labels = [];
    const dsObjOne = {
      label: 'Total Cases',
      data: [],
      backgroundColor: 'rgba(224, 16, 89, 0)',
      borderColor: 'rgba(224, 16, 89, 1)',
      borderWidth: 1.75,
      pointBackgroundColor: 'rgba(255,255,255,0)',
      pointBorderColor: 'rgba(255,255,255,0)',
      order: 2,
    };
    const dsObjTwo = {
      label: 'Active Cases',
      data: [],
      backgroundColor: 'rgba(0, 147, 213, 0)',
      borderColor: 'rgba(0, 147, 213, 1)',
      borderWidth: 1.75,
      pointBackgroundColor: 'rgba(255,255,255,0)',
      pointBorderColor: 'rgba(255,255,255,0)',
      order: 1,
    };

    casegraph.map(i => {
      labels.push(i.x_point);
      dsObjOne.data.push(i.y_point);
      dsObjTwo.data.push(i.y_point_new);
    });

    riskGraphObjCity['labels'] = labels;
    riskGraphObjCity.datasets = [{ ...dsObjOne }, { ...dsObjTwo }];
    this.setState({ riskGraphObjCity });
  };

  parseCaseGraphCountry = () => {
    const { casegraph } = this.state.coronaDashboard;
    const { riskGraphObjCountry } = this.state;
    const labels = [];
    const dsObjOne = {
      label: 'Total Cases',
      data: [],
      backgroundColor: 'rgba(224, 16, 89, 0)',
      borderColor: 'rgba(224, 16, 89, 1)',
      borderWidth: 1.75,
      pointBackgroundColor: 'rgba(255,255,255,0)',
      pointBorderColor: 'rgba(255,255,255,0)',
      order: 2,
    };
    const dsObjTwo = {
      label: 'Active Cases',
      data: [],
      backgroundColor: 'rgba(0, 147, 213, 0)',
      borderColor: 'rgba(0, 147, 213, 1)',
      borderWidth: 1.75,
      pointBackgroundColor: 'rgba(255,255,255,0)',
      pointBorderColor: 'rgba(255,255,255,0)',
      order: 1,
    };

    casegraph.map(i => {
      labels.push(i.x_point);
      dsObjOne.data.push(i.y_point);
      dsObjTwo.data.push(i.y_point_new);
    });

    riskGraphObjCountry['labels'] = labels;
    riskGraphObjCountry.datasets = [{ ...dsObjOne }, { ...dsObjTwo }];
    this.setState({ riskGraphObjCountry });
  };

  parseGrowthGraph = () => {
    const { growthgraph } = this.state.eventDetails;
    const { growthGraphObj } = this.state;
    const labels = [];
    const dsObjOne = {
      label: 'Additional Cases',
      data: [],
      backgroundColor: 'rgba(224, 16, 89, 0)',
      borderColor: 'rgba(224, 16, 89, 1)',
      borderWidth: 1.75,
      pointBackgroundColor: 'rgba(255,255,255,0)',
      pointBorderColor: 'rgba(255,255,255,0)',
    };
    const dsObjTwo = {
      label: 'Additional Deaths',
      data: [],
      backgroundColor: 'rgba(66, 75, 84, 0)',
      borderColor: 'rgba(66, 75, 84, 1)',
      borderWidth: 1.75,
      pointBackgroundColor: 'rgba(255,255,255,0)',
      pointBorderColor: 'rgba(255,255,255,0)',
    };

    growthgraph.map(i => {
      labels.push(i.x_point);
      dsObjOne.data.push(i.y_point);
      dsObjTwo.data.push(i.y_point_new);
    });

    growthGraphObj['labels'] = labels;
    growthGraphObj.datasets = [{ ...dsObjOne }, { ...dsObjTwo }];
    this.setState({ growthGraphObj });
  };

  parseGrowthGraphCity = () => {
    const { growthgraph } = this.state.cityDetails;
    const { growthGraphObjCity } = this.state;
    const labels = [];
    const dsObjOne = {
      label: 'Additional Cases',
      data: [],
      backgroundColor: 'rgba(224, 16, 89, 0)',
      borderColor: 'rgba(224, 16, 89, 1)',
      borderWidth: 1.75,
      pointBackgroundColor: 'rgba(255,255,255,0)',
      pointBorderColor: 'rgba(255,255,255,0)',
    };
    const dsObjTwo = {
      label: 'Additional Deaths',
      data: [],
      backgroundColor: 'rgba(66, 75, 84, 0)',
      borderColor: 'rgba(66, 75, 84, 1)',
      borderWidth: 1.75,
      pointBackgroundColor: 'rgba(255,255,255,0)',
      pointBorderColor: 'rgba(255,255,255,0)',
    };

    growthgraph.map(i => {
      labels.push(i.x_point);
      dsObjOne.data.push(i.y_point);
      dsObjTwo.data.push(i.y_point_new);
    });

    growthGraphObjCity['labels'] = labels;
    growthGraphObjCity.datasets = [{ ...dsObjOne }, { ...dsObjTwo }];
    this.setState({ growthGraphObjCity });
  };

  parseGrowthGraphCountry = () => {
    const { growthgraph } = this.state.coronaDashboard;
    const { growthGraphObjCountry } = this.state;
    const labels = [];
    const dsObjOne = {
      label: 'Additional Cases',
      data: [],
      backgroundColor: 'rgba(224, 16, 89, 0)',
      borderColor: 'rgba(224, 16, 89, 1)',
      borderWidth: 1.75,
      pointBackgroundColor: 'rgba(255,255,255,0)',
      pointBorderColor: 'rgba(255,255,255,0)',
    };
    const dsObjTwo = {
      label: 'Additional Deaths',
      data: [],
      backgroundColor: 'rgba(66, 75, 84, 0)',
      borderColor: 'rgba(66, 75, 84, 1)',
      borderWidth: 1.75,
      pointBackgroundColor: 'rgba(255,255,255,0)',
      pointBorderColor: 'rgba(255,255,255,0)',
    };

    growthgraph.map(i => {
      labels.push(i.x_point);
      dsObjOne.data.push(i.y_point);
      dsObjTwo.data.push(i.y_point_new);
    });

    growthGraphObjCountry['labels'] = labels;
    growthGraphObjCountry.datasets = [{ ...dsObjOne }, { ...dsObjTwo }];
    this.setState({ growthGraphObjCountry });
  };

  parseDeathGraph = () => {
    const { deathgraph } = this.state.eventDetails;
    const { deathGraphObj } = this.state;
    const labels = [];
    const dsObjOne = {
      label: 'Deaths',
      data: [],
      backgroundColor: 'rgba(186, 39, 37, 0)',
      borderColor: 'rgba(186, 39, 37, 1)',
      borderWidth: 1.75,
      pointBackgroundColor: 'rgba(255,255,255,0)',
      pointBorderColor: 'rgba(255,255,255,0)',
      yAxisID: 'A',
    };
    const dsObjTwo = {
      label: 'Deaths Growth rate',
      data: [],
      backgroundColor: 'rgba(66, 75, 84, 0)',
      borderColor: 'rgba(66, 75, 84, 1)',
      borderWidth: 1.75,
      pointBackgroundColor: 'rgba(255,255,255,0)',
      pointBorderColor: 'rgba(255,255,255,0)',
      yAxisID: 'B',
    };

    deathgraph.map(i => {
      labels.push(i.x_point);
      dsObjOne.data.push(i.y_point);
      dsObjTwo.data.push(i.y_point_new);
    });

    deathGraphObj['labels'] = labels;
    deathGraphObj.datasets = [{ ...dsObjOne }, { ...dsObjTwo }];
    this.setState({ deathGraphObj });
  };

  parseDeathGraphCity = () => {
    const { deathgraph } = this.state.cityDetails;
    const { deathGraphObjCity } = this.state;
    const labels = [];
    const dsObjOne = {
      label: 'Deaths',
      data: [],
      backgroundColor: 'rgba(186, 39, 37, 0)',
      borderColor: 'rgba(186, 39, 37, 1)',
      borderWidth: 1.75,
      pointBackgroundColor: 'rgba(255,255,255,0)',
      pointBorderColor: 'rgba(255,255,255,0)',
      yAxisID: 'A',
    };
    const dsObjTwo = {
      label: 'Deaths Growth rate',
      data: [],
      backgroundColor: 'rgba(66, 75, 84, 0)',
      borderColor: 'rgba(66, 75, 84, 1)',
      borderWidth: 1.75,
      pointBackgroundColor: 'rgba(255,255,255,0)',
      pointBorderColor: 'rgba(255,255,255,0)',
      yAxisID: 'B',
    };

    deathgraph.map(i => {
      labels.push(i.x_point);
      dsObjOne.data.push(i.y_point);
      dsObjTwo.data.push(i.y_point_new);
    });

    deathGraphObjCity['labels'] = labels;
    deathGraphObjCity.datasets = [{ ...dsObjOne }, { ...dsObjTwo }];
    this.setState({ deathGraphObjCity });
  };

  parseDeathGraphCountry = () => {
    const { deathgraph } = this.state.coronaDashboard;
    const { deathGraphObjCountry } = this.state;
    const labels = [];
    const dsObjOne = {
      label: 'Deaths',
      data: [],
      backgroundColor: 'rgba(186, 39, 37, 0)',
      borderColor: 'rgba(186, 39, 37, 1)',
      borderWidth: 1.75,
      pointBackgroundColor: 'rgba(255,255,255,0)',
      pointBorderColor: 'rgba(255,255,255,0)',
      yAxisID: 'A',
    };
    const dsObjTwo = {
      label: 'Deaths Growth rate',
      data: [],
      backgroundColor: 'rgba(66, 75, 84, 0)',
      borderColor: 'rgba(66, 75, 84, 1)',
      borderWidth: 1.75,
      pointBackgroundColor: 'rgba(255,255,255,0)',
      pointBorderColor: 'rgba(255,255,255,0)',
      yAxisID: 'B',
    };

    deathgraph.map(i => {
      labels.push(i.x_point);
      dsObjOne.data.push(i.y_point);
      dsObjTwo.data.push(i.y_point_new);
    });

    deathGraphObjCountry['labels'] = labels;
    deathGraphObjCountry.datasets = [{ ...dsObjOne }, { ...dsObjTwo }];
    this.setState({ deathGraphObjCountry });
  };

  render() {
    const {
      appState: { isCovidDashAuth },
      classes,
    } = this.props;
    const {
      advisories,
      coronaDashboard,
      eventDetails,
      cityDetails,
      riskGraphObj,
      deathGraphObj,
      growthGraphObj,
      riskGraphObjCity,
      growthGraphObjCity,
      deathGraphObjCity,
      riskGraphObjCountry,
      growthGraphObjCountry,
      deathGraphObjCountry,
    } = this.state;

    const isLoaded = this.props.appState && this.props.appState.isLoaded;

    return isLoaded ? (
      <div id="covidDashboard">
        {false && <MapButton />}
        <Header handleLogout={this.handleLogout} />
        {coronaDashboard && <Banner coronaDashboard={coronaDashboard} />}
        {coronaDashboard && <SubBanner coronaDashboard={coronaDashboard} />}
        {coronaDashboard && (
          <CovidSlider>
            <div className="covid-slide">
              <div className="covid-slide-inner">
                <Line
                  data={riskGraphObjCountry}
                  height={190}
                  options={{
                    legend: {
                      labels: {
                        fontColor: '#bdbdbd',
                      },
                    },
                    scales: {
                      xAxes: [
                        {
                          gridLines: {
                            display: true,
                          },
                          ticks: {
                            fontColor: '#bdbdbd',
                          },
                        },
                      ],
                      yAxes: [
                        {
                          gridLines: {
                            display: true,
                          },
                          ticks: {
                            fontColor: '#bdbdbd',
                          },
                        },
                      ],
                    },
                  }}
                />
              </div>
            </div>
            <div className="covid-slide">
              <div className="covid-slide-inner">
                <Bar
                  data={growthGraphObjCountry}
                  height={190}
                  options={{
                    legend: {
                      labels: {
                        fontColor: '#bdbdbd',
                      },
                    },
                    scales: {
                      xAxes: [
                        {
                          gridLines: {
                            display: true,
                          },
                          ticks: {
                            fontColor: '#bdbdbd',
                          },
                        },
                      ],
                      yAxes: [
                        {
                          gridLines: {
                            display: true,
                          },
                          ticks: {
                            fontColor: '#bdbdbd',
                          },
                        },
                      ],
                    },
                  }}
                />
              </div>
            </div>
            <div className="covid-slide">
              <div className="covid-slide-inner">
                <Line
                  data={deathGraphObjCountry}
                  height={190}
                  options={{
                    legend: {
                      labels: {
                        fontColor: '#bdbdbd',
                      },
                    },
                    scales: {
                      xAxes: [
                        {
                          gridLines: {
                            display: true,
                          },
                          ticks: {
                            fontColor: '#bdbdbd',
                          },
                        },
                      ],
                      yAxes: [
                        {
                          gridLines: {
                            display: false,
                          },
                          ticks: {
                            fontColor: '#bdbdbd',
                          },
                          id: 'A',
                          type: 'linear',
                          position: 'left',
                        },
                        {
                          id: 'B',
                          type: 'linear',
                          position: 'right',
                          ticks: {
                            max: 100,
                            min: 0,
                          },
                        },
                      ],
                    },
                  }}
                />
              </div>
            </div>
          </CovidSlider>
        )}
        {coronaDashboard && coronaDashboard.cityTable && cityDetails && (
          <Metrowise
            metroTable={coronaDashboard.cityTable}
            defaultcityid={1}
            handleSort={this.handleSortCity}
            eventDetails={cityDetails}
            getCityWiseReport={this.getCityWiseReport}
            riskGraphObjCity={riskGraphObjCity}
            growthGraphObjCity={growthGraphObjCity}
            deathGraphObjCity={deathGraphObjCity}
          />
        )}
        <Disclaimer title={disclaimerTitle} subTitle={disclaimerSubtitle} />
        {coronaDashboard && coronaDashboard.stateTable && eventDetails && (
          <Statewise
            stateTable={coronaDashboard.stateTable}
            defaultstateid={coronaDashboard.defaultstateid}
            handleSort={this.handleSort}
            getStateWiseReport={this.getStateWiseReport}
            eventDetails={eventDetails}
            riskGraphObj={riskGraphObj}
            deathGraphObj={deathGraphObj}
            growthGraphObj={growthGraphObj}
          />
        )}
        {advisories && <Timeline advisories={advisories} />}
        <Footer />
        <CustomModal
          BackdropProps={{
            className: classes.rootBackdrop,
          }}
          showModal={!isCovidDashAuth}
        >
          <div className="modal__inner">
            <div className="loginWrapper loginCovidDashWrap">
              <LoginBox
                parentHandler={this.getUserAuth}
                hasParentHandler={true}
                headerTitle={'COVID-19 TRACKER INDIA'}
                showDefaultFooter={false}
              />
            </div>
          </div>
        </CustomModal>
      </div>
    ) : null;
  }
}

export default Dashboard;
