import React from 'react';
import KaleidoscopeClient from '../../components/kaleidoscopeClient';

const KaleidoscopePageClient = props => (
  <KaleidoscopeClient {...props} isKEvent={false} />
);

export default KaleidoscopePageClient;
