import React from 'react';
import KaleidoscopeClientEvent from '../../components/kaleidoscopeClient/KaleidoscopeClientEvent';

const KEvent = props => <KaleidoscopeClientEvent {...props} />;

export default KEvent;
