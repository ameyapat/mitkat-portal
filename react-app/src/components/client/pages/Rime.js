import React, { Component } from 'react';
import {
  getRimeFeed,
  getRimeSavedFilters,
  saveRimeFilters,
} from '../../../api/api';
import RimeLiveList from '../components/rimeLiveList';
import MapApi from '../components/rimeMap/MapApi';
import './rime.scss';
import RimeFilters from '../components/rimeFilters';
import { connect } from 'react-redux';
import { checkDuplicateKeys } from '../../../helpers/utils';
import {
  categories,
  relevancyConstants,
} from '../components/rimeDashboard/helpers/constants';
import {
  handleSelectAllRelevancy,
  handleSelectAllRiskCat,
  handleSelectAllLanguage,
  updateOperations,
  updateRefreshDuration,
  updateSelectedCategories,
  updateSelectedLocations,
  updateSelectedRelevancy,
  updateTags,
  showApiHitTime,
  updateSelectedLanguage,
  liveMonitorSearchFeed,
  updateLiveMonitorSearchFeed,
} from '../../../actions/rimeActions';
import { toggleRimeEventArchives } from '../../../actions';
import RimeGeoFencing from '../components/rimeGeoFencing';
import { API_ROUTES } from '../../../helpers/http/apiRoutes';
import { fetchApi } from '../../../helpers/http/fetch';
import { withCookies } from 'react-cookie';
import RimeStatistics from '../components/rimeStatistics';
import RimeTopEvents from '../components/rimeTopEvents';
import RimeOnScreenFilters from '../components/rimeOnScreenFilters';
import Loader from '../../ui/loader';
import RimeSearch from '../components/rimeSearch';
import store from '../../../store';
import { toggleEnableSearch } from '../../../actions/riskTrackerAction';
import { toggleSearchBar } from '../../../actions/searchActions';

const current = new Date();
const date = `${current.getDate()}/${current.getMonth() +
  1}/${current.getFullYear()}`;
let reloadData = '';

@withCookies
@connect(
  state => {
    return {
      rime: state.rime,
      themeChange: state.themeChange,
    };
  },
  dispatch => {
    return {
      dispatch,
    };
  },
)
class Rime extends Component {
  state = {
    feed: [],
    reverseFeed: [],
    updatedFeed: [],
    staticFeed: [],
    mapFeed: [],
    topEventsFeed: [],
    riskCategories: [],
    relevancy: [],
    refreshDurations: '',
    address: [],
    addressLabel: [],
    operations: 'or',
    manualTags: '',
    relArray: [],
    catArray: [],
    language: [],
    clientLocations: [],
    apiStartTime: '',
    apiLastHitTime: '',
    loaderFlag: true,
  };

  componentDidMount() {
    this.getClientPins();
    this.getCatAndRelLength();
    this.initSetRimeFilters();
    this.getTopEvents();
  }

  getCatAndRelLength = () => {
    let rArray = [];
    let cArray = [];
    categories.map(item => {
      cArray.push(item.id);
    });
    relevancyConstants.map(item => {
      rArray.push(item.id);
    });
    this.setState({ catArray: cArray, relArray: rArray });
  };

  initSetRimeFilters() {
    const { rime, dispatch } = this.props;
    const { catArray, relArray } = this.state;
    const {
      riskCategories,
      relevancy,
      refreshDuration,
      locations,
      operations,
      tags,
      language,
    } = rime;
    getRimeSavedFilters().then(data => {
      if (data.filter === false) {
        this.setState(
          {
            riskCategories: riskCategories,
            relevancy: relevancy,
            refreshDurations: refreshDuration,
            address: locations,
            operations: operations,
            manualTags: tags,
            language: language,
          },
          () => this.initFetch(),
        );
      } else {
        const {
          address,
          operators,
          tags,
          riskLevel,
          riskCategory,
          timeRange,
          language,
        } = data;
        let add = [];
        address.length > 0 &&
          address.map(item => {
            add.push({ label: item, value: item });
          });
        dispatch(toggleRimeEventArchives(false));
        dispatch(updateSelectedLocations(add));
        dispatch(updateOperations(operators));
        dispatch(updateTags(tags));
        dispatch(updateSelectedRelevancy(riskLevel));
        dispatch(updateSelectedCategories(riskCategory));
        dispatch(updateRefreshDuration(timeRange));
        dispatch(updateSelectedLanguage(language));
        riskCategory.length < Math.max(...catArray) + 1
          ? dispatch(handleSelectAllRiskCat(false))
          : dispatch(handleSelectAllRiskCat(true));
        riskLevel.length < Math.max(...relArray)
          ? dispatch(handleSelectAllRelevancy(false))
          : dispatch(handleSelectAllRelevancy(true));
        language.length < 11
          ? dispatch(handleSelectAllLanguage(false))
          : dispatch(handleSelectAllLanguage(true));
        this.setState(
          {
            riskCategories: riskCategory,
            relevancy: riskLevel,
            refreshDurations: timeRange,
            address: add,
            operations: operators,
            manualTags: tags,
            language: language,
          },
          () => this.initFetch(),
        );
      }
    });
  }

  componentDidUpdate(prevProps) {
    const { rime } = this.props;
    const {
      riskCategories,
      relevancy,
      refreshDuration,
      locations,
      operations,
      tags,
      language,
    } = rime;
    if (prevProps.rime !== this.props.rime) {
      this.setState({
        riskCategories: riskCategories,
        relevancy: relevancy,
        refreshDurations: refreshDuration,
        address: locations,
        operations: operations,
        manualTags: tags,
        language: language,
      });
    }
  }

  initFetch = () => {
    this.handleRimeFeed();
    clearInterval(reloadData);
    reloadData = setInterval(
      this.handleRimeFeed,
      JSON.parse(this.state.refreshDurations) * 60000,
    );
  };

  clearFeed = () => {
    this.setState({ updatedFeed: [], feed: [], staticFeed: [] });
  };

  getAddress = () => {
    const { address } = this.state;
    let addressLabel = [];
    address.length > 0
      ? address.map(subitem => {
          addressLabel.push(subitem.label);
        })
      : addressLabel.push('');
    return addressLabel;
  };

  getManualTags = () => {
    const { manualTags } = this.state;
    let tagList = manualTags ? manualTags.split(',') : [''];
    return tagList;
  };

  handleRimeFeed = () => {
    const {
      riskCategories,
      relevancy,
      feed,
      refreshDurations,
      operations,
      language,
      staticFeed,
    } = this.state;
    const { rime, dispatch } = this.props;

    this.setState({ loaderFlag: true });
    let startTime = new Date(Date.now() - 1000 * 60 * refreshDurations);

    rime.apiHitTime
      ? this.setState({
          apiStartTime: startTime.toString(),
          apiLastHitTime: new Date().toString(),
        })
      : this.setState({ apiLastHitTime: new Date().toString() });

    if (!this.state.apiStartTime) {
      this.setState({
        apiStartTime: startTime.toString(),
      });
    }

    let addressLabel = this.getAddress();
    let manualTaglist = this.getManualTags();

    let reqObj = {
      riskCategory: riskCategories,
      timeRange: JSON.parse(refreshDurations),
      riskLevel: relevancy,
      address: addressLabel,
      tags: manualTaglist,
      operators: operations,
      mode: 'View',
      language: language,
    };
    getRimeFeed(reqObj).then(data => {
      this.setState(
        {
          feed:
            data.length > 0 ? [...feed, ...data].reverse() : this.state.feed,
          staticFeed:
            data.length > 0
              ? [...staticFeed, ...data].reverse()
              : this.state.staticFeed,
        },
        () => {
          dispatch(liveMonitorSearchFeed(this.state.staticFeed));
          dispatch(updateLiveMonitorSearchFeed(this.state.staticFeed));
          if (data) {
            this.setState({ loaderFlag: false });
          }
        },
      );
    });
    dispatch(showApiHitTime(false));
  };

  handleSaveFilters = () => {
    const {
      riskCategories,
      relevancy,
      feed,
      refreshDurations,
      operations,
      language,
    } = this.state;

    let addressLabel = this.getAddress();
    let manualTaglist = this.getManualTags();

    let reqObj = {
      riskCategory: riskCategories,
      timeRange: JSON.parse(refreshDurations),
      riskLevel: relevancy,
      address: addressLabel,
      tags: manualTaglist,
      operators: operations,
      mode: 'View',
      language: language,
    };
    saveRimeFilters(reqObj).then(data => {
      this.initFetch();
    });
  };

  handleUpdateFeed = () => {
    const { feed, updatedFeed } = this.state;
    if (feed?.length) {
      let feedItem = feed.pop();
      let hasDuplicateIds = checkDuplicateKeys(updatedFeed, feedItem);
      if (hasDuplicateIds) {
        return;
      } else {
        updatedFeed.unshift(feedItem);
      }
      this.setState({
        feed,
        updatedFeed,
      });
    }
  };

  getTopEvents = () => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let reqObj = {
      method: 'GET',
      isAuth: true,
      authToken,
      url: API_ROUTES.topEvents,
    };

    fetchApi(reqObj)
      .then(data => {
        this.setState({ topEventsFeed: data });
      })
      .catch(e => console.log(e));
  };

  getClientPins = () => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let reqObj = {
      method: 'POST',
      isAuth: true,
      authToken,
      url: API_ROUTES.getClientLocations,
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        this.setState({ clientLocations: data });
      })
      .catch(e => console.log(e));
  };

  updateFeed = interval => {
    const { feed } = this.state;
    const addFeedItem = setInterval(this.handleUpdateFeed, interval);
    if (feed.length === 0) {
      clearInterval(addFeedItem);
    }
  };

  filterPresentPins = updateFeed => {
    return updateFeed.filter(feed => feed);
  };

  filterRandomPins = updateFeed => {
    return updateFeed.filter(feed => !feed.ispresent);
  };

  handleToggleSearch = show => {
    store.dispatch(toggleEnableSearch(show));
    store.dispatch(toggleSearchBar(show));
  };

  render() {
    const {
      updatedFeed,
      clientLocations,
      feed,
      staticFeed,
      apiLastHitTime,
      apiStartTime,
      topEventsFeed,
      loaderFlag,
    } = this.state;
    const {
      themeChange: { setDarkTheme },
      rime,
    } = this.props;

    return (
      <>
        {loaderFlag && <Loader />}
        <div id="rimeCont" className={`${setDarkTheme ? 'dark' : 'light'}`}>
          <div className="row1">
            <div className="header">
              <div className="headerFilter">
                <RimeFilters
                  handleRimeFeed={this.handleSaveFilters}
                  clearFeed={this.clearFeed}
                />
              </div>
              <div className="header__title">
                <div className="number">
                  {feed?.length ? rime.geoFenceNumbers : 0}
                </div>
                <div className="title">Total Geo-Fenced Events</div>
              </div>
            </div>
            <RimeGeoFencing
              feed={staticFeed}
              clientLocations={clientLocations}
            />
          </div>
          <div className="row2">
            <div className="row2--map">
              <div className="rimeMap">
                <RimeOnScreenFilters
                  feed={staticFeed}
                  clientLocations={clientLocations}
                />
              </div>
            </div>
            <div className="row2__subRow">
              <div className="topEvents">
                <div className="header">Top 10 Worldwide Events</div>
                <RimeTopEvents topEventsFeed={topEventsFeed} />
              </div>
              <div className="numberStats">
                <RimeStatistics
                  feedLength={
                    rime.liveMonitorSearchFeed?.length
                      ? rime.liveMonitorSearchFeed?.length
                      : 0
                  }
                  lastHitTime={apiLastHitTime}
                  firstHitTime={apiStartTime}
                />
              </div>
            </div>
          </div>
          <div className="row3">
            <RimeSearch
              placeholder="Search Events"
              onClose={() => this.handleToggleSearch(false)}
            />
            <RimeLiveList feed={rime.liveMonitorSearchFeed} />
          </div>
        </div>
      </>
    );
  }
}

export default Rime;
