import React, { Component } from 'react';
import { withCookies } from 'react-cookie';
import { fetchApi } from '../../../helpers/http/fetch';
import { API_ROUTES } from '../../../helpers/http/apiRoutes';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import { getArrFromObj, sortBy } from '../../../helpers/utils';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { withRouter } from 'react-router-dom';
import store from '../../../store';
import { toggleIsFetch } from '../../../actions';
import MitkatLogo from '../../../assets/mitkat-logo-filled.png';
import { withStyles, Typography } from '@material-ui/core';

import CoronaTableParams from '../components/coronaClient/coronaTableParams/CoronaTableParams';
import clsx from 'clsx';
import CoronaStateParams from '../components/coronaClient/coronaStateParams/CoronaStateParams';
import Empty from '../../ui/empty';
import InfoGraphCorona from '../components/coronaClient/infoGraphCorona/InfoGraphCorona';
import EventGraph from '../components/heatMap/EventGraph';
import { Bar, Line } from 'react-chartjs-2';
import { getRectCenter } from '@fullcalendar/core';
import CustomLabel from '../../ui/customLabel';

import '../sass/CoronaMedia.scss';
import '../sass/CoronaDashboard.scss';
import DisclaimerBox from '../../ui/disclaimerBox';
import Advisories from '../components/coronaClient/advisories';
import Indicator from '../../ui/indicator/Indicator';

import {
  downloadDistrictReport,
  downloadExcel,
  downloadSummary,
} from '../../admin/helpers/utils';

const drawerWidth = 420;

const styles = theme => ({
  rootAppBar: {
    backgroundColor: '#fff',
  },
  rootToolbar: {
    minHeight: 50,
    display: 'flex',
    justifyContent: 'space-between',
  },
  btnGreen: {
    backgroundColor: '#53ab3e',
    color: 'var(--whiteMediumEmp)',
    fontSize: 11,
    '&:hover': {
      backgroundColor: 'rgba(83, 171, 62, 0.8)',
    },
  },
  logoWrapper: {
    display: 'flex',
    alignItems: 'center',
  },
  godrejLogo: {
    width: '90px',
  },
  mitkatLogo: {
    width: '120px',
    maxWidth: '100%',
    paddingLeft: '15px',
    paddingRight: '15px',
    marginLeft: '15px',
  },
  rootEventWrapper: {
    flex: 3,
    minHeight: '100vh',
    backgroundColor: '#f5f5f5',
  },
  rootContentWrapper: {
    // display: "flex",
    backgroundColor: '#F7FAFC',
    minHeight: '100vh',
  },
  rootBoxes: {
    display: 'flex',
    height: 'calc(100vh - 64px)',
    maxHeight: 220,
  },
  childBox: {
    // minHeight: 400,
    // padding: 8,
    paddingTop: 8,
  },
  boxOne: {
    display: 'flex',
    flex: 2,
    // backgroundColor: 'red',
    height: '100%',
    maxHeight: 220,
    overflow: 'hidden',
    flexWrap: 'wrap',
    maxWidth: 382,
    flexBasis: 382,
    justifyContent: 'space-around',
  },
  boxTwo: {
    flex: 5.6,
    // backgroundColor: 'yellow',
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'space-evenly',
    height: '100%',
    maxHeight: 220,
    // minHeight: 400,
  },
  graphBoxOne: {
    flexBasis: 200,
    background: '#fff',
    boxShadow: '1px 2px 2px 1px rgba(0,0,0,0.2)',
    padding: 10,
    borderRadius: 5,
    height: 200,
    margin: 5,
  },
  rootMapWrappper: {
    flex: 2,
    // backgroundColor: "yellow",
    borderLeft: '1px solid #d3d3d3',
    minHeight: '100vh',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-evenly',
  },
  bannerWrapper: {
    backgroundColor: 'green',
    minHeight: '128px',
  },
  mapWrap: {
    flex: '1',
  },
  emergingRiskWrap: {
    flex: '1',
  },
  filters: {
    borderTop: '1px solid #f1f1f1',
    padding: '10px 20px',
    display: 'flex',
    justifyContent: 'space-between',
    background: '#fff',
    alignItems: 'center',
  },
  rootDrawer: {},
  paper: {
    width: drawerWidth,
  },
  arrowClose: {
    display: 'flex',
    justifyContent: 'space-between',
    padding: '5px 10px',
    borderBottom: '1px solid #f1f1f1',
    alignItems: 'center',
  },
  rootTypography: {
    display: 'flex',
    alignItems: 'center',
  },
  rootTypographyFilter: {
    display: 'flex',
    alignItems: 'center',
    marginRight: '10px',
  },
  rootTypographyDownload: {
    display: 'flex',
    alignItems: 'center',
    marginLeft: '10px',
    color: 'var(--whiteMediumEmp)',
  },
  drawerFilterIcon: {
    marginRight: '10px',
  },
  typographyRiskWatch: {
    marginLeft: 10,
    fontFamily: `'Montserrat', sans-serif`,
    fontSize: '1.1em',
    color: 'rgba(0, 0, 0, 0.87)',
    textTransform: 'uppercase',
    alignItems: 'center',
    display: 'flex',
  },
  btnDownload: {
    // zIndex: 999,
    // position: 'absolute',
    // bottom: '19%',
    // right: '2%',
    // background: '#2C97D0',
    background: '#C5005E',
    // borderRadius: '50%',
    // height: '40px',
    // width: '40px !important',
    display: 'flex',
    // minWidth: '40px',
    alignItems: 'center',
    padding: '6px 8px',
    color: 'var(--whiteMediumEmp)',
    // lineHeight: '55px',
    boxShadow: '0 3px 5px 0 rgba(0, 0, 0, 0.6)',
    '&:hover': {
      background: 'rgba(197, 0, 94, 0.8)',
    },
  },
  rootCoronaPage: {
    position: 'relative',
    '& h1': {
      fontFamily: 'Helvetica',
    },
    '& h2': {
      fontFamily: 'Helvetica',
    },
    '& h3': {
      fontFamily: 'Helvetica',
    },
    '& h4': {
      fontFamily: 'Helvetica',
    },
    '& p': {
      fontFamily: 'Arial, Helvetica, sans-serif',
    },
    '& span': {
      fontFamily: 'Arial, Helvetica, sans-serif',
    },
  },
  downloadIcon: {
    color: 'var(--whiteMediumEmp)',
    marginLeft: 5,
  },
  morePill: {
    fontSize: '0.7em',
    background: 'rgba(0, 0, 0, 0.3)',
    borderRadius: '25px',
    padding: '5px',
    color: 'var(--whiteMediumEmp)',
    margin: '0 0 0 5px',
  },
  subText: {
    fontSize: '15px',
    textTransform: 'capitalize',
    margin: '0 0 0 5px',
    background: 'rgba(0, 0, 0, 0.08)',
    padding: '5px 8px',
    borderRadius: 4,
    '&:last-child': {
      // marginLeft: 0,
    },
  },
  rootStateTableWrapper: {
    padding: '5px 10px 5px 10px',
  },
  stateWiseLabel: {
    // fontWeight: 600,
    fontSize: '14px !important',
    padding: '5px 10px 5px 10px !important',
    color: 'rgba(73, 54, 87, 1)',
  },
  indiaWiseLabel: {
    // fontWeight: 600,
    fontSize: '14px !important',
    padding: '10px 10px 0 10px !important',
    color: 'rgba(73, 54, 87, 1)',
  },
  typography: {
    color: 'rgba(73, 54, 87, 1)',
    display: 'flex',

    '& span': {
      marginRight: 5,
    },
    // fontWeight: 600,
  },
  lastReportTxt: {
    color: '#1E7A2D',
    textTransform: 'uppercase',
  },
  titleCovid: {
    fontWeight: 600,
  },
});

@withCookies
@withRouter
@withStyles(styles)
class CoronaDashboard extends Component {
  state = {
    coronaDashboard: {},
    riskGraphObj: {
      labels: [],
      datasets: [],
    },
    deathGraphObj: {
      labels: [],
      datasets: [],
    },
    growthGraphObj: {
      labels: [],
      datasets: [],
    },
  };

  componentDidMount() {
    this.getCoronaDashboard()
      .then(data => {
        this.setState({ coronaDashboard: data }, () => {
          this.parseCaseGraph();
          this.parseGrowthGraph();
          this.parseDeathGraph();
        });
      })
      .catch(e => console.log('errr!!!'));
  }

  getCoronaDashboard = () => {
    return new Promise((resolve, reject) => {
      try {
        const { cookies } = this.props;
        const reqObj = {
          url: API_ROUTES.coronaDashboard,
          isAuth: true,
          authToken: cookies.get('authToken'),
          method: 'GET',
          showToggle: true,
        };

        fetchApi(reqObj)
          .then(data => {
            resolve(data);
          })
          .catch(e => console.log(e));
      } catch (e) {
        reject(e);
      }
    });
  };

  handleSelect = (type, value) => this.setState({ [type]: value });

  handleLogout = () => {
    const { cookies } = this.props;
    store.dispatch(toggleIsFetch(true));
    setTimeout(() => {
      cookies.remove('authToken');
      cookies.remove('userRole');
      cookies.remove('authUserId');
      this.props.history.push('/');
      store.dispatch(toggleIsFetch(false));
    }, 2000);
  };

  parseCaseGraph = () => {
    const { casegraph } = this.state.coronaDashboard;
    const { riskGraphObj } = this.state;
    const labels = [];
    const dsObjOne = {
      label: 'Total Cases',
      data: [],
      backgroundColor: 'rgba(186, 39, 37, 0.7)',
      borderColor: 'rgba(186, 39, 37, 1)',
      borderWidth: 2,
      pointBackgroundColor: 'rgba(255,255,255,0)',
      pointBorderColor: 'rgba(255,255,255,0)',
      // lineTension: 1,
      order: 2,
    };
    const dsObjTwo = {
      label: 'Active Cases',
      data: [],
      backgroundColor: 'rgba(66, 75, 84, 0.9)',
      borderColor: 'rgba(66, 75, 84, 1)',
      borderWidth: 2,
      pointBackgroundColor: 'rgba(255,255,255,0)',
      pointBorderColor: 'rgba(255,255,255,0)',
      order: 1,
    };

    casegraph.map(i => {
      labels.push(i.x_point);
      dsObjOne.data.push(i.y_point);
      dsObjTwo.data.push(i.y_point_new);
    });

    riskGraphObj['labels'] = labels;
    riskGraphObj.datasets = [{ ...dsObjOne }, { ...dsObjTwo }];
    this.setState({ riskGraphObj });
  };

  parseGrowthGraph = () => {
    const { growthgraph } = this.state.coronaDashboard;
    const { growthGraphObj } = this.state;
    const labels = [];
    const dsObjOne = {
      label: 'Additional Cases',
      data: [],
      backgroundColor: 'rgba(6, 144, 165, 0.7)',
      borderColor: 'rgba(6, 144, 165, 1)',
      borderWidth: 2,
      pointBackgroundColor: 'rgba(255,255,255,0)',
      pointBorderColor: 'rgba(255,255,255,0)',
    };
    const dsObjTwo = {
      label: 'Additional Deaths',
      data: [],
      backgroundColor: 'rgba(66, 75, 84, 1)',
      borderColor: 'rgba(66, 75, 84, 1)',
      borderWidth: 2,
      pointBackgroundColor: 'rgba(255,255,255,0)',
      pointBorderColor: 'rgba(255,255,255,0)',
    };

    growthgraph.map(i => {
      labels.push(i.x_point);
      dsObjOne.data.push(i.y_point);
      dsObjTwo.data.push(i.y_point_new);
    });

    growthGraphObj['labels'] = labels;
    growthGraphObj.datasets = [{ ...dsObjOne }, { ...dsObjTwo }];
    this.setState({ growthGraphObj });
  };

  parseDeathGraph = () => {
    const { deathgraph } = this.state.coronaDashboard;
    const { deathGraphObj } = this.state;
    const labels = [];
    const dsObjOne = {
      label: 'Deaths',
      data: [],
      backgroundColor: 'rgba(186, 39, 37, 0)',
      borderColor: 'rgba(186, 39, 37, 1)',
      borderWidth: 2,
      pointBackgroundColor: 'rgba(255,255,255,0)',
      pointBorderColor: 'rgba(255,255,255,0)',
      yAxisID: 'A',
    };
    const dsObjTwo = {
      label: 'Deaths Growth rate',
      data: [],
      backgroundColor: 'rgba(66, 75, 84, 0)',
      borderColor: 'rgba(66, 75, 84, 1)',
      borderWidth: 2,
      pointBackgroundColor: 'rgba(255,255,255,0)',
      pointBorderColor: 'rgba(255,255,255,0)',
      yAxisID: 'B',
    };

    deathgraph.map(i => {
      labels.push(i.x_point);
      dsObjOne.data.push(i.y_point);
      dsObjTwo.data.push(i.y_point_new);
    });

    deathGraphObj['labels'] = labels;
    deathGraphObj.datasets = [{ ...dsObjOne }, { ...dsObjTwo }];
    this.setState({ deathGraphObj });
  };

  handleSort = (type, sort) => {
    const { stateTable } = this.state.coronaDashboard;
    let sortedStateTable = sortBy(stateTable, type, sort);
    this.setState({ stateTable: sortedStateTable });
  };

  renderCovidMap = () => {
    const { history } = this.props;
    history.push('/covid-19-map');
    // const authUserId = cookies.get('authUserId');

    // if(authUserId){
    //   history.push('/covid-19-map');
    // }else{
    //   history.push({
    //     pathname: '/',
    //     state: {
    //       redirectTo: '/covid-19-map'
    //     }
    //   })
    // }
  };

  render() {
    const { classes, cookies } = this.props;
    const {
      stateTable,
      totalinfected,
      totaldeaths,
      newcases,
      newdeaths,
      growthratecases,
      defaultstateid,
      totalrecoverd,
      subjectTime,
      recoverypercent,
    } = this.state.coronaDashboard;
    const authToken = cookies.get('authToken');

    return (
      <div id="coronaDashboardPage" className={classes.rootCoronaPage}>
        <AppBar
          position="static"
          classes={{ root: classes.rootAppBar }}
          className={'rootAppBar'}
        >
          <Toolbar
            classes={{ root: classes.rootToolbar }}
            className="rootToolbar"
          >
            <div className={clsx(classes.logoWrapper, 'logo-wrapper')}>
              <img
                src={MitkatLogo}
                alt="mitkat-logo"
                className={classes.mitkatLogo}
              />
              <Typography
                component="h1"
                classes={{ root: classes.typography }}
                className="typography"
              >
                <span className={classes.titleCovid}>
                  INDIA COVID-19 TRACKER
                </span>
                <span className={classes.lastReportTxt}>{subjectTime}</span>
              </Typography>
            </div>
            <div className="download-district-covid">
              <Button
                size="small"
                variant="contained"
                color="secondary"
                onClick={() => downloadDistrictReport(authToken)}
                classes={{
                  root: clsx('rootBtnPdf', 'downloadBtns'),
                }}
              >
                <i className="fas fa-file-pdf"></i>{' '}
                <span className="c-mg-10">
                  Complete <span className="hide-sm">Report</span>
                </span>
              </Button>
              <Button
                size="small"
                variant="contained"
                color="secondary"
                onClick={() => downloadSummary(authToken)}
                classes={{
                  root: clsx('rootBtnSummary', 'downloadBtns'),
                }}
              >
                <i className="fas fa-file-download"></i>{' '}
                <span className="c-mg-10">
                  Summary <span className="hide-sm">Report</span>
                </span>
              </Button>
              <Button
                size="small"
                variant="contained"
                color="secondary"
                onClick={() => downloadExcel(authToken)}
                classes={{
                  root: clsx('rootBtnExcel', 'downloadBtns'),
                }}
              >
                <i className="far fa-file-excel"></i>{' '}
                <span className="c-mg-10">
                  Excel <span className="hide-sm">Report</span>
                </span>
              </Button>
            </div>
            {cookies.get('authUserId') ? (
              <Button
                classes={{ root: classes.btnGreen }}
                onClick={() => this.handleLogout()}
              >
                Logout
              </Button>
            ) : (
              <Button
                classes={{ root: classes.btnGreen }}
                onClick={() => this.props.history.push('/')}
              >
                Log In
              </Button>
            )}
          </Toolbar>
        </AppBar>
        <div className={classes.rootContentWrapper}>
          {/* <CustomLabel
                title='India Parameters'
                classes={clsx('select--label', classes.indiaWiseLabel)}
            />       */}
          <div className={clsx(classes.rootBoxes, 'root-boxes')}>
            <div className={clsx(classes.childBox, classes.boxOne, 'box-one')}>
              <InfoGraphCorona
                title={'Total Infected'}
                value={totalinfected}
                icon="fas fa-users-cog"
                color="#FF8A47"
              />
              <InfoGraphCorona
                meta="(after 12 AM)"
                title={'New Cases'}
                value={newcases}
                icon="fas fa-user-plus"
                color="#BBEF1F"
              />
              <InfoGraphCorona
                title={'% Increase'}
                value={growthratecases}
                icon="fas fa-chart-bar"
                color="#FFB728"
              />
              <InfoGraphCorona
                title={'Total Recovered'}
                value={`${totalrecoverd || 0} (${recoverypercent || 0}%)`}
                icon="fas fa-chart-bar"
                color="#BC2C1A"
              />
              <InfoGraphCorona
                title={'Total Deaths'}
                value={totaldeaths}
                icon="fas fa-user-alt-slash"
                color="#D52941"
              />
              <InfoGraphCorona
                meta="(after 12 AM)"
                title={'New Deaths'}
                value={newdeaths}
                icon="fas fa-user-plus"
                color="#FF5F17"
              />
              {/* <InfoGraphCorona title={'No. Death Rates'} value={growthratedeaths} icon="fas fa-chart-bar" color="#BC2C1A" /> */}
            </div>
            {/* graphs goes here */}
            <div className={clsx(classes.childBox, classes.boxTwo, 'box-two')}>
              <div className={clsx(classes.graphBoxOne, 'graph-box-one')}>
                <Line
                  data={this.state.riskGraphObj}
                  height={190}
                  options={{
                    scales: {
                      xAxes: [
                        {
                          gridLines: {
                            display: true,
                          },
                        },
                      ],
                      yAxes: [
                        {
                          gridLines: {
                            display: true,
                          },
                        },
                      ],
                    },
                  }}
                />
              </div>
              <div className={clsx(classes.graphBoxOne, 'graph-box-one')}>
                <Bar
                  data={this.state.growthGraphObj}
                  height={190}
                  options={{
                    scales: {
                      xAxes: [
                        {
                          gridLines: {
                            display: true,
                          },
                        },
                      ],
                      yAxes: [
                        {
                          gridLines: {
                            display: true,
                          },
                        },
                      ],
                    },
                  }}
                />
              </div>
              <div className={clsx(classes.graphBoxOne, 'graph-box-one')}>
                <Line
                  data={this.state.deathGraphObj}
                  height={190}
                  options={{
                    scales: {
                      xAxes: [
                        {
                          gridLines: {
                            display: true,
                          },
                        },
                      ],
                      yAxes: [
                        {
                          gridLines: {
                            display: false,
                          },
                          id: 'A',
                          type: 'linear',
                          position: 'left',
                        },
                        {
                          id: 'B',
                          type: 'linear',
                          position: 'right',
                          ticks: {
                            max: 100,
                            min: 0,
                          },
                        },
                      ],
                    },
                  }}
                />
              </div>
            </div>
          </div>
          {/* table goes here */}
          {/* <CustomLabel
                title='State Wise Parameters'
                classes={clsx('select--label', classes.stateWiseLabel)}
            /> */}
          <div className="metaWrappers">
            <div className="meta-child-box one">
              <DisclaimerBox />
            </div>
            <div className="meta-child-box map-btn-box">
              <button onClick={this.renderCovidMap}>
                <i className="fas fa-virus"></i>
                <span>COVID-19 Map</span>
                <Indicator value="NEW" />
              </button>
            </div>
            <div className="meta-child-box two">
              <Advisories />
            </div>
          </div>
          <div
            className={clsx(
              classes.rootStateTableWrapper,
              'root-state-table-wrapper',
            )}
          >
            {stateTable && stateTable.length > 0 ? (
              <CoronaTableParams
                data={stateTable}
                handleSort={this.handleSort}
                defaultStateId={defaultstateid}
              />
            ) : (
              <Empty title="No data available at this moment" />
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default CoronaDashboard;
