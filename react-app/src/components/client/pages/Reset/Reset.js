import React, { Component } from 'react';
import './resetPassword.scss';

import { resetPassword } from '../../../../api/api';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';

import { withCookies } from 'react-cookie';
import { withRouter } from 'react-router-dom';

//components @material-ui
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CustomMessage from '../../../ui/customMessage';
import validator from 'validator';

@withCookies
@withRouter
class Reset extends Component {
  state = {
    userName: '',
    oldPassword: '',
    newPassword: '',
    confirmPassword: '',
    isValid: false,
    showToast: false,
    toastMsg: '',
  };

  handleSubmit = e => {
    e.preventDefault();
    const { userName, oldPassword, newPassword } = this.state;
    const { cookies } = this.props;
    const payload = {
      username: userName,
      oldpassword: oldPassword,
      newpassword: newPassword,
    };

    const reqObj = {
      method: 'POST',
      url: `${API_ROUTES.resetPassword}`,
      isAuth: true,
      authToken: cookies.get('authToken'),
      showToggle: true,
      data: payload,
    };

    resetPassword(reqObj)
      .then(res => {
        if (res.status === 200) {
          this.setState(
            { toastMsg: `${res.message}, Redirecting...`, showToast: true },
            () => {
              setTimeout(() => {
                this.handleLogout();
              }, 2000);
            },
          );
        } else {
          this.setState({ toastMsg: res.message, showToast: true });
        }
      })
      .catch(e => {
        this.setState({
          toastMsg: 'Opps! Something went wrong',
          showToast: true,
        });
      });
  };

  handleGoBack = () => {
    this.props.history.goBack();
  };

  handleLogout = () => {
    const { cookies, history } = this.props;
    cookies.remove('authToken');
    cookies.remove('userRole');
    setTimeout(() => {
      history.push('/');
    }, 2000);
  };

  handleClose = () => {
    this.setState({ showToast: false });
  };

  handleInputChange = (e, type) => {
    this.setState({ [type]: e.target.value }, () => this.validate());
  };

  validate = () => {
    const { userName, oldPassword, newPassword, confirmPassword } = this.state;
    if (
      userName &&
      oldPassword &&
      newPassword &&
      confirmPassword &&
      newPassword === confirmPassword
    ) {
      this.setState({ isValid: true });
    } else {
      this.setState({ isValid: false });
    }
  };

  render() {
    const {
      showToast,
      toastMsg,
      isValid,
      newPassword,
      confirmPassword,
    } = this.state;

    return (
      <div id="resetPassword">
        <button className="resetPassword__backBtn" onClick={this.handleGoBack}>
          <i className="fas fa-long-arrow-alt-left"></i> Back
        </button>
        <div className="resetPassword__box">
          <div className="resetPassword__header__info">
            <h1>Create new password</h1>
            <p>
              Your new password must be different from previous used passwords.
            </p>
          </div>
          <div className="resetPassword__body">
            <div className="resetPassword__wrap">
              <label>Username</label>
              <input
                onChange={e => this.handleInputChange(e, 'userName')}
                type="text"
                placeholder="Username"
              />
            </div>
            <div className="resetPassword__wrap">
              <label>Old Password</label>
              <input
                onChange={e => this.handleInputChange(e, 'oldPassword')}
                type="password"
                placeholder="Type old password"
              />
            </div>
            <div className="resetPassword__wrap">
              <label>New Password</label>
              <input
                onChange={e => this.handleInputChange(e, 'newPassword')}
                type="password"
                placeholder="New password"
              />
            </div>
            <div className="resetPassword__wrap">
              <label>Confirm Password</label>
              <input
                onChange={e => this.handleInputChange(e, 'confirmPassword')}
                type="password"
                placeholder="Confirm password"
              />
            </div>
          </div>
          {!validator.isStrongPassword(newPassword) && (
            <CustomMessage classes={'resetPassword__customMsg'}>
              <p>
                <span className="customMsg__icon">
                  <i className="fas fa-lock"></i>
                </span>
                Your password needs to:
              </p>
              <ul>
                <li>be atleast 8 characters long.</li>
                <li>include both uppercase &amp; lowercase characters.</li>
                <li>be atleast one number or symbol.</li>
              </ul>
            </CustomMessage>
          )}
          <div className="resetPassword__footer">
            <button
              type="submit"
              onClick={this.handleSubmit}
              disabled={!isValid}
            >
              Reset Password
            </button>
          </div>
        </div>
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={showToast}
          autoHideDuration={3000}
          onClose={this.handleClose}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
          message={<span id="message-id">{toastMsg}</span>}
        />
      </div>
    );
  }
}

export default Reset;
