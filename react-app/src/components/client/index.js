import React, { Component } from 'react';
import './sass/style.scss';
import { connect } from 'react-redux';
import { withCookies } from 'react-cookie';
import { getSubscriptions } from '../../requestor/mitkatWeb/requestor';
import { setAppPermissions } from '../../actions';
import ClientDashboard from './components/clientDashboard';
import ClientSideNav from './components/clientSideNav';
@withCookies
@connect(
  state => {
    return {
      appState: state.appState,
      themeChange: state.themeChange,
    };
  },
  dispatch => {
    return {
      dispatch,
    };
  },
)
class Client extends Component {
  UNSAFE_componentWillMount() {
    let { cookies } = this.props;
    let userRoleFromCookie = cookies.get('userRole');

    if (
      userRoleFromCookie === undefined ||
      (userRoleFromCookie !== 'ROLE_CLIENT' &&
        userRoleFromCookie !== 'ROLE_CLIENT_USER')
    ) {
      this.props.history.push('/');
      return;
    }
  }

  componentDidMount() {
    getSubscriptions().then(data => {
      this.props.dispatch(setAppPermissions(data));
    });
  }

  render() {
    const {
      themeChange: { setDarkTheme },
    } = this.props;

    return (
      <div
        id="client"
        className={`clientWrap ${setDarkTheme ? 'dark' : 'light'}`}
      >
        <ClientSideNav />
        <ClientDashboard />
      </div>
    );
  }
}

export default Client;
