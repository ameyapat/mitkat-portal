import React, { Component } from 'react';
import EventList from './components/EventList';
import TopRiskList from './components/TopRiskList';
import RiskCategoryDistribution from './components/RiskCategoryDistribution';
import RiskDistribution from './components/RiskDistribution';
import RiskLevelDistribution from './components/RiskLevelDistribution';
import TopMenu from '../../ui/TopMenu';
import clientLogo from '../../../components/client/Barclays/assests/barclays_w.png';
import './style.scss';

import CustomGoogleMap from '../../ui/customGoogleMap';
import customMapStyle from './helpers/mapStyle.json';
import CustomModal from '../../ui/modal';

import customLocationIcon from '../Barclays/assests/solid-pins/location.png';
import theftBurglaryIcon from '../Barclays/assests/solid-pins/Theft & Burglary.png';
import vandalismIcon from '../Barclays/assests/solid-pins/Vandalism.png';
import terrorismIcon from '../Barclays/assests/solid-pins/Terrorism.png';
import naturalDisastersIcon from '../Barclays/assests/solid-pins/Natural Disasters.png';
import assaultIcon from '../Barclays/assests/solid-pins/Assault.png';
import fireIncidentsIcon from '../Barclays/assests/solid-pins/Fire Incidents.png';
import adverseWeatherIcon from '../Barclays/assests/solid-pins/Adverse Weather.png';
import civilDisruptionIcon from '../Barclays/assests/solid-pins/Civil Disruption.png';
import regulatoryIcon from '../Barclays/assests/solid-pins/Regulatory.png';
import healthIcon from '../Barclays/assests/solid-pins/Health.png';
import technicalIncidentIcon from '../Barclays/assests/solid-pins/Technical Incident.png';

import {
  toggleBarclaysLocation,
  togglEventAndRiskData,
} from '../../../actions/barclaysActions';
import {
  getDashboardInfoBarclays,
  downloadEventsBarclays,
  getDashEventDetailsBarclays,
  downloadPdfBarclays,
} from '../../../requestor/barclays/requestor';
import { connect } from 'react-redux';

import {
  setDashboardData,
  updateDashboardEventDetails,
} from '../../../actions/barclaysActions';
import Filter from './components/Filter';
import EventDetailsBox from './components/EventDetailsBox';
import moment from 'moment';
import { withCookies } from 'react-cookie';

const mappedIcons = {
  1: theftBurglaryIcon,
  2: vandalismIcon,
  3: terrorismIcon,
  4: naturalDisastersIcon,
  5: assaultIcon,
  6: fireIncidentsIcon,
  7: adverseWeatherIcon,
  8: civilDisruptionIcon,
  9: regulatoryIcon,
  10: healthIcon,
  11: technicalIncidentIcon,
};

@withCookies
class Barclays extends Component {
  state = {
    showModal: false,
    showEventDetailsModal: false,
    isLocation: true,
  };

  componentDidMount() {
    this.initDashboard();
  }

  initDashboard() {
    const {
      filters: {
        startDate,
        endDate,
        selectedCountries,
        selectedCities,
        selectedRiskLevel,
        selectedCategory,
        selectedAssets,
        distance,
      },
    } = this.props.barclaysFilter;
    const reqData = {
      startdate: moment(startDate).format('YYYY-MM-DD'),
      enddate: moment(endDate).format('YYYY-MM-DD'),
      countries: selectedCountries,
      cities: selectedCities,
      assets: selectedAssets,
      risklevels: selectedRiskLevel,
      riskcaegories: selectedRiskLevel,
      distance,
    };
    getDashboardInfoBarclays(reqData).then(data => {
      this.props.dispatch(setDashboardData(data));
    });
  }

  closeModal = () => this.setState({ showModal: false });

  handleOpenFilter = () => this.setState({ showModal: true });

  getEventDetails = id => {
    getDashEventDetailsBarclays(id).then(data => {
      this.props.dispatch(updateDashboardEventDetails(data));
      this.toggleEventDetails(true);
    });
  };

  toggleEventDetails = show => this.setState({ showEventDetailsModal: show });

  handleDownloadPdf = id => {
    downloadPdfBarclays(id);
  };

  toggleOfficeLocation = () => {
    const {
      cookies,
      dispatch,
      barclaysEventDetails: { showBarclaysLocation },
    } = this.props;

    dispatch(toggleBarclaysLocation(!showBarclaysLocation));
  };

  handleDownloadExcel = reqData => {
    downloadEventsBarclays(reqData);
  };

  toggleSwitchItems = type => {
    const { eventAndRiskData } = this.props.barclaysEventDetails;
    eventAndRiskData[type] = !eventAndRiskData[type];
    this.props.dispatch(togglEventAndRiskData(eventAndRiskData));
  };

  render() {
    const {
      toprisks = [],
      riskcategorydata = [],
      riskleveldata = [],
      eventdistributiongraph = [],
      mapcoordinates = [],
      eventlist = [],
      officelocations = [],
    } = this.props.barclays.dashboardData;
    const { showModal, showEventDetailsModal } = this.state;
    const {
      showEventList,
      showTopRiskEventList,
      showEventDistribution,
      showRiskLevelDistribution,
      showRiskCategoryDistribution,
    } = this.props.barclaysEventDetails.eventAndRiskData;

    return (
      <>
        <div id="body--wrap">
          <TopMenu
            clientLogo={clientLogo}
            handleOpenFilter={this.handleOpenFilter}
            toggleOfficeLocation={this.toggleOfficeLocation}
            toggleSwitchItems={this.toggleSwitchItems}
            {...this.props}
          />
          <CustomGoogleMap
            googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyCYOTLyn6xbysArlvlW4zpxnH9p6H3An08&v=3.exp&libraries=geometry,drawing,places"
            loadingElement={<div style={{ height: `100vh` }} />}
            containerElement={
              <div style={{ height: `100vh`, position: 'relative' }} />
            }
            mapElement={<div style={{ height: `100vh` }} />}
            list={
              (mapcoordinates && mapcoordinates.length > 0 && mapcoordinates) ||
              []
            }
            officelocations={
              (officelocations &&
                officelocations.length > 0 &&
                officelocations) ||
              []
            }
            mapStyles={customMapStyle}
            customIcon={mappedIcons}
            customLocationIcon={customLocationIcon}
            onPinClick={this.getEventDetails}
          />

          {eventlist && eventlist.length > 0 && showEventList && (
            <EventList getEventDetails={this.getEventDetails} />
          )}

          {toprisks && toprisks.length > 0 && showTopRiskEventList && (
            <TopRiskList />
          )}

          {riskcategorydata &&
            riskcategorydata.length > 0 &&
            showRiskCategoryDistribution && <RiskCategoryDistribution />}

          {riskleveldata &&
            riskleveldata.length > 0 &&
            showRiskLevelDistribution && <RiskLevelDistribution />}

          {riskleveldata &&
            riskleveldata.length > 0 &&
            eventdistributiongraph.length > 0 &&
            showEventDistribution && <RiskDistribution />}

          <CustomModal showModal={showModal}>
            <Filter
              closeModal={this.closeModal}
              handleDownloadExcel={this.handleDownloadExcel}
            />
          </CustomModal>
          <CustomModal showModal={showEventDetailsModal}>
            <div className="modal__eventDetails">
              <EventDetailsBox
                handleDownloadPdf={this.handleDownloadPdf}
                handleCloseModal={() => this.toggleEventDetails(false)}
              />
            </div>
          </CustomModal>
        </div>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    barclays: state.barclays,
    barclaysFilter: state.barclaysFilter,
    barclaysEventDetails: state.barclaysEventDetails,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Barclays);
