import React, { Component } from 'react';
import TopRiskListItem from './TopRiskListItem';
import './scss/style.scss';

import { connect } from 'react-redux';

class TopRiskList extends Component {
  renderTopRiskListItem = () => {
    const {
      dashboardData: { toprisksnew: toprisks },
    } = this.props.barclays;

    return toprisks.map((item, idx) => (
      <TopRiskListItem key={'_' + idx} item={item} />
    ));
  };
  render() {
    return (
      <div id="topRiskList">
        <div className="topRiskListItem--wrap">
          <h1>
            <i className="fas fa-sort-amount-up-alt"></i> &nbsp; Top 5 risk list{' '}
          </h1>
          <ol className="topRiskListItem--list">
            {this.renderTopRiskListItem()}
          </ol>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    barclays: state.barclays,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TopRiskList);
