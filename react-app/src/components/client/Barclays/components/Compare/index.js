import React, { Component } from 'react';
import './style.scss';
import TopMenu from '../../../../ui/TopMenu';
import clientLogo from '../../../../../components/client/Barclays/assests/barclays_w.png';
import LeftSide from './leftSide';
import RightSide from './rightSide';
import CustomModal from '../../../../ui/modal';
import FilterLeft from './filterLeft';
import FilterRight from './filterRight';

class Compare extends Component {
  state = {
    showModalLeft: false,
    showModalRight: false,
  };

  closeModalLeft = () => this.setState({ showModalLeft: false });

  handleOpenFilterLeft = () => this.setState({ showModalLeft: true });

  closeModalRight = () => this.setState({ showModalRight: false });

  handleOpenFilterRight = () => this.setState({ showModalRight: true });

  render() {
    const { showModalLeft, showModalRight } = this.state;
    return (
      <>
        <TopMenu clientLogo={clientLogo} showClientLogo={false} />
        <div className="compare__wrapper">
          <LeftSide handleOpenFilterLeft={this.handleOpenFilterLeft} />
          <RightSide handleOpenFilterRight={this.handleOpenFilterRight} />
        </div>
        <CustomModal showModal={showModalLeft}>
          <FilterLeft
            closeModalLeft={this.closeModalLeft}
            handleDownloadExcelLeft={this.handleDownloadExcelLeft}
          />
        </CustomModal>
        <CustomModal showModal={showModalRight}>
          <FilterRight
            closeModalRight={this.closeModalRight}
            handleDownloadExcelRight={this.handleDownloadExcelRight}
          />
        </CustomModal>
      </>
    );
  }
}

export default Compare;
