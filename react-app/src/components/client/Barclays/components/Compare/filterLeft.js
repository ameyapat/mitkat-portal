import React, { Component, Fragment } from 'react';
import './style.scss';
import { withCookies } from 'react-cookie';
import injectSheet from 'react-jss';
import style from './style.js';
import { connect } from 'react-redux';
import Select from 'react-select';
import CustomLabel from '../../../../ui/customLabel';
import MultiSelect from '../../../../ui/multiSelect';
import BarclaysTextField from '../../../../admin/components/barclays/components/barclaysTextField';
import BarclaysDatePicker from '../../../../admin/components/barclays/components/barclaysDatePicker';
import { generateObjectPair } from '../../../../admin/components/barclays/helpers/utils';
import {
  getCountryListBarclays,
  getCityListBarclays,
  getAssetListBarclays,
  getRiskLevelBarclays,
  getRiskCategoryBarclays,
  getDashboardInfoBarclays,
} from '../../../../../requestor/barclays/requestor';

import { filterArrObjects } from '../../../../../helpers/utils';
import {
  setDashboardDataLeft,
  updateFiltersLeft,
} from '../../../../../actions/barclaysActions';
import moment from 'moment';

const customStyles = {
  control: provided => ({
    ...provided,
    backgroundColor: 'var(--backgroundSolid) ',
    color: 'var(--whiteMediumEmp)',
  }),
  option: (provided, state) => ({
    ...provided,
    zIndex: '99999',
  }),
  placeholder: provided => ({
    ...provided,
    color: 'var(--whiteMediumEmp)',
  }),
  singleValue: provided => ({
    ...provided,
    color: 'var(--whiteMediumEmp)',
  }),
};

@injectSheet(style)
@withCookies
@connect(
  state => {
    return {
      barclays: state.barclaysCompare,
    };
  },
  dispatch => {
    return { dispatch };
  },
)
class FilterLeft extends Component {
  state = {
    startDate: new Date(),
    endDate: new Date(),
    selectedCountries: [],
    selectedCities: [],
    selectedAssets: [],
    cities: [],
    assets: [],
    countries: [],
    riskLevels: [],
    riskCategories: [],
    distance: 0,
  };

  componentDidMount() {
    getCountryListBarclays().then(countries =>
      this.setState({ countries: generateObjectPair('countries', countries) }),
    );
    getCityListBarclays().then(cities =>
      this.setState({ cities: generateObjectPair('cities', cities) }),
    );
    getAssetListBarclays().then(assets =>
      this.setState({ assets: generateObjectPair('assets', assets) }),
    );
    getRiskLevelBarclays().then(riskLevels => {
      riskLevels = generateObjectPair('riskLevels', riskLevels);

      this.setState({
        riskLevels,
      });
    });
    getRiskCategoryBarclays().then(riskCategories => {
      riskCategories = generateObjectPair('riskCategories', riskCategories);
      this.setState({
        riskCategories,
      });
    });
  }

  handleChange = (type, value) => {
    const { filtersLeft } = this.props.barclays;
    filtersLeft[type] = value;
    this.props.dispatch(updateFiltersLeft(filtersLeft));
  };

  handleApplyFilter = () => {
    const {
      startDate,
      endDate,
      selectedAssets,
      selectedCities,
      selectedCountries,
      selectedRiskLevel,
      selectedCategory,
      distance,
    } = this.props.barclays.filtersLeft;

    const reqData = {
      startdate: moment(startDate).format('YYYY-MM-DD'),
      enddate: moment(endDate).format('YYYY-MM-DD'),
      countries: filterArrObjects(selectedCountries),
      cities: filterArrObjects(selectedCities),
      assets: filterArrObjects(selectedAssets),
      risklevels: filterArrObjects(selectedRiskLevel),
      riskcaegories: filterArrObjects(selectedCategory),
      distance: parseInt(distance),
    };

    this.props.dispatch(setDashboardDataLeft({}));
    this.fetchDashboardData(reqData);
  };

  handleReset = () => {
    const { filtersLeft } = this.props.barclays;
    const startDate = moment()
      .startOf('year')
      .format('YYYY-DD-MM');
    const endDate = new Date();

    filtersLeft['selectedCountries'] = [];
    filtersLeft['selectedRiskLevel'] = [];
    filtersLeft['selectedCategory'] = [];
    filtersLeft['selectedAssets'] = [];
    filtersLeft['selectedCities'] = [];
    filtersLeft['startDate'] = startDate;
    filtersLeft['endDate'] = endDate;
    filtersLeft['distance'] = 0;

    this.props.dispatch(updateFiltersLeft(filtersLeft));
  };

  handleDownloadExcelLeft = () => {
    const {
      startDate,
      endDate,
      selectedAssets,
      selectedCities,
      selectedCountries,
      selectedRiskLevel,
      selectedCategory,
      distance,
    } = this.props.barclays.filtersLeft;

    const reqData = {
      startdate: moment(startDate).format('YYYY-MM-DD'),
      enddate: moment(endDate).format('YYYY-MM-DD'),
      countries: filterArrObjects(selectedCountries),
      cities: filterArrObjects(selectedCities),
      assets: filterArrObjects(selectedAssets),
      risklevels: filterArrObjects(selectedRiskLevel),
      riskcaegories: filterArrObjects(selectedCategory),
      distance: parseInt(distance),
    };
    this.props.handleDownloadExcelLeft(reqData);
  };

  fetchDashboardData = reqData => {
    const { dispatch } = this.props;
    getDashboardInfoBarclays(reqData).then(data => {
      dispatch(setDashboardDataLeft(data));
      this.handleClose();
    });
  };

  handleMultiSelect = (value, type) => {
    this.handleChange(type, value);
  };

  handleInputChange = (e, type) => {
    this.handleChange(type, e.target.value);
  };

  handleClose = () => this.props.closeModalLeft();

  render() {
    let { classes } = this.props;
    const {
      startDate,
      endDate,
      selectedCities,
      selectedAssets,
      selectedCountries,
      selectedCategory,
      selectedRiskLevel,
      distance,
    } = this.props.barclays.filtersLeft;
    let { cities, countries, assets, riskLevels, riskCategories } = this.state;

    return (
      <div id="barclaysFilter">
        <div className="barclaysFilter__head">
          <div className="barclaysFilter__head__inner">
            <h3 className="head--filters"> Event Filters Option 1</h3>
            <span className="btn-times" onClick={this.handleClose}>
              &times;
            </span>
          </div>
        </div>
        <div className="barclaysFilter__body">
          <div className="dateWrapper">
            <div className="date">
              <BarclaysDatePicker
                label={'Enter Start Date'}
                value={startDate}
                type={'startDate'}
                handleDateChange={this.handleChange}
                darkTheme={true}
                rootLabelLight={classes.rootLabelLight}
                rootInputLight={classes.rootInputLight}
                notchedOutlineLight={classes.notchedOutlineLight}
                underline={classes.underline}
              />
            </div>
            <div className="date">
              <BarclaysDatePicker
                label={'Enter End Date'}
                value={endDate}
                type={'endDate'}
                handleDateChange={this.handleChange}
                darkTheme={true}
                rootLabelLight={classes.rootLabelLight}
                rootInputLight={classes.rootInputLight}
                notchedOutlineLight={classes.notchedOutlineLight}
                underline={classes.underline}
              />
            </div>
          </div>
          <div className="twoColWrap multi--selects">
            <div className="col-2">
              <CustomLabel
                title="Countries"
                classes="select--label"
                isRequired={false}
              />
              <MultiSelect
                value={selectedCountries}
                placeholder={'Select Countries'}
                parentEventHandler={value =>
                  this.handleMultiSelect(value, 'selectedCountries')
                }
                options={countries}
                hasCustomStyles={true}
                customStyles={customStyles}
              />
            </div>
            <div className="col-2">
              <CustomLabel
                title="Cities"
                classes="select--label"
                isRequired={false}
              />
              <MultiSelect
                value={selectedCities}
                placeholder={'Select Cities'}
                parentEventHandler={value =>
                  this.handleMultiSelect(value, 'selectedCities')
                }
                options={cities}
                hasCustomStyles={true}
                customStyles={customStyles}
              />
            </div>
            <div className="col-2">
              <CustomLabel
                title="Assets"
                classes="select--label"
                isRequired={false}
              />
              <MultiSelect
                value={selectedAssets}
                placeholder={'Select Assets'}
                parentEventHandler={value =>
                  this.handleMultiSelect(value, 'selectedAssets')
                }
                options={assets}
                hasCustomStyles={true}
                customStyles={customStyles}
              />
            </div>
            <div>
              <BarclaysTextField
                id="distance"
                label="Distance"
                value={distance}
                type={'distance'}
                onChange={this.handleInputChange}
                defaultValue="Enter Distance"
                darkTheme={true}
                rootInput={classes.rootInput}
                rootLabel={classes.rootLabel}
                underline={classes.underline}
              />
            </div>
            <div className="twoColWrap single--selects">
              <div className="col-3">
                <CustomLabel
                  title="Severity Level"
                  classes="select--label"
                  isRequired={true}
                />
                <MultiSelect
                  value={selectedRiskLevel}
                  placeholder={'Severity Level'}
                  parentEventHandler={value =>
                    this.handleMultiSelect(value, 'selectedRiskLevel')
                  }
                  options={riskLevels}
                  hasCustomStyles={true}
                  customStyles={customStyles}
                />
              </div>
              <div className="col-3">
                <CustomLabel
                  title="Risk Category"
                  classes="select--label"
                  isRequired={true}
                />
                <MultiSelect
                  value={selectedCategory}
                  placeholder={'Risk Category'}
                  parentEventHandler={value =>
                    this.handleMultiSelect(value, 'selectedCategory')
                  }
                  options={riskCategories}
                  hasCustomStyles={true}
                  customStyles={customStyles}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="barclaysFilter__footer">
          <button className="btn btn--reset" onClick={this.handleReset}>
            Reset
          </button>
          <button className="btn btn--apply" onClick={this.handleApplyFilter}>
            Apply
          </button>
          <button
            className="btn btn--download"
            onClick={this.handleDownloadExcelLeft}
          >
            Download
          </button>
        </div>
      </div>
    );
  }
}

export default injectSheet(style)(FilterLeft);
