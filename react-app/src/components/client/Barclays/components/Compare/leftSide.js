import React, { Component } from 'react';
import { connect } from 'react-redux';
import { updateDashboardEventDetails } from '../../../../../actions/barclaysActions';
import { getDashEventDetailsBarclays } from '../../../../../requestor/barclays/requestor';
import CustomModal from '../../../../ui/modal';
import EventDetailsBox from './Left/EventDetailsBox';
import EventList from './Left/EventList';
import RiskCategoryDistribution from './Left/RiskCategoryDistribution';
import RiskDistribution from './Left/RiskDistribution';
import RiskLevelDistribution from './Left/RiskLevelDistribution';
import TopRiskList from './Left/TopRiskList';
import TopEvents from './Left/TotalEvents';

class LeftSide extends Component {
  state = {
    showEventDetailsModal: false,
  };

  getEventDetails = id => {
    getDashEventDetailsBarclays(id).then(data => {
      this.props.dispatch(updateDashboardEventDetails(data));
      this.toggleEventDetails(true);
    });
  };
  toggleEventDetails = show => this.setState({ showEventDetailsModal: show });

  render() {
    const {
      toprisks = [],
      riskcategorydata = [],
      riskleveldata = [],
      eventdistributiongraph = [],
      eventlist = [],
    } = this.props.barclays.dashboardDataLeft;
    const { showEventDetailsModal } = this.state;
    return (
      <>
        <div className="compare__box">
          <div className="compare__btn">
            <button
              className="btnEllipsis"
              onClick={this.props.handleOpenFilterLeft}
            >
              <i className="fas fa-filter"></i>
            </button>
          </div>
          {eventlist && eventlist.length > 0 && <TopEvents />}

          {riskleveldata &&
            riskleveldata.length > 0 &&
            eventdistributiongraph.length > 0 && <RiskDistribution />}
          {riskcategorydata && riskcategorydata.length > 0 && (
            <RiskCategoryDistribution />
          )}

          {toprisks && toprisks.length > 0 && <TopRiskList />}
          {riskleveldata && riskleveldata.length > 0 && (
            <RiskLevelDistribution />
          )}
          {eventlist && eventlist.length > 0 && (
            <EventList getEventDetails={this.getEventDetails} />
          )}
        </div>
        <CustomModal showModal={showEventDetailsModal}>
          <div className="modal__eventDetails">
            <EventDetailsBox
              handleDownloadPdf={this.handleDownloadPdf}
              handleCloseModal={() => this.toggleEventDetails(false)}
            />
          </div>
        </CustomModal>
      </>
    );
  }
}
const mapStateToProps = state => {
  return {
    barclays: state.barclaysCompare,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LeftSide);
