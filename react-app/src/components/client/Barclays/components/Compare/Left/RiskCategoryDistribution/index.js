import React from 'react';
import RiskCategories from './RiskCategories';
import './scss/style.scss';

const RiskCategoryDistribution = () => (
  <div id="riskCategoryDistribution">
    <RiskCategories title="Risk Category Distribution" />
  </div>
);

export default RiskCategoryDistribution;
