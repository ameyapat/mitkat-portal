import React, { Component } from 'react';
import DailyTotal from './DailyTotal';
import './scss/style.scss';

class RiskDistribution extends Component {
  render() {
    return (
      <div id="riskDistribution">
        <DailyTotal title="Event Distribution" />
      </div>
    );
  }
}

export default RiskDistribution;
