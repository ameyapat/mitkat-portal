import React, { Component } from 'react';
import './style.scss';

import { connect } from 'react-redux';

class TopEvents extends Component {
  render() {
    const {
      dashboardDataLeft: { totalevents },
    } = this.props.barclays;
    return (
      <div id="topEvents">
        <span className="totalEvents"> {totalevents} </span>
        <p>Total number of Events </p>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    barclays: state.barclaysCompare,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TopEvents);
