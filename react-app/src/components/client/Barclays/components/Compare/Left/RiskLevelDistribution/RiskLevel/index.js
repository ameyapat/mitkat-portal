import React, { Component } from 'react';
import '../scss/style.scss';
import { Doughnut } from 'react-chartjs-2';
import { connect } from 'react-redux';

const options = {
  elements: {
    arc: {
      borderWidth: 0,
    },
  },
  legend: {
    position: 'bottom',
  },
};

const dataSetObj = {
  maxBarThickness: 10,
  backgroundColor: [
    'rgba(224, 16, 89, 0.25)',
    'rgba(255, 125, 131, 0.25)',
    'rgba(255, 169, 89, 0.25)',
    'rgba(255, 220, 133, 0.25)',
    'rgba(33, 217, 174, 0.25)',
  ],
  hoverOffset: 1,
  borderColor: [
    'rgba(224, 16, 89, 1)',
    'rgba(255, 125, 131, 1)',
    'rgba(255, 169, 89, 1)',
    'rgba(255, 220, 133, 1)',
    'rgba(33, 217, 174, 1)',
  ],
  borderWidth: 2,
};

class RiskLevel extends Component {
  state = {
    metaData: {
      datasets: [],
    },
    width: 100,
    height: 45,
  };

  componentDidMount() {
    this.initRiskLevelData();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.barclays !== this.props.barclays) {
      this.initRiskLevelData();
    }
  }

  initRiskLevelData = () => {
    const { riskleveldata = [] } = this.props.barclays.dashboardDataLeft;
    const metaData = {
      datasets: [],
    };
    const labels = [];
    const data = [];

    riskleveldata.map(item => {
      labels.push(item.dataname);
      data.push(item.datavalue);
    });

    dataSetObj.data = data;
    metaData.labels = labels;
    metaData.datasets.push(dataSetObj);

    this.setState({ metaData });
  };

  render() {
    const { title = 'Risk Level Distribution' } = this.props;

    return (
      <div className="riskLevel--wrap">
        <h1>
          <i className="fas fa-chart-pie"></i>&nbsp;{title}
        </h1>
        <Doughnut
          data={this.state.metaData}
          options={options}
          width={this.state.width}
          height={this.state.height}
        />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    barclays: state.barclaysCompare,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RiskLevel);
