import React, { Component } from 'react';
import '../scss/style.scss';
import { Bar } from 'react-chartjs-2';
import { connect } from 'react-redux';

import {
  RISK_CATEGORY_COLORS,
  RISK_CATEGORY_COLORS_OPACITY,
} from '../../../../../helpers/constants';

const options = {
  scales: {
    xAxes: [
      {
        stacked: true,
      },
    ],
    yAxes: [
      {
        stacked: true,
      },
    ],
  },
  legend: {
    position: 'bottom',
    display: false,
  },
};

const dataSetObj = {
  maxBarThickness: 10,
  backgroundColor: [],
  hoverOffset: 4,
  borderColor: [],
  borderWidth: 2,
};

class RiskCategories extends Component {
  state = {
    metaData: {
      datasets: [],
    },
  };

  componentDidMount() {
    this.initRiskCategoryData();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.barclays !== this.props.barclays) {
      this.initRiskCategoryData();
    }
  }

  initRiskCategoryData = () => {
    const { riskcategorydata = [] } = this.props.barclays.dashboardDataLeft;
    const metaData = {
      datasets: [],
    };
    const labels = [];
    const data = [];

    riskcategorydata.map(item => {
      labels.push(item.dataname);
      data.push(item.datavalue);
      dataSetObj.backgroundColor.push(RISK_CATEGORY_COLORS_OPACITY[item.id]);
      dataSetObj.borderColor.push(RISK_CATEGORY_COLORS[item.id]);
    });

    dataSetObj.data = data;
    metaData.labels = labels;
    metaData.datasets.push(dataSetObj);

    this.setState({ metaData });
  };

  render() {
    const { title = 'Risk Category Distribution' } = this.props;

    return (
      <div className="RiskCategories--wrap">
        <h1>
          <i className="fas fa-chart-bar"></i>&nbsp; {title}
        </h1>
        <Bar
          data={this.state.metaData}
          options={options}
          width={100}
          height={45}
        />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    barclays: state.barclaysCompare,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RiskCategories);
