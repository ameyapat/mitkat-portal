import React, { Component } from 'react';
import '../scss/style.scss';

import {
  RISK_CATEGORY_COLORS,
  RISK_CATEGORY_NAME,
} from '../../../../../helpers/constants';

import { toggleRiskTrackerBox } from '../../../../../../../../actions';

import { connect } from 'react-redux';

class EventListItem extends Component {
  render() {
    const {
      id,
      title,
      risklevel,
      riskcategory,
      timestamp,
      getEventDetails,
      lat,
      lng,
      dispatch,
    } = this.props;
    return (
      <li
        className="eventlist--item"
        onClick={() => getEventDetails(id)}
        onMouseOver={() =>
          dispatch(
            toggleRiskTrackerBox({
              riskTrackerDetails: { lat, lng, show: true },
            }),
          )
        }
        onMouseOut={() =>
          dispatch(
            toggleRiskTrackerBox({
              riskTrackerDetails: { lat: '', lng: '', show: false },
            }),
          )
        }
      >
        <div>
          <h1 className="eventlist--title">{title}</h1>
          <span
            className="eventlist--item__pills medium"
            style={{ backgroundColor: RISK_CATEGORY_COLORS[riskcategory] }}
          >
            {RISK_CATEGORY_NAME[riskcategory]}
          </span>
          <div className="eventlist--datetime">
            <i class="far fa-calendar-alt"></i> &nbsp;{timestamp}
          </div>
        </div>
      </li>
    );
  }
}

const mapStateToProps = state => {
  return {
    barclays: state.barclaysCompare,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EventListItem);
