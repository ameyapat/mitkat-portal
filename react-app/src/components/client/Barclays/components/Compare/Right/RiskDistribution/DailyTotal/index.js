import React, { Component } from 'react';
import '../scss/style.scss';
import { Line } from 'react-chartjs-2';
import { connect } from 'react-redux';

const options = {
  maintainAspectRatio: true,
  legend: {
    display: false,
    position: 'bottom',
  },
};

const dataSetObj = {
  maxBarThickness: 20,
  label: 'Event Distribution',
  backgroundColor: 'rgba(255, 164, 100, 0.15)',
  hoverOffset: 4,
  borderColor: 'rgba(255, 164, 100, 1)',
  borderWidth: 2,
};

class DailyTotal extends Component {
  state = {
    metaData: {
      datasets: [],
    },
  };

  componentDidMount() {
    this.initRiskDistributionData();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.barclays !== this.props.barclays) {
      this.initRiskDistributionData();
    }
  }

  initRiskDistributionData = () => {
    const {
      eventdistributiongraph = [],
    } = this.props.barclays.dashboardDataRight;
    const metaData = {
      datasets: [],
    };
    const labels = [];
    const data = [];

    eventdistributiongraph.map(item => {
      labels.push(item.xaxisdatapoint);
      data.push(item.yaxisdatapoint);
    });

    dataSetObj.data = data;
    metaData.labels = labels;
    metaData.datasets.push(dataSetObj);

    this.setState({ metaData });
  };

  render() {
    const { title = 'Event Distribution' } = this.props;
    return (
      <div className="dailyTotal--wrap">
        <h1>
          <i className="fas fa-chart-area"></i>&nbsp;{title}
        </h1>
        <Line
          data={this.state.metaData}
          width={110}
          height={25}
          options={options}
        />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    barclays: state.barclaysCompare,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DailyTotal);
