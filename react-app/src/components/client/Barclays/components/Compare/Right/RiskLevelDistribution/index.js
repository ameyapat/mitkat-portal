import React, { Component } from 'react';
import RiskLevel from './RiskLevel';
import './scss/style.scss';

class RiskLevelDistribution extends Component {
  render() {
    return (
      <div id="riskLevelDistribution">
        <RiskLevel title="Severity Level" />
      </div>
    );
  }
}

export default RiskLevelDistribution;
