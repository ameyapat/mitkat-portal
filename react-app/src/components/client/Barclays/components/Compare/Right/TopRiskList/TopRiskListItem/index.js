import React, { Component } from 'react';
import '../scss/style.scss';

import { RISK_CATEGORY_COLORS } from '../../../../../helpers/constants';
class TopRiskListItem extends Component {
  render() {
    const { riskName, riskcategoryid } = this.props.item;

    return (
      <li className="topRiskListItem--item" style={{ paddingLeft: 0 }}>
        <span
          className="indicators"
          style={{ backgroundColor: RISK_CATEGORY_COLORS[riskcategoryid] }}
        ></span>
        {riskName}
      </li>
    );
  }
}

export default TopRiskListItem;
