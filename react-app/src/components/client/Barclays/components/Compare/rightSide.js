import React, { Component } from 'react';
import { connect } from 'react-redux';
import { updateDashboardEventDetails } from '../../../../../actions/barclaysActions';
import { getDashEventDetailsBarclays } from '../../../../../requestor/barclays/requestor';
import CustomModal from '../../../../ui/modal';
import EventDetailsBox from '../EventDetailsBox';
import EventList from './Right/EventList';
import RiskCategoryDistribution from './Right/RiskCategoryDistribution';
import RiskDistribution from './Right/RiskDistribution';
import RiskLevelDistribution from './Right/RiskLevelDistribution';
import TopRiskList from './Right/TopRiskList';
import TopEvents from './Right/TotalEvents';

class RightSide extends Component {
  state = {
    showEventDetailsModal: false,
  };

  getEventDetails = id => {
    getDashEventDetailsBarclays(id).then(data => {
      this.props.dispatch(updateDashboardEventDetails(data));
      this.toggleEventDetails(true);
    });
  };
  toggleEventDetails = show => this.setState({ showEventDetailsModal: show });

  render() {
    const {
      toprisks = [],
      riskcategorydata = [],
      riskleveldata = [],
      eventdistributiongraph = [],
      eventlist = [],
    } = this.props.barclays.dashboardDataRight;
    const { showEventDetailsModal } = this.state;

    return (
      <>
        <div className="compare__box">
          <div className="compare__btn">
            <button
              className="btnEllipsis float-right"
              onClick={this.props.handleOpenFilterRight}
            >
              <i className="fas fa-filter"></i>
            </button>
          </div>
          {eventlist && eventlist.length > 0 && <TopEvents />}

          {riskleveldata &&
            riskleveldata.length > 0 &&
            eventdistributiongraph.length > 0 && <RiskDistribution />}
          {riskcategorydata && riskcategorydata.length > 0 && (
            <RiskCategoryDistribution />
          )}

          {toprisks && toprisks.length > 0 && <TopRiskList />}
          {riskleveldata && riskleveldata.length > 0 && (
            <RiskLevelDistribution />
          )}
          {eventlist && eventlist.length > 0 && (
            <EventList getEventDetails={this.getEventDetails} />
          )}
        </div>
        <CustomModal showModal={showEventDetailsModal}>
          <div className="modal__eventDetails">
            <EventDetailsBox
              handleDownloadPdf={this.handleDownloadPdf}
              handleCloseModal={() => this.toggleEventDetails(false)}
            />
          </div>
        </CustomModal>
      </>
    );
  }
}
const mapStateToProps = state => {
  return {
    barclays: state.barclaysCompare,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RightSide);
