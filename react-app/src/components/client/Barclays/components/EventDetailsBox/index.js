import React, { Component } from 'react';
import clsx from 'clsx';
import injectSheet from 'react-jss/lib/injectSheet';
import styles from './style';
import { connect } from 'react-redux';
import { RISK_CATEGORY_COLORS } from '../../helpers/constants';
import './style.scss';

@injectSheet(styles)
@connect(state => {
  return {
    barclays: state.barclaysEventDetails,
  };
})
class EventDetailsBox extends Component {
  render() {
    const {
      classes,
      barclays: {
        eventDetails: {
          id = 36,
          subject,
          riskCategoryName,
          riskCategoryId,
          riskLevelId,
          cities,
          countries,
          eventDate,
          validityDate,
          bluf,
          impact,
          action,
          sources,
          imageURL,
        },
      },
      handleDownloadPdf,
      handleCloseModal,
    } = this.props;

    return (
      <>
        <div className="modal--header">
          <span className="btn-times" onClick={handleCloseModal}>
            &times;
          </span>
        </div>

        <div className="modal--body">
          <div className="event__detail">
            <div className="title__wrap">
              <h2>{subject}</h2>
            </div>
            <div className="event--location">
              <div className="event">
                <p>
                  <i className="fas fa-map-marked-alt"></i> &nbsp; {cities}
                  {cities ? ',' : null} {countries}
                </p>
              </div>
            </div>
            <div className="event--detail">
              <div className="event">
                <span
                  className="pill"
                  style={{
                    backgroundColor: `${RISK_CATEGORY_COLORS[riskCategoryId]}`,
                    border: 'none',
                    color: 'rgba(0,0,0,0.85)',
                  }}
                >
                  {riskCategoryName}
                </span>
              </div>
            </div>
            <div className="event--detail">
              <div className="event">
                <p className="date">
                  <i className="fas fa-calendar-alt"></i> &nbsp; {eventDate}{' '}
                  {eventDate === validityDate ? '' : 'to' + ' ' + validityDate}
                </p>
              </div>
            </div>
            <div className="event--detail event--data">
              <div className="event">
                <label>Bluf</label>
                <p className="pre-line">{bluf}</p>
              </div>
              <div className="event">
                <label>Impact</label>
                <p className="pre-line">{impact}</p>
              </div>
              <div className="event">
                <label>Action</label>
                <p className="pre-line">{action}</p>
              </div>
              <div className="event">
                <label>Sources</label>
                <p className="pre-line">{sources}</p>
              </div>
              {imageURL ? (
                <div className="event">
                  <img src={imageURL} alt={subject} />
                </div>
              ) : null}
            </div>
          </div>
        </div>
        <div className="modal--footer">
          <button className="pdf--btn" onClick={() => handleDownloadPdf(id)}>
            <i className="fas fa-file-pdf"></i> &nbsp; Download Pdf
          </button>
        </div>
      </>
    );
  }
}
export default EventDetailsBox;
