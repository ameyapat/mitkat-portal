import React, { Component } from 'react';
import EventListItem from './EventListItem';
import './scss/style.scss';

import { connect } from 'react-redux';

class EventList extends Component {
  renderEventListItem = () => {
    const {
      dashboardData: { eventlist },
    } = this.props.barclays;

    return eventlist.map(item => (
      <EventListItem
        key={item.id}
        id={item.id}
        title={item.title}
        risklevel={item.risklevel}
        riskcategory={item.riskcategory}
        timestamp={item.timestamp}
        getEventDetails={this.props.getEventDetails}
        lat={item.latitude}
        lng={item.longitude}
      />
    ));
  };

  render() { 
    const {
      dashboardData: { totalevents },
    } = this.props.barclays;
    return (
      <div id="eventList">  
        <div className="topEvents"> 
          <span className="totalEvents"> {totalevents} </span> 
          <p>Total number of Events </p> 
        </div>
        <ul className="eventList--wrap">{this.renderEventListItem()}</ul>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    barclays: state.barclays,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EventList);
