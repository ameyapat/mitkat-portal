export default {
  rootLabel: {
    color: 'rgb(255,255,255) !important',
  },
  rootInput: {
    color: 'rgb(255,255,255) !important',
  },
  underline: {
    '&:before': {
      borderBottom: '2px solid #f2f2f2 !important',
    },
    '&:after': {
      borderBottom: '2px solid #f2f2f2 !important',
    },
  },
  rootLabelLight: {
    color: 'rgba(255,255,255, 0.67) !important',
  },
  rootInputLight: {
    color: 'var(--whiteHighEmp) !important',
  },
  notchedOutlineLight: {
    borderColor: '#eee !important',
  },
};
