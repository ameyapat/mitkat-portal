export const RISK_LEVEL_COLORS = {
  1: '#4C9A2A', //verylow
  2: '#76BA1B', //low
  3: '#D4772A', //medium
  4: '#E0AE15', //high
  5: '#C63030', //veryhigh
};

export const RISK_CATEGORY_ICONS = {
  1: 'icon-uniE9C7',
  2: 'icon-uniE9BF',
  3: 'icon-uniE9D6',
  4: 'icon-uniE9EC',
  5: 'icon-uniE9F8',
  6: 'icon-uniE95B',
  7: 'icon-uniE9C5',
};

export const RISK_CATEGORY_NAME = {
  1: 'Theft & Burglary', //
  2: 'Vandalism', //
  3: 'Extremism', //
  4: 'Natural Disasters', //
  5: 'Assault', //
  6: 'Fire Incidents', //
  7: 'Adverse Weather', //
  8: 'Civil Disruption', //
  9: 'Regulatory', //
  10: 'Health', //
  11: 'Technical Incident', //
};

export const RISK_CATEGORY_COLORS = {
  1: '#F57629',
  2: '#EC49BC',
  3: '#334FC9',
  4: '#45D4D9',
  5: 'var(--darkActive)',
  6: '#e01059',
  7: '#B753BC',
  8: '#FE4D80',
  9: '#FFA95A',
  10: '#63bd23',
  11: '#00b1ea',
};

export const RISK_CATEGORY_COLORS_OPACITY = {
  1: 'rgba(245, 118, 41, 0.25)',
  2: 'rgba(236, 73, 188, 0.25)',
  3: 'rgba(51, 79, 201, 0.25)',
  4: 'rgba(69, 212, 217, 0.25)',
  5: 'rgba(171, 193, 66, 0.25)',
  6: 'rgba(224, 16, 89, 0.25)',
  7: 'rgba(183, 83, 188, 0.25)',
  8: 'rgba(254, 77, 128, 0.25)',
  9: 'rgba(255, 169, 90, 0.25)',
  10: 'rgba(99, 189, 35, 0.25)',
  11: 'rgba(0, 177, 234, 0.25)',
};

export const RISK_CATEGORIES_NEON = {
  1: '#FFDC34', // Cyber
  2: '#C5005E', // Insurgency / Terrorism
  3: '#7FFBFF', // Civil Disturbance
  4: '#23CD98', // Environment
  5: '#EEA672', // Health
  6: '#B07AFB', // Critical Infrastructure
  7: '#4BB2E7', // Crime
};

export const RISK_LEVEL_COLORS_CHART = {
  'Level 1': '#C63030 ', //verylow
  'Level 2': '#E0AE15 ', //low
  'Level 3': '#D4772A', //medium
  'Level 4': '#76BA1B', //high
  'Level 5': '#4C9A2A', //veryhigh
};
