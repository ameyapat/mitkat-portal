import { fetchApi } from '../../../helpers/http/fetch';
import { API_ROUTES } from '../../../helpers/http/apiRoutes';
import Cookies from 'universal-cookie';

export const getEventCountries = authToken => {
  let reqObj = {
    isAuth: true,
    authToken,
    method: 'GET',
    url: API_ROUTES.getEventCountryList,
    showToggle: true,
  };

  return new Promise((res, rej) => {
    fetchApi(reqObj)
      .then(response => {
        res(response);
      })
      .catch(e => rej(e));
  });
};

export const getCountryEventDetails = (authToken, countryId) => {
  let reqObj = {
    isAuth: true,
    authToken,
    method: 'GET',
    url: API_ROUTES.getCountryEventDetails + '/' + countryId,
    showToggle: true,
  };

  return new Promise((res, rej) => {
    fetchApi(reqObj)
      .then(response => {
        res(response);
      })
      .catch(e => rej(e));
  });
};

export const getWeeklyDetails = regionId => {
  const cookies = new Cookies();
  const authToken = cookies.get('authToken');

  let reqObj = {
    isAuth: true,
    authToken,
    method: 'GET',
    url: API_ROUTES.getWeekly + '/' + regionId,
    showToggle: true,
  };

  return new Promise((res, rej) => {
    fetchApi(reqObj)
      .then(response => {
        res(response);
      })
      .catch(e => rej(e));
  });
};

export const getWeeklyEventDetails = eventId => {
  const cookies = new Cookies();
  const authToken = cookies.get('authToken');

  let reqObj = {
    isAuth: true,
    authToken,
    method: 'GET',
    url: API_ROUTES.getWeeklyEvents + '/' + eventId,
    showToggle: true,
  };

  return new Promise((res, rej) => {
    fetchApi(reqObj)
      .then(response => {
        res(response);
      })
      .catch(e => rej(e));
  });
};

export const normalizeWatchList = watchList => {
  return new Promise((res, rej) => {
    const normalized = watchList.reduce((acc, item) => {
      return {
        ...acc,
        [item.id]: item,
      };
    }, {});

    res(normalized);
  });
};

export const getLocationValue = value => {
  let geoFencedLocation = value.reduce(function(prev, curr) {
    return prev.distance < curr.distance ? prev : curr;
  });
  return geoFencedLocation;
};
