export default {
  textField: {
    marginBottom: '10px',
    color: '#f1f1f1',
  },
  underline: {
    borderBottom: '1px solid rgba(255,255,255,0.67)',
    '&:after': {
      borderBottom: '2px solid #f1f1f1',
    },
  },
  cssLabel: {
    color: '#f1f1f1 !important',
  },
  cssLabelRoot: {
    color: 'rgba(255,255,255, 0.67)',
  },
};
