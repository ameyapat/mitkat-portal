const validationRules = [
    {
        field: 'password',
        method: 'isEmpty',
        validWhen: false,
        message: 'Password is required'
    },
    {
        field: 'username',
        method: 'isEmpty',
        validWhen: false,
        message: 'Username is required'
    }
]

export default validationRules