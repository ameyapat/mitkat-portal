import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';
import style from './style';
import { withRouter } from 'react-router-dom';
//
import store from '../../store';
import {
  setAuthToken,
  toggleToast,
  setUserRoles,
  setPartnerDetails,
} from '../../actions';
//helpers http
import { fetchApi } from '../../helpers/http/fetch';
import { API_ROUTES } from '../../helpers/http/apiRoutes';
import { withCookies } from 'react-cookie';
import FormValidator from '../../helpers/validation/FormValidator';
import validationRules from './validationRules';
//assets
import mitkatLogo from '../../assets/MitKat_new_logo.png';
import blackPearl from '../../assets/partners/BlackPearl.png';
import sanctuaryRiskLogo from '../../assets/partners/SanctuaryRisk.png';
//
import ReCAPTCHA from 'react-google-recaptcha';
import { getPartnerDetails } from '../../requestor/login/requestor';
import { getSubscriptions } from '../../requestor/mitkatWeb/requestor';
import { setAppPermissions } from '../../actions';
import { themeChangeToggle } from '../../actions/themeAction';

const SITE_KEY = '6Ld4D9AbAAAAACHOjlEVVuMYaWoGFlal2jBxX_VD';

@withCookies
class LoginBox extends Component {
  validator = new FormValidator(validationRules);
  submitted = false;

  state = {
    username: '',
    password: '',
    validation: this.validator.valid(),
  };

  handleChange = (type, value) => this.setState({ [type]: value });

  handleSubmit = e => {
    e.preventDefault();
    const validation = this.validator.validate(this.state);
    const { token } = this.state;
    this.setState({ validation });
    this.submitted = true;
    const { history, hasParentHandler, parentHandler } = this.props;
    {
      /* Removing Captcha - Token for Few Days for SBILife Client */
    }
    if (validation.isValid) {
      let reqObj = {
        usernameOrEmail: this.state.username,
        password: this.state.password,
      };

      if (hasParentHandler) {
        parentHandler(reqObj);
      } else {
        let params = {
          url: API_ROUTES.signInAdmin,
          data: reqObj,
          method: 'POST',
          showToggle: true,
        };

        fetchApi(params)
          .then(res => {
            store.dispatch(setAuthToken(res.accessToken));
            if (res.accessToken) {
              Promise.all([
                this.getUserRoles(res.accessToken),
                this.getUserId(res.accessToken),
                getPartnerDetails(res.accessToken),
                getSubscriptions(res.accessToken),
              ])
                .then(values => {
                  const userRole = values[0];
                  const userId = values[1];
                  const partnerDetails = values[2];
                  const firstViewId = values[3].firstViewId;
                  const appPermissions = values[3];

                  if (
                    userRole[0].name === 'ROLE_CLIENT' ||
                    userRole[0].name === 'ROLE_CLIENT_USER'
                  ) {
                    if (userId && userId === 103) {
                      this.props.history.push(`/client/godrej`);
                    } else if (userId && userId === 185) {
                      this.props.history.push(`/client/pestle`);
                    } else if (userId && userId === 166) {
                      this.props.history.push(`/client/godrejAdmin`);
                    } else if (userId && userId === 273) {
                      this.props.history.push(`/barclays/admin`);
                    } else if (userId && userId === 274) {
                      this.props.history.push(`/barclays/dashboard`);
                    } else if (userId && userId === 279) {
                      this.props.history.push(`/demo/dashboard`);
                    } else {
                      if (history.location && history.location.state) {
                        history.push(history.location.state.redirectTo);
                      } else {
                        switch (firstViewId) {
                          case 1:
                            history.push(`/client/dashboard`);
                            break;
                          case 2:
                            history.push(`/client/rime`);
                            break;
                          case 3:
                            history.push(`/client/riskExposure`);
                            break;
                          case 4:
                            history.push(`/covid-19`);
                            break;
                          default:
                            history.push(`/client/dashboard`);
                            break;
                        }
                      }
                    }
                  } else if (userRole[0].name === 'ROLE_ADMIN') {
                    this.props.history.push(`/admin/dashboard/client`);
                  } else if (userRole[0].name === 'ROLE_APPROVER') {
                    this.props.history.push(`admin/dashboard/partnerAlerts`);
                  } else if (userRole[0].name === 'ROLE_CREATOR') {
                    this.props.history.push(`admin/dashboard/dailyRiskTracker`);
                  } else if (userRole[0].name === 'ROLE_PARTNER_ADMIN') {
                    this.props.history.push(`admin/dashboard/client`);
                  } else if (userRole[0].name === 'ROLE_PARTNER_APPROVER') {
                    this.props.history.push(`admin/dashboard/partnerAlerts`);
                  } else if (userRole[0].name === 'ROLE_PARTNER_CREATOR') {
                    this.props.history.push(`admin/dashboard/dailyRiskTracker`);
                  } else {
                    store.dispatch(
                      toggleToast({
                        toastMsg: 'Invalid Username or password!',
                        showToast: true,
                      }),
                    );
                  }
                  store.dispatch(setPartnerDetails(partnerDetails));
                  store.dispatch(setAppPermissions(appPermissions));
                  store.dispatch(
                    themeChangeToggle(
                      appPermissions.colorThemeSetting ? false : true,
                    ),
                  );
                })
                .catch(e => {
                  store.dispatch(
                    toggleToast({
                      toastMsg: 'Something went wrong!',
                      showToast: true,
                    }),
                  );
                });

              this.setCovidDashAuth();
            } else {
              store.dispatch(
                toggleToast({
                  toastMsg: 'Invalid Username or password!',
                  showToast: true,
                }),
              );
            }
          })
          .catch(e => {
            store.dispatch(
              toggleToast({
                toastMsg: 'Something went wrong!',
                showToast: true,
              }),
            );
          });
      }
    } else {
      alert('Please enter a valid username/password');
    }
  };

  setCovidDashAuth = () => {
    let d = new Date();
    d.setTime(d.getTime() + 24 * 60 * 60 * 1000);

    this.props.cookies.set('isCovidDashAuth', true, {
      path: '/',
      expires: d,
    });
  };

  getUserId = token => {
    return new Promise((res, rej) => {
      let reqObj = {
        isAuth: true,
        authToken: token,
        url: API_ROUTES.getUserId,
        method: 'POST',
        showToggle: true,
      };

      fetchApi(reqObj)
        .then(data => {
          let d = new Date();
          d.setTime(d.getTime() + 24 * 60 * 60 * 1000);

          this.props.cookies.set('authUserId', data, {
            path: '/',
            expires: d,
          });
          res(data);
        })
        .catch(e => rej(e));
    });
  };

  getUserRoles = token => {
    return new Promise((res, rej) => {
      let reqObj = {
        isAuth: true,
        authToken: token,
        url: API_ROUTES.getUserRole,
        method: 'POST',
        showToggle: true,
      };
      fetchApi(reqObj)
        .then(data => {
          if (data) {
            let d = new Date();
            d.setTime(d.getTime() + 24 * 60 * 60 * 1000);

            this.props.cookies.set('authToken', token, {
              path: '/',
              expires: d,
            });

            this.props.cookies.set('userRole', data[0].name, {
              path: '/',
              expires: d,
            });

            store.dispatch(setUserRoles(data[0].name));
            res(data);
          }
        })
        .catch(e => rej(e));
    });
  };

  handleCaptcha = value => {
    this.setState({ token: value });
  };

  render() {
    let {
      classes,
      hasParentHandler = false,
      parentHandler,
      headerTitle = 'Risk Intel Tracker Platform',
      showDefaultFooter = true,
    } = this.props;
    let validation = this.submitted
      ? this.validator.validate(this.state)
      : this.state.validation;

    return (
      <>
        <div className="partner__wrap">
          {showDefaultFooter ? (
            <p>
              {headerTitle} is an interactive platform that is updated on
              real-time basis with curated and analysed information on events of
              critical importance to businesses.
              <br />
              To know more visit our
              <a href="https://www.mitkatadvisory.com/" target="_blank">
                MitKat Website
              </a>
              or
              <a href="https://mitkatadvisory.com/EnquiryForm" target="_blank">
                Book a demo
              </a>
              now.
            </p>
          ) : (
            <p>
              <a href="https://www.mitkatadvisory.com/contact"> Click here </a>
              to register for Login Credentials.
            </p>
          )}
          <br /> <br />
          <p className="light-text">Integrated Intelligence Solution By:</p>
          <div className="partner__box">
            <img src={mitkatLogo} alt="mitkat" />
            <img src={blackPearl} alt="BlackPearl" />
            <img
              src={sanctuaryRiskLogo}
              alt="Sanctuary Risk"
              className="partner__logo__background"
            />
          </div>
        </div>
        <form className={`loginBox`} onSubmit={this.handleSubmit}>
          <div className="loginWrap">
            <h1> {headerTitle} </h1>

            <TextField
              error={validation.username.isInvalid}
              id="username"
              label="Username"
              fullWidth
              autoComplete="new-password"
              InputLabelProps={{
                classes: {
                  focused: classes.cssLabel,
                  root: classes.cssLabelRoot,
                },
              }}
              InputProps={{
                classes: {
                  root: classes.textField,
                  underline: classes.underline,
                },
              }}
              inputProps={{
                autocomplete: 'new-password',
                form: {
                  autocomplete: 'off',
                },
              }}
              value={this.state.username}
              onChange={e => this.handleChange('username', e.target.value)}
            />
            <TextField
              error={validation.password.isInvalid}
              id="password"
              type="password"
              label="Password"
              fullWidth
              InputLabelProps={{
                classes: {
                  focused: classes.cssLabel,
                  root: classes.cssLabelRoot,
                },
              }}
              InputProps={{
                classes: {
                  root: classes.textField,
                  underline: classes.underline,
                },
              }}
              value={this.state.password}
              onChange={e => this.handleChange('password', e.target.value)}
            />
            {/* Removing Captcha for Few Days for SBILife Client */}
            {/* <div className="reCAPTCHA">
              <ReCAPTCHA
                sitekey={SITE_KEY}
                onChange={this.handleCaptcha}
                onExpired={this.handleCaptcha}
              />
            </div> */}
            <div className="loginOptions">
              <button className="solidBtn" onClick={this.handleSubmit}>
                Login
              </button>
            </div>
            <div className="subline-text">
              {' '}
              <p>Powered by </p> <img src={mitkatLogo} alt="mitkat" />{' '}
            </div>
          </div>
        </form>
      </>
    );
  }
}

export default withRouter(withStyles(style)(LoginBox));
