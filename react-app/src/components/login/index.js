import React, { Component } from 'react';
import './style.scss';
//component
import LoginBox from './LoginBox';

class Login extends Component {
  render() {
    return (
      <div id="login" className="loginWrap">
        <div className="loginWrapper">
          <LoginBox />
        </div>
      </div>
    );
  }
}

export default Login;
