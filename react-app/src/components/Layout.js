import React, { Component, Fragment } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
//components
import Login from './login';
import Admin from './admin';
import Client from './client';
import Loader from './ui/loader';
import ErrorComponent from './ui/error';
import BizViewMap from './client/components/bizViewMap';
import HeatMap from './client/components/heatMap';
import Godrej from './client/pages/Godrej';
import SessionPage from './client/pages/Session';
import GodrejAdmin from './client/pages/GodrejAdmin';

import Dashboard from './client/pages/Covid/Dashboard/Dashboard';
import Reset from './client/pages/Reset/Reset';
import Vaccination from './client/pages/Covid/Vaccination/Vaccination';
import BarclaysAdminPanel from './admin/pages/BarclaysAdminPanel';
import BarclaysClientDashboard from './client/pages/BarclaysClientDashboard';

//test component
import Test from './Test';
//components @material-ui
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import store from '../store';
import { toggleToast, restrictAccess } from '../actions';
import { themeChangeToggle } from '../actions/themeAction';
import { withCookies } from 'react-cookie';
import CovidMap from './client/pages/CovidMap';

import isMobile from 'ismobilejs';
import Compare from './client/Barclays/components/Compare';
import EmployeeTracking from './client/components/employeeTracking';
import RiskExposureDashboard from './client/components/riskExposureDashboard';
import RimeDashboard from './client/components/rimeDashboard';
import ModalWithPortal from './ui/modalWithPortal/ModalWithPortal';
import CustomModal from './ui/modal';
import SubscribeBox from './ui/subscribeBox';
import { getSubscriptions } from '../requestor/mitkatWeb/requestor';

@withCookies
@connect(state => {
  return {
    appState: state.appState,
  };
})
@withRouter
class Layout extends Component {
  componentDidMount() {
    const isMobileDevice = window.navigator && isMobile(window.navigator);
    const d = new Date();
    const authToken = this.props.cookies.get('authToken');
    d.setTime(d.getTime() + 24 * 60 * 60 * 1000);

    this.props.cookies.set('isMobile', isMobileDevice.phone, {
      path: '/',
      expires: d,
    });
    if (authToken) {
      getSubscriptions(authToken).then(data => {
        this.props.dispatch(
          themeChangeToggle(data.colorThemeSetting ? false : true),
        );
      });
    }
  }

  handleClose = () => {
    store.dispatch(toggleToast({ showToast: false, toastMsg: '' }));
  };

  toggleSubscribeModal = access => {
    this.props.dispatch(restrictAccess(access));
  };

  render() {
    let {
      showToast,
      toastMsg,
      isFetching,
      restrictAccess,
    } = this.props.appState;
    const { cookies } = this.props;
    return (
      <Fragment>
        <Switch>
          <Route exact path="/" component={Login} />
          <Route exact path="/covid-19" component={Dashboard} />
          <Route path="/admin/dashboard" component={Admin} />

          <Route path="/client/dashboard" component={Client} />

          <Route
            path="/client/dashboard/:eventId"
            component={EmployeeTracking}
          />
          <Route path="/client/corona-dashboard" component={Dashboard} />
          <Route exact path="/covid-19/vaccination" component={Vaccination} />
          <Route
            path="/client/godrej"
            render={props =>
              parseInt(cookies.get('authUserId')) === 103 ? (
                <Godrej />
              ) : (
                <Redirect to="/" />
              )
            }
          />
          <Route path="/covid-19-map" render={props => <CovidMap />} />
          <Route
            path="/client/pestle"
            render={props =>
              parseInt(cookies.get('authUserId')) === 185 ? (
                <Godrej />
              ) : (
                <Redirect to="/" />
              )
            }
          />
          <Route
            path="/client/godrejAdmin"
            render={props =>
              parseInt(cookies.get('authUserId')) === 166 ? (
                <GodrejAdmin />
              ) : (
                <Redirect to="/" />
              )
            }
          />
          <Route
            path="/barclays/admin"
            render={props =>
              parseInt(cookies.get('authUserId')) === 273 ||
              parseInt(cookies.get('authUserId')) === 274 ? (
                <BarclaysAdminPanel {...props} />
              ) : (
                <Redirect to="/" />
              )
            }
          />
          <Route
            path="/barclays/dashboard/compare"
            exact
            render={props =>
              parseInt(cookies.get('authUserId')) === 274 ||
              parseInt(cookies.get('authUserId')) === 279 ? (
                <Compare {...props} />
              ) : (
                <Redirect to="/" />
              )
            }
          />
          <Route
            path="/barclays/dashboard"
            render={props =>
              parseInt(cookies.get('authUserId')) === 274 ? (
                <BarclaysClientDashboard {...props} showClientLogo={false} />
              ) : (
                <Redirect to="/" />
              )
            }
          />

          <Route
            path="/demo/dashboard"
            render={props =>
              parseInt(cookies.get('authUserId')) === 279 ? (
                <BarclaysClientDashboard {...props} showClientLogo={false} />
              ) : (
                <Redirect to="/" />
              )
            }
          />
          <Route path="/client/businessView" component={BizViewMap} />
          <Route exact path="/client/heatMap" component={HeatMap} />
          <Route
            path="/client/rime"
            render={props =>
              cookies.get('authToken') ? <RimeDashboard /> : <Redirect to="/" />
            }
          />
          <Route
            path="/client/riskExposure"
            component={RiskExposureDashboard}
          />
          <Route
            path="/reset"
            render={() =>
              cookies.get('authToken') ? <Reset /> : <Redirect to="/" />
            }
          />

          <Route path="/end-of-session" component={SessionPage} />
          <Route path="/mitkat-test" component={Test} />
          <Route path="/*" component={ErrorComponent} />
        </Switch>
        {isFetching && <Loader />}
        {showToast && (
          <Snackbar
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'left',
            }}
            open={showToast}
            autoHideDuration={4000}
            onClose={this.handleClose}
            ContentProps={{
              'aria-describedby': 'message-id',
            }}
            message={<span id="message-id">{toastMsg}</span>}
            action={[
              <IconButton
                key="close"
                aria-label="Close"
                color="inherit"
                onClick={this.handleClose}
              >
                &times;
              </IconButton>,
            ]}
          />
        )}
        <ModalWithPortal>
          <CustomModal showModal={restrictAccess}>
            <div className="subscribeBoxWrap">
              <SubscribeBox
                handleCloseModals={() => this.toggleSubscribeModal(false)}
              />
            </div>
          </CustomModal>
        </ModalWithPortal>
      </Fragment>
    );
  }
}

export default Layout;
