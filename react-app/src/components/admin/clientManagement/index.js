import React, { Component } from 'react';
//components @material-ui
import Modal from '@material-ui/core/Modal';
import Button from '@material-ui/core/Button';
//components
import CreateUser from '../components/CreateUser';
import ViewClientDetails from '../components/ViewClientDetails';
import EditClientDetails from '../components/editClientDetails';
import Table from '../../ui/table';
//helpers
import { fetchApi } from '../../../helpers/http/fetch';
import { API_ROUTES } from '../../../helpers/http/apiRoutes';
import { connect } from 'react-redux';
import { withCookies } from 'react-cookie';
//
import './style.scss';

@withCookies
@connect(state => {
  return {
    appState: state.appState,
  };
})
class ClientManagement extends Component {
  state = {
    showClientModal: false,
    showEditModal: false,
    showClientDetails: false,
    clientList: [],
    clientId: null,
    calcHeight: '100%',
  };

  UNSAFE_componentWillMount() {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');

    if (!authToken) {
      this.props.history.push('/');
    }
  }

  componentDidMount() {
    this.getClientList();
    this.setState({ calcHeight: (window.innerHeight / 100) * 8 + 'vh' });
  }

  getClientList = () => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let params = {
      url: API_ROUTES.adminListAllClients,
      method: 'POST',
      isAuth: true,
      authToken: authToken,
      data: '',
      showToggle: true,
    };

    fetchApi(params)
      .then(data => {
        if (data.status === 401) {
          return;
        } else {
          let { clientList } = this.state;
          clientList = data;
          this.setState({ clientList });
        }
      })
      .catch(e => console.log(e));
  };

  closeClientModal = () => {
    this.setState({ showClientModal: false });
  };

  closeEditModal = () => {
    this.setState({ showEditModal: false });
  };

  openEditModal = (isClientModal = false, clientId) => {
    this.setState({ showEditModal: true, clientId });
  };

  openClientModal = (isClientModal = false, clientId) => {
    if (isClientModal) {
      this.setState({
        showClientModal: true,
        showClientDetails: true,
        clientId,
      });
    } else {
      this.setState({ showClientModal: true, showClientDetails: false });
    }
  };

  handleShowClientDetails = clientId => {
    this.setState({ showClientDetails: true, clientId });
  };

  render() {
    let { clientList, showClientDetails } = this.state;

    return (
      <div className="userManagement">
        <div className="more--options">
          <span>
            <i className="fas fa-users"></i>
            <label>Registered Client List</label>
          </span>
          <button
            className="btn btn--create"
            onClick={() => this.openClientModal(false)}
          >
            <span className="icon-wrap">
              <i className="fas fa-user-plus"></i>
            </span>
            <span className="btn--label">Create Client</span>
          </button>
        </div>
        <div
          className="user--list-wrap"
          style={{ height: this.state.calcHeight }}
        >
          {
            <Table
              tableData={clientList}
              showViewDetails={true}
              // handleUserDetails={this.openClientModal}
              handleUserDetails={this.openEditModal}
              handleInputChange={this.handleInputChange}
              getUserList={this.getClientList}
              meta={{
                enableClearance: false,
              }}
            />
          }
        </div>
        <Modal
          open={this.state.showClientModal}
          // onClose={this.closeClientModal}
        >
          <div
            className={`modal-paper ${showClientDetails &&
              'client-details-modal'}`}
          >
            {showClientDetails ? (
              <ViewClientDetails
                getUserList={this.getClientList}
                handleCloseModal={this.closeClientModal}
                clientId={this.state.clientId}
              />
            ) : (
              <CreateUser
                userType="client"
                getUserList={this.getClientList}
                handleCloseModal={this.closeClientModal}
                showUserDetails={this.handleShowClientDetails}
              />
            )}
          </div>
        </Modal>
        <Modal
          open={this.state.showEditModal}
          // onClose={this.closeEditModal}
        >
          <div
            className={`modal-paper edit-details-modal ${showClientDetails &&
              'client-details-modal'}`}
          >
            {
              <EditClientDetails
                handleCloseModal={this.closeEditModal}
                clientId={this.state.clientId}
              />
            }
          </div>
        </Modal>
      </div>
    );
  }
}

export default ClientManagement;
