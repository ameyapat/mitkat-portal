import React, { Component } from 'react';
//components @material-ui
import Modal from '@material-ui/core/Modal';
import Button from '@material-ui/core/Button';
import CreateUser from '../components/CreateUser';
//components
import TableList from "../../ui/tablelist";
import TableListCell from "../../ui/tablelist/TableListCell";
import TableListRow from "../../ui/tablelist/TableListRow";
import Empty from '../../ui/empty'
//helpers
import { fetchApi } from '../../../helpers/http/fetch';
import { API_ROUTES } from '../../../helpers/http/apiRoutes';
import { connect } from 'react-redux';
import { withCookies } from 'react-cookie';
//
import './style.scss';
import { Divider } from '@material-ui/core';

@withCookies
@connect(state => {
  return {
    appState: state.appState
  }
})
class PerformanceIndex extends Component {

  state = {
    performanceIndexList: []
  }

  UNSAFE_componentWillMount(){
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    
    if(!authToken){
      this.props.history.push('/');
    }
  }

  componentDidMount(){
    this.getQualityList();
  }

  getQualityList = () => {

    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let params = {
      url: API_ROUTES.getQualityList,
      method: 'GET',
      isAuth: true,
      authToken: authToken,
      showToggle: true,
    }

    fetchApi(params)
    .then(data => {
        this.setState({performanceIndexList: data}); 
    })
    .catch((e) => console.log(e))
  }

  renderList = () => {
      return this.state.performanceIndexList.map(item => <TableListRow key={item.userid}>
            <TableListCell value={item.analystname} />
            <TableListCell value={item.qualityrating} />
            <TableListCell value={item.totalreports} />
          </TableListRow>)
  }

  render() {

    let { performanceIndexList } = this.state;

    return (
      <div id="performanceIndex" className="userManagement">
        <div className="more--options">
          <span>
            <i className="fas fa-indent"></i>
            <label>Quality List</label>
          </span>
        </div>
        <div className="user--list-wrap" style={{"height": this.state.calcHeight}}>
          {
              performanceIndexList.length > 0
              ?
              <TableList>
                  <TableListRow>
                      <TableListCell value="Analyst Name" classes={{root: "indexTHead" }} />
                      <TableListCell value="Report quality rating" classes={{root: "indexTHead" }} />
                      <TableListCell value="Total reports" classes={{root: "indexTHead" }} />
                  </TableListRow>
                  <Divider />
                  {
                      this.renderList()
                  }
              </TableList>
              :
              <Empty />
          }
        </div>        
      </div>
    )
  }
}

export default PerformanceIndex
