import React, {Component} from 'react'
import TopicManagementList from '../../components/topicManagementList'
import AddTopic from '../../components/topicManagementList/AddTopic'

import {getAllTopics} from '../../../../api/api'

class TopicManagement extends Component{

    state = {
        allTopics: []
    }

    componentDidMount(){
        this.updateList();
    }

    updateList = () => {
        getAllTopics()
        .then(res => {
            this.setState({allTopics: res})
        })
        .catch(e => console.log("Opps! error fetching all topics"))
    }

    render(){
        return (
          <div id="topicManagement">
            <AddTopic updateList={this.updateList} />
            <TopicManagementList
              allTopics={this.state.allTopics}
              updateList={this.updateList}
            />
          </div>
        );
    }
}

export default TopicManagement