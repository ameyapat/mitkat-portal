import React from 'react';
import Kaleidoscope from '../../components/Kaleidoscope';

const KaleidoscopePage = () => <Kaleidoscope />;

export default KaleidoscopePage;
