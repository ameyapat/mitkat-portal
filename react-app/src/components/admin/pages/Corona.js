import React, { Component } from "react";
import { withStyles, Button } from "@material-ui/core";
import CustomModal from '../../ui/modal';
import { withCookies } from "react-cookie";
import { fetchApi } from "../../../helpers/http/fetch";
import { API_ROUTES } from "../../../helpers/http/apiRoutes";

import store from "../../../store";
import { toggleToast } from "../../../actions";
import EditRiskWatch from '../components/createRiskWatch/EditRiskWatch';
import CoronaList from '../components/corona/listCoronaAlerts/CoronaList';
import '../components/corona/corona-style.scss';
import Empty from "../../ui/empty";
import { connect } from 'react-redux';

const styles = () => ({
    rootCorona: {
        padding: 20,
        borderRadius: 3,
    }
});

const toastObj = {
    toastMsg: 'Error fetching details',
    showToast: true
}

@withCookies
@withStyles(styles)
class Corona extends Component {

  state = {
    coronaAlertList: [],
    defaultList: [],
  }

  componentDidMount() {
    const { cookies } = this.props;
    const authToken = cookies.get("authToken");
    
    this.getCoronaAlerts(authToken)
    .then(data => {
        if(data){
            this.setState({ 
                coronaAlertList: data,
                defaultList: data, 
            }, () => console.log(this.state));
        }else{
            store.dispatch(toggleToast(toastObj))
        }
    })
    .catch(e => {
        store.dispatch(toggleToast(toastObj))
    })
  }

  getCoronaAlerts = (authToken) => {
    return new Promise((res, rej) => {
        const reqObj = {
            url: API_ROUTES.getCoronaReading,
            method: 'GET',
            isAuth: true,
            authToken,
            showToggle: true,
        }

        fetchApi(reqObj)
        .then(data => { 
            res(data)
         })
        .catch(e => rej(e));
    })
  }


  render() {
    const { classes } = this.props;
    const { coronaAlertList } = this.state;
    
    return (
      <div className={classes.rootCorona} id="corona">
          {
              coronaAlertList && coronaAlertList.length > 0
              ?
              <CoronaList 
                data={coronaAlertList}
                getCoronaAlerts={this.getCoronaAlerts}
              />
              :
              <Empty title="No Alerts Available!" />
          }
      </div>
    );
  }
}

const mapStateToProps = (state) => ({ });
const mapDispatchToProps = (dispatch) => ({ dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(Corona);
