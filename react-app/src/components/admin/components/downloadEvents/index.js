import React, { Component } from 'react';
//components
import { DatePicker } from 'material-ui-pickers';
import DownloadEventsList from './downloadEventsList';
import SearchBox from '../../../ui/searchBox';
//styles
import styles from './style';
//helpers
import { setHeight } from '../../helpers/utils';
import { fetchApi } from '../../../../helpers/http/fetch';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import store from '../../../../store';
import { toggleToast } from '../../../../actions';
//npm packages
import injectSheet from 'react-jss';
import moment from 'moment';
import Empty from '../../../ui/empty';
import { withCookies } from 'react-cookie';
import { saveAs } from 'file-saver';
//assets
import emptyImage from '../../../../assets/hugo-no-messages.png';
import SearchWrapTable from '../../../ui/searchWrapTable';
import { advancedSearch } from '../../../../helpers/utils';

@withCookies
@injectSheet(styles)
class DownloadEvents extends Component {
  state = {
    divHeight: 0,
    selectedDate: moment(new Date()).format('YYYY-MM-DD'),
    eventList: [],
    eventIds: null,
    searchValue: '',
    defaultList: [],
  };

  componentDidMount() {
    const el = document.querySelector('#innerDailyRisk');
    const divHeight = setHeight(el);
    this.setState({ divHeight });
  }

  handleDateChange = (type, value) => this.setState({ [type]: value });

  getEvents = () => {
    const { cookies } = this.props;
    let { startDate, endDate } = this.state;
    startDate = moment(startDate).format('YYYY-MM-DD');
    endDate = moment(endDate).format('YYYY-MM-DD');
    let authToken = cookies.get('authToken');
    let params = {
      url: `${API_ROUTES.getEventsForPeriod}`,
      method: 'POST',
      isAuth: true,
      authToken: authToken,
      data: {
        startdate: startDate,
        enddate: endDate,
      },
      showToggle: true,
    };

    fetchApi(params)
      .then(data => {
        this.setState({ eventList: data, defaultList: data });
      })
      .catch(e => console.log(e));
  };

  handleDownloadEvents = () => {
    let { cookies } = this.props;
    const { eventIds } = this.state;
    let authToken = cookies.get('authToken');
    let reqObj = {
      url: `${API_ROUTES.downloadEventsInExcel}`,
      isAuth: true,
      authToken,
      method: 'POST',
      isBlob: true,
      data: {
        eventids: eventIds,
      },
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(blobObj => {
        let objUrl = new Blob([blobObj], {
          type:
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8',
        });
        saveAs(objUrl, 'event-details.xlsx');

        let toastObj = {
          toastMsg: 'Downloading file...',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      })
      .catch(e => {
        let toastObj = {
          toastMsg: 'Error downloading excel sheet',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      });
  };

  handleEmailEvents = () => {
    let { cookies } = this.props;
    const { eventIds } = this.state;
    let authToken = cookies.get('authToken');
    let reqObj = {
      url: `${API_ROUTES.sendSelfEmail}`,
      isAuth: true,
      authToken,
      method: 'POST',
      data: {
        eventids: eventIds,
      },
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        let toastObj = {
          toastMsg: 'Email sent successfully!',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      })
      .catch(e => {
        let toastObj = {
          toastMsg: 'Error sending email try again!',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      });
  };

  updateList = eventIds => this.setState({ eventIds });

  handleInputChange = e => {
    let value = e.target.value;
    this.setState(
      state => {
        return {
          searchValue: value,
        };
      },
      () => {
        this.handleSearch();
      },
    );
  };

  handleSearch = () => {
    let { searchValue, defaultList } = this.state;

    if (searchValue) {
      let searchedList = advancedSearch(this.state.searchValue, defaultList);
      this.setState({ eventList: searchedList });
    } else {
      this.setState({ eventList: defaultList });
    }
  };

  render() {
    const { classes } = this.props;
    const {
      divHeight,
      startDate,
      endDate,
      eventList,
      searchValue,
    } = this.state;

    return (
      <div id="innerDailyRisk" className={classes.rootDailyRisk}>
        <div
          className={classes.innerDailyRisk}
          style={{ height: divHeight - 40 }}
        >
          <div className={classes.dateWrap}>
            <h1>
              <i className="fas fa-list" /> Select start date &amp; end date to
              download events
            </h1>
            <div className={classes.datesWrapper}>
              <DatePicker
                label="Start Date"
                value={startDate}
                onChange={value => this.handleDateChange('startDate', value)}
                animateYearScrolling
                classes={{
                  root: classes.rootDatePicker,
                  input: classes.datePickerInput,
                }}
                InputProps={{
                  classes: {
                    input: classes.txtField,
                  },
                }}
                InputLabelProps={{
                  shrink: true,
                  classes: {
                    root: classes.rootLabel,
                  },
                }}
              />
              <DatePicker
                label="End Date"
                value={endDate}
                onChange={value => this.handleDateChange('endDate', value)}
                animateYearScrolling
                classes={{
                  root: classes.rootDatePicker,
                  input: classes.datePickerInput,
                }}
                minDate={startDate}
                maxDate={moment(startDate).add(30, 'days')}
                InputProps={{
                  classes: {
                    input: classes.txtField,
                  },
                }}
                InputLabelProps={{
                  shrink: true,
                  classes: {
                    root: classes.rootLabel,
                  },
                }}
              />
              <button
                className={classes.btnSubmit}
                onClick={() => this.getEvents()}
              >
                Search
              </button>
              <button
                disabled={!eventList.length > 0}
                className={classes.btnSubmit}
                onClick={() => this.handleDownloadEvents()}
              >
                Download Events
              </button>
              <button
                disabled={!eventList.length > 0}
                className={classes.btnSubmit}
                onClick={() => this.handleEmailEvents()}
              >
                Email Events To Me
              </button>
            </div>
          </div>
          <div className={classes.riskWrapOutter}>
            <div className={classes.rootSearchWrapper}>
              <SearchBox
                placeholder={'Search by keyword'}
                parentHandler={e => this.handleInputChange(e)}
                value={searchValue}
              />
            </div>
            {eventList.length > 0 ? (
              <DownloadEventsList
                items={eventList}
                handleEditEvent={this.handleEditEvent}
                handleShowAlert={this.handleShowAlert}
                updateList={this.updateList}
              />
            ) : (
              <Empty
                imgSrc={emptyImage}
                alt="No items available!"
                title="No items available!"
              />
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default DownloadEvents;
