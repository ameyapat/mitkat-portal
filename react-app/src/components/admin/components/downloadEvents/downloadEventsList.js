import React, { useState, useEffect } from 'react';
//component
import TableList from '../../../ui/tablelist';
import TableListRow from '../../../ui/tablelist/TableListRow';
import TableListCell from '../../../ui/tablelist/TableListCell';
import Status from '../../../ui/status';
import CustomCheckbox from './customCheckbox';
import Checkbox from '@material-ui/core/Checkbox';
//helpers
import injectSheet from 'react-jss';
import { headerItems } from './constants';
//style
import styles from './style';
import TableHead from '../../../ui/tableHead';

const DownloadEventsList = ({ items, classes, updateList }) => {
  
  const [checkedList, handleCheckList] = useState([]);
  const [selectAll, setSelectAll] = useState(false);
  const [checkListMap, setListMap] = useState([]);

  useEffect(() => {
    initItems();
  }, [items]);

  //set initial items with true & false
  function initItems() {
    let newItems = [];
    items.map(i => {
      let ob = { isSelected: false, ...i };
      newItems.push(ob);
    });
    setListMap(newItems);
  }

  function addToList(id) {
    checkedList.push(id);

    let index = checkListMap.findIndex(i => id === i.id);
    checkListMap[index].isSelected = true;

    //check is all selected
    isAllSingleSelected();
    handleCheckList(checkedList);
    updateList(checkedList);
    setListMap(checkListMap);
  }

  function removeFromList(id) {
    const idx = checkedList.findIndex(i => i === id);
    checkedList.splice(idx, 1);

    let index = checkListMap.findIndex(i => id === i.id);
    checkListMap[index].isSelected = false;
    isAllSingleSelected();
    handleCheckList(checkedList);
    updateList(checkedList);
    setListMap(checkListMap);
  }

  //first case - select all & unselect all
  function toggleSelectAll(selectAll) {
    setSelectAll(selectAll);

    if (selectAll) {
      let selectedList = [];

      checkListMap.map(i => {
        i.isSelected = true;
        selectedList.push(i.id);
      });

      setListMap(checkListMap);
      handleCheckList(selectedList); //updates local state with all events ids
      updateList(selectedList); // udpate parent component with all eventids
    } else {
      checkListMap.map(i => {
        i.isSelected = false;
      });
      setListMap(checkListMap);
      handleCheckList([]);
      updateList([]);
    }
  }

  function isAllSingleSelected() {
    const isAllSelected = checkListMap.every(i => i.isSelected === true);
    if (isAllSelected) {
      setSelectAll(true);
    } else {
      setSelectAll(false);
    }
  }

  return (
    <div className={classes.rootDailyRisks}>
      <TableHead
        items={headerItems}
        render={() => (
          <Checkbox
            checked={selectAll}
            color='primary'
            onChange={() => toggleSelectAll(!selectAll)}
            value={'selectAll'}
            classes={{
              root: classes.rootSelectAllCheck
            }}
          />
        )}
      />
      <TableList>
        {checkListMap.map((item, idx) => (
          <TableListRow key={item.id}>
            <TableListCell value={item.eventdate} />
            <TableListCell value={item.title} />
            <TableListCell value={item.country} />
            <TableListCell value={item.state} />
            <TableListCell value={item.city} />
            <TableListCell value={item.status}>
              <Status type={item.status.toLowerCase()} />
            </TableListCell>
            <TableListCell value={item.creator} />
            <TableListCell value={item.storytype} />
            <TableListCell classes={{ root: classes.rootSingleSelect }}>
              <CustomCheckbox
                addToList={addToList}
                removeFromList={removeFromList}
                id={item.id}
                isSelected={item.isSelected}
              />
            </TableListCell>
          </TableListRow>
        ))}
      </TableList>
    </div>
  );
};

export default injectSheet(styles)(DownloadEventsList);
