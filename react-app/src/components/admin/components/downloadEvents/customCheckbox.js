import React, { useState, useEffect } from 'react';
import Checkbox from '@material-ui/core/Checkbox';
import injectSheet from 'react-jss';
import styles from './style';

const CustomCheckbox = ({ id, addToList, removeFromList, isSelected, classes }) => {
  const [isChecked, setChecked] = useState(isSelected);

  useEffect(() => {
    setChecked(isSelected)
  }, [isSelected])

  function handleIsChecked(id, isChecked) {    
    if (isChecked) {
      addToList(id);
    } else {
      removeFromList(id);
    }
  }

  return (
    <Checkbox
      checked={isChecked}
      color='primary'
      onChange={() => handleIsChecked(id, !isChecked)}
      value={id.toString()}
      inputProps={{
        'aria-label': 'primary checkbox'
      }}
      classes={{
        root: classes.customRootCheckbox
      }}
    />
  );
};

export default injectSheet(styles)(CustomCheckbox);
