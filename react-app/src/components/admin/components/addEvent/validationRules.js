const validationRulesAddEvent = [
  {
    field: 'eventTitle',
    method: 'isEmpty',
    validWhen: false,
    message: 'Event Title is required',
  },
  {
    field: 'eventSource',
    method: 'isEmpty',
    validWhen: false,
    message: 'Event Link is required',
  },
  {
    field: 'eventLocation',
    method: 'isEmpty',
    validWhen: false,
    message: 'Location is required',
  },
  {
    field: 'selectedCategory',
    method: 'isEmpty',
    validWhen: false,
    message: 'Category is required',
  },
  {
    field: 'latitude',
    method: 'isEmpty',
    validWhen: false,
    message: 'Latitude is required',
  },
  {
    field: 'longitude',
    method: 'isEmpty',
    validWhen: false,
    message: 'Longitude is required',
  },
  {
    field: 'selectedEventType',
    method: 'isEmpty',
    validWhen: false,
    message: 'Event Type is required',
  },
];

export default validationRulesAddEvent;
