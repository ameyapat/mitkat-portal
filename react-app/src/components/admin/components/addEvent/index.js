import React, { Component } from 'react';
import './style.scss';
import { eventTypeConstants } from '../../../client/components/rimeDashboard/helpers/constants';
import Select from 'react-select';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import CustomLabel from '../../../ui/customLabel';
import { withCookies } from 'react-cookie';
import { getListData } from '../../helpers/utils';
import { fetchApi } from '../../../../helpers/http/fetch';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import store from '../../../../store';
import { toggleToast } from '../../../../actions/';
import injectSheet from 'react-jss';
import style from './style';
import FormValidator from '../../../../helpers/validation/FormValidator';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import validationRulesAddEvent from './validationRules';

const customStyles = {
  control: provided => ({
    ...provided,

    backgroundColor: 'var(--backgroundSolid)',
    color: '#ffffff',
  }),
  option: (provided, state) => ({
    ...provided,
    zIndex: '99999',
  }),
  placeholder: provided => ({
    ...provided,

    color: '#ffffff',
  }),
  singleValue: provided => ({
    ...provided,
    color: '#ffffff',
  }),
};

@withRouter
@withCookies
@injectSheet(style)
@connect(state => {
  return {
    multipleLocations: state.multipleLocations,
  };
})
class AddEvent extends Component {
  validator = new FormValidator(validationRulesAddEvent);
  submitted = false;

  state = {
    eventDetails: '',
    riskCategories: [],
    eventType: eventTypeConstants,
    selectedEventType: { value: 1, label: 'Notification' },
    selectedCategory: {
      value: this.props.categoryId ? this.props.categoryId : 1,
      label: this.props.riskCategory ? this.props.riskCategory : 'Technology',
    },
    eventSource: this.props.eventLink ? this.props.eventLink : '',
    eventTitle: this.props.title,
    eventLocation: '',
    eventDescription: '',
    latitude: '',
    longitude: '',
    isLat: false,
    isLong: false,
    validation: this.validator.valid(),
  };

  componentDidMount() {
    this.getRiskCategories('riskCategory').then(riskCategories => {
      this.setState({ riskCategories });
    });
  }

  componentDidUpdate(prevProps) {
    const prevMultipleLocation =
      prevProps.multipleLocations.selectedMultipleLocations;
    const currMultipleLocation = this.props.multipleLocations
      .selectedMultipleLocations;
    if (currMultipleLocation !== prevMultipleLocation) {
      this.setState({ coordinates: currMultipleLocation });
    }
  }

  handleInputChange = (e, type) => {
    const LAT_RE = /^[+-]?(([1-8]?[0-9])(\.[0-9]{1,6})?|90(\.0{1,6})?)$/; // To Validate Latitude Values
    const LONG_RE = /^[+-]?((([1-9]?[0-9]|1[0-7][0-9])(\.[0-9]{1,6})?)|180(\.0{1,6})?)$/; // To Validate Longitude Values
    this.setState({ [type]: e.target.value }, () => {
      if (type === 'latitude') {
        if (LAT_RE.test(e.target.value)) {
          this.setState({ isLat: true });
        } else {
          this.setState({ isLat: false });
        }
      } else if (type === 'longitude') {
        if (LONG_RE.test(e.target.value)) {
          this.setState({ isLong: true });
        } else {
          this.setState({ isLong: false });
        }
      }
    });
  };

  handleSingleSelectSub = (value, type) => {
    this.setState({ [type]: value });
  };

  getRiskCategories = type => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');

    return new Promise((res, rej) => {
      getListData(type, authToken)
        .then(data => {
          let { riskCategories } = this.state;
          data.map(t => {
            let riskCategoriesObj = {};
            riskCategoriesObj['value'] = t.id;
            riskCategoriesObj['label'] = t.riskcategory;
            riskCategoriesObj['subCategories'] = t?.subcategories || [];
            riskCategories.push(riskCategoriesObj);
          });
          res(riskCategories);
        })
        .catch(e => rej(e));
    });
  };

  handleEventSubmit = e => {
    const { isLat, isLong } = this.state;
    e.preventDefault();
    let { cookies, history } = this.props;
    let authToken = cookies.get('authToken');
    this.validator = new FormValidator(validationRulesAddEvent);
    let validation = this.validator.validate(this.state);
    this.setState({ validation });
    this.submitted = true;

    if (validation.isValid && isLat && isLong) {
      let {
        eventDetails,
        eventTitle,
        eventDescription,
        eventSource,
        eventLocation,
        latitude,
        longitude,
        selectedCategory,
        selectedEventType,
      } = this.state;

      eventDetails = {
        eventTitle: eventTitle,
        eventDescription: eventDescription,
        eventLink: eventSource,
        eventLoc: eventLocation,
        eventPrimaryRiskCategory: selectedCategory.value,
        event_lat: latitude,
        event_long: longitude,
        eventType: selectedEventType.value,
      };

      let reqObj = {
        url: API_ROUTES.addEvent,
        data: eventDetails,
        isAuth: true,
        authToken,
        method: 'POST',
        showToggle: true,
      };

      fetchApi(reqObj)
        .then(data => {
          if (data && data.success === true) {
            let toastObj = {
              toastMsg: data.message,
              showToast: true,
            };
            store.dispatch(toggleToast(toastObj));
            this.setState({
              eventDetails: '',
              eventTitle: '',
              eventDescription: '',
              eventSource: '',
              eventLocation: '',
              latitude: '',
              longitude: '',
              selectedCategory: { value: 1, label: 'Technology' },
              selectedEventType: { value: 1, label: 'Notification' },
            });
          } else {
            let toastObj = {
              toastMsg: 'Something went wrong!, please try again',
              showToast: true,
            };

            store.dispatch(toggleToast(toastObj));
          }
        })
        .catch(e => console.log(e));
    }
  };

  render() {
    let { classes } = this.props;
    let {
      riskCategories,
      selectedCategory,
      eventType,
      selectedEventType,
      isLat,
      isLong,
    } = this.state;

    let validation = this.submitted
      ? this.validator.validate(this.state)
      : this.state.validation;

    return (
      <div className="createEmail__inner">
        <h3>Create new Event</h3>
        <div className="field--add--event">
          <section className="modal--section">
            <h2 className="event-head--title">
              <i className="icon-uniE90E" /> Event Details
            </h2>
            <div className="event--title">
              <TextField
                required
                error={validation.eventTitle.isInvalid}
                id="eventTitle"
                label="Event Title"
                multiline
                fullWidth
                rows="1"
                rowsMax="3"
                value={this.state.eventTitle}
                onChange={e => this.handleInputChange(e, 'eventTitle')}
                defaultValue=""
                margin="normal"
                variant="outlined"
                InputLabelProps={{
                  shrink: true,
                  classes: {
                    root: classes.rootLabel,
                  },
                }}
                InputProps={{
                  maxLength: 5000,
                  classes: {
                    root: classes.rootInput,
                    input: classes.rootInput,
                    notchedOutline: classes.notchedOutline,
                  },
                  endAdornment: null,
                }}
              />
              {validation.eventTitle.isInvalid && (
                <span className="error--text">
                  {validation.eventTitle.message}
                </span>
              )}
            </div>
            <div className="form__group description--formgroup">
              <div className="event--description">
                <TextField
                  id="eventDescription"
                  label="Description"
                  multiline
                  fullWidth
                  rowsMax="6"
                  value={this.state.eventDescription}
                  onChange={e => this.handleInputChange(e, 'eventDescription')}
                  margin="normal"
                  defaultValue="Enter Event Description"
                  variant="outlined"
                  InputLabelProps={{
                    shrink: true,
                    classes: {
                      root: classes.rootLabel,
                    },
                  }}
                  InputProps={{
                    maxLength: 5000,
                    classes: {
                      root: classes.rootInput,
                      input: classes.rootInput,
                      notchedOutline: classes.notchedOutline,
                    },
                    endAdornment: null,
                  }}
                />
              </div>
            </div>
            <div className="event--location">
              <TextField
                required
                error={validation.eventLocation.isInvalid}
                id="eventLocation"
                label="Event Location"
                multiline
                fullWidth
                rows="1"
                rowsMax="3"
                value={this.state.eventLocation}
                onChange={e => this.handleInputChange(e, 'eventLocation')}
                defaultValue="Enter Event Location"
                margin="normal"
                variant="outlined"
                InputLabelProps={{
                  shrink: true,
                  classes: {
                    root: classes.rootLabel,
                  },
                }}
                InputProps={{
                  maxLength: 5000,
                  classes: {
                    root: classes.rootInput,
                    input: classes.rootInput,
                    notchedOutline: classes.notchedOutline,
                  },
                  endAdornment: null,
                }}
              />
              {validation.eventLocation.isInvalid && (
                <span className="error--text">
                  {validation.eventLocation.message}
                </span>
              )}
            </div>
            <div className="event--cordinates">
              <div className="event--latitude">
                <TextField
                  required
                  error={validation.latitude.isInvalid}
                  id="latitude"
                  label="Event Latitude"
                  type="number"
                  fullWidth
                  rows="1"
                  rowsMax="3"
                  value={this.state.latitude}
                  onChange={e => this.handleInputChange(e, 'latitude')}
                  defaultValue="Enter Event Latitude"
                  margin="normal"
                  variant="outlined"
                  InputLabelProps={{
                    shrink: true,
                    classes: {
                      root: classes.rootLabel,
                    },
                  }}
                  InputProps={{
                    maxLength: 5000,
                    classes: {
                      root: classes.rootInput,
                      input: classes.rootInput,
                      notchedOutline: classes.notchedOutline,
                    },
                    endAdornment: null,
                  }}
                />
                {validation.latitude.isInvalid && (
                  <span className="error--text">
                    {validation.latitude.message}
                  </span>
                )}
              </div>
              <div className="event--longitude">
                <TextField
                  required
                  error={validation.longitude.isInvalid}
                  id="eventLongitude"
                  label="Event Longitude"
                  type="number"
                  fullWidth
                  rows="1"
                  rowsMax="3"
                  value={this.state.longitude}
                  onChange={e => this.handleInputChange(e, 'longitude')}
                  defaultValue="Enter Event Longitude"
                  margin="normal"
                  variant="outlined"
                  InputLabelProps={{
                    shrink: true,
                    classes: {
                      root: classes.rootLabel,
                    },
                  }}
                  InputProps={{
                    maxLength: 5000,
                    classes: {
                      root: classes.rootInput,
                      input: classes.rootInput,
                      notchedOutline: classes.notchedOutline,
                    },
                    endAdornment: null,
                  }}
                />
                {validation.longitude.isInvalid && (
                  <span className="error--text">
                    {validation.longitude.message}
                  </span>
                )}
              </div>
            </div>
            <div className="event--sources">
              <TextField
                error={validation.eventSource.isInvalid}
                id="eventSource"
                label="Event Sources"
                multiline
                fullWidth
                rows="1"
                rowsMax="3"
                value={this.state.eventSource}
                onChange={e => this.handleInputChange(e, 'eventSource')}
                defaultValue="Enter Event Source"
                margin="normal"
                variant="outlined"
                InputLabelProps={{
                  shrink: true,
                  classes: {
                    root: classes.rootLabel,
                  },
                }}
                InputProps={{
                  maxLength: 5000,
                  classes: {
                    root: classes.rootInput,
                    input: classes.rootInput,
                    notchedOutline: classes.notchedOutline,
                  },
                  endAdornment: null,
                }}
              />
            </div>
            <div className="twoColWrap single--selects">
              <div className="col-3">
                <CustomLabel
                  title="Risk Category"
                  classes="select--label"
                  isRequired={true}
                />
                <Select
                  defaultValue={selectedCategory}
                  className="select--options"
                  value={selectedCategory}
                  placeholder={'Risk Category'}
                  onChange={value =>
                    this.handleSingleSelectSub(value, 'selectedCategory')
                  }
                  options={riskCategories.length > 0 ? riskCategories : []}
                  styles={customStyles}
                />
                {validation.selectedCategory.isInvalid && (
                  <span className="error--text">
                    {validation.selectedCategory.message}
                  </span>
                )}
              </div>
              <div className="col-3">
                <CustomLabel
                  title="Event Type"
                  classes="select--label"
                  isRequired={true}
                />
                <Select
                  defaultValue={selectedEventType}
                  className="select--options"
                  value={selectedEventType}
                  placeholder={'Risk Category'}
                  onChange={value =>
                    this.handleSingleSelectSub(value, 'selectedEventType')
                  }
                  options={eventType.length > 0 ? eventType : []}
                  styles={customStyles}
                />
                {validation.selectedEventType.isInvalid && (
                  <span className="error--text">
                    {validation.selectedEventType.message}
                  </span>
                )}
              </div>
            </div>
          </section>
        </div>
        <div className="actionables">
          <Button
            onClick={e => this.handleEventSubmit(e)}
            style={{ marginLeft: 20, backgroundColor: '#2980b9' }}
            variant="contained"
            color="secondary"
          >
            Create Event
          </Button>
        </div>
      </div>
    );
  }
}

export default AddEvent;
