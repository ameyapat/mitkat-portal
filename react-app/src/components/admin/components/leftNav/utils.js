export const noAdminCreatorApprover = userRole => {
  if (
    userRole !== 'ROLE_ADMIN' &&
    userRole !== 'ROLE_PARTNER_ADMIN' &&
    userRole !== 'ROLE_PARTNER_CREATOR' &&
    userRole !== 'ROLE_PARTNER_APPROVER'
  ) {
    return true;
  } else {
    return false;
  }
};

export const noAdminPartner = userRole => {
  if (userRole !== 'ROLE_ADMIN' && userRole !== 'ROLE_PARTNER_ADMIN') {
    return true;
  } else {
    if (userRole === 'ROLE_PARTNER_ADMIN') {
      return true;
    }
    return false;
  }
};

export const withAdminPartner = userRole => {
  if (userRole === 'ROLE_ADMIN' || userRole === 'ROLE_PARTNER_ADMIN') {
    return true;
  } else {
    return false;
  }
};

export const withAdminCreatorApprover = userRole => {
  if (
    (userRole !== 'ROLE_ADMIN' &&
      userRole !== 'ROLE_CREATOR' &&
      userRole !== 'ROLE_PARTNER_CREATOR' &&
      userRole === 'ROLE_PARTNER_APPROVER') ||
    userRole === 'ROLE_APPROVER'
  ) {
    return true;
  } else {
    if (userRole === 'ROLE_PARTNER_ADMIN') {
      return true;
    } else {
      return false;
    }
  }
};
