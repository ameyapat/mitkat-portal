import React, { Component } from 'react';
import './style.scss';
//components
import Logo from '../logo';
import NavList from './NavList';
//
import CustomModal from '../../../ui/modal';
import CreateEventUI from '../createEventUI';
import CreateEmailAlert from '../createEmailAlert';
import CreateSmsAlert from '../createSmsAlert';
import CreateRiskWatch from '../createRiskWatch';
import CreateCoronaAlert from '../corona/createCoronaAlert';
import CreateCoronaAlertCity from '../corona/createCoronaAlertCity';
import CreateCovidAlert from '../corona/createCovidAlert/CreateCovidAlert';
import { withRouter } from 'react-router-dom';
import CreateQueries from '../createQueries';
import DownloadQueries from '../downloadQueries';

@withRouter
class LeftNav extends Component {
  state = {
    createEvent: false,
    updateEvent: false,
    createSmsAlert: false,
    createAlertHistory: false,
    createEmailAlert: false,
    createRiskWatch: false,
    createCoronaAlert: false,
    createCoronaAlertCity: false,
    createCovidAlert: false,
    createQueries: false,
    selectedItem: null,
  };

  handleOpenModal = type => {
    this.setState({ [type]: true });
  };

  handleCloseModal = type => {
    this.setState({ [type]: false });
  };

  handleRoute = (to, type) => {
    let path = this.props.match.url;
    this.setState({ selectedItem: type });
    this.props.history.push(`${path}${to}`);
  };

  render() {
    return (
      <div id="leftNav">
        <Logo />
        <NavList
          handleOpenModal={this.handleOpenModal}
          handleRoute={this.handleRoute}
          selectedItem={this.state.selectedItem}
        />
        <CustomModal showModal={this.state.createEvent}>
          <div id="createEvent" className="modal__inner">
            <CreateEventUI closeEventModal={this.handleCloseModal} />
          </div>
        </CustomModal>
        <CustomModal showModal={this.state.createEmailAlert}>
          <div id="createEmail" className="modal__inner">
            <CreateEmailAlert closeEventModal={this.handleCloseModal} leftNavAlert={true}/>
          </div>
        </CustomModal>
        <CustomModal showModal={this.state.createSmsAlert}>
          <div id="createSms" className="modal__inner">
            <CreateSmsAlert closeEventModal={this.handleCloseModal} />
          </div>
        </CustomModal>
        <CustomModal showModal={this.state.createRiskWatch}>
          <div id="createRiskWatch" className="modal__inner">
            <CreateRiskWatch closeEventModal={this.handleCloseModal} />
          </div>
        </CustomModal>
        <CustomModal showModal={this.state.createCoronaAlert}>
          <div id="createCoronaAlert" className="modal__inner">
            <CreateCoronaAlert closeEventModal={this.handleCloseModal} />
          </div>
        </CustomModal>
        <CustomModal showModal={this.state.createCoronaAlertCity}>
          <div id="createCoronaAlertCity" className="modal__inner">
            <CreateCoronaAlertCity closeEventModal={this.handleCloseModal} />
          </div>
        </CustomModal>
        <CustomModal showModal={this.state.createCovidAlert}>
          <div id="createCovidAlert" className="modal__inner">
            <CreateCovidAlert closeEventModal={this.handleCloseModal} />
          </div>
        </CustomModal>

        <CustomModal showModal={this.state.createQueries}>
          <div id="createQueries" className="modal__inner">
            <CreateQueries closeEventModal={this.handleCloseModal} />
          </div>
        </CustomModal>
        <CustomModal showModal={this.state.downloadQueries}>
          <div id="downloadQueries" className="modal__inner">
            <DownloadQueries closeEventModal={this.handleCloseModal} />
          </div>
        </CustomModal>
      </div>
    );
  }
}

export default LeftNav;
