import React, { Component, Fragment } from 'react';
//components
import NavListItem from './NavListItem';
import Divider from '../../../ui/divider';
//helpers
import { navItems } from '../../../../helpers/constants';
import { withCookies } from 'react-cookie';
import { withRouter } from 'react-router-dom';
import CustomAccordion from '../../../ui/customAccordion';
import {
  noAdminCreatorApprover,
  noAdminPartner,
  withAdminPartner,
  withAdminCreatorApprover,
} from './utils';

@withCookies
@withRouter
class NavList extends Component {
  state = {
    createEvent: false,
    userRole: null,
  };

  componentDidMount() {
    let { cookies } = this.props;
    let userRole = cookies.get('userRole');

    this.setState({ userRole, windowHeight: window.innerHeight });
  }

  getAdminList = () => {
    let navXML = [];
    let { userOptions } = navItems;
    let { selectedItem } = this.props;
    const { userRole } = this.state;
    let pathname = this.props.history.location.pathname;

    navXML = userOptions
      .filter(filteredItem => !filteredItem.blackList.includes(userRole))
      .map((item, index) => {
        return (
          <NavListItem
            path={pathname}
            isSelected={item.type === selectedItem ? true : false}
            userRole={userRole}
            key={index}
            title={item.title}
            icon={item.icon}
            type={item.type}
            parentHandler={() => {
              this.props.handleRoute(item.to, item.type);
            }}
          />
        );
      });
    return navXML;
  };

  getEventDatabase = () => {
    let navXML = [];
    let { selectedItem } = this.props;
    let { pathname } = this.props.history.location;
    let { eventDatabase_tabs } = navItems;

    navXML = eventDatabase_tabs.map((item, index) => {
      if (item.isRoute === true) {
        return (
          <NavListItem
            key={index}
            title={item.title}
            isSelected={item.type === selectedItem ? true : false}
            icon={item.icon}
            type={item.type}
            userRole={this.state.userRole}
            parentHandler={() => this.props.handleRoute(item.to, item.type)}
            path={pathname}
          />
        );
      } else {
        return (
          <NavListItem
            key={index}
            title={item.title}
            icon={item.icon}
            type={item.type}
            userRole={this.state.userRole}
            parentHandler={this.handleOpenModal}
            path={pathname}
          />
        );
      }
    });

    return navXML;
  };

  getOthersList = () => {
    let navXML = [];
    let { selectedItem } = this.props;
    let { pathname } = this.props.history.location;
    let { other_tabs } = navItems;

    navXML = other_tabs.map((item, index) => {
      if (item.isRoute === true) {
        return (
          <NavListItem
            key={index}
            title={item.title}
            isSelected={item.type === selectedItem ? true : false}
            icon={item.icon}
            type={item.type}
            userRole={this.state.userRole}
            parentHandler={() => this.props.handleRoute(item.to, item.type)}
            path={pathname}
          />
        );
      } else {
        return (
          <NavListItem
            key={index}
            title={item.title}
            icon={item.icon}
            type={item.type}
            userRole={this.state.userRole}
            parentHandler={this.handleOpenModal}
            path={pathname}
          />
        );
      }
    });

    return navXML;
  };

  getRimeList = () => {
    let navXML = [];
    let { selectedItem } = this.props;
    let { pathname } = this.props.history.location;
    let { datasurfer_tabs } = navItems;

    navXML = datasurfer_tabs.map((item, index) => {
      if (item.isRoute === true) {
        return (
          <NavListItem
            key={index}
            title={item.title}
            isSelected={item.type === selectedItem ? true : false}
            icon={item.icon}
            type={item.type}
            userRole={this.state.userRole}
            parentHandler={() => this.props.handleRoute(item.to, item.type)}
            path={pathname}
          />
        );
      } else {
        return (
          <NavListItem
            key={index}
            title={item.title}
            icon={item.icon}
            type={item.type}
            userRole={this.state.userRole}
            parentHandler={this.handleOpenModal}
            path={pathname}
          />
        );
      }
    });

    return navXML;
  };

  getScreeningList = () => {
    let navXML = [];
    let { selectedItem } = this.props;
    let { pathname } = this.props.history.location;
    let { screening_tabs } = navItems;

    navXML = screening_tabs.map((item, index) => {
      if (item.isRoute === true) {
        return (
          <NavListItem
            key={index}
            title={item.title}
            isSelected={item.type === selectedItem ? true : false}
            icon={item.icon}
            type={item.type}
            userRole={this.state.userRole}
            parentHandler={() => this.props.handleRoute(item.to, item.type)}
            path={pathname}
          />
        );
      } else {
        return (
          <NavListItem
            key={index}
            title={item.title}
            icon={item.icon}
            type={item.type}
            userRole={this.state.userRole}
            parentHandler={this.handleOpenModal}
            path={pathname}
          />
        );
      }
    });

    return navXML;
  };

  getEventList = () => {
    let navXML = [];
    let { selectedItem } = this.props;
    let { pathname } = this.props.history.location;
    let { eventManagement } = navItems;

    navXML = eventManagement.map((item, index) => {
      if (item.isRoute === true) {
        return (
          <NavListItem
            key={index}
            title={item.title}
            isSelected={item.type === selectedItem ? true : false}
            icon={item.icon}
            type={item.type}
            userRole={this.state.userRole}
            parentHandler={() => this.props.handleRoute(item.to, item.type)}
            path={pathname}
          />
        );
      } else {
        return (
          <NavListItem
            key={index}
            title={item.title}
            icon={item.icon}
            type={item.type}
            userRole={this.state.userRole}
            parentHandler={this.handleOpenModal}
            path={pathname}
          />
        );
      }
    });

    return navXML;
  };

  getCovidTabList = () => {
    let navXML = [];
    let { selectedItem } = this.props;
    let { pathname } = this.props.history.location;
    let { covid_19_tabs } = navItems;

    navXML = covid_19_tabs.map((item, index) => {
      if (item.isRoute === true) {
        return (
          <NavListItem
            key={index}
            title={item.title}
            isSelected={item.type === selectedItem ? true : false}
            icon={item.icon}
            type={item.type}
            userRole={this.state.userRole}
            parentHandler={() => this.props.handleRoute(item.to, item.type)}
            path={pathname}
          />
        );
      } else {
        return (
          <NavListItem
            key={index}
            title={item.title}
            icon={item.icon}
            type={item.type}
            userRole={this.state.userRole}
            parentHandler={this.handleOpenModal}
            path={pathname}
          />
        );
      }
    });

    return navXML;
  };

  getDeliverablesTabList = () => {
    let navXML = [];
    let { selectedItem } = this.props;
    let { pathname } = this.props.history.location;
    let { deliverables_tabs } = navItems;

    navXML = deliverables_tabs.map((item, index) => {
      if (item.isRoute === true) {
        return (
          <NavListItem
            key={index}
            title={item.title}
            isSelected={item.type === selectedItem ? true : false}
            icon={item.icon}
            type={item.type}
            userRole={this.state.userRole}
            parentHandler={() => this.props.handleRoute(item.to, item.type)}
            path={pathname}
          />
        );
      } else {
        return (
          <NavListItem
            key={index}
            title={item.title}
            icon={item.icon}
            type={item.type}
            userRole={this.state.userRole}
            parentHandler={this.handleOpenModal}
            path={pathname}
          />
        );
      }
    });

    return navXML;
  };

  getWeekly = () => {
    let navXML = [];
    let { selectedItem } = this.props;
    let { pathname } = this.props.history.location;
    let { weekly } = navItems;

    navXML = weekly.map((item, index) => {
      return (
        <NavListItem
          key={index}
          title={item.title}
          isSelected={item.type === selectedItem ? true : false}
          icon={item.icon}
          userRole={this.state.userRole}
          parentHandler={() => this.props.handleRoute(item.to, item.type)}
          path={pathname}
        />
      );
    });

    return navXML;
  };

  getEventApprovals = () => {
    let navXML = [];
    let { selectedItem } = this.props;
    let { pathname } = this.props.history.location;
    let { eventApprovals } = navItems;

    navXML = eventApprovals.map((item, index) => {
      return (
        <NavListItem
          key={index}
          title={item.title}
          isSelected={item.type === selectedItem ? true : false}
          icon={item.icon}
          userRole={this.state.userRole}
          parentHandler={() => this.props.handleRoute(item.to, item.type)}
          path={pathname}
        />
      );
    });

    return navXML;
  };

  getApproveDeliverables = () => {
    let navXML = [];
    let { selectedItem } = this.props;
    let { pathname } = this.props.history.location;
    let { approve_deliverable_tabs } = navItems;

    navXML = approve_deliverable_tabs.map((item, index) => {
      return (
        <NavListItem
          key={index}
          title={item.title}
          isSelected={item.type === selectedItem ? true : false}
          icon={item.icon}
          userRole={this.state.userRole}
          parentHandler={() => this.props.handleRoute(item.to, item.type)}
          path={pathname}
        />
      );
    });

    return navXML;
  };

  getDispatchOptions = () => {
    let liveXML = [];
    let { selectedItem } = this.props;
    let { dispatchOptions } = navItems;
    let { pathname } = this.props.history.location;

    return (liveXML = dispatchOptions.map((item, index) => {
      return (
        <NavListItem
          key={index}
          title={item.title}
          isSelected={item.type === selectedItem ? true : false}
          icon={item.icon}
          path={pathname}
          parentHandler={() => this.props.handleRoute(item.to, item.type)}
        />
      );
    }));
  };

  getQueryOptions = () => {
    let queryXML = [];
    let { selectedItem } = this.props;
    let { queryOptions } = navItems;
    let { pathname } = this.props.history.location;

    queryXML = queryOptions.map((item, index) => {
      if (item.isRoute === true) {
        return (
          <NavListItem
            key={index}
            title={item.title}
            isSelected={item.type === selectedItem ? true : false}
            icon={item.icon}
            type={item.type}
            userRole={this.state.userRole}
            parentHandler={() => this.props.handleRoute(item.to, item.type)}
            path={pathname}
          />
        );
      } else {
        return (
          <NavListItem
            key={index}
            title={item.title}
            icon={item.icon}
            type={item.type}
            userRole={this.state.userRole}
            parentHandler={this.handleOpenModal}
            path={pathname}
          />
        );
      }
    });

    return queryXML;
  };

  getAdvisoryOptions = () => {
    let advisoryXML = [];
    let { selectedItem } = this.props;
    let { advisoryOptions } = navItems;
    let { pathname } = this.props.history.location;

    return (advisoryXML = advisoryOptions.map((item, index) => {
      return (
        <NavListItem
          key={index}
          title={item.title}
          isSelected={item.type === selectedItem ? true : false}
          icon={item.icon}
          path={pathname}
          parentHandler={() => this.props.handleRoute(item.to, item.type)}
        />
      );
    }));
  };
  getUploadTabs = () => {
    let advisoryXML = [];
    let { selectedItem } = this.props;
    let { upload_tabs } = navItems;
    let { pathname } = this.props.history.location;

    return (advisoryXML = upload_tabs.map((item, index) => {
      return (
        <NavListItem
          key={index}
          title={item.title}
          isSelected={item.type === selectedItem ? true : false}
          icon={item.icon}
          path={pathname}
          parentHandler={() => this.props.handleRoute(item.to, item.type)}
        />
      );
    }));
  };

  getReportsOptions = () => {
    let reportsXML = [];
    let { selectedItem } = this.props;
    let { reportsOptions } = navItems;
    let { pathname } = this.props.history.location;

    return (reportsXML = reportsOptions.map((item, index) => {
      return (
        <NavListItem
          key={index}
          title={item.title}
          isSelected={item.type === selectedItem ? true : false}
          icon={item.icon}
          path={pathname}
          parentHandler={() => this.props.handleRoute(item.to, item.type)}
        />
      );
    }));
  };

  getRiskReviewOptions = () => {
    let riskReviewXML = [];
    let { selectedItem } = this.props;
    let { riskReviewOptions } = navItems;
    let { pathname } = this.props.history.location;

    return (riskReviewXML = riskReviewOptions.map((item, index) => {
      return (
        <NavListItem
          key={index}
          title={item.title}
          isSelected={item.type === selectedItem ? true : false}
          icon={item.icon}
          path={pathname}
          parentHandler={e => {
            e.stopPropagation();
            this.props.handleRoute(item.to, item.type);
          }}
        />
      );
    }));
  };

  handleOpenModal = type => {
    this.props.handleOpenModal(type);
  };

  render() {
    let { userRole, windowHeight } = this.state;

    return (
      <div id="navList" style={{ height: `calc(${windowHeight}px - 60px)` }}>
        {withAdminPartner(userRole) && (
          <Fragment>
            <h3>Admin Options</h3>
            <ul>{this.getAdminList()}</ul>
            <Divider />
          </Fragment>
        )}
        {
          //@Covid-19: Tab
        }
        {noAdminCreatorApprover(userRole) && (
          <Fragment>
            <CustomAccordion title={'Covid-19'} showDefault={false}>
              <ul>{this.getCovidTabList()}</ul>
            </CustomAccordion>
          </Fragment>
        )}
        {
          //@Create Deliverables
        }
        {noAdminPartner(userRole) && (
          <Fragment>
            <CustomAccordion title={'Create Deliverable'} showDefault={false}>
              <ul>{this.getDeliverablesTabList()}</ul>
            </CustomAccordion>
          </Fragment>
        )}
        {
          //@Approve Deliverable
        }
        {withAdminCreatorApprover(userRole) && (
          <Fragment>
            <CustomAccordion title={'Approve Deliverable'} showDefault={false}>
              <ul>{this.getApproveDeliverables()}</ul>
            </CustomAccordion>
          </Fragment>
        )}
        {
          //@Dispatch Deliverable
        }
        {withAdminCreatorApprover(userRole) && (
          <Fragment>
            <CustomAccordion title={'Dispatch Deliverable'} showDefault={false}>
              <ul>{this.getDispatchOptions()}</ul>
            </CustomAccordion>
          </Fragment>
        )}
        {
          //@Event Database
        }
        {noAdminPartner(userRole) && (
          <Fragment>
            <CustomAccordion title={'Event Database'} showDefault={false}>
              <ul>{this.getEventDatabase()}</ul>
            </CustomAccordion>
          </Fragment>
        )}
        {
          //@Forecast
        }
        {noAdminCreatorApprover(userRole) && (
          <Fragment>
            <CustomAccordion title={'Forecast'} showDefault={false}>
              <ul>{this.getWeekly()}</ul>
            </CustomAccordion>
          </Fragment>
        )}
        {
          //@Upload
        }
        {noAdminPartner(userRole) && (
          <Fragment>
            <CustomAccordion title={'Upload'} showDefault={false}>
              <ul>{this.getUploadTabs()}</ul>
            </CustomAccordion>
          </Fragment>
        )}
        {
          //@Query
        }
        {noAdminPartner(userRole) && (
          <Fragment>
            <CustomAccordion title={'Queries'} showDefault={false}>
              <ul>{this.getQueryOptions()}</ul>
            </CustomAccordion>
          </Fragment>
        )}
        {
          //@Others
        }
        {noAdminCreatorApprover(userRole) && (
          <Fragment>
            <CustomAccordion title={'Others'} showDefault={false}>
              <ul>{this.getOthersList()}</ul>
            </CustomAccordion>
          </Fragment>
        )}
        {withAdminCreatorApprover(userRole) && (
          <Fragment>
            <CustomAccordion
              title={'DataSurfer Monitoring'}
              showDefault={false}
            >
              <ul>
                <li>
                  <span className="list">
                    <i className="fa fa-tachometer" aria-hidden="true"></i>
                  </span>
                  <a href="/client/rime" target="_blank" className="adminRime">
                    RIME Dashboard
                  </a>
                </li>
                {this.getRimeList()}
              </ul>
            </CustomAccordion>
          </Fragment>
        )}
        {noAdminCreatorApprover(userRole) && (
          <Fragment>
            <CustomAccordion title={'Screening Section'} showDefault={false}>
              <ul>{this.getScreeningList()}</ul>
            </CustomAccordion>
          </Fragment>
        )}
      </div>
    );
  }
}
export default NavList;
