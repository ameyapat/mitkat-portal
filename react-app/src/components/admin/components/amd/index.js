import React, { Component, Fragment } from 'react';
import './style.scss';
//components
import TableList from '../../../ui/tablelist';
import TableListCell from '../../../ui/tablelist/TableListCell';
import TableListRow from '../../../ui/tablelist/TableListRow';
import CustomModal from '../../../ui/modal';
import Empty from '../../../ui/empty';
import AlertDialog from '../../../ui/alertDialog';
import EditEventUI from '../createEventUI/EditEventUI';
//helpers
import { fetchApi } from '../../../../helpers/http/fetch';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import { withCookies } from 'react-cookie';
import store from '../../../../store';
import { toggleToast } from '../../../../actions';
import { updateMultipleLocation } from '../../../../actions/multipleLocationActions';

@withCookies
class Amd extends Component {
  state = {
    tableData: [],
    showEventModal: false,
    eventDetails: null,
    showAlert: false,
    eventId: '',
  };

  componentDidMount() {
    this.getEvents();
  }

  getEvents = () => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let reqObj = {
      url: API_ROUTES.getTodaysEvent,
      isAuth: true,
      authToken,
      method: 'POST',
      showToggle: true,
    };
    fetchApi(reqObj).then(data => this.setState({ tableData: data }));
  };

  getTableItems = () => {
    let { tableData } = this.state;
    let itemXML = [];

    itemXML = tableData.map(i => {
      let approveStatus = 'pending';

      if (i.status.status === 'Approved') {
        approveStatus = 'approved';
      } else if (i.status.status === 'Pending Approval') {
        approveStatus = 'pending';
      } else {
        approveStatus = 'rejected';
      }

      return (
        <TableListRow key={i.id}>
          <TableListCell value={i.date || '-'} />
          <TableListCell value={i.title || '-'} classes={'amd--title'} />
          <TableListCell value={i.country || '-'} />
          <TableListCell value={i.state || '-'} />
          <TableListCell value={i.city || '-'} />
          <TableListCell>
            <span className={`approve--status ${approveStatus}`}></span>
            <p>{i.status.status || '-'}</p>
          </TableListCell>
          <TableListCell value={i.creator || '-'} />
          <TableListCell value={i.type || '-'} />
          <TableListCell
            value={
              i.importance ? (
                <i class="fas fa-star"></i>
              ) : (
                <i class="far fa-star"></i>
              )
            }
          />
          <TableListCell classes="btn--actions--wrap">
            <button
              className="btn--edit"
              onClick={() => this.handleEditEvent(i.id)}
            >
              <i className="icon-uniE93E"></i>
            </button>
            {/* <button 
                        className="btn--delete" 
                        onClick={() => this.handleDeleteEvent(i.id)}
                    >
                        <i className="icon-uniE949"></i>
                    </button> */}
            <button
              className="btn--delete"
              onClick={() => this.handleShowAlert(i.id)}
            >
              <i className="icon-uniE949"></i>
            </button>
          </TableListCell>
        </TableListRow>
      );
    });

    return itemXML;
  };

  handleDeleteEvent = id => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    //use fetch here
    let reqObj = {
      url: `${API_ROUTES.deleteTodaysEvent}/${id}`,
      isAuth: true,
      authToken,
      method: 'POST',
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        let toastObj = {
          toastMsg: data.message,
          showToast: data.success,
        };
        store.dispatch(toggleToast(toastObj));
        this.getEvents();
      })
      .catch(e => {
        let toastObj = {
          toastMsg: 'Error deleting report',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      });

    this.setState({ showAlert: false, eventId: '' });
  };

  handleEditEvent = id => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    //use fetch here
    let reqObj = {
      url: `${API_ROUTES.editTodaysEvent}/${id}`,
      isAuth: true,
      authToken,
      method: 'POST',
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        //
        this.setState({ eventDetails: data }, () => {
          this.handleModal(true);
        });
      })
      .catch(e => {
        //
        let toastObj = {
          toastMsg: 'Error fetching details',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      });
  };

  handleModal = show => {
    this.setState({ showEventModal: show });
  };

  updateEvent = eventDetails => {
    //
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');

    let reqObj = {
      url: API_ROUTES.updateTodaysEvent,
      data: eventDetails,
      isAuth: true,
      authToken,
      method: 'POST',
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        if (data && data.status == 200) {
          let toastObj = {
            toastMsg: 'Event Updated Successfully!',
            showToast: true,
          };
          this.handleModal(false);
          store.dispatch(toggleToast(toastObj));
          this.getEvents();
          store.dispatch(updateMultipleLocation([]));
        } else if (data && data.status == 502) {
          let toastObj = {
            toastMsg: 'Something went wrong!, please try again',
            showToast: true,
          };
          store.dispatch(toggleToast(toastObj));
        }
      })
      .catch(e => {
        let toastObj = {
          toastMsg: 'Error editing event!',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      });
  };

  handleShowAlert = eventId => {
    this.setState({ showAlert: true, eventId });
  };

  handleCloseAlert = () => {
    this.setState({ showAlert: false, eventId: '' });
  };

  render() {
    let { tableData, eventDetails, showAlert, eventId } = this.state;

    return (
      <div className="amd__wrap">
        <Fragment>
          <h2>Event List</h2>
          {tableData.length > 0 && (
            <div className="isImportantWrap">
              <label>
                IRT: <i class="fas fa-star"></i>
              </label>
              <label>
                Non IRT: <i class="far fa-star"></i>
              </label>
            </div>
          )}
        </Fragment>
        {tableData.length > 0 ? (
          <div className="tablelist__head">
            <div>
              <label>Date</label>
            </div>
            <div className="tl__title">
              <label>Title</label>
            </div>
            <div>
              <label>Country</label>
            </div>
            <div>
              <label>State</label>
            </div>
            <div>
              <label>City</label>
            </div>
            <div>
              <label>Status</label>
            </div>
            <div>
              <label>Creator</label>
            </div>
            <div>
              <label>Type</label>
            </div>
            <div>
              <label>IRT</label>
            </div>
            <div>
              <label>Edit/Delete</label>
            </div>
          </div>
        ) : (
          <Empty title="No events available" />
        )}
        {tableData.length > 0 && (
          <TableList classes={{ root: 'darkBlue' }}>
            {this.getTableItems()}
          </TableList>
        )}
        <CustomModal
          showModal={this.state.showEventModal}
          closeModal={() => this.handleModal(false)}
        >
          <div id="edit-event--modal" className="modal__inner">
            {eventDetails && (
              <EditEventUI
                eventDetails={this.state.eventDetails}
                closeEventModal={this.handleModal}
                isApprover={true}
                updateEvent={this.updateEvent}
              />
            )}
          </div>
        </CustomModal>
        {showAlert && (
          <AlertDialog
            isOpen={showAlert}
            title="Delete Event"
            description="You are about to delete an event, are you sure?"
            parentEventHandler={() => this.handleDeleteEvent(eventId)}
            handleClose={() => this.handleCloseAlert()}
          />
        )}
      </div>
    );
  }
}

export default Amd;
