import React, { Component, Fragment } from 'react';
import './style.scss';
//components
import TableList from '../../../ui/tablelist';
import TableListCell from '../../../ui/tablelist/TableListCell';
import TableListRow from '../../../ui/tablelist/TableListRow';
import Empty from '../../../ui/empty';
//helpers
import { fetchApi } from '../../../../helpers/http/fetch';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import { withCookies } from 'react-cookie';
import { sortByCoverage } from '../../../../helpers/utils';
import { withStyles } from '@material-ui/core';
import clsx from 'clsx';

const styles = () => ({
  rootRiskWatch: {
    background: '#F0F0F1',
    padding: 20,
  },
  innerRiskWatch: {
    '& .dmd__wrap': {
      margin: 0,
    },
  },
  rootThead: {
    fontWeight: 600,
    padding: '15px 3px',
    cursor: 'pointer',
    backgroundColor: 'var(--backgroundSolid)',
    '& p': {
      textTransform: 'uppercase',
    },
  },
  tRowWrap: {},
  rootTRowWrap: {
    padding: '0',
    borderBottom: 0,
    background: 'var(--backgroundSolid)',
    borderRadius: 5,
    '& p': {
      color: 'var(--whiteMediumEmp)',
    },
  },
  rowWrap: {
    backgroundColor: 'var(--backgroundSolid) !important',
    padding: '5px 0',
    borderRadius: '3px',
    color: '#f2f2f2',
    '&:nth-child(even)': {
      backgroundColor: '#101b2d',

      '& p': {
        color: 'var(--whiteMediumEmp)',
      },
    },
  },
  closeTimes: {
    position: 'absolute',
    color: '#000',
    fontSize: 30,
    right: 10,
    top: 10,
    cursor: 'pointer',
  },
  rootBtnGraph: {
    border: '1px solid var(--darkActive)',
    backgroundColor: 'transparent',
    color: 'var(--darkActive)',
    fontSize: '13px',
    minWidth: 32,
    '&:hover': {
      backgroundColor: 'var(--darkActive)',
      color: 'var(--whiteMediumEmp)',
    },

    '& i': {
      margin: 0,
    },
  },
  rootTCellNew: {
    justifyContent: 'space-around',
    '& p': {
      color: 'rgb(255, 255, 255) !important',
      fontWeight: 'bold',
      textAlign: 'center',
    },
  },
  metaWrap: {
    margin: '0 10px',
    padding: 10,
    backgroundColor: '#fff',
    borderRadius: 3,
    height: '100vh',
    overflowX: 'hidden',
    overflowY: 'scroll',
  },
});

@withCookies
class CoverageRating extends Component {
  state = {
    tableData: [],
    showEventModal: false,
    eventDetails: null,
  };

  componentDidMount() {
    this.getEvents();
  }

  getEvents = () => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let reqObj = {
      url: API_ROUTES.getCoverageRating,
      isAuth: true,
      authToken,
      method: 'GET',
      showToggle: true,
    };
    fetchApi(reqObj).then(data =>
      this.setState({ tableData: data, eventDetails: data }),
    );
  };

  handleSort = (type, sort) => {
    let sortedStateTable = sortByCoverage(this.state.eventDetails, type, sort);
    this.setState({ tableData: sortedStateTable });
  };

  renderTheadTitles = (value, meta) => {
    return (
      <span className="thead--titles--wrapper">
        <p>{value.title}</p>
        <span>
          <i className="fas fa-sort"></i>
        </span>
      </span>
    );
  };
  getTableItems = () => {
    let { tableData } = this.state;
    let itemXML = [];

    itemXML = tableData.map(i => {
      return (
        <TableListRow key={i.id}>
          <TableListCell value={i.countryName || '-'} />
          <TableListCell value={i.countryratingpopulation || '-'} />
          <TableListCell value={i.countryratinggdp || '-'} />
          <TableListCell value={i.totalevents || '-'} />
        </TableListRow>
      );
    });

    return itemXML;
  };

  render() {
    let { tableData, eventDetails } = this.state;
    const { classes } = this.props;
    return (
      <div className="coverageRating__wrap">
        <Fragment>
          <h2>Coverage Rating</h2>
        </Fragment>
        {tableData.length > 0 ? (
          <Fragment>
            <div className={classes.tRowWrap}>
              <TableListRow classes={{ root: classes.rootTRowWrap }}>
                <TableListCell
                  type={'country'}
                  value={{
                    title: 'Country Name',
                  }}
                  renderProp={(value, meta) =>
                    this.renderTheadTitles(value, meta)
                  }
                  eventHandler={(type, sortBy) => this.handleSort(type, sortBy)}
                  classes={{ root: clsx(classes.rootThead, 'msite-t-head') }}
                />
                <TableListCell
                  type={'population'}
                  value={{
                    title: 'Population',
                  }}
                  renderProp={(value, meta) =>
                    this.renderTheadTitles(value, meta)
                  }
                  eventHandler={(type, sortBy) => this.handleSort(type, sortBy)}
                  classes={{ root: clsx(classes.rootThead, 'msite-t-head') }}
                />
                <TableListCell
                  type={'gdp'}
                  value={{
                    title: 'GDP',
                  }}
                  renderProp={(value, meta) =>
                    this.renderTheadTitles(value, meta)
                  }
                  eventHandler={(type, sortBy) => this.handleSort(type, sortBy)}
                  classes={{ root: clsx(classes.rootThead, 'msite-t-head') }}
                />
                <TableListCell
                  type={'total'}
                  value={{
                    title: 'Total',
                  }}
                  renderProp={(value, meta) =>
                    this.renderTheadTitles(value, meta)
                  }
                  eventHandler={(type, sortBy) => this.handleSort(type, sortBy)}
                  classes={{ root: clsx(classes.rootThead, 'msite-t-head') }}
                />
              </TableListRow>
            </div>
            <TableList classes={{ root: 'darkBlue' }}>
              {tableData.length > 0 && this.getTableItems()}
            </TableList>
          </Fragment>
        ) : (
          <Empty title="No Coverage Rating!" />
        )}
      </div>
    );
  }
}

export default withStyles(styles)(CoverageRating);
