import React from 'react';
import EditEventUI from '../createEventUI/EditEventUI';
import CustomModal from '../../../ui/modal'

const WeeklyEventModal = ({ showEditEvent, handleModal, eventDetails, handleUpdateEvent }) => {
    return(
        <>
        <CustomModal 
            showModal={showEditEvent} 
            closeModal={() => handleModal(false)}
        >
            <div className="modal__inner">
                {
                eventDetails && <EditEventUI 
                    eventDetails={eventDetails}
                    closeEventModal={handleModal}
                    showUpdateBtn={true}
                    isApprover={false}
                    refreshWeekly={true}
                    refreshWeeklyEvents={handleUpdateEvent}
                />
                }
            </div>
        </CustomModal>
        </>
    )
}

export default WeeklyEventModal;