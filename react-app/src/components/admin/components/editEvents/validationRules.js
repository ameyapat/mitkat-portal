const validationRules = [
    {
        field: 'creator',
        method: 'isEmpty',
        validWhen: false,
        message: 'Creator is required'
    },
    {
        field: 'status',
        method: 'isEmpty',
        validWhen: false,
        message: 'Status is required'
    },
    {
        field: 'startdate',
        method: 'isEmpty',
        validWhen: false,
        message: 'Start date is required'
    },
    {
        field: 'enddate',
        method: 'isEmpty',
        validWhen: false,
        message: 'End date is required'
    }
]

export default validationRules