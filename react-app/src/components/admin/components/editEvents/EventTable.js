import React, { Component } from 'react';
import './style.scss';
import { T_HEAD } from './constants';
import { withCookies } from 'react-cookie';
import store from '../../../../store';
import { toggleToast, refreshEvents } from '../../../../actions';
//
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import { fetchApi } from '../../../../helpers/http/fetch';
import AlertDialog from '../../../ui/alertDialog';

const TableHead = () =>  T_HEAD.map((item, i) => <div key={i}><label>{item}</label></div>);

@withCookies
class EventTable extends Component {

    state = {
        calcHeight: '100%',
        eventId: ''
    }

    componentDidMount(){
        this.setState({calcHeight: (window.innerHeight / 100) * 8 + 'vh'}, () => console.log(this.state))
    }

    handleEditEvent = (id) => {
        this.props.handleEditEvent(id);
    }

    handleDeleteEvent = (id) => {
        let { cookies } = this.props;
        let authToken = cookies.get('authToken');
        //use fetch here
        let reqObj = {
            url: `${API_ROUTES.deleteTodaysEvent}/${id}`,
            isAuth: true,
            authToken,
            method: 'POST',
            showToggle: true,
        }

        fetchApi(reqObj)
        .then(data => {
            let toastObj = {
                toastMsg: data.message,
                showToast: data.success
            }
            store.dispatch(toggleToast(toastObj));
            // this.props.getAllEvents(authToken);
            store.dispatch(refreshEvents(true));
            
        })
        .catch(e => {
            let toastObj = {
                toastMsg: 'Error deleting report',
                showToast: true
            }
            store.dispatch(toggleToast(toastObj))
        })

        this.setState({showAlert: false, eventId: ''})
    }

    handleToast = () => {
        let toastObj = {
            toastMsg: 'You dont seem to have approver privileges.',
            showToast: true
        }
        store.dispatch(toggleToast(toastObj))
    }

    getTableItems = () => {
        let itemXML = [];
        let { events, cookies } = this.props;
        let userRole = cookies.get('userRole');
    
        itemXML = events.map(i => {
            
            let approveStatus = false;

            if(i.status === 'Approved'){
                approveStatus = 'approved';
            }else if(i.status === 'Pending Approval'){
                approveStatus = 'pending';
            }else{
                approveStatus = 'rejected';
            }

            return <div className="t--item">
                    <div className="date"><label>{i.validitydate}</label></div>
                    <div className="title"><p>{i.title}</p></div>
                    <div className="country"><p>{i.country}</p></div>
                    <div className="state"><p>{i.state}</p></div>
                    <div className="city"><p>{i.city}</p></div>
                    <div className="status"><span className={`approve--status ${approveStatus}`}></span><p>{i.status}</p></div>
                    <div className="creator"><p>{i.creator}</p></div>
                    <div className="storytype"><p>{i.storytype}</p></div>
                    <div className="edit--delete">
                        <button 
                            className="btn--edit" 
                            onClick={() => this.handleEditEvent(i.id)}
                        >
                            <i className="icon-uniE93E"></i>
                        </button>
                        {/* <button 
                            className="btn--delete" 
                            onClick={userRole !== 'ROLE_CREATOR' ? 
                            () => this.handleDeleteEvent(i.id)
                            :
                            this.handleToast
                        }>
                            <i className="icon-uniE949"></i>
                        </button> */}
                        <button 
                            className="btn--delete" 
                            onClick={userRole !== 'ROLE_CREATOR' ? 
                            () => this.handleShowAlert(i.id)
                            :
                            this.handleToast
                        }>
                            <i className="icon-uniE949"></i>
                        </button>
                     </div>
                </div>
        })
    
        return itemXML;
    }

    handleLoadMore = () => {
        //handle load more props function here...
        this.props.handleLoadMore();
    }

    handleShowAlert = (eventId) => {
        this.setState({showAlert: true, eventId})
    }

    handleCloseAlert = () => {
        this.setState({showAlert: false, eventId: ''})
    }

  render() {

    let { events, lastPage } = this.props;
    let { eventId, showAlert } = this.state;

    return (
      <div className="eventTable">
        <div className="table--head">
            <TableHead />
        </div>
        <div className="table--body" style={{"height": this.state.calcHeight}}>
            {
                events && this.getTableItems()
            }
            {
            !lastPage && 
            <div className="table--footer">
                <button onClick={this.handleLoadMore} className="btn btn--loadmore">Load More</button>
            </div>
            }
        </div>
        {
            showAlert &&
            <AlertDialog 
                isOpen={showAlert}
                title="Delete Event" 
                description="You are about to delete an event, are you sure?" 
                parentEventHandler={() => this.handleDeleteEvent(eventId)} 
                handleClose={() => this.handleCloseAlert()} 
            />
        }
      </div>
    )
  }
}

export default EventTable;
