import React, { Component } from 'react';
import { withCookies } from 'react-cookie';
import { connect } from 'react-redux';
import './style.scss';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import { fetchApi } from '../../../../helpers/http/fetch';
//components
import Filters from './Filters';
import EventTable from './EventTable';
import CustomModal from '../../../ui/modal';
import EditEventUI from '../createEventUI/EditEventUI';
//helpers
import store from '../../../../store';
import { toggleToast, refreshEvents } from '../../../../actions';

@withCookies
@connect(state => {
  return {
    appState: state.appState,
    refreshEvents: state.appState.refreshEvents,
  };
})
class EditEvents extends Component {
  state = {
    pageNo: 0,
    events: [],
    eventDetails: null,
    lastPage: false,
    totalPages: '',
    totalEvents: '',
    creatorsList: [],
    showEventModal: false,
    isFilterApplied: false,
    filters: {},
  };

  loadMore = false;

  componentDidMount() {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    this.getAllEvents(authToken); //get all events
    this.getCreatorsList(authToken); //get list of creators
  }

  componentDidUpdate(prevProps, prevState) {
    let { cookies } = this.props;
    let refreshEventsPrev = prevProps.refreshEvents;
    let refreshEventsCurr = this.props.refreshEvents;
    let authToken = cookies.get('authToken');

    if (refreshEventsPrev !== refreshEventsCurr) {
      this.getAllEvents(authToken);
    }
  }

  getCreatorsList = authToken => {
    let reqObj = {
      url: `${API_ROUTES.getCreatorList}`,
      isAuth: true,
      authToken,
      method: 'POST',
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        this.setState({
          creatorsList: data,
        });
      })
      .catch(e => {
        console.log(e);
      });
  };

  getAllEvents = authToken => {
    let { pageNo, lastPage } = this.state;
    let loadMorePages = this.loadMore && !lastPage ? true : false;
    let newPageNo = loadMorePages ? pageNo + 1 : 0;
    let reqObj = {
      url: `${API_ROUTES.getEventHistory}/${newPageNo}`,
      isAuth: true,
      authToken,
      method: 'POST',
      showToggle: true,
    };
    console.log(this.state.events, 'inside getallevents');
    fetchApi(reqObj)
      .then(data => {
        this.setState({
          pageNo: data.page,
          events: [...this.state.events, ...data.content],
          lastPage: data.last,
          totalPages: data.totalPages,
          totalEvents: data.size,
        });
        this.loadMore = data.page >= pageNo ? false : true;
      })
      .catch(e => {
        console.log(e);
      });

    store.dispatch(refreshEvents(false));
  };

  applyFilter = eventFilters => {
    let { pageNo, lastPage } = this.state;
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let reqData = { ...eventFilters };

    let loadMorePages = this.loadMore && !lastPage ? true : false;
    let newPageNo = loadMorePages ? pageNo + 1 : 0;

    let reqObj = {
      url: `${API_ROUTES.filterEventHistory}/${newPageNo}`,
      isAuth: true,
      authToken,
      method: 'POST',
      data: reqData,
      showToggle: true,
    };

    fetchApi(reqObj).then(data => {
      if (data.status != 502) {
        this.setState({
          pageNo: data.page,
          events: data.content,
          lastPage: data.last,
          totalPages: data.totalPages,
          totalEvents: data.size,
          filters: reqData,
          isFilterApplied: true,
        });

        this.loadMore = data.last >= pageNo ? false : true;
      } else {
        let toastObj = {
          toastMsg: 'Something went wrong!',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      }
    });
  };

  handleModal = show => {
    this.setState({ showEventModal: show });
  };

  handleEditEvent = eventId => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');

    let reqObj = {
      method: 'POST',
      isAuth: true,
      authToken,
      url: `${API_ROUTES.viewEventHistory}/${eventId}`,
      showToggle: true,
    };

    fetchApi(reqObj).then(eventDetails => {
      this.setState({ eventDetails }, () => {
        this.handleModal(true);
      });
    });
  };

  handleLoadMore = () => {
    let { cookies } = this.props;
    let { isFilterApplied, filters } = this.state;
    let authToken = cookies.get('authToken');
    this.loadMore = true;
    if (isFilterApplied) {
      this.applyFilter(filters);
    } else {
      this.getAllEvents(authToken);
    }
  };

  render() {
    let { events, creatorsList, eventDetails } = this.state;

    return (
      <div id="editEvents">
        <div className="wrapper">
          {
            // creatorsList && events.length > 0  &&
            <div className="filters">
              <Filters
                creatorsList={creatorsList}
                applyFilter={this.applyFilter}
                getAllEvents={this.getAllEvents}
              />
            </div>
          }
          <div className="content">
            {/* <h3>View Event List</h3> */}
            {events.length > 0 ? (
              <EventTable
                events={events}
                handleEditEvent={this.handleEditEvent}
                getAllEvents={this.getAllEvents}
                lastPage={this.state.lastPage}
                handleLoadMore={this.handleLoadMore}
              />
            ) : (
              <p className="no--updates--txt">No events available!</p>
            )}
          </div>
        </div>
        <CustomModal
          showModal={this.state.showEventModal}
          closeModal={() => this.handleModal(false)}
        >
          <div id="edit-event--modal" className="modal__inner">
            {eventDetails && (
              <EditEventUI
                eventDetails={this.state.eventDetails}
                closeEventModal={this.handleModal}
                showUpdateBtn={true}
                isApprover={false}
                hasAllTopics={true}
              />
            )}
          </div>
        </CustomModal>
      </div>
    );
  }
}

export default EditEvents;
