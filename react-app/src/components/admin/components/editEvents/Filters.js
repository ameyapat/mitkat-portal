import React, { Component, Fragment } from 'react';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import './style.scss';
import FormValidator from '../../../../helpers/validation/FormValidator';
import validationRules from './validationRules';
import { withCookies } from 'react-cookie';
import injectSheet from 'react-jss';
import style from './style.js';
@injectSheet(style)
@withCookies
class Filters extends Component {
  validator = new FormValidator(validationRules);
  submitted = false;

  state = {
    eventFilters: {
      creator: '',
      status: '',
      startdate: '',
      enddate: '',
    },
    validation: this.validator.valid(),
  };

  handleChange = (type, e) => {
    let { eventFilters } = this.state;
    eventFilters[type] = e.target.value;
    this.setState({ eventFilters }, () => console.log(this.state));
  };

  handleApplyFilter = () => {
    let { eventFilters } = this.state;
    let validation = this.validator.validate(eventFilters);
    this.submitted = true;
    this.setState({ validation });
    if (validation.isValid) {
      this.props.applyFilter(eventFilters);
    }
  };

  handleResetFilter = () => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    this.props.getAllEvents(authToken);
  };

  render() {
    let { classes } = this.props;
    let { creatorsList } = this.props;
    let {
      eventFilters,
      eventFilters: { creator, status },
    } = this.state;
    let validation = this.submitted
      ? this.validator.validate(eventFilters)
      : this.state.validation;

    return (
      <Fragment>
        <h3 className="head--filters">Apply Event Filters</h3>
        <div className="editEventFilters">
          <div className="fieldWrap">
            <TextField
              error={validation.startdate.isInvalid}
              id="startDate"
              label="Start Date"
              type="date"
              // defaultValue="2019-01-01"
              className="filter--fields"
              InputLabelProps={{
                shrink: true,
                classes: {
                  root: classes.rootLabel,
                },
              }}
              InputProps={{
                classes: {
                  root: classes.rootInput,
                  input: classes.rootInput,
                  notchedOutline: classes.notchedOutline,
                },
                endAdornment: null,
              }}
              onChange={e => this.handleChange('startdate', e)}
            />
          </div>
          <div className="fieldWrap">
            <TextField
              error={validation.enddate.isInvalid}
              id="endDate"
              label="End Date"
              type="date"
              // defaultValue=""
              // className={classes.textField}
              className="filter--fields"
              InputLabelProps={{
                shrink: true,
                classes: {
                  root: classes.rootLabel,
                },
              }}
              InputProps={{
                classes: {
                  root: classes.rootInput,
                  input: classes.rootInput,
                  notchedOutline: classes.notchedOutline,
                },
                endAdornment: null,
              }}
              onChange={e => this.handleChange('enddate', e)}
            />
          </div>
          <div className="fieldWrap">
            <FormControl
              error={validation.creator.isInvalid}
              variant="outlined"
              className="filter--fields"
            >
              <InputLabel
                ref={ref => {
                  this.InputLabelRef = ref;
                }}
                htmlFor="select-creator"
                className={classes.rootLabel}
              >
                Creator
              </InputLabel>
              <Select
                value={creator}
                onChange={e => this.handleChange('creator', e)}
                inputProps={{
                  classes: {
                    select: classes.rootSelect,
                  },
                }}
                input={
                  <OutlinedInput
                    labelWidth={0}
                    name="creator"
                    id="select-creator"
                  />
                }
              >
                {creatorsList.length > 0 &&
                  creatorsList.map(i => (
                    <MenuItem value={i.id}>{i.username}</MenuItem>
                  ))}
              </Select>
            </FormControl>
          </div>
          <div className="fieldWrap">
            <FormControl
              error={validation.status.isInvalid}
              variant="outlined"
              className="filter--fields"
            >
              <InputLabel
                ref={ref => {
                  this.InputLabelRef = ref;
                }}
                htmlFor="select-status"
                className={classes.rootLabel}
              >
                Status
              </InputLabel>
              <Select
                value={status}
                onChange={e => this.handleChange('status', e)}
                inputProps={{
                  classes: {
                    select: classes.rootSelect,
                  },
                }}
                input={
                  <OutlinedInput
                    labelWidth={0}
                    name="status"
                    id="select-status"
                  />
                }
              >
                <MenuItem value={0}>All</MenuItem>
                <MenuItem value={1}>Pending Approval</MenuItem>
                <MenuItem value={2}>Approved</MenuItem>
                <MenuItem value={3}>Rejected</MenuItem>
              </Select>
            </FormControl>
          </div>
        </div>
        <div className="fieldWrap">
          <button className="btn btn--apply" onClick={this.handleApplyFilter}>
            Apply
          </button>
          <button className="btn btn--reset" onClick={this.handleResetFilter}>
            Reset
          </button>
        </div>
      </Fragment>
    );
  }
}

export default Filters;
