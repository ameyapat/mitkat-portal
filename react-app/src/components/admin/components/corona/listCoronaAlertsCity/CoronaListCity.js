import React, { Component } from 'react';
import { withCookies } from 'react-cookie';
import { withStyles, Button } from '@material-ui/core';
import TableListRow from '../../../../ui/tablelist/TableListRow';
import TableListCell from '../../../../ui/tablelist/TableListCell';
import TableList from '../../../../ui/tablelist';
import { API_ROUTES } from '../../../../../helpers/http/apiRoutes';
import { fetchApi } from '../../../../../helpers/http/fetch';
import { connect } from 'react-redux';
import { toggleToast } from '../../../../../actions';
import CustomModal from '../../../../ui/modal';
import EditCoronaAlert from '../createCoronaAlertCity/EditCoronaAlert';
import '../corona-style.scss';
import moment from 'moment';

const styles = () => ({
  rootRiskWatch: {
    background: '#F0F0F1',
    padding: 20,
  },
  innerRiskWatch: {
    '& .dmd__wrap': {
      margin: 0,
    },
  },
  rootThead: {
    fontWeight: 600,
  },
  tRowWrap: {
    borderTop: '1px solid #f1f1f1',
  },
  rootTRowWrap: {
    padding: '15px 0',
    borderBottom: 0,
    backgroundColor: 'var(--backgroundSolid)',
  },
  rowWrap: {
    backgroundColor: 'var(--backgroundSolid)',
    '&:nth-child(even)': {
      backgroundColor: '#101b2d',
    },
    '& p': {
      color: 'var(--whiteMediumEmp)',
    },
  },
  closeTimes: {
    position: 'absolute',
    color: '#000',
    fontSize: 30,
    right: 10,
    top: 10,
    cursor: 'pointer',
  },
});

@withCookies
@withStyles(styles)
class CoronaListCity extends Component {
  state = {
    eventDetails: {},
    showModal: false,
  };

  handleModal = show => this.setState({ showModal: show });

  renderTHeader = () => {
    const { classes } = this.props;

    return (
      <div className={classes.tRowWrap}>
        <TableListRow classes={{ root: classes.rootTRowWrap }}>
          <TableListCell value={'Date'} classes={{ root: classes.rootThead }} />
          <TableListCell
            value={'City Name'}
            classes={{ root: classes.rootThead }}
          />
          <TableListCell value={'Dead'} classes={{ root: classes.rootThead }} />
          <TableListCell
            value={'Infected'}
            classes={{ root: classes.rootThead }}
          />
          <TableListCell
            value={'Recovered'}
            classes={{ root: classes.rootThead }}
          />
          <TableListCell value={'Edit'} classes={{ root: classes.rootThead }} />
        </TableListRow>
      </div>
    );
  };

  renderTBody = () => {
    return <TableList>{this.renderEventList()}</TableList>;
  };

  renderEventList = () => {
    const { classes, data } = this.props;

    return data.map(event => (
      <TableListRow key={event.eventid} classes={{ root: classes.rowWrap }}>
        <TableListCell value={moment(event.date).format('YYYY-MM-DD')} />
        <TableListCell value={event.cityName} />
        <TableListCell value={event.dead} />
        <TableListCell value={event.infected} />
        <TableListCell value={event.recovered} />
        <TableListCell>
          <Button
            variant="contained"
            color="secondary"
            onClick={() => this.handleEditEvent(event.id)}
          >
            Edit
          </Button>
        </TableListCell>
      </TableListRow>
    ));
  };

  handleEditEvent = id => {
    const { cookies } = this.props;
    const authToken = cookies.get('authToken');

    const reqObj = {
      url: `${API_ROUTES.getReadingCovidCity}/${id}`,
      isAuth: true,
      authToken,
      method: 'GET',
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        this.setState({ eventDetails: data }, () => {
          this.handleModal(true);
          console.log(this.state);
        });
      })
      .catch(e => {
        let toastObj = {
          toastMsg: 'Error fetching details',
          showToast: true,
        };
        this.props.dispatch(toggleToast(toastObj));
      });
  };

  render() {
    return (
      <div className="coronaListWrap">
        {this.renderTHeader()}
        {this.renderTBody()}
        <CustomModal
          showModal={this.state.showModal}
          closeModal={() => this.handleModal(false)}
        >
          <div id="imageRiskWatch" className="modal__inner">
            <EditCoronaAlert
              eventDetails={this.state.eventDetails}
              closeEventModal={() => this.handleModal(false)}
              getCoronaAlerts={this.props.getCoronaAlerts}
            />
          </div>
        </CustomModal>
      </div>
    );
  }
}

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(CoronaListCity);
