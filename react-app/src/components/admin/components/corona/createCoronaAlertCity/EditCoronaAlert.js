import React, { Component } from 'react';
import './style.scss';
//react google places
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';
import Select from 'react-select';
import MultiSelect from '../../../../ui/multiSelect';
import TextField from '@material-ui/core/TextField';
import Radio from '@material-ui/core/Radio';
import Button from '@material-ui/core/Button';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import CustomLabel from '../../../../ui/customLabel';
import AlertDialog from '../../../../ui/alertDialog';
import { withRouter } from 'react-router-dom';
import { withCookies } from 'react-cookie';
import { fetchApi } from '../../../../../helpers/http/fetch';
import { API_ROUTES } from '../../../../../helpers/http/apiRoutes';
import store from '../../../../../store';
import { toggleToast, refreshEvents } from '../../../../../actions';
import injectSheet from 'react-jss';
import style from './style';
import { connect } from 'react-redux';
import { DatePicker } from 'material-ui-pickers';
import {
  getArrFromObj,
  fetchStates,
  fetchCities,
} from '../../../../../helpers/utils';
import { setLabelValues } from './utils';
import { FormGroup } from '@material-ui/core';
import clsx from 'clsx';

const customStyles = {
  control: provided => ({
    ...provided,

    backgroundColor: 'var(--backgroundSolid)',
    color: '#ffffff',
  }),
  option: (provided, state) => ({
    ...provided,
    zIndex: '99999',
  }),
  placeholder: provided => ({
    ...provided,

    color: '#ffffff',
  }),
  singleValue: provided => ({
    ...provided,
    color: '#ffffff',
  }),
};

@withCookies
@withRouter
@injectSheet(style)
class EditCoronaAlert extends Component {
  state = {
    eventDate: '',
    allStates: [],
    selectedCity: '',
    dead: '',
    infected: '',
    tested: '',
    recovered: '',
    eventSource: '',
    comments: '',
    showAlert: false,
  };

  //   validator = new FormValidator(validationRules);
  //   submitted = false;

  componentDidMount() {
    const { cookies, eventDetails } = this.props;
    const authToken = cookies.get('authToken');

    Promise.all([fetchCities(authToken)]).then(values => {
      this.setState({
        allStates: setLabelValues('city', values[0]),
      });
    });

    this.setState({
      eventDate: eventDetails.date,
      selectedCity: {
        label: eventDetails.cityName,
        value: eventDetails.cityid,
      },
      dead: eventDetails.dead,
      infected: eventDetails.infected,
      tested: eventDetails?.tested || 0,
      recovered: eventDetails.recovered,
      eventSource: eventDetails.source,
      comments: eventDetails.comments,
    });
  }

  handleInputChange = (e, type) => {
    if (type === 'eventDate') {
      this.setState({ [type]: e }, () => console.log(this.state));
    } else {
      this.setState({ [type]: e.target.value }, () => console.log(this.state));
    }
  };

  handleAlert = show => this.setState({ showAlert: show });

  handleCloseModal = () => {
    this.setState({ showAlert: false });
    this.props.closeEventModal();
  };

  handleSingleSelect = (value, type) =>
    this.setState({ [type]: value }, () => console.log(this.state));

  handleEventSubmit = e => {
    e.preventDefault();
    let { history } = this.props;

    this.submitted = true;
    const { cookies } = this.props;
    const authToken = cookies.get('authToken');
    const {
      eventDate,
      selectedCity,
      dead,
      infected,
      tested,
      recovered,
      eventSource,
      comments,
    } = this.state;

    const eventDetails = {
      id: this.props.eventDetails.id,
      cityid: selectedCity.value,
      dead: parseInt(dead) || 0,
      infected: parseInt(infected) || 0,
      tested: parseInt(tested) || 0,
      recovered: parseInt(recovered) || 0,
      source: eventSource,
      comments: comments,
      date: eventDate,
    };

    const reqObj = {
      url: API_ROUTES.addCoronaCity,
      data: eventDetails,
      isAuth: true,
      authToken,
      method: 'POST',
      showToggle: true,
    };

    fetchApi(reqObj).then(data => {
      if (data && data.status === 200) {
        let toastObj = {
          toastMsg: 'Alert Updated Successfully!',
          showToast: true,
        };
        this.props.closeEventModal();
        this.props.getCoronaAlerts(authToken);
        store.dispatch(toggleToast(toastObj));
      } else if (data && data.status === 502) {
        let toastObj = {
          toastMsg: 'Something went wrong, Please try again',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      }
    });
  };

  render() {
    const { classes } = this.props;
    const {
      eventDate,
      allStates,
      selectedCity,
      dead,
      infected,
      tested,
      recovered,
      eventSource,
      comments,
      showAlert,
    } = this.state;

    return (
      <div className={'createEvent__inner'}>
        <h3>Update Corona Alert - City</h3>
        <div className={clsx('field--create--event', classes.riskWatchWrap)}>
          <section>
            <FormGroup classes={{ root: classes.formGroup }}>
              <DatePicker
                label="Event Date"
                value={eventDate}
                onChange={value => this.handleInputChange(value, 'eventDate')}
                animateYearScrolling
                InputLabelProps={{
                  shrink: true,
                  classes: {
                    root: classes.rootLabel,
                  },
                }}
                InputProps={{
                  classes: {
                    root: classes.rootInput,
                    input: classes.rootInput,
                    notchedOutline: classes.notchedOutline,
                  },
                  endAdornment: null,
                }}
              />
            </FormGroup>
            <FormGroup classes={{ root: classes.formGroup }}>
              <CustomLabel
                title="Select City"
                classes="select--label"
                isRequired={true}
              />
              <Select
                defaultValue={selectedCity}
                value={selectedCity}
                placeholder={'Select City'}
                onChange={value =>
                  this.handleSingleSelect(value, 'selectedCity')
                }
                options={allStates.length > 0 ? allStates : []}
                // styles={{ menu: styles => ({ ...styles, zIndex: 9999 }) }}
                styles={customStyles}
              />
            </FormGroup>
            <FormGroup classes={{ root: classes.formGroup }}>
              <TextField
                required
                id="deadNumber"
                label="Dead"
                fullWidth
                value={dead}
                onChange={e => this.handleInputChange(e, 'dead')}
                defaultValue="Dead"
                margin="normal"
                variant="outlined"
                InputLabelProps={{
                  shrink: true,
                  classes: {
                    root: classes.rootLabel,
                  },
                }}
                InputProps={{
                  classes: {
                    root: classes.rootInput,
                    input: classes.rootInput,
                    notchedOutline: classes.notchedOutline,
                  },
                  endAdornment: null,
                }}
                type="number"
              />
            </FormGroup>
            <FormGroup classes={{ root: classes.formGroup }}>
              <TextField
                required
                id="infectedNumber"
                label="Infected"
                fullWidth
                value={infected}
                onChange={e => this.handleInputChange(e, 'infected')}
                defaultValue="Infected"
                margin="normal"
                variant="outlined"
                InputLabelProps={{
                  shrink: true,
                  classes: {
                    root: classes.rootLabel,
                  },
                }}
                InputProps={{
                  classes: {
                    root: classes.rootInput,
                    input: classes.rootInput,
                    notchedOutline: classes.notchedOutline,
                  },
                  endAdornment: null,
                }}
                type="number"
              />
            </FormGroup>
            <FormGroup classes={{ root: classes.formGroup }}>
              <TextField
                required
                id="tested"
                label="Tests"
                fullWidth
                value={tested}
                onChange={e => this.handleInputChange(e, 'tested')}
                defaultValue="Tests"
                margin="normal"
                variant="outlined"
                InputLabelProps={{
                  shrink: true,
                  classes: {
                    root: classes.rootLabel,
                  },
                }}
                InputProps={{
                  classes: {
                    root: classes.rootInput,
                    input: classes.rootInput,
                    notchedOutline: classes.notchedOutline,
                  },
                  endAdornment: null,
                }}
                type="number"
              />
            </FormGroup>
            <FormGroup classes={{ root: classes.formGroup }}>
              <TextField
                required
                id="recoveredNumber"
                label="Recovered"
                fullWidth
                value={recovered}
                onChange={e => this.handleInputChange(e, 'recovered')}
                defaultValue="Infected"
                margin="normal"
                variant="outlined"
                InputLabelProps={{
                  shrink: true,
                  classes: {
                    root: classes.rootLabel,
                  },
                }}
                InputProps={{
                  classes: {
                    root: classes.rootInput,
                    input: classes.rootInput,
                    notchedOutline: classes.notchedOutline,
                  },
                  endAdornment: null,
                }}
                type="number"
              />
            </FormGroup>
            <FormGroup>
              <TextField
                required
                id="eventSources"
                label="Event Sources"
                multiline
                fullWidth
                rows="1"
                rowsMax="10"
                value={eventSource}
                onChange={e => this.handleInputChange(e, 'eventSource')}
                defaultValue="Enter Event Source"
                margin="normal"
                variant="outlined"
                InputLabelProps={{
                  shrink: true,
                  classes: {
                    root: classes.rootLabel,
                  },
                }}
                InputProps={{
                  maxLength: 5000,
                  classes: {
                    root: classes.rootInput,
                    input: classes.rootInput,
                    notchedOutline: classes.notchedOutline,
                  },
                  endAdornment: null,
                }}
              />
            </FormGroup>
            <FormGroup>
              <TextField
                required
                id="comments"
                label="Comments"
                multiline
                fullWidth
                rows="1"
                rowsMax="10"
                value={comments}
                onChange={e => this.handleInputChange(e, 'comments')}
                defaultValue="Comments"
                margin="normal"
                variant="outlined"
                InputLabelProps={{
                  shrink: true,
                  classes: {
                    root: classes.rootLabel,
                  },
                }}
                InputProps={{
                  maxLength: 5000,
                  classes: {
                    root: classes.rootInput,
                    input: classes.rootInput,
                    notchedOutline: classes.notchedOutline,
                  },
                  endAdornment: null,
                }}
              />
            </FormGroup>
          </section>
        </div>
        <div className="actionables">
          <Button
            onClick={e => this.handleAlert(true)}
            variant="contained"
            color="default"
          >
            Cancel
          </Button>
          <Button
            onClick={e => this.handleEventSubmit(e)}
            style={{ marginLeft: 20, backgroundColor: '#2980b9' }}
            variant="contained"
            color="secondary"
          >
            Update Corona Alert
          </Button>
        </div>
        {showAlert && (
          <AlertDialog
            isOpen={showAlert}
            title="Cancel Changes"
            description="You are about to cancel event changes, you won't be able to get them back, are you sure?"
            parentEventHandler={e => this.handleCloseModal()}
            handleClose={() => this.handleAlert(false)}
          />
        )}
      </div>
    );
  }
}

export default EditCoronaAlert;
