export const setLabelValues = (type, data) => {
  switch (type) {
    case "states":
      return data.map(item => ({ label: item.label, value: item.value }));
    case "city":
      return data.map(item => ({ label: item.label, value: item.value }));
    default:
      return [{ label: '', value: '' }];
  }
};