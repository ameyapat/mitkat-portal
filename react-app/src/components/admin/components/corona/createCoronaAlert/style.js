export default {
  singleSelect: {
    width: '100%',
  },
  rootText: {
    margin: '15px 0',
  },
  listOfItems: {
    margin: 0,
    borderBottom: '1px solid #f1f1f1',
    paddingBottom: 10,
    fontSize: '0.9em',
    color: 'var(--whiteMediumEmp)',
    fontWeight: '600',
    textAlign: 'right',
    '& i': {
      fontSize: '1.2em',
      marginRight: '3px',
    },
  },
  formGroup: {
    marginBottom: '15px',
    borderRadius: '5px',
  },
  listItemWrapper: {
    paddingTop: '15px',
  },
  btnAddItem: {
    width: '15%',
  },
  monthWrap: {},
  rootRadioBtns: {
    display: 'flex',
    flexDirection: 'row',
  },
  riskWatchWrap: {
    padding: '15px !important',
  },
  rootPriority: {
    width: '15%',
    marginLeft: 10,
  },
  btnWrapAddItem: {
    paddingTop: '10px',
    display: 'flex',
    justifyContent: 'flex-start',
  },
  grpItems: {
    border: '1px solid #d3d3d3',
    padding: '15px 10px',
  },
  itemListWrapper: {
    display: 'flex',
  },
  rootLabel: {
    color: 'rgba(255,255,255, 0.67) !important',
  },
  rootInput: {
    color: 'var(--whiteHighEmp) !important',
  },
  underline: {
    '&:after': {
      borderBottom: '2px solid #34495e !important',
    },
  },
  notchedOutline: {
    borderColor: 'var(--whiteHighEmp) !important ',
  },
};
