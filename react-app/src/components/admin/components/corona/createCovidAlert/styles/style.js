export default {
  formGroup: {
    marginBottom: '15px',
    borderRadius: '5px',
  },
  vaccinatedTextField: {
    color: 'var(--whiteHighEmp) !important ',
    padding: 0,
  },
  radioLabel: {
    color: 'var(--whiteHighEmp) !important ',
  },
  rootLabel: {
    color: 'rgba(255,255,255, 0.67) !important',
  },
  rootInput: {
    color: 'var(--whiteHighEmp) !important',
  },
  underline: {
    '&:after': {
      borderBottom: '2px solid #34495e !important',
    },
  },
  notchedOutline: {
    borderColor: 'var(--whiteHighEmp) !important ',
  },
  input: {
    color: 'var(--whiteHighEmp) !important ',
  },
};
