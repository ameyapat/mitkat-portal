import React from 'react';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import injectSheet from 'react-jss';
import styles from './styles/style';


const VaccineDate = ({handleOnChange, selectedDate, classes}) => {
    return(
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <KeyboardDatePicker
                fullWidth
                margin="normal"
                id="date-picker-dialog"
                label="Date picker dialog"
                format="MM/dd/yyyy"
                value={selectedDate}
                onChange={(value) => handleOnChange(value, "selectedDate")}
                KeyboardButtonProps={{
                    'aria-label': 'change date',
                    classes: {
                        root: classes.input
                     }
                }}
                InputLabelProps={{
                    shrink: true,
                    classes: {
                       root: classes.rootLabel
                    }
                  }}
                  InputProps={{
                      classes: {
                          root: classes.vaccinatedTextField,
                          input: classes.rootInput,
                          notchedOutline: classes.notchedOutline,
                      },
                      endAdornment: null
                  }}
            />
        </MuiPickersUtilsProvider>
    )
}

export default injectSheet(styles)(VaccineDate);