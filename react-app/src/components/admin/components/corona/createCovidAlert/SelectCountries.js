import React, { useState, useEffect } from 'react';
import CustomLabel from '../../../../ui/customLabel';
import Select from 'react-select';
import { FormGroup } from '@material-ui/core';
import {
  convertCountriesArrToObj,
  getCovidCountries,
} from '../../../helpers/utils';

import injectSheet from 'react-jss/lib/injectSheet';
import styles from './styles/style';

const SelectCountires = ({
  handleOnChange,
  selectedCountry,
  defaultValue = '',
  classes,
}) => {
  const [countries, setCountries] = useState([]);

  useEffect(() => {
    getCovidCountries()
      .then(country => {
        setCountries(convertCountriesArrToObj(country));
      })
      .catch(e => console.log(e));
  }, []);

  const customStyles = {
    control: provided => ({
      ...provided,

      backgroundColor: 'var(--backgroundSolid)',
      color: '#ffffff',
    }),
    option: provided => ({
      ...provided,
      backgroundColor: 'var(--backgroundSolid)',
      color: '#ffffff',
    }),
    menu: provided => ({
      ...provided,
      zIndex: '99999',
    }),
    placeholder: provided => ({
      ...provided,

      color: '#ffffff',
    }),
    singleValue: provided => ({
      ...provided,
      color: '#ffffff',
    }),
  };

  return (
    <FormGroup classes={{ root: classes.formGroup }}>
      <CustomLabel
        title="Select Country"
        classes="select--label"
        isRequired={true}
      />
      <Select
        defaultValue={defaultValue}
        value={selectedCountry}
        placeholder={'Select Country'}
        onChange={value => handleOnChange(value, 'selectedCountry')}
        options={countries.length > 0 ? countries : []}
        // styles={{ menu: styles => ({ ...styles, zIndex: 9999 }) }}
        styles={customStyles}
      />
    </FormGroup>
  );
};

export default injectSheet(styles)(SelectCountires);
