import React, { Component } from 'react';
import { getCovidCountriesReadingList } from '../../../helpers/utils';

import TableList from '../../../../ui/tablelist';
import TableListCell from '../../../../ui/tablelist/TableListCell';
import TableListRow from '../../../../ui/tablelist/TableListRow';
import CustomModal from '../../../../ui/modal';
import Empty from '../../../../ui/empty';

import './styles/style.scss';

import moment from 'moment';
import CreateCovidAlert from './CreateCovidAlert';
import { Button } from '@material-ui/core';
class ViewCovidAlert extends Component {
  state = {
    countryReadingList: [],
    readingId: null,
  };

  componentDidMount() {
    this.getCountriesReadingList();
  }

  getCountriesReadingList = () => {
    getCovidCountriesReadingList()
      .then(countryReadingList => {
        this.setState({ countryReadingList });
      })
      .catch(e => console.log(e));
  };

  handleCloseEventModal = () => {
    this.props.closeEventModal('createCovidAlert');
  };

  renderCountryReadingList = () => {
    const { countryReadingList } = this.state;

    return countryReadingList.map(country => {
      return (
        <TableListRow key={country.id}>
          <TableListCell
            value={moment(country.date).format('YYYY-MM-DD') || '-'}
          />
          <TableListCell value={country.countryname || '-'} />
          <TableListCell value={country.vaccinenumber || '-'} />
          <TableListCell>
            <Button
              variant="contained"
              color="secondary"
              onClick={() => {
                this.toggleModal(true);
                this.setState({ readingId: country.id });
              }}
            >
              Edit
            </Button>
          </TableListCell>
        </TableListRow>
      );
    });
  };

  renderCountryReadingHeader = () => {
    return (
      <TableListRow>
        <TableListCell value={'Date'} />
        <TableListCell value={'Country Name'} />
        <TableListCell value={'Vaccination'} />
        <TableListCell value={'#'} />
      </TableListRow>
    );
  };

  toggleModal = showModal => this.setState({ showModal });

  render() {
    const { readingId } = this.state;
    return (
      <div id="countryReadyList">
        {this.renderCountryReadingHeader()}
        {this.state.countryReadingList.length > 0 ? (
          <TableList>{this.renderCountryReadingList()}</TableList>
        ) : (
          <Empty title="No List Available" />
        )}
        <CustomModal
          showModal={this.state.showModal}
          closeModal={() => this.toggleModal(false)}
        >
          <div className="modal__inner">
            <CreateCovidAlert
              isEditable={true}
              toggleModal={this.toggleModal}
              readingId={readingId}
              getCountriesReadingList={this.getCountriesReadingList}
            />
          </div>
        </CustomModal>
      </div>
    );
  }
}

export default ViewCovidAlert;
