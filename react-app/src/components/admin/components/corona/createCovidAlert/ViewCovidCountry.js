import React, { Component } from 'react'
import { getCovidCountries, getEditableCountryDetails, updateCountryDetails, updateCountryCountryReading, getCovidCountriesReadingList, getEditableCountryReading } from '../../../helpers/utils';

import TableList from '../../../../ui/tablelist';
import TableListCell from '../../../../ui/tablelist/TableListCell';
import TableListRow from '../../../../ui/tablelist/TableListRow';
import EditCovidCountry from './EditCovidCountry'

import './styles/style.scss';
import CustomModal from '../../../../ui/modal';

class ViewCovidCountry extends Component{

    state = {
        countries: [],
        showModal: false,
        countryId: null,
        showAlert: false,
    }

    componentDidMount(){
        this.getCountries();
    }

    getCountries = () => {
        getCovidCountries().then((countries) => {
            this.setState({countries}, () => console.log(this.state));
        })
        .catch(e => console.log(e))
    }

    handleCloseEventModal = () => {
        this.props.closeEventModal("createCovidAlert");
    }

    renderCountryList = () => {
        const { countries } = this.state;

        return countries.map((country) => {
            return(
                <TableListRow key={country.id}>
                    <TableListCell value={country.countryname || '-'} />
                    <TableListCell>
                        <button 
                             onClick={() => {
                                this.toggleModal(true)
                                this.setState({countryId: country.id})
                            }} 
                            className="btn btnEdit"
                        >
                            Edit
                        </button>
                    </TableListCell>
                </TableListRow>
            )
        })
    }

    renderCountryHeader = () => {
        return(
            <TableListRow>
                <TableListCell value={"Country Name"} />
                <TableListCell value={"#"} />
            </TableListRow>
        )
    }

    toggleModal = (showModal) => this.setState({showModal})

    handleCloseEventModal = () => {
        this.handleToggleAlert(true);
    }

    render(){
        return(
            <div id="countriesCovid">
                {this.renderCountryHeader()}
                {
                    this.state.countries.length > 0 
                    ?                
                    <TableList>                        
                        {this.renderCountryList()}
                    </TableList>
                    :
                    ""
                }
                <CustomModal
                    showModal={this.state.showModal}
                    closeModal={() => this.toggleModal(false)}
                >
                    <div className="modal__inner">
                        <EditCovidCountry 
                            countryId={this.state.countryId} 
                            toggleModal={this.toggleModal}
                            getCountries={this.getCountries}
                        />
                    </div>
                </CustomModal>
            </div>
        )
    }
}

export default ViewCovidCountry;