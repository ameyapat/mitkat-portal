import React, { useState, useEffect } from 'react';
import TextField from "@material-ui/core/TextField";
import { FormGroup } from "@material-ui/core";
import injectSheet from 'react-jss/lib/injectSheet';

import styles from './styles/style';
import './styles/style.scss';

const VaccinatedTextField = ({handleOnChange, vaccinated, classes}) => {

    return(
        <FormGroup classes={{ root: classes.formGroup }}>
            <TextField
                required
                id="vaccinated"
                label="Number of vaccinated"
                fullWidth
                className="vaccinatedTextField"
                value={vaccinated}
                onChange={e => handleOnChange(parseInt(e.target.value), "vaccinated")}
                defaultValue={vaccinated}
                margin="normal"
                variant="outlined" 
                InputLabelProps={{
                    shrink: true,
                    classes: {
                       root: classes.rootLabel
                    }
                  }}
                  InputProps={{
                      classes: {
                          root: classes.vaccinatedTextField,
                          input: classes.rootInput,
                          notchedOutline: classes.notchedOutline,
                      },
                      endAdornment: null
                  }}
                type="number"
            />
        </FormGroup>
    )
}

export default injectSheet(styles)(VaccinatedTextField)