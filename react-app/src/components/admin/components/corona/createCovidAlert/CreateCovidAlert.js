import React, { Component } from 'react'
import styles from './styles/style';
import './styles/style.scss';

import { updateCountryCountryReading, getEditableCountryReading } from '../../../helpers/utils';
import SelectCountries from './SelectCountries'
import VaccinatedTextField from './VaccinatedTextField'
import VaccineDate from './VaccineDate'
import AlertDialog from '../../../../ui/alertDialog';

import moment from 'moment'
import injectSheet from "react-jss";
import { Divider, FormGroup } from '@material-ui/core';
import { toggleToast } from '../../../../../actions/index'
import store from '../../../../../store';

@injectSheet(styles)
class CreateCovidAlert extends Component{

    state = {
        selectedCountry: "",
        vaccinated: 0,
        selectedDate: moment(new Date()).format("YYYY-MM-DD"),
        toastObj: { showToast: true, toastMsg: "" },
        isDisabled: true,
        showAlert: false,
        currentId: 0
    }

    componentDidMount(){
        const { isEditable, readingId } = this.props;
        if(isEditable){
            getEditableCountryReading(readingId).then((data) => {
                this.setState({
                    selectedCountry: { value: data.countryid, label: data.countryname },
                    selectedDate: moment(data.date).format("YYYY-MM-DD"),
                    vaccinated: data.vaccinenumber,
                    currentId: data.id
                })
            })
        }
    }

    handleCloseEventModal = () => {
        this.handleToggleAlert(true);
    }

    handleOnChange = (value, type) => {
        this.setState({[type]: value});
    }

    handleSubmit = () => {
        const { isEditable, toggleModal, getCountriesReadingList } = this.props;
        const { selectedDate, selectedCountry, vaccinated, toastObj, currentId } = this.state
        const reqObj = {
            "id": currentId,
            "date": moment(selectedDate).format("YYYY-MM-DD"),
            "countryid": selectedCountry.value,
            "vaccinated": vaccinated
        };
          
        updateCountryCountryReading(reqObj).then((data) => {
            if(data && data.status === 200){
                this.setState({toastObj: {...toastObj, toastMsg: data.message}}, () => {
                    if(isEditable){
                        toggleModal(false)
                        getCountriesReadingList()
                    }else{
                        this.props.closeEventModal("createCovidAlert")
                    }
                })
            }else{
                this.setState({toastObj: {...toastObj, toastMsg: data.message}})
            }
            store.dispatch(toggleToast(this.state.toastObj));
        })
    }

    isNotValid = () => {
        const { selectedCountry, vaccinated } = this.state;
        if(selectedCountry && vaccinated){
            return false
        }else{
            return true
        }
    }

    handleToggleAlert = (show) => this.setState({showAlert: show})

    render(){
        const { isEditable, toggleModal } = this.props;
        const { selectedCountry, vaccinated, selectedDate, showAlert } = this.state;
        
        return(
            <>                
                <div className="createCovidAlert__header">
                    <button 
                        onClick={this.handleCloseEventModal} 
                        className="timesClose"
                    >
                        &times;
                    </button>
                </div>
                <Divider />
                <div className="createCovidAlert__body">
                    <FormGroup>
                        <SelectCountries 
                            defaultValue={selectedCountry}
                            selectedCountry={selectedCountry} 
                            handleOnChange={this.handleOnChange} 
                        />
                    </FormGroup>
                    <VaccinatedTextField 
                        handleOnChange={this.handleOnChange} 
                        vaccinated={vaccinated}
                    />
                    <VaccineDate 
                        handleOnChange={this.handleOnChange} 
                        selectedDate={selectedDate} 
                    />
                </div>
                <Divider />
                <div className="createCovidAlert__footer">
                    <div className="createCovidAlert__footer__btnWrapper">
                        <button disabled={this.isNotValid()} onClick={this.handleSubmit} className="btn btnSubmit">Submit</button>
                        <button 
                            onClick={this.handleCloseEventModal} 
                            className="btn btnClose"
                        >
                            Close
                        </button>
                    </div>
                </div>
                {showAlert && (
                <AlertDialog
                    isOpen={showAlert}
                    title='Cancel Changes'
                    description="You are about to cancel event changes, you won't be able to get them back, are you sure?"
                    parentEventHandler={isEditable ? () => toggleModal(false) : e => this.props.closeEventModal("createCovidAlert")}
                    handleClose={() => this.handleToggleAlert(false)}
                />
                )}
            </>
        )
    }
}

export default CreateCovidAlert;