import React, { Component } from 'react';
import styles from './styles/style';
import './styles/style.scss';

import {
  updateCountryDetails,
  getEditableCountryDetails,
} from '../../../helpers/utils';
import AlertDialog from '../../../../ui/alertDialog';

import moment from 'moment';
import injectSheet from 'react-jss';
import { Divider, FormGroup, TextField } from '@material-ui/core';
import { toggleToast } from '../../../../../actions/index';
import store from '../../../../../store';

import countryItems from './helpers/constant';

@injectSheet(styles)
class EditCovidCountry extends Component {
  state = {
    countryDetails: {
      id: '',
      approvedVaccine: '',
      vaccineStartDate: '',
      vaccineTimeline: '',
      phase1: '',
      phase2: '',
      phase3: '',
      moreInfoLink: '',
      vaccineLink: '',
      officialLink: '',
      otherParagraph: '',
    },
    toastObj: { showToast: true, toastMsg: '' },
  };

  componentDidMount() {
    this.handleGetEditableCountryDetails();
  }

  handleGetEditableCountryDetails = () => {
    const { countryId } = this.props;
    const { countryDetails } = this.state;

    getEditableCountryDetails(false, countryId).then(data => {
      countryDetails.id = data.id;
      countryDetails.approvedVaccine = data.approvedVaccine;
      countryDetails.vaccineStartDate = data.vaccinatinStartDate;
      countryDetails.vaccineTimeline = data.vaccineTimeline;
      countryDetails.phase1 = data.phase1;
      countryDetails.phase2 = data.phase2;
      countryDetails.phase3 = data.phase3;
      countryDetails.moreInfoLink = data.moreinfolink;
      countryDetails.vaccineLink = data.vaccinelink;
      countryDetails.officialLink = data.officiallink;
      countryDetails.otherParagraph = data.otherParagraph;

      this.setState({ countryDetails }, () => console.log(this.state));
    });
  };

  handleSubmit = () => {
    const { countryDetails } = this.state;
    const reqData = {
      id: countryDetails.id,
      approvedVaccine: countryDetails.approvedVaccine,
      vaccinatinStartDate: countryDetails.vaccineStartDate,
      vaccineTimeline: countryDetails.vaccineTimeline,
      phase1: countryDetails.phase1,
      phase2: countryDetails.phase2,
      phase3: countryDetails.phase3,
      moreinfolink: countryDetails.moreInfoLink,
      vaccinelink: countryDetails.vaccineLink,
      officiallink: countryDetails.officialLink,
      otherParagraph: countryDetails.otherParagraph,
    };

    updateCountryDetails(reqData).then(data => {
      const { toggleModal, getCountries } = this.props;
      const { toastObj } = this.state;

      if (data && data.status === 200) {
        this.setState(
          { toastObj: { ...toastObj, toastMsg: data.message } },
          () => {
            toggleModal(false);
            getCountries();
          },
        );
      } else {
        this.setState({ toastObj: { ...toastObj, toastMsg: data.message } });
      }
      store.dispatch(toggleToast(this.state.toastObj));
    });
  };

  handleOnChange = (value, type) => {
    const { countryDetails } = this.state;
    countryDetails[type] = value;
    this.setState({ countryDetails }, () => console.log(this.state));
  };

  handleCloseEventModal = () => {
    this.handleToggleAlert(true);
  };

  handleToggleAlert = show => this.setState({ showAlert: show });

  isNotValid = () => {
    const { countryDetails } = this.state;
    if (countryDetails) {
      return false;
    } else {
      return true;
    }
  };

  render() {
    const { classes } = this.props;
    const { showAlert, countryDetails } = this.state;
    return (
      <div id="editCovidCountry">
        <div className="createCovidAlert__header">
          <button onClick={this.handleCloseEventModal} className="timesClose">
            &times;
          </button>
        </div>
        <Divider />
        <div className="createCovidAlert__body">
          {countryItems.map(item => {
            return (
              <FormGroup key={item.id}>
                <TextField
                  required={item.isRequired}
                  id={item.id}
                  label={item.label}
                  fullWidth={item.fullWidth}
                  multiline={item.multiline}
                  rows={item.rows}
                  rowsMax={item.rowsMax}
                  value={countryDetails[item.id]}
                  onChange={e => this.handleOnChange(e.target.value, item.id)}
                  defaultValue={countryDetails[item.id]}
                  margin="normal"
                  variant="outlined"
                  InputLabelProps={{
                    shrink: true,
                    classes: {
                      root: classes.rootLabel,
                    },
                  }}
                  InputProps={{
                    classes: {
                      root: classes.vaccinatedTextField,
                      input: classes.rootInput,
                      notchedOutline: classes.notchedOutline,
                    },
                    endAdornment: null,
                  }}
                />
              </FormGroup>
            );
          })}
        </div>
        <Divider />
        <div className="createCovidAlert__footer">
          <div className="createCovidAlert__footer__btnWrapper">
            <button
              disabled={this.isNotValid()}
              onClick={this.handleSubmit}
              className="btn btnSubmit"
            >
              Submit
            </button>
            <button
              onClick={this.handleCloseEventModal}
              className="btn btnClose"
            >
              Close
            </button>
          </div>
        </div>
        {showAlert && (
          <AlertDialog
            isOpen={showAlert}
            title="Cancel Changes"
            description="You are about to cancel event changes, you won't be able to get them back, are you sure?"
            parentEventHandler={() => this.props.toggleModal(false)}
            handleClose={() => this.handleToggleAlert(false)}
          />
        )}
      </div>
    );
  }
}

export default EditCovidCountry;
