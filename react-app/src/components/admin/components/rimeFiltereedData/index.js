import React, { Component } from 'react';
import { fetchApi } from '../../../../helpers/http/fetch';
import { withCookies } from 'react-cookie';
import './style.scss';
import { connect } from 'react-redux';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import injectSheet from 'react-jss';
import styles from './style';
import RimeFilteredCard from '../rimeFilteredCard';
import SearchBox from '../../../ui/searchBox';
import { rimeFilteredFeedSearch } from '../../../../helpers/utils';
import Loader from '../../../ui/loader';

@injectSheet(styles)
@withCookies
@connect(
  state => {
    return {
      rime: state.rime,
      themeChange: state.themeChange,
    };
  },
  dispatch => {
    return {
      dispatch,
    };
  },
)
class RimeFiltereedFeed extends Component {
  state = {
    rimeFilteredFeed: [],
    finalFeed: [],
    selectedHour: '',
    searchValue: '',
    isFetching: false,
    hoursList: Array.from({ length: 999 }, (_, i) => i + 1),
  };

  handleChange = e => {
    let value = e.target.value;
    this.setState({ isFetching: true });
    this.setState({ selectedHour: value }, () => {
      this.getRimeFilteredFeed(value);
    });
  };
  getHoursList = () => {
    let { hoursList } = this.state;
    let hoursDropdown = [];
    hoursDropdown = hoursList.map(i => <MenuItem value={i}>{i}</MenuItem>);
    return hoursDropdown;
  };

  handleInputChange = e => {
    let value = e.target.value;
    this.setState(
      state => {
        return {
          searchValue: value,
        };
      },
      () => {
        this.handleSearch();
      },
    );
  };

  handleSearch = () => {
    let { searchValue, rimeFilteredFeed, finalFeed } = this.state;
    rimeFilteredFeed.forEach(item => {
      delete item['eventid'];
    });
    finalFeed.forEach(item => {
      delete item['eventid'];
    });
    if (searchValue) {
      let searchedList = rimeFilteredFeedSearch(
        this.state.searchValue,
        rimeFilteredFeed,
      );
      this.setState({ finalFeed: searchedList });
    } else {
      this.setState({ finalFeed: rimeFilteredFeed });
    }
  };

  refreshEditState = editedEvent => {
    this.getRimeFilteredFeed(this.state.selectedHour);
    let filteredFeed = this.state.finalFeed;
    filteredFeed.filter(item => item.id === editedEvent.id);
    this.setState({ finalFeed: [{ editedEvent }, ...filteredFeed] });
  };

  refreshState = id => {
    this.getRimeFilteredFeed(this.state.selectedHour);
    let filteredFeed = this.state.finalFeed;
    filteredFeed.filter(item => item.id === id);
    this.setState({ finalFeed: filteredFeed });
  };

  getRimeFilteredFeed = id => {
    let { rimeFilteredFeed, finalFeed } = this.state;
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let reqObj = {
      method: 'GET',
      isAuth: true,
      authToken,
      url: API_ROUTES.getFilteredFeed + '/' + id,
    };

    fetchApi(reqObj)
      .then(data => {
        this.setState({
          rimeFilteredFeed: data,
          finalFeed: data,
          isFetching: false,
        });
      })
      .catch(e => console.log(e));
  };

  render() {
    const { classes } = this.props;
    const { searchValue, finalFeed } = this.state;

    return (
      <div id="rimeScreening">
        {this.state.isFetching && <Loader />}
        <div className="rimeFilteredTopNavbar">
          <FormControl style={{ width: '110px' }}>
            <InputLabel
              classes={{
                root: classes.rootLabel,
              }}
              htmlFor="select-hours"
            >
              Select Hours
            </InputLabel>
            <Select
              value={this.state.selectedHour}
              onChange={this.handleChange}
              inputProps={{
                shrink: false,
                name: 'Select Hours',
                id: 'select-hours',
                classes: {
                  root: classes.rootInput,
                  input: classes.rootInput,
                  select: classes.rootSelect,
                  icon: classes.rootIcon,
                },
              }}
            >
              {this.getHoursList()}
            </Select>
          </FormControl>
        </div>
        <div className="rimeFilteredDataBody">
          <div className="rimeFilteredData__searchbox">
            <SearchBox
              placeholder={'Search by keyword'}
              parentHandler={e => this.handleInputChange(e)}
              value={searchValue}
            />
          </div>
          <div className="screeningEventList">
            {finalFeed?.length > 0 ? (
              finalFeed.map((item, index) => {
                return (
                  <RimeFilteredCard
                    id={index}
                    srno={index + 1}
                    eventId={item.id}
                    title={item.eventTitle}
                    sourceLinks={item.eventAllNewsLinks}
                    location={item.eventLoc}
                    noOfArticles={item.count}
                    categoryId={item.eventPrimaryRiskCategory}
                    secondaryCatergoryId={item.eventSecondaryRiskCategory}
                    link={item.eventLink}
                    userName={item.userName}
                    hasClose={false}
                    eventDate={item.eventDate}
                    lastUpdatedDate={item.eventLatestUpdate}
                    eventType={item.eventType}
                    imgURL={item.eventImageUrl}
                    tags={item.eventTags}
                    language={item.language}
                    handleDelete={this.refreshState}
                    handleEdit={this.refreshEditState}
                    selectedFeed={item}
                  />
                );
              })
            ) : (
              <div className="rimeFilteredData_notfound"> No feed Found</div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default RimeFiltereedFeed;
