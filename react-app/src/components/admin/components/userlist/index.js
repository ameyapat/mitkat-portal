import React, { Component } from 'react';
import './style.scss';
//components
import UserlistItem from './UserlistItem';
//helpers
import { userlist } from '../../helpers/constants';

export default class UserList extends Component {

    getUserList = () => {
        let userXML = [];
        
        userXML = userlist.map((item, index) => {
            return <UserlistItem 
                    key={index} 
                    title={item.title}
                    date={item.date}
                    location={item.location}
                    scheduled={item.scheduled}
                />
        })

        return userXML;
        
    }

  render() {
    return (
      <div id="userList">
        <ul>
        { this.getUserList() }
        </ul>
      </div>
    )
  }
}
