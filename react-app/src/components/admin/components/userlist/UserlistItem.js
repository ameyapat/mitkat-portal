import React from 'react';

const UserlistItem = ({title, date, location, scheduled}) => <li>
    <div>
        <h3>{title}</h3>
    </div>
    <div>
        <p>{date}</p>
    </div>
    <div>
        <p>{location}</p>
    </div>
    <div>
        <p>{scheduled}</p>
    </div>
</li>

export default UserlistItem;