import { API_ROUTES } from "../../../../helpers/http/apiRoutes";
import { fetchApi } from "../../../../helpers/http/fetch";

export const getMonths = (authToken) => {
    return new Promise((res, rej) => {
      const reqObj = {
        url: API_ROUTES.getMonthsGodrejAdmin,
        isAuth: true,
        authToken,
        method: "POST",
        showToggle: true,
      };

      fetchApi(reqObj)
        .then(data => {
          if (data.status === "200") {
            res(data.output);
          }
        })
        .catch(e => rej(e));
    });
  };

  export const getCountries = (authToken) => {
    return new Promise((res, rej) => {
      const reqObj = {
        url: API_ROUTES.countriesGodrejAdmin,
        isAuth: true,
        authToken,
        method: "POST",
        showToggle: true,
      };

      fetchApi(reqObj)
        .then(data => {
          if (data.status === "200") {
            res(data.output);
          }
        })
        .catch(e => rej(e));
    });
  };

  export const getBizGroups = (authToken) => {
    return new Promise((res, rej) => {
      const reqObj = {
        url: API_ROUTES.bizGrpsGodrejAdmin,
        isAuth: true,
        authToken,
        method: "POST",
        showToggle: true,
      };

      fetchApi(reqObj)
        .then(data => {
          if (data.status === "200") {
            res(data.output);
          }
        })
        .catch(e => rej(e));
    });
  };

  export const getRiskLevel = (authToken) => {
    return new Promise((res, rej) => {
      const reqObj = {
        url: API_ROUTES.riskLevelGodrejAdmin,
        isAuth: true,
        authToken,
        method: "POST",
        showToggle: true,
      };

      fetchApi(reqObj)
        .then(data => {
          if (data.status === "200") {
            res(data.output);
          }
        })
        .catch(e => rej(e));
    });
  };

  export const getPestelCategories = (authToken) => {
    return new Promise((res, rej) => {
      const reqObj = {
        url: API_ROUTES.pestelCatGodrejAdmin,
        isAuth: true,
        authToken,
        method: "POST",
        showToggle: true,
      };

      fetchApi(reqObj)
        .then(data => {
          if (data.status === "200") {
            res(data.output);
          }
        })
        .catch(e => rej(e));
    });
  };

  export const setLabelValues = (type, data) => {
    switch (type) {
      case "months":
        return data.map(item => ({ label: item.month, value: item.id }));
      case "countries":
        return data.map(item => ({
          label: item.countryname,
          value: item.id
        }));
      case "businesses":
        return data.map(item => ({
          label: item.businessName,
          value: item.id
        }));
      case "riskLevels":
        return data.map(item => ({
          label: item.riskLevel,
          value: item.id
        }));
      case "pestelCategories":
        return data.map(item => ({
          label: item.category,
          value: item.id
        }));
      default:
        return [{ label: '', value: '' }];
    }
  };