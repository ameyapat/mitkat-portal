import React, { Component } from 'react';
import './style.scss';
//react google places
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';
import Select from 'react-select';
import MultiSelect from '../../../ui/multiSelect';
import TextField from '@material-ui/core/TextField';
import Radio from '@material-ui/core/Radio';
import Button from '@material-ui/core/Button';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import CustomLabel from '../../../ui/customLabel';
import AlertDialog from '../../../ui/alertDialog';
import { withRouter } from 'react-router-dom';
import { withCookies } from 'react-cookie';
import { fetchApi } from '../../../../helpers/http/fetch';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import store from '../../../../store';
import { toggleToast, refreshEvents } from '../../../../actions/';
import injectSheet from 'react-jss';
import style from './style';
import { connect } from 'react-redux';
import { DatePicker } from 'material-ui-pickers';
import EditableItem from './EditableItem';
import {
  getMonths,
  getCountries,
  getBizGroups,
  getRiskLevel,
  getPestelCategories,
  setLabelValues,
} from './utils';
import { getArrFromObj } from '../../../../helpers/utils';
import { FormGroup } from '@material-ui/core';
import clsx from 'clsx';
import Sortable from 'sortablejs';

@withCookies
@withRouter
@injectSheet(style)
@connect(state => {
  return {
    refreshEvents: state.appState.refreshEvents,
  };
})
class CreateRiskWatch extends Component {
  state = {
    description: '',
    descList: [],
    descListItem: '',
    descItemPriority: '',
    recommendations: '',
    recomListItem: '',
    recomItemPriority: '',
    recomList: [],
    eventSource: '',
    eventTitle: '',
    bizImpact: '',
    impactList: [],
    impactListItem: '',
    impactItemPriority: '',
    showAlert: false,
    location: {
      lat: '',
      lng: '',
    },
    isLive: false,
    months: [],
    countries: [],
    bizGroups: [],
    riskLevels: [],
    pestelCategories: [],
    selectedMonth: '',
    selectedPestelCat: '',
    selectedBusinessGroup: '',
    selectedRiskLevel: [],
    selectedCountries: '',
    eventDate: new Date(),
  };

  //   validator = new FormValidator(validationRules);
  //   submitted = false;

  componentDidMount() {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    Promise.all([
      getMonths(authToken),
      getCountries(authToken),
      getBizGroups(authToken),
      getRiskLevel(authToken),
      getPestelCategories(authToken),
    ]).then(values => {
      this.setState(
        {
          months: setLabelValues('months', values[0]),
          countries: setLabelValues('countries', values[1]),
          bizGroups: setLabelValues('businesses', values[2]),
          riskLevels: setLabelValues('riskLevels', values[3]),
          pestelCategories: setLabelValues('pestelCategories', values[4]),
        },
        () => console.log(this.state),
      );
    });
  }

  componentDidUpdate() {
    const { descList, impactList, recomList } = this.state;
    console.log('component did mount called!');
    if (descList.length > 0) {
      const el = document.getElementById('descriptionList');
      this.createSortableElement(el);
    }

    if (impactList.length > 0) {
      const el = document.getElementById('impactListWrapper');
      this.createSortableElement(el);
    }

    if (recomList.length > 0) {
      const el = document.getElementById('recomList');
      this.createSortableElement(el);
    }
  }

  createSortableElement = el => {
    console.log(el, 'element');
    Sortable.create(el, {
      animation: 150,
      easing: 'cubic-bezier(1, 0, 0, 1)',
      ghostClass: 'activeIndex',
      dragClass: 'sortable-drag',
      chosenClass: 'sortable-chosen',
      onStart: function(evt) {
        console.log(evt.oldIndex, 'oldIndex');
      },
      onEnd: function(evt) {
        console.log(evt.newIndex, 'new index');
      },
    });
  };

  handleInputChange = (e, type) =>
    this.setState({ [type]: e.target.value }, () => console.log(this.state));

  handleDateChange = (type, value) => this.setState({ [type]: value });

  handleAlert = show => this.setState({ showAlert: show });

  handleCloseModal = () => {
    this.setState({ showAlert: false });
    this.props.closeEventModal('createRiskWatch');
  };

  handleMultiSelect = (value, type) => this.setState({ [type]: value });

  handleSingleSelect = (value, type) => {
    this.setState({ [type]: value });
  };

  handleAddressChange = address => this.setState({ address });
  /** function for setting location lat & lng */
  handleSelect = address => {
    geocodeByAddress(address)
      .then(results => getLatLng(results[0]))
      .then(latLng => {
        let { location } = this.state;
        location['lat'] = latLng.lat;
        location['lng'] = latLng.lng;
        this.setState({ location, address });
      })
      .catch(error => {
        console.error('Error', error);
        let toastObj = {
          showToast: true,
          toastObj: 'Error getting location, try again!',
        };
        store.dispatch(toggleToast(toastObj));
      });
  };

  addItemToList = type => {
    switch (type) {
      case 'description':
        let { descList, descListItem, descItemPriority } = this.state;
        descList.push({
          bulletpoint: descListItem,
          priority: parseInt(descItemPriority),
        });
        this.setState(
          { descList, descListItem: '', descItemPriority: '' },
          () => console.log(this.state),
        );
        break;
      case 'impact':
        let { impactList, impactListItem, impactItemPriority } = this.state;
        impactList.push({
          bulletpoint: impactListItem,
          priority: parseInt(impactItemPriority),
        });
        this.setState({
          impactList,
          impactListItem: '',
          impactItemPriority: '',
        });
        break;
      case 'recommendation':
        let { recomList, recomListItem, recomItemPriority } = this.state;
        recomList.push({
          bulletpoint: recomListItem,
          priority: parseInt(recomItemPriority),
        });
        this.setState({ recomList, recomListItem: '', recomItemPriority: '' });
        break;
      default:
        console.log('default');
        break;
    }
  };

  updateListItem = (type, updatedList) => {
    switch (type) {
      case 'description':
        this.setState({ descList: updatedList }, () => console.log(this.state));
        break;
      case 'impact':
        this.setState({ impactList: updatedList });
        break;
      case 'recommendation':
        this.setState({ recomList: updatedList });
        break;
      default:
        console.log('err!!');
        break;
    }
  };

  renderBulletPoints = type => {
    switch (type) {
      case 'description':
        let { descList } = this.state;
        return descList.map((d, idx) => (
          <EditableItem
            key={`${d.priority}_${idx}`}
            idx={idx}
            updateList={this.updateListItem}
            description={d.bulletpoint}
            list={descList}
            type={type}
          />
        ));
      case 'impact':
        let { impactList } = this.state;
        return impactList.map((d, idx) => (
          <EditableItem
            key={`${d.priority}_${idx}`}
            idx={idx}
            updateList={this.updateListItem}
            description={d.bulletpoint}
            list={impactList}
            type={type}
          />
        ));
      case 'recommendation':
        let { recomList } = this.state;
        return recomList.map((d, idx) => (
          <EditableItem
            key={`${d.priority}_${idx}`}
            idx={idx}
            updateList={this.updateListItem}
            description={d.bulletpoint}
            list={recomList}
            type={type}
          />
        ));
      default:
        console.log('default...');
        break;
    }
  };

  handleEventSubmit = e => {
    e.preventDefault();
    let { history } = this.props;

    this.submitted = true;
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let {
      eventTitle,
      description,
      bizImpact,
      recommendations,
      eventSource,
      location: { lat, lng },
      eventDate,
      selectedRiskLevel,
      selectedPestelCat,
      isLive,
      selectedMonth,
      selectedCountries,
      selectedBusinessGroup,
      descList,
      impactList,
      recomList,
    } = this.state;

    const eventDetails = {
      title: eventTitle,
      description: description,
      impact: bizImpact,
      recommendation: recommendations,
      links: eventSource,
      latitude: lat,
      longitude: lng,
      eventdate: eventDate,
      riskLevel: selectedRiskLevel.value, //add helper method here
      pestelCategory: selectedPestelCat.value,
      liveevent: isLive,
      month: selectedMonth.value,
      country: selectedCountries.value,
      // businessgroup: selectedBusinessGroup.value,
      businessgroups: getArrFromObj(selectedBusinessGroup),
      descriptionbullets: descList,
      impactbullets: impactList,
      recommendationbullets: recomList,
    };

    let reqObj = {
      url: API_ROUTES.addGodrejEventAdmin,
      data: eventDetails,
      isAuth: true,
      authToken,
      method: 'POST',
      showToggle: true,
    };

    fetchApi(reqObj).then(data => {
      if (data && data.status === 200) {
        let toastObj = {
          toastMsg: 'Event Created Successfully!',
          showToast: true,
        };
        this.props.closeEventModal('createRiskWatch');
        store.dispatch(toggleToast(toastObj));
        history.push(this.props.match.url + '/viewRiskWatch');
        //   store.dispatch(refreshEvents(true));
      } else if (data && data.status === 502) {
        let toastObj = {
          toastMsg: 'Something went wrong, Please try again',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      }
    });
  };

  render() {
    const { classes } = this.props;
    const {
      riskLevels,
      countries,
      selectedCountries,
      selectedRiskLevel,
      descList,
      description,
      descListItem,
      impactList,
      impactListItem,
      showAlert,
      recomList,
      selectedMonth,
      months,
      eventDate,
      pestelCategories,
      selectedPestelCat,
      selectedBusinessGroup,
      bizGroups,
      recomListItem,
    } = this.state;

    const customStyles = {
      control: provided => ({
        ...provided,

        backgroundColor: 'var(--backgroundSolid)',
        color: '#ffffff',
      }),
      option: (provided, state) => ({
        ...provided,
        zIndex: '99999',
      }),
      placeholder: provided => ({
        ...provided,

        color: '#ffffff',
      }),
      singleValue: provided => ({
        ...provided,
        color: '#ffffff',
      }),
    };
    return (
      <div className={'createEvent__inner'}>
        <h3>Create Risk Watch</h3>
        <div className={clsx('field--create--event', classes.riskWatchWrap)}>
          <section>
            <FormGroup
              classes={{ root: clsx(classes.formGroup, classes.monthWrap) }}
            >
              <CustomLabel
                title="Select Month"
                classes="select--label"
                isRequired={true}
              />
              <Select
                defaultValue={selectedMonth}
                value={selectedMonth}
                placeholder={'Select Month'}
                onChange={value =>
                  this.handleSingleSelect(value, 'selectedMonth')
                }
                options={months.length > 0 ? months : []}
                //styles={{ menu: styles => ({ ...styles, zIndex: 9999 }) }}
                styles={customStyles}
              />
            </FormGroup>
            <FormGroup classes={{ root: classes.formGroup }}>
              <DatePicker
                label="Event Date"
                value={eventDate}
                onChange={value => this.handleDateChange('eventDate', value)}
                animateYearScrolling
                InputLabelProps={{
                  shrink: true,
                  classes: {
                    root: classes.rootLabel,
                  },
                }}
                InputProps={{
                  classes: {
                    root: classes.rootInput,
                    input: classes.rootInput,
                    notchedOutline: classes.notchedOutline,
                  },
                  endAdornment: null,
                }}
              />
            </FormGroup>
            <FormGroup classes={{ root: classes.formGroup }}>
              <TextField
                required
                id="eventTitle"
                label="Event Title"
                multiline
                fullWidth
                rows="1"
                rowsMax="3"
                value={this.state.eventTitle}
                onChange={e => this.handleInputChange(e, 'eventTitle')}
                defaultValue="Enter Event Title"
                margin="normal"
                variant="outlined"
                InputLabelProps={{
                  shrink: true,
                  classes: {
                    root: classes.rootLabel,
                  },
                }}
                InputProps={{
                  maxLength: 5000,
                  classes: {
                    root: classes.rootInput,
                    input: classes.rootInput,
                    notchedOutline: classes.notchedOutline,
                  },
                  endAdornment: null,
                }}
              />
            </FormGroup>
            <FormGroup
              classes={{ root: clsx(classes.formGroup, classes.grpItems) }}
            >
              <CustomLabel
                title="Add Description Details"
                classes="select--label"
                isRequired={true}
              />
              <TextField
                id={'description'}
                label={'Description'}
                multiline
                fullWidth
                // rowsMax="6"
                rows="1"
                rowsMax="10"
                value={description}
                onChange={e => this.handleInputChange(e, 'description')}
                margin="normal"
                defaultValue="Enter Event Description"
                variant="outlined"
                InputLabelProps={{
                  shrink: true,
                  classes: {
                    root: classes.rootLabel,
                  },
                }}
                InputProps={{
                  maxLength: 5000,
                  classes: {
                    root: classes.rootInput,
                    input: classes.rootInput,
                    notchedOutline: classes.notchedOutline,
                  },
                  endAdornment: null,
                }}
              />
              <div className={classes.itemListWrapper}>
                <TextField
                  id="descList"
                  label="Add List Item"
                  multiline
                  fullWidth
                  rows="1"
                  rowsMax="10"
                  value={this.state.descListItem}
                  onChange={e => this.handleInputChange(e, 'descListItem')}
                  defaultValue="Enter List Item"
                  margin="normal"
                  variant="outlined"
                  InputLabelProps={{
                    shrink: true,
                    classes: {
                      root: classes.rootLabel,
                    },
                  }}
                  InputProps={{
                    classes: {
                      root: classes.rootInput,
                      input: classes.rootInput,
                      notchedOutline: classes.notchedOutline,
                    },
                    endAdornment: null,
                  }}
                />
              </div>
              <div className={classes.btnWrapAddItem}>
                <Button
                  color="secondary"
                  variant="outlined"
                  className={classes.btnAddItem}
                  disabled={!descListItem}
                  onClick={() => this.addItemToList('description')}
                >
                  Add Item
                </Button>
              </div>
              {descList.length > 0 && (
                <div className={classes.listItemWrapper}>
                  <h6 className={classes.listOfItems}>
                    <i className="fas fa-sort-numeric-up"></i> Drag to re-order
                    list of items
                  </h6>
                  <div id="descriptionList">
                    {this.renderBulletPoints('description')}
                  </div>
                </div>
              )}
            </FormGroup>
            <FormGroup
              classes={{ root: clsx(classes.formGroup, classes.grpItems) }}
            >
              <TextField
                id={'bizImpact'}
                label={'Business Impact'}
                multiline
                fullWidth
                rowsMax="6"
                value={this.state.bizImpact}
                onChange={e => this.handleInputChange(e, 'bizImpact')}
                margin="normal"
                defaultValue="Enter Business Impact"
                variant="outlined"
                InputLabelProps={{
                  shrink: true,
                  classes: {
                    root: classes.rootLabel,
                  },
                }}
                InputProps={{
                  classes: {
                    root: classes.rootInput,
                    input: classes.rootInput,
                    notchedOutline: classes.notchedOutline,
                  },
                  endAdornment: null,
                }}
              />
              <div className={classes.itemListWrapper}>
                <TextField
                  id="impactList"
                  label="Add List Item"
                  multiline
                  fullWidth
                  rows="1"
                  rowsMax="10"
                  value={this.state.impactListItem}
                  onChange={e => this.handleInputChange(e, 'impactListItem')}
                  defaultValue="Enter List Item"
                  margin="normal"
                  variant="outlined"
                  InputLabelProps={{
                    shrink: true,
                    classes: {
                      root: classes.rootLabel,
                    },
                  }}
                  InputProps={{
                    classes: {
                      root: classes.rootText,
                      input: classes.rootInput,
                      notchedOutline: classes.notchedOutline,
                    },
                    endAdornment: null,
                  }}
                />
              </div>
              <div className={classes.btnWrapAddItem}>
                <Button
                  color="secondary"
                  variant="outlined"
                  className={classes.btnAddItem}
                  disabled={!impactListItem}
                  onClick={() => this.addItemToList('impact')}
                >
                  Add Item
                </Button>
              </div>
              {impactList.length > 0 && (
                <div className={classes.listItemWrapper}>
                  <h6 className={classes.listOfItems}>
                    <i className="fas fa-sort-numeric-up"></i> Drag to re-order
                    list of items
                  </h6>
                  <div id="impactListWrapper">
                    {this.renderBulletPoints('impact')}
                  </div>
                </div>
              )}
            </FormGroup>
            <FormGroup
              classes={{ root: clsx(classes.formGroup, classes.grpItems) }}
            >
              <TextField
                id={'recommendation'}
                label={'Business Recommendations'}
                multiline
                fullWidth
                rowsMax="6"
                value={this.state.recommendations}
                onChange={e => this.handleInputChange(e, 'recommendations')}
                margin="normal"
                defaultValue="Enter Business Recommendations"
                variant="outlined"
                InputLabelProps={{
                  shrink: true,
                  classes: {
                    root: classes.rootLabel,
                  },
                }}
                InputProps={{
                  classes: {
                    root: classes.rootInput,
                    input: classes.rootInput,
                    notchedOutline: classes.notchedOutline,
                  },
                  endAdornment: null,
                }}
              />
              <div className={classes.itemListWrapper}>
                <TextField
                  id="descList"
                  label="Add List Item"
                  multiline
                  fullWidth
                  rows="1"
                  rowsMax="10"
                  value={this.state.recomListItem}
                  onChange={e => this.handleInputChange(e, 'recomListItem')}
                  defaultValue="Enter List Item"
                  margin="normal"
                  variant="outlined"
                  InputLabelProps={{
                    shrink: true,
                    classes: {
                      root: classes.rootLabel,
                    },
                  }}
                  InputProps={{
                    classes: {
                      root: classes.rootText,
                      input: classes.rootInput,
                      notchedOutline: classes.notchedOutline,
                    },
                    endAdornment: null,
                  }}
                />
              </div>
              <div className={classes.btnWrapAddItem}>
                <Button
                  color="secondary"
                  variant="outlined"
                  className={classes.btnAddItem}
                  disabled={!recomListItem}
                  onClick={() => this.addItemToList('recommendation')}
                >
                  Add Item
                </Button>
              </div>
              {recomList.length > 0 && (
                <div className={classes.listItemWrapper}>
                  <h6 className={classes.listOfItems}>
                    <i className="fas fa-sort-numeric-up"></i> Drag to re-order
                    list of items
                  </h6>
                  <div id="recomList">
                    {this.renderBulletPoints('recommendation')}
                  </div>
                </div>
              )}
            </FormGroup>
            <FormGroup classes={{ root: classes.formGroup }}>
              <div id="reactGooglePlaces" className="google--places">
                <label className="location--txt">
                  <i className="icon-uniE9F1" /> Enter Event Location
                  <span className="asterisk">*</span>
                </label>
                <PlacesAutocomplete
                  value={this.state.address}
                  onChange={this.handleAddressChange}
                  onSelect={this.handleSelect}
                >
                  {({
                    getInputProps,
                    suggestions,
                    getSuggestionItemProps,
                    loading,
                  }) => (
                    <div>
                      {/* {validation.address.isInvalid && (
                        <span className="error--text">
                          {validation.address.message}
                        </span>
                      )} */}
                      <input
                        {...getInputProps({
                          placeholder: 'Search Places ...',
                          className: 'location-search-input',
                        })}
                      />
                      {suggestions && (
                        <div className="autocomplete-dropdown-container">
                          {loading && (
                            <div className="loader-places">
                              <p>Loading...</p>
                            </div>
                          )}
                          {suggestions.map(suggestion => {
                            const className = suggestion.active
                              ? 'suggestion-item--active'
                              : 'suggestion-item';
                            const style = suggestion.active
                              ? {
                                  backgroundColor: '#fafafa',
                                  cursor: 'pointer',
                                }
                              : {
                                  backgroundColor: '#ffffff',
                                  cursor: 'pointer',
                                };
                            return (
                              <div
                                {...getSuggestionItemProps(suggestion, {
                                  className,
                                  style,
                                })}
                              >
                                <span className="list--item">
                                  {suggestion.description}
                                </span>
                              </div>
                            );
                          })}
                        </div>
                      )}
                    </div>
                  )}
                </PlacesAutocomplete>
              </div>
            </FormGroup>
            <FormGroup classes={{ root: classes.formGroup }}>
              <CustomLabel
                title="Select Risk Level"
                classes="select--label"
                isRequired={true}
              />
              <Select
                defaultValue={selectedRiskLevel}
                className="select--options"
                value={selectedRiskLevel}
                placeholder={'Risk Level'}
                onChange={value =>
                  this.handleMultiSelect(value, 'selectedRiskLevel')
                }
                options={riskLevels.length > 0 ? riskLevels : []}
                //styles={{ menu: styles => ({ ...styles, zIndex: 9999 }) }}
                styles={customStyles}
              />
            </FormGroup>
            <FormGroup classes={{ root: classes.formGroup }}>
              <CustomLabel
                title="Select Pestel Category"
                classes="select--label"
                isRequired={true}
              />
              {/* <MultiSelect
                classes={{ root: classes.select }}
                defaultValue={selectedPestelCat}
                options={pestelCategories}
                placeholder={"Pestel Category"}
                parentEventHandler={value =>
                  this.handleMultiSelect(value, "selectedPestelCat")
                }
              /> */}
              <Select
                defaultValue={selectedPestelCat}
                value={selectedPestelCat}
                placeholder={'Pestel Category'}
                onChange={value =>
                  this.handleSingleSelect(value, 'selectedPestelCat')
                }
                options={pestelCategories.length > 0 ? pestelCategories : []}
                //styles={{ menu: styles => ({ ...styles, zIndex: 9999 }) }}
                styles={customStyles}
              />
            </FormGroup>
            <FormGroup classes={{ root: classes.formGroup }}>
              <CustomLabel
                title="Select Countries"
                classes="select--label"
                isRequired={true}
              />
              <Select
                defaultValue={selectedCountries}
                value={selectedCountries}
                placeholder={'Select Countries'}
                onChange={value =>
                  this.handleSingleSelect(value, 'selectedCountries')
                }
                options={countries.length > 0 ? countries : []}
                //styles={{ menu: styles => ({ ...styles, zIndex: 9999 }) }}
                styles={customStyles}
              />
            </FormGroup>
            <FormGroup classes={{ root: classes.formGroup }}>
              <CustomLabel
                title="Select Business Groups"
                classes="select--label"
                isRequired={true}
              />
              {/* <Select
                defaultValue={selectedBusinessGroup}
                value={selectedBusinessGroup}
                placeholder={"Select Business Groups"}
                onChange={value =>
                  this.handleSingleSelect(value, "selectedBusinessGroup")
                }
                options={bizGroups.length > 0 ? bizGroups : []}
                styles={{ menu: styles => ({ ...styles, zIndex: 9999 }) }}
              /> */}
              <MultiSelect
                defaultValue={selectedBusinessGroup}
                value={selectedBusinessGroup}
                placeholder={'Select Business Groups'}
                parentEventHandler={value =>
                  this.handleSingleSelect(value, 'selectedBusinessGroup')
                }
                options={bizGroups.length > 0 ? bizGroups : []}
                //styles={{ menu: styles => ({ ...styles, zIndex: 9999 }) }}
                styles={customStyles}
              />
            </FormGroup>
            <FormControl component="fieldset">
              <CustomLabel
                title="Is Event Live"
                classes="select--label"
                isRequired={true}
              />
              <RadioGroup
                aria-label="isLive"
                name="isLive"
                value={this.state.isLive === 'true' ? true : false}
                onChange={e => this.handleInputChange(e, 'isLive')}
                classes={{ root: classes.rootRadioBtns }}
              >
                <FormControlLabel
                  value={true}
                  control={<Radio />}
                  label="Yes"
                  className={classes.radioLabel}
                />
                <FormControlLabel
                  value={false}
                  control={<Radio />}
                  label="No"
                  className={classes.radioLabel}
                />
              </RadioGroup>
            </FormControl>
            <FormGroup>
              <TextField
                id="eventSources"
                label="Event Sources"
                multiline
                fullWidth
                rows="1"
                rowsMax="10"
                value={this.state.eventSource}
                onChange={e => this.handleInputChange(e, 'eventSource')}
                defaultValue="Enter Event Source"
                margin="normal"
                variant="outlined"
                InputLabelProps={{
                  shrink: true,
                  classes: {
                    root: classes.rootLabel,
                  },
                }}
                InputProps={{
                  classes: {
                    root: classes.rootInput,
                    input: classes.rootInput,
                    notchedOutline: classes.notchedOutline,
                  },
                  endAdornment: null,
                }}
              />
            </FormGroup>
          </section>
        </div>
        <div className="actionables">
          <Button
            onClick={e => this.handleAlert(true)}
            variant="contained"
            color="default"
          >
            Cancel
          </Button>
          <Button
            onClick={e => this.handleEventSubmit(e)}
            style={{ marginLeft: 20, backgroundColor: '#2980b9' }}
            variant="contained"
            color="secondary"
          >
            Create Risk Watch
          </Button>
        </div>
        {showAlert && (
          <AlertDialog
            isOpen={showAlert}
            title="Cancel Changes"
            description="You are about to cancel event changes, you won't be able to get them back, are you sure?"
            parentEventHandler={e => this.handleCloseModal()}
            handleClose={() => this.handleAlert(false)}
          />
        )}
      </div>
    );
  }
}

export default CreateRiskWatch;
