import React, { useState, useEffect } from 'react';
import TextField from '@material-ui/core/TextField';
import clsx from 'clsx';
import injectSheet from 'react-jss';

const styles = {
  notEditable: {
    border: 0,
    padding: 0,
    cursor: 'move',
  },
  rootEditableItem: {
    display: 'flex',
    alignItems: 'center',
    borderBottom: '1px solid #d3d3d3',
    '&:last-child': {
      borderBottom: 'none',
    },
    '&:hover': {
      cursor: 'move',
      // background: 'rgba(200, 235, 251, 0.7)',
    },
  },
  formControl: {
    border: 0,
  },
  btnDelete: {
    '& i': {
      color: '#e55039',
    },
  },
  btnSave: {
    '& i': {
      color: '#e58e26',
    },
    '& i.fa-save': {
      color: '#81CA1E',
    },
  },
  btn: {
    margin: '0 5px',
    background: 'transparent',
    padding: 8,
    border: '1px solid #d3d3d3',
    color: '#666',
    borderRadius: 3,
    textTransform: 'capitalize',
    cursor: 'pointer',
  },
  inputNonEditable: {
    userSelect: 'none',
    '&:hover': {
      cursor: 'not-allowed',
      // background: 'transparent'
    },
  },
  rootInput: {
    '&~activeIndex': {
      background: 'red',
    },
  },
  rootText: {
    color: 'var(--whiteHighEmp) !important ',
    margin: '15px 0',
  },
};

const EditableItem = props => {
  const { description, type, classes, idx, updateList } = props;
  const [value, setInputValue] = useState(description);
  const [isEditable, setIsEdit] = useState(false);

  useEffect(() => {
    setInputValue(description);
  }, [description]);

  function saveChange() {
    const { list, idx } = props;
    list[idx]['bulletpoint'] = value;
    setIsEdit(false);
  }

  function handleRemove(type, idx) {
    const { list } = props;
    list.splice(idx, 1);
    updateList(type, list);
  }

  return (
    <div className={clsx(classes.rootEditableItem)}>
      <TextField
        fullWidth
        multiline
        rows="2"
        rowsMax="10"
        value={value}
        disabled={!isEditable}
        onChange={e => setInputValue(e.target.value)}
        InputLabelProps={{
          shrink: true,
          classes: {
            root: classes.rootLabel,
          },
        }}
        InputProps={{
          classes: {
            root: classes.rootText,
            disabled: classes.rootText,
            input: clsx(
              classes.rootInput,
              !isEditable && classes.inputNonEditable,
            ),
            formControl: classes.formControl,
            notchedOutline: isEditable
              ? classes.isEditable
              : classes.notEditable,
          },
        }}
        classes={{
          root: clsx(
            classes.rootTextItem,
            isEditable ? classes.isEditable : classes.notEditable,
          ),
        }}
        margin="normal"
        variant="outlined"
      />
      <button
        className={clsx(classes.btn, classes.btnSave)}
        onClick={() => (isEditable ? saveChange() : setIsEdit(!isEditable))}
      >
        {isEditable ? (
          <i className="far fa-save" />
        ) : (
          <i className="icon-uniE93E" />
        )}
      </button>
      {!isEditable && (
        <button
          className={clsx(classes.btn, classes.btnDelete)}
          onClick={() => handleRemove(type, idx)}
        >
          <i className="fas fa-trash-alt" />
        </button>
      )}
    </div>
  );
};

export default injectSheet(styles)(EditableItem);
