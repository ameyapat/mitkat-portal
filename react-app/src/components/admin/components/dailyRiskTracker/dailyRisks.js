import React from 'react';
//component
import TableList from '../../../ui/tablelist';
import TableListRow from '../../../ui/tablelist/TableListRow';
import TableListCell from '../../../ui/tablelist/TableListCell';
import Status from '../../../ui/status';
//helpers
import injectSheet from 'react-jss';
import { headerItems } from './constants';
//style
import styles from './style';
import TableHead from '../../../ui/tableHead';
import clsx from 'clsx';

//
import { downloadEventDetailsPdf } from '../../../../api/api';

const DailyRisks = ({
  items,
  classes,
  handleEditEvent,
  handleShowAlert,
  handleUploadImage,
  handleAddNewEvent,
}) => (
  <div className={classes.rootDailyRisks}>
    <TableHead items={headerItems} />
    <TableList>
      {items.map((item, idx) => (
        <TableListRow key={item.id}>
          <TableListCell value={item.eventdate} />
          <TableListCell value={item.title} />
          <TableListCell value={item.country} />
          <TableListCell value={item.state} />
          <TableListCell value={item.city} />
          <TableListCell value={item.status}>
            <Status type={item.status.toLowerCase()} />
          </TableListCell>
          <TableListCell value={item.creator} />
          <TableListCell value={item.storytype} />
          <TableListCell classes="btn--actions--wrap">
            <button
              className={clsx(classes.btnUpload, classes.btn)}
              onClick={() => handleUploadImage(item.id)}
            >
              <i className="fas fa-file-upload"></i>
            </button>
            <button
              className={clsx(classes.btnEdit, classes.btn)}
              onClick={() => handleEditEvent(item.id)}
            >
              <i className="icon-uniE93E"></i>
            </button>
            <button
              className={clsx(classes.btnDelete, classes.btn)}
              onClick={() => handleShowAlert(item.id)}
            >
              <i className="icon-uniE949"></i>
            </button>
            <button
              className={clsx(classes.btnNewEvent, classes.btn)}
              onClick={() => handleAddNewEvent(item.id)}
            >
              <i className="fa fa-plus-square-o"></i>
            </button>
            <button
              className={clsx(classes.btnPdf, classes.btn)}
              onClick={() => downloadEventDetailsPdf(item.id)}
            >
              <i className="far fa-file-pdf"></i>
            </button>
          </TableListCell>
        </TableListRow>
      ))}
    </TableList>
  </div>
);

export default injectSheet(styles)(DailyRisks);
