export default {
  rootDailyRisk: {
    background: 'var(--backgroundSolid)',
    padding: 20,
  },
  innerDailyRisk: {
    background: 'var(--backgroundSolid)',
    padding: 10,
  },
  dateWrap: {
    borderBottom: '1px solid #f1f1f1',
    padding: '15px 0',
    marginBottom: 15,
    '& i': {
      fontSize: 15,
      color: 'var(--whiteMediumEmp)',
      marginRight: 10,
    },
    '& h1': {
      paddingBottom: 15,
      fontSize: 16,
      color: 'var(--whiteMediumEmp)',
      fontWeight: 'normal',
    },
  },
  dateWrapInner: {
    padding: '10px 5px',
    border: '1px solid #f1f1f1',
    boxShadow: '0 6px 10px 0 #f1f1f1',
    borderRadius: 3,
    cursor: 'pointer',
    display: 'inline-block',
  },
  datePickerInput: {
    fontSize: 14,
    color: 'var(--whiteMediumEmp)',
    background: 'red',
  },
  rootDatePicker: {
    maxWidth: '25%',
    display: 'flex',
    marginBottom: 15,
  },
  rootLabel: {
    color: 'rgba(255,255,255, 0.67) !important',
  },
  riskWrapOutter: {
    '& img': {
      width: 256,
    },
  },
  rootDailyRisks: {
    borderTop: '1px dashed #f1f1f1',
    marginTop: 15,
  },
  btnSubmit: {
    margin: 0,
    border: '1px solid #2980b9',
    background: 'transparent',
    textTransform: 'uppercase',
    padding: 8,
    color: '#2980b9',
    borderRadius: 3,
    cursor: 'pointer',
    marginLeft: 15,
  },
  btnEdit: {
    color: '#e58e26',
    background: 'transparent',
    border: '1px solid #e58e26',
  },
  btnNewEvent: {
    color: '#2980b9',
    background: 'transparent',
    border: '1px solid #2980b9',
    '& .fa.fa-plus-square-o': {
      padding: '0 3px',
      color: '#2980b9',
    },
  },
  btnUpload: {
    color: '#e58e26',
    background: 'transparent',
    border: '1px solid #e58e26',

    '& .fas.fa-file-upload': {
      padding: '0 5px',
      color: '#e58e26',
    },
  },
  btnDelete: {
    color: '#e55039',
    background: 'transparent',
    border: '1px solid #e55039',
  },
  btnPdf: {
    color: '#e55039',
    background: 'transparent',
    border: '1px solid rgba(171, 193, 66, 0.5)',

    '& .far.fa-file-pdf': {
      padding: '0 5px',
      color: 'rgba(171, 193, 66, 0.5)',
    },
  },
  btn: {
    borderRadius: 3,
    padding: 5,
    margin: '0 2px',
    cursor: 'pointer',
  },
  txtField: {
    color: 'var(--whiteHighEmp) !important ',
    fontSize: 14,
  },
  datesWrapper: {
    display: 'flex',
    alignItems: 'center',
    // justifyContent: 'space-around'
  },
  rootSearchWrapper: {
    maxWidth: 256,
    margin: '0 auto',
  },
  rootAddForm: {
    background: '#fff',
    // minHeight: 512,
    height: 'auto',
    padding: 30,
    position: 'relative',
    maxWidth: 400,
    margin: '30px auto',

    '& .close-times': {
      color: '#000',
      fontSize: 30,
      position: 'absolute',
      right: 10,
      top: 10,
      cursor: 'pointer',
    },
  },
};
