import React, { Component } from 'react';
//components
import { DatePicker } from 'material-ui-pickers';
import DailyRisks from './dailyRisks';
import CustomModal from '../../../ui/modal';
import EditEventUI from '../createEventUI/EditEventUI';
import AlertDialog from '../../../ui/alertDialog';
import SearchBox from '../../../ui/searchBox';
//styles
import styles from './style';
//helpers
import { setHeight } from '../../helpers/utils';
import { fetchApi, uploadApi } from '../../../../helpers/http/fetch';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import store from '../../../../store';
import { toggleToast } from '../../../../actions';
import { advancedSearch } from '../../../../helpers/utils';
//npm packages
import injectSheet from 'react-jss';
import moment from 'moment';
import Empty from '../../../ui/empty';
import CustomImageUpload from '../../../ui/customImageUpload';
import { withCookies } from 'react-cookie';
//assets
import emptyImage from '../../../../assets/hugo-no-messages.png';
import AddEventUI from '../createEventUI/AddEventUI';

@withCookies
@injectSheet(styles)
class DailyRiskTracker extends Component {
  state = {
    divHeight: 0,
    selectedDate: moment(new Date()).format('YYYY-MM-DD'),
    dailyRiskList: [],
    defaultList: [],
    searchValue: '',
    showEventModal: false,
    showAddEventModal: false,
    eventDetails: null,
    showAlert: false,
    eventId: null,
    showUploadForm: false,
    selectedEventId: null,
  };

  componentDidMount() {
    const el = document.querySelector('#innerDailyRisk');
    const divHeight = setHeight(el);
    this.setState({ divHeight });
  }

  handleDateChange = (type, value) => this.setState({ [type]: value });

  getDailyRisk = () => {
    const { cookies } = this.props;
    let { selectedDate } = this.state;
    selectedDate = moment(selectedDate).format('YYYY-MM-DD');
    let authToken = cookies.get('authToken');
    let params = {
      url: `${API_ROUTES.riskTrackerNew}/${selectedDate}`,
      method: 'POST',
      isAuth: true,
      authToken: authToken,
      data: '',
      showToggle: true,
    };

    fetchApi(params)
      .then(data => {
        this.setState({ dailyRiskList: data, defaultList: data });
      })
      .catch(e => console.log(e));
  };

  handleDeleteEvent = () => {
    let { cookies } = this.props;
    const { eventId } = this.state;
    let authToken = cookies.get('authToken');
    let reqObj = {
      url: `${API_ROUTES.deleteTodaysEvent}/${eventId}`,
      isAuth: true,
      authToken,
      method: 'POST',
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        let toastObj = {
          toastMsg: data.message,
          showToast: data.success,
        };
        store.dispatch(toggleToast(toastObj));
      })
      .catch(e => {
        let toastObj = {
          toastMsg: 'Error deleting report',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      });
    this.setState({ showAlert: false, eventId: '' });
  };

  handleEditEvent = eventId => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');

    let reqObj = {
      method: 'POST',
      isAuth: true,
      authToken,
      url: `${API_ROUTES.viewEventHistory}/${eventId}`,
      showToggle: true,
    };

    fetchApi(reqObj).then(eventDetails => {
      this.setState({ eventDetails }, () => {
        this.showEventModal(true);
      });
    });
  };

  handleAddNewEvent = eventId => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');

    let reqObj = {
      method: 'POST',
      isAuth: true,
      authToken,
      url: `${API_ROUTES.viewEventHistory}/${eventId}`,
      showToggle: true,
    };

    fetchApi(reqObj).then(eventDetails => {
      this.setState({ eventDetails }, () => {
        this.showAddEventModal(true);
      });
    });
  };

  showAddEventModal = show => this.setState({ showAddEventModal: show });
  showEventModal = show => this.setState({ showEventModal: show });

  handleShowAlert = eventId => this.setState({ eventId, showAlert: true });

  handleCloseAlert = () => this.setState({ showAlert: false, eventId: '' });

  handleInputChange = e => {
    let value = e.target.value;
    this.setState(
      state => {
        return {
          searchValue: value,
        };
      },
      () => {
        this.handleSearch();
      },
    );
  };

  handleSearch = () => {
    let { searchValue, defaultList } = this.state;

    if (searchValue) {
      let searchedList = advancedSearch(this.state.searchValue, defaultList);
      this.setState({ dailyRiskList: searchedList });
    } else {
      this.setState({ dailyRiskList: defaultList });
    }
  };

  handleUploadImage = selectedEventId => {
    this.setState({ selectedEventId });
    this.handleToggleShowForm(true);
  };

  handleToggleShowForm = showUploadForm => this.setState({ showUploadForm });

  handleReportUploadImage = data => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let { imageFile } = data;
    const formData = new FormData();

    formData.append('image', imageFile);

    let params = {
      url: `${API_ROUTES.uploadReportingImg}/${this.state.selectedEventId}`,
      reqObj: {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${authToken}`,
        },
        body: formData,
      },
    };

    uploadApi(params.url, params.reqObj).then(res => {
      let toastObj = {
        toastMsg: '',
        showToast: true,
      };
      if (res) {
        toastObj.toastMsg = res.message;
      }
      store.dispatch(toggleToast(toastObj));
    });
  };

  render() {
    const { classes } = this.props;
    const {
      divHeight,
      selectedDate,
      dailyRiskList,
      showEventModal,
      showAddEventModal,
      eventDetails,
      showAlert,
      searchValue,
      showUploadForm,
    } = this.state;

    return (
      <div id="innerDailyRisk" className={classes.rootDailyRisk}>
        <div
          className={classes.innerDailyRisk}
          style={{ height: divHeight - 40 }}
        >
          <div className={classes.dateWrap}>
            <h1>
              <i className="fas fa-list" /> Select date for daily risks
            </h1>
            <div className={classes.datesWrapper}>
              <DatePicker
                label="Select Date"
                value={selectedDate}
                onChange={value => this.handleDateChange('selectedDate', value)}
                animateYearScrolling
                classes={{
                  root: classes.rootDatePicker,
                  input: classes.datePickerInput,
                }}
                InputLabelProps={{
                  shrink: true,
                  classes: {
                    root: classes.rootLabel,
                  },
                }}
                InputProps={{
                  classes: {
                    input: classes.txtField,
                  },
                  // disableUnderline: true
                }}
              />
              <button
                className={classes.btnSubmit}
                onClick={() => this.getDailyRisk()}
              >
                Submit
              </button>
            </div>
          </div>
          <div className={classes.riskWrapOutter}>
            <div className={classes.rootSearchWrapper}>
              <SearchBox
                placeholder={'Search by keyword'}
                parentHandler={e => this.handleInputChange(e)}
                value={searchValue}
              />
            </div>
            {dailyRiskList.length > 0 ? (
              <DailyRisks
                items={dailyRiskList}
                handleEditEvent={this.handleEditEvent}
                handleShowAlert={this.handleShowAlert}
                handleUploadImage={this.handleUploadImage}
                handleAddNewEvent={this.handleAddNewEvent}
              />
            ) : (
              <Empty
                imgSrc={emptyImage}
                alt="No items available!"
                title="No items available!"
              />
            )}
          </div>
        </div>
        <CustomModal
          showModal={showEventModal}
          closeModal={() => this.showEventModal(false)}
        >
          <div id="edit-event--modal" className="modal__inner">
            {eventDetails && (
              <EditEventUI
                eventDetails={eventDetails}
                closeEventModal={this.showEventModal}
                showUpdateBtn={true}
                isApprover={false}
                hasAllTopics={true}
              />
            )}
          </div>
        </CustomModal>
        <CustomModal
          showModal={showAddEventModal}
          closeModal={() => this.showAddEventModal(false)}
        >
          <div id="edit-event--modal" className="modal__inner">
            {eventDetails && (
              <AddEventUI
                eventDetails={eventDetails}
                closeEventModal={this.showAddEventModal}
                showUpdateBtn={true}
                isApprover={false}
                hasAllTopics={true}
              />
            )}
          </div>
        </CustomModal>
        <CustomModal
          showModal={showUploadForm}
          closeModal={() => this.handleToggleShowForm(false)}
        >
          <div className={classes.rootAddForm}>
            <span
              className="close-times"
              onClick={() => this.handleToggleShowForm(false)}
            >
              &times;
            </span>
            <CustomImageUpload
              heading="Upload Reporting Image"
              imagePlaceHolder="Upload Reporting Image"
              handleSubmit={this.handleReportUploadImage}
            />
          </div>
        </CustomModal>
        {showAlert && (
          <AlertDialog
            isOpen={showAlert}
            title="Delete Event"
            description="You are about to delete an event, are you sure?"
            parentEventHandler={() => this.handleDeleteEvent()}
            handleClose={() => this.handleCloseAlert()}
          />
        )}
      </div>
    );
  }
}

export default DailyRiskTracker;
