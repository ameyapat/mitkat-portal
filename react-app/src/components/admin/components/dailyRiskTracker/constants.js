export const headerItems = [
    {
        id: 1,
        title: 'Event Date'
    },
    {
        id: 2,
        title: 'Subject'
    },
    {
        id: 3,
        title: 'Country'
    },
    {
        id: 4,
        title: 'State'
    },
    {
        id: 5,
        title: 'City'
    },
    {
        id: 6,
        title: 'Status'
    },
    {
        id: 7,
        title: 'Creator'
    },
    {
        id: 8,
        title: 'Story Type'
    },
    {
        id: 9,
        title: 'Actions'
    }
]