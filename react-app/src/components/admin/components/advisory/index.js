import React, { Component, Fragment } from 'react';
//
import styles from './style';
import InfoCardList from '../../../ui/infoCardList';
import InfoCard from '../../../ui/infoCard';
import CustomModal from '../../../ui/modal';
import AlertDialog from '../../../ui/alertDialog';
import CustomReportsForm from '../../../ui/customReportsForm';
import Empty from '../../../ui/empty';
//helpers
import injectSheet from 'react-jss';
import { withCookies } from 'react-cookie';
import { fetchApi, uploadApi } from '../../../../helpers/http/fetch';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import store from '../../../../store';
import { toggleToast } from '../../../../actions';
import Actionables from './Actionables';
import { getValuesFromObjects } from './utils';
import { getAdvisoryDetails } from '../../../../requestor/admin/requestor';


class Advisory extends Component {
  state = {
    advisoryList: [],
    modalUrl: '',
    showAlert: false,
    showAddForm: false,
    isEdit: false,
    advisoryDetails: {},
  };

  componentDidMount() {
    this.fetchAdvisoryList().then(data => {
      this.setState({ advisoryList: data });
    });
  }

  fetchAdvisoryList = () => {
    return new Promise((res, rej) => {
      let { cookies } = this.props;
      let authToken = cookies.get('authToken');
      let reqObj = {
        url: API_ROUTES.getAdvisoryListCreator,
        isAuth: true,
        authToken,
        method: 'POST',
        showToggle: true,
      };

      fetchApi(reqObj)
        .then(data => {
          if (data.success) {
            res(data.output);
          }
        })
        .catch(e => rej(e));
    });
  };

  getAdvisoryListXML = () => {
    let { advisoryList } = this.state;
    let advisoryListXML = advisoryList.map(l => {
      return (
        <InfoCard
          key={l.id}
          handleDelete={this.handleDelete}
          handleView={this.handleView}
          handleEdit={this.handleEdit}
          handleDownload={this.handleDownload}
          id={l.id}
          title={l.title}
          imgUrl={l.dispayurl}
          date={l.date}
        />
      );
    });

    return advisoryListXML;
  };

  handleDelete = id => {
    this.setState({
      showAlert: true,
      advisoryId: id,
    });
  };

  handleEdit = id => {
    this.setState(
      {
        advisoryId: id,
      },
      () => {
        this.fetchAdvisoryDetails();
      },
    );
  };

  fetchAdvisoryDetails = () => {
    const { advisoryId } = this.state;
    getAdvisoryDetails(advisoryId)
      .then(advisoryDetails => {
        this.setState({ advisoryDetails });
        this.handleEditForm({
          isEdit: true,
          showAddForm: true,
        });
      })
      .catch(e => console.log('Error fetching advisory details', e));
  };

  handleEditForm = ({ isEdit, showAddForm }) => {
    this.setState({
      isEdit,
      showAddForm,
    });
  };

  handleConfirmDelete = id => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let reqObj = {
      url: `${API_ROUTES.deleteAdvisory}/${id}`,
      isAuth: true,
      authToken,
      method: 'POST',
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        if (data.success) {
          this.setState(
            {
              showAlert: false,
            },
            () => {
              let toastObj = {
                toastMsg: 'Advisory Deleted Successfully!',
                showToast: data.success,
              };
              store.dispatch(toggleToast(toastObj));
              this.fetchAdvisoryList().then(data => {
                this.setState({ advisoryList: data });
              });
            },
          );
        }
      })
      .catch(e => console.log(e));
  };

  handleDownload = id => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let reqObj = {
      url: `${API_ROUTES.downloadAdvisory}/${id}`,
      isAuth: true,
      authToken,
      method: 'POST',
      isBlob: true,
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(blobObj => {
        let a = document.createElement('a');
        document.body.appendChild(a);
        let objUrl = new Blob([blobObj], { type: 'application/pdf' });
        let url = window.URL.createObjectURL(objUrl);
        a.href = url;
        a.download = 'advisory-pdf';
        a.click();
        window.URL.revokeObjectURL(url);
      })
      .catch(e => console.log(e));
  };

  handleView = id => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let reqObj = {
      url: `${API_ROUTES.viewAdvisory}/${id}`,
      isAuth: true,
      authToken,
      method: 'GET',
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        if (data.success) {
          this.setState({
            showModal: true,
            modalUrl: data.output.viewimageurl,
          });
        }
      })
      .catch(e => console.log(e));
  };

  toggleModal = showModal => this.setState({ showModal });

  handleCloseAlert = () => this.setState({ showAlert: false });

  handleToggleAdd = showAddForm => this.setState({ showAddForm });

  handleSubmit = data => {
    // addNewAdvisory
    let { cookies } = this.props;
    const { isEdit, advisoryId } = this.state;
    let authToken = cookies.get('authToken');
    let {
      imageFile,
      document,
      title,
      selectedLiveTopic: { value: topicid },
      selectedCountries,
      date,
    } = data;

    const formData = new FormData();

    formData.append('displaypic', imageFile);
    formData.append('title', title);
    formData.append('advisory', document);
    formData.append('topicid', topicid);
    formData.append('countries', getValuesFromObjects(selectedCountries));
    formData.append('date', date || '');

    if (isEdit) {
      formData.append('advisoryid', advisoryId);
    }

    let params = {
      url: `${!isEdit ? API_ROUTES.addNewAdvisory : API_ROUTES.updateAdvisory}`,
      reqObj: {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${authToken}`,
        },
        body: formData,
      },
    };

    uploadApi(params.url, params.reqObj).then(res => {
      if (res.success) {
        let toastObj = {
          toastMsg: !isEdit
            ? 'Advisory Added Successfully!'
            : 'Advisory Updated Successfully!',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
        this.handleToggleAdd();
        this.fetchAdvisoryList().then(data => {
          this.setState({ advisoryList: data, isEdit: false  });
        });
      } else {
        let toastObj = {
          toastMsg: 'Something went wrong!',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      }
    });
  };

  handleCloseModal = () => {
    this.handleToggleAdd(false)
    this.setState({isEdit: false})
  }

  render() {
    let { classes } = this.props;
    let {
      advisoryList,
      showModal,
      modalUrl,
      showAlert,
      advisoryId,
      showAddForm,
      isEdit,
      advisoryDetails,
    } = this.state;

    return (
      <Fragment>
        <Actionables handleAdd={this.handleToggleAdd} />
        <InfoCardList>
          {advisoryList.length > 0 ? (
            this.getAdvisoryListXML()
          ) : (
            <Empty title="No Advisory Available" />
          )}
          <CustomModal
            showModal={showModal}
            closeModal={() => this.toggleModal(false)}
          >
            <div className={classes.rootIframe}>
              <span
                className="close-times"
                onClick={() => this.toggleModal(false)}
              >
                &times;
              </span>
              <iframe src={modalUrl} height="100%" width="100%" title="pdf" />
            </div>
          </CustomModal>
          <CustomModal
            showModal={showAddForm}
            closeModal={() => this.toggleModal(false)}
          >
            <div className={classes.rootAddForm}>
              <span
                className="close-times"
                onClick={() => this.handleCloseModal()}
              >
                &times;
              </span>
              <CustomReportsForm
                heading="Add New Advisory"
                titlePlaceHolder="Enter title"
                imagePlaceHolder="Upload Display Image"
                filePlaceHolder="Upload Advisory"
                handleSubmit={this.handleSubmit}
                showTopics={true}
                showCountries={true}
                showDate={true}
                isEdit={isEdit}
                details={advisoryDetails}
                showLinks={true}
              />
            </div>
          </CustomModal>
          {showAlert && (
            <AlertDialog
              isOpen={showAlert}
              title="Delete Advisory"
              description="You are about to delete an event, are you sure?"
              parentEventHandler={() => this.handleConfirmDelete(advisoryId)}
              handleClose={() => this.handleCloseAlert()}
            />
          )}
        </InfoCardList>
      </Fragment>
    );
  }
}

export default withCookies(injectSheet(styles)(Advisory));
