import React from "react";
import injectSheet from 'react-jss';
//
import styles from './style';
//classnames & clsx
const Actionables = ({ handleAdd, classes }) => {
  return (
    <div className={classes.rootActionable}>
      <button onClick={() => handleAdd(true)}>
        Add New Advisory <i className="fas fa-plus" />
      </button>
    </div>
  );
};

export default injectSheet(styles)(Actionables);
