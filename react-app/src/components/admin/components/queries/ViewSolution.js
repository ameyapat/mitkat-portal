import React, { Component } from 'react';
//material ui
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
//
import './style.scss';
import { withStyles } from '@material-ui/core';
import styles from './style';
//
import moment from 'moment';

@withStyles(styles)
class ViewSolution extends Component {
  render() {
    let {
      queryDetails: {
        details,
        title,
        clientname,
        querytime,
        id,
        responseuser,
        response,
        status,
        responsetime,
        file,
      },
      classes,
    } = this.props;
    return (
      <div className="edit__solution">
        <div className="statusWrap">
          <span className={`symbol ${status ? 'resolved' : 'pending'}`}></span>
          <span className="status--msg">{status ? 'Resolved' : 'Pending'}</span>
          <span className="status--msg">
            Requested on {moment(querytime).format('LL')}
          </span>
        </div>
        <div className="form--group">
          <h2>{title}</h2>
        </div>
        <div className="form--group assignedWrap">
          <div>
            <label>Assigned To</label>
            <p>{responseuser}</p>
          </div>
          <div>
            <label>Response Time</label>
            <p>
              {responsetime
                ? moment(responsetime).format('LL')
                : 'Awaiting Response'}
            </p>
          </div>
          <div>
            <label>Client Name</label>
            <p>{clientname}</p>
          </div>
        </div>
        <div className="form--group summary">
          <label>Summary</label>
          <p>{details}</p>
        </div>
        <div className="form--group summary">
          <label>Solution</label>
          <p>{response ? response : 'Awaiting Response'}</p>
        </div>
        <div className="form--group summary">
          <label>File</label>
          <p>
            {file ? (
              <a href={`${file}`} target="_blank">
                View File
              </a>
            ) : (
              '-'
            )}
          </p>
        </div>
        <div className="actionables">
          <Button
            size="small"
            variant="outlined"
            className="btn--viewEdit"
            classes={{ root: classes.rootViewEdit }}
            onClick={() => this.props.closeEventModal(false)}
          >
            Cancel
          </Button>
        </div>
      </div>
    );
  }
}

export default ViewSolution;
