import React, { Component } from 'react'
import ViewSolution from './ViewSolution'
import EditSolution from './EditSolution'

//
import './style.scss'

export default class EditInfo extends Component {
  state = {
    queryDetails: {},
  };

  componentDidMount() {
    let { queryDetails } = this.props;
    this.setState({ queryDetails });
  }

  handleChange = (type, value) => {
    let { queryDetails } = this.state;
    queryDetails[type] = value;
    this.setState({ queryDetails });
  };

  handleUpdate = () => {
    let { queryDetails } = this.state;
    this.props.handleUpdate(queryDetails);
  };

  render() {
    let {
      queryDetails: { status },
      queryDetails,
    } = this.state;
    return (
      <div>
        {status === 1 ? (
          <ViewSolution
            queryDetails={queryDetails}
            closeEventModal={this.props.closeEventModal}
          />
        ) : (
          <EditSolution
            queryDetails={queryDetails}
            handleChange={this.handleChange}
            handleUpdate={this.handleUpdate}
            closeEventModal={this.props.closeEventModal}
          />
        )}
      </div>
    );
  }
}
