import React, { Component, Fragment } from 'react';
//components
import TableList from '../../../ui/tablelist';
import TableListCell from '../../../ui/tablelist/TableListCell';
import TableListRow from '../../../ui/tablelist/TableListRow';
import CustomModal from '../../../ui/modal';
import Empty from '../../../ui/empty';
import EditInfo from './EditInfo';
//material ui components
import Button from '@material-ui/core/Button';
//helpers
import { fetchApi } from '../../../../helpers/http/fetch';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import { withCookies } from 'react-cookie';
import store from '../../../../store';
import { toggleToast } from '../../../../actions';
import './style.scss';
import styles from './style';
import { withStyles } from '@material-ui/core';
import moment from 'moment';
import UploadBtn from './UploadBtn';

@withStyles(styles)
@withCookies
class Queries extends Component {
  state = {
    queryList: [],
    pageNo: 0,
    showModal: false,
    queryDetails: {},
  };

  componentDidMount() {
    this.getQueries();
  }

  handleEditEvent = id => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    //use fetch here
    let reqObj = {
      url: `${API_ROUTES.getQueryDetailsCreator}/${id}`,
      isAuth: true,
      authToken,
      method: 'POST',
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        this.setState({ queryDetails: data }, () => {
          this.handleModal(true);
        });
      })
      .catch(e => {
        let toastObj = {
          toastMsg: 'Error fetching details',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      });
  };

  getQueries = () => {
    let { cookies } = this.props;
    let { pageNo } = this.state;
    let authToken = cookies.get('authToken');
    let reqObj = {
      url: `${API_ROUTES.getQueryListCreator}/${pageNo}`,
      isAuth: true,
      authToken,
      method: 'POST',
      showToggle: true,
    };
    fetchApi(reqObj).then(data => {
      this.setState({
        queryList: data.content,
        isLast: data.last,
      });
    });
  };

  getTableItems = () => {
    let { classes } = this.props;
    let { queryList } = this.state;
    let itemXML = [];

    itemXML = queryList.map(i => {
      return (
        <TableListRow key={i.id}>
          <TableListCell value={moment(i.timestamp).format('LL') || '-'} />
          <TableListCell value={i.title || '-'} classes={'amd--title'} />
          <TableListCell value={i.clientname || '-'} />
          <TableListCell value={i.assignedto || '-'} />
          <TableListCell classes={{ root: classes.rootTCell }}>
            <span className={`${i.status ? 'resolved' : 'pending'}`}>
              {i.status ? 'Resolved' : 'Pending'}
            </span>
          </TableListCell>
          <TableListCell classes="btn--actions--wrap">
            <Button
              size="small"
              variant="outlined"
              classes={{ root: classes.rootViewEdit }}
              onClick={() => this.handleEditEvent(i.id)}
            >
              View / Edit
              {/* <i className="icon-uniE93E"></i> */}
            </Button>
            <div className="upload-button">
            <UploadBtn id={i.id} getQueries={this.getQueries} />
            </div>
          </TableListCell>
        </TableListRow>
      );
    });
    return itemXML;
  };

  handleModal = show => {
    this.setState({ showModal: show });
  };

  handleUpdate = queryDetails => {
    //
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let reqData = {
      id: queryDetails.id,
      response: queryDetails.response,
    };

    let reqObj = {
      url: API_ROUTES.addQueryResponseCreator,
      data: reqData,
      isAuth: true,
      authToken,
      method: 'POST',
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        let toastObj = {
          toastMsg: data.message,
          showToast: true,
        };
        this.handleModal(false);
        store.dispatch(toggleToast(toastObj));
        this.getQueries(); //refresh list
      })
      .catch(e => {
        let toastObj = {
          toastMsg: 'Error editing event!',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      });
  };

  render() {
    let { queryList, showModal, queryDetails } = this.state;

    return (
      <div className="queryWrap">
        <h2>Queries</h2>
        {queryList.length > 0 ? (
          <Fragment>
            <div className="tablelist__head">
              <div>
                <label>Date</label>
              </div>
              <div className="tl__title">
                <label>Title</label>
              </div>
              <div>
                <label>Client Name</label>
              </div>
              <div>
                <label>Assigned To</label>
              </div>
              <div>
                <label>Status</label>
              </div>
              <div>
                <label>Actions</label>
              </div>
            </div>
            <TableList>
              {queryList.length > 0 && this.getTableItems()}
            </TableList>
          </Fragment>
        ) : (
          <Empty title="No recent queries!" />
        )}

        <CustomModal
          showModal={showModal}
          closeModal={() => this.handleModal(false)}
        >
          <div className="modal__inner">
            <span
              className="close--times"
              onClick={() => this.handleModal(false)}
            >
              &times;
            </span>
            {showModal && queryDetails && (
              <EditInfo
                queryDetails={queryDetails}
                closeEventModal={this.handleModal}
                handleUpdate={this.handleUpdate}
              />
            )}
          </div>
        </CustomModal>
      </div>
    );
  }
}

export default Queries;
