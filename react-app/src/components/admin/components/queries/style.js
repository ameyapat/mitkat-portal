export default {
  rootViewEdit: {
    border: '1px solid #fff',
    color: '#34495e',

    '&:hover': {
      border: '1px solid #fff',
      color: 'var(--whiteMediumEmp)',
    },
  },
  rootTCell: {
    '& span': {
      padding: '3px !important',
      borderRadius: '3px !important',
      display: 'block',
    },
    color: 'var(--whiteMediumEmp)',
    fontSize: 11,
    // fontWeight: 600,
    '& .resolved': {
      background: 'rgba(39, 174, 96, 0.8)',
    },
    '& .pending': {
      background: 'rgba(243, 156, 18, 0.8)',
    },
  },
  rootLabel: {
    color: 'rgba(255,255,255, 0.67) !important',
  },
  rootInput: {
    color: 'var(--whiteHighEmp) !important',
  },
  underline: {
    '&:after': {
      borderBottom: '2px solid #34495e !important',
    },
  },
  notchedOutline: {
    borderColor: 'var(--whiteHighEmp) !important ',
  },
  btnSolid: {
    background: 'rgb(41, 128, 185)',
    color: 'var(--whiteMediumEmp)',
  },
  btnTransparent: {
    background: 'transparent',
    color: 'rgb(41, 128, 185)',
    border: '1px solid rgb(41, 128, 185)',
    '&:hover': {
      background: 'transparent',
    },
  },
  input: {
    display: 'none',
  },
};
