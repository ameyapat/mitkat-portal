import React, { Component } from 'react';
//material ui
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
//
import './style.scss';
import { withStyles } from '@material-ui/core';
import styles from './style';
//
import moment from 'moment';

@withStyles(styles)
class EditSolution extends Component {
  render() {
    let {
      queryDetails: {
        details,
        title,
        clientname,
        querytime,
        id,
        responseuser,
        response,
        status,
        responsetime,
        file,
      },
      classes,
    } = this.props;
    return (
      <div className="edit__solution">
        <div className="statusWrap">
          <span className={`symbol ${status ? 'resolved' : 'pending'}`}></span>
          <span className="status--msg">{status ? 'Resolved' : 'Pending'}</span>
          <span className="status--msg">
            Requested on {moment(querytime).format('LL')}
          </span>
        </div>
        <div className="form--group">
          <h2>{title}</h2>
        </div>
        <div className="form--group assignedWrap">
          <div>
            <label>Assigned To</label>
            <p>{responseuser}</p>
          </div>
          <div>
            <label>Response Time</label>
            <p>
              {responsetime
                ? moment(responsetime).format('LL')
                : 'Awaiting Response'}
            </p>
          </div>
          <div>
            <label>Client Name</label>
            <p>{clientname}</p>
          </div>
        </div>
        <div className="form--group summary">
          <label>Summary</label>
          <p>{details}</p>
        </div>
        <div className="form--group summary">
          <label>File</label>
          <p>
            {file ? (
              <a href={`${file}`} target="_blank">
                View File
              </a>
            ) : (
              '-'
            )}
          </p>
        </div>
        <TextField
          id="solution"
          label="Solution"
          multiline
          fullWidth
          rows="3"
          value={response}
          onChange={e => this.props.handleChange('response', e.target.value)}
          margin="normal"
          variant="outlined"
          InputLabelProps={{
            shrink: true,
            classes: {
              root: classes.rootLabel,
            },
          }}
          InputProps={{
            classes: {
              root: classes.rootText,
              input: classes.rootInput,
              notchedOutline: classes.notchedOutline,
            },
            endAdornment: null,
          }}
        />
        <div className="actionables">
          <Button
            size="small"
            variant="outlined"
            className="btn--viewEdit"
            classes={{ root: classes.rootViewEdit }}
            onClick={() => this.props.closeEventModal(false)}
          >
            Cancel
          </Button>
          <Button
            size="small"
            variant="outlined"
            classes={{ root: classes.rootViewEdit }}
            onClick={() => this.props.handleUpdate()}
          >
            Submit
          </Button>
        </div>
      </div>
    );
  }
}

export default EditSolution;
