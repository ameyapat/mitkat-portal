import React from 'react';
import './style.scss';

const Search = () => <input id="search" type="text" placeholder="Search" />

export default Search;