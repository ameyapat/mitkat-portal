import React, { Component } from 'react';
import './style.scss';
//
import Metrics from '../metrics';
import InfoCard from '../infoCard';
import UserList from '../userlist';
import Stories from '../stories';
import UserMetrics from '../stories/UserMetrics';

export default class Dashboard extends Component {
  render() {
    return (
      <div id="dashboard">
        <div className="left__dash">
          <Metrics />
          <h2 className="ulist__title">List of active users</h2>
          <Stories />          
        </div>
        <div className="right__dash">
          <UserMetrics />          
        </div>
      </div>
    )
  }
}
