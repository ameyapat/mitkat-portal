import React, { Component } from 'react';
import './style.scss';
import MenuItem from '@material-ui/core/MenuItem';
import { fetchRiskProducts } from '../../../../helpers/utils';
import { withCookies } from 'react-cookie';
import { fetchApi } from '../../../../helpers/http/fetch';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import TableListRow from '../../../ui/tablelist/TableListRow';
import TableListCell from '../../../ui/tablelist/TableListCell';
import TableList from '../../../ui/tablelist';
import store from '../../../../store';
import { toggleToast } from '../../../../actions';
import MultiSelect from '../../../ui/multiSelect';
import Empty from '../../../ui/empty';
import { createValueLabel, filterArrObjects } from '../../../../helpers/utils';
import { getRelevantClientList } from '../../../../requestor/admin/requestor';
import CustomModal from '../../../ui/modal';
import RelevantClients from './RelevantClients';

@withCookies
class DispatchEmail extends Component {
  state = {
    selectedEventId: null,
    eventEmailAlerts: [],
    attachLogo: 1,
    attachSignature: 1,
    selectedClients: [],
    clientList: [],
    showDispatch: false,
    relevantClientList: [],
    selectedRelevantClients: [],
    isRelevantClient: false,
  };

  componentDidMount() {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    this.getEventEmailAlerts(authToken);
    this.getClientList(authToken);
  }
  //to get dropdown list of clients
  getClientList = authToken => {
    let reqObj = {
      url: `${API_ROUTES.getClientList}`,
      method: 'POST',
      isAuth: true,
      authToken,
      showToggle: true,
    };
    fetchApi(reqObj)
      .then(data => {
        this.setState({ clientList: data });
      })
      .catch(e => console.log(e));
  };
  //to get table list of email alerts to be dispatched
  getEventEmailAlerts = authToken => {
    let reqObj = {
      url: `${API_ROUTES.getEventEmailAlert}`,
      method: 'POST',
      isAuth: true,
      authToken,
      showToggle: true,
    };
    fetchApi(reqObj)
      .then(data => {
        this.setState({ eventEmailAlerts: data });
      })
      .catch(e => console.log(e));
  };

  handleSelectEventId = (e, id) => {
    this.setState({ selectedEventId: id, isRelevantClient: false });
  };

  handleSelectEvent = id => {
    this.setState({ selectedEventId: id, isRelevantClient: true });
  };

  clearRelevantClientIds = () => {
    this.setState({ selectedRelevantClients: [] });
  };

  generateListXML = () => {
    let { eventEmailAlerts } = this.state;
    let emailAlertsXML = [];

    return (emailAlertsXML = eventEmailAlerts.map(t => {
      return (
        <TableListRow key={t.id}>
          <TableListCell value={t.date} />
          <TableListCell value={t.title} />
          <TableListCell value={t.country} />
          <TableListCell value={t.state} />
          <TableListCell value={t.metro} />
          {/* <TableListCell value={t.creator} /> missing need to ask ameya */}
          <TableListCell value={t.storytype} />
          <TableListCell
            value={
              t.importance ? (
                <i class="fas fa-star"></i>
              ) : (
                <i class="far fa-star"></i>
              )
            }
          />
          <TableListCell>
            <input
              name="email-alerts"
              type="radio"
              onChange={e => this.handleSelectEventId(e, t.id)}
            />
          </TableListCell>
          <TableListCell>
            <button
              className="btnDispatch"
              onClick={() => this.handleDispatchEmail(t.id)}
            >
              Dispatch
            </button>
          </TableListCell>
        </TableListRow>
      );
    }));
  };

  handleDispatchEmail = id => {
    getRelevantClientList(id).then(data => {
      this.setState({ relevantClientList: data }, () => {
        this.handleSelectEvent(id);
        this.toggleModal(true);
      });
    });
  };

  handleRadioInput = (value, type) => this.setState({ [type]: value });

  toggleModal = show => this.setState({ showDispatch: show });

  submitTracker = () => {
    let { cookies } = this.props;
    let {
      attachLogo,
      attachSignature,
      selectedEventId,
      selectedClients,
      selectedRelevantClients,
      isRelevantClient,
    } = this.state;
    let authToken = cookies.get('authToken');
    let reqData = {
      logo: attachLogo,
      signature: attachSignature,
      clientids: isRelevantClient
        ? selectedRelevantClients
        : filterArrObjects(selectedClients),
      eventids: selectedEventId,
    };
    let reqObj = {
      url: API_ROUTES.dispatchEmailAlert,
      method: 'POST',
      isAuth: true,
      authToken,
      data: reqData,
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        let toastObj = {
          toastMsg: `Total failed emails: ${data.totalFailedEmails}`,
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
        this.toggleModal(false);
        if (isRelevantClient) {
          this.clearRelevantClientIds();
        }
      })
      .catch(e => console.log(e));
  };

  handleMultiSelect = value => {
    let { selectedClients } = this.state;
    selectedClients = [...value];
    this.setState({ selectedClients });
  };

  addToList = id => {
    let { selectedRelevantClients } = this.state;
    if (!selectedRelevantClients.includes(id)) {
      selectedRelevantClients.push(id);
    }
    this.setState({ selectedRelevantClients });
  };

  removeFromList = id => {
    let { selectedRelevantClients } = this.state;
    if (selectedRelevantClients.includes(id)) {
      const idx = selectedRelevantClients.indexOf(id);
      selectedRelevantClients.splice(idx, 1);
    }
    this.setState({ selectedRelevantClients });
  };

  handleRelevantClientDispatch = () => {
    this.submitTracker();
  };

  render() {
    let {
      selectedEventId,
      attachLogo,
      attachSignature,
      eventEmailAlerts,
      clientList,
      selectedClients,
      showDispatch,
      relevantClientList,
      selectedRelevantClients,
      isRelevantClient,
    } = this.state;
    return (
      <div className="dispatchEmail__wrap">
        <div className="product__header">
          <div className="product__title">
            <h3>Dispatch Email Alerts</h3>
          </div>
          {eventEmailAlerts.length > 0 && selectedEventId && !isRelevantClient && (
            <div className="product_select">
              <MultiSelect
                value={selectedClients}
                placeholder={'Select Clients'}
                parentEventHandler={value => this.handleMultiSelect(value)}
                options={createValueLabel(clientList, 'clients')}
              />
            </div>
          )}
        </div>
        {eventEmailAlerts.length > 0 && selectedEventId && !isRelevantClient && (
          <div className="actionables__wrap">
            <div>
              <h3>Attach Logo</h3>
              <label>Organization</label>
              <input
                checked={attachLogo === 1}
                type="radio"
                name="logo"
                value={1}
                onChange={e => this.handleRadioInput(1, 'attachLogo')}
              />
              <label>Client</label>
              <input
                checked={attachLogo === 2}
                type="radio"
                name="logo"
                value={2}
                onChange={e => this.handleRadioInput(2, 'attachLogo')}
              />
              <label>No Logo</label>
              <input
                checked={attachLogo === 0}
                type="radio"
                name="logo"
                value={0}
                onChange={e => this.handleRadioInput(0, 'attachLogo')}
              />
            </div>
            <div>
              <h3>Attach Signature</h3>
              <label>Organization</label>
              <input
                checked={attachSignature === 1}
                type="radio"
                name="signature"
                value={1}
                onChange={e => this.handleRadioInput(1, 'attachSignature')}
              />
              <label>No Signature</label>
              <input
                checked={attachSignature === 0}
                type="radio"
                name="signature"
                value={0}
                onChange={e => this.handleRadioInput(0, 'attachSignature')}
              />
            </div>
            <div>
              <button onClick={() => this.submitTracker()}>
                Send Email Alert
              </button>
            </div>
          </div>
        )}
        {eventEmailAlerts.length > 0 ? (
          <div className="product__body">
            <div>
              <div className="tlist__head">
                <div>
                  <label>Date</label>
                </div>
                <div>
                  <label>Subject</label>
                </div>
                <div>
                  <label>Country</label>
                </div>
                <div>
                  <label>State</label>
                </div>
                <div>
                  <label>City</label>
                </div>
                {/* <div><label>Creator</label></div> */}
                <div>
                  <label>Region Type</label>
                </div>
                <div>
                  <label>IRT</label>
                </div>
                <div>
                  <label>Select</label>
                </div>
                <div>
                  <label>#</label>
                </div>
              </div>
              <TableList classes={{ root: 'darkBlue' }}>
                {this.generateListXML()}
              </TableList>
            </div>
          </div>
        ) : (
          <Empty title="No alerts available!" />
        )}
        <CustomModal
          showModal={showDispatch}
          closeModal={() => this.toggleModal(false)}
        >
          <div className="modal__inner">
            <RelevantClients
              relevantClientList={relevantClientList}
              selectedRelevantClients={selectedRelevantClients}
              addToList={this.addToList}
              removeFromList={this.removeFromList}
              handleRelevantClientDispatch={this.handleRelevantClientDispatch}
              toggleModal={() => this.toggleModal(false)}
            />
          </div>
        </CustomModal>
      </div>
    );
  }
}

export default DispatchEmail;
