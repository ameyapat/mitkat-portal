export default {
  customRootCheckbox: {
    padding: 0,
    marginLeft: 10,
    color: '#a9bf42 !important',
  },
};
