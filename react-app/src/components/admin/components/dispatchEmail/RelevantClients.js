import React from 'react';
import Empty from '../../../ui/empty';
import CustomCheckbox from './CustomCheckbox';
import clsx from 'clsx';
import { Divider } from '@material-ui/core';

const RelevantClients = ({
  relevantClientList,
  selectedRelevantClients,
  addToList,
  removeFromList,
  handleRelevantClientDispatch,
  toggleModal,
}) => {
  const renderClients = () => {
    return relevantClientList.map(client => {
      return (
        <div
          className={clsx({
            highlightClient: client.relevancy,
            relevantClientWrap: true,
          })}
        >
          <div className={clsx({ clientDetailWrap: true })}>
            <span>{client.clientName}</span>
            <span>
              <CustomCheckbox
                addToList={addToList}
                removeFromList={removeFromList}
                id={client.id}
                isSelected={selectedRelevantClients.includes(client.id)}
              />
            </span>
          </div>
        </div>
      );
    });
  };

  return (
    <div id="relevantClients">
      <div className="relevantClients__header">
        <span>
          <h3>Email Dispatch Assistance</h3>
        </span>
        <span>
          <button
            disabled={!selectedRelevantClients?.length > 0}
            className="btnOpenDispatch btnDispatch"
            onClick={e => handleRelevantClientDispatch()}
          >
            Dispatch
          </button>
        </span>
      </div>
      <Divider />
      <div className="relevantClients__body">
        {relevantClientList?.length > 0 ? (
          <div className="client__wrap">{renderClients()}</div>
        ) : (
          <Empty title="No clients available" />
        )}
      </div>
      <div className="relevantClients__footer">
        <button className="btnClose" onClick={toggleModal}>
          Close
        </button>
      </div>
    </div>
  );
};

export default RelevantClients;
