import React, { Component } from 'react';
import './style.scss';

export default class InfoCard extends Component {
  render() {
    let { colorOne, colorTwo, title, info } = this.props;

    return (
      <div className="infoCard" style={{background: `linear-gradient(${colorOne}, ${colorTwo})`}}>
        <h3>{title}</h3>
        <p>{info}</p>
        <button>More Info...</button>
      </div>
    )
  }
}
