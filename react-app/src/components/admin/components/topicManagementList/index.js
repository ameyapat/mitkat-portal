import React, { Component } from 'react'

import './style.scss'
import styles from './style'

import TableList from '../../../ui/tablelist'
import TableListRow from '../../../ui/tablelist/TableListRow'
import TableListCell from '../../../ui/tablelist/TableListCell'
import CustomModal from '../../../ui/modal'
import Empty from '../../../ui/empty'
import EditTopic from "./EditTopic"

import { updateTopic } from "../../../../api/api"
import { toggleToast } from '../../../../actions';
import {
  fetchCountries,
  filterArrObjects,
  createValueLabel,
} from '../../../../helpers/utils';

import { Divider } from '@material-ui/core';
import clsx from 'clsx';
import { connect } from 'react-redux';

class TopicManagementList extends Component {
  state = {
    showModal: false,
    showToast: false,
    selectedCountries: [],
    countries: [],
  };

  componentDidMount() {
    fetchCountries().then(countries => this.setState({ countries }));
  }

  renderListHeader = () => {
    return (
      <TableListRow>
        <TableListCell value="Topic name" classes={{ root: 'tListHead' }} />
        <TableListCell value="Topic status" classes={{ root: 'tListHead' }} />
        <TableListCell value="#" classes={{ root: 'tListHead' }} />
      </TableListRow>
    );
  };

  renderList = allTopics => {
    return allTopics.map(item => {
      return (
        <TableListRow key={item.id}>
          <TableListCell value={item.topicName} />
          <TableListCell>
            <span
              className={clsx(
                'liveTopicStatus',
                item.live ? 'isLive' : 'isNotLive',
              )}
            >
              <i className="fas fa-circle"></i>
            </span>
          </TableListCell>
          <TableListCell>
            <button
              className="btn btnEdit"
              onClick={() =>
                this.handleEdit(
                  item.id,
                  item.topicName,
                  item.live,
                  item.countryids,
                )
              }
            >
              Edit
            </button>
          </TableListCell>
        </TableListRow>
      );
    });
  };

  handleModal = (type, value) => this.setState({ [type]: value });

  handleEdit = (id, topicName, isLive, countryids) => {
    this.setState({
      id,
      showModal: true,
      topicName,
      isLive,
      selectedCountries: this.state.countries.filter(country =>
        countryids.includes(country.value),
      ),
    });
  };

  handleEditTopic = (type, value) => this.setState({ [type]: value });

  handleSubmit = () => {
    const { selectedCountries, isLive, topicName, id } = this.state;
    const reqData = {
      id: id || 0,
      topicName: topicName,
      live: isLive,
      countryids: filterArrObjects(selectedCountries),
    };

    updateTopic(reqData)
      .then(res => {
        let toastObj = {
          toastMsg: 'Opps! something went wrong',
          showToast: true,
        };

        if (res.success) {
          toastObj.toastMsg = res.message;
          this.setState({ showModal: false });
        }
        this.props.updateList();
        this.props.dispatch(toggleToast(toastObj));
      })
      .catch(e => console.log(e));
  };

  handleMultiSelect = (value, type) => {
    this.setState({ [type]: value });
  };

  render() {
    const { allTopics } = this.props;
    const {
      showModal,
      id,
      topicName,
      isLive,
      countries,
      selectedCountries,
    } = this.state;

    return (
      <div className="topicManagementList">
        {allTopics.length > 0 ? (
          <TableList>
            {this.renderListHeader()}
            <Divider />
            {this.renderList(allTopics)}
          </TableList>
        ) : (
          <Empty title="No topics found" />
        )}
        {showModal && (
          <CustomModal
            showModal={showModal}
            closeModal={() => this.handleModal('showModal', false)}
          >
            <div className="topicManagementList_modal modal__inner">
              <div className="topic__header">
                <h3>Edit Topic Details</h3>
              </div>
              <Divider />
              <div className="topic__body">
                <EditTopic
                  id={id}
                  topicName={topicName}
                  isLive={isLive}
                  handleEditTopic={this.handleEditTopic}
                  handleSubmit={this.handleSubmit}
                  countries={countries}
                  selectedCountries={selectedCountries}
                  handleMultiSelect={this.handleMultiSelect}
                />
              </div>
              <div className="topic__footer">
                <button
                  className="btn btnSubmit"
                  onClick={() => this.handleSubmit()}
                >
                  Update
                </button>
                <button
                  className="btn btnClose"
                  onClick={() => this.handleModal('showModal', false)}
                >
                  Close
                </button>
              </div>
            </div>
          </CustomModal>
        )}
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch => {
    return {
        dispatch
    }
})

export default connect(() => {}, mapDispatchToProps)(TopicManagementList)