import React, { Component } from 'react'

import './style.scss'
import styles from './style'

import CustomModal from '../../../ui/modal'
import EditTopic from "./EditTopic"

import { updateTopic } from "../../../../api/api"
import { toggleToast } from '../../../../actions';
import { fetchCountries, filterArrObjects } from '../../../../helpers/utils';
import { Divider } from '@material-ui/core';
import { connect } from 'react-redux';

class AddTopic extends Component {
  state = {
    showModal: false,
    showToast: false,
    selectedCountries: [],
    countries: [],
  };

  componentDidMount() {
    fetchCountries().then(countries => this.setState({ countries }));
  }

  handleModal = (type, value) => this.setState({ [type]: value });

  handleEdit = (id, topicName, isLive) => {
    this.setState({ id, showModal: true, topicName, isLive });
  };

  handleEditTopic = (type, value) =>
    this.setState({ [type]: value });

  handleMultiSelect = (value, type) => {
    this.setState({ [type]: value });
  };

  handleSubmit = () => {
    const { selectedCountries, isLive, topicName } = this.state;
    const reqData = {
      id: 0,
      topicName,
      live: isLive,
      countryids: filterArrObjects(selectedCountries),
    };

    updateTopic(reqData)
      .then(res => {
        let toastObj = {
          toastMsg: 'Opps! something went wrong',
          showToast: true,
        };

        if (res.success) {
          toastObj.toastMsg = res.message;
          this.setState({ showModal: false });
          this.props.updateList();
        }

        this.props.dispatch(toggleToast(toastObj));
      })
      .catch(e => console.log(e));
  };

  render() {
    const {
      showModal,
      selectedCountries,
      countries,
      id,
      topicName,
      isLive,
    } = this.state;

    return (
      <div className="topicManagementList addTopic">
        <button
          className="btn btnAdd"
          onClick={() => this.handleModal('showModal', true)}
        >
          Add <i className="fas fa-plus"></i>
        </button>
        {showModal && (
          <CustomModal
            showModal={showModal}
            closeModal={() => this.handleModal('showModal', false)}
          >
            <div className="topicManagementList_modal modal__inner">
              <div className="topic__header">
                <h3>Add Topic Details</h3>
              </div>
              <Divider />
              <div className="topic__body">
                <EditTopic
                  id={id}
                  topicName={topicName}
                  isLive={isLive}
                  handleEditTopic={this.handleEditTopic}
                  handleSubmit={this.handleSubmit}
                  handleMultiSelect={this.handleMultiSelect}
                  selectedCountries={selectedCountries}
                  countries={countries}
                />
              </div>
              <div className="topic__footer">
                <button
                  className="btn btnSubmit"
                  onClick={() => this.handleSubmit()}
                >
                  Add
                </button>
                <button
                  className="btn btnClose"
                  onClick={() => this.handleModal('showModal', false)}
                >
                  Close
                </button>
              </div>
            </div>
          </CustomModal>
        )}
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch => {
    return {
        dispatch
    }
})

export default connect(() => {}, mapDispatchToProps)(AddTopic)