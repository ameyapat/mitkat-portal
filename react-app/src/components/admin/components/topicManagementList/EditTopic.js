import React, { Component } from 'react'

import './style.scss'
import styles from './style'

import TextField from '@material-ui/core/TextField'
import FormControlLabel from '@material-ui/core/FormControlLabel';
import MultiSelect from '../../../ui/multiSelect';
import CustomLabel from '../../../ui/customLabel';
import Switch from '@material-ui/core/Switch';
import { withStyles } from '@material-ui/core/styles';

@withStyles(styles)
class EditTopic extends Component {
  render() {
    const { classes } = this.props;
    return (
      <div className="editTopicManagement">
        <h4 className="userSettings__label">Topic Name</h4>
        <TextField
          fullWidth
          value={this.props.topicName}
          onChange={e =>
            this.props.handleEditTopic('topicName', e.target.value)
          }
          margin="normal"
          variant="outlined"
          InputLabelProps={{
            shrink: true,
            classes: {
              root: classes.rootLabel,
            },
          }}
          InputProps={{
            classes: {
              root: classes.rootText,
              input: classes.rootInput,
              notchedOutline: classes.notchedOutline,
            },
            endAdornment: null,
          }}
        />
        <>
          <CustomLabel
            title="Countries"
            classes="select--label"
            isRequired={true}
          />
          <MultiSelect
            value={this.props.selectedCountries}
            placeholder={'Select Countries'}
            parentEventHandler={value =>
              this.props.handleMultiSelect(value, 'selectedCountries')
            }
            options={this.props.countries}
          />
        </>
        <div className="userSettings_toggles">
          <h4 className="userSettings__label">Topic Status</h4>
          <FormControlLabel
            control={
              <Switch
                checked={this.props.isLive}
                onChange={() =>
                  this.props.handleEditTopic('isLive', !this.props.isLive)
                }
                name="isLiveTopic"
              />
            }
            classes={{
              root: classes.rootControlLabel,
              label: classes.rootLabelControlLabel,
              switchBase: classes.switchBase,
              thumb: classes.switchThumb,
              track: classes.switchTrack,
              checked: classes.switchChecked,
            }}
          />
        </div>
      </div>
    );
  }
}

export default EditTopic