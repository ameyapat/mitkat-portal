export default {
  rootControlLabel: {
    color: 'red',
  },
  rootLabelControlLabel: {
    color: 'red !important',
  },
  switchBase: {
    color: 'red',
    '&$checked': {
      color: 'yellow',
    },
    '&$checked + $track': {
      backgroundColor: 'green',
    },
  },
  thumb: {},
  track: {},
  checked: {},
  rootLabel: {
    color: 'rgba(255,255,255, 0.67) !important',
  },
  rootInput: {
    color: 'var(--whiteHighEmp) !important',
  },
  underline: {
    '&:after': {
      borderBottom: '2px solid #34495e !important',
    },
  },
  notchedOutline: {
    borderColor: 'var(--whiteHighEmp) !important ',
  },
};
