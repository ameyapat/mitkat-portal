import React, { Component } from 'react';
//helpers
import styles from './style';
import injectSheet from 'react-jss';
import Select from 'react-select';
import Switch from '@material-ui/core/Switch';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
// import { redefineObj } from './utils';

class AccessControlContact extends Component {
  state = {
    primaryRes: {},
    secondaryRes: {},
    isSubscribed: '',
    travelRiskSubscription: '',
  };

  //   componentDidMount() {
  //     let {
  //       queryDetails: {
  //         subscription,
  //         primaryspoc,
  //         secondaryspoc,
  //         travelRiskSubscription,
  //       },
  //     } = this.props;
  //     let { primaryRes, secondaryRes } = this.state;

  //     if (primaryspoc) {
  //       primaryRes['value'] = primaryspoc.id;
  //       primaryRes['label'] = primaryspoc.username;
  //     }

  //     if (secondaryspoc) {
  //       secondaryRes['value'] = secondaryspoc.id;
  //       secondaryRes['label'] = secondaryspoc.username;
  //     }

  //     this.setState({
  //       isSubscribed: subscription,
  //       primaryRes,
  //       secondaryRes,
  //       travelRiskSubscription,
  //     });
  //   }

  //   handleSelectChange = (type, e) => this.setState({ [type]: e });

  //   handleInputchange = (type, value) =>
  //     this.setState({ [type]: value }, () =>
  //       console.log(this.state.travelRiskSubscription),
  //     );

  //   handleSubmit = () => this.props.handleSubscription(this.state);

  render() {
    let { classes, adminUserDetails, secondarySPOC, primarySPOC } = this.props;
    let {
      primaryRes,
      secondaryRes,
      isSubscribed,
      travelRiskSubscription,
    } = this.state;

    return (
      <div className={classes.rootSubscribeBox}>
        <div className="">
          <label>Primary Responder</label>
          <Select
            className="select--options"
            value={primarySPOC}
            placeholder="Primary Responder"
            onChange={e => this.props.handleSelectChange('primarySPOC', e)}
            options={adminUserDetails && redefineObj(adminUserDetails)}
          />
        </div>
        <div>
          <label>Secondary Responder</label>
          <Select
            className="select--options"
            value={secondarySPOC}
            placeholder="Secondary Responder"
            onChange={e => this.props.handleSelectChange('secondarySPOC', e)}
            options={adminUserDetails && redefineObj(adminUserDetails)}
          />
        </div>
      </div>
    );
  }
}

export default injectSheet(styles)(AccessControlContact);
