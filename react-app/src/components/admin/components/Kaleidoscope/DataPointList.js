import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import TableList from '../../../ui/tablelist';
import TableListRow from '../../../ui/tablelist/TableListRow';
import TableListCell from '../../../ui/tablelist/TableListCell';
import { updateQuadrantPoints } from '../../../../actions/quadrantActions';
import './style.scss';
import moment from 'moment';

const DataPointList = ({
  quadrantPointsProp,
  toggleDataPointsModal,
  setCurrentPointId,
}) => {
  const dispatch = useDispatch();
  const newQuadrantPoints = useSelector(
    state => state.quadrantState.quadrantPoints,
  );

  const deleteDataPoint = id => {
    const index = newQuadrantPoints.findIndex(item => item.id === id);
    newQuadrantPoints.splice(index, 1);
    dispatch(updateQuadrantPoints(newQuadrantPoints));
  };

  const renderDataPointHeader = () => {
    return (
      <TableListRow>
        <TableListCell value="Month" />
        <TableListCell value="Year" />
        <TableListCell value="Likelihood" />
        <TableListCell value="Impact" />
        <TableListCell value="Region" />
        <TableListCell value="#" />
      </TableListRow>
    );
  };

  const renderDataPointBody = () => {
    return (
      <TableList classes={{ root: 'kaleidoscope-table-list' }}>
        {quadrantPointsProp.map(filteredItem => {
          return (
            <TableListRow
              key={filteredItem.sid}
              classes={{ root: 'kaleidoscope-table-row' }}
            >
              <TableListCell
                // value={moment(filteredItem.month).format('MMMM')}
                value={filteredItem.month}
              />
              <TableListCell
                // value={moment(filteredItem.year).format('YYYY')}
                value={filteredItem.year}
              />
              <TableListCell value={filteredItem.likelihood} />
              <TableListCell value={filteredItem.impact} />
              <TableListCell value={filteredItem.regionScale} />
              <TableListCell>
                <div className="kaleidoscope-btnWrapper">
                  <button
                    className="btn--edit"
                    onClick={() => {
                      toggleDataPointsModal(true, true);
                      setCurrentPointId(filteredItem.sid);
                    }}
                  >
                    <i className="icon-uniE93E"></i>
                  </button>
                  <button
                    className="btn--delete"
                    onClick={() => deleteDataPoint(filteredItem.sid)}
                  >
                    <i className="icon-uniE949"></i>
                  </button>
                </div>
              </TableListCell>
            </TableListRow>
          );
        })}
      </TableList>
    );
  };
  return (
    <div id="dataPointList">
      {renderDataPointHeader()}
      {renderDataPointBody()}
    </div>
  );
};

export default DataPointList;
