import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import TextField from '@material-ui/core/TextField';
import Select from 'react-select';
import MultiSelect from '../../../ui/multiSelect';
import {
  updateQuadrantDetail,
  updateQuadrantPoints,
} from '../../../../actions/quadrantActions';
import AlertDialog from '../../../ui/alertDialog';
import CustomSlider from '../../../ui/customSlider';
import DataPointList from './DataPointList';
import './style.scss';
import { Divider } from '@material-ui/core';
import CustomLabel from '../../../ui/customLabel';
import { getLiveTopics, getRiskCategories } from '../../../../api/api';
import { getQuadrantEventDetails } from '../../../../requestor/quadrants/requestor';
import { fetchCountries } from '../../../../helpers/utils';
import { sortSelectedCountries } from './utils';
import injectSheet from 'react-jss';
import styles from './style';
import { v4 as uuidv4 } from 'uuid';
import customStyles from './customStyles';

const KscopeUpdateEvent = ({
  isUpdateEvent,
  toggleQuadModal,
  toggleDataPointsModal,
  handleEventSubmit,
  classes,
  currentEditId,
  clearDetails,
  setCurrentPointId,
}) => {
  const [id, setId] = useState(null);
  const [title, setTitle] = useState('');
  const [forecast, setForecast] = useState('');
  const [likelihood, setLikelihood] = useState(0);
  const [impact, setImpact] = useState(0);
  const [regionScale, setRegionScale] = useState(0);
  const [showAlert, setShowAlert] = useState(false);
  const [allLiveTopics, setLiveTopics] = useState([]);
  const [countries, initCountries] = useState([]);
  const [selectedLiveTopic, setSelectedLiveTopic] = useState({});
  const [riskCategories, setRiskCategories] = useState([]);
  const [selectedRiskCategory, setSelectedRiskCategory] = useState({});
  const [selectedCountries, setSelectedCountries] = useState([]);

  const dispatch = useDispatch();

  const quadrantDetailsProp = useSelector(
    state => state.quadrantState.quadrantDetails,
  );

  const quadrantPointsProp = useSelector(
    state => state.quadrantState.quadrantPoints,
  );

  useEffect(() => {
    getLiveTopics()
      .then(data => {
        let allLiveTopics = [];
        allLiveTopics = data.map(t => {
          let allLiveTopicsObj = {};
          allLiveTopicsObj['value'] = t.id;
          allLiveTopicsObj['label'] = t.topicName;
          return allLiveTopicsObj;
        });
        setLiveTopics(allLiveTopics);
        setSelectedLiveTopic({
          label: allLiveTopics[0].label,
          value: allLiveTopics[0].value,
        });
      })
      .catch(e => console.log('Error fetching live topics', e));
  }, []);

  useEffect(() => {
    fetchCountries().then(countries => {
      initCountries(countries);
    });
  }, []);

  useEffect(() => {
    getRiskCategories()
      .then(riskCategories => {
        setRiskCategories(riskCategories);
        setSelectedRiskCategory({
          label: riskCategories[0].label,
          value: riskCategories[0].value,
        });
      })
      .catch(e => console.log('Error fetching live topics', e));
  }, []);

  useEffect(() => {
    const quadrantDetails = {
      ...quadrantDetailsProp,
      id,
      title,
      forecast,
      likelihood,
      impact,
      regionScale,
      topicid: selectedLiveTopic.value,
      riskCategoryid: selectedRiskCategory.value,
      quadrantCountries: selectedCountries,
    };
    dispatch(updateQuadrantDetail(quadrantDetails));
  }, [
    id,
    title,
    forecast,
    likelihood,
    impact,
    regionScale,
    selectedLiveTopic,
    selectedRiskCategory,
    selectedCountries,
  ]);

  useEffect(() => {
    if (
      isUpdateEvent &&
      allLiveTopics.length > 0 &&
      riskCategories.length > 0 &&
      countries.length > 0
    ) {
      getQuadrantEventDetails(currentEditId).then(data => {
        const newQuadrantDetailsProp = {
          id: data.id,
          title: data.title,
          topicid: data.topicid,
          riskCategoryid: data.riskCategoryid,
          likelihood: data.likelihood,
          impact: data.impact,
          regionScale: data.regionScale,
          forecast: data.forecast,
          quadrantCountries: sortSelectedCountries(
            data.quadrantCountries,
            countries,
          ),
        };

        const editedQuadrantPointsProp = data.quadrantPoints.map(item => {
          item['sid'] = item.id || uuidv4();
          return item;
        });

        setLikelihood(data.likelihood);
        setId(data.id);
        setTitle(data.title);
        setForecast(data.forecast);
        setImpact(data.impact);
        setRegionScale(data.regionScale);
        setSelectedLiveTopic({
          value: data.topicid,
          label: allLiveTopics.find(item => item.value === data.topicid).label,
        });

        setSelectedRiskCategory({
          value: data.riskCategoryid,
          label: riskCategories.find(item => item.value === data.riskCategoryid)
            .label,
        });

        setSelectedCountries(
          sortSelectedCountries(data.quadrantCountries, countries),
        );

        dispatch(
          updateQuadrantDetail({
            ...quadrantDetailsProp,
            ...newQuadrantDetailsProp,
          }),
        );

        dispatch(
          updateQuadrantPoints([
            ...quadrantPointsProp,
            ...editedQuadrantPointsProp,
          ]),
        );
      });
    }
  }, [isUpdateEvent, currentEditId, allLiveTopics, riskCategories, countries]);

  const handleMultiSelect = (value, type) => {
    setSelectedCountries(value);
  };

  return (
    <div id="kscopeUpdateEvent">
      <div className="kscopeUpdateEvent__header">
        <h3>{isUpdateEvent ? 'Update' : 'Create'} Event</h3>
      </div>
      <section className="kscopeUpdateEvent__body">
        <div className="event--title">
          <TextField
            required
            // error={validation.eventTitle.isInvalid}
            id="title"
            label="Title"
            multiline
            fullWidth
            rows="1"
            rowsMax="3"
            value={title}
            onChange={e => setTitle(e.target.value)}
            defaultValue="Enter Event Title"
            margin="normal"
            variant="outlined"
            InputLabelProps={{
              shrink: true,
              classes: {
                root: classes.rootLabel,
              },
            }}
            InputProps={{
              maxLength: 5000,
              classes: {
                root: classes.rootInput,
                input: classes.rootInput,
                notchedOutline: classes.notchedOutline,
              },
              endAdornment: null,
            }}
          />
          {/* {validation.eventTitle.isInvalid && (
                <span className="error--text">
                  {validation.eventTitle.message}
                </span>
              )} */}
        </div>
        <div className="col-2">
          <CustomLabel
            title="Select Topic"
            classes="select--label"
            isRequired={false}
          />
          <Select
            defaultValue={selectedLiveTopic}
            className="select--options"
            value={selectedLiveTopic}
            placeholder={'Select Topic'}
            onChange={value => setSelectedLiveTopic(value)}
            options={allLiveTopics.length > 0 ? allLiveTopics : []}
            styles={customStyles}
          />
          {/* {validation.selectedStory.isInvalid && (
                  <span className='error--text'>
                    {validation.selectedStory.message}
                  </span>
                )} */}
        </div>
        <div className="col-2">
          <CustomLabel
            title="Countries"
            classes="select--label"
            isRequired={true}
          />
          <MultiSelect
            value={selectedCountries}
            placeholder={'Select Countries'}
            parentEventHandler={value =>
              handleMultiSelect(value, 'selectedCountries')
            }
            options={countries}
          />
        </div>
        <div className="col-2">
          <CustomLabel
            title="Select Risk Category"
            classes="select--label"
            isRequired={false}
          />
          <Select
            defaultValue={selectedRiskCategory}
            className="select--options"
            value={selectedRiskCategory}
            placeholder={'Select Risk Category'}
            onChange={value => setSelectedRiskCategory(value)}
            options={riskCategories.length > 0 ? riskCategories : []}
            styles={customStyles}
          />
          {/* {validation.selectedStory.isInvalid && (
                  <span className='error--text'>
                    {validation.selectedStory.message}
                  </span>
                )} */}
        </div>
        <CustomLabel
          title={'Historic Data Section'}
          classes={'label'}
          isRequired={false}
        />
        <div className="historicDataWrapper">
          <button onClick={() => toggleDataPointsModal(true, false)}>
            Add New Datapoint
          </button>
        </div>
        <Divider className="divider" />
        {quadrantPointsProp.length > 0 && (
          <DataPointList
            quadrantPointsProp={quadrantPointsProp}
            toggleDataPointsModal={toggleDataPointsModal}
            setCurrentPointId={setCurrentPointId}
          />
        )}
        {/* <CustomLabel
          title={'Forecast Section (For next quarter)'}
          classes={'label'}
          isRequired={false}
        />
        <Divider className="divider" /> */}
        {/* <div className="sliderWapper">
          <CustomSlider
            aria-label={'Likelihood'}
            defaultValue={0}
            value={likelihood}
            step={0.1}
            min={0}
            max={5}
            handleOnChange={(e, value) => setLikelihood(value)}
            labelTitle={'Likelihood'}
          />
        </div> */}
        {/* <div className="sliderWapper">
          <CustomSlider
            aria-label={'Impact'}
            defaultValue={0}
            value={impact}
            step={0.1}
            min={0}
            max={5}
            handleOnChange={(e, value) => setImpact(value)}
            labelTitle={'Impact'}
          />
        </div> */}
        {/* <div className="sliderWapper">
          <CustomSlider
            aria-label={'Region Scale'}
            defaultValue={0}
            value={regionScale}
            step={1}
            min={1}
            max={5}
            handleOnChange={(e, value) => setRegionScale(value)}
            labelTitle={'Region Scale'}
          />
        </div> */}
        {/* <div className="event--title"> */}
        {/* <TextField
            required
            // error={validation.eventTitle.isInvalid}
            id="forecast"
            label="Forecast"
            multiline
            fullWidth
            rows="1"
            rowsMax="3"
            value={forecast}
            onChange={e => setForecast(e.target.value)}
            defaultValue="Enter Forecast"
            margin="normal"
            variant="outlined"
            InputLabelProps={{
              shrink: true,
              classes: {
                root: classes.rootLabel,
              },
            }}
            InputProps={{
              maxLength: 5000,
              classes: {
                root: classes.rootInput,
                input: classes.rootInput,
                notchedOutline: classes.notchedOutline,
              },
              endAdornment: null,
            }}
          /> */}
        {/* {validation.eventTitle.isInvalid && (
                <span className="error--text">
                  {validation.eventTitle.message}
                </span>
              )} */}
        {/* </div> */}
      </section>
      <div className="kscopeUpdateEvent__footer">
        <div className="actionables">
          <button className="btnCancel" onClick={e => setShowAlert(true)}>
            Cancel
          </button>
          <button className="btnUpdate" onClick={e => handleEventSubmit(e)}>
            {isUpdateEvent ? 'Update' : 'Create'} Event
          </button>
        </div>
      </div>
      {showAlert && (
        <AlertDialog
          isOpen={showAlert}
          title="Cancel Changes"
          description="You are about to cancel event changes, you won't be able to get them back, are you sure?"
          parentEventHandler={() => {
            toggleQuadModal(false, false);
            clearDetails();
          }}
          handleClose={() => setShowAlert(false)}
        />
      )}
    </div>
  );
};

export default injectSheet(styles)(KscopeUpdateEvent);
