export default {
  rootLabel: {
    color: 'rgba(255,255,255, 0.67) !important',
    whiteSpace: 'pre-line',
  },
  rootInput: {
    color: 'var(--whiteHighEmp) !important',
  },
  underline: {
    '&:after': {
      borderBottom: '2px solid #34495e !important',
    },
  },
  notchedOutline: {
    borderColor: 'var(--whiteHighEmp) !important ',
  },
  rootLabelLight: {
    color: 'var(--whiteHighEmp) !important',
  },
  rootInputLight: {
    color: 'var(--whiteHighEmp) !important',
  },
  notchedOutlineLight: {
    bordercolor: 'var(--whiteHighEmp) !important',
  },
};
