import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import TextField from '@material-ui/core/TextField';
import { updateQuadrantPoints } from '../../../../actions/quadrantActions';
import AlertDialog from '../../../ui/alertDialog';
import CustomSlider from '../../../ui/customSlider';
import CustomDatePicker from '../../../ui/customDatePicker';
import './style.scss';
import injectSheet from 'react-jss';
import styles from './style';
import moment from 'moment';
import { v4 as uuidv4 } from 'uuid';

const KscopeUpdateDataPoints = ({
  isUpdateDataPoints,
  toggleDataPointsModal,
  classes,
  currentCurrentPointId,
  setCurrentPointId,
}) => {
  const [month, setMonth] = useState(moment(new Date()).format('M'));
  const [year, setYear] = useState(moment(new Date()).format('YYYY'));
  const [interplay, setInterplay] = useState('');
  const [forecast, setForecast] = useState('');
  const [likelihood, setLikelihood] = useState(0);
  const [impact, setImpact] = useState(0);
  const [development, setDevelopments] = useState('');
  const [regionScale, setRegionScale] = useState(0);
  const [showAlert, setShowAlert] = useState(false);
  const dispatch = useDispatch();

  const quadrantPointsProp = useSelector(
    state => state.quadrantState.quadrantPoints,
  );

  useEffect(() => {
    if (isUpdateDataPoints && currentCurrentPointId) {
      const currentDataPointItem = quadrantPointsProp.filter(
        item => item.sid === currentCurrentPointId,
      );

      const {
        regionScale,
        interplay,
        forecast,
        month: currentMonth,
        year: currentYear,
        likelihood,
        impact,
        development,
      } = currentDataPointItem[0];
      setRegionScale(regionScale);
      setInterplay(interplay);
      setForecast(forecast);
      setMonth(
        moment()
          .month(currentMonth - 1)
          .format('M'),
      );
      setYear(
        moment()
          .year(currentYear)
          .format('YYYY'),
      );
      setLikelihood(likelihood);
      setImpact(impact);
      setDevelopments(development);
    }
  }, [isUpdateDataPoints, currentCurrentPointId]);

  const handleSave = () => {
    const newQuadrantPointsDetails = [
      {
        sid: uuidv4(),
        month: parseInt(moment(month).format('M')),
        year: parseInt(moment(year).format('YYYY')),
        likelihood,
        interplay,
        forecast,
        impact,
        regionScale,
        development,
      },
    ];

    /**
     * isUpdateDataPoints && currentCurrentPointId
     *
     */
    if (isUpdateDataPoints && currentCurrentPointId) {
      const index = quadrantPointsProp.findIndex(item => {
        if (item.id) {
          if (item.id === currentCurrentPointId) {
            return item;
          }
        } else {
          if (item.sid === currentCurrentPointId) {
            return item;
          }
        }
      });
      const newQuadrantPointsProp = [...quadrantPointsProp];
      newQuadrantPointsProp[index]['month'] = parseInt(
        moment(month).format('M'),
      );
      newQuadrantPointsProp[index]['year'] = parseInt(
        moment(year).format('YYYY'),
      );
      newQuadrantPointsProp[index]['likelihood'] = likelihood;
      newQuadrantPointsProp[index]['interplay'] = interplay;
      newQuadrantPointsProp[index]['forecast'] = forecast;
      newQuadrantPointsProp[index]['impact'] = impact;
      newQuadrantPointsProp[index]['development'] = development;
      newQuadrantPointsProp[index]['regionScale'] = regionScale;

      dispatch(updateQuadrantPoints([...newQuadrantPointsProp]));
    } else {
      dispatch(
        updateQuadrantPoints([
          ...quadrantPointsProp,
          ...newQuadrantPointsDetails,
        ]),
      );
    }
    /**
     *
     */
    toggleDataPointsModal(false, false);
    setCurrentPointId(null);
  };

  return (
    <div id="kscopeUpdateEvent">
      <div className="kscopeUpdateEvent__header">
        <h3>Update Datapoints</h3>
      </div>
      <section className="kscopeUpdateEvent__body">
        <div className="dateWrapper">
          <CustomDatePicker
            label={'Enter Month'}
            value={month}
            defaultValue={month}
            type={'month'}
            handleDateChange={(type, value) => setMonth(value)}
            views={['month']}
            inputLabelClasses={{
              root: classes.rootLabelLight,
            }}
            inputPropsClasses={{
              root: classes.rootInputLight,
              input: classes.rootInputLight,
              notchedOutline: classes.notchedOutlineLight,
            }}
            format="MMMM"
          />
        </div>
        <div className="dateWrapper">
          <CustomDatePicker
            label={'Enter Year'}
            value={year}
            type={'year'}
            handleDateChange={(type, value) => setYear(value)}
            views={['year']}
            inputLabelClasses={{
              root: classes.rootLabelLight,
            }}
            inputPropsClasses={{
              root: classes.rootInputLight,
              input: classes.rootInputLight,
              notchedOutline: classes.notchedOutlineLight,
            }}
          />
        </div>
        <div className="sliderWapper">
          <CustomSlider
            aria-label={'Likelihood'}
            defaultValue={0}
            value={likelihood}
            step={0.1}
            min={0}
            max={5}
            handleOnChange={(e, value) => setLikelihood(value)}
            labelTitle={'Likelihood'}
          />
        </div>
        <div className="sliderWapper">
          <CustomSlider
            aria-label={'Impact'}
            defaultValue={0}
            value={impact}
            step={0.1}
            min={0}
            max={5}
            handleOnChange={(e, value) => setImpact(value)}
            labelTitle={'Impact'}
          />
        </div>
        <div className="sliderWapper">
          <CustomSlider
            aria-label={'Region Scale'}
            defaultValue={1}
            value={regionScale}
            step={1}
            min={1}
            max={5}
            handleOnChange={(e, value) => setRegionScale(value)}
            labelTitle={'Region Scale'}
          />
        </div>
        <div className="event--title">
          <TextField
            required
            // error={validation.eventTitle.isInvalid}
            id="development"
            label="Development"
            multiline
            fullWidth
            rows="1"
            rowsMax="3"
            value={development}
            onChange={e => setDevelopments(e.target.value)}
            defaultValue="Enter Development"
            margin="normal"
            variant="outlined"
            InputLabelProps={{
              shrink: true,
              classes: {
                root: classes.rootLabel,
              },
            }}
            InputProps={{
              maxLength: 5000,
              classes: {
                root: classes.rootInput,
                input: classes.rootInput,
                notchedOutline: classes.notchedOutline,
              },
              endAdornment: null,
            }}
          />
          {/* {validation.eventTitle.isInvalid && (
                <span className="error--text">
                  {validation.eventTitle.message}
                </span>
              )} */}
        </div>
        <div className="event--title">
          <TextField
            required
            // error={validation.eventTitle.isInvalid}
            id="interplay"
            label="Interplay"
            multiline
            fullWidth
            rows="1"
            rowsMax="3"
            value={interplay}
            onChange={e => setInterplay(e.target.value)}
            defaultValue="Enter Interplay"
            margin="normal"
            variant="outlined"
            InputLabelProps={{
              shrink: true,
              classes: {
                root: classes.rootLabel,
              },
            }}
            InputProps={{
              maxLength: 5000,
              classes: {
                root: classes.rootInput,
                input: classes.rootInput,
                notchedOutline: classes.notchedOutline,
              },
              endAdornment: null,
            }}
          />
          {/* {validation.eventTitle.isInvalid && (
                <span className="error--text">
                  {validation.eventTitle.message}
                </span>
              )} */}
        </div>
        <div className="event--title">
          <TextField
            required
            // error={validation.eventTitle.isInvalid}
            id="forecast"
            label="Forecast"
            multiline
            fullWidth
            rows="1"
            rowsMax="3"
            value={forecast}
            onChange={e => setForecast(e.target.value)}
            defaultValue="Enter Forecast"
            margin="normal"
            variant="outlined"
            InputLabelProps={{
              shrink: true,
              classes: {
                root: classes.rootLabel,
              },
            }}
            InputProps={{
              maxLength: 5000,
              classes: {
                root: classes.rootInput,
                input: classes.rootInput,
                notchedOutline: classes.notchedOutline,
              },
              endAdornment: null,
            }}
          />
          {/* {validation.eventTitle.isInvalid && (
                <span className="error--text">
                  {validation.eventTitle.message}
                </span>
              )} */}
        </div>
      </section>
      <div className="kscopeUpdateEvent__footer">
        <div className="actionables">
          <button className="btnCancel" onClick={e => setShowAlert(true)}>
            Cancel
          </button>
          <button className="btnUpdate" onClick={e => handleSave()}>
            Save
          </button>
        </div>
      </div>
      {showAlert && (
        <AlertDialog
          isOpen={showAlert}
          title="Cancel Changes"
          description="You are about to cancel event changes, you won't be able to get them back, are you sure?"
          parentEventHandler={() => toggleDataPointsModal(false, false)}
          handleClose={() => setShowAlert(false)}
        />
      )}
    </div>
  );
};

export default injectSheet(styles)(KscopeUpdateDataPoints);
