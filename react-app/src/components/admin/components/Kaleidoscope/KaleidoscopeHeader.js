import React, { useEffect, useState } from 'react';
import './style.scss';
import SearchBox from '../../../ui/searchBox';

const KaleidoscopeHeader = ({ setSearchedValue, parentHandler }) => {
  const [searchValue, setSearchValue] = useState('');

  useEffect(() => {
    setSearchedValue(searchValue);
  }, [searchValue]);

  return (
    <div id="kaleidoscope-header">
      <SearchBox
        parentHandler={e => setSearchValue(e.target.value)}
        placeholder={'Search by title'}
        value={searchValue}
      />
      <button className="btnCreateEvent" onClick={() => parentHandler()}>
        Create Event
      </button>
    </div>
  );
};

export default KaleidoscopeHeader;
