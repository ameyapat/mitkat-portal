const customStyles = {
  control: provided => ({
    ...provided,
    backgroundColor: 'var(--backgroundSolid)',
    color: '#ffffff',
  }),
  option: (provided, state) => ({
    ...provided,
    zIndex: '99999',
  }),
  placeholder: provided => ({
    ...provided,
    color: '#ffffff',
  }),
  singleValue: provided => ({
    ...provided,
    color: '#ffffff',
  }),
};
export default customStyles;
