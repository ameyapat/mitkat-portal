import React, { Component } from 'react';
import CustomModal from '../../../ui/modal';
import {
  getQuadrantEventList,
  getQuadrantEventDetails,
  updateQuadrantDetails,
  deleteQuadrantDetails,
  getQuadrants,
} from '../../../../requestor/quadrants/requestor';
import {
  setQuadrantList,
  updateQuadrantPoints,
  updateQuadrantDetail,
} from '../../../../actions/quadrantActions';
import { sortByCountryIds } from './utils';
import { toggleToast } from '../../../../actions/index';
import TableList from '../../../ui/tablelist';
import TableListRow from '../../../ui/tablelist/TableListRow';
import TableListCell from '../../../ui/tablelist/TableListCell';
import KaleidoscopeHeader from './KaleidoscopeHeader';
import KscopeUpdateEvent from './KscopeUpdateEvent';
import KscopeUpdateDataPoints from './KscopeUpdateDataPoints';
import { connect } from 'react-redux';
import './style.scss';

const toastObj = {
  toastMsg: '',
  showToast: true,
};

class Kaleidoscope extends Component {
  state = {
    showQuadModal: false,
    showDataPointsModal: false,
    searchedValue: null,
    isUpdateEvent: false,
    isUpdateDataPoints: false,
    currentEditId: null,
    currentCurrentPointId: null,
  };

  componentDidMount() {
    this.fetchQuadrantEventList();
  }

  fetchQuadrantEventList = () => {
    const { dispatch } = this.props;
    getQuadrantEventList().then(data => {
      dispatch(setQuadrantList(data));
    });
  };

  resetSearchKeyword = () => this.setState({ searchedValue: null });

  handleDeleteQuadDetail = id => {
    const { dispatch } = this.props;
    deleteQuadrantDetails(id).then(data => {
      if (data && data.success === true) {
        let toastObj = {
          toastMsg: data.message,
          showToast: true,
        };
        dispatch(toggleToast(toastObj));
        this.resetSearchKeyword();
        this.fetchQuadrantEventList();
      } else {
        let toastObj = {
          toastMsg: 'Something went wrong, Please try again',
          showToast: true,
        };
        dispatch(toggleToast(toastObj));
      }
    });
  };

  renderQuadrantListHeader = () => {
    return (
      <TableListRow>
        <TableListCell value="Title" />
        <TableListCell value="Topic" />
        <TableListCell value="Last Coordinates for Month" />
        <TableListCell value="#" />
      </TableListRow>
    );
  };

  renderQuadrantList = quadrantEventList => {
    const { searchedValue } = this.state;
    const regex = new RegExp(searchedValue, 'gi');
    return (
      <TableList>
        {quadrantEventList
          .filter(filteredItem => {
            if (searchedValue === null || searchedValue === '') {
              return filteredItem;
            } else if (
              filteredItem.title.match(regex) ||
              filteredItem.topicName.match(regex) ||
              filteredItem.monthYear.match(regex)
            ) {
              return filteredItem;
            }
          })
          .map(filteredItem => {
            return (
              <TableListRow
                key={filteredItem.id}
                classes={{ root: 'kaleidoscope-table-row' }}
              >
                <TableListCell value={filteredItem.title} />
                <TableListCell value={filteredItem.topicName} />
                <TableListCell value={filteredItem.monthYear} />
                <TableListCell>
                  <div className="kaleidoscope-btnWrapper">
                    <button
                      className="btn--edit"
                      onClick={() => {
                        this.toggleQuadModal(true, true);
                        this.setEditId(filteredItem.id);
                      }}
                    >
                      <i className="icon-uniE93E"></i>
                    </button>
                    <button
                      className="btn--delete"
                      onClick={() =>
                        this.handleDeleteQuadDetail(filteredItem.id)
                      }
                    >
                      <i className="icon-uniE949"></i>
                    </button>
                  </div>
                </TableListCell>
              </TableListRow>
            );
          })}
      </TableList>
    );
  };

  setSearchedValue = searchedValue => this.setState({ searchedValue });

  toggleQuadModal = (showQuadModal, isUpdateEvent) =>
    this.setState({ showQuadModal, isUpdateEvent });

  toggleDataPointsModal = (showDataPointsModal, isUpdateDataPoints) => {
    this.setState({ showDataPointsModal, isUpdateDataPoints });
  };

  submitQuadrantDetails = () => {
    const {
      quadrantDetails: {
        id,
        title,
        topicid,
        riskCategoryid,
        quadrantCountries,
        // likelihood,
        // impact,
        // regionScale,
        // forecast,
      },
      quadrantPoints,
    } = this.props.quadrantState;
    const { dispatch } = this.props;
    const reqData = {
      id,
      title,
      topicid,
      riskCategoryid,
      quadrantCountries: sortByCountryIds(quadrantCountries),
      // likelihood,
      // impact,
      // regionScale,
      // forecast,
      quadrantPoints,
    };

    updateQuadrantDetails(reqData).then(data => {
      toastObj.toastMsg = data.message;

      if (data && data.success === true) {
        this.resetSearchKeyword();
        this.fetchQuadrantEventList();
        this.toggleQuadModal(false, false);
        this.clearDetails();
      }

      dispatch(toggleToast(toastObj));
    });
  };

  setEditId = currentEditId => {
    this.setState({ currentEditId });
  };

  setCurrentPointId = currentCurrentPointId => {
    this.setState({ currentCurrentPointId }, () => {
      if (currentCurrentPointId) {
        this.toggleDataPointsModal(true, true);
      }
    });
  };

  clearDetails = () => {
    const { dispatch } = this.props;
    dispatch(updateQuadrantDetail({}));
    dispatch(updateQuadrantPoints([]));
  };

  render() {
    const {
      quadrantState: { quadrantEventList = [] },
    } = this.props;
    const {
      showQuadModal,
      showDataPointsModal,
      isUpdateEvent,
      currentEditId,
      currentCurrentPointId,
      isUpdateDataPoints,
    } = this.state;
    return (
      <div id="kaleidoscope">
        <KaleidoscopeHeader
          setSearchedValue={this.setSearchedValue}
          parentHandler={() => this.toggleQuadModal(true, false)}
        />
        <div className="kaleidoscope__wrapper">
          {quadrantEventList.length && (
            <div>
              {this.renderQuadrantListHeader()}
              {this.renderQuadrantList(quadrantEventList)}
            </div>
          )}
        </div>

        <CustomModal showModal={showQuadModal}>
          <div className="modal__inner">
            <KscopeUpdateEvent
              isUpdateEvent={isUpdateEvent}
              toggleQuadModal={() => {
                this.toggleQuadModal(false, false);
                this.clearDetails();
              }}
              handleEventSubmit={this.submitQuadrantDetails}
              toggleDataPointsModal={this.toggleDataPointsModal}
              currentEditId={currentEditId}
              clearDetails={this.clearDetails}
              setCurrentPointId={this.setCurrentPointId}
            />
          </div>
        </CustomModal>
        <CustomModal showModal={showDataPointsModal}>
          <div className="modal__inner">
            <KscopeUpdateDataPoints
              toggleDataPointsModal={this.toggleDataPointsModal}
              setCurrentPointId={this.setCurrentPointId}
              currentCurrentPointId={currentCurrentPointId}
              isUpdateDataPoints={isUpdateDataPoints}
            />
          </div>
        </CustomModal>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    quadrantState: state.quadrantState,
  };
};

const mapDispatchToProps = dispatch => ({ dispatch });

export default connect(mapStateToProps, mapDispatchToProps)(Kaleidoscope);
