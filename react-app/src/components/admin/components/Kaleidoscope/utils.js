export const sortByCountryIds = selectedCountries => {
  const withCountryIds = [];
  selectedCountries.map(country => {
    withCountryIds.push({
      countryid: country.value,
    });
  });
  return withCountryIds;
};

export const sortSelectedCountries = (selectedCountries, countries) => {
  const withCountryValues = [];

  selectedCountries.map(country => {
    withCountryValues.push({
      value: country.countryid,
      label: getLabelsFromCountries(country.countryid, countries)[0].label,
    });
  });

  return withCountryValues;
};

const getLabelsFromCountries = (countryId, countries) =>
  countries.filter(country => country.value === countryId);
