import React, { Component } from 'react'
import PropTypes from 'prop-types';

import CustomModal from '../../../ui/modal'
import ModalWithPortal from '../../../ui/modalWithPortal/ModalWithPortal'

import './style.scss';


class MultipleLocations extends Component{
    render(){
        const { showModal = false, children, closeMultipleLocationModal } = this.props;
        return(
            <ModalWithPortal>
                <CustomModal showModal={showModal}>
                    <>
                    <div className="multiple__header"></div>
                    {children}
                    <button className="btn btnCloseMuliple" onClick={() => closeMultipleLocationModal(false)}>&times;</button>
                    <div className="multiple__footer">
                        <button className="btn btnUpdateMultiple" onClick={() => closeMultipleLocationModal(false)}>Done</button>
                    </div>
                    </>
                </CustomModal>
            </ModalWithPortal>
        )
    }
}


MultipleLocations.propTypes = {
    showModal: PropTypes.bool.isRequired,
    children: PropTypes.node.isRequired
}

export default MultipleLocations