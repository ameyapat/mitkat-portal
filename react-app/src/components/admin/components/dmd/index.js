import React, { Component, Fragment } from 'react';
import './style.scss';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import { fetchRiskProducts, searchByKeyword } from '../../../../helpers/utils';
import { withCookies } from 'react-cookie';
import { fetchApi } from '../../../../helpers/http/fetch';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import TableListRow from '../../../ui/tablelist/TableListRow';
import TableListCell from '../../../ui/tablelist/TableListCell';
import TableList from '../../../ui/tablelist';
import SearchBox from '../../../ui/searchBox';
import store from '../../../../store';
import { toggleToast } from '../../../../actions';
import injectSheet from 'react-jss';
import styles from './style';

@injectSheet(styles)
@withCookies
class Dmd extends Component {
  state = {
    selectedProduct: '',
    products: [],
    trackers: [],
    defaultTrackers: [],
    selectedTrackers: [],
    attachLogo: 1,
    attachSignature: 1,
  };

  componentDidMount() {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    fetchRiskProducts(authToken).then(products =>
      this.setState({ products }, () => console.log(this.state)),
    );
  }

  handleChange = e => {
    let value = e.target.value;
    this.setState({ selectedProduct: value }, () => {
      this.getTrackerList(value);
    });
  };

  getRiskXML = () => {
    let { products } = this.state;
    let productsXML = [];
    productsXML = products.map(i => (
      <MenuItem value={i.id}>{i.trackerName}</MenuItem>
    ));
    return productsXML;
  };

  getTrackerList = id => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let reqObj = {
      url: `${API_ROUTES.getEventsForProduct}/${id}`,
      method: 'POST',
      isAuth: true,
      authToken,
      showToggle: true,
    };
    fetchApi(reqObj)
      .then(data => {
        this.setState({ trackers: data, defaultTrackers: data });
      })
      .catch(e => console.log(e));
  };

  handleSelectChange = (e, id) => {
    let { selectedTrackers } = this.state;
    // let isChecked = e.target.checked;
    if (selectedTrackers.includes(id)) {
      let index = selectedTrackers.indexOf(id);
      selectedTrackers.splice(index, 1);
    } else {
      selectedTrackers.push(id);
    }

    this.setState({ selectedTrackers }, () => console.log(this.state));
  };

  generateListXML = () => {
    let { trackers, selectedTrackers } = this.state;
    let trackersXML = [];

    return (trackersXML = trackers.map(t => {
      return (
        <TableListRow
          key={t.id}
          classes={{ root: t.relevency ? 'highlight' : '' }}
        >
          <TableListCell value={t.date} />
          <TableListCell value={t.title} />
          <TableListCell value={t.country} />
          <TableListCell value={t.state} />
          <TableListCell value={t.metro} />
          <TableListCell value={t.creator} />
          <TableListCell value={t.storytype} />
          <TableListCell
            value={
              t.importance ? (
                <i class="fas fa-star"></i>
              ) : (
                <i class="far fa-star"></i>
              )
            }
          />
          <TableListCell>
            <input
              checked={selectedTrackers.includes(t.id)} // @sr => dev test this
              type="checkbox"
              onChange={e => this.handleSelectChange(e, t.id)}
            />
          </TableListCell>
        </TableListRow>
      );
    }));
  };

  handleRadioInput = (value, type) => this.setState({ [type]: value });

  submitTracker = () => {
    let { cookies } = this.props;
    let {
      attachLogo,
      attachSignature,
      selectedProduct,
      selectedTrackers,
    } = this.state;
    let authToken = cookies.get('authToken');
    let reqData = {
      logo: attachLogo,
      signature: attachSignature,
      clientid: selectedProduct,
      eventid: selectedTrackers,
    };
    let reqObj = {
      url: API_ROUTES.dispatchMail,
      method: 'POST',
      isAuth: true,
      authToken,
      data: reqData,
      showToggle: true,
    };
    fetchApi(reqObj)
      .then(data => {
        let toastObj = {
          toastMsg: `Total failed emails: ${data.totalFailedEmails}`,
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      })
      .catch(e => console.log(e));
  };

  handleSearch = e => {
    let value = e.target.value;
    let { trackers, defaultTrackers } = this.state;
    trackers = searchByKeyword(value, trackers);
    if (trackers.length > 0 && value) {
      this.setState({ trackers });
    } else {
      this.setState({ trackers: defaultTrackers });
    }
  };

  render() {
    let {
      products,
      trackers,
      attachLogo,
      attachSignature,
      selectedTrackers,
    } = this.state;
    const { classes } = this.props;
    return (
      <div className="dmd__wrap">
        <div className="product__header">
          <div className="product__title">
            <h3>Dispatch Morning Deliverables</h3>
            {trackers.length > 0 && (
              <Fragment>
                <label>
                  IRT: <i class="fas fa-star"></i>
                </label>
                <label>
                  Non IRT: <i class="far fa-star"></i>
                </label>
              </Fragment>
            )}
          </div>
          {trackers.length > 0 && (
            <div className="product__search">
              <SearchBox
                classes="fa fa-search"
                placeholder="Search by keyword"
                parentHandler={this.handleSearch}
              />
            </div>
          )}
          <div className="product_select">
            <FormControl className="form--control">
              <InputLabel
                classes={{
                  root: classes.rootLabel,
                }}
                htmlFor="select-product"
              >
                Select Product
              </InputLabel>
              <Select
                value={this.state.selectedProduct}
                onChange={this.handleChange}
                inputProps={{
                  shrink: false,
                  name: 'Select Product',
                  id: 'select-product',
                  classes: {
                    root: classes.rootInput,
                    input: classes.rootInput,
                    select: classes.rootSelect,
                    icon: classes.rootIcon,
                  },
                }}
              >
                {products.length > 0 && this.getRiskXML()}
              </Select>
            </FormControl>
          </div>
        </div>
        {trackers.length > 0 && selectedTrackers.length > 0 && (
          <div className="actionables__wrap">
            <div>
              <h3>Attach Logo</h3>
              <label>Organization</label>
              <input
                checked={attachLogo === 1}
                type="radio"
                name="logo"
                value={1}
                onChange={e => this.handleRadioInput(1, 'attachLogo')}
              />
              <label>Client</label>
              <input
                checked={attachLogo === 2}
                type="radio"
                name="logo"
                value={2}
                onChange={e => this.handleRadioInput(2, 'attachLogo')}
              />
              <label>No Logo</label>
              <input
                checked={attachLogo === 0}
                type="radio"
                name="logo"
                value={0}
                onChange={e => this.handleRadioInput(0, 'attachLogo')}
              />
            </div>
            <div>
              <h3>Attach Signature</h3>
              <label>Organization</label>
              <input
                checked={attachSignature === 1}
                type="radio"
                name="signature"
                value={1}
                onChange={e => this.handleRadioInput(1, 'attachSignature')}
              />
              <label>No Signature</label>
              <input
                checked={attachSignature === 0}
                type="radio"
                name="signature"
                value={0}
                onChange={e => this.handleRadioInput(0, 'attachSignature')}
              />
            </div>
            <div>
              <button onClick={() => this.submitTracker()}>Send Tracker</button>
            </div>
          </div>
        )}
        {trackers.length > 0 ? (
          <div className="product__body">
            <div>
              <div className="tlist__head">
                <div>
                  <label>Date</label>
                </div>
                <div>
                  <label>Subject</label>
                </div>
                <div>
                  <label>Country</label>
                </div>
                <div>
                  <label>State</label>
                </div>
                <div>
                  <label>City</label>
                </div>
                <div>
                  <label>Creator</label>
                </div>
                <div>
                  <label>Region Type</label>
                </div>
                <div>
                  <label>IRT</label>
                </div>
                <div>
                  <label>Select</label>
                </div>
              </div>
              <TableList classes={{ root: 'darkBlue' }}>
                {this.generateListXML()}
              </TableList>
            </div>
          </div>
        ) : (
          <p className="info--txt">Select product to view list of events</p>
        )}
      </div>
    );
  }
}

export default Dmd;
