import React, { Component } from 'react';
//components
import CustomModal from '../../../ui/modal';
import EditEventUI from '../createEventUI/EditEventUI';
import TextField from '@material-ui/core/TextField';
//styles
import styles from './style';
//helpers
import { getCreatorTrackingId } from '../../../../requestor/admin/requestor';
import store from '../../../../store';
import { toggleToast } from '../../../../actions';
//npm packages
import injectSheet from 'react-jss';
import { withCookies } from 'react-cookie';
import DailyRisks from '../dailyRiskTracker/dailyRisks';
import Empty from '../../../ui/empty';
import emptyImage from '../../../../assets/hugo-no-messages.png';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import { fetchApi, uploadApi } from '../../../../helpers/http/fetch';
import AddEventUI from '../createEventUI/AddEventUI';
import AlertDialog from '../../../ui/alertDialog';
import CustomImageUpload from '../../../ui/customImageUpload';

@withCookies
@injectSheet(styles)
class TrackById extends Component {
  state = {
    trackingId: '',
    showEventModal: false,
    eventDetails: null,
    dailyRiskList: [],
    defaultList: [],
    showAddEventModal: false,
    showAlert: false,
    showUploadForm: false,
  };

  editByTrackingId = () => {
    getCreatorTrackingId(this.state.trackingId)
      .then(data => {
        this.setState({
          dailyRiskList: data,
          defaultList: data,
        });
      })
      .catch(e => {
        let toastObj = {
          toastMsg: 'Something Went Wrong!',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      });
  };

  handleDeleteEvent = () => {
    let { cookies } = this.props;
    const { eventId } = this.state;
    let authToken = cookies.get('authToken');
    let reqObj = {
      url: `${API_ROUTES.deleteTodaysEvent}/${eventId}`,
      isAuth: true,
      authToken,
      method: 'POST',
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        let toastObj = {
          toastMsg: data.message,
          showToast: data.success,
        };
        store.dispatch(toggleToast(toastObj));
      })
      .catch(e => {
        let toastObj = {
          toastMsg: 'Error deleting report',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      });
    this.setState({ showAlert: false, eventId: '' });
  };

  handleEditEvent = eventId => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');

    let reqObj = {
      method: 'POST',
      isAuth: true,
      authToken,
      url: `${API_ROUTES.viewEventHistory}/${eventId}`,
      showToggle: true,
    };

    fetchApi(reqObj).then(eventDetails => {
      this.setState({ eventDetails }, () => {
        this.showEventModal(true);
      });
    });
  };

  handleAddNewEvent = eventId => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');

    let reqObj = {
      method: 'POST',
      isAuth: true,
      authToken,
      url: `${API_ROUTES.viewEventHistory}/${eventId}`,
      showToggle: true,
    };

    fetchApi(reqObj).then(eventDetails => {
      this.setState({ eventDetails }, () => {
        this.showAddEventModal(true);
      });
    });
  };

  showAddEventModal = show => this.setState({ showAddEventModal: show });
  showEventModal = show => this.setState({ showEventModal: show });
  handleShowAlert = eventId => this.setState({ eventId, showAlert: true });
  handleCloseAlert = () => this.setState({ showAlert: false, eventId: '' });

  handleInputChange = e => {
    this.setState({
      trackingId: e.target.value,
    });
  };

  handleUploadImage = selectedEventId => {
    this.setState({ selectedEventId });
    this.handleToggleShowForm(true);
  };

  handleToggleShowForm = showUploadForm => this.setState({ showUploadForm });

  handleReportUploadImage = data => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let { imageFile } = data;
    const formData = new FormData();

    formData.append('image', imageFile);

    let params = {
      url: `${API_ROUTES.uploadReportingImg}/${this.state.selectedEventId}`,
      reqObj: {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${authToken}`,
        },
        body: formData,
      },
    };

    uploadApi(params.url, params.reqObj).then(res => {
      let toastObj = {
        toastMsg: '',
        showToast: true,
      };
      if (res) {
        toastObj.toastMsg = res.message;
      }
      store.dispatch(toggleToast(toastObj));
    });
  };

  render() {
    const { classes } = this.props;
    const {
      showEventModal,
      eventDetails,
      trackingId,
      dailyRiskList,
      showAddEventModal,
      showAlert,
      showUploadForm,
    } = this.state;

    return (
      <div id="innerDailyRisk" className={classes.rootDailyRisk}>
        <div className={classes.dateWrap}>
          <h1>
            <i className="fas fa-list" /> Enter Alert Id
          </h1>
          <div className={classes.datesWrapper}>
            <TextField
              // error={validation[input.id].isInvalid}
              id={'trackingId'}
              label={'Enter Alert Id'}
              fullWidth
              type={'text'}
              InputLabelProps={{
                shrink: true,
                classes: {
                  root: classes.rootLabel,
                },
              }}
              InputProps={{
                classes: {
                  root: classes.rootText,
                  input: classes.rootInput,
                  notchedOutline: classes.notchedOutline,
                },
                endAdornment: null,
              }}
              value={trackingId}
              onChange={this.handleInputChange}
              margin="normal"
            />
            <button
              className={classes.btnSubmit}
              onClick={() => this.editByTrackingId()}
            >
              Submit
            </button>
          </div>
        </div>
        <div className={classes.riskWrapOutter}>
          {dailyRiskList?.length > 0 ? (
            <DailyRisks
              items={dailyRiskList}
              handleEditEvent={this.handleEditEvent}
              handleShowAlert={this.handleShowAlert}
              handleUploadImage={this.handleUploadImage}
              handleAddNewEvent={this.handleAddNewEvent}
            />
          ) : (
            <Empty
              imgSrc={emptyImage}
              alt="No items available!"
              title="No items available!"
            />
          )}
        </div>
        <CustomModal
          showModal={showEventModal}
          closeModal={() => this.showEventModal(false)}
        >
          <div id="edit-event--modal" className="modal__inner">
            {eventDetails && (
              <EditEventUI
                eventDetails={eventDetails}
                closeEventModal={this.showEventModal}
                showUpdateBtn={true}
                isApprover={false}
                hasAllTopics={true}
              />
            )}
          </div>
        </CustomModal>
        <CustomModal
          showModal={showAddEventModal}
          closeModal={() => this.showAddEventModal(false)}
        >
          <div id="edit-event--modal" className="modal__inner">
            {eventDetails && (
              <AddEventUI
                eventDetails={eventDetails}
                closeEventModal={this.showAddEventModal}
                showUpdateBtn={true}
                isApprover={false}
                hasAllTopics={true}
              />
            )}
          </div>
        </CustomModal>
        <CustomModal
          showModal={showUploadForm}
          closeModal={() => this.handleToggleShowForm(false)}
        >
          <div className={classes.rootAddForm}>
            <span
              className="close-times"
              onClick={() => this.handleToggleShowForm(false)}
            >
              &times;
            </span>
            <CustomImageUpload
              heading="Upload Reporting Image"
              imagePlaceHolder="Upload Reporting Image"
              handleSubmit={this.handleReportUploadImage}
            />
          </div>
        </CustomModal>
        {showAlert && (
          <AlertDialog
            isOpen={showAlert}
            title="Delete Event"
            description="You are about to delete an event, are you sure?"
            parentEventHandler={() => this.handleDeleteEvent()}
            handleClose={() => this.handleCloseAlert()}
          />
        )}
      </div>
    );
  }
}

export default TrackById;
