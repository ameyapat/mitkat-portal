export default {
  rootDailyRisk: {
    background: 'var(--backgroundSolid)',
    padding: 20,
  },
  innerDailyRisk: {
    background: 'var(--backgroundSolid)',
    padding: 10,
  },
  dateWrap: {
    borderBottom: '0px solid #f1f1f1',
    padding: '15px 0',
    marginBottom: 15,
    '& i': {
      fontSize: 15,
      color: 'var(--whiteMediumEmp)',
      marginRight: 10,
    },
    '& h1': {
      paddingBottom: 15,
      fontSize: 16,
      color: 'var(--whiteMediumEmp)',
      fontWeight: 'normal',
    },
  },
  dateWrapInner: {
    padding: '10px 5px',
    border: '1px solid #f1f1f1',
    boxShadow: '0 6px 10px 0 #f1f1f1',
    borderRadius: 3,
    cursor: 'pointer',
    display: 'inline-block',
  },
  riskWrapOutter: {
    '& img': {
      width: 256,
    },
  },
  rootDailyRisks: {
    borderTop: '1px dashed #f1f1f1',
    marginTop: 15,
  },
  btnSubmit: {
    margin: 0,
    marginRight: 15,
    border: '1px solid #2980b9',
    background: 'transparent',
    textTransform: 'uppercase',
    padding: 8,
    color: '#2980b9',
    borderRadius: 3,
    cursor: 'pointer',

    '&:disabled': {
      opacity: '0.4',
      cursor: 'not-allowed',
    },
  },
  datesWrapper: {
    display: 'flex',
    alignItems: 'center',
  },
  rootText: {
    margin: '15px 0',
    color: 'var(--whiteHighEmp) !important',
  },
  notchedOutline: {
    borderColor: 'var(--whiteHighEmp) !important ',
  },
  rootInput: {
    color: 'var(--whiteHighEmp) !important',
  },
  rootLabel: {
    color: 'rgba(255,255,255, 0.67) !important',
  },
  rootAddForm: {
    background: '#fff',
    height: 'auto',
    padding: 30,
    position: 'relative',
    maxWidth: 400,
    margin: '30px auto',

    '& .close-times': {
      color: '#000',
      fontSize: 30,
      position: 'absolute',
      right: 10,
      top: 10,
      cursor: 'pointer',
    },
  },
};
