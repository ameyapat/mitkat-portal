import React, { useEffect, useState } from 'react';
import './style.scss';
import moment from 'moment';
import {
  categories,
  eventTypeConstants,
} from '../../../client/components/rimeDashboard/helpers/constants';
// common style
// import '../../../client/sass/style.scss';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import { updateAdminSelectedFeed } from '../../../../actions/rimeActions';
import Typography from '@mui/material/Typography';
import { useSelector, useDispatch } from 'react-redux';
import CustomModal from '../../../ui/modal';
import Checkbox from '@mui/material/Checkbox';
import ArticleModal from '../../../client/components/rimeEventCard/articleModal';
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye';
import {
  lastUpdateDate,
  eDate,
} from '../../../client/components/rimeDashboard/helpers/utils';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import injectSheet from 'react-jss';
import styles from './style';
import TextField from '@mui/material/TextField';

const pinMappingRgba = {
  1: 'low',
  2: 'medium',
  3: 'high',
};

const PastDataCard = props => {
  const {
    srno,
    eventId,
    title,
    categoryId,
    secondaryCatergoryId,
    eventDate,
    relevancy,
    sourceLinks,
    location,
    noOfArticles,
    lastUpdatedDate,
    tags,
    checkSubmitEvent,
    classes,
    userName,
  } = props;

  useEffect(() => {
    if (checkSubmitEvent) {
      setChecked(false);
    }
  });
  const adminSelectedFeed = useSelector(
    store => store.rime.updateAdminSelectedFeed,
  );

  const [category, setCategory] = useState('');
  const [showModal, setShowModal] = useState(false);
  const [checked, setChecked] = useState(false);
  const [eventType, setEventType] = useState(1);
  const [selectedFeedId, setSelectedFeedId] = useState('');
  const [showEventType, setShowEventType] = useState(false);
  const [dropDownCategory, setDropDownCategory] = useState(categoryId);
  const [showCategoryDropdown, setshowCategoryDropdown] = useState(false);
  const [eventTitle, setEventTitle] = useState(title);
  const [showTitleTextField, setShowTitleTextField] = useState(false);
  const [eventLocation, setEventLocation] = useState(location);
  const [showLocationTextField, setShowLocationTextField] = useState(false);
  const setTheme = useSelector(state => state.themeChange.setDarkTheme);
  const refreshDuration = useSelector(state => state.rime.refreshDuration);
  const dispatch = useDispatch();

  const TIMEOUT = refreshDuration * 60000;

  const mapRiskCategory = () => {
    categories.map(subitem => {
      if (categoryId === subitem.id) {
        setCategory(subitem.label);
      }
    });
  };

  const handleModal = () => {
    setShowModal(false);
  };

  const showArticleModal = () => {
    setShowModal(true);
  };

  const getEventTypeList = () => {
    let eventTypeList = [];
    eventTypeList = eventTypeConstants.map(i => (
      <MenuItem value={i.id}>{i.label}</MenuItem>
    ));
    return eventTypeList;
  };
  const getCategoryList = () => {
    let categoryList = [];
    categoryList = categories.map(i => (
      <MenuItem value={i.id}>{i.label}</MenuItem>
    ));
    return categoryList;
  };
  const handleChange = (event, value) => {
    let check = event.target.checked;
    if (check) {
      let feedWithDeletedData = adminSelectedFeed?.filter(
        item => item.eventId !== eventId,
      );
      setSelectedFeedId(value);
      let adminSelection = {
        eventId: value,
        eventType: eventType,
        eventPrimaryRiskCategory: dropDownCategory,
        eventTitle: eventTitle,
        eventLoc: eventLocation,
      };
      dispatch(
        updateAdminSelectedFeed([...feedWithDeletedData, adminSelection]),
      );
    } else {
      let feedWithDeletedData = adminSelectedFeed?.filter(
        item => item.eventId !== eventId,
      );
      dispatch(updateAdminSelectedFeed([...feedWithDeletedData]));
    }
    setChecked(event.target.checked);
    setShowEventType(event.target.checked);
    setshowCategoryDropdown(event.target.checked);
    setShowTitleTextField(event.target.checked);
    setShowLocationTextField(event.target.checked);
  };

  const handleEventChange = e => {
    let value = e.target.value;
    if (value) {
      let feedWithDeletedData = adminSelectedFeed?.filter(
        item => item.eventId !== eventId,
      );
      setEventType(value);
      let adminSelection = {
        eventId: selectedFeedId,
        eventType: value,
        eventPrimaryRiskCategory: dropDownCategory,
        eventTitle: eventTitle,
        eventLoc: eventLocation,
      };
      dispatch(
        updateAdminSelectedFeed([...feedWithDeletedData, adminSelection]),
      );
    }
  };
  const handleTitleChange = e => {
    let value = e.target.value;
    if (value) {
      let feedWithDeletedData = adminSelectedFeed?.filter(
        item => item.eventId !== eventId,
      );
      setEventTitle(value);
      let adminSelection = {
        eventId: selectedFeedId,
        eventType: eventType,
        eventPrimaryRiskCategory: dropDownCategory,
        eventTitle: value,
        eventLoc: eventLocation,
      };
      dispatch(
        updateAdminSelectedFeed([...feedWithDeletedData, adminSelection]),
      );
    }
  };

  const handleLocationChange = e => {
    let value = e.target.value;
    if (value) {
      let feedWithDeletedData = adminSelectedFeed?.filter(
        item => item.eventId !== eventId,
      );
      setEventLocation(value);
      let adminSelection = {
        eventId: selectedFeedId,
        eventType: eventType,
        eventPrimaryRiskCategory: dropDownCategory,
        eventTitle: eventTitle,
        eventLoc: value,
      };
      dispatch(
        updateAdminSelectedFeed([...feedWithDeletedData, adminSelection]),
      );
    }
  };

  const handleCategoryChange = e => {
    let value = e.target.value;
    if (value) {
      let feedWithDeletedData = adminSelectedFeed?.filter(
        item => item.eventId !== eventId,
      );
      setDropDownCategory(value);
      let adminSelection = {
        eventId: selectedFeedId,
        eventType: eventType,
        eventPrimaryRiskCategory: value,
        eventTitle: eventTitle,
        eventLoc: eventLocation,
      };
      dispatch(
        updateAdminSelectedFeed([...feedWithDeletedData, adminSelection]),
      );
    }
  };

  useEffect(() => {
    mapRiskCategory();
    setTimeout(() => {}, TIMEOUT);
  });

  let tag = tags && tags.split(',');
  let allLinks = sourceLinks && sourceLinks.split(',');

  const utcLastUpdateDate = lastUpdateDate(lastUpdatedDate);
  const utcDate = eDate(eventDate);

  return (
    <div className="screening_card">
      <Typography component="div" className="pastData__eventsrno">
        {srno}
      </Typography>
      <div id="pastData__card">
        <Card
          className={`screening_eventCardList ${pinMappingRgba[relevancy]}`}
        >
          <div className="pastData__header">
            <div className="pastData_pill_container">
              <div
                className={`pastData__pill ${relevancy &&
                  pinMappingRgba[relevancy]}`}
              >
                <Typography component="span" className="pastData__cat__pill">
                  {category ? category : 'Not Available'}
                </Typography>
              </div>
            </div>
            <div
              className="article__content__pill"
              onClick={() => showArticleModal()}
            >
              <Typography
                variant="subtitle1"
                color="white"
                component="span"
                className="pastData__eventbody__Head"
              >
                <span className="title">No of Articles:</span>
              </Typography>
              <Typography
                component="span"
                className="pastData__eventbody__Head"
              >
                &nbsp;
                {noOfArticles ? noOfArticles : 'Not Available'}
              </Typography>
              <span>&nbsp;</span>
              <RemoveRedEyeIcon className="eyeIcon" />
            </div>
            <div className="eventFilteredCard_userName">
              <Typography
                variant="subtitle1"
                color="text.secondary"
                component="span"
                className="rimeFilteredCard__eventbody__Head"
              >
                <span className="title">Username:</span>
              </Typography>
              <Typography
                component="span"
                className="rimeFilteredCard__eventbody__Head"
              >
                &nbsp;
                {userName ? userName : 'Not Available'}
              </Typography>
            </div>
            <div className="pastData__card__innercolumn">
              <div>
                <Typography component="span" className="screening_event_date">
                  &nbsp;{moment(utcDate).format('Do MMM YYYY')}
                </Typography>
                <Typography
                  variant="subtitle1"
                  color="white"
                  component="span"
                  className="screening_update__date"
                >
                  Updated:&nbsp;
                </Typography>
                <Typography component="span" className="screening_update__date">
                  {moment(utcLastUpdateDate).format('Do MMM YYYY, h:mm:ss a')}
                </Typography>
              </div>
            </div>
          </div>
          <div className="pastData__card__row1">
            <div className="pastData__card__innercolumn2">
              <Box sx={{ display: 'flex', flexDirection: 'column' }}>
                <div className="pastData_heading">
                  <Typography
                    component="div"
                    className="pastData__eventtitle__wrap"
                    style={{ color: 'white' }}
                  >
                    {title}
                  </Typography>
                  <div className="event_screening_location">
                    <Typography
                      variant="subtitle1"
                      color="white"
                      component="span"
                      className="pastData__eventbody__subHead"
                    >
                      Location:
                    </Typography>
                    <Typography
                      component="span"
                      className="pastData__eventbody__location_wrap"
                    >
                      &nbsp;{location ? location : 'Not Available'}
                    </Typography>
                  </div>
                  <div>
                    <Checkbox
                      checked={checked}
                      onClick={e => handleChange(e, eventId)}
                      inputProps={{ 'aria-label': 'controlled' }}
                      style={{ paddingBottom: '5px', color: 'white' }}
                    />
                  </div>
                </div>
              </Box>
            </div>
          </div>
          <div className="screening_dropdowns">
            {showEventType && (
              <div>
                <FormControl className="eventType_dropdown">
                  <InputLabel
                    classes={{
                      root: classes.rootLabel,
                    }}
                    htmlFor="select-event-type"
                  >
                    Select Event Type
                  </InputLabel>
                  <Select
                    value={eventType}
                    onChange={handleEventChange}
                    inputProps={{
                      shrink: false,
                      name: 'Select Event Type',
                      id: 'select-event-type',
                      classes: {
                        root: classes.rootInput,
                        input: classes.rootInput,
                        select: classes.rootSelect,
                        icon: classes.rootIcon,
                      },
                    }}
                  >
                    {getEventTypeList()}
                  </Select>
                </FormControl>
              </div>
            )}
            {showCategoryDropdown && (
              <div>
                <FormControl className="riskCategory_dropdown">
                  <InputLabel
                    classes={{
                      root: classes.rootLabel,
                    }}
                    htmlFor="select-event-type"
                  >
                    Select Risk category
                  </InputLabel>
                  <Select
                    value={dropDownCategory}
                    onChange={handleCategoryChange}
                    inputProps={{
                      shrink: false,
                      name: 'Select Event Type',
                      id: 'select-event-type',
                      classes: {
                        root: classes.rootInput,
                        input: classes.rootInput,
                        select: classes.rootSelect,
                        icon: classes.rootIcon,
                      },
                    }}
                  >
                    {getCategoryList()}
                  </Select>
                </FormControl>
              </div>
            )}
            {showTitleTextField && (
              <div className="editTitle_container">
                <TextField
                  id="EditTitle"
                  label="Edit Title"
                  variant="standard"
                  type="text"
                  InputLabelProps={{
                    shrink: true,
                  }}
                  value={eventTitle}
                  fullWidth
                  onChange={handleTitleChange}
                />
              </div>
            )}
            {showLocationTextField && (
              <div className="editLocation_container">
                <TextField
                  id="EditLocation"
                  label="Edit Location"
                  variant="standard"
                  type="text"
                  fullWidth
                  InputLabelProps={{
                    shrink: true,
                  }}
                  value={eventLocation}
                  onChange={handleLocationChange}
                />
              </div>
            )}
          </div>
        </Card>
        <CustomModal showModal={showModal}>
          <div
            id="rimeEventArchivesModalCont"
            className={`modal__inner ${setTheme ? 'dark' : 'light'}`}
          >
            <ArticleModal
              alllinks={allLinks}
              closeEventModal={() => handleModal()}
            />
          </div>
        </CustomModal>
      </div>
    </div>
  );
};

export default injectSheet(styles)(PastDataCard);
