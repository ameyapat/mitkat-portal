import React, { Component } from 'react';
import { fetchApi } from '../../../../helpers/http/fetch';
import { withCookies } from 'react-cookie';
import { updateAdminSelectedFeed } from '../../../../actions/rimeActions';
import './style.scss';
import ScreeningEventCard from '../screeningEventCard';
import { connect } from 'react-redux';
import store from '../../../../store';
import { toggleToast } from '../../../../actions';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import Loader from '../../../ui/loader';
import {
  rimeFilteredFeedSearch,
  rimeScreeningFeedSearch,
} from '../../../../helpers/utils';
import SearchBox from '../../../ui/searchBox';
import PastDataCard from '../pastDataCard';

@withCookies
@connect(
  state => {
    return {
      rime: state.rime,
      themeChange: state.themeChange,
    };
  },
  dispatch => {
    return {
      dispatch,
    };
  },
)
class PastData extends Component {
  state = {
    rimeScreeningFeed: [],
    initialFeed: [],
    selectedFeed: [],
    searchValue: '',
    checkSubmitEvent: false,
    isFetching: false,
    adminSelectedFeed: [],
  };

  componentDidMount() {
    this.getScreeningFeed();
  }
  getScreeningFeed() {
    this.setState({ rimeScreeningFeed: [], initialFeed: [], isFetching: true });
    const { rimeScreeningFeed } = this.state;
    store.dispatch(updateAdminSelectedFeed([]));
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let reqObj1 = {
      method: 'GET',
      isAuth: true,
      authToken,
      url: API_ROUTES.pastEvents,
    };

    fetchApi(reqObj1)
      .then(data => {
        this.setState({
          rimeScreeningFeed:
            data.length > 0
              ? [...rimeScreeningFeed, ...data].reverse()
              : this.state.rimeScreeningFeed,
          initialFeed: data,
          isFetching: false,
        });
      })
      .catch(e => console.log(e));
  }

  handleInputChange = e => {
    let value = e.target.value;
    this.setState(
      state => {
        return {
          searchValue: value,
        };
      },
      () => {
        this.handleSearch();
      },
    );
  };

  handleSearch = () => {
    let { searchValue, rimeScreeningFeed, initialFeed } = this.state;
    this.state.rimeScreeningFeed.forEach(item => {
      delete item['eventid'];
    });
    this.state.initialFeed.forEach(item => {
      delete item['eventid'];
    });
    if (searchValue) {
      let searchedList = rimeScreeningFeedSearch(
        this.state.searchValue,
        initialFeed,
      );
      this.setState({ rimeScreeningFeed: searchedList });
    } else {
      this.setState({ rimeScreeningFeed: initialFeed });
    }
  };

  setAllData = () => {
    this.setState({
      checkSubmitEvent: false,
      selectedFeed: [],
      rimeScreeningFeed: [],
    });
    store.dispatch(updateAdminSelectedFeed([]));
  };

  handleSubmit() {
    const { updateAdminSelectedFeed } = this.props.rime;
    const { rimeScreeningFeed, selectedFeed } = this.state;
    updateAdminSelectedFeed?.length &&
      updateAdminSelectedFeed.map(item => {
        rimeScreeningFeed.map(selectedItem => {
          if (item.eventId === selectedItem.eventId) {
            selectedItem.deletedID = '';
            selectedItem.eventType = item.eventType;
            selectedItem.eventPrimaryRiskCategory =
              item.eventPrimaryRiskCategory;
            selectedItem.eventTitle = item.eventTitle;
            selectedItem.eventLoc = item.eventLoc;
            selectedFeed.push(selectedItem);
          }
        });
      });
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let reqObj = {
      url: API_ROUTES.addSelectedEvents,
      method: 'POST',
      isAuth: true,
      authToken,
      data: { data: selectedFeed },
      showToggle: true,
    };

    fetchApi(reqObj).then(data => {
      if (data.success) {
        let toastObj = {
          toastMsg: 'data updated successfully',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
        this.setAllData();
      } else {
        store.dispatch(
          toggleToast({ showToast: true, toastMsg: data.message }),
        );
      }
    });
  }

  render() {
    const {
      rimeScreeningFeed,
      checkSubmitEvent,
      isFetching,
      searchValue,
    } = this.state;
    return (
      <div id="rimeScreening">
        {isFetching && <Loader />}
        <div className="screeningTopNavbar">
          <div className="rimeFilteredData__searchbox">
            <SearchBox
              placeholder={'Search by keyword'}
              parentHandler={e => this.handleInputChange(e)}
              value={searchValue}
            />
          </div>
          <div className="leftNav">
            <button
              className="submit_button"
              onClick={() => this.handleSubmit()}
            >
              Submit
            </button>
          </div>
        </div>
        <div className="screeningEventList">
          {rimeScreeningFeed?.length > 0 ? (
            rimeScreeningFeed.map((item, index) => {
              return (
                <PastDataCard
                  srno={index + 1}
                  eventId={item.eventId}
                  title={item.eventTitle}
                  sourceLinks={item.eventAllNewsLinks}
                  location={item.eventLoc}
                  noOfArticles={item.count}
                  categoryId={item.eventPrimaryRiskCategory}
                  secondaryCatergoryId={item.eventSecondaryRiskCategory}
                  link={item.eventLink}
                  userName={item.userName}
                  hasClose={false}
                  eventDate={item.eventDate}
                  lastUpdatedDate={item.eventLatestUpdate}
                  relevancy={item.eventRiskLevel}
                  imgURL={item.eventImageUrl}
                  tags={item.eventTags}
                  language={item.language}
                  checkSubmitEvent={checkSubmitEvent}
                />
              );
            })
          ) : (
            <div className="event_screening_notfound"> No feed Found</div>
          )}
        </div>
      </div>
    );
  }
}

export default PastData;
