import React, { Component, Fragment } from 'react';
//components @material-ui
import TextField from '@material-ui/core/TextField';
import RadioGroup from '@material-ui/core/RadioGroup';
import Radio from '@material-ui/core/Radio';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import { Divider } from '@material-ui/core';
import Select from 'react-select';

import ClientEmails from '../clientEmails';
import SubscribeBox from '../editClientDetails/SubscribeBox';
//constants'
// import { CLIENT_DETAILS } from '../helpers/constants';
//sass
import '../../sass/viewUpdateClient.scss';
//redux
import { connect } from 'react-redux';
import { withCookies } from 'react-cookie';

@withCookies
@connect(state => {
  return {
    appState: state.appState,
  };
})
class CustomClientDetails extends Component {
  renderInputFields = mapOnKey => {
    return mapOnKey.map(input => {
      switch (input.type) {
        case 'text':
          return (
            <Fragment key={input.id}>
              <input
                className={`input--${input.id}`}
                type={input.type}
                value={this.props.attributes[input.selectedText]}
                placeholder={input.label}
                onChange={e =>
                  this.props.handleChange(input.id, e.target.value)
                }
              />
              {/* {input.id === 'companyName' && validation[input.id].isInvalid && (
                <span className="errorTxt">{validation[input.id].message}</span>
              )} */}
            </Fragment>
          );
        case 'number':
          return (
            <Fragment key={input.id}>
              <TextField
                id={input.id}
                label={input.label}
                type={input.type}
                InputLabelProps={{
                  shrink: true,
                }}
                variant="outlined"
                onChange={e =>
                  this.props.handleChange(input.selectedText, e.target.value)
                }
                className={`input--${input.id}`}
                value={this.props.attributes[input.selectedText]}
              />
              {/* {input.id === 'companyName' && validation[input.id].isInvalid && (
                <span className="errorTxt">{validation[input.id].message}</span>
              )} */}
            </Fragment>
          );
        case 'date':
          return (
            <Fragment>
              <TextField
                key={input.id}
                id={input.id}
                label={input.label}
                type="date"
                fullWidth
                onChange={e =>
                  this.props.handleChange(input.id, e.target.value)
                }
                InputLabelProps={{
                  shrink: true,
                }}
                margin="normal"
                value={this.props.attributes[input.selectedText]}
              />
              {/* {validation[input.id].isInvalid && <span className="errorTxt">{validation[input.id].message}</span>} */}
            </Fragment>
          );
        case 'email':
          return (
            <Fragment>
              <ClientEmails
                label={input.label}
                handleAddEmail={this.props.handleAddEmail}
                handleDeleteEmail={this.props.handleDeleteEmail}
                clientEmailList={this.props.clientEmails}
              />
              {/* {validation[input.id].isInvalid && <span className="errorTxt">{validation[input.id].message}</span>} */}
            </Fragment>
          );
        case 'select':
          if (input.isMultiSelect) {
            return (
              <div key={input.id} className="selectWrapper">
                {/* <label>{input.label}</label> */}
                <Select
                  isMulti
                  className="select--options"
                  value={this.props.attributes[input.selectedText]}
                  placeholder={input.label}
                  onChange={e =>
                    this.props.handleSelectChange(e, input.selectedText)
                  }
                  options={this.props.attributes[input.id] || []}
                />
                {/* {(input.selectedText == 'selectedProduct' ||
                  input.selectedText == 'selectedIndustry') &&
                  validation[input.selectedText].isInvalid && (
                    <span className="errorTxt">
                      {validation[input.selectedText].message}
                    </span>
                  )} */}
                {/* {((input.selectedText == 'selectedProduct') || (input.selectedText == 'selectedIndustry')) && <span className="errorTxt">{input.selectedText}</span>} */}
              </div>
            );
          } else {
            return (
              <div key={input.id} className="selectWrapper">
                {/* <label>{input.label}</label> */}
                <Select
                  className="select--options"
                  value={this.props.attributes[input.id]}
                  placeholder={input.label}
                  onChange={e => this.props.handleSelectChange(e, input.id)}
                  options={this.props?.attributes?.[input?.selectedText]}
                />
                {/* { validation[input.selectedText].isInvalid && <span className="errorTxt">{validation[input.selectedText].message}</span>} */}
              </div>
            );
          }
        case 'radio':
          return (
            <FormControl component="fieldset">
              <label>{input.label}</label>
              <RadioGroup
                aria-label={input.label}
                name={input.id}
                value={
                  this.props.attributes[input.selectedText] === 'true'
                    ? true
                    : false
                }
                onChange={e =>
                  this.props.handleChange(input.selectedText, e.target.value)
                }
                className="radioWrapper"
              >
                <FormControlLabel
                  value={true}
                  control={<Radio />}
                  label="Yes"
                />
                <FormControlLabel
                  value={false}
                  control={<Radio />}
                  label="No"
                />
              </RadioGroup>
            </FormControl>
          );
        default:
          return null;
      }
    });
  };

  render() {
    const {
      companyDetails,
      clientMapping,
      accessControl,
      others,
      accessControlNested: { rime, querySystem },
    } = this.props.clientDetails;
    // const { adminUserDetails, queryDetails, handleSubscription } = this.props;

    return (
      <div className="clientDetailsWrap">
        <h2>Company Details</h2>
        <div className="textInputs">
          {this.renderInputFields(companyDetails)}
        </div>
        <Divider className="divider-pd-15" />
        <h2>Client Mapping</h2>
        <div className="textInputs">
          {this.renderInputFields(clientMapping)}
        </div>
        <Divider className="divider-pd-15" />
        <h2>Access Control</h2>
        <div className="textInputs">
          {this.renderInputFields(querySystem)}
          {/* <Select
            className="select--options"
            value={primarySPOC}
            placeholder="Primary Responder"
            onChange={e => this.props.handleSelectChange('primarySPOC', e)}
            options={this.props.adminUserDetails && redefineObj(adminUserDetails)}
          /> */}
          {/* <SubscribeBox
            queryDetails={queryDetails}
            adminUserDetails={adminUserDetails}
            handleSubscription={handleSubscription}
            primarySPOC={this.props.primarySPOC}
            secondarySPOC={this.props.secondarySPOC}
          /> */}
          {this.renderInputFields(rime)}
          {this.renderInputFields(accessControl)}
        </div>
        <Divider className="divider-pd-15" />
        <h2>Others</h2>
        <div className="textInputs">{this.renderInputFields(others)}</div>
        <Divider className="divider-pd-15" />
      </div>
    );
  }
}

export default CustomClientDetails;
