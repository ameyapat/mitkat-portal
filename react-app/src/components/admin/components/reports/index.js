import React, { Component, Fragment } from "react";
//
import styles from "./style";
import InfoCardList from "../../../ui/infoCardList";
import InfoCard from "../../../ui/infoCard";
import CustomModal from "../../../ui/modal";
import AlertDialog from "../../../ui/alertDialog";
import CustomReportsForm from '../../../ui/customReportsForm';
import Empty from '../../../ui/empty';
//helpers
import injectSheet from "react-jss";
import { withCookies } from "react-cookie";
import { fetchApi, uploadApi } from "../../../../helpers/http/fetch";
import { API_ROUTES } from "../../../../helpers/http/apiRoutes";
import store from "../../../../store";
import { toggleToast } from "../../../../actions";
import Actionables from "./Actionables";
import { getValuesFromObjects } from '../advisory/utils';
import { getMonthlyDetails } from '../../../../requestor/admin/requestor';

class Reports extends Component {
  state = {
    reports: [],
    modalUrl: '',
    showAlert: false,
    showAddForm: false,
    isEdit: false,
    monthlyDetails: null,
  };

  componentDidMount() {
    this.fetchReports().then(data => {
      this.setState({ reports: data });
    });
  }

  fetchReports = () => {
    return new Promise((res, rej) => {
      let { cookies } = this.props;
      let authToken = cookies.get('authToken');
      let reqObj = {
        url: API_ROUTES.getReportsCreator,
        isAuth: true,
        authToken,
        method: 'POST',
        showToggle: true,
      };

      fetchApi(reqObj)
        .then(data => {
          if (data.success) {
            res(data.output);
          }
        })
        .catch(e => rej(e));
    });
  };

  getReportsXML = () => {
    let { reports } = this.state;
    let reportsXML = reports.map(l => {
      return (
        <InfoCard
          key={l.id}
          handleDelete={this.handleDelete}
          handleView={this.handleView}
          handleEdit={this.handleEdit}
          handleDownload={this.handleDownload}
          id={l.id}
          title={l.title}
          imgUrl={l.dispayurl}
          date={l.date}
        />
      );
    });

    return reportsXML;
  };

  handleDelete = id => {
    this.setState({
      showAlert: true,
      reportId: id,
    });
  };

  handleEdit = id => {
    this.setState(
      {
        advisoryId: id,
      },
      () => {
        this.fetchMonthlyDetails();
      },
    );
  };

  fetchMonthlyDetails = () => {
    const { advisoryId } = this.state;
    getMonthlyDetails(advisoryId)
      .then(monthlyDetails => {
        this.setState({ monthlyDetails });
        this.handleEditForm({
          isEdit: true,
          showAddForm: true,
        });
      })
      .catch(e => console.log('Error fetching details', e));
  };

  handleEditForm = ({ isEdit, showAddForm }) => {
    this.setState({
      isEdit,
      showAddForm,
    });
  };

  handleConfirmDelete = id => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let reqObj = {
      url: `${API_ROUTES.deleteReport}/${id}`,
      isAuth: true,
      authToken,
      method: 'POST',
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        if (data.success) {
          this.setState(
            {
              showAlert: false,
            },
            () => {
              let toastObj = {
                toastMsg: 'Report Deleted Successfully!',
                showToast: data.success,
              };
              store.dispatch(toggleToast(toastObj));
              this.fetchReports().then(data => {
                this.setState({ reports: data });
              });
            },
          );
        }
      })
      .catch(e => console.log(e));
  };

  handleDownload = id => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let reqObj = {
      url: `${API_ROUTES.downloadReport}/${id}`,
      isAuth: true,
      authToken,
      method: 'POST',
      isBlob: true,
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(blobObj => {
        let a = document.createElement('a');
        document.body.appendChild(a);
        let objUrl = new Blob([blobObj], { type: 'application/pdf' });
        let url = window.URL.createObjectURL(objUrl);
        a.href = url;
        a.download = 'report-pdf';
        a.click();
        window.URL.revokeObjectURL(url);
      })
      .catch(e => console.log(e));
  };

  handleView = id => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let reqObj = {
      url: `${API_ROUTES.viewReport}/${id}`,
      isAuth: true,
      authToken,
      method: 'POST',
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        if (data.success) {
          this.setState({
            showModal: true,
            modalUrl: data.output.viewimageurl,
          });
        }
      })
      .catch(e => console.log(e));
  };

  toggleModal = showModal => this.setState({ showModal });

  handleCloseAlert = () => this.setState({ showAlert: false });

  handleToggleAdd = showAddForm => this.setState({ showAddForm });

  handleEditForm = ({ isEdit, showAddForm }) => {
    this.setState({
      isEdit,
      showAddForm,
    });
  };

  handleSubmit = data => {
    // addNewReport
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    const { isEdit, advisoryId } = this.state;
    let { imageFile, document, title, selectedCountries, date } = data;
    const formData = new FormData();

    formData.append('displaypic', imageFile);
    formData.append('title', title);
    formData.append('report', document);
    formData.append('type', '1');
    formData.append('countries', getValuesFromObjects(selectedCountries));
    formData.append('date', date || '');

    if (isEdit) {
      formData.append('advisoryid', advisoryId);
    }

    let params = {
      url: `${!isEdit ? API_ROUTES.addNewReport : API_ROUTES.updateNewReport}`,
      reqObj: {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${authToken}`,
        },
        body: formData,
      },
    };

    uploadApi(params.url, params.reqObj).then(res => {
      if (res.success) {
        let toastObj = {
          toastMsg: !isEdit
            ? 'Report Added Successfully!'
            : 'Report Updated Successfully!',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
        this.handleToggleAdd();
        this.fetchReports().then(data => {
          this.setState({ reports: data, isEdit: false });
        });
      } else {
        let toastObj = {
          toastMsg: 'Something went wrong!',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      }
    });
  };

  handleCloseModal = () => {
    this.handleToggleAdd(false);
    this.setState({ isEdit: false });
  };

  render() {
    let { classes } = this.props;
    let {
      reports,
      showModal,
      modalUrl,
      showAlert,
      reportId,
      showAddForm,
      isEdit,
      monthlyDetails,
    } = this.state;

    return (
      <Fragment>
        <Actionables handleAdd={this.handleToggleAdd} />
        <InfoCardList>
          {reports.length > 0 ? (
            this.getReportsXML()
          ) : (
            <Empty title="No Reports Available" />
          )}
          <CustomModal
            showModal={showModal}
            closeModal={() => this.toggleModal(false)}
          >
            <div className={classes.rootIframe}>
              <span
                className="close-times"
                onClick={() => this.toggleModal(false)}
              >
                &times;
              </span>
              <iframe src={modalUrl} height="100%" width="100%" title="pdf" />
            </div>
          </CustomModal>
          <CustomModal
            showModal={showAddForm}
            closeModal={() => this.toggleModal(false)}
          >
            <div className={classes.rootAddForm}>
              <span
                className="close-times"
                onClick={() => this.handleCloseModal()}
              >
                &times;
              </span>
              <CustomReportsForm
                heading="Add New Report"
                titlePlaceHolder="Enter title"
                imagePlaceHolder="Upload Display Image"
                filePlaceHolder="Upload Report"
                handleSubmit={this.handleSubmit}
                hasCustomProps={false}
                showTopics={false}
                showCountries={true}
                showDate={true}
                isEdit={isEdit}
                details={monthlyDetails}
                showLinks={true}
              />
            </div>
          </CustomModal>
          {showAlert && (
            <AlertDialog
              isOpen={showAlert}
              title="Delete Report"
              description="You are about to delete an event, are you sure?"
              parentEventHandler={() => this.handleConfirmDelete(reportId)}
              handleClose={() => this.handleCloseAlert()}
            />
          )}
        </InfoCardList>
      </Fragment>
    );
  }
}

export default withCookies(injectSheet(styles)(Reports));
