import React, { Component } from 'react';
//styles
import './style.scss';
//components
import TopNav from '../topNav';
import Dashboard from '../dashboard';
import UserManagement from '../../userManagement';
import PartnerManagement from '../../partnerManagement';
import PartnerAlerts from '../../partnerManagement/PartnerAlerts';
import ClientManagement from '../../clientManagement';
import UpdateEvents from '../updateEvents';
import EditEvents from '../editEvents';
import Amd from '../amd';
import Dmd from '../dmd';
import ApproveEmail from '../approveEmail';
import TrackDispatch from '../trackDispatch';
import DispatchEmail from '../dispatchEmail';
import ApproveSms from '../approveSms';
import DispatchSms from '../dispatchSms';
import Queries from '../queries';
import Advisory from '../advisory';
import TravelAdvisory from '../travelAdvisory';
import Blog from '../blog';
import Podcast from '../podcast';
import Reports from '../reports';
import SpecialReports from '../specialReports';
import RiskReview from '../riskReview';
import { Switch, Route, withRouter } from 'react-router-dom';
import DailyRiskTracker from '../dailyRiskTracker';
import DownloadEvents from '../downloadEvents';
import TrackById from '../trackById';
import RiskWatch from '../../pages/RiskWatch';
import Corona from '../../pages/Corona';
import CoronaCity from '../../pages/CoronaCity';
import ViewCovidState from '../viewCovidState';
import ViewCovidCity from '../viewCovidCity';
import MonthlyCompilation from '../monthlyCompilation';
import PerformanceIndex from '../../performanceIndex';
import TopicManagement from '../../pages/topicManagement';
import IndiaWeekly from '../indiaWeekly';
import SouthAsiaWeekly from '../southAsiaWeekly';
import EMEAWeekly from '../emeaWeekly';
import AmericasWeekly from '../americasWeekly';
import ApacWeekly from '../apacWeekly';
import ViewCovidAlert from '../corona/createCovidAlert/ViewCovidAlert';
import ViewCovidCountry from '../corona/createCovidAlert/ViewCovidCountry';
import KaleidoscopePage from '../../pages/KaleidoscopePage';
import CoverageRating from '../coverageRating';
import DuplicateEvents from '../duplicateEvents';
import Screening from '../screening';
import RimeFiltereedFeed from '../rimeFiltereedData';
import CountryAnalysis from '../countryAnalysis';
import CuratorAnalysis from '../curatorAnalysis';
import CityAnalysis from '../cityAnalysis';
import MonitoringScreen from '../monitoringScreen';
import CreateEmailAlert from '../createEmailAlert';
import AddEvent from '../addEvent';
import MissedLinks from '../missedLinks';
import PastData from '../pastData';

@withRouter
class RightDash extends Component {
  render() {
    return (
      <div id="rightDash">
        <TopNav />
        <div>
          <Switch>
            <Route
              exact
              path={`${this.props.match.url}`}
              component={Dashboard}
            />
            <Route
              exact
              path={`${this.props.match.path}/user`}
              component={UserManagement}
            />
            <Route
              exact
              path={`${this.props.match.path}/partner`}
              component={PartnerManagement}
            />
            <Route
              exact
              path={`${this.props.match.path}/partnerAlerts`}
              component={PartnerAlerts}
            />
            <Route
              path={`${this.props.match.path}/client`}
              component={ClientManagement}
            />
            <Route
              path={`${this.props.match.path}/performanceIndex`}
              component={PerformanceIndex}
            />
            <Route
              path={`${this.props.match.path}/updateEvents`}
              component={UpdateEvents}
            />
            <Route
              path={`${this.props.match.path}/editEvents`}
              component={EditEvents}
            />
            <Route path={`${this.props.match.path}/amd`} component={Amd} />
            <Route path={`${this.props.match.path}/dmd`} component={Dmd} />
            <Route
              path={`${this.props.match.path}/approveEmail`}
              component={ApproveEmail}
            />
            <Route
              path={`${this.props.match.path}/dispatchEmail`}
              component={DispatchEmail}
            />
            <Route
              path={`${this.props.match.path}/trackDispatch`}
              component={TrackDispatch}
            />
            <Route
              path={`${this.props.match.path}/duplicateEvents`}
              component={DuplicateEvents}
            />
            <Route
              path={`${this.props.match.path}/approveSms`}
              component={ApproveSms}
            />
            <Route
              path={`${this.props.match.path}/dispatchSms`}
              component={DispatchSms}
            />
            <Route
              path={`${this.props.match.path}/queries`}
              component={Queries}
            />
            <Route
              path={`${this.props.match.path}/advisory`}
              component={Advisory}
            />
            <Route
              path={`${this.props.match.path}/travelAdvisory`}
              component={TravelAdvisory}
            />
            <Route path={`${this.props.match.path}/blog`} component={Blog} />
            <Route
              path={`${this.props.match.path}/podcast`}
              component={Podcast}
            />
            <Route
              path={`${this.props.match.path}/reports`}
              component={Reports}
            />
            <Route
              path={`${this.props.match.path}/specialReports`}
              component={SpecialReports}
            />
            <Route
              path={`${this.props.match.path}/riskReview`}
              component={RiskReview}
            />
            <Route
              path={`${this.props.match.path}/dailyRiskTracker`}
              component={DailyRiskTracker}
            />
            <Route
              path={`${this.props.match.path}/downloadEvents`}
              component={DownloadEvents}
            />
            <Route
              path={`${this.props.match.path}/trackingId`}
              component={TrackById}
            />
            <Route
              path={`${this.props.match.path}/viewRiskWatch`}
              component={RiskWatch}
            />
            <Route
              path={`${this.props.match.path}/monthlyCompilation`}
              component={MonthlyCompilation}
            />
            <Route
              path={`${this.props.match.path}/rimeScreening`}
              component={Screening}
            />
            <Route
              path={`${this.props.match.path}/addEvent`}
              component={AddEvent}
            />
            <Route
              path={`${this.props.match.path}/monitoringScreen`}
              component={MonitoringScreen}
            />
            <Route
              path={`${this.props.match.path}/rimeFilteredData`}
              component={RimeFiltereedFeed}
            />
            <Route
              path={`${this.props.match.path}/countryAnalysis`}
              component={CountryAnalysis}
            />
            <Route
              path={`${this.props.match.path}/cityAnalysis`}
              component={CityAnalysis}
            />
            <Route
              path={`${this.props.match.path}/missedLinks`}
              component={MissedLinks}
            />
            <Route
              path={`${this.props.match.path}/pastData`}
              component={PastData}
            />
            <Route
              path={`${this.props.match.path}/curatorAnalysis`}
              component={CuratorAnalysis}
            />
            <Route
              path={`${this.props.match.path}/viewCoronaAlerts`}
              component={Corona}
            />
            <Route
              path={`${this.props.match.path}/viewCoronaAlertsCity`}
              component={CoronaCity}
            />
            <Route
              path={`${this.props.match.path}/viewCovidState`}
              component={ViewCovidState}
            />
            <Route
              path={`${this.props.match.path}/viewCovidCity`}
              component={ViewCovidCity}
            />
            <Route
              path={`${this.props.match.path}/topicManagement`}
              component={TopicManagement}
            />
            <Route
              path={`${this.props.match.path}/coverageRating`}
              component={CoverageRating}
            />

            <Route
              path={`${this.props.match.path}/kaleidoscope`}
              component={KaleidoscopePage}
            />
            <Route
              path={`${this.props.match.path}/indiaWeekly`}
              component={IndiaWeekly}
            />
            <Route
              path={`${this.props.match.path}/southasiaWeekly`}
              component={SouthAsiaWeekly}
            />
            <Route
              path={`${this.props.match.path}/apacWeekly`}
              component={ApacWeekly}
            />
            <Route
              path={`${this.props.match.path}/emeaWeekly`}
              component={EMEAWeekly}
            />
            <Route
              path={`${this.props.match.path}/americasWeekly`}
              component={AmericasWeekly}
            />
            <Route
              path={`${this.props.match.path}/viewCovidAlert`}
              component={ViewCovidAlert}
            />
            <Route
              path={`${this.props.match.path}/viewCovidCountry`}
              component={ViewCovidCountry}
            />
          </Switch>
        </div>
      </div>
    );
  }
}

export default RightDash;
