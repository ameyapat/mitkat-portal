import React, { Component } from 'react';
import './style.scss';

import { fetchApi } from '../../../../helpers/http/fetch';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';

import TableList from '../../../ui/tablelist';
import TableListRow from '../../../ui/tablelist/TableListRow';
import TableListCell from '../../../ui/tablelist/TableListCell';
import Empty from '../../../ui/empty';

import { withCookies } from 'react-cookie';

@withCookies
class TrackDispatch extends Component {
  state = {
    dispatchList: [],
  };

  componentDidMount() {
    const { cookies } = this.props;
    const authToken = cookies.get('authToken');

    let reqObj = {
      url: `${API_ROUTES.trackDispatch}`,
      method: 'GET',
      isAuth: true,
      authToken,
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => this.setState({ dispatchList: data }))
      .catch(e => console.log(e));
  }

  renderDispatchList = () => {
    const { dispatchList } = this.state;
    return (
      dispatchList.length > 0 &&
      dispatchList.map(item => {
        return (
          <TableListRow id={item.dispatchtime}>
            <TableListCell value={item.trackername} />
            <TableListCell value={item.dispatchedBy} />
            <TableListCell value={item.dispatchtime} />
            <TableListCell>
              {item.isdispatchedtoday ? (
                <span className="circleDispatched isDispatched">
                  <i className="fas fa-ellipsis-h"></i>
                </span>
              ) : (
                <span className="circleDispatched notDispatched">
                  <i className="fas fa-ellipsis-h"></i>
                </span>
              )}
            </TableListCell>
          </TableListRow>
        );
      })
    );
  };

  render() {
    const { dispatchList } = this.state;
    return (
      <div id="trackDispatch">
        <div className="tlist__head">
          <div>
            <label>Tracker Name</label>
          </div>
          <div>
            <label>Dispatched By</label>
          </div>
          <div>
            <label>Last Dispatch Time</label>
          </div>
          <div>
            <label>Has Dispatched Today?</label>
          </div>
        </div>
        <TableList classes={{ root: 'darkBlue' }}>
          {dispatchList.length > 0 ? (
            this.renderDispatchList()
          ) : (
            <Empty title="No dispatch records found" />
          )}
        </TableList>
      </div>
    );
  }
}

export default TrackDispatch;
