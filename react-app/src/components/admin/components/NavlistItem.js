import React from 'react';
import { withRouter, Link } from 'react-router-dom';

const NavlistItem = ({ index, text, to, match, icon, handleClick, currActiveIndex }) => ( <Link className={currActiveIndex === index ? 'active' : ''} to={`${match.url}/${to}`}>
            <li onClick={() => handleClick(index)} className="nav-list-item">
                <i className={icon}></i>
                <p>{text}</p>
            </li>
        </Link> 
);

export default withRouter(NavlistItem);