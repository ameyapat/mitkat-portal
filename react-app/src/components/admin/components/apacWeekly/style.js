export default {
  rootDailyRisk: {
    background: 'var(--backgroundSolid)',
    padding: 20,
  },
  innerDailyRisk: {
    background: 'var(--backgroundSolid)',
    padding: 10,
  },
  dateWrap: {
    borderBottom: '1px solid #f1f1f1',
    padding: '15px 0',
    marginBottom: 15,
    '& i': {
      fontSize: 15,
      color: 'var(--whiteMediumEmp)',
      marginRight: 10,
    },
    '& h1': {
      paddingBottom: 15,
      fontSize: 16,
      color: 'var(--whiteMediumEmp)',
      fontWeight: 'normal',
    },
  },
  dateWrapInner: {
    padding: '10px 5px',
    border: '1px solid #f1f1f1',
    boxShadow: '0 6px 10px 0 #f1f1f1',
    borderRadius: 3,
    cursor: 'pointer',
    display: 'inline-block',
  },
  datePickerInput: {
    fontSize: 14,
    color: 'var(--whiteMediumEmp)',
    background: 'red',
  },
  rootDatePicker: {
    maxWidth: '25%',
    display: 'flex',
    margin: '0 15px 15px',

    '&:nth-child(1)': {
      marginLeft: 0,
    },
  },
  riskWrapOutter: {
    '& img': {
      width: 256,
    },
  },
  rootDailyRisks: {
    borderTop: '1px dashed #f1f1f1',
    marginTop: 15,
  },
  btnSubmit: {
    margin: 0,
    marginRight: 15,
    border: '1px solid #2980b9',
    background: 'transparent',
    textTransform: 'uppercase',
    padding: 8,
    color: '#2980b9',
    borderRadius: 3,
    cursor: 'pointer',

    '&:disabled': {
      opacity: '0.4',
      cursor: 'not-allowed',
    },
  },
  btnEdit: {
    color: '#e58e26',
    background: 'transparent',
    border: '1px solid #e58e26',
  },
  btnDelete: {
    color: '#e55039',
    background: 'transparent',
    border: '1px solid #e55039',
  },
  btn: {
    borderRadius: 3,
    padding: 5,
    margin: '0 2px',
    cursor: 'pointer',
  },
  datesWrapper: {
    display: 'flex',
    alignItems: 'center',
    // justifyContent: 'space-around'
  },
  txtField: {
    color: '#666',
    fontSize: 14,
  },
  rootSearchWrapper: {
    maxWidth: 256,
    margin: '0 auto',
  },
  rootSelectAllCheck: {
    padding: 0,
  },
  rootSingleSelect: {
    // background: 'red'
  },
  customRootCheckbox: {
    padding: 0,
    marginLeft: 10,
    color: '#a9bf42 !important',
  },
  rootLabel: {
    color: 'rgba(255,255,255, 0.67) !important',
  },
  rootInput: {
    color: 'var(--whiteHighEmp) !important',
  },
  underline: {
    '&:after': {
      borderBottom: '2px solid #34495e !important',
    },
  },
  notchedOutline: {
    borderColor: 'var(--whiteHighEmp) !important ',
  },
};
