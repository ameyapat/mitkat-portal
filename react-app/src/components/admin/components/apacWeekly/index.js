import React, { Component, Fragment } from 'react';
import './style.scss';
//components
import Empty from '../../../ui/empty';
//Helpers
import { fetchApi } from '../../../../helpers/http/fetch';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import { withCookies } from 'react-cookie';
import TextField from '@material-ui/core/TextField';
import store from '../../../../store';
import { toggleToast } from '../../../../actions/';
import { editEventDetails, updateEvent } from '../../helpers/utils';
import CustomCheckbox from './customCheckbox';
import WeeklyEventModal from '../weeklyEventModal';
import injectSheet from 'react-jss';
import style from './style';
import CustomModal from '../../../ui/modal';
import WeeklyDispatchList from '../weeklyDispatchList';
@injectSheet(style)
@withCookies
class ApacWeekly extends Component {
  state = {
    eventListMapped: [],
    showEventModal: false,
    eventDetails: null,
    showAlert: false,
    eventId: '',
    description: '',
    selectedEventIds: [],
    calendarDate: '',
    showEditEvent: false,
    showDispatchModal: false,
  };

  componentDidMount() {
    this.getEvents();
  }

  getEvents = () => {
    let { cookies } = this.props;
    let { selectedEventIds } = this.state;
    let authToken = cookies.get('authToken');
    let reqObj = {
      url: API_ROUTES.getApacWeekly,
      isAuth: true,
      authToken,
      method: 'GET',
      showToggle: true,
    };

    fetchApi(reqObj).then(data => {
      let eventListMapped = data;
      eventListMapped.map(firstLevel => {
        firstLevel.eventlist.map(event => {
          event.isChecked = true;
          selectedEventIds.push(event.id);
          return event;
        });
      });
      this.setState({ eventListMapped, selectedEventIds });
    });
  };

  handleInputChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  dispatchWeekly = () => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let { selectedEventIds } = this.state;
    let dispatchData = { eventids: selectedEventIds };

    let reqObj = {
      url: API_ROUTES.dispatchApacWeekly,
      data: dispatchData,
      isAuth: true,
      authToken,
      method: 'POST',
      showToggle: true,
    };

    fetchApi(reqObj).then(data => {
      if (data && data.success === true) {
        let toastObj = {
          toastMsg: data.message,
          showToast: true,
        };

        store.dispatch(toggleToast(toastObj));
        this.toggleDispatchModal(false);
      }
      let toastObj = {
        toastMsg: 'Something went wrong, Please try again',
        showToast: true,
      };
      store.dispatch(toggleToast(toastObj));
    });
  };

  handleEventSave = (e, description, eventId) => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let weeklyDes = { description: this.state[description], eventid: eventId };
    let reqObj = {
      url: API_ROUTES.updateWeekly,
      data: weeklyDes,
      isAuth: true,
      authToken,
      method: 'POST',
      showToggle: true,
    };

    fetchApi(reqObj).then(data => {
      if (data && data.success === true) {
        let toastObj = {
          toastMsg: data.message,
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      }
      let toastObj = {
        toastMsg: 'Something went wrong, Please try again',
        showToast: true,
      };
      store.dispatch(toggleToast(toastObj));
    });
  };

  addToList = id => {
    let { selectedEventIds, eventListMapped } = this.state;

    if (!selectedEventIds.includes(id)) {
      selectedEventIds.push(id);
      eventListMapped.map(firstLevel => {
        firstLevel.eventlist.map(i => {
          if (i.id === id) {
            i.isChecked = true;
          }
        });
      });
    }

    this.setState({ selectedEventIds, eventListMapped });
  };

  handleEditEvent = eventId => {
    editEventDetails(eventId).then(eventDetails => {
      this.setState({ eventDetails }, () => {
        this.handleModal(true);
      });
    });
  };

  removeFromList = id => {
    let { selectedEventIds, eventListMapped } = this.state;

    if (selectedEventIds.includes(id)) {
      const idx = selectedEventIds.indexOf(id);
      selectedEventIds.splice(idx, 1);
      eventListMapped.map(firstLevel => {
        firstLevel.eventlist.map(i => {
          if (i.id === id) {
            i.isChecked = false;
          }
        });
      });
    }

    this.setState({ selectedEventIds, eventListMapped });
  };

  renderDispatchList = () => {
    let { eventListMapped } = this.state;

    return eventListMapped && eventListMapped.length > 0 ? (
      eventListMapped.map((firstLevel, idx) => (
        <div key={`${idx}_calEntries`} className="dispatch--divider">
          <div className="table__header">
            <span className="dispatch-date-pill">
              Date: {firstLevel.calenderdate}
            </span>
          </div>
          {firstLevel.eventlist.map(event => (
            <WeeklyDispatchList
              key={event.id}
              addToList={this.addToList}
              removeFromList={this.removeFromList}
              id={event.id}
              isSelected={event.isChecked}
              title={event.title}
            />
          ))}
        </div>
      ))
    ) : (
      <Empty title="No events available" />
    );
  };

  getTableItems = () => {
    let { eventListMapped } = this.state;
    let { classes } = this.props;
    return eventListMapped && eventListMapped.length > 0 ? (
      eventListMapped.map((firstLevel, idx) => (
        <Fragment key={`${idx}_calEntries`}>
          <tr>
            <td colSpan="4" className="table__header">
              Date: {firstLevel.calenderdate}
            </td>
          </tr>
          {firstLevel.eventlist.map(event => (
            <tr key={event.id}>
              <td>
                <p> Title: &nbsp; {event.title} </p>
                <TextField
                  placeholder={`Enter Description`}
                  name={`description_${event.id}_value`}
                  value={this.state[`description_${event.id}_value`]}
                  onChange={e => this.handleInputChange(e)}
                  defaultValue={event.description}
                  margin="normal"
                  variant="outlined"
                  multiline
                  fullWidth
                  rows="1"
                  rowsMax="3"
                  InputLabelProps={{
                    shrink: true,
                    classes: {
                      root: classes.rootLabel,
                    },
                  }}
                  InputProps={{
                    classes: {
                      root: classes.rootText,
                      input: classes.rootInput,
                      notchedOutline: classes.notchedOutline,
                    },
                    endAdornment: null,
                  }}
                />
              </td>
              <td>
                <button
                  className="btnSave"
                  onClick={e =>
                    this.handleEventSave(
                      e,
                      `description_${event.id}_value`,
                      event.id,
                    )
                  }
                >
                  Save
                </button>
                <button
                  className="btnSave"
                  onClick={() => this.handleEditEvent(event.id)}
                >
                  Edit
                </button>
              </td>
              {false && (
                <td>
                  <CustomCheckbox
                    addToList={this.addToList}
                    removeFromList={this.removeFromList}
                    id={event.id}
                    isSelected={event.isChecked}
                  />
                </td>
              )}
            </tr>
          ))}
        </Fragment>
      ))
    ) : (
      <Empty title="No events available" />
    );
  };

  handleModal = show => this.setState({ showEditEvent: show });

  handleUpdateEvent = () => {
    this.getEvents();
  };

  toggleDispatchModal = show => this.setState({ showDispatchModal: show });

  render() {
    let {
      eventListMapped,
      eventDetails,
      showEditEvent,
      showDispatchModal,
    } = this.state;

    return (
      <div className="apacweekly__wrap">
        <h2>
          APAC Weekly
          <button
            className="btnOpenDispatch"
            onClick={e => this.toggleDispatchModal(true)}
          >
            Dispatch Weekly
          </button>
        </h2>
        {eventListMapped.length > 0 ? (
          <div className="table__wrap">
            <table className="table">
              <tbody>{this.getTableItems()}</tbody>
            </table>
          </div>
        ) : (
          <Empty title="No events available" />
        )}
        {showEditEvent && (
          <WeeklyEventModal
            showEditEvent={showEditEvent}
            eventDetails={eventDetails}
            handleModal={this.handleModal}
            handleUpdateEvent={this.handleUpdateEvent}
          />
        )}
        {showDispatchModal && (
          <CustomModal showModal={showDispatchModal}>
            <div className="modal__inner dispatch">
              <div className="modal__body--dispatch">
                {this.renderDispatchList()}
              </div>
              <div className="modal__footer--dispatch">
                <button
                  className="btnDispatch"
                  onClick={e => this.dispatchWeekly()}
                >
                  Submit
                </button>
                <button
                  className="btnCancelDispatch"
                  onClick={e => this.toggleDispatchModal(false)}
                >
                  Cancel
                </button>
              </div>
            </div>
          </CustomModal>
        )}
      </div>
    );
  }
}

export default ApacWeekly;
