import React, { Component } from 'react';
//styles
import './style.scss';
import { connect } from 'react-redux';

@connect(state => {
  return {
    partnerDetails: state.partnerDetails.partnerDetails,
  };
})
class Logo extends Component {
  render() {
    let { whiteLogo, partnerName } = this.props.partnerDetails;

    return (
      <div id="logoWrap">
        <div className="l__logo">
          <img src={whiteLogo} alt={partnerName} />
        </div>
      </div>
    );
  }
}

export default Logo;
