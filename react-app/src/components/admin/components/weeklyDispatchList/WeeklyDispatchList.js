import React from 'react';
import CustomCheckbox from '../indiaWeekly/customCheckbox';
import './style.scss';

const WeeklyDispatchList = ({
  addToList,
  removeFromList,
  id,
  isSelected,
  title = '',
}) => {
  return (
    <div className="WeeklyDispatchList">
      <div className="WeeklyDispatchList__title">
        <h3>{title}</h3>
      </div>
      <div className="WeeklyDispatchList__checkbox">
        <CustomCheckbox
          addToList={addToList}
          removeFromList={removeFromList}
          id={id}
          isSelected={isSelected}
        />
      </div>
    </div>
  );
};

export default WeeklyDispatchList;
