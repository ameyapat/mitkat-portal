import React from 'react';
import './style.scss';

const MetricsItems = ({color, num,  title}) => (
    <div className="metricsItem">
        <div className="circle" style={{
            borderLeft: `7px solid #f1f1f1`,
            borderRight: `7px solid ${color}`,
            borderBottom: `7px solid ${color}`
        }}>
            <label>{num}</label>
        </div>
        <div><p>{title}</p></div>
    </div>
)

export default MetricsItems;
