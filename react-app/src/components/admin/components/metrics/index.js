import React, { Component } from 'react'
//components
import MetricsItems from './MetricsItems';
//helper
import { metrics } from '../../../../helpers/constants';

export default class Metrics extends Component {

    generateMetrics = () => {
        let metricsXML = [];
        metricsXML = metrics.map((item, index) => <MetricsItems 
            key={index}
            color={item.color}
            num={item.number}
            title={item.title}
        />)

        return metricsXML;
    }

  render() {
    return (
      <div id="metricsWrap">
        {this.generateMetrics()}
      </div>
    )
  }
}
