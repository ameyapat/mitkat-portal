import React, { Component, Fragment } from 'react';
import './style.scss';

class ClientEmails extends Component {

    state = {
        inputEmail: ""
    }

    handleInputChange = (e) => {
        this.setState({inputEmail: e.target.value});
    }

    handleAddEmail = () => {
        let { inputEmail } = this.state;
        this.props.handleAddEmail(inputEmail, 'add');
        this.setState({inputEmail: ''});
    }

    handleDeleteEmail = (index) => {
        this.props.handleDeleteEmail(index, 'delete');
    }

  render() {
      let { clientEmailList, label } = this.props;
      let { inputEmail } = this.state;
    return (
      <div className="clientEmailsWrap">
          <h4>Client Emails</h4>
          <div>
              <div className="email--input--wrap">
                <label>{label}</label>
                <input 
                    value={this.state.inputEmail}
                    className="email--input" 
                    onChange={this.handleInputChange}
                    type="email" 
                    placeholder="Enter Email"
                    />
                </div>
                {
                    inputEmail.length > 0 &&
                    <button 
                        className="btn btn--add" 
                        onClick={() => this.handleAddEmail()}
                    >
                    Add
                    </button>
                }
          </div>
          {
              clientEmailList && clientEmailList.length > 0 &&
                <div className="email--list--wrap">
                <h3>Emails list of emails</h3>
                {
                    clientEmailList.map((eid, i) => <Fragment>
                        <div className="delete--wrap">
                            <div>
                                <p>{eid}</p>
                            </div>
                            <div>
                                <button
                                className="btn btn--delete"
                                onClick={() => this.handleDeleteEmail(i)}
                                >
                                Delete
                                </button>
                            </div>
                        </div>
                    </Fragment>)
                }                    
                </div>              
          }
      </div>
    )
  }
}

export default ClientEmails;
