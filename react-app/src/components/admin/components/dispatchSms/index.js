import React, { Component } from 'react';
import './style.scss';
import { withCookies } from 'react-cookie';
import { fetchApi } from '../../../../helpers/http/fetch';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import TableListRow from '../../../ui/tablelist/TableListRow';
import TableListCell from '../../../ui/tablelist/TableListCell';
import Empty from '../../../ui/empty';
import TableList from '../../../ui/tablelist';
import store from '../../../../store';
import { toggleToast } from '../../../../actions';

@withCookies
class DispatchSms extends Component {
  state = {
    selectedSmsId: null,
    smsAlertList: [],
  };

  componentDidMount() {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    // this.getSmsAlerts(authToken);
  }

  //to get table list of sms alerts to be dispatched
  getSmsAlerts = authToken => {
    let reqObj = {
      url: `${API_ROUTES.getSmsDispatchList}`,
      method: 'POST',
      isAuth: true,
      authToken,
      showToggle: true,
    };
    fetchApi(reqObj)
      .then(data => {
        this.setState({ smsAlertList: data });
      })
      .catch(e => console.log(e));
  };

  handleSelectEvent = (e, id) => {
    this.setState({ selectedSmsId: id });
  };

  generateListXML = () => {
    let { smsAlertList } = this.state;
    let emailAlertsXML = [];

    return (emailAlertsXML = smsAlertList.map(t => {
      return (
        <TableListRow key={t.id}>
          <TableListCell value={t.date} />
          <TableListCell value={t.title} />
          <TableListCell value={t.country} />
          <TableListCell value={t.state} />
          <TableListCell value={t.metro} />
          <TableListCell value={t.status.id} />
          <TableListCell>
            <input
              name="sms-alerts"
              type="radio"
              onChange={e => this.handleSelectEvent(e, t.id)}
            />
          </TableListCell>
        </TableListRow>
      );
    }));
  };

  handleRadioInput = (value, type) => this.setState({ [type]: value });

  submitTracker = () => {
    let { cookies } = this.props;
    let { selectedSmsId } = this.state;
    let authToken = cookies.get('authToken');
    let reqData = {
      smsid: selectedSmsId,
    };
    let reqObj = {
      url: API_ROUTES.dispatchSms,
      method: 'POST',
      isAuth: true,
      authToken,
      data: reqData,
      showToggle: true,
    };
    fetchApi(reqObj)
      .then(data => {
        let toastObj = {
          toastMsg: data.message,
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      })
      .catch(e => console.log(e));
  };

  handleMultiSelect = value => {
    let { selectedClients } = this.state;
    // if(selectedClients.includes(value)){
    //     let index = selectedClients.indexOf(value);
    //     selectedClients.splice(index, 1);
    // }else{
    //     selectedClients = [...value];
    // }
    selectedClients = [...value];
    this.setState({ selectedClients });
  };

  render() {
    let { smsAlertList, selectedSmsId } = this.state;
    return (
      <div className="dispatchSms__wrap">
        <div className="product__header">
          <div className="product__title">
            <h3>Dispatch Sms Alerts</h3>
          </div>
          {selectedSmsId && (
            <div>
              <button onClick={() => this.submitTracker()}>
                Send Email Alert
              </button>
            </div>
          )}
        </div>
        {smsAlertList.length > 0 ? (
          <div className="product__body">
            <div>
              <div className="tlist__head">
                <div>
                  <label>Date</label>
                </div>
                <div>
                  <label>Subject</label>
                </div>
                <div>
                  <label>Country</label>
                </div>
                <div>
                  <label>State</label>
                </div>
                <div>
                  <label>City</label>
                </div>
                <div>
                  <label>Status</label>
                </div>
                <div>
                  <label>Select</label>
                </div>
              </div>
              <TableList>{this.generateListXML()}</TableList>
            </div>
          </div>
        ) : (
          <Empty title="No alerts available!" />
        )}
      </div>
    );
  }
}

export default DispatchSms;
