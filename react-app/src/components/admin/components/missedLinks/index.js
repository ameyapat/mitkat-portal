import React, { Component, Fragment } from 'react';
import { fetchApi } from '../../../../helpers/http/fetch';
import { withCookies } from 'react-cookie';
import './style.scss';
import { connect } from 'react-redux';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import injectSheet from 'react-jss';
import styles from './style';
import RimeFilteredCard from '../rimeFilteredCard';
import SearchBox from '../../../ui/searchBox';
import {
  rimeFilteredFeedSearch,
  sortByMissedLinks,
} from '../../../../helpers/utils';
import Loader from '../../../ui/loader';
import MonitoringScreenCard from '../monitoringScreenCard';
import MissedLinksCard from '../missedLinksCard';
import TableListRow from '../../../ui/tablelist/TableListRow';
import TableListCell from '../../../ui/tablelist/TableListCell';
import moment from 'moment';
import TableList from '../../../ui/tablelist';
import Empty from '../../../ui/empty';
import clsx from 'clsx';

@injectSheet(styles)
@withCookies
@connect(
  state => {
    return {
      rime: state.rime,
      themeChange: state.themeChange,
    };
  },
  dispatch => {
    return {
      dispatch,
    };
  },
)
class MissedLinks extends Component {
  state = {
    rimeFilteredFeed: [],
    finalFeed: [],
    selectedHour: '',
    searchValue: '',
    isFetching: false,
    hoursList: Array.from({ length: 999 }, (_, i) => i + 1),
  };

  componentDidMount() {
    this.getRimeFilteredFeed();
  }

  handleInputChange = e => {
    let value = e.target.value;
    this.setState(
      state => {
        return {
          searchValue: value,
        };
      },
      () => {
        this.handleSearch();
      },
    );
  };

  handleSearch = () => {
    let { searchValue, rimeFilteredFeed, finalFeed } = this.state;
    rimeFilteredFeed.forEach(item => {
      delete item['eventid'];
    });
    finalFeed.forEach(item => {
      delete item['eventid'];
    });
    if (searchValue) {
      let searchedList = rimeFilteredFeedSearch(
        this.state.searchValue,
        rimeFilteredFeed,
      );
      this.setState({ finalFeed: searchedList });
    } else {
      this.setState({ finalFeed: rimeFilteredFeed });
    }
  };

  refreshEditState = editedEvent => {
    this.getRimeFilteredFeed(this.state.selectedHour);
    let filteredFeed = this.state.finalFeed;
    filteredFeed.filter(item => item.id === editedEvent.id);
    this.setState({ finalFeed: [{ editedEvent }, ...filteredFeed] });
  };

  refreshState = id => {
    this.getRimeFilteredFeed(this.state.selectedHour);
    let filteredFeed = this.state.finalFeed;
    filteredFeed.filter(item => item.id === id);
    this.setState({ finalFeed: filteredFeed });
  };

  getRimeFilteredFeed = () => {
    let { rimeFilteredFeed, finalFeed } = this.state;
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let reqObj = {
      method: 'GET',
      isAuth: true,
      authToken,
      url: API_ROUTES.getMissedLinks,
    };

    fetchApi(reqObj)
      .then(data => {
        this.setState({
          rimeFilteredFeed: data,
          finalFeed: data,
          isFetching: false,
        });
      })
      .catch(e => console.log(e));
  };

  getTableItems = () => {
    let { finalFeed } = this.state;
    const { classes } = this.props;
    let itemXML = [];

    itemXML = finalFeed.map((item, index) => {
      return (
        <TableListRow key={index + 1}>
          <TableListCell value={item.id || '-'} />
          <TableListCell value={item.eventTitle || '-'} />
          <TableListCell
            value={item.sourceLink || '-'}
            classes={{ root: classes.sourceLink }}
          />
          <TableListCell
            value={moment(item.time).format('Do MMM YYYY, h:mm:ss a') || '-'}
          />
        </TableListRow>
      );
    });

    return itemXML;
  };

  renderTheadTitles = (value, meta) => {
    return (
      <span className="thead--titles--wrapper">
        <p>{value.title}</p>
        <span>
          <i className="fas fa-sort"></i>
        </span>
      </span>
    );
  };

  handleSort = (type, sort) => {
    let sortedStateTable = sortByMissedLinks(this.state.finalFeed, type, sort);
    this.setState({ tableData: sortedStateTable });
  };

  render() {
    const { classes } = this.props;
    const { searchValue, finalFeed } = this.state;

    return (
      <div id="rimeScreening">
        {this.state.isFetching && <Loader />}
        <div className="rimeFilteredDataBody">
          <div className="screeningEventList">
            {finalFeed.length > 0 ? (
              <Fragment>
                <div className={classes.tRowWrap}>
                  <TableListRow classes={{ root: classes.rootTRowWrap }}>
                    <TableListCell
                      type={'id'}
                      value={{
                        title: 'Id',
                      }}
                      renderProp={(value, meta) =>
                        this.renderTheadTitles(value, meta)
                      }
                      eventHandler={(type, sortBy) =>
                        this.handleSort(type, sortBy)
                      }
                      classes={{
                        root: clsx(classes.rootThead, 'msite-t-head'),
                      }}
                    />
                    <TableListCell
                      type={'title'}
                      value={{
                        title: 'Title',
                      }}
                      renderProp={(value, meta) =>
                        this.renderTheadTitles(value, meta)
                      }
                      eventHandler={(type, sortBy) =>
                        this.handleSort(type, sortBy)
                      }
                      classes={{
                        root: clsx(classes.rootThead, 'msite-t-head'),
                      }}
                    />
                    <TableListCell
                      type={'link'}
                      value={{
                        title: 'Link',
                      }}
                      renderProp={(value, meta) =>
                        this.renderTheadTitles(value, meta)
                      }
                      eventHandler={(type, sortBy) =>
                        this.handleSort(type, sortBy)
                      }
                      classes={{
                        root: clsx(classes.rootThead, 'msite-t-head'),
                      }}
                    />
                    <TableListCell
                      type={'date'}
                      value={{
                        title: 'Date',
                      }}
                      renderProp={(value, meta) =>
                        this.renderTheadTitles(value, meta)
                      }
                      eventHandler={(type, sortBy) =>
                        this.handleSort(type, sortBy)
                      }
                      classes={{
                        root: clsx(classes.rootThead, 'msite-t-head'),
                      }}
                    />
                  </TableListRow>
                </div>
                <TableList classes={{ root: 'darkBlue' }}>
                  {finalFeed.length > 0 && this.getTableItems()}
                </TableList>
              </Fragment>
            ) : (
              <Empty title="No events available" />
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default MissedLinks;
