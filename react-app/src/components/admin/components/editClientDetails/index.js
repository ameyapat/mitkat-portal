import React, { Component, Fragment } from 'react';
import { fetchApi } from '../../../../helpers/http/fetch';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import { withCookies } from 'react-cookie';
import Divider from '@material-ui/core/Divider';
import Select from 'react-select';
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';
import store from '../../../../store';
import { toggleToast } from '../../../../actions';
import { CLIENT_DETAILS } from '../../helpers/constants';
import {
  getArrFromObj,
  fetchCountries,
  fetchStates,
  fetchCities,
  fetchProducts,
  fetchIndustries,
  fetchClientTypes,
  toBool,
  toBoolean,
} from '../../../../helpers/utils';
import { getFirstView } from './utils';
import { getRiskLevels, getRiskCategories } from '../../../../api/api';
import {
  getRimeRegions,
  getUsersInAdmin,
} from '../../../../requestor/mitkatWeb/requestor';
//components @material-ui
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
//components
import ClientEmails from '../clientEmails';
import ClientLocations from './ClientLocations';
import SubscribeBox from './SubscribeBox';
import CustomClientDetails from '../customClientDetails';
import { getViewList } from '../../../../requestor/admin/requestor';

import './style.scss';

@withCookies
class EditClientDetails extends Component {
  state = {
    companyName: null,
    startDate: null,
    endDate: null,
    clientEmails: [],
    contactNumber: null,
    countries: [],
    states: [],
    cities: [],
    typeOfProducts: [],
    impactIndustries: [],
    uploadLogo: null,
    selectedStates: [],
    selectedCities: [],
    selectedCountries: [],
    selectedProduct: null,
    selectedIndustry: null,
    selectedClientType: '',
    // selectedEmails: []
    clientLocations: [],
    lat: '',
    lng: '',
    clientTitle: '',
    address: '',
    queryDetails: {},
    // adminUserDetails: [],
    clientTypes: [],
    //
    querySubscription: null,
    riskTracker: null,
    primarySPOC: '',
    secondarySPOC: '',
    selectedRiskCategories: [],
    selectedRiskLevels: [],
    selectedRimeRegions: [],
    selectedImpactRadius: false,
    selectedRime: false,
    selectedOtherRime: false,
    currForecasting: false,
    currCovidDashboard: false,
    currVaccination: false,
    currAnalytics: false,
    adminUserDetails: [],
    primaryContact: '',
    secondaryContact: '',
    clientData: null,
    firstViewId: 1,
    allViews: [],
  };

  componentDidMount() {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    getRiskCategories().then(riskCategories => {
      this.setState({ riskCategories });
    });
    getRiskLevels().then(riskLevels => {
      this.setState({ riskLevels });
    });
    getRimeRegions().then(rimeRegions => {
      this.setState({ rimeRegions });
    });
    fetchStates(authToken).then(states => this.setState({ states }));
    fetchCountries(authToken).then(countries => this.setState({ countries }));
    fetchCities(authToken).then(cities => this.setState({ cities }));
    fetchProducts(authToken).then(typeOfProducts =>
      this.setState({ typeOfProducts }),
    );
    getUsersInAdmin().then(data => {
      let redefinedArr = [];
      data.map(i => {
        let redefinedObj = {};
        redefinedObj['value'] = i.id;
        redefinedObj['label'] = i.username;
        redefinedArr.push(redefinedObj);
      });
      this.setState({ adminUserDetails: redefinedArr });
    });
    fetchIndustries(authToken).then(impactIndustries =>
      this.setState({ impactIndustries }),
    );
    fetchClientTypes(authToken).then(allClientTypes =>
      this.setState({ allClientTypes }),
    );
    this.getClientLocations();
    Promise.all([this.getUsersInAdmin(), this.getQueryList()]).then(values => {
      this.setState({ queryDetails: values[1] });
    });
    getViewList().then(allViews => {
      let allViewsArr = [];
      allViews.map(i => {
        let allViewsObj = {};
        allViewsObj['value'] = i.id;
        allViewsObj['label'] = i.viewName;
        allViewsArr.push(allViewsObj);
      });

      this.setState(
        {
          allViews: allViewsArr,
        },
        () => {
          this.fetchClientDetails(authToken);
        },
      );
    });
  }

  getQueryList = () => {
    return new Promise((resolve, reject) => {
      let { cookies, clientId } = this.props;
      let authTokenFromCookie = cookies.get('authToken');

      let params = {
        url: `${API_ROUTES.getDefaultQueryList}/${clientId}`,
        method: 'POST',
        isAuth: true,
        authToken: authTokenFromCookie,
        showToggle: true,
      };

      fetchApi(params)
        .then(data => {
          resolve(data);
        })
        .catch(e => reject(e));
    });
  };

  getUsersInAdmin = () => {
    return new Promise((resolve, reject) => {
      let { cookies } = this.props;
      let authTokenFromCookie = cookies.get('authToken');

      let params = {
        url: `${API_ROUTES.getUsersInAdmin}`,
        method: 'POST',
        isAuth: true,
        authToken: authTokenFromCookie,
        showToggle: true,
      };

      fetchApi(params)
        .then(data => {
          resolve(data);
        })
        .catch(e => reject(e));
    });
  };

  addClientLocation = () => {
    let { cookies, clientId } = this.props;
    let { lat, lng, clientTitle } = this.state;
    let authTokenFromCookie = cookies.get('authToken');

    let reqObj = {
      clientid: clientId,
      latitude: lat,
      longitude: lng,
      title: clientTitle,
    };

    let params = {
      url: `${API_ROUTES.addNewClientLocation}`,
      method: 'POST',
      isAuth: true,
      authToken: authTokenFromCookie,
      data: reqObj,
      showToggle: true,
    };

    fetchApi(params)
      .then(data => {
        if (data.success) {
          let toastObj = {
            toastMsg: data.message,
            showToast: true,
          };
          store.dispatch(toggleToast(toastObj));
          this.getClientLocations();
          this.setState({ address: '', clientTitle: '' });
        }
      })
      .catch(e => {
        let toastObj = {
          toastMsg: 'Something went wrong, please try again',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      });
  };

  getClientLocations = () => {
    let { cookies, clientId } = this.props;
    let authTokenFromCookie = cookies.get('authToken');

    let params = {
      url: `${API_ROUTES.viewClientLocation}/${clientId}`,
      method: 'POST',
      isAuth: true,
      authToken: authTokenFromCookie,
      showToggle: true,
    };

    fetchApi(params)
      .then(data => {
        this.setState({ clientLocations: data });
      })
      .catch(e => {
        console.log(e);
      });
  };

  redefineCountryObj = countryArr => {
    let newCountryArr = [];

    countryArr.map(i => {
      let countryObj = {};
      countryObj['value'] = i.id;
      countryObj['label'] = i.countryname;
      newCountryArr.push(countryObj);
    });

    return newCountryArr;
  };

  redefineRiskCatObj = selectedRiskCategories => {
    let selectedCategoriesArr = [];

    selectedRiskCategories.map(i => {
      let riskCatObj = {};
      riskCatObj['value'] = i.riskcategoryid;
      riskCatObj['label'] = i.riskcategory;
      selectedCategoriesArr.push(riskCatObj);
    });

    return selectedCategoriesArr;
  };

  redefineRiskLevelObj = selectedRiskLevels => {
    const selectedLevelsArr = [];

    selectedRiskLevels.map(i => {
      let riskLevelObj = {};
      riskLevelObj['value'] = i.risklevelid;
      riskLevelObj['label'] = i.level;
      selectedLevelsArr.push(riskLevelObj);
    });

    return selectedLevelsArr;
  };

  redefineRimeRegionObj = riskRimeRegionArr => {
    let newRiskRimeRegionArr = [];

    riskRimeRegionArr.map(i => {
      let rimeRegionObj = {};
      rimeRegionObj['value'] = i.id;
      rimeRegionObj['label'] = i.regionName;
      newRiskRimeRegionArr.push(rimeRegionObj);
    });

    return newRiskRimeRegionArr;
  };

  redefineStateObj = stateArr => {
    let newStateArr = [];

    stateArr.map(i => {
      let stateObj = {};
      stateObj['value'] = i.id;
      stateObj['label'] = i.statename;
      newStateArr.push(stateObj);
    });

    return newStateArr;
  };

  redefineCitiesObj = cityArr => {
    let newCityArr = [];

    cityArr.map(i => {
      let cityObj = {};
      cityObj['value'] = i.id;
      cityObj['label'] = i.cityname;
      newCityArr.push(cityObj);
    });

    return newCityArr;
  };

  redefineIndustryObj = arr => {
    let newArr = [];

    arr.map(i => {
      let newObj = {};
      newObj['value'] = i.id;
      newObj['label'] = i.industryname;
      newArr.push(newObj);
    });

    return newArr;
  };

  redefineProductObj = arr => {
    let newArr = [];
    arr.map(i => {
      let newObj = {};
      newObj['value'] = i.id;
      newObj['label'] = i.type;
      newArr.push(newObj);
    });

    return newArr;
  };

  redefineSelectedEmails = arr => {
    let newArr = [];
    arr.map(i => {
      newArr.push(i.email);
    });

    return newArr;
  };

  fetchClientDetails = authToken => {
    const { adminUserDetails } = this.state;
    let { clientId } = this.props;
    let reqObj = {
      url: `${API_ROUTES.adminGetClientDetails}/${clientId}`,
      method: 'POST',
      isAuth: true,
      authToken,
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        if (data) {
          this.setState({
            clientData: data,
            companyName: data.companyname,
            startDate: data.contractdate,
            endDate: data.contractenddate,
            contactNumber: data.contactnumber,
            contactPerson: data.contactperson,
            uploadLogo: data.logo,
            clientEmails: this.redefineSelectedEmails(data.clientemail),
            selectedStates: this.redefineStateObj(data.state),
            selectedCities: this.redefineCitiesObj(data.cities),
            selectedCountries: this.redefineCountryObj(data.countries),
            selectedProduct: this.redefineProductObj(data.producttype),
            selectedIndustry: this.redefineIndustryObj(data.impactindustry),
            selectedClientType: {
              label: data.clienttype.type,
              value: data.clienttype.id,
            },
            officeLocations: data.officelocations,
            querySubscription: toBoolean(data.querysubscription),
            riskTracker: toBoolean(data.riskTracker),
            firstViewId: getFirstView(data.firstViewId, this.state.allViews),
            selectedRiskCategories:
              this.redefineRiskCatObj(data.riskcategories) || [], //
            selectedRiskLevels:
              this.redefineRiskLevelObj(data.risklevels) || [], //
            selectedRimeRegions:
              this.redefineRimeRegionObj(data.rimeregions) || [], //
            selectedImpactRadius: toBoolean(data.impactradiusbased),
            selectedRime: toBoolean(data.rime),
            selectedOtherRime: toBoolean(data.nootherrimefeed),
            currForecasting: toBoolean(data.forecasting),
            currCovidDashboard: toBoolean(data.coviddashboard),
            currVaccination: toBoolean(data.vaccination),
            analytics: toBoolean(data.analytics),
            citybrief: toBoolean(data.citybrief),
          });
        }
      })
      .catch(e => console.log(e));
  };

  submitClientDetails = () => {
    let reqObj = {};
    let { cookies, clientId } = this.props;
    let {
      companyName,
      startDate,
      endDate,
      clientEmails,
      contactPerson,
      contactNumber,
      selectedCities,
      selectedCountries,
      selectedStates,
      uploadLogo,
      selectedIndustry,
      selectedProduct,
      selectedClientType,
      selectedImpactRadius,
      querySubscription,
      riskTracker,
      primarySPOC,
      secondarySPOC,
      selectedRiskCategories,
      selectedRiskLevels,
      selectedRimeRegions,
      selectedRime,
      selectedOtherRime,
      currForecasting,
      currCovidDashboard,
      currVaccination,
      analytics,
      citybrief,
      primaryContact,
      secondaryContact,
      firstViewId,
    } = this.state;

    let authTokenFromCookie = cookies.get('authToken');

    // let fd = new FormData();
    // fd.append('logo', uploadLogo, 'testname');

    reqObj = {
      clientid: clientId, //string
      companyname: companyName, //string
      contractdate: startDate, //"2018-06-17"
      contractenddate: endDate, //"2018-06-17"
      contactnumber: contactNumber, //string
      contactperson: contactPerson, //string
      // "logo": uploadLogo,
      logo: null,
      // "clientemails": getArrFromObj(clientemails), //array
      clientemails: clientEmails, //array
      officelocations: null,
      countries: getArrFromObj(selectedCountries), //array
      states: getArrFromObj(selectedStates), //array
      cities: getArrFromObj(selectedCities), //array
      producttypes: getArrFromObj(selectedProduct), //array
      impactindustry: getArrFromObj(selectedIndustry), //array
      clienttype: selectedClientType.value,
      querysubscription: toBool(querySubscription),
      riskTracker: toBool(riskTracker),
      primaryspoc: primaryContact.value,
      secondaryspoc: secondaryContact.value,
      riskcategories: getArrFromObj(selectedRiskCategories),
      risklevels: getArrFromObj(selectedRiskLevels),
      rimeregions: getArrFromObj(selectedRimeRegions),
      impactradius: toBool(selectedImpactRadius),
      rime: toBool(selectedRime),
      otherrime: toBool(selectedOtherRime),
      forecasting: toBool(currForecasting),
      coviddashboard: toBool(currCovidDashboard),
      vaccination: toBool(currVaccination),
      analytics: toBool(analytics),
      citybrief: toBool(citybrief),
      firstViewId: firstViewId.value,
    };

    let params = {
      url: API_ROUTES.updateClientDetails,
      method: 'POST',
      isAuth: true,
      hasMultiPart: true,
      authToken: authTokenFromCookie,
      data: reqObj,
    };

    fetchApi(params)
      .then(res => {
        let toastObj = {
          toastMsg: res.message,
          showToast: res.success,
        };
        this.props.handleCloseModal();
        store.dispatch(toggleToast(toastObj));
        // console.log(res);
      })
      .catch(e => {
        let toastObj = {
          toastMsg: 'Error updating client details.',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
        console.log(e);
      });
  };

  handleSelectChange = (selectedOption, type) => {
    this.setState({ [type]: selectedOption });
  };

  handleChange = (type, value) => {
    this.setState({ [type]: value }, () => {
      console.log(this.state, 'type', 'value');
    });
  };

  handleUploadFile = (type, e) => {
    let file = e.target.files[0];
    this.setState({ [type]: file });
  };

  handleDeleteEmail = id => {
    let { clientEmails } = this.state;
    clientEmails.splice(id, 1);
    let toastObj = {
      toastMsg: 'Email deleted',
      showToast: true,
    };
    store.dispatch(toggleToast(toastObj));
    this.setState({ clientEmails });
  };

  handleAddEmail = (value, type) => {
    let { clientEmails } = this.state;
    if (type === 'add') {
      if (clientEmails.includes(value)) {
        let toastObj = {
          toastMsg: 'Email already exists!',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
        return;
      } else {
        clientEmails.push(value);
      }
    }
    this.setState({ clientEmails });
  };

  handleLocDelete = clientId => {
    let { cookies } = this.props;
    let authTokenFromCookie = cookies.get('authToken');

    let params = {
      url: `${API_ROUTES.deleteClientLocation}/${clientId}`,
      method: 'POST',
      isAuth: true,
      authToken: authTokenFromCookie,
      showToggle: true,
    };

    fetchApi(params)
      .then(data => {
        if (data.success) {
          let toastObj = {
            toastMsg: data.message,
            showToast: true,
          };
          store.dispatch(toggleToast(toastObj));
          this.getClientLocations();
        }
      })
      .catch(e => {
        let toastObj = {
          toastMsg: 'Something went wrong, please try again',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
        console.log(e);
      });
  };

  handleAddressChange = address => {
    this.setState({ address });
  };
  /** function for setting location lat & lng */
  handleSelect = address => {
    geocodeByAddress(address)
      .then(results => getLatLng(results[0]))
      .then(latLng => {
        let { lat, lng } = this.state;
        lat = latLng.lat;
        lng = latLng.lng;
        this.setState({ lat, lng, address });
      })
      .catch(error => {
        console.error('Error', error);
        let toastObj = {
          showToast: true,
          toastObj: 'Error getting location, try again!',
        };
        store.dispatch(toggleToast(toastObj));
      });
  };

  handleSubscription = data => {
    let { cookies, clientId } = this.props;
    let authTokenFromCookie = cookies.get('authToken');

    let reqData = {
      subscription: data.isSubscribed,
      clientid: clientId,
      primaryspoc: data.primaryRes.value,
      secondaryspoc: data.secondaryRes.value,
      travelRiskSubscription: data.travelRiskSubscription ? 1 : 0,
    };

    let params = {
      url: `${API_ROUTES.handleUserQuery}`,
      method: 'POST',
      isAuth: true,
      authToken: authTokenFromCookie,
      data: reqData,
      showToggle: true,
    };

    fetchApi(params)
      .then(data => {
        if (data.success) {
          let toastObj = {
            toastMsg: data.message,
            showToast: true,
          };
          store.dispatch(toggleToast(toastObj));
        }
      })
      .catch(e => {
        let toastObj = {
          toastMsg: 'Something went wrong, please try again',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      });
  };

  render() {
    let {
      uploadLogo,
      clientLocations,
      clientTitle,
      queryDetails,
      adminUserDetails,
      primarySPOC,
      secondarySPOC,
    } = this.state;
    let fileName;

    if (uploadLogo) {
      fileName = uploadLogo.name;
    }

    return (
      <div className="clientDetailsWrap">
        <span
          className="close-edit-details"
          onClick={e => this.props.handleCloseModal()}
        >
          &times;
        </span>
        <h2>Enter Client Details</h2>
        <Divider style={{ marginBottom: '10px' }} />
        <div className="location-services-wrap">
          <div className="add-location">
            <div className="title-loc">
              <label>Client Locations</label>
              <input
                type="text"
                placeholder="Enter Location Name"
                value={clientTitle}
                onChange={e => this.handleChange('clientTitle', e.target.value)}
              />
            </div>
            <div className="google-places-loc">
              <div id="reactGooglePlaces" className="google--places">
                <label className="location--txt" style={{ color: '#666' }}>
                  <i className="icon-uniE9F1" style={{ color: '#666' }} /> Enter
                  Event Location
                </label>
                <PlacesAutocomplete
                  value={this.state.address}
                  onChange={this.handleAddressChange}
                  onSelect={this.handleSelect}
                  // searchOptions={searchOptions}
                >
                  {({
                    getInputProps,
                    suggestions,
                    getSuggestionItemProps,
                    loading,
                  }) => (
                    <div>
                      {
                        // validation.address.isInvalid && <span className="error--text">{validation.address.message}</span>
                      }
                      <input
                        {...getInputProps({
                          placeholder: 'Search Places ...',
                          className: 'location-search-input',
                        })}
                      />
                      {suggestions && (
                        <div className="autocomplete-dropdown-container">
                          {loading && (
                            <div className="loader-places">
                              <p>Loading...</p>
                            </div>
                          )}
                          {suggestions.map(suggestion => {
                            const className = suggestion.active
                              ? 'suggestion-item--active'
                              : 'suggestion-item';
                            // inline style for demonstration purpose
                            const style = suggestion.active
                              ? {
                                  backgroundColor: '#fafafa',
                                  cursor: 'pointer',
                                }
                              : {
                                  backgroundColor: '#ffffff',
                                  cursor: 'pointer',
                                };
                            return (
                              <div
                                {...getSuggestionItemProps(suggestion, {
                                  className,
                                  style,
                                })}
                              >
                                <span className="list--item">
                                  {suggestion.description}
                                </span>
                              </div>
                            );
                          })}
                        </div>
                      )}
                    </div>
                  )}
                </PlacesAutocomplete>
              </div>
            </div>
            <div className="location-submit">
              <button
                disabled={!this.state.address || !this.state.clientTitle}
                className="btn btn-add-loc"
                onClick={() => this.addClientLocation()}
              >
                Add Location
              </button>
            </div>
          </div>
          {clientLocations.length > 0 && (
            <div className="existing--location">
              <h3>Existing Client Locations</h3>
              <div className="location-table-body">
                {clientLocations.length > 0 && (
                  <ul>
                    {clientLocations.map(c => (
                      <ClientLocations
                        key={c.locationid}
                        id={c.locationid}
                        title={c.title}
                        handleDelete={this.handleLocDelete}
                      />
                    ))}
                  </ul>
                )}
              </div>
            </div>
          )}
        </div>
        {false && (
          <div className="subscribtionDetails">
            <h4>Subscribtion Details</h4>
            {adminUserDetails.length > 0 && (
              <SubscribeBox
                queryDetails={queryDetails}
                adminUserDetails={adminUserDetails}
                handleSubscription={this.handleSubscription}
              />
            )}
          </div>
        )}

        {adminUserDetails.length && (
          <CustomClientDetails
            clientDetails={CLIENT_DETAILS}
            clientEmails={this.state.clientEmails}
            handleSelectChange={this.handleSelectChange}
            handleAddEmail={this.handleAddEmail}
            handleDeleteEmail={this.handleDeleteEmail}
            handleChange={this.handleChange}
            attributes={this.state}
          />
        )}

        <div className="submit-btn">
          <Button
            className="cancel-edit-btn"
            variant="outlined"
            color="default"
            onClick={e => this.props.handleCloseModal()}
          >
            Cancel
          </Button>
          <Button
            variant="outlined"
            color="default"
            onClick={e => this.submitClientDetails()}
          >
            Submit
          </Button>
        </div>
      </div>
    );
  }
}

export default EditClientDetails;
