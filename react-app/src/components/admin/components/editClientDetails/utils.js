export function redefineObj(data) {
  let redefinedArr = [];
  data.map(i => {
    let redefinedObj = {};
    redefinedObj['value'] = i.id;
    redefinedObj['label'] = i.username;
    redefinedArr.push(redefinedObj);
  });
  return redefinedArr;
}

export function getFirstView(firstViewId, allViews) {
  return allViews.find(view => view.value === firstViewId);
}
