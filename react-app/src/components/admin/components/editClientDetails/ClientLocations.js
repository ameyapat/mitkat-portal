import React from 'react';
import './style.scss';

const ClientLocations = ({handleDelete, id, title}) => <li className="c-location-wrap"><p>{title}</p><button className="btn--delete" onClick={() => handleDelete(id)}><i className="icon-uniE949"></i></button></li>

export default ClientLocations