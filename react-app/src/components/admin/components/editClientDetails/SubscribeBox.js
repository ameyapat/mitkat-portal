import React, { Component } from 'react';
//helpers
import styles from './style';
import injectSheet from 'react-jss';
import Select from 'react-select';
import Switch from '@material-ui/core/Switch';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { redefineObj } from './utils';

class SubscribeBox extends Component {
  state = {
    primaryRes: {},
    secondaryRes: {},
    isSubscribed: '',
    travelRiskSubscription: '',
  };

  componentDidMount() {
    let {
      queryDetails: {
        subscription,
        primaryspoc,
        secondaryspoc,
        travelRiskSubscription,
      },
    } = this.props;
    let { primaryRes, secondaryRes } = this.state;

    if (primaryspoc) {
      primaryRes['value'] = primaryspoc.id;
      primaryRes['label'] = primaryspoc.username;
    }

    if (secondaryspoc) {
      secondaryRes['value'] = secondaryspoc.id;
      secondaryRes['label'] = secondaryspoc.username;
    }

    this.setState({
      isSubscribed: subscription,
      primaryRes,
      secondaryRes,
      travelRiskSubscription,
    });
  }

  handleSelectChange = (type, e) => this.setState({ [type]: e });

  handleInputchange = (type, value) =>
    this.setState({ [type]: value }, () =>
      console.log(this.state.travelRiskSubscription),
    );

  handleSubmit = () => this.props.handleSubscription(this.state);

  render() {
    let { classes, adminUserDetails, secondarySPOC, primarySPOC } = this.props;
    let {
      primaryRes,
      secondaryRes,
      isSubscribed,
      travelRiskSubscription,
    } = this.state;

    return (
      <div className={classes.rootSubscribeBox}>
        {false && (
          <div className="subscribe-options">
            <div className="s-option">
              <input
                type="radio"
                name="subscription"
                checked={isSubscribed == 1 ? true : false}
                value={1}
                onChange={e =>
                  this.handleInputchange('isSubscribed', e.target.value)
                }
              />
              <label>Yes</label>
            </div>
            <div className="s-option">
              <input
                type="radio"
                name="subscription"
                checked={isSubscribed != 1 ? true : false}
                value={0}
                onChange={e =>
                  this.handleInputchange('isSubscribed', e.target.value)
                }
              />
              <label>No</label>
            </div>
          </div>
        )}
        {false && (
          <>
            <div className="">
              <label>Primary Responder</label>
              <Select
                className="select--options"
                value={primarySPOC}
                placeholder="Primary Responder"
                onChange={e => this.props.handleSelectChange('primarySPOC', e)}
                options={adminUserDetails && redefineObj(adminUserDetails)}
              />
            </div>
            <div>
              <label>Secondary Responder</label>
              <Select
                className="select--options"
                value={secondarySPOC}
                placeholder="Secondary Responder"
                onChange={e =>
                  this.props.handleSelectChange('secondarySPOC', e)
                }
                options={adminUserDetails && redefineObj(adminUserDetails)}
              />
            </div>
          </>
        )}
        {false && (
          <>
            <div className={'rootTravelSubscription'}>
              <FormGroup row className={'formGroup'}>
                <h3>Is Travel Risk Advisory Subscribed?</h3>
                <FormControlLabel
                  control={
                    <Switch
                      checked={travelRiskSubscription}
                      onChange={e =>
                        this.handleInputchange(
                          'travelRiskSubscription',
                          e.target.checked,
                        )
                      }
                      value="travelRiskSubscription"
                      color="primary"
                      inputProps={{ 'aria-label': 'travel risk subscription' }}
                    />
                  }
                  label={travelRiskSubscription ? 'Yes' : 'No'}
                  classes={{
                    root: classes.controlLabel,
                  }}
                />
              </FormGroup>
            </div>
            <div className="subscribe--actions">
              <button className="btn btn-sub" onClick={this.handleSubmit}>
                Submit
              </button>
            </div>
          </>
        )}
      </div>
    );
  }
}

export default injectSheet(styles)(SubscribeBox);
