export default {
    rootSubscribeBox: {
        padding: '10px 0',
        borderTop: '1px solid #f1f1f1',
        // margin: '15px 0',
        borderBottom: '1px solid #f1f1f1',

        "& .subscribe-options":{
            display: 'flex',
            // justifyContent: 'space-between',
            padding: '0 0 15px',

            "& .s-option":{
                marginRight: 15,
                
                "& label":{
                    paddingRight: 10
                },
                "& input[type='radio']":{
                    marginRight: 10,
                    cursor: 'pointer'
                }
            }
        },
        "& .select--options":{
            margin: ' 0 0 15px 0'
        },
        "& label":{
            color: '#666',
            fontSize: 14,
            marginBottom: 10,
            display: 'block'
        }
    },
    controlLabel: {
        display: 'flex !important',
        margin: '0 !important',
    },
}