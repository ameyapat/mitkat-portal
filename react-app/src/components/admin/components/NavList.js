import React, { Component } from 'react';
//component
import NavlistItem from './NavlistItem';
//constant
import { NAV_LIST } from '../helpers/constants';

class NavList extends Component {

    state = {
        currActiveIndex: 0
    }

    handleClick = (key) => {
        this.setState({currActiveIndex: key})
    }

    getNavList = () => {

        let listUserXML = [];

        listUserXML = NAV_LIST.map((list, index) => {
            return <NavlistItem 
                    currActiveIndex={this.state.currActiveIndex}
                    handleClick={this.handleClick} 
                    key={index} 
                    index={index}
                    icon={list.icon} 
                    to={list.to} 
                    text={list.text} 
                />
        });

        return listUserXML;
    }

    render(){        

        return(
            <nav className="navlistWrap">
                <ul>
                    { this.getNavList() }                        
                </ul>
            </nav>
        )
    }
}

export default NavList;