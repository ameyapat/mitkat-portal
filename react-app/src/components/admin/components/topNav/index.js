import React, { Component } from 'react';
//styles
import './style.scss';
//components
import Search from '../search';
import { withRouter } from 'react-router-dom';
import { withCookies } from 'react-cookie';

@withRouter
@withCookies
class TopNav extends Component {
  handleLogout = () => {
    let { cookies } = this.props;
    cookies.remove('authToken');
    cookies.remove('userRole');
    setTimeout(() => {
      this.props.history.push('/');
    }, 2000);
  };

  render() {
    return (
      <div id="topNav">
        <div className="rightWrap">
          <div className="profileOptions">
            <span className="user__name">
              <span className="temp-logout" onClick={this.handleLogout}>
                logout
              </span>
              <i className="fas fa-lock"></i>
            </span>
          </div>
        </div>
      </div>
    );
  }
}

export default TopNav;
