import React, { Component } from 'react';
import { withCookies } from 'react-cookie';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import { fetchApi } from '../../../../helpers/http/fetch';
import { createValueLabel } from '../../../../helpers/utils';
import { Button } from '@material-ui/core';
import injectSheet from 'react-jss';
import style from './style';
import Select from 'react-select';
import { toggleToast } from '../../../../actions';
import store from '../../../../store';
import AlertDialog from '../../../ui/alertDialog';
import { saveAs } from 'file-saver';
import './style.scss';

const customStyles = {
  control: provided => ({
    ...provided,

    backgroundColor: 'var(--backgroundSolid)',
    color: '#ffffff',
  }),
  option: (provided, state) => ({
    ...provided,
    zIndex: '99999',
  }),
  placeholder: provided => ({
    ...provided,

    color: '#ffffff',
  }),
  singleValue: provided => ({
    ...provided,
    color: '#ffffff',
  }),
};
@withCookies
@injectSheet(style)
class DownloadQueries extends Component {
  state = {
    showAlert: false,
    clientList: [],
    selectedClients: [],
  };

  componentDidMount() {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    this.getClientList(authToken);
  }
  //to get dropdown list of clients
  getClientList = authToken => {
    let reqObj = {
      url: `${API_ROUTES.getClientList}`,
      method: 'POST',
      isAuth: true,
      authToken,
      showToggle: true,
    };
    fetchApi(reqObj)
      .then(data => {
        this.setState({ clientList: data });
      })
      .catch(e => console.log(e));
  };

  handleSingleSelect = (value, type) => {
    this.setState({ [type]: value });
  };

  handleShowAlert = () => {
    this.setState({ showAlert: true });
  };

  handleCloseAlert = () => {
    this.setState({ showAlert: false });
  };

  handleCloseModal = () => {
    this.setState({ showAlert: false });
    this.props.closeEventModal('downloadQueries');
  };

  handleDownload = e => {
    e.preventDefault();
    let { cookies } = this.props;
    let { selectedClients } = this.state;
    let authToken = cookies.get('authToken');
    const clientId = selectedClients.value;

    let reqObj = {
      url: `${API_ROUTES.downloadQuery}/${clientId}`,
      isAuth: true,
      authToken,
      method: 'POST',
      isBlob: true,
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(blobObj => {
        let objUrl = new Blob([blobObj], {
          type: 'application/vnd.ms-excel',
        });
        saveAs(objUrl, 'query-list.xlsx');

        let toastObj = {
          toastMsg: 'Downloading file...',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      })
      .catch(e => {
        let toastObj = {
          toastMsg: 'Error downloading excel sheet',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      });
  };

  render() {
    let { classes } = this.props;
    let { clientList, selectedClients, showAlert } = this.state;

    return (
      <div id="createQueries" className="createEvent__inner">
        <h3>Download queries</h3>
        <div className="queryModal">
          <Select
            defaultValue={selectedClients}
            className="select--options"
            value={selectedClients}
            placeholder={'Select Clients'}
            onChange={value =>
              this.handleSingleSelect(value, 'selectedClients')
            }
            options={createValueLabel(clientList, 'clients')}
            styles={customStyles}
          />
        </div>
        <div className="actionables">
          <Button
            onClick={e => this.handleShowAlert()}
            variant="contained"
            color="default"
          >
            Cancel
          </Button>
          <Button
            onClick={e => this.handleDownload(e)}
            style={{ marginLeft: 20, backgroundColor: '#2980b9' }}
            variant="contained"
            color="secondary"
          >
            Download Query
          </Button>
        </div>
        {showAlert && (
          <AlertDialog
            isOpen={showAlert}
            title="Cancel Changes"
            description="You are about to cancel event changes, you won't be able to get them back, are you sure?"
            parentEventHandler={e => this.handleCloseModal()}
            handleClose={() => this.handleCloseAlert()}
          />
        )}
      </div>
    );
  }
}

export default DownloadQueries;
