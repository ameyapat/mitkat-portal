import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import './style.scss';
// import CustomEditor from '../../../ui/customEditor';
import injectSheet from 'react-jss';
import style from './style';
@injectSheet(style)
class AddItem extends Component {
  componentDidUpdate(prevProps) {
    // console.log('crr props inside add item ', this.props.titleUpdate);
  }

  render() {
    let { titleUpdate, isTitleEditable, classes } = this.props;

    return (
      <div className="addItemWrap">
        <TextField
          error={this.props.hasError}
          id="updatesTitle"
          label="Title"
          multiline
          fullWidth
          rowsMax="10"
          value={this.props.titleUpdate}
          onChange={e => this.props.parentHandler(e, 'titleUpdate')}
          margin="normal"
          variant="outlined"
          InputLabelProps={{
            shrink: true,
            classes: {
              root: classes.rootLabel,
            },
          }}
          InputProps={{
            classes: {
              root: classes.rootText,
              input: classes.rootInput,
              notchedOutline: classes.notchedOutline,
            },
            endAdornment: null,
          }}
        />
        {/* <CustomEditor                                                             
          type={'titleUpdate'}                                                 
          editorState={titleUpdate}                                                
          onEditorStateChange={this.props.onEditorStateChange}
        />   */}
        <button
          disabled={titleUpdate.length === 0}
          className="btn--addTitle"
          onClick={() => this.props.handleSubmit()}
        >
          {isTitleEditable ? 'Update Title' : 'Add New Title'}
        </button>
      </div>
    );
  }
}

export default AddItem;
