const validationRules = [
    {
        field: 'titleUpdate',
        method: 'isEmpty',
        validWhen: false,
        message: 'Title is required'
    }
]

export default validationRules;