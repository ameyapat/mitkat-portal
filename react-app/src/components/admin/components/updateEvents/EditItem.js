import React, { Component } from 'react';
// import CustomEditor from '../../../ui/customEditor';
import TextField from '@material-ui/core/TextField';
//style
import './style.scss';
import injectSheet from 'react-jss';
import style from './style';
@injectSheet(style)
class EditItem extends Component {
  render() {
    let { titleUpdate, classes } = this.props;

    return (
      <div className="addItemWrap">
        <TextField
          error={this.props.hasError}
          id="updatesTitle"
          label="Title"
          multiline
          fullWidth
          rowsMax="10"
          value={this.props.titleUpdate}
          onChange={e => this.props.parentHandler(e, 'titleUpdate')}
          margin="normal"
          variant="outlined"
          InputLabelProps={{
            shrink: true,
            classes: {
              root: classes.rootLabel,
            },
          }}
          InputProps={{
            classes: {
              root: classes.rootText,
              input: classes.rootInput,
              notchedOutline: classes.notchedOutline,
            },
            endAdornment: null,
          }}
        />
        {/* <CustomEditor                                                             
          type={'titleUpdate'}                                                 
          editorState={titleUpdate}                                                
          onEditorStateChange={this.props.onEditorStateChange}
        />   */}
        <button
          className="btn--addTitle"
          onClick={() => this.props.handleSubmit()}
        >
          Update Title
        </button>
      </div>
    );
  }
}

export default EditItem;
