import React, { Component } from 'react';
import './style.scss';

class UpdateItems extends Component {
  render() {

    let { id, country, state, city, creator, title, storyType, date, parentHandler } = this.props;

    return (
      <div className="updateItems">
        <h4>{title}</h4>
        <label className="">{creator}</label>
        <div className="meta">
            <span className="pills">{country}</span>
            <span className="pills">{state}</span>
            <span className="pills">{city}</span>
        </div>
        <button className="add--update--btn" onClick={() => parentHandler(id)}>Add New Update</button>
      </div>
    )
  }
}

export default UpdateItems;