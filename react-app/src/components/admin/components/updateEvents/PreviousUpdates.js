import React, { Component } from 'react';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import { fetchApi } from '../../../../helpers/http/fetch';
import { withCookies } from 'react-cookie';
//components
import UpdatesList from './UpdatesList';
import store from '../../../../store';
import { toggleToast } from '../../../../actions';

@withCookies
class PreviousUpdates extends Component {
  state = {
    listOfUpdates: [],
  };

  UNSAFE_componentWillMount() {
    this.getPreviousUpdates();
  }

  getPreviousUpdates = () => {
    let { id, cookies } = this.props;
    let authToken = cookies.get('authToken');

    let reqObj = {
      method: 'POST',
      isAuth: true,
      authToken,
      url: `${API_ROUTES.getLiveEventsUpdatesList}/${id}`,
      showToggle: true,
    };

    fetchApi(reqObj).then(data => {
      this.setState({ listOfUpdates: data });
    });
  };

  handleDelete = id => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');

    let reqObj = {
      method: 'POST',
      isAuth: true,
      authToken,
      url: `${API_ROUTES.deleteEventUpdate}/${id}`,
      showToggle: true,
    };

    fetchApi(reqObj).then(data => {
      console.log(data);

      let toastObj = {
        toastMsg: data.message,
        showToast: data.success,
      };
      this.props.handleCloseModal('showAddModal');
      store.dispatch(toggleToast(toastObj));
    });
  };

  render() {
    let { listOfUpdates } = this.state;

    return (
      <div>
        {listOfUpdates.length > 0 ? (
          <UpdatesList
            listOfUpdates={listOfUpdates}
            handleDelete={this.handleDelete}
            getPreviousUpdates={this.getPreviousUpdates}
            isTitleEditable={this.props.isTitleEditable}
          />
        ) : (
          <p className="no--updates">No updates available!</p>
        )}
      </div>
    );
  }
}

export default PreviousUpdates;
