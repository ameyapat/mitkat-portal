import React, { Component } from 'react';
import { withCookies } from 'react-cookie';
import { fetchApi } from '../../../../helpers/http/fetch';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
//components
import UpdateItems from './updateItems';
import CustomModal from '../../../ui/modal';

import './style.scss';
import AddItem from './AddItem';
import EditItem from './EditItem';
import PreviousUpdates from './PreviousUpdates';
import store from '../../../../store';
import { toggleToast, updateEventTitle } from '../../../../actions';
import validationRules from './validationRules';
import FormValidator from '../../../../helpers/validation/FormValidator';
import draftToHtml from 'draftjs-to-html';
// import htmlToDraft from 'html-to-draftjs';
import { convertToRaw } from 'draft-js';
import { connect } from 'react-redux';
import clsx from 'clsx';
import injectSheet from 'react-jss';

const styles = {
  rootAddEventModal: {
    minWidth: 1024,
  },
};

@injectSheet(styles)
@withCookies
@connect(state => {
  return {
    updateEvent: state.updateEvent,
  };
})
class UpdateEvents extends Component {
  validator = new FormValidator(validationRules);
  submitted = false;

  state = {
    liveEvents: [],
    showAddModal: false,
    titleUpdate: '',
    currUpdateId: '',
    validation: this.validator.valid(),
  };

  componentDidMount() {
    this.getLiveEvents();

    console.log(this.props);
  }

  componentDidUpdate(prevProps) {
    let currTitle = this.props.updateEvent.title;
    let prevTitle = prevProps.updateEvent.title;

    if (prevTitle !== currTitle)
      this.setState({ titleUpdate: currTitle }, () => console.log(this.state));
  }

  getLiveEvents = () => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');

    let reqObj = {
      method: 'POST',
      isAuth: true,
      authToken,
      url: API_ROUTES.getLiveEvents,
      showToggle: true,
    };

    fetchApi(reqObj).then(data => {
      this.setState(state => ({ liveEvents: data }));
    });
  };

  getEventsXML = () => {
    let { liveEvents } = this.state;
    let liveEventsXML = [];

    return (liveEventsXML = liveEvents.map(item => (
      <UpdateItems
        key={item.id}
        id={item.id}
        country={item.country}
        state={item.state}
        city={item.city}
        creator={item.creator}
        title={item.title}
        storyType={item.storytype}
        date={item.validitydate}
        parentHandler={this.handleUpdateItem}
      />
    )));
  };

  handleUpdateItem = id => {
    this.setState({ showAddModal: true, currUpdateId: id });
  };

  handleCloseModal = type => {
    this.setState({ [type]: false });
    store.dispatch(
      updateEventTitle({ title: '', isTitleEditable: false, id: '' }),
    );
  };

  handleInputChange = (e, type) => {
    this.setState({ [type]: e.target.value });
  };

  handleSubmit = () => {
    let { cookies } = this.props;
    let { titleUpdate, currUpdateId } = this.state;
    let authToken = cookies.get('authToken');

    let validation = this.validator.validate(this.state);
    this.setState({ validation });
    this.submitted = true;

    if (validation.isValid) {
      let dataObj = {
        eventid: currUpdateId,
        title: titleUpdate,
      };

      let reqObj = {
        isAuth: true,
        method: 'POST',
        authToken,
        data: dataObj,
        url: API_ROUTES.addEventUpdate,
        showToggle: true,
      };

      fetchApi(reqObj).then(data => {
        if (data && data.success) {
          let toastObj = {
            toastMsg: data.message,
            showToast: data.success,
          };
          this.setState({ showAddModal: false, titleUpdate: '' });
          store.dispatch(toggleToast(toastObj));
        }
      });
    }
  };

  onEditorStateChange = (value, type) => {
    let editorState = draftToHtml(convertToRaw(value.getCurrentContent()));
    let clearState = editorState.replace(/\&nbsp;/g, '');
    this.setState({ [type]: clearState });
  };

  handleOnSave = () => {
    let {
      cookies,
      updateEvent: { id },
    } = this.props;
    let { titleUpdate } = this.state;
    let authToken = cookies.get('authToken');
    let reqData = {
      updateid: id,
      title: titleUpdate,
    };

    let reqObj = {
      method: 'POST',
      isAuth: true,
      authToken,
      url: API_ROUTES.editEventUpdate,
      data: reqData,
      showToggle: true,
    };

    fetchApi(reqObj).then(data => {
      if (data && data.success) {
        let toastObj = {
          toastMsg: data.message,
          showToast: data.success,
        };
        this.setState({ isEditable: false, showAddModal: false });
        store.dispatch(toggleToast(toastObj));
        store.dispatch(
          updateEventTitle({ title: '', isTitleEditable: false, id: '' }),
        );
        // this.props.getPreviousUpdates();
      }
    });
  };

  handleCloseNewUpdate = () => {
    this.setState({ showAddModal: false, titleUpdate: '' });
  };

  render() {
    let {
      updateEvent: { isTitleEditable, title },
      classes,
    } = this.props;
    let { liveEvents, showAddModal, currUpdateId, titleUpdate } = this.state;
    let validation = this.submitted
      ? this.validator.validate(this.state)
      : this.state.validation;

    return (
      <div className="updateEventsWrap">
        <div className="update__events__inner">
          <div className="live__events">
            <h3>All Live Events</h3>
            <div className="items__wrap">
              {liveEvents.length > 0 ? (
                this.getEventsXML()
              ) : (
                <p className="no--updates--txt">No updates available!</p>
              )}
            </div>
          </div>
        </div>
        <CustomModal
          closeModal={() => this.handleCloseModal('showAddModal')}
          showModal={showAddModal}
        >
          <div className={clsx('modal__inner', classes.rootAddEventModal)}>
            <div className="outerAddUpdate">
              <h3 className="outerAddUpdate__header">
                Add new update
                <span
                  onClick={this.handleCloseNewUpdate}
                  className="outerAddUpdate_close"
                >
                  &times;
                </span>
              </h3>
              {isTitleEditable && titleUpdate ? (
                <EditItem
                  titleUpdate={this.state.titleUpdate}
                  parentHandler={this.handleInputChange}
                  handleSubmit={this.handleOnSave}
                  onEditorStateChange={this.onEditorStateChange}
                />
              ) : (
                <AddItem
                  hasError={validation.titleUpdate.isInvalid}
                  titleUpdate={this.state.titleUpdate}
                  parentHandler={this.handleInputChange}
                  handleSubmit={this.handleSubmit}
                  onEditorStateChange={this.onEditorStateChange}
                />
              )}
            </div>
            <div className="outerListUpdate">
              <h3>List of previous updates</h3>
              <PreviousUpdates
                id={currUpdateId}
                handleCloseModal={this.handleCloseModal}
                isTitleEditable={isTitleEditable}
              />
            </div>
            <div className="close--update">
              <button
                className="btn btn--close"
                onClick={() => this.handleCloseModal('showAddModal')}
              >
                Close
              </button>
            </div>
          </div>
        </CustomModal>
      </div>
    );
  }
}

export default UpdateEvents;
