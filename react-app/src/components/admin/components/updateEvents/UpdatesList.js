import React, { Component } from 'react';
import UpdateListItem from './UpdateListItem';

import injectSheet from 'react-jss';

const styles = {
  rootUpdateList: {
    maxHeight: 150,
    overflowY: 'scroll',
    padding: '10px'
  }
}

@injectSheet(styles)
class UpdatesList extends Component {

    getListXML = () => {
       let { listOfUpdates } = this.props; 
       let updatesXML = [];

       return updatesXML = listOfUpdates.map(item => <UpdateListItem 
          key={item.id}
          id={item.id}
          title={item.title}
          description={item.description}
          timeUpdate={item.timeupdate}
          handleDelete={this.props.handleDelete}
          getPreviousUpdates={this.props.getPreviousUpdates}
          isTitleEditable={this.props.isTitleEditable}
       />)
    }

  render() {
    let { classes } = this.props;
    return (
      <div className={classes.rootUpdateList}>
        { this.props.listOfUpdates && this.getListXML()}
      </div>
    )
  }
}

export default UpdatesList;
