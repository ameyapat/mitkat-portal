import React, { Component } from 'react'
import store from '../../../../store';
import { toggleToast } from '../../../../actions';
import { fetchApi } from '../../../../helpers/http/fetch';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import { withCookies } from 'react-cookie';
import { updateEventTitle } from '../../../../actions';

@withCookies
class UpdateListItem extends Component {

  state = {
      editedUpdateTitle: "",
      isEditable: false
  }

  componentDidMount(){
      this.setState({editedUpdateTitle: this.props.title});
  }

  handleEdit = () => {      
    let { id } = this.props;

    this.setState({isEditable: true});
    store.dispatch(updateEventTitle({ title: this.props.title, isTitleEditable: true, id }));
  }

  handleEditChange = (e) => {
      this.setState({editedUpdateTitle: e.target.value});
  }

//   handleOnSave = () => {

//         let { cookies } = this.props;
//         let authToken = cookies.get("authToken");

//         let reqData = {
//             "updateid": this.props.id,
//             "title": this.state.editedUpdateTitle
//         }

//         let reqObj = {
//             method: 'POST',
//             isAuth: true,
//             authToken,
//             url: API_ROUTES.editEventUpdate,
//             data: reqData
//         }

//         fetchApi(reqObj)
//         .then(data => {
//             if(data && data.success){
//                 let toastObj = {
//                     toastMsg: data.message,
//                     showToast: data.success
//                 }
//                 this.setState({isEditable: false});
//                 store.dispatch(toggleToast(toastObj));
//                 this.props.getPreviousUpdates();
//             }
//         })     
//   }

  render() {
      let { title, id, description, timeUpdate, handleDelete,
         handleEdit } = this.props;
      let { isEditable, editedUpdateTitle } = this.state;      
    
      return (
      <div className="update__listItem">
        <div className="update__title">
            {
                // isEditable
                // ?
                // <input 
                //     type="input" 
                //     value={editedUpdateTitle} 
                //     onChange={this.handleEditChange} 
                // />
                // :
                // <p dangerouslySetInnerHTML={{__html: title}}></p>
            }
            
                <p dangerouslySetInnerHTML={{__html: title}}></p>
            
        </div>
        {/* <div className="update__description"><p>{description}</p></div> */}
        <div className="update__date"><p>{timeUpdate}</p></div>
        <div className="update__edit">
            {/* <button onClick={isEditable ? this.handleOnSave : this.handleEdit} className="btn btn--edit">
             <i className="icon-uniE93E"></i> 
             {
                 isEditable ? "Save" : "Edit" 
             }
            </button> */}
            {
                // !isEditable &&
                !this.props.isTitleEditable &&
                <button onClick={this.handleEdit} className="btn btn--edit">
                <i className="icon-uniE93E"></i> 
                    Edit 
                </button>
            }
        </div>
        <div className="update__delete">
            <button onClick={() => handleDelete(id)} className="btn btn--delete">
                <i className="icon-uniE949"></i> Delete
            </button>
        </div>
      </div>
    )
  }
}

export default UpdateListItem;
