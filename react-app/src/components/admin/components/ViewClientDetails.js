import React, { Component, Fragment } from 'react';
//components @material-ui
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import RadioGroup from '@material-ui/core/RadioGroup';
import Radio from '@material-ui/core/Radio';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import ClientEmails from './clientEmails';
//constants'
import { CLIENT_DETAILS } from '../helpers/constants';
import Select from 'react-select';
//sass
import '../sass/viewUpdateClient.scss';
//helpers
import { fetchApi } from '../../../helpers/http/fetch';
import {
  getArrFromObj,
  fetchCountries,
  fetchStates,
  fetchCities,
  fetchProducts,
  fetchIndustries,
  fetchClientTypes,
  toBool,
} from '../../../helpers/utils';
import { API_ROUTES } from '../../../helpers/http/apiRoutes';
//redux
import { connect } from 'react-redux';
import { withCookies } from 'react-cookie';
import store from '../../../store';
import { toggleToast } from '../../../actions';
import FormValidator from '../../../helpers/validation/FormValidator';
import { clientValidationRules } from './validationRules';
import { getRiskLevels, getRiskCategories } from '../../../api/api';
import {
  getRimeRegions,
  getUsersInAdmin,
} from '../../../requestor/mitkatWeb/requestor';
import { Divider } from '@material-ui/core';
import CustomLabel from '../../ui/customLabel';
import { getViewList } from '../../../requestor/admin/requestor';
@withCookies
@connect(state => {
  return {
    appState: state.appState,
  };
})
class ViewClientDetails extends Component {
  validator = new FormValidator(clientValidationRules);
  submitted = false;

  state = {
    companyName: '',
    startDate: '',
    endDate: '',
    clientEmails: [],
    contactNumber: '',
    contactPerson: '',
    countries: [],
    states: [],
    cities: [],
    typeOfProducts: [],
    impactIndustries: [],
    uploadLogo: '',
    clientLocations: [],
    clientTypes: [],
    allClientTypes: [],
    riskCategories: [],
    riskLevels: [],
    rimeRegions: [],
    validation: this.validator.valid(),
    selectedStates: [],
    selectedCities: [],
    selectedCountries: [],
    selectedProduct: '',
    selectedIndustry: '',
    selectedClientType: '',
    primarySPOC: '',
    secondarySPOC: '',
    selectedRiskCategories: [],
    selectedRiskLevels: [],
    selectedRimeRegions: [],
    querySubscription: false,
    riskTracker: false,
    selectedImpactRadius: false,
    selectedRime: false,
    selectedOtherRime: false,
    currForecasting: false,
    currCovidDashboard: false,
    currVaccination: false,
    currAnalytics: false,
    adminUserDetails: [],
    primaryContact: '',
    secondaryContact: '',
    firstViewId: {},
    allViews: [],
  };

  componentDidMount() {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');

    fetchCountries(authToken).then(countries => {
      this.setState({ countries });
    });

    fetchStates(authToken).then(states => {
      this.setState({ states });
    });

    fetchCities(authToken).then(cities => {
      this.setState({ cities });
    });

    getUsersInAdmin().then(data => {
      let redefinedArr = [];
      data.map(i => {
        let redefinedObj = {};
        redefinedObj['value'] = i.id;
        redefinedObj['label'] = i.username;
        redefinedArr.push(redefinedObj);
      });
      this.setState({ adminUserDetails: redefinedArr });
    });

    fetchProducts(authToken).then(typeOfProducts => {
      this.setState({ typeOfProducts });
    });

    fetchIndustries(authToken).then(impactIndustries => {
      this.setState({ impactIndustries });
    });

    fetchClientTypes(authToken).then(allClientTypes =>
      this.setState({ allClientTypes }),
    );

    getRiskCategories().then(riskCategories => {
      this.setState({ riskCategories });
    });

    getRiskLevels().then(riskLevels => {
      this.setState({ riskLevels });
    });

    getRimeRegions().then(rimeRegions => {
      this.setState({ rimeRegions });
    });

    getViewList().then(allViews => {
      let allViewsArr = [];
      allViews.map(i => {
        let allViewsObj = {};
        allViewsObj['value'] = i.id;
        allViewsObj['label'] = i.viewName;
        allViewsArr.push(allViewsObj);
      });

      this.setState({
        allViews: allViewsArr,
        firstViewId: { value: allViews[0].id, label: allViews[0].viewName },
      });
    });
  }

  handleChange = (type, value) => {
    this.setState({ [type]: value });
  };

  handleEmailChange = (type, value) => {
    let clientEmailsArr = [];
    clientEmailsArr.push(value);

    this.setState({ [type]: clientEmailsArr });
  };

  handleUploadFile = (type, e) => {
    let file = e.target.files[0];
    this.setState({ [type]: file });
  };

  submitClientDetails = () => {
    let reqObj = {};
    let { cookies, clientId } = this.props;
    let {
      companyName,
      startDate,
      endDate,
      clientEmails,
      contactPerson,
      contactNumber,
      selectedCities,
      selectedCountries,
      selectedStates,
      uploadLogo,
      selectedIndustry,
      selectedProduct,
      travelRiskSubscription,
      selectedClientType,
      //
      selectedImpactRadius,
      querySubscription,
      riskTracker,
      primarySPOC,
      secondarySPOC,
      selectedRiskCategories,
      selectedRiskLevels,
      selectedRimeRegions,
      selectedRime,
      selectedOtherRime,
      currForecasting,
      currCovidDashboard,
      currVaccination,
      currAnalytics,
      primaryContact,
      secondaryContact,
      firstViewId,
    } = this.state;

    let authTokenFromCookie = cookies.get('authToken');
    let validation = this.validator.validate(this.state);
    this.submitted = true;
    this.setState({ validation });

    if (validation.isValid) {
      reqObj = {
        clientid: clientId, //string
        companyname: companyName, //string
        contractdate: startDate, //"2018-06-17"
        contractenddate: endDate, //"2018-06-17"
        contactnumber: contactNumber, //string
        contactperson: contactPerson, //string
        logo: null,
        clientemails: clientEmails, //array
        officelocations: null,
        countries: getArrFromObj(selectedCountries), //array
        states: getArrFromObj(selectedStates), //array
        cities: getArrFromObj(selectedCities), //array
        producttypes: getArrFromObj(selectedProduct), //array
        impactindustry: getArrFromObj(selectedIndustry), //array
        clienttype: selectedClientType.value,
        // @extra fields
        querysubscription: toBool(querySubscription),
        riskTracker: toBool(riskTracker),
        primaryspoc: primaryContact.value,
        secondaryspoc: secondaryContact.value,
        riskcategories: getArrFromObj(selectedRiskCategories),
        risklevels: getArrFromObj(selectedRiskLevels),
        rimeregions: getArrFromObj(selectedRimeRegions),
        impactradius: toBool(selectedImpactRadius),
        rime: toBool(selectedRime),
        otherrime: toBool(selectedOtherRime),
        forecasting: toBool(currForecasting),
        coviddashboard: toBool(currCovidDashboard),
        vaccination: toBool(currVaccination),
        analytics: toBool(currAnalytics),
        firstViewId: firstViewId.value,
      };

      let params = {
        url: API_ROUTES.adminUpdateClientDetails,
        method: 'POST',
        isAuth: true,
        authToken: authTokenFromCookie,
        data: reqObj,
        showToggle: true,
      };

      fetchApi(params)
        .then(res => {
          let toastObj = {
            toastMsg: res.message,
            showToast: res.success,
          };
          this.props.handleCloseModal();
          store.dispatch(toggleToast(toastObj));
        })
        .catch(e => {
          let toastObj = {
            toastMsg: 'Error while creating client, please try again.',
            showToast: true,
          };
          store.dispatch(toggleToast(toastObj));
          console.log(e);
        });
    }
  };

  handleSelectChange = (selectedOption, type) => {
    this.setState({ [type]: selectedOption });
  };

  handleDeleteEmail = id => {
    let { clientEmails } = this.state;
    clientEmails.splice(id, 1);
    let toastObj = {
      toastMsg: 'Email deleted',
      showToast: true,
    };
    store.dispatch(toggleToast(toastObj));
    this.setState({ clientEmails });
  };

  handleAddEmail = (value, type) => {
    let { clientEmails } = this.state;
    if (type === 'add') {
      if (clientEmails.includes(value)) {
        let toastObj = {
          toastMsg: 'Email already exists!',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
        return;
      } else {
        clientEmails.push(value);
      }
    }
    this.setState({ clientEmails });
  };

  renderInputFields = mapOnKey => {
    // @sr: will take care of validation later
    let validation = this.submitted
      ? this.validator.validate(this.state)
      : this.state.validation;
    const { classes } = this.props;

    return mapOnKey.map(input => {
      switch (input.type) {
        case 'text':
          return (
            <Fragment key={input.id}>
              <CustomLabel
                title={input.label}
                classes="select--label"
                isRequired={input?.isRequired}
              />
              <input
                className={`input--${input.id}`}
                type={input.type}
                placeholder={input?.placeholder}
                onChange={e => this.handleChange(input.id, e.target.value)}
              />
              {/* {input.id === 'companyName' && validation[input.id].isInvalid && (
                <span className="errorTxt">{validation[input.id].message}</span>
              )} */}
            </Fragment>
          );
        case 'number':
          return (
            <Fragment key={input.id}>
              <TextField
                id={input.id}
                label={input.label}
                type={input.type}
                InputLabelProps={{
                  shrink: true,
                }}
                variant="outlined"
                onChange={e =>
                  this.handleChange(input.selectedText, e.target.value)
                }
                className={`input--${input.id}`}
              />
              {/* {input.id === 'companyName' && validation[input.id].isInvalid && (
                <span className="errorTxt">{validation[input.id].message}</span>
              )} */}
            </Fragment>
          );
        case 'date':
          return (
            <Fragment>
              <TextField
                key={input.id}
                id={input.id}
                label={input.label}
                type="date"
                fullWidth
                onChange={e => this.handleChange(input.id, e.target.value)}
                InputLabelProps={{
                  shrink: true,
                }}
                margin="normal"
              />
              {/* {validation[input.id].isInvalid && <span className="errorTxt">{validation[input.id].message}</span>} */}
            </Fragment>
          );
        case 'email':
          return (
            <Fragment>
              <ClientEmails
                label={input.label}
                handleAddEmail={this.handleAddEmail}
                handleDeleteEmail={this.handleDeleteEmail}
                clientEmailList={this.state.clientEmails}
              />
              {/* {validation[input.id].isInvalid && <span className="errorTxt">{validation[input.id].message}</span>} */}
            </Fragment>
          );
        case 'select':
          if (input.isMultiSelect) {
            return (
              <div key={input.id} className="selectWrapper">
                <CustomLabel
                  title={input.label}
                  classes="select--label"
                  isRequired={input?.isRequired}
                />
                <Select
                  isMulti
                  className="select--options"
                  value={this.state[input.selectedText]}
                  placeholder={input.label}
                  onChange={e => this.handleSelectChange(e, input.selectedText)}
                  options={this.state[input.id] || []}
                />
                {/* {(input.selectedText == 'selectedProduct' ||
                  input.selectedText == 'selectedIndustry') &&
                  validation[input.selectedText].isInvalid && (
                    <span className="errorTxt">
                      {validation[input.selectedText].message}
                    </span>
                  )} */}
                {/* {((input.selectedText == 'selectedProduct') || (input.selectedText == 'selectedIndustry')) && <span className="errorTxt">{input.selectedText}</span>} */}
              </div>
            );
          } else {
            return (
              <div key={input.id} className="selectWrapper">
                <CustomLabel
                  title={input.label}
                  classes="select--label"
                  isRequired={input?.isRequired}
                />
                <Select
                  className="select--options"
                  value={this.state[input.id]}
                  placeholder={input.label}
                  onChange={e => this.handleSelectChange(e, input.id)}
                  options={this.state[input.selectedText] || []}
                />
                {/* { validation[input.selectedText].isInvalid && <span className="errorTxt">{validation[input.selectedText].message}</span>} */}
              </div>
            );
          }
        case 'radio':
          return (
            <FormControl component="fieldset">
              <CustomLabel
                title={input.label}
                classes="select--label"
                isRequired={input?.isRequired}
              />
              <RadioGroup
                aria-label={input.label}
                name={input.id}
                value={this.state[input.selectedText] === 'true' ? true : false}
                onChange={e =>
                  this.handleChange(input.selectedText, e.target.value)
                }
                className="radioWrapper"
              >
                <FormControlLabel
                  value={true}
                  control={<Radio />}
                  label="Yes"
                />
                <FormControlLabel
                  value={false}
                  control={<Radio />}
                  label="No"
                />
              </RadioGroup>
            </FormControl>
          );
        default:
          return null;
      }
    });
  };

  render() {
    const {
      companyDetails,
      clientMapping,
      accessControl,
      others,
      accessControlNested: { rime, querySystem },
    } = CLIENT_DETAILS;

    const { adminUserDetails, queryDetails, handleSubscription } = this.state;

    return (
      <div className="clientDetailsWrap">
        <h2>Company Details</h2>
        <div className="textInputs">
          {this.renderInputFields(companyDetails)}
        </div>
        <Divider className="divider-pd-15" />
        <h2>Client Mapping</h2>
        <div className="textInputs">
          {this.renderInputFields(clientMapping)}
        </div>
        <Divider className="divider-pd-15" />
        <h2>Access Control</h2>
        <div className="textInputs">
          {this.renderInputFields(querySystem)}
          {this.renderInputFields(rime)}
          {this.renderInputFields(accessControl)}
        </div>
        <Divider className="divider-pd-15" />
        <h2>Others</h2>
        <div className="textInputs">{this.renderInputFields(others)}</div>
        <Divider className="divider-pd-15" />
        <div className="submit-btn">
          <Button
            variant="outlined"
            color="default"
            onClick={e => this.submitClientDetails()}
          >
            Submit
          </Button>
        </div>
      </div>
    );
  }
}

export default ViewClientDetails;
