export default {
  rootAdvisory: {
    padding: '20px 15px',
  },
  rootIframe: {
    outline: 'none',
    position: 'relative',
    margin: '10px 35px',
    padding: '0 50px',
    height: '100%',
    '& .close-times': {
      position: 'absolute',
      top: 0,
      right: 0,
      fontSize: 24,
      color: 'var(--whiteMediumEmp)',
      cursor: 'pointer',
    },
  },
  rootAddForm: {
    background: '#fff',
    // minHeight: 512,
    height: 'auto',
    padding: 30,
    position: 'relative',
    maxWidth: 400,
    margin: '30px auto',

    '& .close-times': {
      color: '#000',
      fontSize: 30,
      position: 'absolute',
      right: 10,
      top: 10,
      cursor: 'pointer',
    },
  },
  rootActionable: {
    padding: '20px 0 15px 25px',
    marginBottom: '10px',
    background: 'var(--backgroundSolid)',
    borderBottom: '1px solid #f1f1f1',
    '& button': {
      border: '1px solid #fff',
      background: 'rgba(255, 255, 255, 0)',
      padding: '6px',
      borderRadius: 3,
      cursor: 'pointer',
      color: 'var(--whiteMediumEmp)',

      '&:hover': {
        background: 'rgba(255, 255, 255, 0.2)',
        // boxShadow: '3px 4px 10px 0px rgba(0,0,0,0.4)'
      },
    },
  },
};
