import React, { Component, Fragment } from 'react';
//
import styles from './style';
import CustomModal from '../../../ui/modal';
import AlertDialog from '../../../ui/alertDialog';
import CustomReportsForm from '../../../ui/customReportsForm';
import Empty from '../../../ui/empty';
// //components
import TableList from '../../../ui/tablelist';
import TableListCell from '../../../ui/tablelist/TableListCell';
import TableListRow from '../../../ui/tablelist/TableListRow';
//helpers
import injectSheet from 'react-jss';
import { withCookies } from 'react-cookie';
import { fetchApi } from '../../../../helpers/http/fetch';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import store from '../../../../store';
import { toggleToast } from '../../../../actions';
import Actionables from './Actionables';
import { getValuesFromObjects } from './utils';
import {
  getPodcastList,
  editPodcast,
  deletePodcast,
  createUpdatePodcast,
} from '../../../../requestor/admin/requestor';
import moment from 'moment';

class Podcast extends Component {
  state = {
    podcastList: [],
    modalUrl: '',
    showAlert: false,
    showAddForm: false,
    isEdit: false,
    blogDetails: {},
    reportId: null,
  };

  componentDidMount() {
    getPodcastList().then(podcastList => {
      this.setState({ podcastList });
    });
  }

  getPodcastListXML = () => {
    let { podcastList } = this.state;
    return podcastList.map(i => {
      return (
        <TableListRow key={i.id}>
          <TableListCell value={i?.title || '-'} />
          <TableListCell
            value={moment(i?.uploadTime).format('YYYY-MM-DD') || '-'}
          />
          <TableListCell value={i?.linkURL || '-'} />
          <TableListCell classes="btn--actions--wrap">
            <button
              className="btn--edit"
              onClick={() => this.handleEdit(i?.id)}
            >
              <i className="icon-uniE93E"></i>
            </button>
            <button
              className="btn--delete"
              onClick={() => this.handleDelete(i?.id)}
            >
              <i className="icon-uniE949"></i>
            </button>
          </TableListCell>
        </TableListRow>
      );
    });
  };

  handleDelete = id => {
    this.setState({
      showAlert: true,
      reportId: id,
    });
  };

  handleEdit = id => {
    this.setState(
      {
        reportId: id,
      },
      () => {
        this.fetchPodcastDetails();
      },
    );
  };

  fetchPodcastDetails = () => {
    const { reportId } = this.state;
    editPodcast(reportId)
      .then(data => {
        this.setState({
          podcastDetails: { ...data, advisorycountries: data?.videocountries },
        });
        this.handleEditForm({
          isEdit: true,
          showAddForm: true,
        });
      })
      .catch(e => console.log('Error fetching details', e));
  };

  handleEditForm = ({ isEdit, showAddForm }) => {
    this.setState({
      isEdit,
      showAddForm,
    });
  };

  handleConfirmDelete = id => {
    deletePodcast(id)
      .then(data => {
        if (data.success) {
          this.setState(
            {
              showAlert: false,
            },
            () => {
              let toastObj = {
                toastMsg: data.message,
                showToast: data.success,
              };
              store.dispatch(toggleToast(toastObj));
              getPodcastList().then(podcastList => {
                this.setState({ podcastList });
              });
            },
          );
        }
      })
      .catch(e => console.log(e));
  };

  handleDownload = id => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let reqObj = {
      url: `${API_ROUTES.downloadAdvisory}/${id}`,
      isAuth: true,
      authToken,
      method: 'POST',
      isBlob: true,
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(blobObj => {
        let a = document.createElement('a');
        document.body.appendChild(a);
        let objUrl = new Blob([blobObj], { type: 'application/pdf' });
        let url = window.URL.createObjectURL(objUrl);
        a.href = url;
        a.download = 'advisory-pdf';
        a.click();
        window.URL.revokeObjectURL(url);
      })
      .catch(e => console.log(e));
  };

  handleView = id => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let reqObj = {
      url: `${API_ROUTES.viewAdvisory}/${id}`,
      isAuth: true,
      authToken,
      method: 'POST',
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        if (data.success) {
          this.setState({
            showModal: true,
            modalUrl: data.output.viewimageurl,
          });
        }
      })
      .catch(e => console.log(e));
  };

  toggleModal = showModal => this.setState({ showModal });

  handleCloseAlert = () => this.setState({ showAlert: false });

  handleToggleAdd = showAddForm => this.setState({ showAddForm });

  handleSubmit = data => {
    const { isEdit, reportId } = this.state;
    let {
      title,
      selectedLiveTopic: { value: topicid },
      selectedCountries,
      date,
      linkURL,
    } = data;

    let reqData = {
      ...(isEdit && { id: reportId }),
      title,
      linkURL,
      topicid: topicid || 0,
      countryids: getValuesFromObjects(selectedCountries),
      uploadTime: date
    };
    
    createUpdatePodcast(reqData).then(res => {
      if (res.success) {
        let toastObj = {
          toastMsg: res.message,
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
        this.handleToggleAdd(false);
        getPodcastList().then(podcastList => {
          this.setState({ podcastList, isEdit: false });
        });
      } else {
        let toastObj = {
          toastMsg: 'Something went wrong!',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      }
    });
  };

  handleCloseModal = () => {
    this.handleToggleAdd(false)
    this.setState({isEdit: false})
  }

  render() {
    let { classes } = this.props;
    let {
      podcastList,
      showModal,
      modalUrl,
      showAlert,
      reportId,
      showAddForm,
      isEdit,
      podcastDetails,
    } = this.state;

    return (
      <div className="approveEmail__wrap">
        <Actionables handleAdd={this.handleToggleAdd} />

        {podcastList?.length > 0 ? (
          <Fragment>
            <div
              className="tablelist__head"
              style={{ borderBottom: `1px solid rgba(255,255,255,0.6)` }}
            >
              <div>
                <label>Title</label>
              </div>
              <div>
                <label>Date</label>
              </div>
              <div>
                <label>Link</label>
              </div>
              <div>
                <label>#</label>
              </div>
            </div>
            <TableList classes={{ root: 'darkBlue' }}>
              {this.getPodcastListXML()}
            </TableList>
          </Fragment>
        ) : (
          <Empty title="No Podcasts Available" />
        )}
        <CustomModal
          showModal={showModal}
          closeModal={() => this.toggleModal(false)}
        >
          <div className={classes.rootIframe}>
            <span
              className="close-times"
              onClick={() => this.toggleModal(false)}
            >
              &times;
            </span>
            <iframe src={modalUrl} height="100%" width="100%" title="pdf" />
          </div>
        </CustomModal>
        <CustomModal
          showModal={showAddForm}
          closeModal={() => this.handleCloseModal()}
        >
          <div className={classes.rootAddForm}>
            <span
              className="close-times"
              onClick={() =>
                this.handleEditForm({ isEdit: false, showAddForm: false })
              }
            >
              &times;
            </span>
            <CustomReportsForm
              heading="Create New Podcast"
              titlePlaceHolder="Enter title"
              handleSubmit={this.handleSubmit}
              showTopics={true}
              showCountries={true}
              showDate={true}
              showPodcastLink={true}
              showUploadTabs={false}
              isEdit={isEdit}
              details={podcastDetails}
            />
          </div>
        </CustomModal>
        {showAlert && (
          <AlertDialog
            isOpen={showAlert}
            title="Delete Podcast"
            description="You are about to delete an event, are you sure?"
            parentEventHandler={() => this.handleConfirmDelete(reportId)}
            handleClose={() => this.handleCloseAlert()}
          />
        )}
      </div>
    );
  }
}

export default withCookies(injectSheet(styles)(Podcast));
