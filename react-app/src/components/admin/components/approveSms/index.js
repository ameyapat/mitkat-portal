import React, { Component, Fragment } from 'react';
import './style.scss';
//components
import TableList from '../../../ui/tablelist';
import TableListCell from '../../../ui/tablelist/TableListCell';
import TableListRow from '../../../ui/tablelist/TableListRow';
import CustomModal from '../../../ui/modal';
import EditSmsAlert from './EditSmsAlert';
//helpers
import { fetchApi } from '../../../../helpers/http/fetch';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import { withCookies } from 'react-cookie';
import store from '../../../../store';
import { toggleToast } from '../../../../actions';
import Empty from '../../../ui/empty';

@withCookies
class ApproveSms extends Component {
  state = {
    tableData: [],
    showEventModal: false,
    eventDetails: null,
  };

  componentDidMount() {
    // this.getSmsAlerts();
  }

  getSmsAlerts = () => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let reqObj = {
      url: API_ROUTES.getSmsAlertsApproval,
      isAuth: true,
      authToken,
      method: 'POST',
      showToggle: true,
    };
    fetchApi(reqObj).then(data => this.setState({ tableData: data }));
  };

  getTableItems = () => {
    let { tableData } = this.state;
    let itemXML = [];

    itemXML = tableData.map(i => {
      let approveStatus = 'pending';

      if (i.status.status === 'Approved') {
        approveStatus = 'approved';
      } else if (i.status.status === 'Pending Approval') {
        approveStatus = 'pending';
      } else {
        approveStatus = 'rejected';
      }

      return (
        <TableListRow key={i.id}>
          <TableListCell value={i.date || '-'} />
          <TableListCell value={i.title || '-'} classes={'amd--title'} />
          <TableListCell value={i.country || '-'} />
          <TableListCell value={i.state || '-'} />
          <TableListCell value={i.city || '-'} />
          <TableListCell>
            <span className={`approve--status ${approveStatus}`}></span>
            <p>{i.status.status || '-'}</p>
          </TableListCell>
          <TableListCell value={i.creator || '-'} />
          {/* <TableListCell value={i.type || '-'} /> */}
          {/* <TableListCell value={i.importance ? 'Yes' : 'No'} /> */}
          <TableListCell classes="btn--actions--wrap">
            <button
              className="btn--edit"
              onClick={() => this.handleEditEvent(i.id)}
            >
              <i className="icon-uniE93E"></i>
            </button>
            {/* <button 
                            className="btn--delete" 
                            onClick={() => this.handleDeleteEvent(i.id)}
                        >
                            <i className="icon-uniE949"></i>
                        </button> */}
          </TableListCell>
        </TableListRow>
      );
    });
    return itemXML;
  };

  handleDeleteEvent = id => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    //use fetch here
    let reqObj = {
      url: `${API_ROUTES.deleteTodaysEvent}/${id}`,
      isAuth: true,
      authToken,
      method: 'POST',
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        let toastObj = {
          toastMsg: data.message,
          showToast: data.success,
        };
        store.dispatch(toggleToast(toastObj));
        this.getEvents();
      })
      .catch(e => {
        let toastObj = {
          toastMsg: 'Error deleting report',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      });
  };

  handleEditEvent = id => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    //use fetch here
    let reqObj = {
      url: `${API_ROUTES.editSmsAlertApproval}/${id}`,
      isAuth: true,
      authToken,
      method: 'POST',
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
                      this.setState({ eventDetails: data }, () => {
                        this.handleModal(true);
                      });
                    })
      .catch(e => {
                    let toastObj = {
                      toastMsg: 'Error fetching details',
                      showToast: true,
                    };
                    store.dispatch(toggleToast(toastObj));
                  });
  };

  handleModal = show => {
    this.setState({ showEventModal: show });
  };

  updateEvent = eventDetails => {
    
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');

    let reqObj = {
      url: API_ROUTES.updateSmsAlertApproval,
      data: eventDetails,
      isAuth: true,
      authToken,
      method: 'POST',
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        let toastObj = {
          toastMsg: data.message,
          showToast: true,
        };
        this.handleModal(false);
        store.dispatch(toggleToast(toastObj));
        this.getSmsAlerts(); //refresh list
      })
      .catch(e => {
        let toastObj = {
          toastMsg: 'Error editing event!',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      });
  };

  render() {
    let { tableData, eventDetails } = this.state;

    return (
      <div className="approveSms__wrap">
        <h2>Sms alerts</h2>
        {tableData.length > 0 ? (
          <Fragment>
            <div className="tablelist__head">
              <div>
                <label>Date</label>
              </div>
              <div className="tl__title">
                <label>Title</label>
              </div>
              <div>
                <label>Country</label>
              </div>
              <div>
                <label>State</label>
              </div>
              <div>
                <label>City</label>
              </div>
              <div>
                <label>Status</label>
              </div>
              <div>
                <label>Creator</label>
              </div>
              {/* <div><label>Type</label></div> */}
              {/* <div><label>Importance</label></div> */}
              <div>
                <label>Edit</label>
              </div>
            </div>
            <TableList>
              {tableData.length > 0 && this.getTableItems()}
            </TableList>
          </Fragment>
        ) : (
          <Empty title="No Sms alerts!" />
        )}

        <CustomModal
          showModal={this.state.showEventModal}
          closeModal={() => this.handleModal(false)}
        >
          <div id="edit-event--modal" className="modal__inner">
            {eventDetails && (
              <EditSmsAlert
                eventDetails={this.state.eventDetails}
                closeEventModal={this.handleModal}
                updateEvent={this.updateEvent}
              />
            )}
          </div>
        </CustomModal>
      </div>
    );
  }
}

export default ApproveSms;
