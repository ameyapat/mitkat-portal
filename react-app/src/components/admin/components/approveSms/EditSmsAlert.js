import React, { Component } from 'react';
import './style.scss';
//react google places
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';
//material ui components
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import TextField from '@material-ui/core/TextField';
// import Radio from '@material-ui/core/Radio';
import Button from '@material-ui/core/Button';
// import RadioGroup from '@material-ui/core/RadioGroup';
// import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
// import FormLabel from '@material-ui/core/FormLabel';
import MultiSelect from '../../../ui/multiSelect';
//helpers
import { withCookies } from 'react-cookie';
import { getListData } from '../../helpers/utils';
import {
  filterArrObjects,
  fetchCountries,
  fetchStates,
  fetchCities,
  fetchProducts,
  fetchIndustries,
  createValueLabel,
} from '../../../../helpers/utils';
import { fetchApi } from '../../../../helpers/http/fetch';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import store from '../../../../store';
import { toggleToast } from '../../../../actions/';
import injectSheet from 'react-jss';
import style from './style';

const searchOptions = {
  componentRestrictions: { country: 'in' },
};

@withCookies
@injectSheet(style)
class EditSmsAlert extends Component {
  state = {
    address: '',
    riskLevels: [],
    riskCategories: [],
    typeOfStories: [],
    countries: [],
    states: [],
    metros: [],
    industries: [],
    selectedRiskLevel: '',
    selectedCategory: '',
    selectedStory: '',
    selectedCountries: [],
    selectedStates: [],
    selectedMetros: [],
    eventDetails: '',
    eventSource: '',
    eventTitle: '',
    location: '',
    impactAnalysis: '',
    recommendations: '',
    isImportant: false,
    approveStatus: null,
    isLive: false,
    impactedIndustries: [],
    //
    location: {
      lat: '',
      lng: '',
    },
    id: '',
  };

  componentDidMount() {
    let { cookies, eventDetails, isApprover } = this.props;
    let {
      id,
      location,
      selectedMetros,
      selectedCountries,
      selectedState,
      impactedIndustries,
      selectedStates,
      approveStatus,
      eventTitle,
      eventSource,
      selectedRiskLevel,
      regionType,
      selectedCategory,
      selectedStory,
    } = this.state;
    let authToken = cookies.get('authToken');

    id = eventDetails.id;
    eventTitle = eventDetails.title;
    eventSource = eventDetails.links;
    location['lat'] = eventDetails.latitude;
    location['lng'] = eventDetails.longitude;
    selectedMetros = createValueLabel(eventDetails.cities, 'city'); //method to add value & label
    selectedCountries = createValueLabel(eventDetails.countries, 'country'); //method to add value & label
    selectedStates = createValueLabel(eventDetails.state, 'state'); //method to add value & label
    impactedIndustries = createValueLabel(
      eventDetails.impactindustry,
      'impactIndustry',
    ); //method to add value & label
    approveStatus = parseInt(eventDetails.status.id);
    selectedRiskLevel = eventDetails.riskLevel.id;
    selectedCategory = eventDetails.riskCategory.id;
    selectedStory = eventDetails.regionType.id;
    regionType = eventDetails.regionType ? eventDetails.regionType.id : '';

    this.setState(
      {
        id,
        location,
        selectedMetros,
        selectedCountries,
        selectedState,
        approveStatus,
        eventTitle,
        eventSource,
        selectedStates,
        impactedIndustries,
        selectedRiskLevel,
        selectedCategory,
        regionType,
        selectedStory,
      },
      () => {
        this.getReverseGeoCode();
      },
    );

    this.getRiskLevels('riskLevel');
    this.getRiskCategories('riskCategory');
    this.getStories('typeOfStory');
    fetchCountries(authToken).then(countries => this.setState({ countries }));
    fetchStates(authToken).then(states => this.setState({ states }));
    fetchCities(authToken).then(metros => this.setState({ metros }));
    fetchIndustries(authToken).then(industries =>
      this.setState({ industries }),
    );
  }

  getReverseGeoCode = () => {
    let {
      location: { lat, lng },
    } = this.state;

    let reqObj = {
      url: `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&key=AIzaSyCYOTLyn6xbysArlvlW4zpxnH9p6H3An08&v=3.exp`,
      method: 'GET',
      isAuth: false,
    };

    fetch(reqObj.url)
      .then(data => data.json())
      .then(res => {
        let address = res.results[0].formatted_address;
        this.setState({ address });
      })
      .catch(e => console.log(e));
  };

  /** handle text area changes */
  handleInputChange = (e, type) => {
    this.setState({ [type]: e.target.value }, () => console.log(this.state));
  };

  handleChange = (e, type) => {
    switch (type) {
      case 'riskLevel':
        this.setState({ selectedRiskLevel: e.target.value });
        break;
      case 'riskCategory':
        this.setState({ selectedCategory: e.target.value });
        break;
      case 'typeOfStory':
        this.setState({ selectedStory: e.target.value });
        break;
      case 'approveStatus':
        this.setState({ approveStatus: e.target.value });
        break;
      default:
        console.log('something went wrong');
        break;
    }
  };

  getRiskLevels = type => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');

    getListData(type, authToken).then(data => {
      this.setState({ riskLevels: data });
    });
  };

  getStories = type => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');

    getListData(type, authToken).then(data => {
      this.setState({ typeOfStories: data });
    });
  };

  getRiskCategories = type => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');

    getListData(type, authToken).then(data => {
      this.setState({ riskCategories: data });
    });
  };

  riskLevelsXML = () => {
    let { riskLevels } = this.state;
    let riskLevelXML = [];
    riskLevelXML = riskLevels.map(item => (
      <MenuItem key={item.id} value={item.id}>
        {item.level}
      </MenuItem>
    ));
    return riskLevelXML;
  };

  riskCategoriesXML = () => {
    let { riskCategories } = this.state;
    let riskCategoryXML = [];
    riskCategoryXML = riskCategories.map(item => (
      <MenuItem key={item.id} value={item.id}>
        {item.riskcategory}
      </MenuItem>
    ));
    return riskCategoryXML;
  };

  storiesXML = () => {
    let { typeOfStories } = this.state;
    let storiesXML = [];
    storiesXML = typeOfStories.map(item => (
      <MenuItem key={item.id} value={item.id}>
        {item.regiontype}
      </MenuItem>
    ));
    return storiesXML;
  };

  handleMultiSelect = (value, type) => {
    let values = this.state[type];
    if (values.includes(value)) {
      let index = values.indexOf(value);
      values.splice(index, 1);
    } else {
      values = [...value];
    }
    this.setState({ [type]: values });
  };

  handleAddressChange = address => {
    this.setState({ address });
  };
  /** function for setting location lat & lng */
  handleSelect = address => {
    geocodeByAddress(address)
      .then(results => getLatLng(results[0]))
      .then(latLng => {
        let { location } = this.state;
        location['lat'] = latLng.lat;
        location['lng'] = latLng.lng;
        this.setState({ location }, () => console.log(this.state));
      })
      .catch(error => {
        console.error('Error', error);
        let toastObj = {
          showToast: true,
          toastObj: 'Error getting location, try again!',
        };
        store.dispatch(toggleToast(toastObj));
      });
  };

  handleUpdateEvent = () => {
    let {
      id,
      eventDetails,
      selectedCountries,
      eventSource,
      eventTitle,
      selectedRiskLevel,
      selectedCategory,
      impactedIndustries,
      approveStatus,
      selectedMetros,
      selectedStates,
      selectedStory,
      location: { lat, lng },
    } = this.state;

    eventDetails = {
      id: id,
      title: eventTitle,
      links: eventSource,
      latitude: lat, //lat
      longitude: lng, //long
      riskLevel: selectedRiskLevel,
      riskcategory: selectedCategory,
      regionType: selectedStory,
      status: approveStatus,
      impactindustry: filterArrObjects(impactedIndustries),
      countries: filterArrObjects(selectedCountries),
      state: filterArrObjects(selectedStates),
      cities: filterArrObjects(selectedMetros),
    };

    this.props.updateEvent(eventDetails);
  };

  render() {
    let { classes } = this.props;
    let {
      riskLevels,
      riskCategories,
      countries,
      states,
      metros,
      industries,
      selectedCountries,
      selectedStates,
      selectedRiskLevel,
      selectedCategory,
      typeOfStories,
      selectedStory,
      selectedMetros,
      impactedIndustries,
      approveStatus,
    } = this.state;

    return (
      <div className="createEvent__inner">
        <h3>Edit Event</h3>
        <div className="field--create--event">
          <div id="reactGooglePlaces" className="google--places">
            <label className="location--txt">
              <i className="icon-uniE9F1"></i> Enter Event Location
            </label>
            <PlacesAutocomplete
              value={this.state.address}
              onChange={this.handleAddressChange}
              onSelect={this.handleSelect}
              searchOptions={searchOptions}
            >
              {({
                getInputProps,
                suggestions,
                getSuggestionItemProps,
                loading,
              }) => (
                <div>
                  <input
                    {...getInputProps({
                      placeholder: 'Search Places ...',
                      className: 'location-search-input',
                    })}
                  />
                  {suggestions && (
                    <div className="autocomplete-dropdown-container">
                      {loading && (
                        <div className="loader-places">
                          <p>Loading...</p>
                        </div>
                      )}
                      {suggestions.map(suggestion => {
                        const className = suggestion.active
                          ? 'suggestion-item--active'
                          : 'suggestion-item';
                        // inline style for demonstration purpose
                        const style = suggestion.active
                          ? { backgroundColor: '#fafafa', cursor: 'pointer' }
                          : { backgroundColor: '#ffffff', cursor: 'pointer' };
                        return (
                          <div
                            {...getSuggestionItemProps(suggestion, {
                              className,
                              style,
                            })}
                          >
                            <span className="list--item">
                              {suggestion.description}
                            </span>
                          </div>
                        );
                      })}
                    </div>
                  )}
                </div>
              )}
            </PlacesAutocomplete>
          </div>
          <h2 className="event-head--title">
            <i className="icon-uniE90E"></i> Event Details
          </h2>
          {/**event sources */}
          <div className="event--sources">
            <TextField
              id="eventSources"
              label="Event Sources"
              multiline
              fullWidth
              rows="3"
              value={this.state.eventSource}
              onChange={e => this.handleInputChange(e, 'eventSource')}
              // className={classes.textField}
              defaultValue="Enter Event Source"
              margin="normal"
              variant="outlined"
            />
          </div>
          {/**event title */}
          <div className="event--title">
            <TextField
              id="eventTitle"
              label="Event Title"
              multiline
              fullWidth
              rows="3"
              value={this.state.eventTitle}
              onChange={e => this.handleInputChange(e, 'eventTitle')}
              // className={classes.textField}
              defaultValue="Enter Event Title"
              margin="normal"
              variant="outlined"
            />
          </div>
          {/** multi selects goes here */}
          <h2 className="event-head--title">
            <i className="icon-uniE9EC"></i> Event Locations
          </h2>
          <div className="twoColWrap multi--selects">
            <div className="col-2">
              <MultiSelect
                value={selectedCountries}
                placeholder={'Select Countries'}
                parentEventHandler={value =>
                  this.handleMultiSelect(value, 'selectedCountries')
                }
                options={countries}
              />
            </div>
            <div className="col-2">
              <MultiSelect
                value={selectedStates}
                placeholder={'Select States'}
                parentEventHandler={value =>
                  this.handleMultiSelect(value, 'selectedStates')
                }
                options={states}
              />
            </div>
            <div className="col-2">
              <MultiSelect
                value={selectedMetros}
                placeholder={'Select Metros'}
                parentEventHandler={value =>
                  this.handleMultiSelect(value, 'selectedMetros')
                }
                options={metros}
              />
            </div>
            <div className="col-2">
              <MultiSelect
                value={impactedIndustries}
                placeholder={'Impact Industry'}
                parentEventHandler={value =>
                  this.handleMultiSelect(value, 'impactedIndustries')
                }
                options={industries}
              />
            </div>
          </div>
          {/** single selects goes here */}
          <h2 className="event-head--title">
            <i className="icon-uniE99D"></i> Event Type
          </h2>
          <div className="twoColWrap single--selects">
            <div className="col-3">
              <FormControl
                variant="outlined"
                classes={{
                  root: classes.singleSelect,
                }}
              >
                <InputLabel
                  classes={{
                    outlined: {
                      background: 'red',
                      height: 10,
                      width: '100%',
                    },
                  }}
                  ref={ref => {
                    this.riskLevel = ref;
                  }}
                  htmlFor="riskLevel"
                >
                  Risk Level
                </InputLabel>
                <Select
                  value={selectedRiskLevel}
                  onChange={e => this.handleChange(e, 'riskLevel')}
                  input={
                    <OutlinedInput
                      labelWidth={0}
                      name="Risk Level"
                      id="riskLevel"
                    />
                  }
                >
                  {riskLevels.length > 0 && this.riskLevelsXML()}
                </Select>
              </FormControl>
            </div>
            <div className="col-3">
              <FormControl
                variant="outlined"
                classes={{
                  root: classes.singleSelect,
                }}
              >
                <InputLabel
                  ref={ref => {
                    this.riskCategory = ref;
                  }}
                  htmlFor="riskCategory"
                >
                  Risk Category
                </InputLabel>
                <Select
                  value={selectedCategory}
                  onChange={e => this.handleChange(e, 'riskCategory')}
                  input={
                    <OutlinedInput
                      labelWidth={this.state.labelWidth}
                      name="Risk Category"
                      id="riskCategory"
                    />
                  }
                >
                  {riskCategories.length > 0 && this.riskCategoriesXML()}
                </Select>
              </FormControl>
            </div>
            <div className="col-3">
              <FormControl
                variant="outlined"
                classes={{
                  root: classes.singleSelect,
                }}
              >
                <InputLabel
                  ref={ref => {
                    this.typeOfStory = ref;
                  }}
                  htmlFor="typeOfStory"
                >
                  Type Of Story
                </InputLabel>
                <Select
                  value={selectedStory}
                  onChange={e => this.handleChange(e, 'typeOfStory')}
                  input={
                    <OutlinedInput
                      labelWidth={this.state.labelWidth}
                      name="Type Of Story"
                      id="typeOfStory"
                    />
                  }
                >
                  {typeOfStories.length > 0 && this.storiesXML()}
                </Select>
              </FormControl>
            </div>
          </div>
          <div className="status__wrap">
            <Select
              value={approveStatus}
              fullWidth
              onChange={e => this.handleChange(e, 'approveStatus')}
              input={
                <OutlinedInput
                  labelWidth={this.state.labelWidth}
                  name="Approval Status"
                  id="approveStatus"
                />
              }
            >
              <MenuItem key={1} value={1}>
                Pending Approval
              </MenuItem>
              <MenuItem key={2} value={2}>
                Approved
              </MenuItem>
              <MenuItem key={3} value={3}>
                Rejected
              </MenuItem>
            </Select>
            <label className="label-input">Approval Status</label>
          </div>
        </div>
        <div className="actionables">
          <Button
            onClick={() => this.props.closeEventModal(false)}
            variant="contained"
            color="default"
          >
            Cancel
          </Button>
          <Button
            onClick={this.handleUpdateEvent}
            style={{ marginLeft: 20, backgroundColor: '#2980b9' }}
            variant="contained"
            color="secondary"
          >
            Update
          </Button>
        </div>
      </div>
    );
  }
}

export default EditSmsAlert;
