import React, { Component, Fragment } from 'react';
//import './style.scss';
//components
import TableList from '../../../ui/tablelist';
import TableListCell from '../../../ui/tablelist/TableListCell';
import TableListRow from '../../../ui/tablelist/TableListRow';
import CustomModal from '../../../ui/modal';
import Empty from '../../../ui/empty';
import EditEventUI from '../createEventUI/EditEventUI';
//helpers
import { fetchApi } from '../../../../helpers/http/fetch';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import { withCookies } from 'react-cookie';
import store from '../../../../store';
import { toggleToast } from '../../../../actions';

@withCookies
class DuplicateEvents extends Component {
  state = {
    tableData: [],
    showEventModal: false,
    eventDetails: null,
  };

  componentDidMount() {
    this.getEvents();
  }

  getEvents = () => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let reqObj = {
      url: API_ROUTES.getSimilarityAnalysis,
      isAuth: true,
      authToken,
      method: 'GET',
      showToggle: true,
    };
    fetchApi(reqObj).then(data => this.setState({ tableData: data }));
  };

  getTableItems = () => {
    let { tableData } = this.state;
    let itemXML = [];

    itemXML = tableData.map(i => {
      return (
        <TableListRow key={i.id}>
          <TableListCell value={i.eventtitle || '-'} />
          <TableListCell
            value={i.similareventtitle || '-'}
            customStyles={{ color: '#' + i.color }}
          />
          <TableListCell
            value={i.similaritypercent || '-'}
            customStyles={{ color: '#' + i.color }}
          />
          <TableListCell classes="btn--actions--wrap">
            <button
              className="btn--edit"
              onClick={() => this.handleEditEvent(i.eventid)}
            >
              <i className="icon-uniE93E"></i>
            </button>
            <button
              className="btn--delete"
              onClick={() => this.handleDeleteEvent(i.eventid)}
            >
              <i className="icon-uniE949"></i>
            </button>
          </TableListCell>
        </TableListRow>
      );
    });

    return itemXML;
  };

  handleDeleteEvent = id => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    //use fetch here
    let reqObj = {
      url: `${API_ROUTES.deleteTodaysEvent}/${id}`,
      isAuth: true,
      authToken,
      method: 'POST',
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        let toastObj = {
          toastMsg: data.message,
          showToast: data.success,
        };
        store.dispatch(toggleToast(toastObj));
        this.getEvents();
      })
      .catch(e => {
        let toastObj = {
          toastMsg: 'Error deleting report',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      });
  };

  handleEditEvent = id => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    //use fetch here
    let reqObj = {
      url: `${API_ROUTES.editTodaysEvent}/${id}`,
      isAuth: true,
      authToken,
      method: 'POST',
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        this.setState({ eventDetails: data }, () => {
          this.handleModal(true);
        });
      })
      .catch(e => {
        let toastObj = {
          toastMsg: 'Error fetching details',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      });
  };

  handleModal = show => {
    this.setState({ showEventModal: show });
  };

  updateEvent = eventDetails => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');

    let reqObj = {
      url: API_ROUTES.updateTodaysEvent,
      data: eventDetails,
      isAuth: true,
      authToken,
      method: 'POST',
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        let toastObj = {
          toastMsg: 'Event Updated Successfully!',
          showToast: true,
        };
        this.handleModal(false);
        store.dispatch(toggleToast(toastObj));
        this.getEvents();
      })
      .catch(e => {
        let toastObj = {
          toastMsg: 'Error editing event!',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      });
  };

  render() {
    let { tableData, eventDetails } = this.state;

    return (
      <div className="coverageRating__wrap">
        <Fragment>
          <h2>Duplicate Events</h2>
        </Fragment>
        {tableData.length > 0 ? (
          <Fragment>
            <div className="tablelist__head">
              <div className="tablelist__left">
                <label>Event Title</label>
              </div>
              <div>
                <label>Similar Event Title</label>
              </div>
              <div>
                <label>Similarity Percent</label>
              </div>
              <div>
                <label>Edit/Delete</label>
              </div>
            </div>
            <TableList classes={{ root: 'darkBlue' }}>
              {tableData.length > 0 && this.getTableItems()}
            </TableList>
          </Fragment>
        ) : (
          <Empty title="No Coverage Rating!" />
        )}
        <CustomModal
          showModal={this.state.showEventModal}
          closeModal={() => this.handleModal(false)}
        >
          <div id="edit-event--modal" className="modal__inner">
            {eventDetails && (
              <EditEventUI
                eventDetails={this.state.eventDetails}
                closeEventModal={this.handleModal}
                isApprover={true}
                updateEvent={this.updateEvent}
              />
            )}
          </div>
        </CustomModal>
      </div>
    );
  }
}

export default DuplicateEvents;
