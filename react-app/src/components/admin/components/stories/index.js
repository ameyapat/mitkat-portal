import React, { Component } from 'react';
// import Chart from 'chart.js';
// import { storiesMetrics } from '../../helpers/constants';
import { Bar } from 'react-chartjs-2';
import './style.scss';

const data = {
    labels: ["Approved", "Pending Approval", "Rejected Stories"],
    datasets: [{
        label: '# of Stories',
        data: [12, 19, 3, 5, 2, 3],
        backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)'
        ],
        borderColor: [
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
        ],
        borderWidth: 1
    }]
}

export default class Stories extends Component {
  render() {
    return (
      <div id="stories">
        <Bar height={100} data={data} />
      </div>
    )
  }
}
