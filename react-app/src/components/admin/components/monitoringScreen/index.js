import React, { Component, useEffect } from 'react';
import { fetchApi } from '../../../../helpers/http/fetch';
import { withCookies } from 'react-cookie';
import './style.scss';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import TreeView from '@mui/lab/TreeView';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MonitoringScreenCard from '../monitoringScreenCard';
import TreeItem from '@mui/lab/TreeItem';
import { Checkbox, FormControlLabel } from '@material-ui/core';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import moment from 'moment';

const MonitoringScreen = props => {
  const { cookies } = props;
  const [selected, setSelected] = React.useState([]);
  const [eventList, setEventList] = React.useState([]);
  const [filterList, setFilterList] = React.useState('');
  const [filteredEventList, setFilteredEventList] = React.useState([]);
  const [apiLastHit, setApiLastHit] = React.useState('');
  useEffect(() => {
    initFetch();
  }, []);

  useEffect(() => {
    checkData();
  }, [eventList]);

  const initFetch = () => {
    getRimeFilteredFeed();
    clearInterval(reloadData);
    const reloadData = setInterval(getRimeFilteredFeed, 600000);
  };

  const getRimeFilteredFeed = () => {
    let authToken = cookies.get('authToken');
    let reqObj = {
      method: 'GET',
      isAuth: true,
      authToken,
      url: API_ROUTES.getMonitoringFeed,
    };

    fetchApi(reqObj)
      .then(data => {
        setEventList(data);
        setApiLastHit(new Date().toString());
      })
      .catch(e => console.log(e));
  };

  const checkData = () => {
    let checkboxData = [];
    let count = eventList.regionList && eventList.regionList.length + 1;
    eventList.regionList &&
      eventList.regionList.map((item, srNo) => {
        checkboxData.push({
          // id: JSON.stringify(srNo + 1),
          id: srNo + 1,
          name: item.regionName,
          children: item?.countryList.map(subitem => {
            return {
              // id: JSON.stringify(count++),
              id: count++,
              name: subitem.countryName,
            };
          }),
        });
      });
    setFilterList({
      id: 0,
      name: 'Select All',
      children: checkboxData,
    });
  };

  function getChildById(node, id) {
    let array = [];

    function getAllChild(nodes) {
      if (nodes === null) return [];
      array.push(nodes.id);
      if (Array.isArray(nodes.children)) {
        nodes.children.forEach(node => {
          array = [...array, ...getAllChild(node)];
          array = array.filter((v, i) => array.indexOf(v) === i);
        });
      }
      return array;
    }

    function getNodeById(nodes, id) {
      if (nodes.id === id) {
        return nodes;
      } else if (Array.isArray(nodes.children)) {
        let result = null;
        nodes.children.forEach(node => {
          if (!!getNodeById(node, id)) {
            result = getNodeById(node, id);
          }
        });
        return result;
      }

      return null;
    }

    return getAllChild(getNodeById(node, id));
  }

  function getUniqueFeedByTitle(arr,key){
    return [...new Map(arr.map(item => [item[key], item])).values()]
  }
  function getFilteredList() {
    let list = [];
    selected &&
      selected.map(item => {
        eventList.regionList &&
          eventList.regionList.map(subitem => {
            subitem.countryList.map(subsubitem => {
              if (item - 17 === subsubitem.countryid) {
                subsubitem.events.map(event => {
                  list.push(event);
                });
              }
            });
          });
      });
    const filteredfeed = list.sort((a, b) => {
      return new Date(b.eventDate) - new Date(a.eventDate);
    });
    setFilteredEventList(getUniqueFeedByTitle(filteredfeed,'eventTitle'));
  }

  useEffect(() => {
    getFilteredList();
  }, [selected, eventList]);

  function getOnChange(checked, nodes) {
    const allNode = getChildById(filterList, nodes.id);
    let array = checked
      ? [...selected, ...allNode]
      : selected.filter(value => !allNode.includes(value));

    array = array.filter((v, i) => array.indexOf(v) === i);
    setSelected(array);
  }

  const renderTree = nodes => (
    <TreeItem
      key={nodes.id}
      nodeId={nodes.id}
      label={
        <FormControlLabel
          control={
            <Checkbox
              checked={selected.some(item => item === nodes.id)}
              onChange={event =>
                getOnChange(event.currentTarget.checked, nodes)
              }
              onClick={e => e.stopPropagation()}
              icon={<CheckBoxOutlineBlankIcon className="checkbox_icon" />}
              checkedIcon={<CheckBoxIcon className="checkbox_icon" />}
            />
          }
          label={<>{nodes.name}</>}
          key={nodes.id}
        />
      }
    >
      {Array.isArray(nodes.children)
        ? nodes.children.map(node => renderTree(node))
        : null}
    </TreeItem>
  );

  return (
    <div id="monitoringScreen">
      {/* {this.state.isFetching && <Loader />} */}
      <div className="monitoringScreenEventList">
        {filteredEventList?.length > 0 ? (
          filteredEventList.map((item, index) => {
            return (
              <MonitoringScreenCard
                id={index}
                srno={index + 1}
                eventId={item.id}
                title={item.eventTitle}
                sourceLinks={item.eventAllNewsLinks}
                location={item.eventLoc}
                noOfArticles={item.count}
                categoryId={item.eventPrimaryRiskCategory}
                secondaryCatergoryId={item.eventSecondaryRiskCategory}
                link={item.eventLink}
                userName={item.userName}
                hasClose={false}
                eventDate={item.eventDate}
                lastUpdatedDate={item.eventLatestUpdate}
                eventType={item.eventType}
                imgURL={item.eventImageUrl}
                tags={item.eventTags}
                language={item.language}
                selectedFeed={item}
              />
            );
          })
        ) : (
          <div className="rimeFilteredData_notfound"> No feed Found</div>
        )}
      </div>
      <div className="monitoringScreenRight_section">
        <div className="monitoring_filters">
          <div className="filters_header">
            <h1 className="filters_heading">Filters List</h1>
          </div>
          <TreeView
            defaultCollapseIcon={<ExpandMoreIcon />}
            defaultExpanded={[0]}
            defaultExpandIcon={<ChevronRightIcon />}
          >
            {renderTree(filterList)}
          </TreeView>
        </div>
        <div className="api_statistics">
          <div className="date">
            {moment(new Date(apiLastHit).valueOf()).format('ll')}
          </div>
          <div className="time">
            {moment(new Date(apiLastHit).valueOf()).format('LT')}
          </div>
          <div className="header">API Last Hit Time</div>
        </div>
      </div>
    </div>
  );
};

export default withCookies(MonitoringScreen);
