export default {
  rootTravelAdvisory: {
    margin: 10,
    padding: '20px 15px',
    background: 'var(--backgroundSolid)',
    '& h2': {
      fontSize: 15,
      margin: '0 0 15px',
      color: 'var(--whiteMediumEmp)',
      textTransform: 'uppercase',
    },
    '& .file--name': {
      paddingLeft: 10,
    },
  },
  rootIframe: {
    outline: 'none',
    position: 'relative',
    margin: '10px 35px',
    padding: '0 50px',
    height: '100%',
    '& .close-times': {
      position: 'absolute',
      top: 0,
      right: 0,
      fontSize: 24,
      color: 'var(--whiteMediumEmp)',
      cursor: 'pointer',
    },
  },
  rootAddForm: {
    background: '#fff',
    // minHeight: 512,
    height: 'auto',
    padding: 30,
    position: 'relative',
    maxWidth: 400,
    margin: '30px auto',

    '& .close-times': {
      color: '#000',
      fontSize: 30,
      position: 'absolute',
      right: 10,
      top: 10,
      cursor: 'pointer',
    },
  },
  rootActionable: {
    padding: '20px 0 15px 25px',
    background: '#fbf8f8',
    borderBottom: '1px solid #f1f1f1',
  },
  btn: {
    padding: '6px',
    borderRadius: 3,
    cursor: 'pointer',
    border: '1px solid rgb(41, 128, 185)',
    '&:hover': {
      background: 'rgb(41, 128, 185)',
    },
  },
  btnSolid: {
    background: 'rgb(41, 128, 185)',
    color: 'var(--whiteMediumEmp)',
  },
  btnTransparent: {
    background: 'transparent',
    color: 'rgb(41, 128, 185)',
    border: '1px solid rgb(41, 128, 185)',
    '&:hover': {
      background: 'transparent',
    },
  },
  input: {
    display: 'none',
  },
  tHead: {
    fontWeight: 'bold',
  },
};
