import React from 'react';
import TableListRow from '../../../ui/tablelist/TableListRow';
import TableListCell from "../../../ui/tablelist/TableListCell";
import UploadBtn from './UploadBtn';
import Button from '@material-ui/core/Button';
import moment from "moment";
import clsx from 'clsx';
import { withStyles } from '@material-ui/core';
import styles from './style';

const AdvisoryList = ({ advisoryList, handleView, classes, fetchAdvisoryList }) => {


    return advisoryList.map(l => {
        return (
          <TableListRow key={l.id}>
            <TableListCell value={l.locationName} />
            <TableListCell value={moment(l.lastUpdateTime).format('MMMM Do YYYY, h:mm:ss a')} />
            <TableListCell>
              <Button onClick={() => handleView(l.url)} variant="contained" component="span" className={clsx(classes.btn, classes.btnSolid)}>
                View
              </Button>
            </TableListCell>
            <TableListCell classes={{root: classes.tCell}}>
              <UploadBtn id={l.id} fetchAdvisoryList={fetchAdvisoryList} />
            </TableListCell>
          </TableListRow>
        );
      });
}

export default withStyles(styles)(AdvisoryList);