import React, { useState, useEffect } from "react";
import { uploadApi } from '../../../../helpers/http/fetch';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import store from '../../../../store';
import { toggleToast } from '../../../../actions';
import Button from "@material-ui/core/Button";
import clsx from "clsx";
import { withStyles } from "@material-ui/core";
import { withCookies } from 'react-cookie';
import styles from "./style";

const UploadBtn = ({ classes, id, cookies, fetchAdvisoryList }) => {
  let [document, setDocument] = useState(null);

  useEffect(() => {
    if(document) uploadFile();
  }, [document]);

  function uploadFile(){
    const authToken = cookies.get("authToken");
    const formData = new FormData();

    formData.append("locationid", id);
    formData.append("advisory", document);

    let params = {
      url: `${API_ROUTES.uploadTravelAdvisory}`,
      reqObj: {
        method: "POST",
        headers: {
          Authorization: `Bearer ${authToken}`
        },
        body: formData
      }
    };

    uploadApi(params.url, params.reqObj)
      .then(res => {
        if (res.success) {
          let toastObj = {
            toastMsg: res.message,
            showToast: true
          };
          store.dispatch(toggleToast(toastObj));
          fetchAdvisoryList();
        } else {
          let toastObj = {
            toastMsg: "Something went wrong!",
            showToast: true
          };
          store.dispatch(toggleToast(toastObj));
        }
      });
  }

  function handleFileChange(file) {
    setDocument(file, () => uploadFile());
  }

  return (
    <>
      <div>
        <input
          accept="image/*"
          className={classes.input}
          id={`upload-${id}`}
          multiple
          type="file"
          onChange={e => handleFileChange(e.target.files[0])}
        />
        <label htmlFor={`upload-${id}`}>
          <Button
            variant="contained"
            component="span"
            className={clsx(classes.btn, classes.btnTransparent)}
          >
            Upload
          </Button>
        </label>
      </div>
    </>
  );
};

export default withCookies(withStyles(styles)(UploadBtn));
