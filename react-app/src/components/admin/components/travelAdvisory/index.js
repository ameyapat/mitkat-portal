import React, { Component, Fragment } from 'react';
//
import styles from './style';

import CustomModal from '../../../ui/modal';

import Empty from '../../../ui/empty';
//helpers

import injectSheet from 'react-jss';
import { withCookies } from 'react-cookie';
import { fetchApi, uploadApi } from '../../../../helpers/http/fetch';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import store from '../../../../store';
import { toggleToast } from '../../../../actions';
import TableList from '../../../ui/tablelist';
import TableListRow from '../../../ui/tablelist/TableListRow';
import TableListCell from '../../../ui/tablelist/TableListCell';
import AdvisoryList from './AdvisoryList';
import clsx from 'clsx';

class TravelAdvisory extends Component {
  state = {
    advisoryList: [],
    modalUrl: '',
    showModal: false,
    showAddForm: false,
    document: null,
  };

  componentDidMount() {
    this.fetchAdvisoryList().then(data => {
      this.setState({ advisoryList: data }, () => console.log(this.state));
    });
  }

  fetchAdvisoryList = () => {
    return new Promise((res, rej) => {
      let { cookies } = this.props;
      let authToken = cookies.get('authToken');
      let reqObj = {
        url: API_ROUTES.getTravelAdvisories,
        isAuth: true,
        authToken,
        method: 'POST',
        showToggle: true,
      };

      fetchApi(reqObj)
        .then(data => {
          if (data.success) {
            res(data.output);
          }
        })
        .catch(e => rej(e));
    });
  };

  handleView = modalUrl => {
    this.setState({
      showModal: true,
      modalUrl,
    });
  };

  getAdvisoryListXML = () => (
    <AdvisoryList
      advisoryList={this.state.advisoryList}
      handleView={this.handleView}
      fetchAdvisoryList={this.fetchAdvisoryList}
    />
  );

  handleDownload = id => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let reqObj = {
      url: `${API_ROUTES.downloadAdvisory}/${id}`,
      isAuth: true,
      authToken,
      method: 'POST',
      isBlob: true,
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(blobObj => {
        let a = document.createElement('a');
        document.body.appendChild(a);
        let objUrl = new Blob([blobObj], { type: 'application/pdf' });
        let url = window.URL.createObjectURL(objUrl);
        a.href = url;
        a.download = 'advisory-pdf';
        a.click();
        window.URL.revokeObjectURL(url);
      })
      .catch(e => console.log(e));
  };

  toggleModal = showModal => this.setState({ showModal });

  handleSubmit = data => {
    // addNewAdvisory
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let { imageFile } = data;
    const formData = new FormData();

    formData.append('displaypic', imageFile);

    let params = {
      url: `${API_ROUTES.addNewAdvisory}`,
      reqObj: {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${authToken}`,
        },
        body: formData,
      },
    };

    uploadApi(params.url, params.reqObj).then(res => {
      if (res.success) {
        let toastObj = {
          toastMsg: 'Advisory Added Successfully!',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
        this.handleToggleAdd();
        this.fetchAdvisoryList().then(data => {
          this.setState({ advisoryList: data });
        });
      } else {
        let toastObj = {
          toastMsg: 'Something went wrong!',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      }
    });
  };

  render() {
    let { classes } = this.props;
    let { showModal, modalUrl } = this.state;

    return (
      <Fragment>
        <div className={classes.rootTravelAdvisory}>
          <h2>Travel Advisory</h2>
          <TableList>
            <TableListRow>
              <TableListCell
                value="City Name"
                classes={{ root: classes.tHead }}
              />
              <TableListCell
                value="Last Updated At"
                classes={{ root: classes.tHead }}
              />
              <TableListCell value="#" classes={{ root: classes.tHead }} />
              <TableListCell value="#" classes={{ root: classes.tHead }} />
            </TableListRow>
            {this.getAdvisoryListXML()}
          </TableList>
        </div>
        <CustomModal
          showModal={showModal}
          closeModal={() => this.toggleModal(false)}
        >
          <div className={classes.rootIframe}>
            <span
              className="close-times"
              onClick={() => this.toggleModal(false)}
            >
              &times;
            </span>
            <iframe src={modalUrl} height="100%" width="100%" title="pdf" />
          </div>
        </CustomModal>
      </Fragment>
    );
  }
}

export default withCookies(injectSheet(styles)(TravelAdvisory));
