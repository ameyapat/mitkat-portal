import React, { Component } from 'react';
import CustomEditor from '../../../../ui/customEditor';
import { TextField } from '@material-ui/core';
import './style.scss';
import clsx from 'clsx';
import AlertDialog from '../../../../ui/alertDialog';
import injectSheet from "react-jss";
import style from "./style";

@injectSheet(style)
class EditStatesForm extends Component{

    state = {
        showAlert: false,
    }

    handleClose = () => {
        this.setState({showAlert: true});
    }

    render(){
        const { resources, implementedMeasure, classes } = this.props;
        const { showAlert } = this.state;

        return(
            <div className="edit-states-form">
                <CustomEditor                                                             
                    type={'implementedMeasure'}                                                 
                    editorState={implementedMeasure || ''}                                                
                    onEditorStateChange={this.props.onEditorStateChange}
                />
                 <TextField
                    id={'sources-corona'}
                    label={'Sources'}
                    multiline
                    fullWidth                    
                    rowsMax="6"
                    value={resources || ''}
                    onChange={ (e) => this.props.handleInputChange(e.target.value, 'resources')}
                    margin="normal"
                    variant="outlined"
                    InputLabelProps={{
                        shrink: true,
                        classes: {
                          root: classes.rootLabel
                        }
                    }}
                    InputProps={{
                        maxLength: 5000,
                        classes: {
                            root: classes.rootInput,
                            input: classes.rootInput,
                            notchedOutline: classes.notchedOutline,
                        },
                        endAdornment: null
                    }}
                />
                <div>
                    <button 
                        // disabled={resources.length || implementedMeasure.length === 0}
                        className={clsx("btn--addTitle", "submit--btn--close")} 
                        onClick={() => this.handleClose()}
                    >
                        Cancel
                    </button>
                    <button 
                        // disabled={resources.length || implementedMeasure.length === 0}
                        className={clsx("btn--addTitle", "submit--btn--stateForm")} 
                        onClick={() => this.props.postStateDetails()}
                    >
                        Submit
                    </button>
                </div>
                {
                    showAlert &&
                    <AlertDialog 
                        isOpen={showAlert}
                        title="Cancel Edit" 
                        description="You are about to cancel editing an event, are you sure?" 
                        parentEventHandler={() => this.props.closeModal()} 
                        handleClose={() => this.setState({showAlert: false})} 
                    />
                }
            </div>
        )
    }
}

export default EditStatesForm;