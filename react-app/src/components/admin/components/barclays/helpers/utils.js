export function generateObjectPair(type, arr) {
  switch (type) {
    case 'countries':
      const countryArr = [];
      arr.map(item => {
        const countryObj = {};
        countryObj['value'] = item.id;
        countryObj['label'] = item.countryname;
        countryArr.push(countryObj);
      });
      return countryArr;
    case 'cities':
      const cityArr = [];
      arr.map(item => {
        const cityObj = {};
        cityObj['value'] = item.id;
        cityObj['label'] = item.cityname;
        cityArr.push(cityObj);
      });
      return cityArr;
    case 'assets':
      const assetsArr = [];
      arr.map(item => {
        const assetsObj = {};
        assetsObj['value'] = item.id;
        assetsObj['label'] = item.address;
        assetsArr.push(assetsObj);
      });
      return assetsArr;
    case 'riskLevels':
      const riskLevelsArr = [];
      arr.map(item => {
        const riskLevelsObj = {};
        riskLevelsObj['value'] = item.id;
        riskLevelsObj['label'] = item.level;
        riskLevelsArr.push(riskLevelsObj);
      });
      return riskLevelsArr;
    case 'riskCategories':
      const riskCategoriesArr = [];
      arr.map(item => {
        const riskCategoriesObj = {};
        riskCategoriesObj['value'] = item.id;
        riskCategoriesObj['label'] = item.riskcategory;
        riskCategoriesArr.push(riskCategoriesObj);
      });
      return riskCategoriesArr;
    case 'subRiskCategories':
      const subRiskCategoriesArr = [];
      arr.map(item => {
        const subRiskCategoriesObj = {};
        subRiskCategoriesObj['value'] = item.id;
        subRiskCategoriesObj['label'] = item.risksubcategory;
        subRiskCategoriesArr.push(subRiskCategoriesObj);
      });
      return subRiskCategoriesArr;
    default:
      break;
  }
}
