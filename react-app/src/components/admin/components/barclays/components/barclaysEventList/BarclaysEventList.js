import React, { Component } from 'react';
import Empty from '../../../../../ui/empty';
import Tablelist from '../../../../../ui/tablelist';
import TableListCell from '../../../../../ui/tablelist/TableListCell';
import TableListRow from '../../../../../ui/tablelist/TableListRow';
import BarclaysDatePicker from '../barclaysDatePicker';
import { Button } from '@material-ui/core';
import './style.scss';
import moment from 'moment';
import Wrapper from '../wrapper';
import { connect } from 'react-redux';
@connect(state => {
  return {
    appState: state.appState,
  };
})
class BarclaysEventList extends Component {
  renderEventList = () => {
    const { eventList } = this.props;
    return eventList.map(event => (
      <TableListRow>
        <TableListCell value={event.eventdate} />
        <TableListCell value={event.title} />
        <TableListCell value={event.country} />
        <TableListCell value={event.city} />
        <TableListCell>
        <Button
            title="Image Upload"
            className="btn btnUpload"
            onClick={() =>
              this.props.handleUploadEventImg({
                selectedEventId: event.id,
              })
            }
            
          >
            <i class="fas fa-upload"></i>
          </Button>
          <Button
            title="Edit Event"
            className="btn btnEdit"
            onClick={() =>
              this.props.handleEditEvent({
                isEdit: true,
                selectedEventId: event.id,
              })
            }
          >
            <i class="fas fa-edit"></i>
          </Button>
          <Button 
            title="Delete Event"
            className="btn btnDelete"
            onClick={() =>
              this.props.handleDeleteEvent({
                selectedEventId: event.id,
              })
            }
          >
            <i class="far fa-trash-alt"></i>
          </Button>
        </TableListCell>
      </TableListRow>
    ));
  };

  renderEventListHeader = () => {
    return (
      <TableListRow>
        <TableListCell value={'Event Date'} />
        <TableListCell value={'Title'} />
        <TableListCell value={'Country'} />
        <TableListCell value={'City'} />
        <TableListCell value={'#'} />
      </TableListRow>
    );
  };

  handleDateChange = (type, value) => this.props.handleDateChange(type, value);

  render() {
    const { eventList, reportingDate } = this.props;
    return (
      <div id="barclaysEventList">
        <Wrapper classes={'date-picker-wrapper'}>
          <h2>Event List </h2>
          <div>
            <BarclaysDatePicker
              label={'Enter Reporting Date'}
              value={reportingDate}
              type={'reportingDate'}
              handleDateChange={this.handleDateChange}
            />
            <Button
              className="btn btnSubmit"
              onClick={() => this.props.handleGetEventList()}
            >
              Submit
            </Button>
          </div>
        </Wrapper>
        <div className="barclaysEventList__Box">
          {eventList.length > 0 && this.renderEventListHeader()}
          {eventList.length > 0 ? (
            <Tablelist classes={{ root: '' }}>
              {this.renderEventList()}
            </Tablelist>
          ) : (
            <Empty title="No event found" />
          )}
        </div>
      </div>
    );
  }
}

export default BarclaysEventList;
