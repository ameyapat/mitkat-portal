export default {
    rootLabel: {
        color: 'rgb(0,0,0) !important',
    },
    rootInput: {
        color: 'rgb(0,0,0) !important',
    },
    underline:{
        '&:after': {
            borderBottom: '2px solid #777 !important',
          }
    },
}