import React from 'react';
import './style.scss';

const NavListItem = ({ title, icon, type, parentHandler, isSelected }) => {
  return (
    <li
      className={`${isSelected && 'active'}`}
      onClick={() => parentHandler(type)}
    >
      <p>
        <span className="list__icon">
          <i className={icon}></i>
        </span>
        {title}
      </p>
    </li>
  );
};

export default NavListItem;
