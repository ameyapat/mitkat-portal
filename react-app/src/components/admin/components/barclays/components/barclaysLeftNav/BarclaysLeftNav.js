import React, { Component } from 'react';
import { nav } from '../../constants';
import NavListItem from './NavListItem';
import './style.scss';

class BarclaysLeftNav extends Component {
  getEventList = () => {
    return nav.map((item, index) => {
      const { handleRoute, handleOpenModal } = this.props;
      return (
        <NavListItem
          key={index}
          title={item.title}
          isSelected={item.isActive}
          icon={item.icon}
          type={item.type}
          parentHandler={() =>
            item.isRoute === true
              ? handleRoute(item.to, item.type)
              : handleOpenModal(item.type)
          }
        />
      );
    });
  };

  render() {
    return <ul className="barclayEventList">{this.getEventList()}</ul>;
  }
}

export default BarclaysLeftNav;
