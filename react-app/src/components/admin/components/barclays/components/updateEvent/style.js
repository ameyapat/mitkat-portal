export default {
  singleSelect: {
    width: '100%',
  },
  rootText: {
    margin: '15px 0',
    color: 'var(--whiteHighEmp) !important',
  },
  listOfItems: {
    margin: 0,
    borderBottom: '1px solid #f1f1f1',
    paddingBottom: 10,
    fontSize: 12,
    color: '#555',
    fontWeight: 'normal',
  },
  size: {
    width: 40,
    height: 30,
  },
  sizeIcon: {
    fontSize: 20,
    color: 'rgba(0, 119, 182, 0.87)',
  },
  rootLabel: {
    color: 'rgba(0,0,0, 0.67) !important',
  },
  rootInput: {
    color: 'rgba(0,0,0, 0.87) !important',
  },
  underline: {
    '&:after': {
      borderBottom: '2px solid #000 !important',
    },
  },
  notchedOutline: {
    borderColor: 'var(--whiteHighEmp) !important ',
  },
  label: {
    color: '#000 !important',
  },
  radioLabel: {
    color: '#000 !important',
  },
  rootPaper: {
    backgroundColor: 'var(--whiteHighEmp) !important ',
  },
  rootText: {
    color: '#000 !important',
  },
};
