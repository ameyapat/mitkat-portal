import React, { Component } from 'react';
import './style.scss';
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';
import Select from 'react-select';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';

import MultiSelect from '../../../../../ui/multiSelect';
import MultipleLocations from '../../../multipleLocations';
import CustomMap from '../../../../../ui/customMap';
import CustomLabel from '../../../../../ui/customLabel';
import AlertDialog from '../../../../../ui/alertDialog';
import MultipleLocationList from '../../../../../ui/multipleLocationList';
import BarclaysDatePicker from '../barclaysDatePicker';
import BarclaysTextField from '../barclaysTextField';

import { withRouter } from 'react-router-dom';
import { withCookies } from 'react-cookie';

import {
  filterArrObjects,
  filterByLatLng,
} from '../../../../../../helpers/utils';
import FormValidator from '../../../../../../helpers/validation/FormValidator';
import { generateObjectPair } from '../../helpers/utils';

import { toggleToast } from '../../../../../../actions';
import { updateMultipleLocation } from '../../../../../../actions/multipleLocationActions';

import style from './style';
import validationRules from './validationRules';

import injectSheet from 'react-jss';
import { connect } from 'react-redux';

import {
  getCountryListBarclays,
  getCityListBarclays,
  getAssetListBarclays,
  getRiskLevelBarclays,
  getRiskCategoryBarclays,
  getSubRiskCategoryBarclays,
  updateEventBarclays,
  getEventBarclays,
} from '../../../../../../requestor/barclays/requestor';

import { errorMsg } from '../Barclays';

const customStyles = {
  control: provided => ({
    ...provided,
    backgroundColor: '#fff ',
    color: '#000',
  }),
  option: (provided, state) => ({
    ...provided,
    zIndex: '99999',
  }),
  placeholder: provided => ({
    ...provided,
    color: '#000',
  }),
  singleValue: provided => ({
    ...provided,
    color: '#000',
  }),
};

@withCookies
@withRouter
@injectSheet(style)
@connect(
  state => {
    return {
      refreshEvents: state.appState.refreshEvents,
      multipleLocations: state.multipleLocations,
      appState: state.appState,
    };
  },
  dispatch => {
    return { dispatch };
  },
)
class UpdateEvent extends Component {
  validator = new FormValidator(validationRules);
  submitted = false;

  state = {
    validation: this.validator.valid(),
    showAlert: false,
    location: {
      lat: '',
      lng: '',
    },
    eventTitle: '',
    bluf: '',
    impact: '',
    actions: '',
    sources: '',
    coordinates: [],
    isLocationSpecific: false,
    selectedCountries: [],
    selectedCities: [],
    selectedAssets: [],
    selectedRiskLevel: {},
    selectedCategory: {},
    selectedSubCategory: {},
    eventDate: new Date(),
    reportingDate: new Date(),
    validityDate: new Date(),
    countries: [],
    cities: [],
    assets: [],
    riskLevels: [],
    riskCategories: [],
    subRiskCategories: [],
    eventId: 0,
  };

  componentDidMount() {
    getCountryListBarclays().then(countries =>
      this.setState({ countries: generateObjectPair('countries', countries) }),
    );
    getCityListBarclays().then(cities =>
      this.setState({ cities: generateObjectPair('cities', cities) }),
    );
    getAssetListBarclays().then(assets =>
      this.setState({ assets: generateObjectPair('assets', assets) }),
    );
    getRiskLevelBarclays().then(riskLevels => {
      riskLevels = generateObjectPair('riskLevels', riskLevels);

      this.setState({
        riskLevels,
        selectedRiskLevel: riskLevels[0],
      });
    });
    getRiskCategoryBarclays().then(riskCategories => {
      riskCategories = generateObjectPair('riskCategories', riskCategories);
      this.setState(
        {
          riskCategories,
          selectedCategory: this.props.isEdit ? {} : riskCategories[0],
        },
        () => {
          !this.props.isEdit && this.getInitialSubCategories();
        },
      );
    });

    if (this.props.isEdit) {
      this.getEventDetails();
    }
  }

  componentDidUpdate(prevProps) {
    const prevMultipleLocation =
      prevProps.multipleLocations.selectedMultipleLocations;
    const currMultipleLocation = this.props.multipleLocations
      .selectedMultipleLocations;
    if (currMultipleLocation !== prevMultipleLocation) {
      this.setState({ coordinates: currMultipleLocation });
    }
  }

  getEventDetails = () => {
    getEventBarclays(this.props.selectedEventId).then(eventDetails => {
      this.setInitialEventDetails(eventDetails);
    });
  };

  setInitialEventDetails = eventDetails => {
    this.setState(
      {
        eventTitle: eventDetails.title,
        bluf: eventDetails.bluf,
        impact: eventDetails.impact,
        actions: eventDetails.action,
        sources: eventDetails.sources,
        isLocationSpecific: eventDetails.locationSpecific,
        selectedCountries: generateObjectPair(
          'countries',
          eventDetails.countries,
        ),
        selectedCities: generateObjectPair('cities', eventDetails.cities),
        selectedAssets: generateObjectPair('assets', eventDetails.assets),
        selectedRiskLevel: {
          value: eventDetails.risklevel.id,
          label: eventDetails.risklevel.level,
        },
        selectedCategory: {
          value: eventDetails.riskcategory.id,
          label: eventDetails.riskcategory.riskcategory,
        },
        eventDate: eventDetails.eventdate,
        reportingDate: eventDetails.reportingdate,
        validityDate: eventDetails.validitydate,
        eventId: eventDetails.id,
      },
      () => {
        this.getReverseGeoCode(eventDetails.locations);
        this.fetchSubCategoryByRisk(
          eventDetails.riskcategory.id,
          eventDetails.risksubcategory.id,
          eventDetails.risksubcategory.risksubcategory,
        );
      },
    );
  };

  getReverseGeoCode = locations => {
    locations.map(item => {
      this.fetchFormattedAddress(item.latitude, item.longitude);
    });
  };

  fetchFormattedAddress = (lat, lng) => {
    let reqObj = {
      url: `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&key=AIzaSyCYOTLyn6xbysArlvlW4zpxnH9p6H3An08&v=3.exp`,
      method: 'GET',
      isAuth: false,
    };

    const newCoordinateObj = {};
    const newCoordinateArr = [];

    fetch(reqObj.url)
      .then(data => data.json())
      .then(res => {
        let address = res.results[0].formatted_address;
        newCoordinateObj.lat = lat;
        newCoordinateObj.lng = lng;
        newCoordinateObj.formattedAddress = address;
        newCoordinateObj.id = res.results[0].place_id;

        newCoordinateArr.push(newCoordinateObj);

        return newCoordinateArr;
      })
      .then(newCoordinateArr => {
        this.setState(
          { coordinates: [...this.state.coordinates, ...newCoordinateArr] },
          () => {
            this.props.dispatch(updateMultipleLocation(this.state.coordinates));
          },
        );
      })
      .catch(e => console.log(e));
  };

  getInitialSubCategories = () => {
    const { riskCategories } = this.state;
    const initId = riskCategories[0].value;

    this.fetchNewSubCategory(initId);
  };

  fetchNewSubCategory = id => {
    getSubRiskCategoryBarclays(id).then(subRiskCategories => {
      subRiskCategories = generateObjectPair(
        'subRiskCategories',
        subRiskCategories,
      );
      this.setState({
        subRiskCategories,
        selectedSubCategory: subRiskCategories[0],
      });
    });
  };

  handleInputChange = (e, type) => this.setState({ [type]: e.target.value });

  handleDateChange = (type, value) => this.setState({ [type]: value });

  handleSingleSelect = (value, type) => {
    this.setState({ [type]: value });
  };

  handleSingleSelectSub = (value, type) => {
    this.setState({ [type]: value }, () => {
      this.fetchSubCategory(value);
    });
  };

  fetchSubCategory = item => {
    this.fetchNewSubCategory(item.value);
  };

  fetchSubCategoryByRisk = (riskcategory, subriskcategory, label) => {
    getSubRiskCategoryBarclays(riskcategory).then(subRiskCategories => {
      subRiskCategories = generateObjectPair(
        'subRiskCategories',
        subRiskCategories,
      );
      this.setState({
        subRiskCategories,
        selectedSubCategory: { value: subriskcategory, label },
      });
    });
  };

  handleEventSubmit = e => {
    e.preventDefault();
    let {
      history,
      multipleLocations: { selectedMultipleLocations },
      dispatch,
      isEdit,
    } = this.props;
    this.validator = new FormValidator(validationRules);
    let validation = this.validator.validate(this.state);
    this.setState({ validation });
    this.submitted = true;

    if (validation.isValid) {
      let {
        eventTitle,
        bluf,
        impact,
        actions,
        sources,
        isLocationSpecific,
        selectedCountries,
        selectedCities,
        selectedAssets,
        selectedRiskLevel,
        selectedCategory,
        selectedSubCategory,
        eventDate,
        reportingDate,
        validityDate,
        eventId,
      } = this.state;

      const eventDetails = {
        id: isEdit ? eventId : 0,
        title: eventTitle,
        bluf: bluf,
        impact: impact,
        action: actions,
        sources: sources,
        risklevel: selectedRiskLevel.value,
        riskcategory: selectedCategory.value,
        risksubcategory: selectedSubCategory.value,
        locationSpecific: isLocationSpecific,
        reportingdate: reportingDate,
        eventdate: eventDate,
        validitydate: validityDate,
        countries: filterArrObjects(selectedCountries),
        cities: filterArrObjects(selectedCities),
        assets: filterArrObjects(selectedAssets),
        locations: filterByLatLng(selectedMultipleLocations),
      };

      updateEventBarclays(eventDetails).then(data => {
        if (data && data.status == 200) {
          let toastObj = {
            toastMsg: data.message,
            showToast: true,
          };
          this.props.handleGetEventList();
          this.handleCloseModal();
          dispatch(toggleToast(toastObj));
          dispatch(updateMultipleLocation([]));
        } else {
          let toastObj = {
            toastMsg: errorMsg,
            showToast: true,
          };
          dispatch(toggleToast(toastObj));
        }
        this.props.handleToggleIsEdit(false);
      });
    }
  };

  riskLevelsXML = () => {
    let { riskLevels } = this.state;
    let riskLevelXML = [];
    riskLevelXML = riskLevels.map(item => (
      <MenuItem key={item.id} value={item.id}>
        {item.level}
      </MenuItem>
    ));
    return riskLevelXML;
  };

  riskCategoriesXML = () => {
    let { riskCategories } = this.state;
    let riskCategoryXML = [];
    riskCategoryXML = riskCategories.map(item => (
      <MenuItem key={item.id} value={item.id}>
        {item.riskcategory}
      </MenuItem>
    ));
    return riskCategoryXML;
  };

  handleMultiSelect = (value, type) => {
    this.setState({ [type]: value });
  };

  handleShowAlert = () => {
    this.setState({ showAlert: true });
  };

  handleCloseAlert = () => {
    this.setState({ showAlert: false });
  };

  handleCloseModal = () => {
    this.setState({ showAlert: false });
    this.props.closeEventModal('createEvent');
  };

  handleIsLocSpecific = () => {
    this.setState({ isLocationSpecific: !this.state.isLocationSpecific });
  };

  toggleMultipleLocationModal = showMultipleLocationsModal =>
    this.setState({ showMultipleLocationsModal });

  render() {
    let {
      classes,
      multipleLocations: { selectedMultipleLocations },
      isEdit,
    } = this.props;
    let {
      riskLevels,
      riskCategories,
      subRiskCategories,
      countries,
      cities,
      assets,
      selectedCountries,
      selectedCities,
      selectedAssets,
      validityDate,
      reportingDate,
      eventDate,
      eventTitle,
      bluf,
      impact,
      actions,
      sources,
      selectedCategory,
      selectedRiskLevel,
      selectedSubCategory,
      isLocationSpecific,
      showAlert,
    } = this.state;
    let validation = this.submitted
      ? this.validator.validate(this.state)
      : this.state.validation;

    return (
      <div id="updateBarclaysEvent" className="createEvent__innerBox">
        <h3 className="updateBarclaysEvent__title">
          <span className="updateBarclaysEvent__title-icon">
            <i className="fas fa-calendar-week"></i>
          </span>
          Create a new event
        </h3>
        <div className="field--create--event">
          {
            // multiple location begins
          }
          <section className="modal--section">
            <section id="reactGooglePlaces" className="google--places">
              <button
                className="addLocationBtn"
                onClick={() => this.toggleMultipleLocationModal(true)}
              >
                <span className="addLocationBtn__icon">
                  <i className="fas fa-map-marker-alt"></i>
                </span>
                <span className="addLocationBtn__text">
                  {selectedMultipleLocations.length > 0
                    ? 'Add/Update Location'
                    : 'Add New Location*'}
                </span>
              </button>
              {validation.coordinates.isInvalid && (
                <span className="error--text">
                  {validation.coordinates.message}
                </span>
              )}
              <MultipleLocations
                showModal={this.state.showMultipleLocationsModal}
                closeMultipleLocationModal={this.toggleMultipleLocationModal}
              >
                <div
                  id="multipleLocationMap"
                  className="modal__inner"
                  style={{ overflow: 'hidden' }}
                >
                  <CustomMap />
                </div>
              </MultipleLocations>
              {selectedMultipleLocations.length > 0 && (
                <MultipleLocationList
                  selectedMultipleLocations={selectedMultipleLocations}
                  type="detail"
                />
              )}
            </section>
          </section>
          {
            // multiple location ends
          }
          {
            // select countries, cities & assets begin
          }
          <section className="modal--section">
            <div className="twoColWrap multi--selects">
              <div className="col-2">
                <CustomLabel
                  title="Countries"
                  classes="select--label"
                  isRequired={false}
                />
                <MultiSelect
                  value={selectedCountries}
                  placeholder={'Select Countries'}
                  parentEventHandler={value =>
                    this.handleMultiSelect(value, 'selectedCountries')
                  }
                  options={countries}
                  hasCustomStyles={true}
                  customStyles={customStyles}
                />
                {validation.selectedCountries.isInvalid && (
                  <span className="error--text">
                    {validation.selectedCountries.message}
                  </span>
                )}
              </div>
              <div className="col-2">
                <CustomLabel
                  title="Cities"
                  classes="select--label"
                  isRequired={false}
                />
                <MultiSelect
                  value={selectedCities}
                  placeholder={'Select Cities'}
                  parentEventHandler={value =>
                    this.handleMultiSelect(value, 'selectedCities')
                  }
                  options={cities}
                  hasCustomStyles={true}
                  customStyles={customStyles}
                />
              </div>
              <div className="col-2">
                <CustomLabel
                  title="Assets"
                  classes="select--label"
                  isRequired={false}
                />
                <MultiSelect
                  value={selectedAssets}
                  placeholder={'Select Assets'}
                  parentEventHandler={value =>
                    this.handleMultiSelect(value, 'selectedAssets')
                  }
                  options={assets}
                  hasCustomStyles={true}
                  customStyles={customStyles}
                />
              </div>
            </div>
            {
              // select countries, cities & assets begin
            }
          </section>
          {/** Event Date starts */}
          <section className="modal--section">
            <h2 className="event-head--title">
              <i className="icon-uniE9F4" /> Tracker Date
              <span className="asterisk">*</span>
            </h2>
            <div className="col-2">
              <div className="dateWrapper">
                <div className="date">
                  <BarclaysDatePicker
                    label={'Enter Reporting Date'}
                    value={reportingDate}
                    type={'reportingDate'}
                    handleDateChange={this.handleDateChange}
                  />
                </div>
                <div className="date">
                  <BarclaysDatePicker
                    label={'Enter Event Date'}
                    value={eventDate}
                    type={'eventDate'}
                    handleDateChange={this.handleDateChange}
                  />
                </div>
                <div className="date">
                  <BarclaysDatePicker
                    label={'Enter Validity Date'}
                    value={validityDate}
                    type={'validityDate'}
                    handleDateChange={this.handleDateChange}
                  />
                </div>
              </div>
            </div>
          </section>
          {/** Event Date ends */}
          {/**event impact analysis new starts */}
          <section className="modal--section">
            <h2 className="event-head--title">
              <i className="icon-uniE90E" /> Event Details
            </h2>
            {/**event title starts */}
            <div className="event--title">
              <BarclaysTextField
                error={validation.eventTitle.isInvalid}
                id="eventTitle"
                label="Event Title"
                value={eventTitle}
                type={'eventTitle'}
                onChange={this.handleInputChange}
                defaultValue="Enter Event Title"
                classes={classes}
                required
              />
              {validation.eventTitle.isInvalid && (
                <span className="error--text">
                  {validation.eventTitle.message}
                </span>
              )}
            </div>
            {/** event title ends */}
            {/** bluf starts */}
            <div className="event--title">
              <BarclaysTextField
                id="bluf"
                label="BLUF"
                value={bluf}
                type={'bluf'}
                onChange={this.handleInputChange}
                defaultValue="Enter BLUF"
                classes={classes}
              />
            </div>
            {/** bluf ends */}
            {/** impact starts */}
            <div className="event--title">
              <BarclaysTextField
                id="impact"
                label="IMPACT"
                value={impact}
                type={'impact'}
                onChange={this.handleInputChange}
                defaultValue="Enter IMPACT"
                classes={classes}
              />
            </div>
            {/** impact ends */}
            {/** actions starts */}
            <div className="event--title">
              <BarclaysTextField
                id="actions"
                label="Actions"
                value={actions}
                type={'actions'}
                onChange={this.handleInputChange}
                defaultValue="Enter Actions"
                classes={classes}
              />
            </div>
            {/** actions ends */}
            {/** sources starts */}
            <div className="event--title">
              <BarclaysTextField
                id="sources"
                label="Sources"
                value={sources}
                type={'sources'}
                onChange={this.handleInputChange}
                defaultValue="Enter Sources"
                classes={classes}
              />
            </div>
            {/** sources ends */}
          </section>
          {/**event impact analysis new ends */}
          {/** level types begin */}
          <section className="modal--section">
            <h2 className="event-head--title">
              <i className="icon-uniE99D" /> Event Type
            </h2>
            <div className="twoColWrap single--selects">
              <div className="col-3">
                <CustomLabel
                  title="Severity Level"
                  classes="select--label"
                  isRequired={true}
                />
                <Select
                  defaultValue={selectedRiskLevel}
                  className="select--options"
                  value={selectedRiskLevel}
                  placeholder={'Severity Level'}
                  onChange={value =>
                    this.handleSingleSelect(value, 'selectedRiskLevel')
                  }
                  options={riskLevels.length > 0 ? riskLevels : []}
                  styles={customStyles}
                />
                {validation.selectedRiskLevel.isInvalid && (
                  <span className="error--text">
                    {validation.selectedRiskLevel.message}
                  </span>
                )}
              </div>
              <div className="col-3">
                <CustomLabel
                  title="Risk Category"
                  classes="select--label"
                  isRequired={true}
                />
                <Select
                  defaultValue={selectedCategory}
                  className="select--options"
                  value={selectedCategory}
                  placeholder={'Risk Category'}
                  onChange={value =>
                    this.handleSingleSelectSub(value, 'selectedCategory')
                  }
                  options={riskCategories.length > 0 ? riskCategories : []}
                  styles={customStyles}
                />
                {validation.selectedCategory.isInvalid && (
                  <span className="error--text">
                    {validation.selectedCategory.message}
                  </span>
                )}
              </div>
              <div className="col-3">
                <CustomLabel
                  title="Sub Risk Category"
                  classes="select--label"
                  isRequired={true}
                />
                <Select
                  defaultValue={selectedSubCategory}
                  className="select--options"
                  value={selectedSubCategory}
                  placeholder={'Sub Risk Category'}
                  onChange={value =>
                    this.handleSingleSelect(value, 'selectedSubCategory')
                  }
                  options={
                    subRiskCategories.length > 0 ? subRiskCategories : []
                  }
                  styles={customStyles}
                />
              </div>
            </div>
          </section>
          {/** level type ends */}
          {/** is location begins */}
          <section className="modal--section">
            <FormControlLabel
              classes={{
                label: classes.label,
              }}
              control={
                <Checkbox
                  className={classes.size}
                  checked={isLocationSpecific}
                  onChange={this.handleIsLocSpecific}
                  value={isLocationSpecific}
                  icon={
                    <CheckBoxOutlineBlankIcon className={classes.sizeIcon} />
                  }
                  checkedIcon={<CheckBoxIcon className={classes.sizeIcon} />}
                />
              }
              label={'Is story location specific?'}
            />
          </section>
          {/** is location ends */}
        </div>
        <div className="actionables">
          <Button
            onClick={e => this.handleShowAlert()}
            variant="contained"
            color="default"
          >
            Cancel
          </Button>
          <Button
            onClick={e => this.handleEventSubmit(e)}
            style={{ marginLeft: 20, backgroundColor: '#002e4a' }}
            variant="contained"
            color="secondary"
          >
            {isEdit ? 'Update' : 'Create'}
          </Button>
        </div>
        {showAlert && (
          <AlertDialog
            isOpen={showAlert}
            title="Cancel Changes"
            description="You are about to cancel event changes, you won't be able to get them back, are you sure?"
            parentEventHandler={e => this.handleCloseModal()}
            handleClose={() => this.handleCloseAlert()}
            rootPaper={classes.rootPaper}
            rootText={classes.rootText}
          />
        )}
      </div>
    );
  }
}

export default UpdateEvent;
