const validationRules = [
  {
    field: 'eventTitle',
    method: 'isEmpty',
    validWhen: false,
    message: 'Event title is required',
  },
  {
    field: 'coordinates',
    method: 'isEmpty',
    validWhen: false,
    message: 'Atleast one location is required!',
  },
  {
    field: 'eventDate',
    method: 'isEmpty',
    validWhen: false,
    message: 'Event date is required',
  },
  {
    field: 'validityDate',
    method: 'isEmpty',
    validWhen: false,
    message: 'Validity date is required',
  },
  {
    field: 'selectedRiskLevel',
    method: 'isEmpty',
    validWhen: false,
    message: 'RiskLevel is required',
  },
  {
    field: 'selectedCategory',
    method: 'isEmpty',
    validWhen: false,
    message: 'Category is required',
  },
  {
    field: 'selectedCountries',
    method: 'isEmpty',
    validWhen: false,
    message: 'At least one Country is required',
  },
];

export default validationRules;
