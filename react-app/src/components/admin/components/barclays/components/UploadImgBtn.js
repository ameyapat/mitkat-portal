import React, { useState, useEffect } from 'react';
import { withCookies } from 'react-cookie';
import { API_ROUTES } from '../../../../../helpers/http/apiRoutes';
import store from '../../../../../store';
import { toggleToast } from '../../../../../actions';

const UploadImgBtn = ({
  selectedEventId: getEventId,
  cookies,
  closeUploadEventImg,
}) => {
  const [reportimage, setReportimage] = useState();
  const [eventID, setEventId] = useState();

  useEffect(() => {
    setEventId(getEventId.selectedEventId);
    return;
  }, []);

  function formSubmit(event) {
    event.preventDefault();
    const data = new FormData();
    const authToken = cookies.get('authToken');
    data.append('reportimage', reportimage);
    /** Need to refactor the code below */
    fetch(`${API_ROUTES.uploadEventImg}/${eventID}`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${authToken}`,
      },
      body: data,
    })
      .then(res => {
        if (res.status == 201) {
          let toastObj = {
            toastMsg: 'Image uploaded successfully!',
            showToast: true,
          };
          store.dispatch(toggleToast(toastObj));
          closeUploadEventImg();
        } else {
          let toastObj = {
            toastMsg: 'Something went wrong!',
            showToast: true,
          };
          store.dispatch(toggleToast(toastObj));
        }
      })
      .catch(e => console.log(e, '----Error'));
  }

  return (
    <>
      <div>
        <form className="contact-form px-3" onSubmit={formSubmit}>
          <input
            type="file"
            name="reportImg"
            onChange={event => setReportimage(event.target.files[0])}
          />
          <input
            type="submit"
            name="btnSubmit"
            className="btn-submit"
            value="Upload"
          />
        </form>
      </div>
    </>
  );
};

export default withCookies(UploadImgBtn);
