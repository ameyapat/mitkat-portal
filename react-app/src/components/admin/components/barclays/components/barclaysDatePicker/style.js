export default {
  rootLabelLight: {
    color: 'rgba(0,0,0, 0.67) ',
  },
  rootInputLight: {
    color: 'rgba(0,0,0, 0.87) ',
  },
  notchedOutlineLight: {
    borderColor: '#757575 ',
  },
};
