import React from 'react';
import { DatePicker } from 'material-ui-pickers';
import injectSheet from 'react-jss';
import styles from './style';

const BarclaysDatePicker = ({
  label,
  value,
  type,
  classes,
  rootLabelLight,
  rootInputLight,
  underline,
  notchedOutlineLight,
  handleDateChange,
  darkTheme = false,
}) => {
  return (
    <DatePicker
      label={label}
      value={value}
      fullWidth
      onChange={value => handleDateChange(type, value)}
      animateYearScrolling
      InputLabelProps={{
        shrink: true,
        classes: { 
          root: darkTheme ? rootLabelLight : classes.rootLabelLight,
        },
      }}
      InputProps={{
        classes: { 
          root: darkTheme ? rootInputLight : classes.rootInputLight,
          input: darkTheme ? rootInputLight : classes.rootInputLight,
          notchedOutline: darkTheme ? notchedOutlineLight : classes.notchedOutlineLight,
          underline: darkTheme ? underline : classes.underline,
        },
        endAdornment: null,
      }}
    />
  );
};

export default injectSheet(styles)(BarclaysDatePicker);
