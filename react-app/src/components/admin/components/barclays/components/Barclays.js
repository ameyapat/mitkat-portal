import React, { Component } from 'react';
import CustomModal from '../../../../ui/modal';
import BarclaysLeftNav from './barclaysLeftNav';
import BarclaysRightNav from './barclaysRightNav';
import BarclaysHeader from './barclaysHeader';
import styles from './style';
import injectSheet from 'react-jss/lib/injectSheet';
import Button from '@material-ui/core/Button';
import UpdateEvent from './updateEvent';
import {
  deleteEventBarclays,
  getEventListBarclays,
} from '../../../../../requestor/barclays/requestor';
import { toggleToast, toggleIsFetch } from '../../../../../actions';
import { updateMultipleLocation } from '../../../../../actions/multipleLocationActions';
import './style.scss';
import Wrapper from './wrapper';
import { withRouter } from 'react-router-dom';
import barclaysBg from '../../../../../assets/barclays-bg.png';
import { connect } from 'react-redux';
import { withCookies } from 'react-cookie';
import moment from 'moment';
import { toggleUploadModal } from '../../../../../actions/barclaysActions';
import UploadImgBtn from './UploadImgBtn';
export const errorMsg = 'Something went wrong, Please try again';

@injectSheet(styles)
@withRouter
@connect(
  state => {
    return {
      barclays: state.barclays,
    };
  },
  dispatch => {
    return { dispatch };
  },
)
@withCookies
class Barclays extends Component {
  state = {
    createEvent: false,
    isEdit: false,
    selectedEventId: null,
    reportingDate: moment(new Date()).format('YYYY-MM-DD'),
    eventList: [],
  };

  componentDidMount() {
    this.handleGetEventList();
  }

  handleRoute = (to, type) => {
    let path = this.props.match.url;
    this.setState({ selectedItem: type });
    this.props.history.push(`${path}${to}`);
  };

  handleOpenModal = type => {
    this.setState({ [type]: true });
  };

  handleCloseModal = type => {
    this.setState({ [type]: false, isEdit: false }, () =>
      this.props.dispatch(updateMultipleLocation([])),
    );
  };

  handleLogout = () => {
    let { cookies, dispatch } = this.props;
    cookies.remove('authToken');
    cookies.remove('userRole');

    dispatch(toggleIsFetch(true));

    setTimeout(() => {
      dispatch(toggleIsFetch(false));
      this.props.history.push('/');
    }, 2000);
  };

  handleEditEvent = ({ isEdit, selectedEventId }) => {
    this.setState({ isEdit, selectedEventId }, () => {
      this.handleOpenModal('createEvent');
    });
  };

  handleDeleteEvent = ({ selectedEventId }) => {
    const { dispatch } = this.props;
    deleteEventBarclays(selectedEventId).then(response => {
      if (response.success) {
        let toastObj = {
          toastMsg: response.message,
          showToast: true,
        };

        this.handleGetEventList();
        dispatch(toggleToast(toastObj));
      } else {
        let toastObj = {
          toastMsg: errorMsg,
          showToast: true,
        };
        dispatch(toggleToast(toastObj));
      }
    });
  };

  handleUploadEventImg = selectedEventId => {
    this.props.dispatch(toggleUploadModal(true));
    this.setState({ selectedEventId });
  };

  closeUploadEventImg = () => {
    this.props.dispatch(toggleUploadModal(false));
  };

  handleToggleIsEdit = value => this.setState({ isEdit: value });

  handleGetEventList = () => {
    const { reportingDate } = this.state;

    getEventListBarclays(reportingDate).then(eventList =>
      this.setState({ eventList }),
    );
  };

  handleDateChange = (type, value) =>
    this.setState({ [type]: moment(value).format('YYYY-MM-DD') });

  render() {
    const {
      isEdit,
      selectedEventId,
      createEvent,
      eventList,
      reportingDate,
    } = this.state;

    const { showUploads } = this.props.barclays;

    return (
      <>
        <BarclaysHeader handleLogout={this.handleLogout} />
        <Wrapper classes="is-grid">
          <Wrapper classes="left-grid">
            <img src={barclaysBg} />
            <BarclaysLeftNav
              handleOpenModal={this.handleOpenModal}
              handleRoute={this.handleRoute}
            />
          </Wrapper>
          <Wrapper classes="border-left right-grid">
            <BarclaysRightNav
              handleEditEvent={this.handleEditEvent}
              handleDeleteEvent={this.handleDeleteEvent}
              handleUploadEventImg={this.handleUploadEventImg}
              eventList={eventList}
              handleDateChange={this.handleDateChange}
              reportingDate={reportingDate}
              handleGetEventList={this.handleGetEventList}
              handleToggleIsEdit={this.handleToggleIsEdit}
            />
          </Wrapper>
        </Wrapper>
        {createEvent && (
          <CustomModal showModal={createEvent}>
            <div id="createEvent__Modal" className="modal__inner">
              <UpdateEvent
                closeEventModal={this.handleCloseModal}
                isEdit={isEdit}
                selectedEventId={selectedEventId}
                handleToggleIsEdit={this.handleToggleIsEdit}
                handleGetEventList={this.handleGetEventList}
              />
            </div>
          </CustomModal>
        )}

        <CustomModal showModal={showUploads}>
          <div id="uploadEventImgModal">
            <div className="uploadEventImgModal__header">
              <h1>Upload Image</h1>
              <button className="btn--close" onClick={this.closeUploadEventImg}>
                &times;
              </button>
            </div>
            <div className="uploadEventImgModal__body">
              <UploadImgBtn
                selectedEventId={selectedEventId}
                closeUploadEventImg={this.closeUploadEventImg}
              />
            </div>
          </div>
        </CustomModal>
      </>
    );
  }
}

export default Barclays;
