import React from 'react';
import './style.scss';

const Wrapper = ({ classes, children }) => {
  return <div className={classes}>{children}</div>;
};

export default Wrapper;
