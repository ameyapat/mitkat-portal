import React from 'react';
import TextField from '@material-ui/core/TextField';
import injectSheet from 'react-jss';
import styles from '../style';

const BarclaysTextField = ({
  error,
  id,
  label,
  value,
  onChange,
  type,
  defaultValue,
  classes,
  rootInput,
  rootLabel,
  underline,
  required = false,
  variant,
  fullWidth = true,
  darkTheme = false,
}) => (
  <TextField
    required={required}
    error={error}
    id={id}
    label={label}
    multiline
    fullWidth={fullWidth}
    rows="1"
    rowsMax="3"
    value={value}
    onChange={e => onChange(e, type)}
    defaultValue={defaultValue}
    margin="normal"
    variant={variant}
    InputLabelProps={{
      shrink: true,
      classes: {
        root: darkTheme ? rootLabel : classes.rootLabel,
      },
    }}
    InputProps={{
      maxLength: 5000,
      classes: {
        root: darkTheme ? rootInput : classes.rootInput,
        input: darkTheme ? rootInput : classes.rootInput,
        underline: darkTheme ? underline : classes.underline,
      },
      endAdornment: null,
    }}
  />
);

export default injectSheet(styles)(BarclaysTextField);
