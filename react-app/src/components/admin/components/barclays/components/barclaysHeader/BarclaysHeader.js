import React from 'react';
//assets
import MitkatLogo from '../../../../../../assets/logo.png';
import BarclaysLogo from '../../../../../../assets/barclays.png';
//utils
import { AppBar, Button, Toolbar } from '@material-ui/core';
import injectSheet from 'react-jss/lib/injectSheet';
//style
import styles from './style';

const BarclaysHeader = ({ classes, handleLogout }) => (
  <AppBar
    color="default"
    position="fixed"
    classes={{ root: classes.rootAppBar }}
  >
    <Toolbar classes={{ root: classes.rootToolbar }}>
      <div className={classes.logoWrapper}>
        <img
          src={BarclaysLogo}
          alt="barclays-logo"
          className={classes.barclaysLogo}
        />
        <img
          src={MitkatLogo}
          alt="mitkat-logo"
          className={classes.mitkatLogo}
        />
      </div>
      <Button
        color="default"
        classes={{ root: classes.btnLogout }}
        onClick={handleLogout}
      >
        Logout
      </Button>
    </Toolbar>
  </AppBar>
);

export default injectSheet(styles)(BarclaysHeader);
