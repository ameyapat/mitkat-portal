export default {
  btnLogout: {
    backgroundColor: '#0076b6 !important',
    color: 'var(--whiteHighEmp) !important ',
    '&:hover': {
      backgroundColor: 'rgba(83, 171, 62, 0.8)',
    },
  },
  rootAppBar: {
    backgroundColor: '#eaeaea !important',
  },
  rootToolbar: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  logoWrapper: {
    display: 'flex',
    alignItems: 'center',
  },
  barclaysLogo: {
    width: '150px',
  },
  mitkatLogo: {
    width: '125px',
    maxWidth: '100%',
    borderLeft: '1px solid #d3d3d3',
    paddingLeft: '15px',
    marginLeft: '15px',
  },
};
