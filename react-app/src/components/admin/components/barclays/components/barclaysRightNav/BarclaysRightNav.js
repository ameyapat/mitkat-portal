import React from 'react';
import { Switch, Route } from 'react-router-dom';
import BarclaysEventList from '../barclaysEventList';

const BarclaysRightNav = ({
  handleEditEvent,
  handleDeleteEvent,
  handleUploadEventImg,
  eventList,
  handleDateChange,
  reportingDate,
  handleGetEventList,
  handleToggleIsEdit,
}) => (
  <Switch>
    <Route
      path="/barclays/admin"
      render={props => (
        <BarclaysEventList
          handleEditEvent={handleEditEvent}
          handleDeleteEvent={handleDeleteEvent}
          handleUploadEventImg={handleUploadEventImg}
          eventList={eventList}
          handleDateChange={handleDateChange}
          reportingDate={reportingDate}
          handleGetEventList={handleGetEventList}
          handleToggleIsEdit={handleToggleIsEdit}
        />
      )}
    />
  </Switch>
);

export default BarclaysRightNav;
