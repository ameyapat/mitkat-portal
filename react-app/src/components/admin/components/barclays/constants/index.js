export const nav = [
  {
    id: 1,
    title: 'Events',
    icon: 'fas fa-newspaper',
    to: 'barclays/admin',
    type: 'eventList',
    isNew: false,
    isRoute: false,
    isActive: true,
  },
  {
    id: 2,
    title: 'Create Event',
    icon: 'fas fa-calendar-week',
    to: '',
    type: 'createEvent',
    isNew: false,
    isRoute: false,
    isActive: false,
  },
];
