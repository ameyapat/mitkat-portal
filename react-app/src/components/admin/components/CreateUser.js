import React, { Component } from 'react';
//components @material-ui
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
//hoc
import { withStyles } from '@material-ui/core';
//style
import style from '../style';
import '../sass/createUser.scss';
//constant
import { USER_INPUT } from '../helpers/constants';
import { fetchApi } from '../../../helpers/http/fetch';
import { API_ROUTES } from '../../../helpers/http/apiRoutes';
//
import { connect } from 'react-redux';
import store from '../../../store';
import { toggleToast } from '../../../actions';
import { withCookies } from 'react-cookie';
import FormValidator from '../../../helpers/validation/FormValidator';
import validationRules from './validationRules';
import CustomMessage from '../../ui/customMessage';

@withCookies
@withStyles(style)
@connect(state => {
  return {
    appState: state.appState,
  };
})
class CreateUser extends Component {
  validator = new FormValidator(validationRules);
  submitted = false;

  state = {
    name: '',
    username: '',
    email: '',
    password: '',
    clearance: '',
    validation: this.validator.valid(),
  };

  handleChange = (type, value) => {
    this.setState({ [type]: value });
  };

  generateUrl = type => {
    switch (type) {
      case 'user':
        return API_ROUTES.adminCreateUser;
      case 'partner':
        return API_ROUTES.createPartner;
      case 'client':
        return API_ROUTES.adminCreateClient;
    }
  };

  handleSubmit = e => {
    e.preventDefault();
    let validation = this.validator.validate(this.state);
    this.setState({ validation });
    this.submitted = true;

    if (validation.isValid) {
      let { userType, cookies } = this.props;
      let reqBody = { ...this.state };
      let authTokenFromCookie = cookies.get('authToken');

      let params = {
        url: this.generateUrl(userType),
        method: 'POST',
        data: reqBody,
        isAuth: true,
        authToken: authTokenFromCookie,
        showToggle: true,
      };

      fetchApi(params)
        .then(data => {
          if (data.success) {
            store.dispatch(
              toggleToast({ showToast: true, toastMsg: data.message }),
            );
            this.props.getUserList();

            if (this.props.userType !== 'client') {
              this.props.handleCloseModal();
            } else {
              this.props.showUserDetails(data.clientid);
            }
          } else {
            store.dispatch(
              toggleToast({ showToast: true, toastMsg: data.message }),
            );
          }
        })
        .catch(e => {
          console.log(e);
          store.dispatch(
            toggleToast({ showToast: true, toastMsg: 'Something went wrong!' }),
          );
          this.props.handleCloseModal();
        });
    }
  };

  render() {
    let { classes, userType } = this.props;
    let validation = this.submitted
      ? this.validator.validate(this.state)
      : this.state.validation;
    const { password } = this.state.validation;

    return (
      <div className="createUserWrap">
        <div className="create__user__inner">
          <div className="form__wrap">
            <h2>Enter Credentials</h2>
            <form onSubmit={this.handleSubmit}>
              {USER_INPUT.map(input => (
                <TextField
                  error={validation[input.id].isInvalid}
                  key={input.id}
                  id={input.id}
                  label={input.label}
                  fullWidth
                  type={input.type === 'password' ? 'password' : 'text'}
                  // className={classes.textField}
                  value={this.state[input.type]}
                  onChange={e => this.handleChange(input.type, e.target.value)}
                  margin="normal"
                />
              ))}
              {userType !== 'client' && userType !== 'partner' && (
                <FormControl className={classes.formControl} fullWidth>
                  <InputLabel htmlFor="clearance">Clearance</InputLabel>
                  <Select
                    value={this.state.clearance}
                    onChange={e =>
                      this.handleChange('clearance', e.target.value)
                    }
                    inputProps={{
                      name: 'clearance',
                      id: 'clearance',
                    }}
                  >
                    <MenuItem value={1}>Creator</MenuItem>
                    <MenuItem value={2}>Approver</MenuItem>
                  </Select>
                </FormControl>
              )}
              {password.isInvalid && (
                <CustomMessage>
                  <p>
                    <span className="customMsg__icon">
                      <i className="fas fa-lock"></i>
                    </span>
                    Your password needs to:
                  </p>
                  <ul>
                    <li>be atleast 8 characters long.</li>
                    <li>include both uppercase &amp; lowercase characters.</li>
                    <li>be atleast one number or symbol.</li>
                  </ul>
                </CustomMessage>
              )}
              <div className="submit-wrap">
                <button
                  className="solidBtn createUserBtn"
                  onClick={this.handleSubmit}
                >
                  Create {userType === 'user' ? 'User' : 'Client'}
                </button>
                <button
                  className="solidBtn createUserBtn"
                  onClick={this.props.handleCloseModal}
                >
                  Close
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default CreateUser;
