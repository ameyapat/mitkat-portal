import React, { useEffect, useState } from 'react';
import './style.scss';
import moment from 'moment';
import {
  categories,
  eventTypeConstants,
} from '../../../client/components/rimeDashboard/helpers/constants';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import Typography from '@mui/material/Typography';
import { useSelector, useDispatch } from 'react-redux';
import CustomModal from '../../../ui/modal';
import ArticleModal from '../../../client/components/rimeEventCard/articleModal';
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye';
import { withCookies } from 'react-cookie';
import {
  lastUpdateDate,
  eDate,
} from '../../../client/components/rimeDashboard/helpers/utils';
import Loader from '../../../ui/loader';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import { fetchApi } from '../../../../helpers/http/fetch';
import { toggleToast } from '../../../../actions';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import { TextField } from '@material-ui/core';
import injectSheet from 'react-jss';
import styles from './style';
import CreateEmailAlert from '../createEmailAlert';
import { link } from 'fs-extra';

const pinMappingRgba = {
  1: 'low',
  2: 'medium',
  3: 'high',
};

const MissedLinksCard = props => {
  const {
    srno,
    eventId,
    title,
    categoryId,
    userName,
    eventDate,
    eventType,
    link,
    sourceLinks,
    location,
    noOfArticles,
    lastUpdatedDate,
    tags,
    cookies,
    selectedFeed,
    classes,
  } = props;

  const [isNew, setIsNew] = useState(true);
  const [eventTypeTag, setEventType] = useState('');
  const [category, setCategory] = useState('');
  const [showModal, setShowModal] = useState(false);
  const setTheme = useSelector(state => state.themeChange.setDarkTheme);
  const refreshDuration = useSelector(state => state.rime.refreshDuration);
  const [isFetching, setIsFetching] = useState(false);
  const [eventTitle, setEventTitle] = useState(title);
  const [showCreateAlert, setShowCreateAlert] = useState(false);
  const dispatch = useDispatch();

  const TIMEOUT = refreshDuration * 60000;

  const mapRiskCategory = () => {
    eventTypeConstants.map(subitem => {
      if (eventType === subitem.id) {
        setEventType(subitem.label);
      }
    });
    categories.map(subitem => {
      if (categoryId === subitem.id) {
        setCategory(subitem.label);
      }
    });
  };

  const handleEditEvent = eventd => {
    setShowCreateAlert(true);
  };

  const getEventTypeList = () => {
    let eventTypeList = [];
    eventTypeList = eventTypeConstants.map(i => (
      <MenuItem value={i.id}>{i.label}</MenuItem>
    ));
    return eventTypeList;
  };

  const getCategoryList = () => {
    let categoryList = [];
    categoryList = categories.map(i => (
      <MenuItem value={i.id}>{i.label}</MenuItem>
    ));
    return categoryList;
  };

  const handleModal = () => {
    setShowModal(false);
  };
  const handleCloseModal = () => {
    setShowCreateAlert(false);
  };

  const showArticleModal = () => {
    setShowModal(true);
  };

  useEffect(() => {
    mapRiskCategory();
    setTimeout(() => {
      setIsNew(false);
    }, TIMEOUT);
  });

  let tag = tags && tags.split(',');
  let allLinks = sourceLinks && sourceLinks.split(',');

  const utcLastUpdateDate = lastUpdateDate(lastUpdatedDate);
  const utcDate = eDate(eventDate);

  return (
    <div className="rimeFiltered_card">
      {isFetching && <Loader />}

      <Typography component="div" className="monitoringScreenCard__eventsrno">
        {srno}
      </Typography>
      <div id="monitoringScreenCard__card">
        <Card className={`monitoringScreenCardList ${pinMappingRgba[eventType]}`}>
          <div className="monitoringScreenCard__header">
            <div className="monitoringScreenCard_pill_container">
              {/* <div className="eventFilteredCard_userName">
                <Typography
                  variant="subtitle1"
                  color="text.secondary"
                  component="span"
                  className="monitoringScreenCard__eventbody__Head"
                >
                  <span className="title">Surfer Name:</span>
                </Typography>
                <Typography
                  component="span"
                  className="monitoringScreenCard__eventbody__Head"
                >
                  &nbsp;
                  {userName ? userName : 'Not Available'}
                </Typography>
              </div> */}
            </div>
            <div
              className="article__content__pill"
              onClick={() => showArticleModal()}
            >
              <span>&nbsp;</span>
              <RemoveRedEyeIcon className="eyeIcon" />
            </div>
            <div className="monitoringScreenCard__card__innercolumn">
              <div>
                <Typography
                  variant="subtitle1"
                  color="text.secondary"
                  component="span"
                  className="monitoringScreenCard__date"
                >
                  Updated:&nbsp;
                </Typography>
                <Typography component="span" className="monitoringScreenCard__date">
                  {moment(utcLastUpdateDate).format('Do MMM YYYY, h:mm:ss a')}
                </Typography>
              </div>
            </div>
          </div>
          <div className="monitoringScreenCard__card__row1">
            <div className="monitoringScreenCard__card__innercolumn2">
              <Box sx={{ display: 'flex', flexDirection: 'column' }}>
                <div className="monitoringScreenCard_heading">
                  <Typography
                    component="div"
                    className="monitoringCard__eventtitle__wrap"
                  >
                    {title}
                  </Typography>
                  <div className="event_screening_location">
                    <Typography
                      variant="subtitle1"
                      color="text.secondary"
                      component="span"
                      className="monitoringScreenCard__eventbody__subHead"
                    >
                      Location:
                    </Typography>
                    <Typography
                      component="span"
                      className="monitoringScreenCard__eventbody__location_wrap"
                    >
                      &nbsp;{location ? location : 'Not Available'}
                    </Typography>
                  </div>
                </div>
              </Box>
            </div>
          </div>
        </Card>
        <CustomModal showModal={showModal}>
          <div
            id="rimeEventArchivesModalCont"
            className={`modal__inner ${setTheme ? 'dark' : 'light'}`}
          >
            <ArticleModal
              alllinks={link}
              closeEventModal={() => handleModal()}
            />
          </div>
        </CustomModal>

        <CustomModal showModal={showCreateAlert}>
          <div id="createEmail" className="modal__inner">
            <CreateEmailAlert
              closeEventModal={() => handleCloseModal()}
              riskCategory={category}
              categoryId={categoryId}
              eventLink={link}
              title={eventTitle}
            />
          </div>
        </CustomModal>
      </div>
    </div>
  );
};

export default withCookies(injectSheet(styles)(MissedLinksCard));
