import React, { useEffect, useState } from 'react';
import './style.scss';
import moment from 'moment';
import {
  categories,
  eventTypeConstants,
} from '../../../client/components/rimeDashboard/helpers/constants';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import Typography from '@mui/material/Typography';
import { useSelector, useDispatch } from 'react-redux';
import CustomModal from '../../../ui/modal';
import ArticleModal from '../../../client/components/rimeEventCard/articleModal';
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye';
import { withCookies } from 'react-cookie';
import {
  lastUpdateDate,
  eDate,
} from '../../../client/components/rimeDashboard/helpers/utils';
import Loader from '../../../ui/loader';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import { fetchApi } from '../../../../helpers/http/fetch';
import { toggleToast } from '../../../../actions';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import { TextField } from '@material-ui/core';
import injectSheet from 'react-jss';
import styles from './style';

const pinMappingRgba = {
  1: 'low',
  2: 'medium',
  3: 'high',
};

const RimeFilteredCard = props => {
  const {
    srno,
    eventId,
    title,
    categoryId,
    userName,
    eventDate,
    eventType,
    sourceLinks,
    location,
    noOfArticles,
    lastUpdatedDate,
    tags,
    cookies,
    selectedFeed,
    classes,
  } = props;

  const [isNew, setIsNew] = useState(true);
  const [eventTypeTag, setEventType] = useState('');
  const [category, setCategory] = useState('');
  const [editedField, seteditedField] = useState('');
  const [showModal, setShowModal] = useState(false);
  const setTheme = useSelector(state => state.themeChange.setDarkTheme);
  const refreshDuration = useSelector(state => state.rime.refreshDuration);
  const [showEventType, setShowEventType] = useState(false);
  const [dropdownEventType, setDropdownEventType] = useState(eventType);
  const [showCategoryDropdown, setshowCategoryDropdown] = useState(false);
  const [dropDownCategory, setDropDownCategory] = useState(categoryId);
  const [isFetching, setIsFetching] = useState(false);
  const [eventTitle, setEventTitle] = useState(title);
  const [showTitleTextField, setShowTitleTextField] = useState(false);
  const [eventLocation, setEventLocation] = useState(location);
  const [showLocationTextField, setShowLocationTextField] = useState(false);
  const [showSubmitButton, setShowSubmitButton] = useState(false);
  const dispatch = useDispatch();

  const TIMEOUT = refreshDuration * 60000;

  const mapRiskCategory = () => {
    eventTypeConstants.map(subitem => {
      if (eventType === subitem.id) {
        setEventType(subitem.label);
      }
    });
    categories.map(subitem => {
      if (categoryId === subitem.id) {
        setCategory(subitem.label);
      }
    });
  };

  const handleEditEvent = eventid => {
    setShowEventType(true);
    setshowCategoryDropdown(true);
    setShowTitleTextField(true);
    setShowLocationTextField(true);
    setShowSubmitButton(true);
    seteditedField(selectedFeed);
  };

  const handleEventChange = e => {
    let value = e.target.value;
    if (value) {
      setDropdownEventType(value);
      seteditedField({ ...editedField, eventType: value });
    }
  };

  const handleCategoryChange = e => {
    let value = e.target.value;
    if (value) {
      setDropDownCategory(value);
      seteditedField({ ...editedField, eventPrimaryRiskCategory: value });
    }
  };

  const handleTitleChange = e => {
    let value = e.target.value;
    if (value) {
      setEventTitle(value);
      seteditedField({ ...editedField, eventTitle: value });
    }
  };

  const handleLocationChange = e => {
    let value = e.target.value;
    if (value) {
      setEventLocation(value);
      seteditedField({ ...editedField, eventLoc: value });
    }
  };

  const handleSubmit = e => {
    let authToken = cookies.get('authToken');
    setIsFetching(true);
    let reqObj = {
      url: API_ROUTES.editFilteredCardEvent,
      data: editedField,
      isAuth: true,
      authToken,
      method: 'POST',
      showToggle: true,
    };
    fetchApi(reqObj)
      .then(data => {
        let toastObj = {
          toastMsg: 'Event Updated Successfully!',
          showToast: true,
        };
        dispatch(toggleToast(toastObj));
        setShowEventType(false);
        setshowCategoryDropdown(false);
        setShowTitleTextField(false);
        setShowLocationTextField(false);
        setShowSubmitButton(false);
        props.handleEdit(editedField);
        setIsFetching(false);

        if (data && data.status == 502) {
          let toastObj = {
            toastMsg: 'Something went wrong!, please try again',
            showToast: true,
          };
          dispatch(toggleToast(toastObj));
        }
      })
      .catch(e => {
        let toastObj = {
          toastMsg: 'Error editing event!',
          showToast: true,
        };
        dispatch(toggleToast(toastObj));
      });
  };

  const handleDeleteEvent = id => {
    let authToken = cookies.get('authToken');
    let reqObj = {
      url: `${API_ROUTES.deleteScreenEvent}/${id}`,
      isAuth: true,
      authToken,
      method: 'GET',
      showToggle: true,
    };
    fetchApi(reqObj)
      .then(data => {
        let toastObj = {
          toastMsg: data.message,
          showToast: data.success,
        };
        dispatch(toggleToast(toastObj));
        props.handleDelete(id);
      })
      .catch(e => {
        let toastObj = {
          toastMsg: 'Error deleting report',
          showToast: true,
        };
        dispatch(toggleToast(toastObj));
      });
  };

  const getEventTypeList = () => {
    let eventTypeList = [];
    eventTypeList = eventTypeConstants.map(i => (
      <MenuItem value={i.id}>{i.label}</MenuItem>
    ));
    return eventTypeList;
  };

  const getCategoryList = () => {
    let categoryList = [];
    categoryList = categories.map(i => (
      <MenuItem value={i.id}>{i.label}</MenuItem>
    ));
    return categoryList;
  };

  const handleModal = () => {
    setShowModal(false);
  };

  const showArticleModal = () => {
    setShowModal(true);
  };

  useEffect(() => {
    mapRiskCategory();
    setTimeout(() => {
      setIsNew(false);
    }, TIMEOUT);
  });

  let tag = tags && tags.split(',');
  let allLinks = sourceLinks && sourceLinks.split(',');

  const utcLastUpdateDate = lastUpdateDate(lastUpdatedDate);
  const utcDate = eDate(eventDate);

  return (
    <div className="rimeFiltered_card">
      {isFetching && <Loader />}

      <Typography component="div" className="rimeFilteredCard__eventsrno">
        {srno}
      </Typography>
      <div id="rimeFilteredCard__card">
        <Card className={`rimeFilteredCardList ${pinMappingRgba[eventType]}`}>
          <div className="rimeFilteredCard__header">
            <div className="rimeFilteredCard_pill_container">
              <div
                className={`rimeFilteredCard__pill ${eventType &&
                  pinMappingRgba[eventType]}`}
              >
                <Typography
                  component="span"
                  className="rimeFilteredCard__cat__pill"
                >
                  {eventTypeTag ? eventTypeTag : 'Not Available'}
                </Typography>
              </div>
              <div className="eventFilteredCard_riskCategory_pill">
                <Typography
                  component="span"
                  className="rimeFilteredCard__category__cat__pill"
                >
                  {category ? category : 'Not Available'}
                </Typography>
              </div>
              <div className="eventFilteredCard_userName">
                <Typography
                  variant="subtitle1"
                  color="text.secondary"
                  component="span"
                  className="rimeFilteredCard__eventbody__Head"
                >
                  <span className="title">Surfer Name:</span>
                </Typography>
                <Typography
                  component="span"
                  className="rimeFilteredCard__eventbody__Head"
                >
                  &nbsp;
                  {userName ? userName : 'Not Available'}
                </Typography>
              </div>
            </div>
            <div
              className="article__content__pill"
              onClick={() => showArticleModal()}
            >
              <Typography
                variant="subtitle1"
                color="text.secondary"
                component="span"
                className="rimeFilteredCard__eventbody__Head"
              >
                <span className="title">No of Articles:</span>
              </Typography>
              <Typography
                component="span"
                className="rimeFilteredCard__eventbody__Head"
              >
                &nbsp;
                {noOfArticles ? noOfArticles : 'Not Available'}
              </Typography>
              <span>&nbsp;</span>
              <RemoveRedEyeIcon className="eyeIcon" />
            </div>
            <div className="rimeFilteredCard__card__innercolumn">
              <div>
                <Typography component="span" className="rimeFilteredCard_date">
                  &nbsp;{moment(utcDate).format('Do MMM YYYY')}
                </Typography>
                <Typography
                  variant="subtitle1"
                  color="text.secondary"
                  component="span"
                  className="rimeFilteredCard__date"
                >
                  Updated:&nbsp;
                </Typography>
                <Typography component="span" className="rimeFilteredCard__date">
                  {moment(utcLastUpdateDate).format('Do MMM YYYY, h:mm:ss a')}
                </Typography>
              </div>
            </div>
          </div>
          <div className="rimeFilteredCard__card__row1">
            <div className="rimeFilteredCard__card__innercolumn2">
              <Box sx={{ display: 'flex', flexDirection: 'column' }}>
                <div className="rimeFilteredCard_heading">
                  <Typography
                    component="div"
                    className="rimeFilteredCard__eventtitle__wrap"
                    style={{ color: 'black' }}
                  >
                    {title}
                  </Typography>
                  <div className="buttons__wrap">
                    <button
                      className="btn--edit"
                      onClick={() => handleEditEvent(eventId)}
                    >
                      <i className="icon-uniE93E"></i>
                    </button>
                    <button
                      className="btn--delete"
                      onClick={() => handleDeleteEvent(eventId)}
                    >
                      <i className="icon-uniE949"></i>
                    </button>
                  </div>
                  <div className="event_screening_location">
                    <Typography
                      variant="subtitle1"
                      color="text.secondary"
                      component="span"
                      className="rimeFilteredCard__eventbody__subHead"
                    >
                      Location:
                    </Typography>
                    <Typography
                      component="span"
                      className="rimeFilteredCard__eventbody__location_wrap"
                    >
                      &nbsp;{location ? location : 'Not Available'}
                    </Typography>
                  </div>
                </div>
              </Box>
            </div>
          </div>
          <div className="filter_edit_options">
            {showEventType && (
              <div>
                <FormControl className="eventType_dropdown">
                  <InputLabel
                    classes={{
                      root: classes.rootLabel,
                    }}
                    htmlFor="select-event-type"
                  >
                    Select Event Type
                  </InputLabel>
                  <Select
                    value={dropdownEventType}
                    onChange={handleEventChange}
                    inputProps={{
                      shrink: false,
                      name: 'Select Event Type',
                      id: 'select-event-type',
                      classes: {
                        root: classes.rootInput,
                        input: classes.rootInput,
                        select: classes.rootSelect,
                        icon: classes.rootIcon,
                      },
                    }}
                  >
                    {getEventTypeList()}
                  </Select>
                </FormControl>
              </div>
            )}
            {showCategoryDropdown && (
              <div>
                <FormControl className="riskCategory_dropdown">
                  <InputLabel
                    classes={{
                      root: classes.rootLabel,
                    }}
                    htmlFor="select-event-type"
                  >
                    Select Risk category
                  </InputLabel>
                  <Select
                    value={dropDownCategory}
                    onChange={handleCategoryChange}
                    inputProps={{
                      shrink: false,
                      name: 'Select Event Type',
                      id: 'select-event-type',
                      classes: {
                        root: classes.rootInput,
                        input: classes.rootInput,
                        select: classes.rootSelect,
                        icon: classes.rootIcon,
                      },
                    }}
                  >
                    {getCategoryList()}
                  </Select>
                </FormControl>
              </div>
            )}
            {showTitleTextField && (
              <div className="editTitle_container">
                <TextField
                  id="EditTitle"
                  label="Edit Title"
                  variant="standard"
                  type="text"
                  InputLabelProps={{
                    shrink: true,
                  }}
                  value={eventTitle}
                  fullWidth
                  onChange={handleTitleChange}
                />
              </div>
            )}
            {showLocationTextField && (
              <div className="editLocation_container">
                <TextField
                  id="EditLocation"
                  label="Edit Location"
                  variant="standard"
                  type="text"
                  fullWidth
                  InputLabelProps={{
                    shrink: true,
                  }}
                  value={eventLocation}
                  onChange={handleLocationChange}
                />
              </div>
            )}
            {showSubmitButton && (
              <div className="submit_button_container">
                <button
                  className="submit_button"
                  onClick={() => handleSubmit()}
                >
                  Submit
                </button>
              </div>
            )}
          </div>
        </Card>
        <CustomModal showModal={showModal}>
          <div
            id="rimeEventArchivesModalCont"
            className={`modal__inner ${setTheme ? 'dark' : 'light'}`}
          >
            <ArticleModal
              alllinks={allLinks}
              closeEventModal={() => handleModal()}
            />
          </div>
        </CustomModal>
      </div>
    </div>
  );
};

export default withCookies(injectSheet(styles)(RimeFilteredCard));
