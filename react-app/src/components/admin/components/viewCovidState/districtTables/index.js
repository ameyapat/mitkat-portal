import React, { Component } from 'react';
import TableListRow from '../../../../ui/tablelist/TableListRow';
import TableListCell from '../../../../ui/tablelist/TableListCell';
import { withStyles, Button, TextField } from '@material-ui/core';
import TableList from '../../../../ui/tablelist';
import clsx from 'clsx';
import './style.scss';

const styles = () => ({
  rootRiskWatch: {
    background: '#F0F0F1',
    padding: 20,
  },
  innerRiskWatch: {
    '& .dmd__wrap': {
      margin: 0,
    },
  },
  rootThead: {
    fontWeight: 600,
    padding: '10px 3px',
    cursor: 'pointer',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
    '& p': {
      textTransform: 'uppercase',
    },
    '&:hover': {
      backgroundColor: 'rgba(0, 0, 0, 0.8)',
    },
  },
  tRowWrap: {},
  rootTRowWrap: {
    padding: '0',
    borderBottom: 0,
    borderRadius: 3,

    '& p': {
      color: 'var(--whiteMediumEmp)',
    },
  },
  rowWrap: {
    backgroundColor: 'var(--backgroundSolid)',
    padding: '0',
    borderRadius: '3px',
    '&:nth-child(even)': {
      backgroundColor: '#101b2d',

      '& p': {
        color: '#6c757d',
      },
    },
  },
  closeTimes: {
    position: 'absolute',
    color: '#000',
    fontSize: 30,
    right: 10,
    top: 10,
    cursor: 'pointer',
  },
  counter: {
    margin: '8px 5px',
    '& input': {
      padding: '10px',
      fontSize: '13px',
    },
  },
  rootBtnGraph: {
    backgroundColor: 'transparent',
    border: '1px solid rgba(26, 115, 232, 0.8)',
    color: 'rgba(26, 115, 232, 0.8)',
    fontSize: '13px',
    minWidth: 32,
    '&:hover': {
      backgroundColor: 'rgba(26, 115, 232, 0.7)',
      color: 'var(--whiteMediumEmp)',
      border: '1px solid rgba(26, 115, 232, 0.8)',
    },

    '& i': {
      margin: 0,
    },
  },
  rootBtnDistrict: {
    backgroundColor: 'transparent',
    fontSize: '13px',
    minWidth: 32,
    color: 'rgb(108, 117, 125)',
    '&:hover': {
      backgroundColor: 'rgba(52, 73, 94, 0.7)',
      color: 'var(--whiteMediumEmp)',
    },

    '& i': {
      margin: 0,
    },
  },
  rootTCellNew: {
    justifyContent: 'space-around',
    '& p': {
      color: 'rgb(108, 117, 125)',
      fontWeight: 'bold',
      textAlign: 'center',
    },
  },
  boxOne: {
    flex: 1,
  },
  boxTwo: {
    flex: 1,
  },
  metaWrap: {
    margin: '0 10px',
    padding: 10,
    backgroundColor: '#fff',
    boxShadow: '1px 1px 1px 1px rgba(0,0,0,0.2)',
    borderRadius: 3,
    height: '100vh',
    overflowX: 'hidden',
    overflowY: 'scroll',
  },
  graphWrappers: {
    margin: '5px 0',
  },
  rootLabel: {
    color: 'rgba(255,255,255, 0.67) !important',
  },
  rootInput: {
    color: 'var(--whiteHighEmp) !important',
  },
  underline: {
    '&:after': {
      borderBottom: '2px solid #34495e !important',
    },
  },
  notchedOutline: {
    borderColor: 'var(--whiteHighEmp) !important ',
  },
});

@withStyles(styles)
class DistrictTables extends Component {
  renderTheadTitles = (value, meta) => {
    return (
      <span className="thead--titles--wrapper">
        <p>{value.title}</p>
      </span>
    );
  };

  renderTHeader = () => {
    const { classes } = this.props;
    return (
      <div className={classes.tRowWrap}>
        <TableListRow classes={{ root: classes.rootTRowWrap }}>
          <TableListCell
            styles={{ flex: 2 }}
            value={{
              title: 'District Name',
            }}
            type={'district'}
            renderProp={(value, meta) => this.renderTheadTitles(value, meta)}
            classes={{ root: clsx(classes.rootThead, 'msite-t-head') }}
          />
          <TableListCell
            value={'Numbers'}
            classes={{
              root: clsx(
                classes.rootThead,
                'msite-t-head',
                'hide-me',
                'flx-xs',
              ),
            }}
          />
        </TableListRow>
      </div>
    );
  };

  renderTBody = () => {
    return <TableList>{this.renderEventList()}</TableList>;
  };

  renderEventList = () => {
    const { classes, districts } = this.props;
    return districts.map(event => (
      <TableListRow
        key={event.districtName + '_' + event.id}
        classes={{ root: classes.rowWrap }}
      >
        <TableListCell
          styles={{ flex: 2 }}
          value={event.districtName || '-'}
          classes={{ root: clsx(classes.rootTCellNew, 'msite-t-head') }}
        />
        <TableListCell classes={{ root: 'msite-t-head' }}>
          <TextField
            required
            id={`count_${event.id}`}
            label="Count"
            fullWidth
            value={event.count || 0}
            onChange={e =>
              this.props.handleCountChange(
                e.target.value,
                event,
                'districtName',
              )
            }
            // defaultValue="Count"
            margin="normal"
            variant="outlined"
            InputLabelProps={{
              shrink: true,
              classes: {
                root: classes.rootLabel,
              },
            }}
            InputProps={{
              classes: {
                root: classes.rootInput,
                input: classes.rootInput,
                notchedOutline: classes.notchedOutline,
              },
              endAdornment: null,
            }}
            type="number"
            classes={{
              root: classes.counter,
            }}
          />
        </TableListCell>
      </TableListRow>
    ));
  };

  handleInputChange = (e, type) => {};

  render() {
    const { classes } = this.props;

    return (
      <div className="districtTableParams">
        <div className={clsx(classes.boxOne)}>
          {this.renderTHeader()}
          {this.renderTBody()}
        </div>
      </div>
    );
  }
}

export default DistrictTables;
