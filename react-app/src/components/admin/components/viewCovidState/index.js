import React, { Component } from 'react';
import { fetchStates } from '../../../../helpers/utils';
import { withCookies } from 'react-cookie';
import clsx from 'clsx';
import './style.scss';

import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import { fetchApi } from '../../../../helpers/http/fetch';
import CustomModal from '../../../ui/modal';
import CoronaStatesTable from './coronaStatesTable/CoronaStatesTable';
import Empty from '../../../ui/empty';
import EditStatesForm from './editStatesForm';

import draftToHtml from 'draftjs-to-html';
import { convertToRaw } from 'draft-js';
import { toggleToast } from '../../../../actions';
import store from '../../../../store';
import { Button, withStyles } from '@material-ui/core';
import EditDistrictForm from './editDistrictForm';

import { downloadSummary, downloadExcel, downloadDistrictReport } from '../../helpers/utils';

const styles = () => ({
    downloadBtns: {
        fontSize: '13px',
        minWidth: 32,  
        '& i':{
            margin: 0,
        }
    },
    rootBtnPdf: {
        backgroundColor: 'rgba(29, 53, 87, 1)',
        '&:hover':{
            backgroundColor: 'rgba(29, 53, 87, 0.8)'
        },
    },
    rootBtnExcel: {
        backgroundColor: 'rgba(112, 193, 179, 1)', 
        marginLeft: 10,
        '&:hover':{
            backgroundColor: 'rgba(112, 193, 179, 0.8)'
        },
    },
    rootBtnSummary: {
        backgroundColor: 'rgba(69, 123, 157, 1)',
        marginLeft: 10,
        '&:hover':{
            backgroundColor: 'rgba(69, 123, 157, 0.8)'
        },
    },
})

@withCookies
@withStyles(styles)
class ViewCovidState extends Component{

    state = {
        states: [],
        districtDetails: null,
        eventDetails: {},
        showModal: false,
        showDistrictModal: false
    }

    componentDidMount(){
        const { cookies } = this.props;
        fetchStates(cookies.get('authToken')).then(states => this.setState({states}, () => console.log(this.state)));
    }

    getStateDetails = (id) => {
        const { cookies } = this.props;
        const reqObj = {
            url: `${API_ROUTES.getStateDetails}/${id}`,
            isAuth: true,
            authToken: cookies.get("authToken"),
            method: "GET",
            showToggle: true,
        };
        
        fetchApi(reqObj)
        .then(eventDetails => {
            this.setState({ eventDetails, showModal: true });
        })
        .catch(e => console.log(e));
    }

    getDistrictDetails = (id) => {
        const { cookies } = this.props;
        const reqObj = {
            url: `${API_ROUTES.getDistricts}/${id}`,
            isAuth: true,
            authToken: cookies.get("authToken"),
            method: "GET",
            showToggle: true,
        };
        
        fetchApi(reqObj)
        .then(districtDetails => {
            this.setState({ districtDetails, showDistrictModal: true, defaultDistricts: districtDetails.districts  });
        })
        .catch(e => console.log(e));
    }

    postStateDetails = () => {
        const { cookies } = this.props;
        const { id, resources, implementedMeasure } = this.state.eventDetails;

        let reqDataObj = {
            id,
            implementedMeasure,
            resources
        }

        const reqObj = {
            url: API_ROUTES.postStateDetails,
            isAuth: true,
            authToken: cookies.get("authToken"),
            method: "POST",
            showToggle: true,
            data: reqDataObj
        };
        
        fetchApi(reqObj)
        .then(data => {
            const toastObj = {
                toastMsg: data.message,
                showToast: true
              };
            store.dispatch(toggleToast(toastObj))
            this.handleModal(false);

        })
        .catch(e => console.log(e));
    }

    getDistricts = (districts) => {
        return new Promise((res, rej) => {

            const districtsArr = []

            districts.map((d) => {
                districtsArr.push({
                    id: parseInt(d.id),
                    count: parseInt(d.count)
                })
            })
            res(districtsArr);
        })
    }

    updateDistrictDetails = async () => {
        const { cookies } = this.props;
        const { districts, stateid } = this.state.districtDetails;

        let reqDataObj = {
            districts: await this.getDistricts(districts)
        }
        
        const reqObj = {
            url: `${API_ROUTES.saveDistricts}/${stateid}`,
            isAuth: true,
            authToken: cookies.get("authToken"),
            method: "POST",
            showToggle: true,
            data: reqDataObj
        };
        
        fetchApi(reqObj)
        .then(data => {
            const toastObj = {
                toastMsg: data.message,
                showToast: true
              };
            store.dispatch(toggleToast(toastObj))
            this.handleDistrictModal(false);
        })
        .catch(e => console.log(e));
    }

    handleModal = (show) => this.setState({showModal: show});

    handleDistrictModal = (show) => this.setState({showDistrictModal: show});

    onEditorStateChange = (value, type) => {
        let { eventDetails } = this.state;
        let editorState = draftToHtml(convertToRaw(value.getCurrentContent()));        
        let clearState = editorState.replace(/&nbsp;/g, '');        

        eventDetails[type] = clearState;
        this.setState({ eventDetails });
      }

      handleInputChange = (value, type) => {
          let { eventDetails } = this.state;
          eventDetails[type] = value;
          this.setState({eventDetails});
    }


      handleCountChange = (value, details, type) => {
          let { districtDetails } = this.state; 
          const { districts } = this.state.districtDetails;
          const obj = districts.find((detail) => detail.id === details.id);
          const index = districts.indexOf(obj);
          districts[index]["count"] = value;

          this.setState({ districtDetails });
      }

      searchDistricts = (value, list) => {
        let regex = new RegExp(value, 'gi');
    
        return list.filter((item, idx) => {
            for(let k in item){
                let stringItem = item[k] && item[k].toString();
                if(stringItem && stringItem.match(regex)){
                    return item;
                }
            }
        })
    }

    handleSearch = e => {
        let value = e.target.value;
        let { districts } = this.state.districtDetails;
        let { districtDetails } = this.state;
        districts = this.searchDistricts(value, districts);
        districtDetails['districts'] = districts;
        if (districts.length > 0 && value) {
          this.setState({ districtDetails });
        } else {
            districtDetails['districts'] = this.state.defaultDistricts;
          this.setState({ districtDetails });
        }
      };


    render(){
        const { classes, cookies } = this.props;
        const { states, eventDetails, districtDetails } = this.state;
        const authToken = cookies.get('authToken');
        return(
            <div id="covidStateDetails">
                <div className="download-district-covid" style={{ paddingBottom: 20 }}>                                
                    <Button
                        variant='contained'
                        color='secondary'
                        onClick={() => downloadDistrictReport(authToken)}
                        classes={{
                            root: clsx(classes.rootBtnPdf, classes.downloadBtns)
                        }}
                        >
                        Download Pdf              
                    </Button> 
                    <Button
                        variant='contained'
                        color='secondary'                    
                        onClick={() => downloadExcel(authToken)}
                        classes={{
                            root: clsx(classes.rootBtnExcel, classes.downloadBtns)
                        }}
                        >
                        Download Excel              
                    </Button> 
                    <Button
                        variant='contained'
                        color='secondary'
                        onClick={() => downloadSummary(authToken)}
                        classes={{
                            root: clsx(classes.rootBtnSummary, classes.downloadBtns)
                        }}
                        >
                        Download Summary              
                    </Button> 
                </div>
                {
                    states.length > 0
                    ?
                    <CoronaStatesTable 
                        data={states} 
                        getStateDetails={this.getStateDetails}
                        getDistrictDetails={this.getDistrictDetails}
                    />
                    :
                    <Empty title="No states available!" />
                }
                <CustomModal 
                    showModal={this.state.showModal} 
                    closeModal={() => this.handleModal(false)}
                    disableBackdropClick={true}
                    showCloseOption={false}
                >
                    <div id="stateDetailsModal" className="modal__inner">
                        <EditStatesForm
                            eventDetails={eventDetails} 
                            closeModal={() => this.handleModal(false)}
                            resources={eventDetails.resources}
                            implementedMeasure={eventDetails.implementedMeasure}
                            onEditorStateChange={this.onEditorStateChange}
                            handleInputChange={this.handleInputChange}
                            postStateDetails={this.postStateDetails}
                        />
                    </div>
                </CustomModal>
                <CustomModal 
                    showModal={this.state.showDistrictModal} 
                    closeModal={() => this.handleModal(false)}
                    disableBackdropClick={true}
                    showCloseOption={false}
                >
                    <div id="districtDetailsModal" className="modal__inner">
                        <EditDistrictForm 
                            districtDetails={districtDetails}
                            closeModal={() => this.handleDistrictModal(false)} 
                            updateDistrictDetails={this.updateDistrictDetails}
                            handleCountChange={this.handleCountChange}
                            handleSearch={this.handleSearch}
                        />
                    </div>
                </CustomModal>
            </div>
        )
    }
}

export default ViewCovidState;