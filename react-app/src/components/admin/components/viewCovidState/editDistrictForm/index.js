import React, { Component } from 'react';
import { TextField } from '@material-ui/core';

import clsx from 'clsx';
import SearchBox from '../../../../ui/searchBox';
import AlertDialog from '../../../../ui/alertDialog';

import './style.scss';
import DistrictTables from '../districtTables';
import injectSheet from "react-jss";
import style from "./style";

@injectSheet(style)
class EditDistrictForm extends Component{

    state = {
        showAlert: false,
    }

    handleClose = () => {
        this.setState({showAlert: true});
    }

    render(){
        const { districtDetails, classes } = this.props;
        const { showAlert } = this.state;
        
        return(
            <div id="editDistrictForm">
                <div className="box-cont">
                        <div className="box--cont-inner">
                            <TextField
                                id={'state-name-ip'}
                                label={'State Name'}
                                fullWidth                    
                                value={districtDetails.statename || ''}
                                margin="normal"
                                variant="outlined"
                                disabled
                                className="state-name--text"
                                InputLabelProps={{
                                    shrink: true,
                                    classes: {
                                      root: classes.rootLabel
                                    }
                                }}
                                InputProps={{
                                    classes: {
                                        root: classes.rootInput,
                                        input: classes.rootInput,
                                        notchedOutline: classes.notchedOutline,
                                    },
                                    endAdornment: null
                                }}
                            />
                        </div>
                        <div className="box--cont-inner">
                            {true && (
                                <div className="product__search">
                                <SearchBox
                                    classes={{ icon : "fa fa-search" }}
                                    placeholder="Search by District name"
                                    parentHandler={this.props.handleSearch}
                                />
                                </div>
                            )}
                        </div>
                </div>
                <div className="district-list-cont">
                    <DistrictTables 
                        districts={districtDetails.districts || []} 
                        handleCountChange={this.props.handleCountChange} 
                    />
                </div>
                <div className="district-actions-cont">
                    <button 
                        // disabled={resources.length || implementedMeasure.length === 0}
                        className={clsx("btn--addTitle", "submit--btn--close")} 
                        onClick={() => this.handleClose()}
                    >
                        Cancel
                    </button>
                    <button 
                        // disabled={resources.length || implementedMeasure.length === 0}
                        className={clsx("btn--addTitle", "submit--btn--stateForm")} 
                        onClick={() => this.props.updateDistrictDetails()}
                    >
                        Submit
                    </button>
                </div>
                {
                    showAlert &&
                    <AlertDialog 
                        isOpen={showAlert}
                        title="Cancel Edit" 
                        description="You are about to cancel editing an event, are you sure?" 
                        parentEventHandler={() => this.props.closeModal()} 
                        handleClose={() => this.setState({showAlert: false})} 
                    />
                }

            </div>
        )
    }
}

export default EditDistrictForm;