import React, { Component } from 'react'
import TableList from '../../../ui/tablelist'
import TableListRow from '../../../ui/tablelist/TableListRow'
import TableListCell from '../../../ui/tablelist/TableListCell'
import './style.scss'

class MonthlyCompilation extends Component {
    render(){
        return(
            <div id="monthlyCompilation">
                <TableList>
                    <div className="monthlyCompilation__header">
                        <TableListRow>
                            <TableListCell value="Compilation Type" classes={{root: "titleHead"}} />
                            <TableListCell value="#" classes={{root: "titleHead"}} />
                        </TableListRow>
                    </div>
                    <div className="monthlyCompilation__body">                    
                        <TableListRow>
                            <TableListCell value="Banking compilation" />
                            <TableListCell classes={{root: "titleLink"}}>
                                <a href="https://mitkatrisktracker.com/api/external/pdfbankingcompilation" target="_blank" rel="noopener noreferrer">Download PDF</a>
                                <a href="https://mitkatrisktracker.com/api/external/excelbankingcompilation" target="_blank" rel="noopener noreferrer">Download Excel</a>
                            </TableListCell>
                        </TableListRow>
                        <TableListRow>
                            <TableListCell value="Liquor compilation" />
                            <TableListCell classes={{root: "titleLink"}}>
                                <a href="https://mitkatrisktracker.com/api/external/pdfliquorcompilation" target="_blank" rel="noopener noreferrer">Download PDF</a>
                                <a href="https://mitkatrisktracker.com/api/external/excelliquorcompilation" target="_blank" rel="noopener noreferrer">Download Excel</a>
                            </TableListCell>
                        </TableListRow>
                    </div>
                </TableList>
            </div>
        )
    }
}

export default MonthlyCompilation