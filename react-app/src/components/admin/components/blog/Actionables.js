import React from 'react';
import injectSheet from 'react-jss';
import styles from './style';

const Actionables = ({ handleAdd, classes }) => {
  return (
    <div className={classes.rootActionable}>
      <button onClick={() => handleAdd(true)}>
        Create New Blog <i className="fas fa-plus" />
      </button>
    </div>
  );
};

export default injectSheet(styles)(Actionables);
