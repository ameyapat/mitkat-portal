import React, { Component, Fragment } from 'react';
//
import styles from './style';
import InfoCardList from '../../../ui/infoCardList';
import InfoCard from '../../../ui/infoCard';
import CustomModal from '../../../ui/modal';
import AlertDialog from '../../../ui/alertDialog';
import CustomReportsForm from '../../../ui/customReportsForm';
import Empty from '../../../ui/empty';
//helpers
import injectSheet from 'react-jss';
import { withCookies } from 'react-cookie';
import { fetchApi, uploadApi } from '../../../../helpers/http/fetch';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import store from '../../../../store';
import { toggleToast } from '../../../../actions';
import Actionables from './Actionables';
import { getValuesFromObjects } from './utils';
import {
  deleteBlog,
  getBlogDetails,
  getBlogList,
  viewBlog,
  downloadBlog,
} from '../../../../requestor/admin/requestor';

class Blog extends Component {
  state = {
    blogList: [],
    modalUrl: '',
    showAlert: false,
    showAddForm: false,
    isEdit: false,
    blogDetails: {},
  };

  componentDidMount() {
    getBlogList().then(data => {
      this.setState({ blogList: data.output });
    });
  }

  getBlogListXML = () => {
    let { blogList } = this.state;
    return blogList.map(l => {
      return (
        <InfoCard
          key={l.id}
          handleDelete={this.handleDelete}
          handleView={this.handleView}
          handleEdit={this.handleEdit}
          handleDownload={this.handleDownload}
          id={l.id}
          title={l.title}
          imgUrl={l.dispayurl}
          date={l.date}
        />
      );
    });
  };

  handleDelete = id => {
    this.setState({
      showAlert: true,
      reportId: id,
    });
  };

  handleEdit = id => {
    this.setState(
      {
        reportId: id,
      },
      () => {
        this.fetchBlogDetails();
      },
    );
  };

  fetchBlogDetails = () => {
    const { reportId } = this.state;
    getBlogDetails(reportId)
      .then(data => {
        this.setState(
          { blogDetails: { ...data, advisoryurl: data?.blog } },
          () => {
            this.handleEditForm({
              isEdit: true,
              showAddForm: true,
            });
          },
        );
      })
      .catch(e => console.log('Error fetching details', e));
  };

  handleEditForm = ({ isEdit, showAddForm }) => {
    this.setState({
      isEdit,
      showAddForm,
    });
  };

  handleConfirmDelete = id => {
    deleteBlog(id)
      .then(data => {
        if (data.success) {
          this.setState(
            {
              showAlert: false,
            },
            () => {
              let toastObj = {
                toastMsg: 'Blog Deleted Successfully!',
                showToast: data.success,
              };
              store.dispatch(toggleToast(toastObj));
              getBlogList().then(data => {
                this.setState({ blogList: data.output });
              });
            },
          );
        }
      })
      .catch(e => console.log(e));
  };

  handleDownload = id => {
    downloadBlog(id)
      .then(blobObj => {
        let a = document.createElement('a');
        document.body.appendChild(a);
        let objUrl = new Blob([blobObj], { type: 'application/pdf' });
        let url = window.URL.createObjectURL(objUrl);
        a.href = url;
        a.download = `blog-pdf-${id}`;
        a.click();
        window.URL.revokeObjectURL(url);
      })
      .catch(e => console.log(e));
  };

  handleView = id => {
    viewBlog(id)
      .then(data => {
        if (data.success) {
          this.setState({
            showModal: true,
            modalUrl: data.output.viewimageurl,
          });
        }
      })
      .catch(e => console.log(e));
  };

  toggleModal = showModal => this.setState({ showModal });

  handleCloseAlert = () => this.setState({ showAlert: false });

  handleToggleAdd = showAddForm => this.setState({ showAddForm });

  handleSubmit = data => {
    let { cookies } = this.props;
    const { isEdit, reportId } = this.state;
    let authToken = cookies.get('authToken');
    let {
      imageFile,
      document,
      title,
      selectedLiveTopic: { value: topicid },
      selectedCountries,
      date,
    } = data;

    const formData = new FormData();

    formData.append('displaypic', imageFile);
    formData.append('title', title);
    formData.append('blog', document);
    formData.append('topicid', topicid || 0);
    formData.append('countries', getValuesFromObjects(selectedCountries));

    if (isEdit) {
      formData.append('date', date || '');
      formData.append('advisoryid', reportId);
    }

    let params = {
      url: `${!isEdit ? API_ROUTES.createBlog : API_ROUTES.updateBlog}`,
      reqObj: {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${authToken}`,
        },
        body: formData,
      },
    };

    uploadApi(params.url, params.reqObj).then(res => {
      if (res.success) {
        let toastObj = {
          toastMsg: !isEdit
            ? 'Blog Added Successfully!'
            : 'Blog Updated Successfully!',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
        this.handleToggleAdd();
        getBlogList().then(data => {
          this.setState({ blogList: data.output, isEdit: false });
        });
      } else {
        let toastObj = {
          toastMsg: 'Something went wrong!',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      }
    });
  };

  handleCloseModal = () => {
    this.handleToggleAdd(false);
    this.setState({ isEdit: false });
  };

  render() {
    let { classes } = this.props;
    let {
      blogList,
      showModal,
      modalUrl,
      showAlert,
      reportId,
      showAddForm,
      isEdit,
      blogDetails,
    } = this.state;

    return (
      <Fragment>
        <Actionables handleAdd={this.handleToggleAdd} />
        <InfoCardList>
          {blogList?.length > 0 ? (
            this.getBlogListXML()
          ) : (
            <Empty title="No Blogs Available" />
          )}
          <CustomModal
            showModal={showModal}
            closeModal={() => this.toggleModal(false)}
          >
            <div className={classes.rootIframe}>
              <span
                className="close-times"
                onClick={() => this.toggleModal(false)}
              >
                &times;
              </span>
              <iframe src={modalUrl} height="100%" width="100%" title="pdf" />
            </div>
          </CustomModal>
          <CustomModal
            showModal={showAddForm}
            closeModal={() => this.toggleModal(false)}
          >
            <div className={classes.rootAddForm}>
              <span
                className="close-times"
                onClick={() => this.handleCloseModal()}
              >
                &times;
              </span>
              <CustomReportsForm
                heading="Create New Blog"
                titlePlaceHolder="Enter title"
                imagePlaceHolder="Upload Display Image"
                filePlaceHolder="Upload Blog"
                handleSubmit={this.handleSubmit}
                showTopics={true}
                showCountries={true}
                showDate={isEdit}
                isEdit={isEdit}
                details={blogDetails}
                showLinks={true}
              />
            </div>
          </CustomModal>
          {showAlert && (
            <AlertDialog
              isOpen={showAlert}
              title="Delete Advisory"
              description="You are about to delete an event, are you sure?"
              parentEventHandler={() => this.handleConfirmDelete(reportId)}
              handleClose={() => this.handleCloseAlert()}
            />
          )}
        </InfoCardList>
      </Fragment>
    );
  }
}

export default withCookies(injectSheet(styles)(Blog));
