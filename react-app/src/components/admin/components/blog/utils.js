export const getValuesFromObjects = arrOfObjects =>
  arrOfObjects.map(item => item?.value || []);
