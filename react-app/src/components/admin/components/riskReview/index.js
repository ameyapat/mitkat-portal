import React, { Component, Fragment } from "react";
//
import styles from "./style";
import InfoCardList from "../../../ui/infoCardList";
import InfoCard from "../../../ui/infoCard";
import CustomModal from "../../../ui/modal";
import AlertDialog from "../../../ui/alertDialog";
import CustomReportsForm from '../../../ui/customReportsForm';
import Empty from '../../../ui/empty';
//helpers
import injectSheet from "react-jss";
import { withCookies } from "react-cookie";
import { fetchApi, uploadApi } from "../../../../helpers/http/fetch";
import { API_ROUTES } from "../../../../helpers/http/apiRoutes";
import store from "../../../../store";
import { toggleToast } from "../../../../actions";
import Actionables from "./Actionables";
import { getValuesFromObjects } from '../advisory/utils';
import { getRiskReviewDetails } from '../../../../requestor/admin/requestor';

class RiskReview extends Component {
  state = {
    reviews: [],
    modalUrl: '',
    showAlert: false,
    showAddForm: false,
    isEdit: false,
    reportDetails: null,
  };

  componentDidMount() {
    this.fetchRiskReviews().then(data => {
      this.setState({ reviews: data });
    });
  }

  fetchRiskReviews = () => {
    return new Promise((res, rej) => {
      let { cookies } = this.props;
      let authToken = cookies.get('authToken');
      let reqObj = {
        url: API_ROUTES.getRiskReviewsCreator,
        isAuth: true,
        authToken,
        method: 'POST',
        showToggle: true,
      };

      fetchApi(reqObj)
        .then(data => {
          if (data.success) {
            res(data.output);
          }
        })
        .catch(e => rej(e));
    });
  };

  getRiskReviewsXML = () => {
    let { reviews } = this.state;
    let reviewsXML = reviews.map(l => {
      return (
        <InfoCard
          key={l.id}
          handleDelete={this.handleDelete}
          handleView={this.handleView}
          handleEdit={this.handleEdit}
          handleDownload={this.handleDownload}
          id={l.id}
          title={l.title}
          imgUrl={l.dispayurl}
          date={l.date}
        />
      );
    });

    return reviewsXML;
  };

  handleDelete = id => {
    this.setState({
      showAlert: true,
      reviewId: id,
    });
  };

  handleEdit = id => {
    this.setState(
      {
        advisoryId: id,
      },
      () => {
        this.fetchRiskReviewDetails();
      },
    );
  };

  fetchRiskReviewDetails = () => {
    const { advisoryId } = this.state;
    getRiskReviewDetails(advisoryId)
      .then(reportDetails => {
        this.setState({
          reportDetails: {
            ...reportDetails,
            advisoryurl: reportDetails?.riskreviewurl,
          },
        });

        this.handleEditForm({
          isEdit: true,
          showAddForm: true,
        });
      })
      .catch(e => console.log('Error fetching details', e));
  };

  handleEditForm = ({ isEdit, showAddForm }) => {
    this.setState({
      isEdit,
      showAddForm,
    });
  };

  handleConfirmDelete = id => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let reqObj = {
      url: `${API_ROUTES.deleteRiskReview}/${id}`,
      isAuth: true,
      authToken,
      method: 'POST',
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        if (data.success) {
          this.setState(
            {
              showAlert: false,
            },
            () => {
              let toastObj = {
                toastMsg: 'Review Deleted Successfully!',
                showToast: data.success,
              };
              store.dispatch(toggleToast(toastObj));
              this.fetchRiskReviews().then(data => {
                this.setState({ reviews: data });
              });
            },
          );
        }
      })
      .catch(e => console.log(e));
  };

  handleDownload = id => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let reqObj = {
      url: `${API_ROUTES.downloadRiskReview}/${id}`,
      isAuth: true,
      authToken,
      method: 'POST',
      isBlob: true,
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(blobObj => {
        let a = document.createElement('a');
        document.body.appendChild(a);
        let objUrl = new Blob([blobObj], { type: 'application/pdf' });
        let url = window.URL.createObjectURL(objUrl);
        a.href = url;
        a.download = 'risk-review-pdf';
        a.click();
        window.URL.revokeObjectURL(url);
      })
      .catch(e => console.log(e));
  };

  handleView = id => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let reqObj = {
      url: `${API_ROUTES.viewRiskReview}/${id}`,
      isAuth: true,
      authToken,
      method: 'POST',
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        if (data.success) {
          this.setState({
            showModal: true,
            modalUrl: data.output.viewimageurl,
          });
        }
      })
      .catch(e => console.log(e));
  };

  toggleModal = showModal => this.setState({ showModal });

  handleCloseAlert = () => this.setState({ showAlert: false });

  handleToggleAdd = showAddForm => this.setState({ showAddForm });

  handleSubmit = data => {
    // addNewReport
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    const { isEdit, advisoryId } = this.state;
    let { imageFile, document, title, selectedCountries, date } = data;
    const formData = new FormData();

    formData.append('displaypic', imageFile);
    formData.append('title', title);
    formData.append('riskreview', document);

    formData.append('countries', getValuesFromObjects(selectedCountries));
    formData.append('date', date || '');

    if (isEdit) {
      formData.append('advisoryid', advisoryId);
    }

    let params = {
      url: `${
        !isEdit ? API_ROUTES.addRiskReview : API_ROUTES.updateRiskReview
      }`,
      reqObj: {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${authToken}`,
        },
        body: formData,
      },
    };

    uploadApi(params.url, params.reqObj)
      .then(res => {
        if (res.success) {
          let toastObj = {
            toastMsg: !isEdit
              ? 'Review Added Successfully!'
              : 'Review Updated Successfully!',
            showToast: true,
          };
          store.dispatch(toggleToast(toastObj));
          this.handleToggleAdd();
          this.fetchRiskReviews().then(data => {
            this.setState({ reviews: data, isEdit: false });
          });
        } else {
          let toastObj = {
            toastMsg: 'Something went wrong!',
            showToast: true,
          };
          store.dispatch(toggleToast(toastObj));
        }
      })
      .catch(e => {});
  };

  handleCloseModal = () => {
    this.handleToggleAdd(false);
    this.setState({ isEdit: false });
  };

  render() {
    let { classes } = this.props;
    let {
      reviews,
      showModal,
      modalUrl,
      showAlert,
      reviewId,
      showAddForm,
      isEdit,
      reportDetails,
    } = this.state;

    return (
      <Fragment>
        <Actionables handleAdd={this.handleToggleAdd} />
        <InfoCardList>
          {reviews.length > 0 ? (
            this.getRiskReviewsXML()
          ) : (
            <Empty title="No Reviews Available" />
          )}
          <CustomModal
            showModal={showModal}
            closeModal={() => this.toggleModal(false)}
          >
            <div className={classes.rootIframe}>
              <span
                className="close-times"
                onClick={() => this.toggleModal(false)}
              >
                &times;
              </span>
              <iframe src={modalUrl} height="100%" width="100%" title="pdf" />
            </div>
          </CustomModal>
          <CustomModal
            showModal={showAddForm}
            closeModal={() => this.toggleModal(false)}
          >
            <div className={classes.rootAddForm}>
              <span
                className="close-times"
                onClick={() => this.handleCloseModal()}
              >
                &times;
              </span>
              <CustomReportsForm
                heading="Add New Report"
                titlePlaceHolder="Enter title"
                imagePlaceHolder="Upload Display Image"
                filePlaceHolder="Upload Review"
                handleSubmit={this.handleSubmit}
                showTopics={false}
                showCountries={true}
                showDate={true}
                isEdit={isEdit}
                details={reportDetails}
                showLinks={true}
              />
            </div>
          </CustomModal>
          {showAlert && (
            <AlertDialog
              isOpen={showAlert}
              title="Delete Review"
              description="You are about to delete an event, are you sure?"
              parentEventHandler={() => this.handleConfirmDelete(reviewId)}
              handleClose={() => this.handleCloseAlert()}
            />
          )}
        </InfoCardList>
      </Fragment>
    );
  }
}

export default withCookies(injectSheet(styles)(RiskReview));
