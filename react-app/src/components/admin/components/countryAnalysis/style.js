export default {
  rootRiskWatch: {
    background: '#F0F0F1',
    padding: 20,
  },
  innerRiskWatch: {
    '& .dmd__wrap': {
      margin: 0,
    },
  },
  rootThead: {
    fontWeight: 500,
    padding: '15px 3px',
    cursor: 'pointer',
    backgroundColor: 'var(--backgroundSolid)'
  },
  tRowWrap: {},
  rootTRowWrap: {
    padding: '0',
    borderBottom: 0,
    background: 'var(--backgroundSolid)',
    borderRadius: 5,
    '& p': {
      color: 'var(--whiteMediumEmp)',
    },
  },
  rowWrap: {
    backgroundColor: 'var(--backgroundSolid) !important',
    padding: '5px 0',
    borderRadius: '3px',
    color: '#f2f2f2',
    '&:nth-child(even)': {
      backgroundColor: '#101b2d',

      '& p': {
        color: 'var(--whiteMediumEmp)',
      },
    },
  },
  closeTimes: {
    position: 'absolute',
    color: '#000',
    fontSize: 30,
    right: 10,
    top: 10,
    cursor: 'pointer',
  },
  rootBtnGraph: {
    border: '1px solid var(--darkActive)',
    backgroundColor: 'transparent',
    color: 'var(--darkActive)',
    fontSize: '13px',
    minWidth: 32,
    '&:hover': {
      backgroundColor: 'var(--darkActive)',
      color: 'var(--whiteMediumEmp)',
    },

    '& i': {
      margin: 0,
    },
  },
  rootTCellNew: {
    justifyContent: 'space-around',
    '& p': {
      color: 'rgb(255, 255, 255) !important',
      fontWeight: 'bold',
      textAlign: 'center',
    },
  },
  metaWrap: {
    margin: '0 10px',
    padding: 10,
    backgroundColor: '#fff',
    borderRadius: 3,
    height: '100vh',
    overflowX: 'hidden',
    overflowY: 'scroll',
  },
  rootLabel: {
    color: 'rgba(255,255,255, 0.67) !important',
  },
  rootInput: {
    color: 'var(--whiteHighEmp) !important',
  },
  underline: {
    '&:after': {
      borderBottom: '2px solid #34495e !important',
    },
  },
  notchedOutline: {
    borderColor: 'var(--whiteHighEmp) !important ',
  },
  rootSelect: {
    color: 'rgba(255,255,255, 0.67) !important',
    borderBottom: '1px solid #eee !important',
  },
  countryName: {
    justifyContent: 'flex-start',
  },
};
