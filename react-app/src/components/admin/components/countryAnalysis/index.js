import React, { Component, Fragment } from 'react';
import './style.scss';
//components
import TableList from '../../../ui/tablelist';
import TableListCell from '../../../ui/tablelist/TableListCell';
import TableListRow from '../../../ui/tablelist/TableListRow';
import CustomModal from '../../../ui/modal';
import Empty from '../../../ui/empty';
//helpers
import { fetchApi } from '../../../../helpers/http/fetch';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import { withCookies } from 'react-cookie';
import styles from './style';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import injectSheet from 'react-jss';
import AlertViewUI from '../alertViewUI';
import Loader from '../../../ui/loader';
import clsx from 'clsx';
import { sortByCountryAnalysis } from '../../../../helpers/utils';
import { downloadCountryAnalysis } from '../../../../requestor/admin/requestor';
import DowloadExcel from './DownloadExcel';
@injectSheet(styles)
@withCookies
class CountryAnalysis extends Component {
  state = {
    tableData: [],
    showEventModal: false,
    eventDetails: null,
    showAlert: false,
    selectedCountry: '',
    showEventModal: false,
    viewDetails: false,
    isFetching: false,
    eventId: '',
    hoursList: Array.from({ length: 999 }, (_, i) => i + 1),
  };

  handleModal = show => this.setState({ showEventModal: show });

  handleChange = e => {
    let value = e.target.value;
    this.setState({ isFetching: true });
    this.setState({ selectedHour: value }, () => {
      this.getRimeFilteredFeed(value);
    });
  };
  getHoursList = () => {
    let { hoursList } = this.state;
    let hoursDropdown = [];
    hoursDropdown = hoursList.map(i => <MenuItem value={i}>{i}</MenuItem>);
    return hoursDropdown;
  };
  getRimeFilteredFeed = id => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let reqObj = {
      method: 'GET',
      isAuth: true,
      authToken,
      url: API_ROUTES.getCountryAnalysis + '/' + id,
    };

    fetchApi(reqObj)
      .then(data => {
        this.setState({
          tableData: data,
          eventDetails: data,
          isFetching: false,
        });
      })
      .catch(e => console.log(e));
  };

  getEvents = id => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let reqObj = {
      url: API_ROUTES.getCountryAnalysis + '/' + id,
      isAuth: true,
      authToken,
      method: 'GET',
      showToggle: true,
    };
    fetchApi(reqObj).then(data => this.setState({ tableData: data }));
  };

  handleViewEvent = id => {
    let { tableData } = this.state;
    tableData.map((item, index) => {
      if (index === id) {
        this.setState({ selectedCountry: item, viewDetails: true });
      }
    });

    this.handleModal(true);
  };
  handleViewDetails = () => {
    this.setState({ viewDetails: false });
  };

  getTableItems = () => {
    let { tableData } = this.state;
    const { classes } = this.props;
    let itemXML = [];

    itemXML = tableData.map((item, index) => {
      return (
        <TableListRow key={index + 1}>
          <TableListCell
            value={item.countryName || '-'}
            classes={{ root: classes.countryName }}
          />
          <TableListCell value={item.riskTrackerNumber || '-'} />
          <TableListCell value={item.rimeNumber || '-'} />
          <TableListCell value={item.crucial || '-'} />
          <TableListCell value={item.warning || '-'} />
          <TableListCell value={item.notificaton || '-'} />
          <TableListCell value={item.highrelevency || '-'} />
          <TableListCell value={item.mediumrelevency || '-'} />
          <TableListCell value={item.lowrelevency || '-'} />
          <TableListCell classes="btn--actions--wrap">
            <button
              className="btn--edit"
              onClick={() => this.handleViewEvent(index)}
            >
              View
            </button>
          </TableListCell>
        </TableListRow>
      );
    });

    return itemXML;
  };

  renderTheadTitles = (value, meta) => {
    return (
      <span className="thead--titles--wrapper">
        <p>{value.title}</p>
        <span>
          <i className="fas fa-sort"></i>
        </span>
      </span>
    );
  };

  handleSort = (type, sort) => {
    let sortedStateTable = sortByCountryAnalysis(
      this.state.eventDetails,
      type,
      sort,
    );
    this.setState({ tableData: sortedStateTable });
  };
  handleModal = show => {
    this.setState({ showEventModal: show });
  };

  handleShowAlert = eventId => {
    this.setState({ showAlert: true, eventId });
  };

  handleCloseAlert = () => {
    this.setState({ showAlert: false, eventId: '' });
  };

  downloadCountryAnalysis = () => {
    const { selectedHour } = this.state;
    downloadCountryAnalysis(selectedHour).then(objUrl => {
      let a = document.createElement('a');
      document.body.appendChild(a);
      let url = window.URL.createObjectURL(objUrl);
      a.href = url;
      a.download = 'country-analysis-report.xls';
      a.click();
      window.URL.revokeObjectURL(url);
    });
  };

  render() {
    let { tableData, selectedHour, viewDetails } = this.state;
    const { classes } = this.props;

    return (
      <div className="countryAnalysisWrap">
        <div className="flex-1">
          {!viewDetails && (
            <div className="left_nav">
              {tableData.length > 0 && (
                <DowloadExcel
                  bodyArr={tableData}
                  buttonText="EXPORT TABLE DATA"
                />
              )}
              <div className="col-span-1"></div>
              {selectedHour && (
                <button
                  className="solidBtn"
                  onClick={() => this.downloadCountryAnalysis()}
                >
                  Download Report
                </button>
              )}
            </div>
          )}
          {viewDetails && (
            <div className="countryAnalysis__header">
              <div className="back__button__wrapper">
                <button
                  className="back__button"
                  onClick={() => this.handleViewDetails()}
                >
                  <i className="fas fa-arrow-left"></i>
                </button>
              </div>
              <div className="country__header">
                <h1>Country Details</h1>
              </div>
            </div>
          )}
        </div>
        {this.state.isFetching && <Loader />}
        {!viewDetails && (
          <div className="countryAnalysisTopNavbar">
            <FormControl style={{ width: '110px' }}>
              <InputLabel
                classes={{
                  root: classes.rootLabel,
                }}
                htmlFor="select-hours"
              >
                Select Hours
              </InputLabel>
              <Select
                value={this.state.selectedHour}
                onChange={this.handleChange}
                inputProps={{
                  shrink: false,
                  name: 'Select Hours',
                  id: 'select-hours',
                  classes: {
                    root: classes.rootInput,
                    input: classes.rootInput,
                    select: classes.rootSelect,
                    icon: classes.rootIcon,
                  },
                }}
              >
                {this.getHoursList()}
              </Select>
            </FormControl>
          </div>
        )}
        {!viewDetails && (
          <div>
            {tableData.length > 0 ? (
              <Fragment>
                <div className={classes.tRowWrap}>
                  <TableListRow classes={{ root: classes.rootTRowWrap }}>
                    <TableListCell
                      type={'country'}
                      value={{
                        title: 'Country Name',
                      }}
                      renderProp={(value, meta) =>
                        this.renderTheadTitles(value, meta)
                      }
                      eventHandler={(type, sortBy) =>
                        this.handleSort(type, sortBy)
                      }
                      classes={{
                        root: clsx(classes.rootThead, 'msite-t-head'),
                      }}
                    />
                    <TableListCell
                      type={'riskTrackerAlerts'}
                      value={{
                        title: 'Risk Tracker alerts',
                      }}
                      renderProp={(value, meta) =>
                        this.renderTheadTitles(value, meta)
                      }
                      eventHandler={(type, sortBy) =>
                        this.handleSort(type, sortBy)
                      }
                      classes={{
                        root: clsx(classes.rootThead, 'msite-t-head'),
                      }}
                    />
                    <TableListCell
                      type={'rimeAlerts'}
                      value={{
                        title: 'Surfer Alerts',
                      }}
                      renderProp={(value, meta) =>
                        this.renderTheadTitles(value, meta)
                      }
                      eventHandler={(type, sortBy) =>
                        this.handleSort(type, sortBy)
                      }
                      classes={{
                        root: clsx(classes.rootThead, 'msite-t-head'),
                      }}
                    />
                    <TableListCell
                      type={'crucial'}
                      value={{
                        title: 'Crucial',
                      }}
                      renderProp={(value, meta) =>
                        this.renderTheadTitles(value, meta)
                      }
                      eventHandler={(type, sortBy) =>
                        this.handleSort(type, sortBy)
                      }
                      classes={{
                        root: clsx(classes.rootThead, 'msite-t-head'),
                      }}
                    />
                    <TableListCell
                      type={'warning'}
                      value={{
                        title: 'Warning',
                      }}
                      renderProp={(value, meta) =>
                        this.renderTheadTitles(value, meta)
                      }
                      eventHandler={(type, sortBy) =>
                        this.handleSort(type, sortBy)
                      }
                      classes={{
                        root: clsx(classes.rootThead, 'msite-t-head'),
                      }}
                    />{' '}
                    <TableListCell
                      type={'notificaton'}
                      value={{
                        title: 'Notificaton',
                      }}
                      renderProp={(value, meta) =>
                        this.renderTheadTitles(value, meta)
                      }
                      eventHandler={(type, sortBy) =>
                        this.handleSort(type, sortBy)
                      }
                      classes={{
                        root: clsx(classes.rootThead, 'msite-t-head'),
                      }}
                    />{' '}
                    <TableListCell
                      type={'highrelevency'}
                      value={{
                        title: 'High Relevency',
                      }}
                      renderProp={(value, meta) =>
                        this.renderTheadTitles(value, meta)
                      }
                      eventHandler={(type, sortBy) =>
                        this.handleSort(type, sortBy)
                      }
                      classes={{
                        root: clsx(classes.rootThead, 'msite-t-head'),
                      }}
                    />
                    <TableListCell
                      type={'mediumrelevency'}
                      value={{
                        title: 'Medium Relevency',
                      }}
                      renderProp={(value, meta) =>
                        this.renderTheadTitles(value, meta)
                      }
                      eventHandler={(type, sortBy) =>
                        this.handleSort(type, sortBy)
                      }
                      classes={{
                        root: clsx(classes.rootThead, 'msite-t-head'),
                      }}
                    />
                    <TableListCell
                      type={'lowrelevency'}
                      value={{
                        title: 'Low Relevency',
                      }}
                      renderProp={(value, meta) =>
                        this.renderTheadTitles(value, meta)
                      }
                      eventHandler={(type, sortBy) =>
                        this.handleSort(type, sortBy)
                      }
                      classes={{
                        root: clsx(classes.rootThead, 'msite-t-head'),
                      }}
                    />
                    <TableListCell value={'DETAILS'} />
                  </TableListRow>
                </div>
                <TableList classes={{ root: 'darkBlue' }}>
                  {tableData.length > 0 && this.getTableItems()}
                </TableList>
              </Fragment>
            ) : (
              <Empty title="No events available" />
            )}
          </div>
        )}
        {viewDetails && (
          <div>
            {tableData && (
              <AlertViewUI
                selectedCountry={this.state.selectedCountry}
                closeModal={() => this.handleModal(false)}
              />
            )}
          </div>
        )}
      </div>
    );
  }
}

export default CountryAnalysis;
