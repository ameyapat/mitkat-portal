import React, { useRef } from 'react';
import { useDownloadExcel } from 'react-export-table-to-excel';
import './style.scss';

const DowloadExcel = ({
  fileName = 'country-analysis',
  sheetName = 'country',
  buttonText = 'Export Excel',
  headerArr = [
    'Country Name',
    'Risk Tracker alerts',
    'Surfer Alerts',
    'Crucial',
    'Warning',
    'Notificaton',
    'High Relevency',
    'Medium Relevency',
    'Low Relevency',
  ],
  bodyArr = [],
}) => {
  const tableRef = useRef(null);
  const { onDownload } = useDownloadExcel({
    currentTableRef: tableRef.current,
    filename: fileName,
    sheet: sheetName,
  });
  return (
    <>
      <button className="solidBtn" onClick={onDownload}>
        {buttonText}
      </button>
      <div class="hide-table">
        <table ref={tableRef}>
          <tr>
            {headerArr.length > 0 &&
              headerArr.map(headerItem => (
                <th key={headerItem}>{headerItem}</th>
              ))}
          </tr>
          {bodyArr.length > 0 &&
            bodyArr.map(item => (
              <tr key={item.id}>
                <td>{item.countryName}</td>
                <td>{item.riskTrackerNumber}</td>
                <td>{item.rimeNumber}</td>
                <td>{item.crucial}</td>
                <td>{item.warning}</td>
                <td>{item.notificaton}</td>
                <td>{item.highrelevency}</td>
                <td>{item.mediumrelevency}</td>
                <td>{item.lowrelevency}</td>
              </tr>
            ))}
        </table>
      </div>
    </>
  );
};

export default DowloadExcel;
