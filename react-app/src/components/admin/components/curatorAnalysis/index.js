import React, { Component, Fragment } from 'react';
import './style.scss';
//components
import TableList from '../../../ui/tablelist';
import TableListCell from '../../../ui/tablelist/TableListCell';
import TableListRow from '../../../ui/tablelist/TableListRow';
import Empty from '../../../ui/empty';
//helpers
import { fetchApi } from '../../../../helpers/http/fetch';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import { withCookies } from 'react-cookie';
import styles from './style';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import injectSheet from 'react-jss';
import Loader from '../../../ui/loader';

@injectSheet(styles)
@withCookies
class CuratorAnalysis extends Component {
  state = {
    tableData: [],
    showEventModal: false,
    eventDetails: null,
    showAlert: false,
    selectedCountry: '',
    showEventModal: false,
    isFetching: false,
    eventId: '',
    hoursList: Array.from({ length: 999 }, (_, i) => i + 1),
  };

  handleModal = show => this.setState({ showEventModal: show });

  handleChange = e => {
    let value = e.target.value;
    this.setState({ isFetching: true });
    this.setState({ selectedHour: value }, () => {
      this.getRimeFilteredFeed(value);
    });
  };
  getHoursList = () => {
    let { hoursList } = this.state;
    let hoursDropdown = [];
    hoursDropdown = hoursList.map(i => <MenuItem value={i}>{i}</MenuItem>);
    return hoursDropdown;
  };
  getRimeFilteredFeed = id => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let reqObj = {
      method: 'GET',
      isAuth: true,
      authToken,
      url: API_ROUTES.getCuratorAnalysis + '/' + id,
    };

    fetchApi(reqObj)
      .then(data => {
        this.setState({ tableData: data, isFetching: false });
      })
      .catch(e => console.log(e));
  };

  getEvents = id => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let reqObj = {
      url: API_ROUTES.getCuratorAnalysis + '/' + id,
      isAuth: true,
      authToken,
      method: 'GET',
      showToggle: true,
    };
    fetchApi(reqObj).then(data => this.setState({ tableData: data }));
  };

  handleViewEvent = id => {
    let { tableData } = this.state;
    tableData.map((item, index) => {
      if (index === id) {
        this.setState({ selectedCountry: item });
      }
    });

    this.handleModal(true);
  };

  getTableItems = () => {
    let { tableData } = this.state;
    const { classes } = this.props;
    let itemXML = [];

    itemXML = tableData.map((item, index) => {
      return (
        <TableListRow key={index + 1}>
          <TableListCell
            value={index + 1 || '-'}
            classes={{ root: classes.srNo }}
          />
          <TableListCell value={item.userName || '-'} />
          <TableListCell value={item.events || '-'} />
          <TableListCell value={item.totalData || '-'} />
          <TableListCell value={item.coverageration || '-'} />
        </TableListRow>
      );
    });

    return itemXML;
  };

  handleModal = show => {
    this.setState({ showEventModal: show });
  };

  render() {
    let { tableData, eventDetails, showAlert, eventId } = this.state;
    const { classes } = this.props;

    return (
      <div className="countryAnalysisWrap">
        {this.state.isFetching && <Loader />}
        <div className="countryAnalysisTopNavbar">
          <FormControl style={{ width: '110px' }}>
            <InputLabel
              classes={{
                root: classes.rootLabel,
              }}
              htmlFor="select-hours"
            >
              Select Hours
            </InputLabel>
            <Select
              value={this.state.selectedHour}
              onChange={this.handleChange}
              inputProps={{
                shrink: false,
                name: 'Select Hours',
                id: 'select-hours',
                classes: {
                  root: classes.rootInput,
                  input: classes.rootInput,
                  select: classes.rootSelect,
                  icon: classes.rootIcon,
                },
              }}
            >
              {this.getHoursList()}
            </Select>
          </FormControl>
        </div>
        {tableData.length > 0 ? (
          <div className="tablelist__head">
            <div>
              <label>Sr Number</label>
            </div>
            <div>
              <label>Curator Name</label>
            </div>
            <div>
              <label>Number Of Events</label>
            </div>
            <div>
              <label>Total Data</label>
            </div>
            <div>
              <label>Coverage Ratio</label>
            </div>
          </div>
        ) : (
          <Empty title="No events available" />
        )}
        {tableData.length > 0 && (
          <TableList classes={{ root: 'darkBlue' }}>
            {this.getTableItems()}
          </TableList>
        )}
      </div>
    );
  }
}

export default CuratorAnalysis;
