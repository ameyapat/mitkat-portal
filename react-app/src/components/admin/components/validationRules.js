const validationRules = [
  {
    field: 'name',
    method: 'isEmpty',
    validWhen: false,
    message: 'Name is required',
  },
  {
    field: 'username',
    method: 'isEmpty',
    validWhen: false,
    message: 'Username is required',
  },
  {
    field: 'password',
    method: 'isEmpty',
    validWhen: false,
    message: 'Password is required',
  },
  {
    field: 'email',
    method: 'isEmpty',
    validWhen: false,
    message: 'Email is required',
  },
  {
    field: 'password',
    method: 'isStrongPassword',
    validWhen: true,
    message: 'Password',
  },
  // {
  //     field: 'clearance',
  //     method: 'isEmpty',
  //     validWhen: false,
  //     message: 'Clearance is required'
  // }
];

export const clientValidationRules = [
  {
    field: 'companyName',
    method: 'isEmpty',
    validWhen: false,
    message: 'Company name is required',
  },
  // {
  //     field: 'startDate',
  //     method: 'isEmpty',
  //     validWhen: false,
  //     message: 'Start Date is required'
  // },
  // {
  //     field: 'endDate',
  //     method: 'isEmpty',
  //     validWhen: false,
  //     message: 'End Date is required'
  // },
  // {
  //     field: 'contactPerson',
  //     method: 'isEmpty',
  //     validWhen: false,
  //     message: 'Contact person is required'
  // },
  // {
  //     field: 'contactNumber',
  //     method: 'isEmpty',
  //     validWhen: false,
  //     message: 'Contact number is required'
  // },
  // {
  //     field: 'clientEmails',
  //     method: 'isEmpty',
  //     validWhen: false,
  //     message: 'Email is required'
  // },
  {
    field: 'selectedProduct',
    method: 'isEmpty',
    validWhen: false,
    message: 'Type of product is required',
  },
  {
    field: 'selectedIndustry',
    method: 'isEmpty',
    validWhen: false,
    message: 'Impact industry is required',
  },
  // {
  //     field: 'selectedCities',
  //     method: 'isEmpty',
  //     validWhen: false,
  //     message: 'Cities is required'
  // },
  // {
  //     field: 'selectedStates',
  //     method: 'isEmpty',
  //     validWhen: false,
  //     message: 'States is required'
  // },
  // {
  //     field: 'selectedCountries',
  //     method: 'isEmpty',
  //     validWhen: false,
  //     message: 'Countries is required'
  // },
];

export default validationRules;
