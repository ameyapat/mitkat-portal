const validationRules = [
  {
    field: 'eventTitle',
    method: 'isEmpty',
    validWhen: false,
    message: 'Event title is required',
  },
  {
    field: 'coordinates',
    method: 'isEmpty',
    validWhen: false,
    message: 'Atleast one location is required!',
  },
  // {
  //   field: 'address',
  //   method: 'isEmpty',
  //   validWhen: false,
  //   message: 'Address is required',
  // },
  // {
  //     field: 'description',
  //     method: 'isEmpty',
  //     validWhen: false,
  //     message: 'Description is required'
  // },
  // {
  //     field: 'background',
  //     method: 'isEmpty',
  //     validWhen: false,
  //     message: 'Background is required'
  // },
  // {
  //     field: 'impactAnalysis',
  //     method: 'isEmpty',
  //     validWhen: false,
  //     message: 'Impact Analysis is required'
  // },
  // {
  //     field: 'recommendations',
  //     method: 'isEmpty',
  //     validWhen: false,
  //     message: 'recommendations is required'
  // },
  // {
  //     field: 'eventSource',
  //     method: 'isEmpty',
  //     validWhen: false,
  //     message: 'Event Source is required'
  // },
  {
    field: 'eventDate',
    method: 'isEmpty',
    validWhen: false,
    message: 'Event date is required',
  },
  {
    field: 'validityDate',
    method: 'isEmpty',
    validWhen: false,
    message: 'Validity date is required',
  },
  {
    field: 'selectedRiskLevel',
    method: 'isEmpty',
    validWhen: false,
    message: 'RiskLevel is required',
  },
  {
    field: 'selectedCategory',
    method: 'isEmpty',
    validWhen: false,
    message: 'Category is required',
  },
  {
    field: 'selectedSubCategory',
    method: 'isEmpty',
    validWhen: false,
    message: 'SubCategory is required',
  },
  {
    field: 'selectedStory',
    method: 'isEmpty',
    validWhen: false,
    message: 'Story is required',
  },
  {
    field: 'impactedIndustries',
    method: 'isEmpty',
    validWhen: false,
    message: 'Industries is required',
  },
  {
    field: 'selectedCountries',
    method: 'isEmpty',
    validWhen: false,
    message: 'Countries is required',
  },
  // {
  //     field: 'selectedStates',
  //     method: 'isEmpty',
  //     validWhen: false,
  //     message: 'States is required'
  // },
  // {
  //     field: 'selectedMetros',
  //     method: 'isEmpty',
  //     validWhen: false,
  //     message: 'Metros is required'
  // },
];

export default validationRules;
