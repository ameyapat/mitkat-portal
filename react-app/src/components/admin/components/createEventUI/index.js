import React, { Component } from 'react';
import './style.scss';
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';
import Select from 'react-select';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Radio from '@material-ui/core/Radio';
import Button from '@material-ui/core/Button';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import MultiSelect from '../../../ui/multiSelect';
import Checkbox from '@material-ui/core/Checkbox';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import MultipleLocations from '../multipleLocations';
import CustomMap from '../../../ui/customMap';
import CustomLabel from '../../../ui/customLabel';
import AlertDialog from '../../../ui/alertDialog';
import { withRouter } from 'react-router-dom';
import { withCookies } from 'react-cookie';
import { getListData } from '../../helpers/utils';
import {
  filterArrObjects,
  fetchCountries,
  fetchStates,
  fetchCities,
  fetchIndustries,
  getImpactRadius,
  filterByLatLng,
} from '../../../../helpers/utils';
import { getLiveTopics } from '../../../../api/api';
import { fetchApi } from '../../../../helpers/http/fetch';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import store from '../../../../store';
import { toggleToast, refreshEvents } from '../../../../actions/';
import { updateMultipleLocation } from '../../../../actions/multipleLocationActions';
import injectSheet from 'react-jss';
import style from './style';
import validationRules from './validationRules';
import FormValidator from '../../../../helpers/validation/FormValidator';
import { connect } from 'react-redux';
import { DatePicker } from 'material-ui-pickers';
import EditableItem from '../EditableItem';
import MultipleLocationList from '../../../ui/multipleLocationList';

const searchOptions = {
  componentRestrictions: { country: 'in' },
};

const stateRules = [
  {
    field: 'selectedStates',
    method: 'isEmpty',
    validWhen: false,
    message: 'States is required',
  },
];

const updatedRules = [...validationRules, ...stateRules];

const customStyles = {
  control: provided => ({
    ...provided,
    backgroundColor: 'var(--backgroundSolid)',
    color: '#ffffff',
  }),
  option: (provided, state) => ({
    ...provided,
    zIndex: '99999',
  }),
  placeholder: provided => ({
    ...provided,
    color: '#ffffff',
  }),
  singleValue: provided => ({
    ...provided,
    color: '#ffffff',
  }),
};

@withCookies
@withRouter
@injectSheet(style)
@connect(state => {
  return {
    refreshEvents: state.appState.refreshEvents,
    multipleLocations: state.multipleLocations,
  };
})
class CreateEventUI extends Component {
  validator = new FormValidator(validationRules);
  submitted = false;

  state = {
    address: '',
    riskLevels: [],
    riskCategories: [],
    subRiskCategories: [],
    typeOfStories: [],
    countries: [],
    states: [],
    metros: [],
    industries: [],
    selectedRiskLevel: { value: 2, label: 'Low' },
    selectedCategory: { value: 1, label: 'Technology' },
    selectedSubCategory: '',
    selectedStory: '',
    selectedCountries: [],
    selectedStates: [],
    selectedMetros: [],
    eventDetails: '',
    eventDate: new Date(),
    eventDateNew: new Date(),
    validityDate: new Date(),
    eventSource: '',
    eventTitle: '',
    location: '',
    descArr: [
      {
        id: 1,
        label: 'Description',
        type: 'description',
        title: 'description',
      },
    ],
    showDescList: false,
    showBgList: false,
    description: '',
    description2: '',
    descInputItem: '',
    descListItem: '',
    bgListItem: '',
    descList: [],
    showDescBtn: true,
    showBgBtn: true,
    showDescAddListBtn: true,
    showBgAddListBtn: true,
    bgArr: [
      { id: 1, label: 'Background', type: 'background', title: 'background' },
    ],
    background: '',
    background2: '',
    bgList: [],
    impactArr: [
      {
        id: 1,
        label: 'Impact Analysis',
        type: 'impactAnalysis',
        title: 'impactAnalysis',
      },
    ],
    showImpactBtn: true,
    showImpactAddListBtn: true,
    showImpactList: false,
    impactAnalysis: '',
    impactAnalysis2: '',
    impactList: [],
    impactListItem: '',
    recomArr: [
      {
        id: 1,
        label: 'Recommendation',
        type: 'recommendations',
        title: 'recommendations',
      },
    ],
    recommendations: '',
    recommendation2: '',
    recomList: [],
    recomListItem: '',
    showRecomBtn: true,
    showRecomAddListBtn: true,
    showRecomList: false,
    isImportant: false,
    isLive: false,
    impactedIndustries: [{ value: 5, label: 'Other' }],
    location: {
      lat: '',
      lng: '',
    },
    validation: this.validator.valid(),
    showAlert: false,
    calImpactRadius: '',
    blockage: 0,
    isLocationSpecific: false,
    allTopics: [],
    allLiveTopics: [],
    selectedTopic: '',
    selectedLiveTopic: '',
    coordinates: [],
  };

  componentDidMount() {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');

    this.getRiskLevels('riskLevel').then(riskLevels =>
      this.setState({ riskLevels }, () => console.log(this.state)),
    );
    this.getRiskCategories('riskCategory').then(riskCategories => {
      this.setState({ riskCategories });
      this.getInitialSubCategories();
    });
    this.getStories('typeOfStory').then(typeOfStories =>
      this.setState({ typeOfStories }),
    );
    fetchCountries(authToken).then(countries => this.setState({ countries }));
    fetchStates(authToken).then(states => this.setState({ states }));
    fetchCities(authToken).then(metros => this.setState({ metros }));
    fetchIndustries(authToken).then(industries =>
      this.setState({ industries }),
    );
    getLiveTopics()
      .then(data => {
        let allLiveTopics = [];
        allLiveTopics = data.map(t => {
          let allLiveTopicsObj = {};
          allLiveTopicsObj['value'] = t.id;
          allLiveTopicsObj['label'] = t.topicName;
          return allLiveTopicsObj;
        });
        this.setState({ allLiveTopics }, () => console.log(this.state));
      })
      .catch(e => console.log('Error fetching live topics', e));
  }

  componentDidUpdate(prevProps) {
    const prevMultipleLocation =
      prevProps.multipleLocations.selectedMultipleLocations;
    const currMultipleLocation = this.props.multipleLocations
      .selectedMultipleLocations;
    if (currMultipleLocation !== prevMultipleLocation) {
      this.setState({ coordinates: currMultipleLocation });
    }
  }

  getReverseGeoCode = () => {
    let {
      eventDetails: { coordinates },
    } = this.props;
    coordinates.map(item => {
      this.fetchFormattedAddress(item.latitude, item.longitude);
    });
  };

  getInitialSubCategories = () => {
    const { riskCategories } = this.state;

    this.getSubRiskCategories(riskCategories[0], 'subRiskCategory').then(
      subRiskCategories => {
        this.setState({ subRiskCategories }, () => {
          this.handleImpactRadius();
        });
      },
    );
  };

  handleImpactRadius = () => {
    let {
      selectedSubCategory,
      selectedRiskLevel,
      selectedCategory,
      riskCategories,
    } = this.state;

    const subCategories = Object.values(riskCategories).filter(
      category => category?.value === selectedCategory?.value,
    )[0]?.subCategories;
    const impactRadiusArr = subCategories.filter(
      category => category?.id === selectedSubCategory?.value,
    );
    const calImpactRadius = impactRadiusArr?.[0]?.impRadius?.filter(
      item => item?.riskLevelId === selectedRiskLevel?.value,
    )[0]?.impactRadius;

    this.setState({ calImpactRadius });
  };

  handleInputChange = (e, type) => {
    this.setState({ [type]: e.target.value });
  };

  handleDateChange = (type, value) => this.setState({ [type]: value });

  handleSingleSelect = (value, type) => {
    this.setState({ [type]: value }, () => {
      if (
        type === 'selectedRiskLevel' ||
        type === 'selectedCategory' ||
        type === 'selectedSubCategory'
      ) {
        this.handleImpactRadius();
      }
    });
  };

  handleSingleSelectSub = (value, type) => {
    this.setState({ [type]: value }, () => {
      this.fetchSubCategory(value);
    });
  };

  fetchSubCategory = category => {
    this.getSubRiskCategories(category, 'subRiskCategory').then(
      subRiskCategories => {
        this.setState({ selectedSubCategory: subRiskCategories[0] });
        this.setState({ subRiskCategories });
      },
    );
  };

  handleChange = (e, type) => {
    switch (type) {
      case 'riskLevel':
        this.setState({ selectedRiskLevel: e.target.value });
        break;
      case 'riskCategory':
        this.setState({ selectedCategory: e.target.value });
        break;
      case 'typeOfStory':
        this.setState({ selectedStory: e.target.value });
        break;
      default:
        break;
    }
  };

  getStories = type => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');

    return new Promise((res, rej) => {
      getListData(type, authToken)
        .then(data => {
          let { typeOfStories } = this.state;
          data.map(t => {
            let typeOfStoryObj = {};
            typeOfStoryObj['value'] = t.id;
            typeOfStoryObj['label'] = t.regiontype;
            typeOfStories.push(typeOfStoryObj);
          });
          res(typeOfStories);
        })
        .catch(e => rej(e));
    });
  };

  getRiskCategories = type => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');

    return new Promise((res, rej) => {
      getListData(type, authToken)
        .then(data => {
          let { riskCategories } = this.state;
          data.map(t => {
            let riskCategoriesObj = {};
            riskCategoriesObj['value'] = t.id;
            riskCategoriesObj['label'] = t.riskcategory;
            riskCategoriesObj['subCategories'] = t?.subcategories || [];

            riskCategories.push(riskCategoriesObj);
          });
          res(riskCategories);
        })
        .catch(e => rej(e));
    });
  };

  getSubRiskCategories = (category, type) => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    const subRiskCategories = [];

    return new Promise((res, rej) => {
      getListData(type, authToken, category)
        .then(data => {
          data.map(t => {
            let subRiskCategoriesObj = {};
            subRiskCategoriesObj['value'] = t.id;
            subRiskCategoriesObj['label'] = t.risksubcategory;
            subRiskCategories.push(subRiskCategoriesObj);
          });
          res(subRiskCategories);
        })
        .catch(e => rej(e));
    });
  };

  getRiskLevels = type => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    return new Promise((res, rej) => {
      getListData(type, authToken)
        .then(data => {
          let { riskLevels } = this.state;
          data.map(t => {
            let riskLevelsObj = {};
            riskLevelsObj['value'] = t.id;
            riskLevelsObj['label'] = t.level;
            riskLevels.push(riskLevelsObj);
          });
          res(riskLevels);
        })
        .catch(e => rej(e));
    });
  };

  hasIndia = () => {
    let { selectedCountries } = this.state;
    let hasIndia = false;
    selectedCountries.map(c =>
      c.label === 'India'
        ? (hasIndia = true)
        : c.label === 'United States of America'
        ? (hasIndia = true)
        : (hasIndia = false),
    );

    return hasIndia;
  };

  handleEventSubmit = e => {
    e.preventDefault();
    let {
      history,
      multipleLocations: { selectedMultipleLocations },
    } = this.props;
    this.validator = this.hasIndia()
      ? new FormValidator(updatedRules)
      : new FormValidator(validationRules);
    let validation = this.validator.validate(this.state);
    this.setState({ validation });
    this.submitted = true;

    if (validation.isValid) {
      let { cookies } = this.props;
      let authToken = cookies.get('authToken');
      let {
        eventDetails,
        selectedCountries,
        eventSource,
        eventDate,
        eventDateNew,
        validityDate,
        background,
        background2,
        bgList,
        eventTitle,
        impactAnalysis,
        impactAnalysis2,
        impactList,
        recommendations,
        recommendation2,
        recomList,
        descList,
        description,
        description2,
        selectedRiskLevel,
        selectedCategory,
        selectedSubCategory,
        impactedIndustries,
        selectedMetros,
        selectedStates,
        isImportant,
        isLive,
        selectedStory,
        location: { lat, lng },
        calImpactRadius,
        blockage,
        isLocationSpecific,
        selectedLiveTopic,
      } = this.state;

      eventDetails = {
        title: eventTitle,
        description: description,
        description2: description2,
        descriptionbullets: descList,
        background: background,
        background2: background2,
        backgroundbullets: bgList,
        impact: impactAnalysis,
        impact2: impactAnalysis2,
        impactbullets: impactList,
        recommendation: recommendations,
        recommendation2: recommendation2,
        recommendationbullets: recomList,
        links: eventSource,
        latitude: lat,
        longitude: lng,
        storydate: eventDate,
        eventdate: eventDateNew,
        validitydate: validityDate,
        riskLevel: selectedRiskLevel.value,
        riskcategory: selectedCategory.value,
        risksubcategoryid: selectedSubCategory.value,
        regionType: selectedStory.value,
        impactindustry: filterArrObjects(impactedIndustries),
        countries: filterArrObjects(selectedCountries),
        state: filterArrObjects(selectedStates),
        cities: filterArrObjects(selectedMetros),
        importance: isImportant,
        liveevent: isLive,
        impactRadius: calImpactRadius,
        blockage,
        islocationspecific: isLocationSpecific,
        topicname: selectedLiveTopic.topicName,
        topicid: selectedLiveTopic.value,
        coordinates: filterByLatLng(selectedMultipleLocations),
      };

      let reqObj = {
        url: API_ROUTES.addEventCreator,
        data: eventDetails,
        isAuth: true,
        authToken,
        method: 'POST',
        showToggle: true,
      };

      fetchApi(reqObj).then(data => {
        if (data && data.status == 200) {
          let toastObj = {
            toastMsg: 'Event Created Successfully!',
            showToast: true,
          };
          this.props.closeEventModal('createEvent');
          store.dispatch(toggleToast(toastObj));
          history.push(this.props.match.url + '/dailyRiskTracker');
          store.dispatch(refreshEvents(true));
          store.dispatch(updateMultipleLocation([]));
        } else {
          let toastObj = {
            toastMsg: 'Something went wrong, Please try again',
            showToast: true,
          };
          store.dispatch(toggleToast(toastObj));
        }
      });
    }
  };

  riskLevelsXML = () => {
    let { riskLevels } = this.state;
    let riskLevelXML = [];
    riskLevelXML = riskLevels.map(item => (
      <MenuItem key={item.id} value={item.id}>
        {item.level}
      </MenuItem>
    ));
    return riskLevelXML;
  };

  riskCategoriesXML = () => {
    let { riskCategories } = this.state;
    let riskCategoryXML = [];
    riskCategoryXML = riskCategories.map(item => (
      <MenuItem key={item.id} value={item.id}>
        {item.riskcategory}
      </MenuItem>
    ));
    return riskCategoryXML;
  };

  storiesXML = () => {
    let { typeOfStories } = this.state;
    let storiesXML = [];
    storiesXML = typeOfStories.map(item => (
      <MenuItem key={item.id} value={item.id}>
        {item.regiontype}
      </MenuItem>
    ));
    return storiesXML;
  };

  handleMultiSelect = (value, type) => {
    let values = this.state[type];
    if (values.includes(value)) {
      let index = values.indexOf(value);
      values.splice(index, 1);
    } else {
      values = [...value];
    }
    this.setState({ [type]: values });
  };

  handleAddressChange = address => {
    this.setState({ address });
  };

  handleSelect = address => {
    geocodeByAddress(address)
      .then(results => getLatLng(results[0]))
      .then(latLng => {
        let { location } = this.state;
        location['lat'] = latLng.lat;
        location['lng'] = latLng.lng;
        this.setState({ location, address });
      })
      .catch(error => {
        console.error('Error', error);
        let toastObj = {
          showToast: true,
          toastObj: 'Error getting location, try again!',
        };
        store.dispatch(toggleToast(toastObj));
      });
  };

  addPara = type => {
    let { descArr, bgArr, impactArr, recomArr } = this.state;
    switch (type) {
      case 'description':
        let newPara = [
          {
            id: 2,
            label: 'Description 2',
            type: 'description2',
            title: 'description2',
          },
        ];
        this.setState({
          descArr: [...descArr, ...newPara],
          showDescBtn: false,
        });
        break;
      case 'background':
        let newBg = [
          {
            id: 2,
            label: 'Background 2',
            type: 'background2',
            title: 'background2',
          },
        ];
        this.setState({ bgArr: [...bgArr, ...newBg], showBgBtn: false });
        break;
      case 'impactAnalysis':
        let newImpact = [
          {
            id: 2,
            label: 'Impact Analysis 2',
            type: 'impactAnalysis2',
            title: 'impactAnalysis2',
          },
        ];
        this.setState({
          impactArr: [...impactArr, ...newImpact],
          showImpactBtn: false,
        });
        break;
      case 'recom':
        let newRecom = [
          {
            id: 2,
            label: 'Recommendation 2',
            type: 'recommendation2',
            title: 'recommendation2',
          },
        ];
        this.setState({
          recomArr: [...recomArr, ...newRecom],
          showRecomBtn: false,
        });
        break;
      default:
        console.log('default...');
        break;
    }
  };

  addBullets = type => {
    switch (type) {
      case 'description':
        this.setState({ showDescList: true, showDescAddListBtn: false });
        break;
      case 'background':
        this.setState({ showBgList: true, showBgAddListBtn: false });
        break;
      case 'impactAnalysis':
        this.setState({ showImpactList: true, showImpactAddListBtn: false });
        break;
      case 'recom':
        this.setState({ showRecomList: true, showRecomAddListBtn: false });
        break;
      default:
        console.log('err');
        break;
    }
  };

  showBulletPoints = type => {
    switch (type) {
      case 'desc':
        let { descList } = this.state;
        let descListXML = [];
        return (descListXML = descList.map((d, idx) => (
          <EditableItem
            key={idx}
            idx={idx}
            removeItemFromList={this.removeItemFromList}
            replaceItemFromList={this.replaceItemFromList}
            updateList={this.updateList}
            description={d}
            list={descList}
            type={type}
          />
        )));
      case 'bg':
        let { bgList } = this.state;
        let bgListXML = [];
        return (bgListXML = bgList.map((d, idx) => (
          <EditableItem
            key={idx}
            idx={idx}
            updateList={this.updateList}
            removeItemFromList={this.removeItemFromList}
            replaceItemFromList={this.replaceItemFromList}
            description={d}
            list={bgList}
            type={type}
          />
        )));
      case 'impactAnalysis':
        let { impactList } = this.state;
        let impactListXML = [];
        return (impactListXML = impactList.map((d, idx) => (
          <EditableItem
            key={idx}
            idx={idx}
            updateList={this.updateList}
            removeItemFromList={this.removeItemFromList}
            replaceItemFromList={this.replaceItemFromList}
            description={d}
            list={impactList}
            type={type}
          />
        )));
      case 'recom':
        let { recomList } = this.state;
        let recomListXML = [];
        return (recomListXML = recomList.map((d, idx) => (
          <EditableItem
            key={idx}
            idx={idx}
            updateList={this.updateList}
            removeItemFromList={this.removeItemFromList}
            replaceItemFromList={this.replaceItemFromList}
            description={d}
            list={recomList}
            type={type}
          />
        )));
      default:
        console.log('default...');
        break;
    }
  };

  addItemToList = type => {
    switch (type) {
      case 'description':
        let { descList, descListItem } = this.state;
        if (descListItem !== '') {
          descList.push(descListItem);
        } else {
          return;
        }
        this.setState({ descList, descListItem: '' });
        break;
      case 'background':
        let { bgList, bgListItem } = this.state;
        if (bgListItem !== '') {
          bgList.push(bgListItem);
        } else {
          return;
        }
        this.setState({ bgList, bgListItem: '' });
        break;
      case 'impactAnalysis':
        let { impactList, impactListItem } = this.state;
        if (impactListItem !== '') {
          impactList.push(impactListItem);
        } else {
          return;
        }
        this.setState({ impactList, impactListItem: '' });
        break;
      case 'recom':
        let { recomList, recomListItem } = this.state;
        if (recomListItem !== '') {
          recomList.push(recomListItem);
        } else {
          return;
        }
        this.setState({ recomList, recomListItem: '' });
        break;
      default:
        console.log('default');
    }
  };

  updateList = (type, updatedList) => {
    switch (type) {
      case 'desc':
        this.setState({ descList: updatedList });
        break;
      case 'bg':
        this.setState({ bgList: updatedList });
        break;
      case 'impactAnalysis':
        this.setState({ impactList: updatedList });
        break;
      case 'recom':
        this.setState({ recomList: updatedList });
        break;
      default:
        console.log('err!!');
        break;
    }
  };

  generateDescription = () => {
    let { descArr } = this.state;
    let descXML = [];
    let { classes } = this.props;
    descXML = descArr.map(desc => {
      return (
        <div className="event--description">
          <TextField
            id={`${desc.type}_${desc.id}`}
            label={desc.label}
            multiline
            fullWidth
            rowsMax="6"
            value={this.state[desc.type]}
            onChange={e => this.handleInputChange(e, desc.type)}
            margin="normal"
            defaultValue="Enter Event Description"
            variant="outlined"
            InputLabelProps={{
              shrink: true,
              classes: {
                root: classes.rootLabel,
              },
            }}
            InputProps={{
              maxLength: 5000,
              classes: {
                root: classes.rootInput,
                input: classes.rootInput,
                notchedOutline: classes.notchedOutline,
              },
              endAdornment: null,
            }}
          />
        </div>
      );
    });
    return descXML;
  };

  generateBg = () => {
    let { bgArr } = this.state;
    let bgXML = [];
    let { classes } = this.props;
    bgXML = bgArr.map(bg => {
      return (
        <div className="event--bg">
          <TextField
            id={`${bg.type}_${bg.id}`}
            label={bg.label}
            multiline
            fullWidth
            rowsMax="6"
            value={this.state[bg.type]}
            onChange={e => this.handleInputChange(e, bg.type)}
            defaultValue="Enter Event Background"
            margin="normal"
            variant="outlined"
            InputLabelProps={{
              shrink: true,
              classes: {
                root: classes.rootLabel,
              },
            }}
            InputProps={{
              maxLength: 5000,
              classes: {
                root: classes.rootInput,
                input: classes.rootInput,
                notchedOutline: classes.notchedOutline,
              },
              endAdornment: null,
            }}
          />
        </div>
      );
    });
    return bgXML;
  };

  generateImpactAnalysis = () => {
    let { impactArr } = this.state;
    let impactXML = [];
    let { classes } = this.props;
    impactXML = impactArr.map(i => {
      return (
        <div className="event--impactAnalysis">
          <TextField
            id={`${i.type}_${i.id}`}
            label={i.label}
            multiline
            fullWidth
            rowsMax="6"
            value={this.state[i.type]}
            onChange={e => this.handleInputChange(e, i.type)}
            margin="normal"
            defaultValue="Enter Impact Analysis"
            variant="outlined"
            InputLabelProps={{
              shrink: true,
              classes: {
                root: classes.rootLabel,
              },
            }}
            InputProps={{
              maxLength: 5000,
              classes: {
                root: classes.rootInput,
                input: classes.rootInput,
                notchedOutline: classes.notchedOutline,
              },
              endAdornment: null,
            }}
          />
        </div>
      );
    });
    return impactXML;
  };

  generateRecommendation = () => {
    let { recomArr } = this.state;
    let recomXML = [];
    let { classes } = this.props;
    recomXML = recomArr.map(i => {
      return (
        <div className="event--recommendation">
          <TextField
            id={`${i.type}_${i.id}`}
            label={i.label}
            multiline
            fullWidth
            rowsMax="6"
            value={this.state[i.type]}
            onChange={e => this.handleInputChange(e, i.type)}
            margin="normal"
            defaultValue="Enter Recommendations"
            variant="outlined"
            InputLabelProps={{
              shrink: true,
              classes: {
                root: classes.rootLabel,
              },
            }}
            InputProps={{
              maxLength: 5000,
              classes: {
                root: classes.rootInput,
                input: classes.rootInput,
                notchedOutline: classes.notchedOutline,
              },
              endAdornment: null,
            }}
          />
        </div>
      );
    });
    return recomXML;
  };

  handleShowAlert = () => {
    this.setState({ showAlert: true });
  };

  handleCloseAlert = () => {
    this.setState({ showAlert: false });
  };

  handleCloseModal = () => {
    this.setState({ showAlert: false });
    this.props.closeEventModal('createEvent');
  };

  handleIsLocSpecific = e => {
    this.setState({ isLocationSpecific: !this.state.isLocationSpecific });
  };

  toggleMultipleLocationModal = showMultipleLocationsModal =>
    this.setState({ showMultipleLocationsModal });

  render() {
    let {
      classes,
      multipleLocations: { selectedMultipleLocations },
    } = this.props;
    let {
      riskLevels,
      riskCategories,
      subRiskCategories,
      countries,
      states,
      metros,
      industries,
      selectedCountries,
      selectedStates,
      selectedRiskLevel,
      selectedCategory,
      selectedSubCategory,
      typeOfStories,
      selectedStory,
      selectedMetros,
      impactedIndustries,
      showDescBtn,
      showBgBtn,
      showDescList,
      showDescAddListBtn,
      descList,
      showBgList,
      showBgAddListBtn,
      bgList,
      impactList,
      showImpactBtn,
      showImpactAddListBtn,
      showImpactList,
      showRecomBtn,
      showRecomAddListBtn,
      showRecomList,
      recomList,
      showAlert,
      calImpactRadius,
      blockage,
      selectedTopic,
      selectedLiveTopic,
      allTopics,
      allLiveTopics,
    } = this.state;
    let validation = this.submitted
      ? this.validator.validate(this.state)
      : this.state.validation;

    return (
      <div id="createEventMitkat" className="createEvent__inner">
        <h3>Create a new Event, for quick user integration</h3>
        <div className="field--create--event">
          <section className="modal--section">
            {true && (
              <section id="reactGooglePlaces" className="google--places">
                <button
                  className="addLocationBtn"
                  onClick={() => this.toggleMultipleLocationModal(true)}
                >
                  <span className="addLocationBtn__icon">
                    <i className="fas fa-map-marker-alt"></i>
                  </span>
                  <span className="addLocationBtn__text">
                    {selectedMultipleLocations.length > 0
                      ? 'Add/Update Location'
                      : 'Add New Location*'}
                  </span>
                </button>
                {validation.coordinates.isInvalid && (
                  <span className="error--text">
                    {validation.coordinates.message}
                  </span>
                )}
                <MultipleLocations
                  showModal={this.state.showMultipleLocationsModal}
                  closeMultipleLocationModal={this.toggleMultipleLocationModal}
                >
                  <div
                    id="multipleLocationMap"
                    className="modal__inner"
                    style={{ overflow: 'hidden' }}
                  >
                    <CustomMap />
                  </div>
                </MultipleLocations>
                {selectedMultipleLocations.length > 0 && (
                  <MultipleLocationList
                    selectedMultipleLocations={selectedMultipleLocations}
                    type="detail"
                  />
                )}
              </section>
            )}
            {false && (
              <div>
                <label className="location--txt">
                  <i className="icon-uniE9F1" /> Enter Event Location
                  <span className="asterisk">*</span>
                </label>
                <PlacesAutocomplete
                  value={this.state.address}
                  onChange={this.handleAddressChange}
                  onSelect={this.handleSelect}
                >
                  {({
                    getInputProps,
                    suggestions,
                    getSuggestionItemProps,
                    loading,
                  }) => (
                    <div>
                      {validation.address.isInvalid && (
                        <span className="error--text">
                          {validation.address.message}
                        </span>
                      )}
                      <input
                        {...getInputProps({
                          placeholder: 'Search Places ...',
                          className: 'location-search-input',
                        })}
                      />
                      {suggestions && (
                        <div className="autocomplete-dropdown-container">
                          {loading && (
                            <div className="loader-places">
                              <p>Loading...</p>
                            </div>
                          )}
                          {suggestions.map(suggestion => {
                            const className = suggestion.active
                              ? 'suggestion-item--active'
                              : 'suggestion-item';
                            // inline style for demonstration purpose
                            const style = suggestion.active
                              ? {
                                  backgroundColor: '#fafafa',
                                  cursor: 'pointer',
                                }
                              : {
                                  backgroundColor: '#ffffff',
                                  cursor: 'pointer',
                                };
                            return (
                              <div
                                {...getSuggestionItemProps(suggestion, {
                                  className,
                                  style,
                                })}
                              >
                                <span className="list--item">
                                  {suggestion.description}
                                </span>
                              </div>
                            );
                          })}
                        </div>
                      )}
                    </div>
                  )}
                </PlacesAutocomplete>
              </div>
            )}
            {/** Event single select starts */}
            <div className="twoColWrap multi--selects">
              <div className="col-2">
                {/** change to single select is fine, change handle multiselect method to single */}
                <CustomLabel
                  title="Type Of Story"
                  classes="select--label"
                  isRequired={true}
                />
                <Select
                  defaultValue={selectedStory}
                  className="select--options"
                  value={selectedStory}
                  placeholder={'Type Of Story'}
                  onChange={value =>
                    this.handleSingleSelect(value, 'selectedStory')
                  }
                  options={typeOfStories.length > 0 ? typeOfStories : []}
                  styles={customStyles}
                />
                {validation.selectedStory.isInvalid && (
                  <span className="error--text">
                    {validation.selectedStory.message}
                  </span>
                )}
              </div>
              <div className="col-2">
                <CustomLabel
                  title="Countries"
                  classes="select--label"
                  isRequired={true}
                />
                <MultiSelect
                  value={selectedCountries}
                  placeholder={'Select Countries'}
                  parentEventHandler={value =>
                    this.handleMultiSelect(value, 'selectedCountries')
                  }
                  options={countries}
                  allowSelectAll={true}
                />
                {validation.selectedCountries.isInvalid && (
                  <span className="error--text">
                    {validation.selectedCountries.message}
                  </span>
                )}
              </div>
              <div className="col-2">
                <CustomLabel title="States" classes="select--label" />
                <MultiSelect
                  value={selectedStates}
                  placeholder={'Select States'}
                  parentEventHandler={value =>
                    this.handleMultiSelect(value, 'selectedStates')
                  }
                  options={states}
                  allowSelectAll={true}
                />
                {this.hasIndia() &&
                  validation.selectedStates &&
                  validation.selectedStates.isInvalid && (
                    <span className="error--text">
                      {validation.selectedStates.message}
                    </span>
                  )}
              </div>
              <div className="col-2">
                <CustomLabel title="Cities" classes="select--label" />
                <MultiSelect
                  value={selectedMetros}
                  placeholder={'Select Metros'}
                  parentEventHandler={value =>
                    this.handleMultiSelect(value, 'selectedMetros')
                  }
                  options={metros}
                  allowSelectAll={true}
                />
                {
                  // validation.selectedMetros.isInvalid && <span className="error--text">{validation.selectedMetros.message}</span>
                }
              </div>
            </div>
            {/** Event single select ends */}
          </section>
          {/** Event Date starts */}
          <section className="modal--section">
            <h2 className="event-head--title">
              <i className="icon-uniE9F4" /> Tracker Date
              <span className="asterisk">*</span>
            </h2>
            <div className="col-2">
              <div className="dateWrapper">
                <div className="date">
                  <DatePicker
                    label="Tracker Date"
                    value={this.state.eventDate}
                    onChange={value =>
                      this.handleDateChange('eventDate', value)
                    }
                    animateYearScrolling
                    InputLabelProps={{
                      shrink: true,
                      classes: {
                        root: classes.rootLabel,
                      },
                    }}
                    InputProps={{
                      classes: {
                        root: classes.rootInput,
                        input: classes.rootInput,
                        notchedOutline: classes.notchedOutline,
                      },
                      endAdornment: null,
                    }}
                  />
                </div>
                <div className="date">
                  <DatePicker
                    label="Event Date"
                    value={this.state.eventDateNew}
                    onChange={value =>
                      this.handleDateChange('eventDateNew', value)
                    }
                    animateYearScrolling
                    InputLabelProps={{
                      shrink: true,
                      classes: {
                        root: classes.rootLabel,
                      },
                    }}
                    InputProps={{
                      classes: {
                        root: classes.rootInput,
                        input: classes.rootInput,
                        notchedOutline: classes.notchedOutline,
                      },
                      endAdornment: null,
                    }}
                  />
                </div>
                <div className="date">
                  <DatePicker
                    label="Validity Date"
                    value={this.state.validityDate}
                    onChange={value =>
                      this.handleDateChange('validityDate', value)
                    }
                    animateYearScrolling
                    InputLabelProps={{
                      shrink: true,
                      classes: {
                        root: classes.rootLabel,
                      },
                    }}
                    InputProps={{
                      classes: {
                        root: classes.rootInput,
                        input: classes.rootInput,
                        notchedOutline: classes.notchedOutline,
                      },
                      endAdornment: null,
                    }}
                  />
                </div>
              </div>
            </div>
          </section>
          {/** Event Date ends */}
          <section className="modal--section">
            <h2 className="event-head--title">
              <i className="icon-uniE90E" /> Event Details
            </h2>
            {/**event title starts */}
            <div className="event--title">
              <TextField
                required
                error={validation.eventTitle.isInvalid}
                id="eventTitle"
                label="Event Title"
                multiline
                fullWidth
                rows="1"
                rowsMax="3"
                value={this.state.eventTitle}
                onChange={e => this.handleInputChange(e, 'eventTitle')}
                defaultValue="Enter Event Title"
                margin="normal"
                variant="outlined"
                InputLabelProps={{
                  shrink: true,
                  classes: {
                    root: classes.rootLabel,
                  },
                }}
                InputProps={{
                  maxLength: 5000,
                  classes: {
                    root: classes.rootInput,
                    input: classes.rootInput,
                    notchedOutline: classes.notchedOutline,
                  },
                  endAdornment: null,
                }}
              />
              {validation.eventTitle.isInvalid && (
                <span className="error--text">
                  {validation.eventTitle.message}
                </span>
              )}
            </div>
            {/** event title ends */}
            {/**event description starts */}
            <div className="form__group description--formgroup">
              {/* <h2 className="event-head--title">Event Description</h2> */}
              {this.generateDescription()}
              {showDescList && (
                <div className="event--bg">
                  <TextField
                    // error={validation.background.isInvalid}
                    id="descList"
                    label="Add List Item"
                    multiline
                    fullWidth
                    rows="1"
                    rowsMax="10"
                    value={this.state.descListItem}
                    onChange={e => this.handleInputChange(e, 'descListItem')}
                    // className={classes.textField}
                    defaultValue="Enter List Item"
                    margin="normal"
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                      classes: {
                        root: classes.rootLabel,
                      },
                    }}
                    InputProps={{
                      maxLength: 5000,
                      classes: {
                        root: classes.rootText,
                        input: classes.rootInput,
                        notchedOutline: classes.notchedOutline,
                      },
                      endAdornment: null,
                    }}
                  />
                  <button
                    className="btn btn--addListItem add"
                    onClick={() => this.addItemToList('description')}
                  >
                    <i className="fas fa-plus"></i>
                  </button>
                </div>
              )}
              {descList.length > 0 && (
                <>
                  <h6 className={classes.listOfItems}>List of items</h6>
                  {this.showBulletPoints('desc')}
                </>
              )}
              <div className="groupBtnWrap">
                {showDescBtn && (
                  <div className="mg">
                    <button
                      className="btn btn--addListItem"
                      onClick={() => this.addPara('description')}
                    >
                      Add More Description
                    </button>
                  </div>
                )}
                {showDescAddListBtn && (
                  <div className="mg">
                    <button
                      className="btn btn--addListItem"
                      onClick={() => this.addBullets('description')}
                    >
                      Add List Items
                    </button>
                  </div>
                )}
              </div>
            </div>
            {/** event description ends */}
            {/** event bg new starts */}
            <div className="form__group bg--formgroup">
              {/* <h2 className="event-head--title">Event Background</h2>                 */}
              {this.generateBg()}
              {showBgList && (
                <div className="event--bg">
                  <TextField
                    // error={validation.background.isInvalid}
                    id="bgList"
                    label="Add List Item"
                    multiline
                    fullWidth
                    rows="1"
                    rowsMax="10"
                    value={this.state.bgListItem}
                    onChange={e => this.handleInputChange(e, 'bgListItem')}
                    // className={classes.textField}
                    defaultValue="Enter List Item"
                    margin="normal"
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                      classes: {
                        root: classes.rootLabel,
                      },
                    }}
                    InputProps={{
                      maxLength: 5000,
                      classes: {
                        root: classes.rootText,
                        input: classes.rootInput,
                        notchedOutline: classes.notchedOutline,
                      },
                      endAdornment: null,
                    }}
                  />
                  <button
                    className="btn btn--addListItem add"
                    onClick={() => this.addItemToList('background')}
                  >
                    <i className="fas fa-plus"></i>
                  </button>
                </div>
              )}
              {bgList.length > 0 && (
                <>
                  <h6 className={classes.listOfItems}>List of items</h6>
                  {this.showBulletPoints('bg')}
                </>
              )}
              <div className="groupBtnWrap">
                {showBgBtn && (
                  <div className="mg">
                    <button
                      className="btn btn--addListItem"
                      onClick={() => this.addPara('background')}
                    >
                      Add More Background
                    </button>
                  </div>
                )}
                {showBgAddListBtn && (
                  <div className="mg">
                    <button
                      className="btn btn--addListItem"
                      onClick={() => this.addBullets('background')}
                    >
                      Add List Items
                    </button>
                  </div>
                )}
              </div>
            </div>
            {/** event bg new ends */}
            {/**event impact analysis new starts */}
            <div className="form__group bg--formgroup">
              {/* <h2 className="event-head--title">Event Impact Analysis</h2>                 */}
              {this.generateImpactAnalysis()}
              {showImpactList && (
                <div className="event--bg">
                  <TextField
                    // error={validation.background.isInvalid}
                    id="impactList"
                    label="Add List Item"
                    multiline
                    fullWidth
                    rows="1"
                    rowsMax="10"
                    value={this.state.impactListItem}
                    onChange={e => this.handleInputChange(e, 'impactListItem')}
                    // className={classes.textField}
                    defaultValue="Enter List Item"
                    margin="normal"
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                      classes: {
                        root: classes.rootLabel,
                      },
                    }}
                    InputProps={{
                      maxLength: 5000,
                      classes: {
                        root: classes.rootText,
                        input: classes.rootInput,
                        notchedOutline: classes.notchedOutline,
                      },
                      endAdornment: null,
                    }}
                  />
                  <button
                    className="btn btn--addListItem add"
                    onClick={() => this.addItemToList('impactAnalysis')}
                  >
                    <i className="fas fa-plus"></i>
                  </button>
                </div>
              )}
              {impactList.length > 0 && (
                <>
                  <h6 className={classes.listOfItems}>List of items</h6>
                  {this.showBulletPoints('impactAnalysis')}
                </>
              )}
              <div className="groupBtnWrap">
                {showImpactBtn && (
                  <div className="mg">
                    <button
                      className="btn btn--addListItem"
                      onClick={() => this.addPara('impactAnalysis')}
                    >
                      Add More Analysis
                    </button>
                  </div>
                )}
                {showImpactAddListBtn && (
                  <div className="mg">
                    <button
                      className="btn btn--addListItem"
                      onClick={() => this.addBullets('impactAnalysis')}
                    >
                      Add List Items
                    </button>
                  </div>
                )}
              </div>
            </div>
            {/**event impact analysis new ends */}
            {/**event recommendation new starts */}
            <div className="form__group bg--formgroup">
              {/* <h2 className="event-head--title">Event Recommendations</h2>                 */}
              {this.generateRecommendation()}
              {showRecomList && (
                <div className="event--bg">
                  <TextField
                    // error={validation.background.isInvalid}
                    id="impactList"
                    label="Add List Item"
                    multiline
                    fullWidth
                    rows="1"
                    rowsMax="10"
                    value={this.state.recomListItem}
                    onChange={e => this.handleInputChange(e, 'recomListItem')}
                    // className={classes.textField}
                    defaultValue="Enter List Item"
                    margin="normal"
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                      classes: {
                        root: classes.rootLabel,
                      },
                    }}
                    InputProps={{
                      maxLength: 5000,
                      classes: {
                        root: classes.rootText,
                        input: classes.rootInput,
                        notchedOutline: classes.notchedOutline,
                      },
                      endAdornment: null,
                    }}
                  />
                  <button
                    className="btn btn--addListItem add"
                    onClick={() => this.addItemToList('recom')}
                  >
                    <i className="fas fa-plus"></i>
                  </button>
                </div>
              )}
              {recomList.length > 0 && (
                <>
                  <h6 className={classes.listOfItems}>List of items</h6>
                  {this.showBulletPoints('recom')}
                </>
              )}
              <div className="groupBtnWrap">
                {showRecomBtn && (
                  <div className="mg">
                    <button
                      className="btn btn--addListItem"
                      onClick={() => this.addPara('recom')}
                    >
                      Add More Recommendation
                    </button>
                  </div>
                )}
                {showRecomAddListBtn && (
                  <div className="mg">
                    <button
                      className="btn btn--addListItem"
                      onClick={() => this.addBullets('recom')}
                    >
                      Add List Items
                    </button>
                  </div>
                )}
              </div>
            </div>
            {/**event recommendation new ends */}
            {/**event sources starts */}
            <div className="event--sources">
              <TextField
                // error={validation.eventSource.isInvalid}
                id="eventSources"
                label="Event Sources"
                multiline
                fullWidth
                rows="1"
                rowsMax="10"
                value={this.state.eventSource}
                onChange={e => this.handleInputChange(e, 'eventSource')}
                // className={classes.textField}
                defaultValue="Enter Event Source"
                margin="normal"
                variant="outlined"
                InputLabelProps={{
                  shrink: true,
                  classes: {
                    root: classes.rootLabel,
                  },
                }}
                InputProps={{
                  maxLength: 5000,
                  classes: {
                    root: classes.rootInput,
                    input: classes.rootInput,
                    notchedOutline: classes.notchedOutline,
                  },
                  endAdornment: null,
                }}
              />
              <p className="event--sources--note">
                Note: Separate multiple news link using delimitator “ ; “ only
              </p>
            </div>
            {/**event sources ends */}
          </section>
          <section className="modal--section">
            {/** single selects goes here */}
            <h2 className="event-head--title">
              <i className="icon-uniE99D" /> Event Type
            </h2>
            <div className="twoColWrap single--selects">
              <div className="col-3">
                <CustomLabel
                  title="Risk Category"
                  classes="select--label"
                  isRequired={true}
                />
                <Select
                  defaultValue={selectedCategory}
                  className="select--options"
                  value={selectedCategory}
                  placeholder={'Risk Category'}
                  onChange={value =>
                    this.handleSingleSelectSub(value, 'selectedCategory')
                  }
                  options={riskCategories.length > 0 ? riskCategories : []}
                  styles={customStyles}
                />
                {validation.selectedCategory.isInvalid && (
                  <span className="error--text">
                    {validation.selectedCategory.message}
                  </span>
                )}
              </div>
              <div className="col-3">
                <CustomLabel
                  title="Sub Risk Category"
                  classes="select--label"
                  isRequired={true}
                />
                <Select
                  defaultValue={selectedSubCategory}
                  className="select--options"
                  value={selectedSubCategory}
                  placeholder={'Sub Risk Category'}
                  onChange={value =>
                    this.handleSingleSelect(value, 'selectedSubCategory')
                  }
                  options={
                    subRiskCategories.length > 0 ? subRiskCategories : []
                  }
                  styles={customStyles}
                />
                {validation.selectedSubCategory.isInvalid && (
                  <span className="error--text">
                    {validation.selectedSubCategory.message}
                  </span>
                )}
              </div>
              <div className="col-3">
                <CustomLabel
                  title="Industries"
                  classes="select--label"
                  isRequired={true}
                />
                <MultiSelect
                  value={impactedIndustries}
                  placeholder={'Impact Industry'}
                  parentEventHandler={value =>
                    this.handleMultiSelect(value, 'impactedIndustries')
                  }
                  options={industries}
                />
                {validation.impactedIndustries.isInvalid && (
                  <span className="error--text">
                    {validation.impactedIndustries.message}
                  </span>
                )}
              </div>
            </div>
          </section>
          <section className="modal--section">
            <div>
              <CustomLabel
                title="Risk Level"
                classes="select--label"
                isRequired={true}
              />
              <Select
                defaultValue={selectedRiskLevel}
                className="select--options"
                value={selectedRiskLevel}
                placeholder={'Risk Level'}
                onChange={value =>
                  this.handleSingleSelect(value, 'selectedRiskLevel')
                }
                options={riskLevels.length > 0 ? riskLevels : []}
                styles={customStyles}
              />
              {validation.selectedRiskLevel.isInvalid && (
                <span className="error--text">
                  {validation.selectedRiskLevel.message}
                </span>
              )}
            </div>
            <div>
              {/**impact radius starts */}
              <div className="event--sources">
                <TextField
                  id="impactRadius"
                  label="Impact Radius"
                  fullWidth
                  value={calImpactRadius}
                  onChange={e => this.handleInputChange(e, 'calImpactRadius')}
                  margin="normal"
                  variant="outlined"
                  InputLabelProps={{
                    shrink: true,
                    classes: {
                      root: classes.rootLabel,
                    },
                  }}
                  InputProps={{
                    classes: {
                      root: classes.rootInput,
                      input: classes.rootInput,
                      notchedOutline: classes.notchedOutline,
                    },
                    endAdornment: null,
                  }}
                />
              </div>
              {/**impact radius ends */}
            </div>
          </section>
          <section className="modal--section">
            {/**blockage starts */}
            <div className="event--sources">
              <TextField
                id="blockage"
                label="Supply change Blockage Duration in Days"
                fullWidth
                value={blockage}
                onChange={e => this.handleInputChange(e, 'blockage')}
                margin="normal"
                variant="outlined"
                InputLabelProps={{
                  shrink: true,
                  classes: {
                    root: classes.rootLabel,
                  },
                }}
                InputProps={{
                  classes: {
                    root: classes.rootInput,
                    input: classes.rootInput,
                    notchedOutline: classes.notchedOutline,
                  },
                  endAdornment: null,
                }}
              />
            </div>
            {/**blockage ends */}
          </section>
          <section className="modal--section">
            <div className="col-2">
              <CustomLabel
                title="Select Topic"
                classes="select--label"
                isRequired={false}
              />
              <Select
                defaultValue={selectedLiveTopic}
                className="select--options"
                value={selectedLiveTopic}
                placeholder={'Select Topic'}
                onChange={value =>
                  this.handleSingleSelect(value, 'selectedLiveTopic')
                }
                options={allLiveTopics.length > 0 ? allLiveTopics : []}
                styles={customStyles}
              />
              {/* {validation.selectedStory.isInvalid && (
                  <span className='error--text'>
                    {validation.selectedStory.message}
                  </span>
                )} */}
            </div>
          </section>
          <section className="modal--section">
            {/** radios goes here */}
            <h2 className="event-head--title">
              <i className="icon-uniE905" /> Event Relevance
            </h2>
            <div className="twoColWrap radios--cont">
              <div className="col-2">
                <FormControl component="fieldset">
                  <FormLabel component="legend" className={classes.radioLabel}>
                    IRT
                  </FormLabel>
                  <RadioGroup
                    aria-label="eventImportance"
                    name="eventImportance"
                    // className={classes.group}
                    value={this.state.isImportant === 'true' ? true : false}
                    onChange={e => this.handleInputChange(e, 'isImportant')}
                  >
                    <FormControlLabel
                      value={true}
                      control={<Radio />}
                      label="Yes"
                      className={classes.radioLabel}
                    />
                    <FormControlLabel
                      value={false}
                      control={<Radio />}
                      label="No"
                      className={classes.radioLabel}
                    />
                  </RadioGroup>
                </FormControl>
              </div>
              <div className="col-2">
                <FormControl component="fieldset">
                  <FormLabel component="legend" className={classes.radioLabel}>
                    Is Event Live
                  </FormLabel>
                  <RadioGroup
                    aria-label="isLive"
                    name="isLive"
                    // className={classes.group}
                    value={this.state.isLive === 'true' ? true : false}
                    onChange={e => this.handleInputChange(e, 'isLive')}
                    className={classes.radioBtn}
                  >
                    <FormControlLabel
                      value={true}
                      control={<Radio />}
                      label="Yes"
                      className={classes.radioLabel}
                    />
                    <FormControlLabel
                      value={false}
                      control={<Radio />}
                      label="No"
                      className={classes.radioLabel}
                    />
                  </RadioGroup>
                </FormControl>
              </div>
            </div>
          </section>
          <section className="modal--section">
            <FormControlLabel
              classes={{
                label: classes.label,
              }}
              control={
                <Checkbox
                  className={classes.size}
                  checked={this.state.isLocationSpecific}
                  onChange={this.handleIsLocSpecific}
                  value={this.state.isLocationSpecific}
                  icon={
                    <CheckBoxOutlineBlankIcon className={classes.sizeIcon} />
                  }
                  checkedIcon={<CheckBoxIcon className={classes.sizeIcon} />}
                />
              }
              label={'Is story location specific?'}
            />
          </section>
        </div>
        <div className="actionables">
          <Button
            onClick={e => this.handleShowAlert()}
            variant="contained"
            color="default"
          >
            Cancel
          </Button>
          <Button
            onClick={e => this.handleEventSubmit(e)}
            style={{ marginLeft: 20, backgroundColor: '#2980b9' }}
            variant="contained"
            color="secondary"
          >
            Create Event
          </Button>
        </div>
        {showAlert && (
          <AlertDialog
            isOpen={showAlert}
            title="Cancel Changes"
            description="You are about to cancel event changes, you won't be able to get them back, are you sure?"
            parentEventHandler={e => this.handleCloseModal()}
            handleClose={() => this.handleCloseAlert()}
          />
        )}
      </div>
    );
  }
}

export default CreateEventUI;
