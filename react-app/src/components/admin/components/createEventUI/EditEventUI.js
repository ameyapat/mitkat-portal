import React, { Component } from 'react';
import './style.scss';
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Radio from '@material-ui/core/Radio';
import Button from '@material-ui/core/Button';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import MultiSelect from '../../../ui/multiSelect';
import Checkbox from '@material-ui/core/Checkbox';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import MultipleLocations from '../multipleLocations';
import CustomMap from '../../../ui/customMap';
import CustomLabel from '../../../ui/customLabel';
import AlertDialog from '../../../ui/alertDialog';
import EditableItem from '../EditableItem';
import { withCookies } from 'react-cookie';
import { getListData, getApprovalObj } from '../../helpers/utils';
import {
  filterArrObjects,
  fetchCountries,
  fetchStates,
  fetchCities,
  fetchIndustries,
  createValueLabel,
  createLabelObj,
  filterByLatLng,
} from '../../../../helpers/utils';
import { fetchApi } from '../../../../helpers/http/fetch';
import { getLiveTopics, getAllTopics } from '../../../../api/api';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import store from '../../../../store';
import { toggleToast, refreshEvents } from '../../../../actions/';
import { updateMultipleLocation } from '../../../../actions/multipleLocationActions';
import injectSheet from 'react-jss';
import style from './style';
import { DatePicker } from 'material-ui-pickers';
import validationRules from './validationRules';
import FormValidator from '../../../../helpers/validation/FormValidator';
import Select from 'react-select';
import { connect } from 'react-redux';
import MultipleLocationList from '../../../ui/multipleLocationList';

const approvalListArr = [
  { value: 1, label: 'Pending Approval' },
  { value: 2, label: 'Approved' },
  { value: 3, label: 'Rejected' },
];

const stateRules = [
  {
    field: 'selectedStates',
    method: 'isEmpty',
    validWhen: false,
    message: 'States is required',
  },
];

const approverRules = [
  {
    field: 'approveStatus',
    method: 'isEmpty',
    validWhen: false,
    message: 'Approve status is required',
  },
];

const updatedRules = [...validationRules, ...stateRules];

@withCookies
@injectSheet(style)
@connect(state => {
  return {
    multipleLocations: state.multipleLocations,
  };
})
class EditEventUI extends Component {
  validator = new FormValidator(
    this.props.isApprover
      ? [...validationRules, ...approverRules]
      : validationRules,
  );
  submitted = false;

  state = {
    address: '',
    riskLevels: [],
    riskCategories: [],
    subRiskCategories: [],
    typeOfStories: [],
    countries: [],
    states: [],
    metros: [],
    industries: [],
    selectedRiskLevel: '',
    selectedCategory: '',
    selectedSubCategory: { value: 1, label: 'Other' },
    selectedStory: '',
    selectedCountries: [],
    selectedStates: [],
    selectedMetros: [],
    eventDetails: '',
    eventDate: '',
    eventDateNew: '',
    validityDate: '',
    eventSource: '',
    eventTitle: '',
    descArr: [
      {
        id: 1,
        label: 'Description',
        type: 'description',
        title: 'description',
      },
      {
        id: 2,
        label: 'Description 2',
        type: 'description2',
        title: 'description2',
      },
    ],
    showDescList: false,
    showBgList: false,
    description: '',
    description2: '',
    descInputItem: '',
    descListItem: '',
    bgListItem: '',
    descList: [],
    showDescBtn: true,
    showBgBtn: true,
    showDescAddListBtn: true,
    showBgAddListBtn: true,
    bgArr: [
      { id: 1, label: 'Background', type: 'background', title: 'background' },
      {
        id: 2,
        label: 'Background 2',
        type: 'background2',
        title: 'background2',
      },
    ],
    background: '',
    background2: '',
    bgList: [],
    impactArr: [
      {
        id: 1,
        label: 'Impact Analysis',
        type: 'impactAnalysis',
        title: 'impactAnalysis',
      },
      {
        id: 2,
        label: 'Impact Analysis 2',
        type: 'impactAnalysis2',
        title: 'impactAnalysis2',
      },
    ],
    showImpactBtn: true,
    showImpactAddListBtn: true,
    showImpactList: false,
    impactAnalysis: '',
    impactAnalysis2: '',
    impactList: [],
    impactListItem: '',
    recomArr: [
      {
        id: 1,
        label: 'Recommendation',
        type: 'recommendations',
        title: 'recommendations',
      },
      {
        id: 2,
        label: 'Recommendation 2',
        type: 'recommendation2',
        title: 'recommendation2',
      },
    ],
    recommendations: '',
    recommendation2: '',
    recomList: [],
    recomListItem: '',
    showRecomBtn: true,
    showRecomAddListBtn: true,
    showRecomList: false,
    isImportant: false,
    approveStatus: '',
    isLive: false,
    impactedIndustries: [],
    location: {
      lat: '',
      lng: '',
    },
    id: '',
    validation: this.validator.valid(),
    showAlert: false,
    calImpactRadius: '',
    blockage: '',
    qualityRating: 0,
    isLocationSpecific: false,
    initSubCatLabel: '',
    allTopics: [],
    allLiveTopics: [],
    selectedTopic: '',
    selectedLiveTopic: '',
    coordinates: [],
  };

  componentDidMount() {
    let { cookies, eventDetails, isApprover } = this.props;
    let {
      id,
      background,
      background2,
      description,
      description2,
      location,
      validityDate,
      eventDate,
      eventDateNew,
      selectedMetros,
      selectedCountries,
      selectedState,
      impactedIndustries,
      selectedStates,
      approveStatus,
      eventTitle,
      eventSource,
      impactAnalysis,
      impactAnalysis2,
      recommendations,
      recommendation2,
      selectedRiskLevel,
      selectedCategory,
      selectedSubCategory,
      selectedStory,
      bgList,
      recomList,
      impactList,
      descList,
      showDescBtn,
      showImpactBtn,
      showBgBtn,
      showRecomBtn,
      isImportant,
      isLive,
      calImpactRadius,
      blockage,
      qualityRating,
      selectedLiveTopic,
      selectedTopic,
      isLocationSpecific,
      coordinates,
    } = this.state;
    let authToken = cookies.get('authToken');

    id = eventDetails.id;
    eventTitle = eventDetails.title;
    eventSource = eventDetails.links;
    background = eventDetails.background;
    background2 = eventDetails.background2;
    impactAnalysis = eventDetails.impact;
    impactAnalysis2 = eventDetails.impact2;
    recommendations = eventDetails.recommendation;
    recommendation2 = eventDetails.recommendation2;
    description = eventDetails.description;
    description2 = eventDetails.description2;
    location['lat'] = eventDetails.latitude;
    location['lng'] = eventDetails.longitude;
    selectedRiskLevel = createLabelObj(
      eventDetails.riskLevel,
      'selectedRiskLevel',
    );
    selectedCategory = createLabelObj(
      eventDetails.riskCategory,
      'selectedCategory',
    );
    selectedStory = createLabelObj(eventDetails.regionType, 'selectedStory');
    selectedMetros = createValueLabel(eventDetails.cities, 'city'); //method to add value & label
    selectedCountries = createValueLabel(eventDetails.countries, 'country'); //method to add value & label
    selectedStates = createValueLabel(eventDetails.state, 'state'); //method to add value & label
    impactedIndustries = createValueLabel(
      eventDetails.impactindustry,
      'impactIndustry',
    ); //method to add value & label
    validityDate = eventDetails.validitydate;
    eventDate = eventDetails.storydate;
    eventDateNew = eventDetails.eventdate;
    approveStatus = isApprover
      ? getApprovalObj(parseInt(eventDetails.status.id))
      : '';
    bgList = eventDetails.backgroundbullets || [];
    recomList = eventDetails.recommendationbullets || [];
    impactList = eventDetails.impactbullets || [];
    descList = eventDetails.descriptionbullets || [];
    showDescBtn = false;
    showRecomBtn = false;
    showBgBtn = false;
    showImpactBtn = false;
    isImportant = eventDetails.importance == true ? 'true' : 'false';
    isLive = eventDetails.liveevent == true ? 'true' : 'false';
    calImpactRadius = eventDetails.impactRadius;
    blockage = eventDetails.blockage;
    isLocationSpecific = eventDetails.islocationspecific || false;
    selectedLiveTopic = {
      value: eventDetails.topicid,
      label: eventDetails.topicname,
    };
    selectedTopic = {
      value: eventDetails.topicid,
      label: eventDetails.topicname,
    };
    qualityRating = eventDetails.quality || 0;

    this.setState(
      {
        id,
        background,
        background2,
        description,
        description2,
        location,
        validityDate,
        eventDate,
        eventDateNew,
        selectedMetros,
        selectedCountries,
        selectedState,
        approveStatus,
        eventTitle,
        eventSource,
        selectedStates,
        impactedIndustries,
        recommendations,
        recommendation2,
        impactAnalysis,
        impactAnalysis2,
        selectedRiskLevel,
        selectedCategory,
        selectedSubCategory,
        selectedStory,
        bgList,
        recomList,
        impactList,
        descList,
        showDescBtn,
        showRecomBtn,
        showBgBtn,
        showImpactBtn,
        isImportant,
        isLive,
        calImpactRadius,
        blockage,
        qualityRating,
        isLocationSpecific,
        selectedLiveTopic,
        selectedTopic,
      },
      () => {
        this.getReverseGeoCode();
      },
    );

    this.getRiskLevels('riskLevel').then(riskLevels =>
      this.setState({ riskLevels }),
    );
    this.getRiskCategories('riskCategory').then(riskCategories => {
      this.setState({ riskCategories });
      this.getInitialSubCategories();
    });
    this.getStories('typeOfStory').then(typeOfStories =>
      this.setState({ typeOfStories }),
    );
    fetchCountries(authToken).then(countries => this.setState({ countries }));
    fetchStates(authToken).then(states => this.setState({ states }));
    fetchCities(authToken).then(metros => this.setState({ metros }));
    fetchIndustries(authToken).then(industries =>
      this.setState({ industries }),
    );
    getLiveTopics()
      .then(data => {
        let allLiveTopics = [];
        allLiveTopics = data.map(t => {
          let allLiveTopicsObj = {};
          allLiveTopicsObj['value'] = t.id;
          allLiveTopicsObj['label'] = t.topicName;
          return allLiveTopicsObj;
        });
        this.setState({ allLiveTopics });
      })
      .catch(e => console.log('Error fetching live topics', e));

    getAllTopics()
      .then(data => {
        let allTopics = [];
        allTopics = data.map(t => {
          let allTopicsObj = {};
          allTopicsObj['value'] = t.id;
          allTopicsObj['label'] = t.topicName;
          return allTopicsObj;
        });
        this.setState({ allTopics });
      })
      .catch(e => console.log('Error fetching all topics', e));
  }

  componentDidUpdate(prevProps) {
    const prevMultipleLocation =
      prevProps.multipleLocations.selectedMultipleLocations;
    const currMultipleLocation = this.props.multipleLocations
      .selectedMultipleLocations;
    if (currMultipleLocation !== prevMultipleLocation) {
      this.setState({ coordinates: currMultipleLocation });
    }
  }

  getInitialSubCat = () => {
    const { eventDetails } = this.props;
    let { subRiskCategories, initSubCatLabel } = this.state;
    initSubCatLabel = subRiskCategories.find(
      categories => categories.value === eventDetails.risksubcategoryid,
    );
    this.setState({ initSubCatLabel, selectedSubCategory: initSubCatLabel });
  };

  getInitialSubCategories = () => {
    const { riskCategories, selectedCategory } = this.state;

    this.getSubRiskCategories(selectedCategory, 'subRiskCategory').then(
      subRiskCategories => {
        this.setState({ subRiskCategories }, () => {
          this.getInitialSubCat();
        });
      },
    );
  };

  getReverseGeoCode = () => {
    let {
      eventDetails: { coordinates = [] },
    } = this.props;
    coordinates.map(item => {
      this.fetchFormattedAddress(item.latitude, item.longitude);
    });
  };

  // getReverseGeoCode = () => {
  //   let {
  //     location: { lat, lng },
  //   } = this.state;

  //   let reqObj = {
  //     url: `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&key=AIzaSyCYOTLyn6xbysArlvlW4zpxnH9p6H3An08&v=3.exp`,
  //     method: 'GET',
  //     isAuth: false,
  //   };

  //   fetch(reqObj.url)
  //     .then(data => data.json())
  //     .then(res => {
  //       let address = res.results[0].formatted_address;
  //       this.setState({ address });
  //     })
  //     .catch(e => console.log(e));
  // };

  fetchFormattedAddress = (lat, lng) => {
    let reqObj = {
      url: `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&key=AIzaSyCYOTLyn6xbysArlvlW4zpxnH9p6H3An08&v=3.exp`,
      method: 'GET',
      isAuth: false,
    };

    const newCoordinateObj = {};
    const newCoordinateArr = [];

    fetch(reqObj.url)
      .then(data => data.json())
      .then(res => {
        let address = res.results[0].formatted_address;
        newCoordinateObj.lat = lat;
        newCoordinateObj.lng = lng;
        newCoordinateObj.formattedAddress = address;
        newCoordinateObj.id = res.results[0].place_id;

        newCoordinateArr.push(newCoordinateObj);

        return newCoordinateArr;
      })
      .then(newCoordinateArr => {
        this.setState(
          { coordinates: [...this.state.coordinates, ...newCoordinateArr] },
          () => {
            store.dispatch(updateMultipleLocation(this.state.coordinates));
          },
        );
      })
      .catch(e => console.log(e));
  };

  /** handle text area changes */
  handleInputChange = (e, type) => {
    this.setState({ [type]: e.target.value });
  };

  handleDateChange = (type, value) => this.setState({ [type]: value });

  handleChange = (e, type) => {
    switch (type) {
      case 'riskLevel':
        this.setState({ selectedRiskLevel: e.target.value });
        break;
      case 'riskCategory':
        this.setState({ selectedCategory: e.target.value });
      case 'subRiskCategory':
        this.setState({ selectedSubCategory: e.target.value });
        break;
      case 'typeOfStory':
        this.setState({ selectedStory: e.target.value });
        break;
      case 'approveStatus':
        this.setState({ approveStatus: e.target.value });
        break;
      default:
        console.log('something went wrong');
        break;
    }
  };

  fetchSubCategory = category => {
    this.getSubRiskCategories(category, 'subRiskCategory').then(
      subRiskCategories => {
        this.setState({ selectedSubCategory: subRiskCategories[0] });
        this.setState({ subRiskCategories });
      },
    );
  };

  getStories = type => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');

    return new Promise((res, rej) => {
      getListData(type, authToken)
        .then(data => {
          let { typeOfStories } = this.state;
          data.map(t => {
            let typeOfStoryObj = {};
            typeOfStoryObj['value'] = t.id;
            typeOfStoryObj['label'] = t.regiontype;
            typeOfStories.push(typeOfStoryObj);
          });
          res(typeOfStories);
        })
        .catch(e => rej(e));
    });
  };

  getRiskCategories = type => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');

    return new Promise((res, rej) => {
      getListData(type, authToken)
        .then(data => {
          let { riskCategories } = this.state;
          data.map(t => {
            let riskCategoriesObj = {};
            riskCategoriesObj['value'] = t.id;
            riskCategoriesObj['label'] = t.riskcategory;
            riskCategories.push(riskCategoriesObj);
          });
          res(riskCategories);
        })
        .catch(e => rej(e));
    });
  };

  getSubRiskCategories = (category, type) => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    const subRiskCategories = [];

    return new Promise((res, rej) => {
      getListData(type, authToken, category)
        .then(data => {
          data.map(t => {
            let subRiskCategoriesObj = {};
            subRiskCategoriesObj['value'] = t.id;
            subRiskCategoriesObj['label'] = t.risksubcategory;
            subRiskCategories.push(subRiskCategoriesObj);
          });
          res(subRiskCategories);
        })
        .catch(e => rej(e));
    });
  };

  getRiskLevels = type => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    return new Promise((res, rej) => {
      getListData(type, authToken)
        .then(data => {
          let { riskLevels } = this.state;

          data.map(t => {
            let riskLevelsObj = {};
            riskLevelsObj['value'] = t.id;
            riskLevelsObj['label'] = t.level;
            riskLevels.push(riskLevelsObj);
          });
          res(riskLevels);
        })
        .catch(e => rej(e));
    });
  };

  hasIndia = () => {
    let { selectedCountries } = this.state;
    let hasIndia = false;

    selectedCountries.map(c =>
      c.label === 'India' ? (hasIndia = true) : (hasIndia = false),
    );
    return hasIndia;
  };

  handleEventSubmit = e => {
    e.preventDefault();
    let {
      isApprover,
      multipleLocations: { selectedMultipleLocations },
    } = this.props;
    this.validator = this.hasIndia()
      ? new FormValidator(
          isApprover ? [...updatedRules, ...approverRules] : updatedRules,
        )
      : new FormValidator(
          isApprover ? [...validationRules, ...approverRules] : validationRules,
        );
    let validation = this.validator.validate(this.state);
    this.setState({ validation });
    this.submitted = true;

    if (validation.isValid) {
      let { cookies } = this.props;
      let authToken = cookies.get('authToken');
      let {
        id,
        eventDetails,
        selectedCountries,
        eventSource,
        eventDate,
        validityDate,
        eventDateNew,
        background,
        background2,
        bgList,
        eventTitle,
        descList,
        description,
        description2,
        impactAnalysis,
        impactAnalysis2,
        impactList,
        recommendations,
        recommendation2,
        recomList,
        selectedRiskLevel,
        selectedCategory,
        selectedSubCategory,
        impactedIndustries,
        selectedMetros,
        selectedStates,
        isImportant,
        isLive,
        selectedStory,
        location: { lat, lng },
        calImpactRadius,
        blockage,
        isLocationSpecific,
        selectedLiveTopic,
        selectedTopic,
      } = this.state;

      eventDetails = {
        id: id,
        title: eventTitle,
        description: description,
        description2: description2,
        descriptionbullets: descList,
        background: background,
        background2: background2,
        backgroundbullets: bgList,
        impact: impactAnalysis,
        impact2: impactAnalysis2,
        impactbullets: impactList,
        recommendation: recommendations,
        recommendation2: recommendation2,
        recommendationbullets: recomList,
        links: eventSource,
        latitude: lat, //lat
        longitude: lng, //long
        storydate: eventDate,
        eventdate: eventDateNew,
        validitydate: validityDate,
        riskLevel: selectedRiskLevel.value || '',
        riskcategory: selectedCategory.value || '',
        risksubcategoryid: selectedSubCategory.value || '',
        regionType: selectedStory.value,
        impactindustry: filterArrObjects(impactedIndustries),
        countries: filterArrObjects(selectedCountries),
        state: filterArrObjects(selectedStates),
        cities: filterArrObjects(selectedMetros),
        importance: isImportant,
        liveevent: isLive,
        impactRadius: calImpactRadius,
        blockage,
        islocationspecific: isLocationSpecific,
        topicname: selectedLiveTopic.label,
        topicid: this.props.hasAllTopics
          ? selectedTopic.value
          : selectedLiveTopic.value,
        coordinates: filterByLatLng(selectedMultipleLocations),
      };

      let reqObj = {
        url: API_ROUTES.editEventCreator,
        data: eventDetails,
        isAuth: true,
        authToken,
        method: 'POST',
        showToggle: true,
      };

      fetchApi(reqObj).then(data => {
        if (data && data.status == 200) {
          let toastObj = {
            toastMsg: 'Event Updated Successfully!',
            showToast: true,
          };
          this.props.closeEventModal(false);
          store.dispatch(refreshEvents(true));
          store.dispatch(toggleToast(toastObj));
          store.dispatch(updateMultipleLocation([]));
          if (this.props.refreshWeekly) {
            this.props.refreshWeeklyEvents();
          }
        } else if (data && data.status == 502) {
          let toastObj = {
            toastMsg: 'Something went wrong, Please try again',
            showToast: true,
          };
          store.dispatch(toggleToast(toastObj));
        }
      });
    }
  };

  riskLevelsXML = () => {
    let { riskLevels } = this.state;
    let riskLevelXML = [];
    riskLevelXML = riskLevels.map(item => (
      <MenuItem key={item.id} value={item.id}>
        {item.level}
      </MenuItem>
    ));
    return riskLevelXML;
  };

  riskCategoriesXML = () => {
    let { riskCategories } = this.state;
    let riskCategoryXML = [];
    riskCategoryXML = riskCategories.map(item => (
      <MenuItem key={item.id} value={item.id}>
        {item.riskcategory}
      </MenuItem>
    ));
    return riskCategoryXML;
  };

  storiesXML = () => {
    let { typeOfStories } = this.state;
    let storiesXML = [];
    storiesXML = typeOfStories.map(item => (
      <MenuItem key={item.id} value={item.id}>
        {item.regiontype}
      </MenuItem>
    ));
    return storiesXML;
  };

  handleSingleSelect = (value, type) => {
    this.setState({ [type]: value });
  };

  handleSingleSelectSub = (value, type) => {
    this.setState({ [type]: value }, () => this.fetchSubCategory(value));
  };

  handleMultiSelect = (value, type) => {
    let values = this.state[type];
    if (values.includes(value)) {
      let index = values.indexOf(value);
      values.splice(index, 1);
    } else {
      values = [...value];
    }
    this.setState({ [type]: values }, () => console.log(this.state));
  };

  handleAddressChange = address => {
    this.setState({ address });
  };
  /** function for setting location lat & lng */
  handleSelect = address => {
    geocodeByAddress(address)
      .then(results => getLatLng(results[0]))
      .then(latLng => {
        let { location } = this.state;
        location['lat'] = latLng.lat;
        location['lng'] = latLng.lng;
        this.setState({ location }, () => console.log(this.state));
      })
      .catch(error => {
        console.error('Error', error);
        let toastObj = {
          showToast: true,
          toastObj: 'Error getting location, try again!',
        };
        store.dispatch(toggleToast(toastObj));
      });
  };

  handleUpdateEvent = () => {
    let {
      isApprover,
      multipleLocations: { selectedMultipleLocations },
    } = this.props;
    this.validator = this.hasIndia()
      ? new FormValidator(
          isApprover ? [...updatedRules, ...approverRules] : updatedRules,
        )
      : new FormValidator(
          isApprover ? [...validationRules, ...approverRules] : validationRules,
        );
    let validation = this.validator.validate(this.state);
    this.setState({ validation });
    this.submitted = true;

    if (validation.isValid) {
      let {
        id,
        eventDetails,
        selectedCountries,
        eventSource,
        eventDate,
        eventDateNew,
        validityDate,
        background,
        background2,
        bgList,
        eventTitle,
        description,
        description2,
        descList,
        impactAnalysis,
        impactAnalysis2,
        impactList,
        recommendations,
        recommendation2,
        recomList,
        selectedRiskLevel,
        selectedCategory,
        selectedSubCategory,
        impactedIndustries,
        approveStatus,
        selectedMetros,
        selectedStates,
        isImportant,
        isLive,
        selectedStory,
        location: { lat, lng },
        calImpactRadius,
        blockage,
        qualityRating,
        isLocationSpecific,
        selectedLiveTopic,
        selectedTopic,
      } = this.state;

      eventDetails = {
        id: id,
        title: eventTitle,
        description: description,
        description2: description2,
        descriptionbullets: descList,
        background: background,
        background2: background2,
        backgroundbullets: bgList,
        impact: impactAnalysis,
        impact2: impactAnalysis2,
        impactbullets: impactList,
        recommendation: recommendations,
        recommendation2: recommendation2,
        recommendationbullets: recomList,
        links: eventSource,
        latitude: lat, //lat
        longitude: lng, //long
        storydate: eventDate,
        eventdate: eventDateNew,
        validitydate: validityDate,
        riskLevel: selectedRiskLevel.value || '',
        riskcategory: selectedCategory.value || '',
        risksubcategoryid: selectedSubCategory.value || '',
        regionType: selectedStory.value,
        status: approveStatus.value,
        impactindustry: filterArrObjects(impactedIndustries),
        countries: filterArrObjects(selectedCountries),
        state: filterArrObjects(selectedStates),
        cities: filterArrObjects(selectedMetros),
        importance: isImportant,
        liveevent: isLive,
        impactRadius: calImpactRadius,
        blockage,
        quality: parseInt(qualityRating),
        islocationspecific: isLocationSpecific,
        topicid: this.props.hasAllTopics
          ? selectedTopic.value
          : selectedLiveTopic.value,
        topicname: selectedLiveTopic.label,
        coordinates: filterByLatLng(selectedMultipleLocations),
      };
      this.props.updateEvent(eventDetails);
    }
  };

  addPara = type => {
    let { descArr, bgArr, impactArr, recomArr } = this.state;
    switch (type) {
      case 'description':
        let newPara = [
          {
            id: 2,
            label: 'Description 2',
            type: 'description2',
            title: 'description2',
          },
        ];
        this.setState({
          descArr: [...descArr, ...newPara],
          showDescBtn: false,
        });
        break;
      case 'background':
        let newBg = [
          {
            id: 2,
            label: 'Background 2',
            type: 'background2',
            title: 'background2',
          },
        ];
        this.setState({ bgArr: [...bgArr, ...newBg], showBgBtn: false });
        break;
      case 'impactAnalysis':
        let newImpact = [
          {
            id: 2,
            label: 'Impact Analysis 2',
            type: 'impactAnalysis2',
            title: 'impactAnalysis2',
          },
        ];
        this.setState({
          impactArr: [...impactArr, ...newImpact],
          showImpactBtn: false,
        });
        break;
      case 'recom':
        let newRecom = [
          {
            id: 2,
            label: 'Recommendation 2',
            type: 'recommendation2',
            title: 'recommendation2',
          },
        ];
        this.setState({
          recomArr: [...recomArr, ...newRecom],
          showRecomBtn: false,
        });
        break;
      default:
        console.log('default...');
        break;
    }
  };

  addBullets = type => {
    switch (type) {
      case 'description':
        this.setState({ showDescList: true, showDescAddListBtn: false });
        break;
      case 'background':
        this.setState({ showBgList: true, showBgAddListBtn: false });
        break;
      case 'impactAnalysis':
        this.setState({ showImpactList: true, showImpactAddListBtn: false });
        break;
      case 'recom':
        this.setState({ showRecomList: true, showRecomAddListBtn: false });
        break;
      default:
        break;
    }
  };

  showBulletPoints = type => {
    switch (type) {
      case 'desc':
        let { descList } = this.state;
        let descListXML = [];
        return (descListXML = descList.map((d, idx) => (
          <EditableItem
            key={idx}
            idx={idx}
            updateList={this.updateList}
            description={d}
            list={descList}
            type={type}
          />
        )));
      case 'bg':
        let { bgList } = this.state;
        let bgListXML = [];
        return (bgListXML = bgList.map((d, idx) => (
          <EditableItem
            key={idx}
            idx={idx}
            updateList={this.updateList}
            description={d}
            list={bgList}
            type={type}
          />
        )));
      case 'impactAnalysis':
        let { impactList } = this.state;
        let impactListXML = [];
        return (impactListXML = impactList.map((d, idx) => (
          <EditableItem
            key={idx}
            idx={idx}
            updateList={this.updateList}
            description={d}
            list={impactList}
            type={type}
          />
        )));
      case 'recom':
        let { recomList } = this.state;
        let recomListXML = [];
        return (recomListXML = recomList.map((d, idx) => (
          <EditableItem
            key={idx}
            idx={idx}
            updateList={this.updateList}
            description={d}
            list={recomList}
            type={type}
          />
        )));
      default:
        console.log('default...');
        break;
    }
  };

  addItemToList = type => {
    switch (type) {
      case 'description':
        let { descList, descListItem } = this.state;
        descList.push(descListItem);
        this.setState({ descList, descListItem: '' });
        break;
      case 'background':
        let { bgList, bgListItem } = this.state;
        bgList.push(bgListItem);
        this.setState({ bgList, bgListItem: '' });
        break;
      case 'impactAnalysis':
        let { impactList, impactListItem } = this.state;
        impactList.push(impactListItem);
        this.setState({ impactList, impactListItem: '' });
        break;
      case 'recom':
        let { recomList, recomListItem } = this.state;
        recomList.push(recomListItem);
        this.setState({ recomList, recomListItem: '' });
        break;
    }
  };

  updateList = (type, updatedList) => {
    switch (type) {
      case 'desc':
        this.setState({ descList: updatedList });
        break;
      case 'bg':
        this.setState({ bgList: updatedList });
        break;
      case 'impactAnalysis':
        this.setState({ impactList: updatedList });
        break;
      case 'recom':
        this.setState({ recomList: updatedList });
        break;
      default:
        console.log('err!!');
        break;
    }
  };

  generateDescription = () => {
    let { descArr } = this.state;
    let descXML = [];
    let { classes } = this.props;
    descXML = descArr.map(desc => {
      return (
        <div className="event--description">
          <TextField
            // error={validation.description.isInvalid}
            id={`${desc.type}_${desc.id}`}
            label={desc.label}
            multiline
            fullWidth
            // rows="3"
            rowsMax="6"
            value={this.state[desc.type]}
            onChange={e => this.handleInputChange(e, desc.type)}
            // className={classes.textField}
            margin="normal"
            defaultValue="Enter Event Description"
            variant="outlined"
            InputLabelProps={{
              shrink: true,
              classes: {
                root: classes.rootLabel,
              },
            }}
            InputProps={{
              maxLength: 5000,
              classes: {
                root: classes.rootText,
                input: classes.rootInput,
                notchedOutline: classes.notchedOutline,
              },
              endAdornment: null,
            }}
          />
        </div>
      );
    });
    return descXML;
  };

  generateBg = () => {
    let { bgArr } = this.state;
    let bgXML = [];
    let { classes } = this.props;
    bgXML = bgArr.map(bg => {
      return (
        <div className="event--bg">
          <TextField
            // error={validation.background.isInvalid}
            id={`${bg.type}_${bg.id}`}
            label={bg.label}
            multiline
            fullWidth
            // rows="3"
            rowsMax="6"
            value={this.state[bg.type]}
            onChange={e => this.handleInputChange(e, bg.type)}
            // className={classes.textField}
            defaultValue="Enter Event Background"
            margin="normal"
            variant="outlined"
            InputLabelProps={{
              shrink: true,
              classes: {
                root: classes.rootLabel,
              },
            }}
            InputProps={{
              maxLength: 5000,
              classes: {
                root: classes.rootText,
                input: classes.rootInput,
                notchedOutline: classes.notchedOutline,
              },
              endAdornment: null,
            }}
          />
        </div>
      );
    });
    return bgXML;
  };

  generateImpactAnalysis = () => {
    let { impactArr } = this.state;
    let impactXML = [];
    let { classes } = this.props;
    impactXML = impactArr.map(i => {
      return (
        <div className="event--impactAnalysis">
          <TextField
            // error={validation.impactAnalysis.isInvalid}
            id={`${i.type}_${i.id}`}
            label={i.label}
            multiline
            fullWidth
            // rows="3"
            rowsMax="6"
            value={this.state[i.type]}
            onChange={e => this.handleInputChange(e, i.type)}
            // className={classes.textField}
            margin="normal"
            defaultValue="Enter Impact Analysis"
            variant="outlined"
            InputLabelProps={{
              shrink: true,
              classes: {
                root: classes.rootLabel,
              },
            }}
            InputProps={{
              maxLength: 5000,
              classes: {
                root: classes.rootText,
                input: classes.rootInput,
                notchedOutline: classes.notchedOutline,
              },
              endAdornment: null,
            }}
          />
        </div>
      );
    });
    return impactXML;
  };

  generateRecommendation = () => {
    let { recomArr } = this.state;
    let recomXML = [];
    let { classes } = this.props;
    recomXML = recomArr.map(i => {
      return (
        <div className="event--recommendation">
          <TextField
            // error={validation.recommendations.isInvalid}
            id={`${i.type}_${i.id}`}
            label={i.label}
            multiline
            fullWidth
            // rows="3"
            rowsMax="6"
            value={this.state[i.type]}
            onChange={e => this.handleInputChange(e, i.type)}
            // className={classes.textField}
            margin="normal"
            // defaultValue="Enter Recommendations"
            variant="outlined"
            InputLabelProps={{
              shrink: true,
              classes: {
                root: classes.rootLabel,
              },
            }}
            InputProps={{
              maxLength: 5000,
              classes: {
                root: classes.rootText,
                input: classes.rootInput,
                notchedOutline: classes.notchedOutline,
              },
              endAdornment: null,
            }}
          />
        </div>
      );
    });
    return recomXML;
  };

  handleShowAlert = () => {
    this.setState({ showAlert: true });
  };

  handleCloseAlert = () => {
    this.setState({ showAlert: false });
  };

  handleCloseModal = () => {
    this.setState({ showAlert: false });
    store.dispatch(updateMultipleLocation([]));
    this.props.closeEventModal(false);
  };

  setQualityRating = qualityRating => {
    this.setState({ qualityRating });
  };

  handleIsLocSpecific = e => {
    this.setState({ isLocationSpecific: !this.state.isLocationSpecific });
  };

  toggleMultipleLocationModal = showMultipleLocationsModal =>
    this.setState({ showMultipleLocationsModal });

  render() {
    let {
      classes,
      isApprover,
      showUpdateBtn,
      hasAllTopics = false,
      multipleLocations: { selectedMultipleLocations },
    } = this.props;
    let {
      riskLevels,
      riskCategories,
      countries,
      states,
      metros,
      industries,
      selectedCountries,
      selectedStates,
      selectedRiskLevel,
      selectedCategory,
      selectedSubCategory,
      typeOfStories,
      selectedStory,
      selectedMetros,
      impactedIndustries,
      approveStatus,
      showDescBtn,
      showBgBtn,
      showDescList,
      showDescAddListBtn,
      descList,
      showBgList,
      showBgAddListBtn,
      bgList,
      impactList,
      showImpactBtn,
      showImpactAddListBtn,
      showImpactList,
      showRecomBtn,
      showRecomAddListBtn,
      showRecomList,
      recomList,
      isImportant,
      isLive,
      showAlert,
      selectedLiveTopic,
      selectedTopic,
      allLiveTopics,
      allTopics,
      subRiskCategories,
      blockage,
    } = this.state;
    let validation = this.submitted
      ? this.validator.validate(this.state)
      : this.state.validation;
    const customStyles = {
      control: provided => ({
        ...provided,

        backgroundColor: 'var(--backgroundSolid)',
        color: '#ffffff',
      }),
      option: (provided, state) => ({
        ...provided,
        zIndex: '99999',
      }),
      placeholder: provided => ({
        ...provided,

        color: '#ffffff',
      }),
      singleValue: provided => ({
        ...provided,
        color: '#ffffff',
      }),
    };
    return (
      <div id="updateEventMitkat" className="createEvent__inner">
        <h3>Edit Event</h3>
        <div className="field--create--event">
          <section className="modal--section">
            {true && (
              <section id="reactGooglePlaces" className="google--places">
                <button
                  className="addLocationBtn"
                  onClick={() => this.toggleMultipleLocationModal(true)}
                >
                  <span className="addLocationBtn__icon">
                    <i className="fas fa-map-marker-alt"></i>
                  </span>
                  <span className="addLocationBtn__text">
                    {selectedMultipleLocations.length > 0
                      ? 'Add/Update Location'
                      : 'Add New Location*'}
                  </span>
                </button>
                {validation.coordinates.isInvalid && (
                  <span className="error--text">
                    {validation.coordinates.message}
                  </span>
                )}
                <MultipleLocations
                  showModal={this.state.showMultipleLocationsModal}
                  closeMultipleLocationModal={this.toggleMultipleLocationModal}
                >
                  <div
                    id="multipleLocationMap"
                    className="modal__inner"
                    style={{ overflow: 'hidden' }}
                  >
                    <CustomMap />
                  </div>
                </MultipleLocations>
                {selectedMultipleLocations.length > 0 && (
                  <MultipleLocationList
                    selectedMultipleLocations={selectedMultipleLocations}
                    type="detail"
                  />
                )}
              </section>
            )}
            {false && (
              <div id="reactGooglePlaces" className="google--places">
                <label className="location--txt">
                  <i className="icon-uniE9F1"></i> Enter Event Location
                  <span className="asterisk">*</span>
                </label>
                <PlacesAutocomplete
                  value={this.state.address}
                  onChange={this.handleAddressChange}
                  onSelect={this.handleSelect}
                >
                  {({
                    getInputProps,
                    suggestions,
                    getSuggestionItemProps,
                    loading,
                  }) => (
                    <div>
                      {validation.address.isInvalid && (
                        <span className="error--text">
                          {validation.address.message}
                        </span>
                      )}
                      <input
                        {...getInputProps({
                          placeholder: 'Search Places ...',
                          className: 'location-search-input',
                        })}
                      />
                      {suggestions && (
                        <div className="autocomplete-dropdown-container">
                          {loading && (
                            <div className="loader-places">
                              <p>Loading...</p>
                            </div>
                          )}
                          {suggestions.map(suggestion => {
                            const className = suggestion.active
                              ? 'suggestion-item--active'
                              : 'suggestion-item';

                            const style = suggestion.active
                              ? {
                                  backgroundColor: '#fafafa',
                                  cursor: 'pointer',
                                }
                              : {
                                  backgroundColor: '#ffffff',
                                  cursor: 'pointer',
                                };
                            return (
                              <div
                                {...getSuggestionItemProps(suggestion, {
                                  className,
                                  style,
                                })}
                              >
                                <span className="list--item">
                                  {suggestion.description}
                                </span>
                              </div>
                            );
                          })}
                        </div>
                      )}
                    </div>
                  )}
                </PlacesAutocomplete>
              </div>
            )}
            {/** Event single select starts */}
            <div className="twoColWrap multi--selects">
              <div className="col-2">
                {/** change to single select is fine, change handle multiselect method to single */}
                <CustomLabel
                  title="Type Of Story"
                  classes="select--label"
                  isRequired={true}
                />
                {/* <MultiSelect
                      value={selectedStory}
                      placeholder={'Type Of Story'}
                      parentEventHandler={(value) => this.handleMultiSelect(value, 'selectedStory')}
                      // parentEventHandler={(value) => this.handleSingleSelect(value, 'selectedStory')}
                      options={typeOfStories.length > 0 ? typeOfStories : []}
                    /> */}
                <Select
                  defaultValue={selectedStory}
                  className="select--options"
                  value={selectedStory}
                  placeholder={'Type Of Story'}
                  onChange={value =>
                    this.handleSingleSelect(value, 'selectedStory')
                  }
                  options={typeOfStories.length > 0 ? typeOfStories : []}
                  styles={customStyles}
                />
                {validation.selectedStory.isInvalid && (
                  <span className="error--text">
                    {validation.selectedStory.message}
                  </span>
                )}
              </div>
              <div className="col-2">
                <CustomLabel
                  title="Countries"
                  classes="select--label"
                  isRequired={true}
                />
                <MultiSelect
                  value={selectedCountries}
                  placeholder={'Select Countries'}
                  parentEventHandler={value =>
                    this.handleMultiSelect(value, 'selectedCountries')
                  }
                  options={countries}
                />
                {validation.selectedCountries.isInvalid && (
                  <span className="error--text">
                    {validation.selectedCountries.message}
                  </span>
                )}
              </div>
              <div className="col-2">
                <CustomLabel title="States" classes="select--label" />
                <MultiSelect
                  value={selectedStates}
                  placeholder={'Select States'}
                  parentEventHandler={value =>
                    this.handleMultiSelect(value, 'selectedStates')
                  }
                  options={states}
                />
                {this.hasIndia() &&
                  validation.selectedStates &&
                  validation.selectedStates.isInvalid && (
                    <span className="error--text">
                      {validation.selectedStates.message}
                    </span>
                  )}
              </div>
              <div className="col-2">
                <CustomLabel title="Cities" classes="select--label" />
                <MultiSelect
                  value={selectedMetros}
                  placeholder={'Select Metros'}
                  parentEventHandler={value =>
                    this.handleMultiSelect(value, 'selectedMetros')
                  }
                  options={metros}
                />
                {
                  // validation.selectedMetros.isInvalid && <span className="error--text">{validation.selectedMetros.message}</span>
                }
              </div>
            </div>
            {/** Event single select ends */}
          </section>
          {/** Event Date starts */}
          <section className="modal--section">
            <h2 className="event-head--title">
              <i className="icon-uniE9F4"></i> Tracker Date
              <span className="asterisk">*</span>
            </h2>
            <div className="col-2">
              <div className="dateWrapper">
                <div className="date">
                  <DatePicker
                    label="Tracker Date"
                    value={this.state.eventDate}
                    onChange={value =>
                      this.handleDateChange('eventDate', value)
                    }
                    animateYearScrolling
                    InputLabelProps={{
                      shrink: true,
                      classes: {
                        root: classes.rootLabel,
                      },
                    }}
                    InputProps={{
                      classes: {
                        root: classes.rootInput,
                        input: classes.rootInput,
                        notchedOutline: classes.notchedOutline,
                      },
                      endAdornment: null,
                    }}
                  />
                </div>
                <div className="date">
                  <DatePicker
                    label="Event Date"
                    value={this.state.eventDateNew}
                    onChange={value =>
                      this.handleDateChange('eventDateNew', value)
                    }
                    animateYearScrolling
                    InputLabelProps={{
                      shrink: true,
                      classes: {
                        root: classes.rootLabel,
                      },
                    }}
                    InputProps={{
                      classes: {
                        root: classes.rootInput,
                        input: classes.rootInput,
                        notchedOutline: classes.notchedOutline,
                      },
                      endAdornment: null,
                    }}
                  />
                </div>
                <div className="date">
                  <DatePicker
                    label="Validity Date"
                    value={this.state.validityDate}
                    onChange={value =>
                      this.handleDateChange('validityDate', value)
                    }
                    animateYearScrolling
                    InputLabelProps={{
                      shrink: true,
                      classes: {
                        root: classes.rootLabel,
                      },
                    }}
                    InputProps={{
                      classes: {
                        root: classes.rootInput,
                        input: classes.rootInput,
                        notchedOutline: classes.notchedOutline,
                      },
                      endAdornment: null,
                    }}
                  />
                </div>
              </div>
            </div>
          </section>
          {/** Event Date ends */}
          <section className="modal--section">
            <h2 className="event-head--title">
              <i className="icon-uniE90E"></i> Event Details
            </h2>
            {/**event title starts */}
            <div className="event--title">
              <TextField
                required
                error={validation.eventTitle.isInvalid}
                id="eventTitle"
                label="Event Title"
                multiline
                fullWidth
                rows="1"
                rowsMax="3"
                value={this.state.eventTitle}
                onChange={e => this.handleInputChange(e, 'eventTitle')}
                // className={classes.textField}
                defaultValue="Enter Event Title"
                margin="normal"
                variant="outlined"
                InputLabelProps={{
                  shrink: true,
                  classes: {
                    root: classes.rootLabel,
                  },
                }}
                InputProps={{
                  maxLength: 5000,
                  classes: {
                    root: classes.rootInput,
                    input: classes.rootInput,
                    notchedOutline: classes.notchedOutline,
                  },
                  endAdornment: null,
                }}
              />
            </div>
            {/** event title ends */}
            {/**event description starts */}
            <div className="form__group description--formgroup">
              {/* <h2 className="event-head--title">Event Description</h2> */}
              {this.generateDescription()}
              {showDescList && (
                <div className="event--bg">
                  <TextField
                    // error={validation.background.isInvalid}
                    id="descList"
                    label="Add List Item"
                    multiline
                    fullWidth
                    rows="1"
                    rowsMax="10"
                    value={this.state.descListItem}
                    onChange={e => this.handleInputChange(e, 'descListItem')}
                    // className={classes.textField}
                    defaultValue="Enter List Item"
                    margin="normal"
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                      classes: {
                        root: classes.rootLabel,
                      },
                    }}
                    InputProps={{
                      maxLength: 5000,
                      classes: {
                        root: classes.rootText,
                        input: classes.rootInput,
                        notchedOutline: classes.notchedOutline,
                      },
                      endAdornment: null,
                    }}
                  />
                  <button
                    className="btn btn--addListItem add"
                    onClick={() =>
                      this.state.descListItem &&
                      this.addItemToList('description')
                    }
                  >
                    <i className="fas fa-plus"></i>
                  </button>
                </div>
              )}
              {descList.length > 0 && (
                <>
                  <h6 className={classes.listOfItems}>List of items</h6>
                  {this.showBulletPoints('desc')}
                </>
              )}
              <div className="groupBtnWrap">
                {showDescBtn && (
                  <div className="mg">
                    <button
                      className="btn btn--addListItem"
                      onClick={() => this.addPara('description')}
                    >
                      Add More Description
                    </button>
                  </div>
                )}
                {showDescAddListBtn && (
                  <div className="mg">
                    <button
                      className="btn btn--addListItem"
                      onClick={() => this.addBullets('description')}
                    >
                      Add List Items
                    </button>
                  </div>
                )}
              </div>
            </div>
            {/** event description ends */}
            {/** event bg new starts */}
            <div className="form__group bg--formgroup">
              {/* <h2 className="event-head--title">Event Background</h2>                 */}
              {this.generateBg()}
              {showBgList && (
                <div className="event--bg">
                  <TextField
                    // error={validation.background.isInvalid}
                    id="bgList"
                    label="Add List Item"
                    multiline
                    fullWidth
                    rows="1"
                    rowsMax="10"
                    value={this.state.bgListItem}
                    onChange={e => this.handleInputChange(e, 'bgListItem')}
                    // className={classes.textField}
                    defaultValue="Enter List Item"
                    margin="normal"
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                      classes: {
                        root: classes.rootLabel,
                      },
                    }}
                    InputProps={{
                      maxLength: 5000,
                      classes: {
                        root: classes.rootText,
                        input: classes.rootInput,
                        notchedOutline: classes.notchedOutline,
                      },
                      endAdornment: null,
                    }}
                  />
                  <button
                    className="btn btn--addListItem add"
                    onClick={() =>
                      this.state.bgListItem && this.addItemToList('background')
                    }
                  >
                    <i className="fas fa-plus"></i>
                  </button>
                </div>
              )}
              {bgList.length > 0 && (
                <>
                  <h6 className={classes.listOfItems}>List of items</h6>
                  {this.showBulletPoints('bg')}
                </>
              )}
              <div className="groupBtnWrap">
                {showBgBtn && (
                  <div className="mg">
                    <button
                      className="btn btn--addListItem"
                      onClick={() => this.addPara('background')}
                    >
                      Add More Background
                    </button>
                  </div>
                )}
                {showBgAddListBtn && (
                  <div className="mg">
                    <button
                      className="btn btn--addListItem"
                      onClick={() => this.addBullets('background')}
                    >
                      Add List Items
                    </button>
                  </div>
                )}
              </div>
            </div>
            {/** event bg new ends */}
            {/**event impact analysis new starts */}
            <div className="form__group bg--formgroup">
              {/* <h2 className="event-head--title">Event Impact Analysis</h2>                 */}
              {this.generateImpactAnalysis()}
              {showImpactList && (
                <div className="event--bg">
                  <TextField
                    // error={validation.background.isInvalid}
                    id="impactList"
                    label="Add List Item"
                    multiline
                    fullWidth
                    rows="1"
                    rowsMax="10"
                    value={this.state.impactListItem}
                    onChange={e => this.handleInputChange(e, 'impactListItem')}
                    // className={classes.textField}
                    defaultValue="Enter List Item"
                    margin="normal"
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                      classes: {
                        root: classes.rootLabel,
                      },
                    }}
                    InputProps={{
                      maxLength: 5000,
                      classes: {
                        root: classes.rootText,
                        input: classes.rootInput,
                        notchedOutline: classes.notchedOutline,
                      },
                      endAdornment: null,
                    }}
                  />
                  <button
                    className="btn btn--addListItem add"
                    onClick={() =>
                      this.state.impactListItem &&
                      this.addItemToList('impactAnalysis')
                    }
                  >
                    <i className="fas fa-plus"></i>
                  </button>
                </div>
              )}
              {impactList.length > 0 && (
                <>
                  <h6 className={classes.listOfItems}>List of items</h6>
                  {this.showBulletPoints('impactAnalysis')}
                </>
              )}
              <div className="groupBtnWrap">
                {showImpactBtn && (
                  <div className="mg">
                    <button
                      className="btn btn--addListItem"
                      onClick={() => this.addPara('impactAnalysis')}
                    >
                      Add More Analysis
                    </button>
                  </div>
                )}
                {showImpactAddListBtn && (
                  <div className="mg">
                    <button
                      className="btn btn--addListItem"
                      onClick={() => this.addBullets('impactAnalysis')}
                    >
                      Add List Items
                    </button>
                  </div>
                )}
              </div>
            </div>
            {/**event impact analysis new ends */}
            {/**event recommendation new starts */}
            <div className="form__group bg--formgroup">
              {/* <h2 className="event-head--title">Event Recommendations</h2>                 */}
              {this.generateRecommendation()}
              {showRecomList && (
                <div className="event--bg">
                  <TextField
                    // error={validation.background.isInvalid}
                    id="impactList"
                    label="Add List Item"
                    multiline
                    fullWidth
                    rows="1"
                    rowsMax="10"
                    value={this.state.recomListItem}
                    onChange={e => this.handleInputChange(e, 'recomListItem')}
                    // className={classes.textField}
                    defaultValue="Enter List Item"
                    margin="normal"
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                      classes: {
                        root: classes.rootLabel,
                      },
                    }}
                    InputProps={{
                      maxLength: 5000,
                      classes: {
                        root: classes.rootText,
                        input: classes.rootInput,
                        notchedOutline: classes.notchedOutline,
                      },
                      endAdornment: null,
                    }}
                  />
                  <button
                    className="btn btn--addListItem add"
                    onClick={() =>
                      this.state.recomListItem && this.addItemToList('recom')
                    }
                  >
                    <i className="fas fa-plus"></i>
                  </button>
                </div>
              )}
              {recomList.length > 0 && (
                <>
                  <h6 className={classes.listOfItems}>List of items</h6>
                  {this.showBulletPoints('recom')}
                </>
              )}
              <div className="groupBtnWrap">
                {showRecomBtn && (
                  <div className="mg">
                    <button
                      className="btn btn--addListItem"
                      onClick={() => this.addPara('recom')}
                    >
                      Add More Recommendation
                    </button>
                  </div>
                )}
                {showRecomAddListBtn && (
                  <div className="mg">
                    <button
                      className="btn btn--addListItem"
                      onClick={() => this.addBullets('recom')}
                    >
                      Add List Items
                    </button>
                  </div>
                )}
              </div>
            </div>
            {/**event recommendation new ends */}
            {/**event sources starts */}
            <div className="event--sources">
              <TextField
                // error={validation.eventSource.isInvalid}
                id="eventSources"
                label="Event Sources"
                multiline
                fullWidth
                rows="6"
                rowsMax="50"
                value={this.state.eventSource}
                onChange={e => this.handleInputChange(e, 'eventSource')}
                // className={classes.textField}
                defaultValue="Enter Event Source"
                margin="normal"
                variant="outlined"
                InputLabelProps={{
                  shrink: true,
                  classes: {
                    root: classes.rootLabel,
                  },
                }}
                InputProps={{
                  maxLength: 5000,
                  classes: {
                    root: classes.rootText,
                    input: classes.rootInput,
                    notchedOutline: classes.notchedOutline,
                  },
                  endAdornment: null,
                }}
              />
            </div>
            {/**event sources ends */}
          </section>
          <section className="modal--section">
            {/** single selects goes here */}
            <h2 className="event-head--title">
              <i className="icon-uniE99D"></i> Event Type
            </h2>
            <div className="twoColWrap single--selects">
              <div className="col-3">
                {/** change to single select is fine */}
                <CustomLabel
                  title="Risk Level"
                  classes="select--label"
                  isRequired={true}
                />
                <Select
                  defaultValue={selectedRiskLevel}
                  className="select--options"
                  value={selectedRiskLevel}
                  placeholder={'Risk Level'}
                  onChange={value =>
                    this.handleSingleSelect(value, 'selectedRiskLevel')
                  }
                  options={riskLevels.length > 0 ? riskLevels : []}
                  styles={customStyles}
                />
                {validation.selectedRiskLevel.isInvalid && (
                  <span className="error--text">
                    {validation.selectedRiskLevel.message}
                  </span>
                )}
              </div>
              <div className="col-3">
                {/** change to single select is fine */}
                <CustomLabel
                  title="Risk Category"
                  classes="select--label"
                  isRequired={true}
                />
                <Select
                  defaultValue={selectedCategory}
                  className="select--options"
                  value={selectedCategory}
                  placeholder={'Risk Category'}
                  onChange={value =>
                    this.handleSingleSelectSub(value, 'selectedCategory')
                  }
                  options={riskCategories.length > 0 ? riskCategories : []}
                  styles={customStyles}
                />
                {validation.selectedCategory.isInvalid && (
                  <span className="error--text">
                    {validation.selectedCategory.message}
                  </span>
                )}
              </div>
              <div className="col-3">
                <CustomLabel
                  title="Sub Risk Category"
                  classes="select--label"
                  isRequired={true}
                />
                <Select
                  defaultValue={selectedSubCategory}
                  className="select--options"
                  value={selectedSubCategory}
                  placeholder={'Sub Risk Category'}
                  onChange={value =>
                    this.handleSingleSelect(value, 'selectedSubCategory')
                  }
                  options={
                    subRiskCategories.length > 0 ? subRiskCategories : []
                  }
                  styles={customStyles}
                />
                {/* {validation.selectedCategory.isInvalid && (
                  <span className='error--text'>
                    {validation.selectedCategory.message}
                  </span>
                )} */}
              </div>
              <div className="col-3">
                {/** multi select is fine */}
                <CustomLabel
                  title="Industries"
                  classes="select--label"
                  isRequired={true}
                />
                <MultiSelect
                  value={impactedIndustries}
                  placeholder={'Impact Industry'}
                  parentEventHandler={value =>
                    this.handleMultiSelect(value, 'impactedIndustries')
                  }
                  options={industries}
                />
                {/* <Select                     
                      defaultValue={impactedIndustries}
                      className="select--options"
                      value={impactedIndustries}
                      placeholder={'Impact Industry'}
                      onChange={(value) => this.handleSingleSelect(value, 'impactedIndustries')}                    
                      options={industries.length > 0 ? industries : []}
                  /> */}
                {validation.impactedIndustries.isInvalid && (
                  <span className="error--text">
                    {validation.impactedIndustries.message}
                  </span>
                )}
              </div>
            </div>
          </section>
          <section className="modal--section">
            {/**impact radius starts */}
            <div className="event--sources">
              <TextField
                id="impactRadius"
                label="Impact Radius"
                fullWidth
                value={this.state.calImpactRadius}
                onChange={e => this.handleInputChange(e, 'calImpactRadius')}
                margin="normal"
                variant="outlined"
                InputLabelProps={{
                  shrink: true,
                  classes: {
                    root: classes.rootLabel,
                  },
                }}
                InputProps={{
                  classes: {
                    root: classes.rootInput,
                    input: classes.rootInput,
                    notchedOutline: classes.notchedOutline,
                  },
                  endAdornment: null,
                }}
              />
            </div>
            {/**impact radius ends */}
          </section>
          <section className="modal--section">
            {/**blockage starts */}
            <div className="event--sources">
              <TextField
                id="blockage"
                label="Supply change Blockage Duration in Days"
                fullWidth
                value={blockage}
                onChange={e => this.handleInputChange(e, 'blockage')}
                margin="normal"
                variant="outlined"
                InputLabelProps={{
                  shrink: true,
                  classes: {
                    root: classes.rootLabel,
                  },
                }}
                InputProps={{
                  classes: {
                    root: classes.rootInput,
                    input: classes.rootInput,
                    notchedOutline: classes.notchedOutline,
                  },
                  endAdornment: null,
                }}
              />
            </div>
            {/**blockage ends */}
          </section>
          <section className="modal--section">
            <div className="col-2">
              <CustomLabel
                title="Select Topic"
                classes="select--label"
                isRequired={false}
              />
              <Select
                defaultValue={hasAllTopics ? selectedTopic : selectedLiveTopic}
                className="select--options"
                value={hasAllTopics ? selectedTopic : selectedLiveTopic}
                placeholder={'Select Topic'}
                onChange={value =>
                  this.handleSingleSelect(
                    value,
                    hasAllTopics ? 'selectedTopic' : 'selectedLiveTopic',
                  )
                }
                options={
                  hasAllTopics
                    ? allTopics.length > 0
                      ? allTopics
                      : []
                    : allLiveTopics.length > 0
                    ? allLiveTopics
                    : []
                }
                styles={customStyles}
              />
              {/* {validation.selectedStory.isInvalid && (
                  <span className='error--text'>
                    {validation.selectedStory.message}
                  </span>
                )} */}
            </div>
          </section>
          <section className="modal--section">
            {/** radios goes here */}
            <h2 className="event-head--title">
              <i className="icon-uniE905"></i> Event Relevance
            </h2>
            <div className="twoColWrap radios--cont">
              <div className="col-2">
                <FormControl component="fieldset">
                  <FormLabel component="legend" className={classes.radioLabel}>
                    IRT
                  </FormLabel>
                  <RadioGroup
                    aria-label="eventImportance"
                    name="eventImportance"
                    // className={classes.group}
                    value={isImportant == 'true' ? true : false}
                    onChange={e => this.handleInputChange(e, 'isImportant')}
                  >
                    <FormControlLabel
                      value={true}
                      control={<Radio />}
                      label="Yes"
                      className={classes.radioLabel}
                    />
                    <FormControlLabel
                      value={false}
                      control={<Radio />}
                      label="No"
                      className={classes.radioLabel}
                    />
                  </RadioGroup>
                </FormControl>
              </div>
              <div className="col-2">
                <FormControl component="fieldset">
                  <FormLabel component="legend" className={classes.radioLabel}>
                    Is Event Live
                  </FormLabel>
                  <RadioGroup
                    aria-label="isLive"
                    name="isLive"
                    // className={classes.group}
                    value={isLive == 'true' ? true : false}
                    onChange={e => this.handleInputChange(e, 'isLive')}
                  >
                    <FormControlLabel
                      value={true}
                      control={<Radio />}
                      label="Yes"
                      className={classes.radioLabel}
                    />
                    <FormControlLabel
                      value={false}
                      control={<Radio />}
                      label="No"
                      className={classes.radioLabel}
                    />
                  </RadioGroup>
                </FormControl>
              </div>
            </div>
          </section>
          {isApprover && (
            <section className="modal--section">
              <div className="status__wrap">
                <label className="label-input">Approval Status</label>
                <Select
                  defaultValue={approveStatus}
                  className="select--options"
                  value={approveStatus}
                  placeholder={'Approval Status'}
                  onChange={value =>
                    this.handleSingleSelect(value, 'approveStatus')
                  }
                  options={approvalListArr}
                  styles={customStyles}
                />

                {validation.approveStatus.isInvalid && (
                  <span className="error--text">
                    {validation.approveStatus.message}
                  </span>
                )}
              </div>
              <div>
                <CustomLabel title="Quality Rating" />
                <TextField
                  type="number"
                  value={this.state.qualityRating}
                  onChange={e => this.setQualityRating(e.target.value)}
                  InputLabelProps={{
                    shrink: true,
                    classes: {
                      root: classes.rootLabel,
                    },
                  }}
                  InputProps={{
                    inputProps: {
                      max: 5,
                      min: 0,
                    },
                    classes: {
                      root: classes.rootInput,
                      input: classes.rootInput,
                      notchedOutline: classes.notchedOutline,
                    },
                    endAdornment: null,
                  }}
                  //   InputProps={{
                  //     inputProps: {
                  //         max: 5, min: 0
                  //     }
                  // }}
                  // InputProps={{
                  //   classes: {
                  //     input: classes.rootInput,
                  //     formControl: classes.formControl,
                  //     notchedOutline: isEditable
                  //       ? classes.isEditable
                  //       : classes.notEditable
                  //   }
                  // }}
                  // classes={{
                  //   root: clsx(
                  //     classes.rootTextItem,
                  //     isEditable ? classes.isEditable : classes.notEditable
                  //   )
                  // }}
                  margin="normal"
                  variant="outlined"
                />
              </div>
            </section>
          )}
          <section className="modal--section">
            <FormControlLabel
              classes={{
                label: classes.label,
              }}
              control={
                <Checkbox
                  className={classes.size}
                  checked={this.state.isLocationSpecific}
                  onChange={this.handleIsLocSpecific}
                  value={this.state.isLocationSpecific}
                  icon={
                    <CheckBoxOutlineBlankIcon className={classes.sizeIcon} />
                  }
                  checkedIcon={<CheckBoxIcon className={classes.sizeIcon} />}
                />
              }
              label={'Is story location specific?'}
            />
          </section>
        </div>
        <div className="actionables">
          {/* <Button 
            onClick={() => this.props.closeEventModal(false)} 
            variant="contained"
            color="default"
          >
            Cancel
          </Button> */}
          <Button
            onClick={() => this.handleShowAlert()}
            variant="contained"
            color="default"
          >
            Cancel
          </Button>
          <Button
            onClick={
              isApprover
                ? this.handleUpdateEvent
                : e => this.handleEventSubmit(e)
            }
            style={{ marginLeft: 20, backgroundColor: '#2980b9' }}
            variant="contained"
            color="secondary"
          >
            {isApprover || showUpdateBtn ? 'Update' : 'Create Event'}
          </Button>
        </div>
        {showAlert && (
          <AlertDialog
            isOpen={showAlert}
            title="Cancel Changes"
            description="You are about to cancel event changes, you won't be able to get them back, are you sure?"
            parentEventHandler={e => this.handleCloseModal()}
            handleClose={() => this.handleCloseAlert()}
          />
        )}
      </div>
    );
  }
}

export default EditEventUI;
