import React, { Component } from 'react';
import { withCookies } from 'react-cookie';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import { fetchApi } from '../../../../helpers/http/fetch';
import { createValueLabel } from '../../../../helpers/utils';
import { Button, TextField } from '@material-ui/core';
import { DatePicker } from 'material-ui-pickers';
import MultiSelect from '../../../ui/multiSelect';
import injectSheet from 'react-jss';
import style from './style';
import Select from 'react-select';
import { toggleToast } from '../../../../actions';
import store from '../../../../store';
import AlertDialog from '../../../ui/alertDialog';
import './style.scss';

const customStyles = {
  control: provided => ({
    ...provided,
    backgroundColor: 'var(--backgroundSolid) ',
    color: 'var(--whiteMediumEmp)',
  }),
  option: (provided, state) => ({
    ...provided,
    zIndex: '99999',
  }),
  placeholder: provided => ({
    ...provided,
    color: 'var(--whiteMediumEmp)',
  }),
  singleValue: provided => ({
    ...provided,
    color: 'var(--whiteMediumEmp)',
  }),
};
@withCookies
@injectSheet(style)
class CreateQueries extends Component {
  state = {
    showAlert: false,
    clientList: [],
    originationList: [
      { value: 'NA', label: 'NA' },
      { value: 'Email', label: 'Email' },
      { value: 'WhatsApp', label: 'WhatsApp' },
      { value: 'Call', label: 'Call' },
      { value: 'Mobile/Web App', label: 'Mobile/Web App' },
    ],
    selectedOrigination: [],
    selectedClients: [],
    queryDate: new Date(),
    queryTitle: '',
    queryDetails: '',
  };

  componentDidMount() {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    this.getClientList(authToken);
  }
  //to get dropdown list of clients
  getClientList = authToken => {
    let reqObj = {
      url: `${API_ROUTES.getClientList}`,
      method: 'POST',
      isAuth: true,
      authToken,
      showToggle: true,
    };
    fetchApi(reqObj)
      .then(data => {
        this.setState({ clientList: data });
      })
      .catch(e => console.log(e));
  };

  handleSingleSelect = (value, type) => {
    this.setState({ [type]: value });
  };
  handleDateChange = (type, value) => this.setState({ [type]: value });

  handleInputChange = (e, type) => {
    this.setState({ [type]: e.target.value });
  };

  handleShowAlert = () => {
    this.setState({ showAlert: true });
  };

  handleCloseAlert = () => {
    this.setState({ showAlert: false });
  };

  handleCloseModal = () => {
    this.setState({ showAlert: false });
    this.props.closeEventModal('createQueries');
  };

  handleEventSubmit = e => {
    e.preventDefault();
    let { cookies } = this.props;
    let {
      selectedClients,
      queryDate,
      queryTitle,
      queryDetails,
      selectedOrigination,
    } = this.state;
    let authToken = cookies.get('authToken');
    let reqData = {
      querydate: queryDate,
      clientid: selectedClients.value,
      queryOrigination: selectedOrigination.value,
      title: queryTitle,
      details: queryDetails,
    };

    let reqObj = {
      url: API_ROUTES.addQuery,
      data: reqData,
      isAuth: true,
      authToken,
      method: 'POST',
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        if (data && data.success == true) {
          let toastObj = {
            toastMsg: data.message,
            showToast: true,
          };
          this.props.closeEventModal('createQueries');
          store.dispatch(toggleToast(toastObj));
        } else {
          let toastObj = {
            toastMsg: 'Something went wrong!, please try again',
            showToast: true,
          };

          store.dispatch(toggleToast(toastObj));
        }
      })
      .catch(e => console.log(e));
  };

  render() {
    let { classes } = this.props;
    let {
      clientList,
      selectedClients,
      selectedOrigination,
      originationList,
      showAlert,
    } = this.state;

    return (
      <div id="createQueries" className="createEvent__inner">
        <h3>Create New queries</h3>
        <div className="queryModal">
          <Select
            defaultValue={selectedClients}
            className="select--options"
            value={selectedClients}
            placeholder={'Select Clients'}
            onChange={value =>
              this.handleSingleSelect(value, 'selectedClients')
            }
            options={createValueLabel(clientList, 'clients')}
            styles={customStyles}
          />

          <div className="date">
            <DatePicker
              label="Query Date"
              value={this.state.queryDate}
              onChange={value => this.handleDateChange('queryDate', value)}
              animateYearScrolling
              InputLabelProps={{
                shrink: true,
                classes: {
                  root: classes.rootLabel,
                },
              }}
              InputProps={{
                classes: {
                  root: classes.rootInput,
                  input: classes.rootInput,
                  notchedOutline: classes.notchedOutline,
                },
                endAdornment: null,
              }}
            />
          </div>
          <div className="event--title">
            <TextField
              id="queryTitle"
              label="Query Title"
              multiline
              fullWidth
              rows="1"
              rowsMax="3"
              value={this.state.queryTitle}
              onChange={e => this.handleInputChange(e, 'queryTitle')}
              margin="normal"
              variant="outlined"
              InputLabelProps={{
                shrink: true,
                classes: {
                  root: classes.rootLabel,
                },
              }}
              InputProps={{
                maxLength: 5000,
                classes: {
                  root: classes.rootInput,
                  input: classes.rootInput,
                  notchedOutline: classes.notchedOutline,
                },
                endAdornment: null,
              }}
            />
          </div>
          <div className="event--bg">
            <TextField
              id="queryDetails"
              label="Query Details"
              multiline
              fullWidth
              rows="1"
              rowsMax="10"
              value={this.state.queryDetails}
              onChange={e => this.handleInputChange(e, 'queryDetails')}
              margin="normal"
              variant="outlined"
              InputLabelProps={{
                shrink: true,
                classes: {
                  root: classes.rootLabel,
                },
              }}
              InputProps={{
                maxLength: 5000,
                classes: {
                  root: classes.rootText,
                  input: classes.rootInput,
                  notchedOutline: classes.notchedOutline,
                },
                endAdornment: null,
              }}
            />
          </div>
          <Select
            defaultValue={selectedOrigination}
            className="select--options"
            value={selectedOrigination}
            placeholder={'Origination of Query'}
            onChange={value =>
              this.handleSingleSelect(value, 'selectedOrigination')
            }
            options={originationList}
            styles={customStyles}
          />
        </div>
        <div className="actionables">
          <Button
            onClick={e => this.handleShowAlert()}
            variant="contained"
            color="default"
          >
            Cancel
          </Button>
          <Button
            onClick={e => this.handleEventSubmit(e)}
            style={{ marginLeft: 20, backgroundColor: '#2980b9' }}
            variant="contained"
            color="secondary"
          >
            Create Query
          </Button>
        </div>
        {showAlert && (
          <AlertDialog
            isOpen={showAlert}
            title="Cancel Changes"
            description="You are about to cancel event changes, you won't be able to get them back, are you sure?"
            parentEventHandler={e => this.handleCloseModal()}
            handleClose={() => this.handleCloseAlert()}
          />
        )}
      </div>
    );
  }
}

export default CreateQueries;
