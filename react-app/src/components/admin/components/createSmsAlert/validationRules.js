const validationRules = [
    {
        field: 'eventTitle',
        method: 'isEmpty',
        validWhen: false,
        message: 'Event title is required'
    },
    {
        field: 'address',
        method: 'isEmpty',
        validWhen: false,
        message: 'Address is required'
    },
    {
        field: 'selectedRiskLevel',
        method: 'isEmpty',
        validWhen: false,
        message: 'RiskLevel is required'
    },
    {
        field: 'selectedCategory',
        method: 'isEmpty',
        validWhen: false,
        message: 'Category is required'
    },
    {
        field: 'selectedStory',
        method: 'isEmpty',
        validWhen: false,
        message: 'Story is required'
    },
    {
        field: 'impactedIndustries',
        method: 'isEmpty',
        validWhen: false,
        message: 'Industries is required'
    },
    {
        field: 'selectedCountries',
        method: 'isEmpty',
        validWhen: false,
        message: 'Countries is required'
    },
    // {
    //     field: 'selectedStates',
    //     method: 'isEmpty',
    //     validWhen: false,
    //     message: 'States is required'
    // },
    // {
    //     field: 'selectedMetros',
    //     method: 'isEmpty',
    //     validWhen: false,
    //     message: 'Metros is required'
    // },
]

export default validationRules