export default {
  singleSelect: {
    width: '100%',
    color: 'var(--whiteHighEmp) !important',
    borderColor: 'var(--whiteHighEmp) !important ',
  },
  rootLabel: {
    color: 'rgba(255,255,255, 0.67) !important',
  },
  rootInput: {
    color: 'var(--whiteHighEmp) !important',
  },
  underline: {
    '&:after': {
      borderBottom: '2px solid #34495e !important',
    },
  },
  notchedOutline: {
    borderColor: 'var(--whiteHighEmp) !important ',
  },
  rootSelect: {
    color: 'rgba(255,255,255, 0.67) !important',
    borderBottom: '1px solid #eee !important',
  },
};
