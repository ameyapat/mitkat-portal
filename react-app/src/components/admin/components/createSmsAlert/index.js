import React, { Component } from 'react';
import './style.scss';
//react google places
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from 'react-places-autocomplete';
//material ui components
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import MultiSelect from '../../../ui/multiSelect';
//helpers
import { withCookies } from 'react-cookie';
import { getListData } from '../../helpers/utils';
import { filterArrObjects, fetchCountries, fetchStates, 
  fetchCities, fetchIndustries } from '../../../../helpers/utils';
import { fetchApi } from '../../../../helpers/http/fetch';
import { API_ROUTES } from '../../../../helpers/http/apiRoutes';
import store from '../../../../store';
import { toggleToast, refreshEvents } from '../../../../actions/';
import injectSheet from 'react-jss';
import style from './style';
import FormValidator from '../../../../helpers/validation/FormValidator';
import validationRules from './validationRules';
import CustomLabel from '../../../ui/customLabel';
import AlertDialog from '../../../ui/alertDialog';
import { withRouter } from 'react-router-dom';

const stateRules = [{  
  field: 'selectedStates',
  method: 'isEmpty',
  validWhen: false,
  message: 'States is required'
}]

const updatedRules = [...validationRules, ...stateRules];

// const searchOptions = {
//   componentRestrictions: {country: 'in'}
// }
@withRouter
@withCookies
@injectSheet(style)
class CreateSmsAlert extends Component {

  validator = new FormValidator(validationRules)
  submitted = false;

  state = {
    address: '',
    riskLevels: [],
    riskCategories: [],
    typeOfStories: [],
    countries: [],
    states: [],
    metros: [],
    industries: [],
    selectedRiskLevel: "",
    selectedCategory: "",
    selectedStory: "",
    selectedCountries: [],
    selectedStates: [],
    selectedMetros: [],
    eventDetails: null,
    eventDate: null,
    validityDate: null,
    eventSource: '',
    eventTitle: '',
    location: null,
    description: '',
    background: '',
    impactAnalysis: '',
    recommendations: '',
    isImportant: false,
    isLive: false,
    impactedIndustries: [],
    //
    location: {
      lat: '',
      lng: ''
    },
    validation: this.validator.valid(),
    showAlert: false,
  }

  componentDidMount(){
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');

    this.getRiskLevels('riskLevel');
    this.getRiskCategories('riskCategory');
    this.getStories('typeOfStory');
    fetchCountries(authToken).then(countries => this.setState({countries}));
    fetchStates(authToken).then(states => this.setState({states}));
    fetchCities(authToken).then(metros => this.setState({metros}));
    fetchIndustries(authToken).then(industries => this.setState({industries}));
  }

  
  /** handle text area changes */
  handleInputChange = (e, type) => {
    this.setState({[type]: e.target.value}, () => console.log(this.state));
  }

  handleChange = (e, type) => {
    switch(type){
      case 'riskLevel':
        this.setState({selectedRiskLevel: e.target.value});
      break;
      case 'riskCategory':
        this.setState({selectedCategory: e.target.value});
      break;
      case 'typeOfStory':
        this.setState({selectedStory: e.target.value});
      break;
    }
  }

  getRiskLevels = (type) => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');

      getListData(type, authToken)
      .then(data => {
        this.setState({riskLevels: data})
      })
  }

  getStories = (type) => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');

      getListData(type, authToken)
      .then(data => {
        this.setState({typeOfStories: data})
      })
  }

  getRiskCategories = (type) => {

    let { cookies } = this.props;
    let authToken = cookies.get('authToken');

      getListData(type, authToken)
      .then(data => {
        this.setState({riskCategories: data})
      })
  }

  hasIndia = () => {
    let { selectedCountries } = this.state;
    let hasIndia= false;
    selectedCountries.map(c => c.label === 'India' ? hasIndia = true : hasIndia = false);
    return hasIndia;
    // selectedCountries.map(c => {
    //   if(c.label === 'India'){
    //     return true;
    //   }
    // })
  }

  handleEventSubmit = (e) => {
    e.preventDefault();

    let { cookies, history } = this.props;
    let authToken = cookies.get('authToken');
    this.validator = this.hasIndia() ? new FormValidator(updatedRules) : new FormValidator(validationRules);
    let validation = this.validator.validate(this.state);
    this.submitted = true;
    let { eventDetails, selectedCountries, eventSource,
    eventTitle, selectedRiskLevel, selectedCategory,
    impactedIndustries, selectedMetros, selectedStates,
    selectedStory, location: { lat, lng } } = this.state;

    this.setState({ validation });

    if(validation.isValid){
      eventDetails = { 
        "title": eventTitle,
        "links": eventSource,
        "latitude": lat,//lat
        "longitude": lng,//long
        "riskLevel": selectedRiskLevel,
        "riskcategory": selectedCategory,
        "regionType": selectedStory,
        "impactindustry": filterArrObjects(impactedIndustries),
        "countries": filterArrObjects(selectedCountries),
        "state": filterArrObjects(selectedStates),
        "cities": filterArrObjects(selectedMetros)
      }
      
      let reqObj = {
        url: API_ROUTES.createSmsAlert,
        data: eventDetails,
        isAuth: true,
        authToken,
        method: 'POST',
        showToggle: true,
      }
      
        fetchApi(reqObj)
        .then(data => {
          if(data && data.status == 200){
          let toastObj = {
            toastMsg: data.message,
            showToast: true
          }
          this.props.closeEventModal('createSmsAlert');
          store.dispatch(toggleToast(toastObj));
          history.push(this.props.match.url + '/editEvents');
          store.dispatch(refreshEvents(true))
        }else if(data && data.status == 502){
          let toastObj = {
            toastMsg: data.message,
            showToast: true
          }          
          store.dispatch(toggleToast(toastObj));         
        }
      })
    }
  }
  
  riskLevelsXML = () => {
    let { riskLevels } = this.state;
    let riskLevelXML = [];
    riskLevelXML = riskLevels.map(item => <MenuItem key={item.id} value={item.id}>{item.level}</MenuItem>);
    return riskLevelXML;
  }

  riskCategoriesXML = () => {
    let { riskCategories } = this.state;
    let riskCategoryXML = [];
    riskCategoryXML = riskCategories.map(item => <MenuItem key={item.id} value={item.id}>{item.riskcategory}</MenuItem>);
    return riskCategoryXML;
  }

  storiesXML = () => {
    let { typeOfStories } = this.state;
    let storiesXML = [];
    storiesXML = typeOfStories.map(item => <MenuItem key={item.id} value={item.id}>{item.regiontype}</MenuItem>);
    return storiesXML;
  }

  handleMultiSelect = (value, type) => {
    let values = this.state[type];
    if(values.includes(value)){
      let index = values.indexOf(value);
      values.splice(index, 1);
    }else{
      values = [...value];
    }
    this.setState({[type]: values}, () => console.log(this.state));
  }

  handleAddressChange = address => {
    this.setState({ address });
  };
  /** function for setting location lat & lng */
  handleSelect = address => {
    geocodeByAddress(address)
      .then(results => getLatLng(results[0]))
      .then(latLng => {
        let { location } = this.state;
        location['lat'] = latLng.lat;
        location['lng'] = latLng.lng;
        this.setState({location, address}, () => console.log(this.state));
      })
      .catch(error => {
        console.error('Error', error)
        let toastObj = {showToast: true, toastObj: 'Error getting location, try again!'};
        store.dispatch(toggleToast(toastObj));
      });
  };

  handleShowAlert = () => {
    this.setState({showAlert: true});
  }

  handleCloseAlert = () => {
      this.setState({showAlert: false});
  }

  handleCloseModal = () => {
    this.setState({showAlert: false});
    this.props.closeEventModal('createSmsAlert')
  }


  render() {
    let { classes } = this.props;
    let { riskLevels, riskCategories, countries, states, metros,
      industries, selectedCountries, selectedStates, selectedRiskLevel,
      selectedCategory, typeOfStories, selectedStory, selectedMetros,
      impactedIndustries, showAlert } = this.state;
    let validation = this.submitted ? this.validator.validate(this.state) : this.state.validation;

    return (
      <div className="createSms__inner">
        <h3>Create sms alert</h3>
        <div className="field--create--event">
          <div id="reactGooglePlaces" className="google--places">
          <label className="location--txt"><i className="icon-uniE9F1"></i> Enter Event Location</label>
          <PlacesAutocomplete
            value={this.state.address}
            onChange={this.handleAddressChange}
            onSelect={this.handleSelect}
            // searchOptions={searchOptions}
          >
            {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
              <div>
                {
                  validation.address.isInvalid && <span className="error--text">{validation.address.message}</span>
                }
                <input
                  {...getInputProps({
                    placeholder: 'Search Places ...',
                    className: 'location-search-input',
                  })}
                />
                {
                  suggestions &&
                  <div className="autocomplete-dropdown-container">
                    {loading && <div className="loader-places"><p>Loading...</p></div>}
                    {suggestions.map(suggestion => {
                      const className = suggestion.active
                        ? 'suggestion-item--active'
                        : 'suggestion-item';
                      // inline style for demonstration purpose
                      const style = suggestion.active
                        ? { backgroundColor: '#fafafa', cursor: 'pointer' }
                        : { backgroundColor: '#ffffff', cursor: 'pointer' };
                      return (
                        <div
                          {...getSuggestionItemProps(suggestion, {
                            className,
                            style,
                          })}
                        >
                          <span className="list--item">{suggestion.description}</span>
                        </div>
                      );
                    })}
                  </div>
                }
              </div>
            )}
          </PlacesAutocomplete>
          </div>
          <h2 className="event-head--title"><i className="icon-uniE90E"></i> Event Details</h2>
          {/**event title */}
          <div className="event--title">
              <TextField
                  id="eventTitle"
                  label="Event Title"
                  multiline
                  fullWidth
                  rows="3"
                  value={this.state.eventTitle}
                  onChange={ (e) => this.handleInputChange(e, 'eventTitle')}
                  // className={classes.textField}
                  defaultValue="Enter Event Title"
                  margin="normal"
                  variant="outlined"
                  InputLabelProps={{
                    shrink: true,
                    classes: {
                       root: classes.rootLabel
                    }
                  }}
                  InputProps={{
                    maxLength: 5000,
                      classes: {
                          root: classes.rootInput,
                          input: classes.rootInput,
                          notchedOutline: classes.notchedOutline,
                      },
                      endAdornment: null
                  }}
                />
              </div>
          {/**event sources */}
            <div className="event--sources">
                <TextField
                  error={validation.eventTitle.isInvalid}
                  id="eventSources"
                  label="Event Sources"
                  multiline
                  fullWidth
                  // rows="3"
                  rowsMax="6"
                  value={this.state.eventSource}
                  onChange={ (e) => this.handleInputChange(e, 'eventSource')}
                  // className={classes.textField}
                  defaultValue="Enter Event Source"
                  margin="normal"
                  variant="outlined"
                  InputLabelProps={{
                    shrink: true,
                    classes: {
                       root: classes.rootLabel
                    }
                  }}
                  InputProps={{
                    maxLength: 5000,
                      classes: {
                          root: classes.rootInput,
                          input: classes.rootInput,
                          notchedOutline: classes.notchedOutline,
                      },
                      endAdornment: null
                  }}
                  />
            </div>           
              {/** multi selects goes here */}
              <h2 className="event-head--title"><i className="icon-uniE9EC"></i> Event Locations</h2>
              <div className="twoColWrap multi--selects">
                <div className="col-2">
                  <CustomLabel title="Countries" classes="select--label" />
                  <MultiSelect
                      value={selectedCountries}
                      placeholder={'Select Countries'}
                      parentEventHandler={(value) => this.handleMultiSelect(value, 'selectedCountries')}
                      options={countries}
                  />
                  {
                    validation.selectedCountries.isInvalid && <span className="error--text">{validation.selectedCountries.message}</span>
                  }
                </div>
                <div className="col-2">
                  <CustomLabel title="States" classes="select--label" />
                  <MultiSelect
                      value={selectedStates}
                      placeholder={'Select States'}
                      parentEventHandler={(value) => this.handleMultiSelect(value, 'selectedStates')}
                      options={states}
                  />     
                  {
                    this.hasIndia() && validation.selectedStates && validation.selectedStates.isInvalid && <span className="error--text">{validation.selectedStates.message}</span>
                  }             
                </div>
                <div className="col-2">
                  <CustomLabel title="Cities" classes="select--label" />
                  <MultiSelect
                      value={selectedMetros}
                      placeholder={'Select Metros'}
                      parentEventHandler={(value) => this.handleMultiSelect(value, 'selectedMetros')}
                      options={metros}
                  />
                </div>
                <div className="col-2">
                  <CustomLabel title="Industries" classes="select--label" />
                  <MultiSelect
                      value={impactedIndustries}
                      placeholder={'Impact Industry'}
                      parentEventHandler={(value) => this.handleMultiSelect(value, 'impactedIndustries')}
                      options={industries}
                  />
                  {
                    validation.impactedIndustries.isInvalid && <span className="error--text">{validation.impactedIndustries.message}</span>
                  }
                </div>
              </div>
              {/** single selects goes here */}
              <h2 className="event-head--title"><i className="icon-uniE99D"></i> Event Type</h2>
              <div className="twoColWrap single--selects">
                <div className="col-3"> 
                  <FormControl 
                    error={validation.selectedRiskLevel.isInvalid}
                    variant="outlined" 
                    classes={{
                      root: classes.singleSelect
                    }}
                  >
                    <InputLabel
                      classes={{
                        outlined: {
                          background: 'red',
                          height: 10,
                          width: '100%'
                        }
                      }}
                      ref={ref => {
                        this.riskLevel = ref;
                      }}
                      htmlFor="riskLevel"
                      className={classes.rootLabel}
                    >
                      Risk Level
                    </InputLabel>
                    <Select
                      value={selectedRiskLevel}
                      onChange={(e) => this.handleChange(e, 'riskLevel')}
                      inputProps={{
                          classes: {
                              select: classes.rootSelect,
                          }                          
                      }}
                      input={
                        <OutlinedInput
                          labelWidth={0}
                          name="Risk Level"
                          id="riskLevel"
                        />
                      }
                    >
                      {riskLevels.length > 0 && this.riskLevelsXML()}
                    </Select>
                  </FormControl>
                </div>
                <div className="col-3"> 
                  <FormControl 
                    error={validation.selectedCategory.isInvalid}
                    variant="outlined" 
                    classes={{
                      root: classes.singleSelect
                    }}
                  >
                    <InputLabel
                      ref={ref => {
                        this.riskCategory = ref;
                      }}
                      htmlFor="riskCategory"
                      className={classes.rootLabel}
                    >
                      Risk Category
                    </InputLabel>
                    <Select
                      value={selectedCategory}
                      onChange={(e) => this.handleChange(e, 'riskCategory')}
                      inputProps={{
                          classes: {
                              select: classes.rootSelect,
                          }                          
                      }}
                      input={
                        <OutlinedInput
                          labelWidth={this.state.labelWidth}
                          name="Risk Category"
                          id="riskCategory"
                        />
                      }
                    >
                      {riskCategories.length > 0 && this.riskCategoriesXML()}
                    </Select>
                  </FormControl>
                </div>
                <div className="col-3"> 
                  <FormControl 
                    error={validation.selectedStory.isInvalid}
                    variant="outlined" 
                    classes={{
                      root: classes.singleSelect
                    }}
                  >
                    <InputLabel
                      ref={ref => {
                        this.typeOfStory = ref;
                      }}
                      htmlFor="typeOfStory"
                      className={classes.rootLabel}
                    >
                      Type Of Story
                    </InputLabel>
                    <Select
                      value={selectedStory}
                      onChange={(e) => this.handleChange(e, 'typeOfStory')}
                      inputProps={{
                        classes: {
                            select: classes.rootSelect,
                        }                          
                    }}
                      input={
                        <OutlinedInput
                          labelWidth={this.state.labelWidth}
                          name="Type Of Story"
                          id="typeOfStory"
                        />
                      }
                    >
                      {typeOfStories.length > 0 && this.storiesXML()}
                    </Select>
                  </FormControl>
                </div>
            </div>
        </div>
        <div className="actionables">
          {/* <Button 
            onClick={(e) => this.props.closeEventModal('createSmsAlert')} 
            variant="contained" 
            color="default"
          >
            Cancel
          </Button> */}
          <Button 
            onClick={(e) => this.handleShowAlert()} 
            variant="contained" 
            color="default"
          >
            Cancel
          </Button>
          <Button 
            onClick={(e) => this.handleEventSubmit(e)} 
            style={{marginLeft: 20, backgroundColor: '#2980b9'}} 
            variant="contained" 
            color="secondary"
          >
            Create Sms Alert
          </Button>
        </div>
        {
              showAlert &&
              <AlertDialog 
                  isOpen={showAlert}
                  title="Cancel Changes" 
                  description="You are about to cancel event changes, you won't be able to get them back, are you sure?" 
                  parentEventHandler={(e) => this.handleCloseModal()} 
                  handleClose={() => this.handleCloseAlert()} 
              />
          }
      </div>
    )
  }
}

export default CreateSmsAlert
