export default {
  rootLabel: {
    color: 'rgba(255,255,255, 0.67) !important',
  },
  rootInput: {
    color: 'var(--whiteHighEmp) !important',
  },
  underline: {
    '&:after': {
      borderBottom: '2px solid #34495e !important',
    },
  },
  notchedOutline: {
    borderColor: 'var(--whiteHighEmp) !important ',
  },
  rootSelect: {
    color: 'rgba(255,255,255, 0.67) !important',
    borderBottom: '1px solid #eee !important',
  },
  srNo: {
    justifyContent: 'center !important',
  },
  title: {
    flexDirection: 'column',
    paddingRight: '5px',
    justifyContent: 'center !important',
  },
  alertTableList: {
    height: '375px',
  },
};
