import React, { useEffect, useState } from 'react';
import './style.scss';
import moment from 'moment';
import {
  categories,
  eventTypeConstants,
} from '../../../../client/components/rimeDashboard/helpers/constants';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import Typography from '@mui/material/Typography';
import { useSelector, useDispatch } from 'react-redux';
import CustomModal from '../../../../ui/modal';
import ArticleModal from '../../../../client/components/rimeEventCard/articleModal';
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye';
import { withCookies } from 'react-cookie';
import Loader from '../../../../ui/loader';
import MenuItem from '@material-ui/core/MenuItem';
import { TextField } from '@material-ui/core';
import injectSheet from 'react-jss';
import styles from './style';
import CreateEmailAlert from '../../createEmailAlert';
import { link } from 'fs-extra';

const pinMappingRgba = {
  1: 'low',
  2: 'medium',
  3: 'high',
};

const AlertViewCard = props => {
  const {
    srno,
    eventId,
    title,
    categoryId,
    userName,
    eventDate,
    eventType,
    link,
    sourceLinks,
    location,
    noOfArticles,
    lastUpdatedDate,
    tags,
    cookies,
    selectedFeed,
    classes,
  } = props;

  const [isNew, setIsNew] = useState(true);
  const [eventTypeTag, setEventType] = useState('');
  const [category, setCategory] = useState('');
  const [showModal, setShowModal] = useState(false);
  const setTheme = useSelector(state => state.themeChange.setDarkTheme);
  const refreshDuration = useSelector(state => state.rime.refreshDuration);
  const [isFetching, setIsFetching] = useState(false);
  const [eventTitle, setEventTitle] = useState(title);
  const [showCreateAlert, setShowCreateAlert] = useState(false);
  const dispatch = useDispatch();

  const TIMEOUT = refreshDuration * 60000;

  const mapRiskCategory = () => {
    eventTypeConstants.map(subitem => {
      if (eventType === subitem.id) {
        setEventType(subitem.label);
      }
    });
    categories.map(subitem => {
      if (categoryId === subitem.id) {
        setCategory(subitem.label);
      }
    });
  };

  const handleEditEvent = eventd => {
    setShowCreateAlert(true);
  };

  const getEventTypeList = () => {
    let eventTypeList = [];
    eventTypeList = eventTypeConstants.map(i => (
      <MenuItem value={i.id}>{i.label}</MenuItem>
    ));
    return eventTypeList;
  };

  const getCategoryList = () => {
    let categoryList = [];
    categoryList = categories.map(i => (
      <MenuItem value={i.id}>{i.label}</MenuItem>
    ));
    return categoryList;
  };

  const handleModal = () => {
    setShowModal(false);
  };
  const handleCloseModal = () => {
    setShowCreateAlert(false);
  };

  const showArticleModal = () => {
    setShowModal(true);
  };

  useEffect(() => {
    mapRiskCategory();
    setTimeout(() => {
      setIsNew(false);
    }, TIMEOUT);
  });

  let tag = tags && tags.split(',');
  let allLinks = sourceLinks && sourceLinks.split(',');

  return (
    <div className="alertView_card">
      {isFetching && <Loader />}
      <div id="alertViewCard__card">
        <Card className={`alertViewCardList ${pinMappingRgba[eventType]}`}>
          <div className="alertViewCard__header">
            <div className="alertViewCard_pill_container">
              <div
                className={`alertViewCard__pill ${eventType &&
                  pinMappingRgba[eventType]}`}
              >
                <Typography
                  component="span"
                  className="alertViewCard__cat__pill"
                >
                  {eventTypeTag ? eventTypeTag : 'Not Available'}
                </Typography>
              </div>
              <div className="eventFilteredCard_riskCategory_pill">
                <Typography
                  component="span"
                  className="alertViewCard__category__cat__pill"
                >
                  {category ? category : 'Not Available'}
                </Typography>
              </div>
              {/* <div className="eventFilteredCard_userName">
                <Typography
                  variant="subtitle1"
                  color="text.secondary"
                  component="span"
                  className="alertViewCard__eventbody__Head"
                >
                  <span className="title">Surfer Name:</span>
                </Typography>
                <Typography
                  component="span"
                  className="alertViewCard__eventbody__Head"
                >
                  &nbsp;
                  {userName ? userName : 'Not Available'}
                </Typography>
              </div> */}
            </div>
            <div
              className="article__content__pill"
              onClick={() => showArticleModal()}
            >
              <Typography
                component="span"
                className="alertViewCard__eventbody__Head"
              >
                &nbsp;
                {noOfArticles ? noOfArticles : ' '}
              </Typography>
              <span>&nbsp;</span>
              <RemoveRedEyeIcon className="eyeIcon" />
            </div>
            <div className="buttons__wrap">
              <button
                className="btn--edit"
                onClick={() => handleEditEvent(eventTitle)}
              >
                Create Alert
              </button>
            </div>
            <div className="alertViewCard__card__innercolumn">
              <div>
                {/* <Typography component="span" className="alertViewCard_date">
                  &nbsp;{moment(utcDate).format('Do MMM YYYY')}
                </Typography> */}
                <Typography
                  variant="subtitle1"
                  color="text.secondary"
                  component="span"
                  className="alertViewCard__date"
                >
                  Updated:&nbsp;
                </Typography>
                <Typography component="span" className="alertViewCard__date">
                  {moment(eventDate).format('Do MMM YYYY, h:mm:ss a')}
                </Typography>
              </div>
            </div>
          </div>
          <div className="alertViewCard__card__row1">
            <div className="alertViewCard__card__innercolumn2">
              <Box sx={{ display: 'flex', flexDirection: 'column' }}>
                <div className="alertViewCard_heading">
                  <Typography
                    component="div"
                    className="alertViewCard__eventtitle__wrap"
                    style={{ color: 'black' }}
                  >
                    {title}
                  </Typography>

                  <div className="event_screening_location">
                    <Typography
                      variant="subtitle1"
                      color="text.secondary"
                      component="span"
                      className="alertViewCard__eventbody__subHead"
                    >
                      Location:
                    </Typography>
                    <Typography
                      component="span"
                      className="alertViewCard__eventbody__location_wrap"
                    >
                      &nbsp;{location ? location : 'Not Available'}
                    </Typography>
                  </div>
                </div>
              </Box>
            </div>
          </div>
        </Card>
        <CustomModal showModal={showModal}>
          <div
            id="rimeEventArchivesModalCont"
            className={`modal__inner ${setTheme ? 'dark' : 'light'}`}
          >
            <ArticleModal
              alllinks={allLinks}
              closeEventModal={() => handleModal()}
            />
          </div>
        </CustomModal>

        <CustomModal showModal={showCreateAlert}>
          <div id="createEmail" className="modal__inner">
            <CreateEmailAlert
              closeEventModal={() => handleCloseModal()}
              riskCategory={category}
              categoryId={categoryId}
              eventLink={link}
              title={eventTitle}
            />
          </div>
        </CustomModal>
      </div>
    </div>
  );
};

export default withCookies(injectSheet(styles)(AlertViewCard));
