import moment from 'moment';
import React, { Component } from 'react';
import {
  categories,
  eventTypeConstants,
  riskLevels,
} from '../../../client/components/rimeDashboard/helpers/constants';
import {
  eDate,
  istDates,
} from '../../../client/components/rimeDashboard/helpers/utils';
import EventType from '../../../ui/eventType';
import RiskLevels from '../../../ui/riskLevels';
import TableList from '../../../ui/tablelist';
import TableListCell from '../../../ui/tablelist/TableListCell';
import TableListRow from '../../../ui/tablelist/TableListRow';
import './style.scss';
import styles from './style';
import injectSheet from 'react-jss';
import CreateEmailAlert from '../createEmailAlert';
import CustomModal from '../../../ui/modal';
import MonitoringScreenCard from '../monitoringScreenCard';
import AlertViewCard from './AlertViewCard';

@injectSheet(styles)
class AlertViewUI extends Component {
  state = {
    eventType: '',
    showCreateAlert: false,
    eventTitle: '',
    riskCategory: '',
    eventLink: '',
    riskcategoryId: 0,
  };
  getRiskCategory = id => {
    let value = '';
    categories.map(item => {
      if (item.id === id) {
        value = item.label;
      }
    });
    return value;
  };

  getEventType = id => {
    //  const eventType =this.state;
    let value = '';
    eventTypeConstants.map(item => {
      if (item.id === id) {
        value = item.label;
        //  this.setState({eventType:item.label})
      }
    });
    return value;
  };

  utcDate = eventDate => {
    let value = eDate(eventDate);
    return value;
  };
  
  istDate = eventDate => {
    let value = istDates(eventDate);
    return value;
  };
  handleCreateEmailAlert = item => {
    this.setState({
      showCreateAlert: true,
      eventTitle: item.eventTitle,
      riskCategory: this.getRiskCategory(item.eventPrimaryRiskCategory) || '-',
      riskCategoryId: item.eventPrimaryRiskCategory,
      eventLink: item.eventLink,
    });
  };
  handleCloseModal = () => {
    this.setState({ showCreateAlert: false });
  };
  geteventDate = date => {
    let value = eDate(date);
    return value;
  };

  getRiskLevel = id => {
    let value = '';
    riskLevels.map(item => {
      if (item.id === id) {
        value = item.label;
      }
    });
    return value;
  };
  getRimeCells = () => {
    const { eventType } = this.state;
    let { selectedCountry, classes } = this.props;
    let itemXML = [];
    let rimeEventList = [];
    rimeEventList = selectedCountry.rimeEvents;
    itemXML = rimeEventList.map((item, index) => {
      return (
        <AlertViewCard
          id={index}
          srno={index + 1}
          eventId={item.id}
          title={item.eventTitle}
          sourceLinks={item.eventAllNewsLinks}
          location={item.eventLoc}
          noOfArticles={item.count}
          categoryId={item.eventPrimaryRiskCategory}
          secondaryCatergoryId={item.eventSecondaryRiskCategory}
          link={item.eventLink}
          userName={item.userName}
          hasClose={false}
          eventDate={this.utcDate(item.eventLatestUpdate)}
          lastUpdatedDate={item.eventLatestUpdate}
          eventType={item.eventType}
          imgURL={item.eventImageUrl}
          tags={item.eventTags}
          language={item.language}
          handleDelete={this.refreshState}
          handleEdit={this.refreshEditState}
          selectedFeed={item}
        />
      );
    });
    return itemXML;
  };

  getRiskTrackerCells = () => {
    let { selectedCountry, classes } = this.props;
    let itemXML = [];
    let rimeTrackerList = [];
    rimeTrackerList = selectedCountry?.riskTrackerEvents;
    itemXML = rimeTrackerList?.map((item, index) => {
      return (
        <AlertViewCard
          id={index}
          srno={index + 1}
          eventId={item.id}
          title={item.title}
          sourceLinks={item.eventAllNewsLinks}
          location={item.locationName}
          noOfArticles={item.count}
          categoryId={item.riskcategory}
          secondaryCatergoryId={item.eventSecondaryRiskCategory}
          link={item.sourceLink}
          userName={item.userName}
          hasClose={false}
          eventDate={this.istDate(item.eventDate)}
          lastUpdatedDate={item.eventDate}
          eventType={item.risklevel}
          imgURL={item.eventImageUrl}
          tags={item.eventTags}
          language={item.language}
          handleDelete={this.refreshState}
          handleEdit={this.refreshEditState}
          selectedFeed={item}
        />
      );
    });
    return itemXML;
  };

  render() {
    const { selectedCountry, closeModal } = this.props;
    const {
      eventType,
      eventTitle,
      riskCategory,
      riskcategoryId,
      eventLink,
      showCreateAlert,
    } = this.state;
    return (
      <div>
        <div className="alertViewTable">
          {selectedCountry?.rimeEvents?.length > 0 ? (
            <div className="rime_alerts">
              <h1>Surfer Alerts</h1>
              {this.getRimeCells()}
            </div>
          ) : (
            <div className="riskTracker_alerts">
              <h1> No Rime Alerts found</h1>
            </div>
          )}
          {selectedCountry?.riskTrackerEvents?.length > 0 ? (
            <div className="riskTracker_alerts">
              <h1>Risk Tracker Alerts</h1>
              {this.getRiskTrackerCells()}
            </div>
          ) : (
            <div className="riskTracker_alerts">
              <h1> No Risk Tracker Alerts found</h1>
            </div>
          )}
        </div>
        <CustomModal showModal={showCreateAlert}>
          <div id="createEmail" className="modal__inner">
            <CreateEmailAlert
              closeEventModal={() => this.handleCloseModal()}
              riskCategory={riskCategory}
              categoryId={riskcategoryId}
              eventLink={eventLink}
              title={eventTitle}
            />
          </div>
        </CustomModal>
      </div>
    );
  }
}

export default AlertViewUI;
