import { fetchApi } from '../../../helpers/http/fetch';
import { API_ROUTES } from '../../../helpers/http/apiRoutes';
import Cookies from 'universal-cookie';

// const cookies = new Cookies();
// const authToken = cookies.get('authToken');

export const convertCountriesArrToObj = (arr) => {
  return arr.map((i) => ({label: i.countryname, value: i.id}))
}

export const getCovidCountries = (isAuth = true) => {
  const cookies = new Cookies();
  const authToken = cookies.get('authToken');

  let reqObj = {
      isAuth,
      authToken,
      method: 'GET',
      url: API_ROUTES.getCountryList,
      showToggle: true,
  }
  return new Promise((res, rej) => {
    fetchApi(reqObj)
    .then((response) => {
      res(response);
    })
    .catch((e) => rej(e))
  })
}


export const getCovidCountriesReadingList = () => {
  const cookies = new Cookies();
  const authToken = cookies.get('authToken');

  let reqObj = {
      isAuth: true,
      authToken,
      method: 'GET',
      url: API_ROUTES.getCountryReadingList,
      showToggle: true,
  }
  return new Promise((res, rej) => {
    fetchApi(reqObj)
    .then((response) => {
      res(response);
    })
    .catch((e) => rej(e))
  })
}

export const getEditableCountryDetails = (isAuth = true, countryId) => {
  const cookies = new Cookies();
  const authToken = cookies.get('authToken');

  let reqObj = {
    isAuth,
    authToken,
    method: 'GET',
    url: API_ROUTES.editCountryDetails + "/" + countryId,
    showToggle: true,
  }
  return new Promise((res, rej) => {
    fetchApi(reqObj)
    .then(response => {
      res(response)
    })
    .catch(e => rej(e))
  })
}

export const getEditableCountryReading = (readingId) => {
  const cookies = new Cookies();
  const authToken = cookies.get('authToken');

  let reqObj = {
    isAuth: true,
    authToken,
    method: 'GET',
    url: API_ROUTES.editCountryReading + "/" + readingId,
    showToggle: true,
  }
  return new Promise((res, rej) => {
    fetchApi(reqObj)
    .then(response => {
      res(response)
    })
    .catch(e => rej(e))
  })
}

export const updateCountryDetails = (data) => {
  const cookies = new Cookies();
  const authToken = cookies.get('authToken');

  let reqObj = {
    isAuth: true,
    authToken,
    method: 'POST',
    url: API_ROUTES.editCountryList,
    data,
    showToggle: true,
  }
  return new Promise((res, rej) => {
    fetchApi(reqObj)
    .then(response => {
      res(response)
    })
    .catch(e => rej(e))
  })
}

export const updateCountryCountryReading = (data) => {
  const cookies = new Cookies();
  const authToken = cookies.get('authToken');

  let reqObj = {
    isAuth: true,
    authToken,
    method: 'POST',
    url: API_ROUTES.updateCoronaCountry,
    data,
    showToggle: true,
  }
  return new Promise((res, rej) => {
    fetchApi(reqObj)
    .then(response => {
      res(response)
    })
    .catch(e => rej(e))
  })
}

export const getListData = (type, authToken, rest) => {

    let reqObj = {
        isAuth: true,
        authToken,
        method: 'POST',
        url: null,
        showToggle: true,
    }

    switch(type){
        case 'riskLevel':
            reqObj.url = API_ROUTES.fetchRiskLevel;
            break;
        case 'riskCategory':
            reqObj.url = API_ROUTES.fetchRiskCategory;
            break;
        case 'subRiskCategory':
            reqObj.url = `${API_ROUTES.fetchSubRiskCategory}/${rest.value}`;
            reqObj.method = 'GET';
            break;
        case 'typeOfStory':
            reqObj.url = API_ROUTES.fetchTypeOfStory;
            break;
        default:
            console.log('Something went wrong!');
            break;
    }

    return new Promise((resolve, reject) => {
        fetchApi(reqObj)
        .then(data => {
            resolve(data);
        })
        .catch((e) => reject(e))
    })
}

export function getApprovalObj(statusId){
    switch(statusId){
        case 1:
            return {"value": 1, "label": "Pending Approval"}
        case 2:
            return {"value": 2, "label": "Approved"}
        case 3:
            return {"value": 2, "label": "Rejected"}
        default:
            return {"value": '', "label": ""}
    }
}


export function setHeight(el){
    const windowHeight = window.innerHeight;
    const elHeight = el.getBoundingClientRect().top;
    const newHeight = windowHeight - elHeight;
    return newHeight;
}

export function downloadSummary(authToken){
    
        let reqObj = {
          url: API_ROUTES.downloadSummary,
          isAuth: true,
          authToken,
          method: "POST",
          isBlob: true,
          showToggle: true,
        };
    
        fetchApi(reqObj)
          .then(blobObj => {
            let a = document.createElement("a");
            document.body.appendChild(a);
            let objUrl = new Blob([blobObj], { type: "application/pdf" });
            let url = window.URL.createObjectURL(objUrl);
            a.href = url;
            a.download = "summary-report-pdf";
            a.click();
            window.URL.revokeObjectURL(url);
          })
          .catch(e => console.log(e));
}

export function downloadExcel(authToken){
    let reqObj = {
      url: API_ROUTES.downloadDistrictReportExcel,
      isAuth: true,
      authToken,
      method: "POST",
      isBlob: true,
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(blobObj => {
        let a = document.createElement("a");
        document.body.appendChild(a);
        let objUrl = new Blob([blobObj], { type: "application/vnd.ms-excel" });
        let url = window.URL.createObjectURL(objUrl);
        a.href = url;
        a.download = "district-report-excel.xls";
        a.click();
        window.URL.revokeObjectURL(url);
      })
      .catch(e => console.log(e));
};

export function downloadEmpExcel(authToken){
    let reqObj = {
      url: API_ROUTES.downloadEmpExcel,
      isAuth: authToken ? true : false,
      ...authToken && { authToken: authToken },
      method: "POST",
      isBlob: true,
      showToggle: true,
    };
    fetchApi(reqObj)
      .then(blobObj => {
        let a = document.createElement("a");
        document.body.appendChild(a);
        let objUrl = new Blob([blobObj], { type: "application/vnd.ms-excel" });
        let url = window.URL.createObjectURL(objUrl);
        a.href = url;
        a.download = "employee-details-excel.xls";
        a.click();
        window.URL.revokeObjectURL(url);
      })
      .catch(e => console.log(e));
};

export function downloadLocationExcel(authToken){
    let reqObj = {
      url: API_ROUTES.excelLocationReport,
      isAuth: authToken ? true : false,
      ...authToken && { authToken: authToken },
      method: "POST",
      isBlob: true,
      showToggle: true,
    };
    fetchApi(reqObj)
      .then(blobObj => {
        let a = document.createElement("a");
        document.body.appendChild(a);
        let objUrl = new Blob([blobObj], { type: "application/vnd.ms-excel" });
        let url = window.URL.createObjectURL(objUrl);
        a.href = url;
        a.download = "location-details-excel.xls";
        a.click();
        window.URL.revokeObjectURL(url);
      })
      .catch(e => console.log(e));
};

export function downloadDistrictReport(authToken){
    let reqObj = {
      url: API_ROUTES.downloadDistrictReport,
      isAuth: true,
      authToken,
      method: "POST",
      isBlob: true,
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(blobObj => {
        let a = document.createElement("a");
        document.body.appendChild(a);
        let objUrl = new Blob([blobObj], { type: "application/pdf" });
        let url = window.URL.createObjectURL(objUrl);
        a.href = url;
        a.download = "district-report-pdf";
        a.click();
        window.URL.revokeObjectURL(url);
      })
      .catch(e => console.log(e));
  };

export function downloadMetroReport(authToken){
    let reqObj = {
      url: API_ROUTES.downloadCovidCity,
      isAuth: true,
      authToken,
      method: "POST",
      isBlob: true,
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(blobObj => {
        let a = document.createElement("a");
        document.body.appendChild(a);
        let objUrl = new Blob([blobObj], { type: "application/pdf" });
        let url = window.URL.createObjectURL(objUrl);
        a.href = url;
        a.download = "metro-report-pdf";
        a.click();
        window.URL.revokeObjectURL(url);
      })
      .catch(e => console.log(e));
  };

  export function downloadRimeExcel(authToken, value){
    let reqObj = {
      url: `${API_ROUTES.rimeTimeRange}`,
      isAuth: true,
      authToken,
      method: "POST",
      isBlob: true,
      showToggle: true,
      data: value,
    };

    fetchApi(reqObj)
      .then(blobObj => {
        let a = document.createElement("a");
        document.body.appendChild(a);
        let objUrl = new Blob([blobObj], { type: "application/vnd.ms-excel" });
        let url = window.URL.createObjectURL(objUrl);
        a.href = url;
        a.download = "rime-archive-events-excel.xls";
        a.click();
        window.URL.revokeObjectURL(url);
      })
      .catch(e => console.log(e));
}; 

export const getCoronaCountryDashboard = (isAuth = true, countryId) => {
  const cookies = new Cookies();
  const authToken = cookies.get('authToken');

  let reqObj = {
    isAuth,
    authToken,
    method: 'GET',
    url: API_ROUTES.getCoronaCountryDashboard + "/" + countryId,
    showToggle: true,
  }
  return new Promise((res, rej) => {
    fetchApi(reqObj)
    .then(response => {
      res(response)
    })
    .catch(e => rej(e))
  })
}; 


export const downloadCountryReport = (countryId) => {
  const cookies = new Cookies();
  const authToken = cookies.get('authToken');

  let reqObj = {
      url: API_ROUTES.downloadCountryReport + "/" + countryId, 
      isAuth: false,
      authToken,
      isBlob: true,
      method: 'GET',
      showToggle: true,
  }
  fetchApi(reqObj)
  .then(blobObj => {
      var a = document.createElement("a");
      document.body.appendChild(a);
      let objUrl = new Blob([blobObj], {type: "application/pdf"});
      let url = window.URL.createObjectURL(objUrl);
      a.href = url;
      a.download = 'country-report';
      a.click();
      window.URL.revokeObjectURL(url);
  })
}


export function editEventDetails(eventId){

  const cookies = new Cookies();
  const authToken = cookies.get('authToken');

  const reqObj = {
    url: `${API_ROUTES.editTodaysEvent}/${eventId}`,
    isAuth: true,
    authToken,
    method: 'POST',
    showToggle: true,
  }

  return new Promise((res, rej) => {
    try{
      fetchApi(reqObj)
      .then((response) => {
        res(response)
      })
    }catch(e){
      rej(e);
    }
  })

}

export function updateEvent(eventDetails){

  const cookies = new Cookies();
  const authToken = cookies.get('authToken');

  let reqObj = {
      url: API_ROUTES.updateTodaysEvent,
      data: eventDetails,
      isAuth: true,
      authToken,
      method: 'POST',
      showToggle: true,
    }

    return new Promise((res, rej) => {
      try{
        fetchApi(reqObj)
        .then((response) => res(response))
      }catch(e){
        rej(e)
      }
    })

    // fetchApi(reqObj)
    // .then(data => {
    //     if(data && data.status == 200){
    //         let toastObj = {
    //             toastMsg: 'Event Updated Successfully!',
    //             showToast: true
    //           }
    //           this.handleModal(false);
    //           store.dispatch(toggleToast(toastObj));
    //           this.getEvents();
    //       }else if(data && data.status == 502){
    //           let toastObj = {
    //               toastMsg: 'Something went wrong!, please try again',
    //               showToast: true
    //             }
    //             store.dispatch(toggleToast(toastObj));
    //       }
    // })
    // .catch(e => {
    //     let toastObj = {
    //       toastMsg: 'Error editing event!',
    //       showToast: true
    //     }
    //     store.dispatch(toggleToast(toastObj));
    // })
}



