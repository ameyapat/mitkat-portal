import React, { Component } from 'react';
//components @material-ui
import Modal from '@material-ui/core/Modal';
import CreateUser from '../components/CreateUser';
//components
import Table from '../../ui/table';
//helpers
import { fetchApi } from '../../../helpers/http/fetch';
import { API_ROUTES } from '../../../helpers/http/apiRoutes';
import { connect } from 'react-redux';
import { withCookies } from 'react-cookie';
//
import './style.scss';

@withCookies
@connect(state => {
  return {
    appState: state.appState,
  };
})
class UserManagement extends Component {
  state = {
    showCreateUserModal: false,
    showUserDetails: false,
    userList: [],
    calcHeight: '100%',
  };

  UNSAFE_componentWillMount() {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');

    if (!authToken) {
      this.props.history.push('/');
    }
  }

  componentDidMount() {
    this.getUserList();
    this.setState({ calcHeight: (window.innerHeight / 100) * 8 + 'vh' });
  }

  getUserList = () => {
    let { cookies } = this.props;
    let authToken = cookies.get('authToken');
    let params = {
      url: API_ROUTES.adminListAllUsers,
      method: 'POST',
      isAuth: true,
      authToken: authToken,
      data: '',
      showToggle: true,
    };

    fetchApi(params)
      .then(data => {
        if (data.status === 401) {
          return;
        } else {
          let { userList } = this.state;
          userList = data;
          this.setState({ userList });
        }
      })
      .catch(e => console.log(e));
  };

  handleCloseModal = () => {
    this.setState({ showCreateUserModal: false });
  };

  handleOpenModal = () => {
    this.setState({ showCreateUserModal: true });
  };

  handleUserDetailsModal = () => {
    this.setState({ showUserDetails: true });
  };

  closeUserDetailsModal = () => {
    this.setState({ showUserDetails: false });
  };

  handleInputChange = (type, value) => {
    // console.log(type, value);
  };

  render() {
    let { userList } = this.state;

    return (
      <div className="userManagement">
        <div className="more--options">
          <span>
            <i className="fas fa-users"></i>
            <label>User List</label>
          </span>
          <button className="btn--user" onClick={this.handleOpenModal}>
            <span className="icon-wrap">
              <i className="fas fa-user-plus"></i>
            </span>
            <span className="btn--label">Create User</span>
          </button>
        </div>
        <div
          className="user--list-wrap"
          style={{ height: this.state.calcHeight }}
        >
          {
            <Table
              tableData={userList}
              showViewDetails={false}
              handleUserDetails={this.handleUserDetailsModal}
              handleInputChange={this.handleInputChange}
              getUserList={this.getUserList}
              meta={{
                enableClearance: true,
              }}
            />
          }
        </div>
        <Modal
          open={this.state.showCreateUserModal}
          // onClose={this.handleCloseModal}
        >
          <div className="modal-paper">
            <CreateUser
              userType="user"
              getUserList={this.getUserList}
              handleCloseModal={this.handleCloseModal}
            />
          </div>
        </Modal>
      </div>
    );
  }
}

export default UserManagement
