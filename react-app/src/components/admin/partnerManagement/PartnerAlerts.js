import React, { Component, Fragment } from 'react';
import './style.scss';
// //helpers
import { withCookies } from 'react-cookie';
import {
  getPartnerEvents,
  deletePartnerEvent,
  updtePartnerEvent,
  getPartnerEventById,
} from '../../../requestor/admin/requestor';
// //components
import TableList from '../../ui/tablelist';
import TableListCell from '../../ui/tablelist/TableListCell';
import TableListRow from '../../ui/tablelist/TableListRow';
import CustomModal from '../../ui/modal';
import Empty from '../../ui/empty';
import EditEventUI from '../components/createEventUI/EditEventUI';
import { fetchApi } from '../../../helpers/http/fetch';
import { API_ROUTES } from '../../../helpers/http/apiRoutes';
import store from '../../../store';
import { toggleToast } from '../../../actions';

@withCookies
class PartnerAlerts extends Component {
  state = {
    tableData: [],
    showEventModal: false,
    eventDetails: null,
  };

  componentDidMount() {
    this.getEvents();
  }

  getEvents = () => {
    getPartnerEvents().then(data => this.setState({ tableData: data }));
  };

  getTableItems = () => {
    let { tableData } = this.state;

    return tableData.map(i => {
      return (
        <TableListRow key={i.eventId}>
          <TableListCell value={i.eventTime || '-'} />
          <TableListCell value={i.title || '-'} />
          <TableListCell value={i.country || '-'} />
          <TableListCell value={i.riskCategory || '-'} />
          <TableListCell value={i.riskLevel || '-'} />
          <TableListCell value={i.partnerName || '-'} />
          <TableListCell classes="btn--actions--wrap">
            <button
              className="btn--edit"
              onClick={() => this.handleEditEvent(i.eventId)}
            >
              <i className="icon-uniE93E"></i>
            </button>
            <button
              className="btn--delete"
              onClick={() => this.handleDeleteEvent(i.eventId)}
            >
              <i className="icon-uniE949"></i>
            </button>
          </TableListCell>
        </TableListRow>
      );
    });
  };

  handleDeleteEvent = id => {
    deletePartnerEvent(id)
      .then(data => {
        let toastObj = {
          toastMsg: data.message,
          showToast: data.success,
        };
        store.dispatch(toggleToast(toastObj));
        this.getEvents();
      })
      .catch(e => {
        let toastObj = {
          toastMsg: 'Error deleting report',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      });
  };

  handleEditEvent = eventId => {
    getPartnerEventById(eventId)
      .then(data => {
        this.setState({ eventDetails: data }, () => {
          this.handleModal(true);
        });
      })
      .catch(e => {
        let toastObj = {
          toastMsg: 'Error fetching details',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      });
  };

  updateEvent = eventDetails => {
    updtePartnerEvent(eventDetails)
      .then(data => {
        let toastObj = {
          toastMsg: 'Event Updated Successfully!',
          showToast: true,
        };
        this.handleModal(false);
        store.dispatch(toggleToast(toastObj));
        this.getEvents();
      })
      .catch(e => {
        let toastObj = {
          toastMsg: 'Error editing event!',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      });
  };

  handleModal = show => {
    this.setState({ showEventModal: show });
  };

  render() {
    let { tableData, eventDetails } = this.state;

    return (
      <div className="approveEmail__wrap">
        {/* <Fragment>
          <h2>Email Alerts</h2>
          {tableData.length > 0 && (
            <div className="isImportantWrap">
              <label>
                IRT: <i class="fas fa-star"></i>
              </label>
              <label>
                Not IRT: <i class="far fa-star"></i>
              </label>
            </div>
          )}
          </Fragment> */}
        {tableData.length > 0 ? (
          <Fragment>
            <div className="tablelist__head">
              <div>
                <label>Creation</label>
              </div>
              <div>
                <label>Title</label>
              </div>
              <div>
                <label>Country</label>
              </div>
              <div>
                <label>Risk Category</label>
              </div>
              <div>
                <label>Risk Level</label>
              </div>
              <div>
                <label>Partner Name</label>
              </div>
              <div>
                <label>Edit/Delete</label>
              </div>
            </div>
            <TableList classes={{ root: 'darkBlue' }}>
              {tableData.length > 0 && this.getTableItems()}
            </TableList>
          </Fragment>
        ) : (
          <Empty title="No email alerts!" />
        )}

        <CustomModal
          showModal={this.state.showEventModal}
          closeModal={() => this.handleModal(false)}
        >
          <div id="editPartnerApprover" className="modal__inner">
            {eventDetails && (
              <EditEventUI
                eventDetails={this.state.eventDetails}
                closeEventModal={() => this.handleModal(false)}
                isApprover={true}
                updateEvent={this.updateEvent}
              />
            )}
          </div>
        </CustomModal>
      </div>
    );
  }
}

export default PartnerAlerts;
