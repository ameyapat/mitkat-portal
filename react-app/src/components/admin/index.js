import React, { Component } from 'react';
import './sass/style.scss';
//component
import RightDash from './components/rightDash';
import LeftNav from './components/leftNav';
import { withCookies } from 'react-cookie';
import { connect } from 'react-redux';

@withCookies
@connect(state => {
  return {
    authToken: state.appState.authToken,
    userRole: state.appState.userRole,
  };
})
class Admin extends Component {
  UNSAFE_componentWillMount() {
    let { cookies } = this.props; // authToken, userRole
    let userRoleFromCookie = cookies.get('userRole');

    if (
      userRoleFromCookie === undefined ||
      userRoleFromCookie === 'ROLE_CLIENT' ||
      userRoleFromCookie === 'ROLE_CLIENT_USER'
    ) {
      this.props.history.push('/');
      return;
    }
  }

  render() {
    return (
      <div id="admin" className="adminWrap">
        <LeftNav />
        <RightDash />
      </div>
    );
  }
}

export default Admin;
