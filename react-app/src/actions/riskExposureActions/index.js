export const SET_RISK_DASHBOARD_DATA = 'SET_RISK_DASHBOARD_DATA';

export const setRiskDashboardData = payload => {
  return {
    type: SET_RISK_DASHBOARD_DATA,
    payload,
  };
};
