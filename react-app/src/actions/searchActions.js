export const TOGGLE_SEARCH_BAR = 'TOGGLE_SEARCH_BAR';

export const toggleSearchBar = (show) => {
    return{
        type: TOGGLE_SEARCH_BAR,
        payload: show
    }
}

