export const SET_EMP_TRACKING_DATA = 'SET_EMP_TRACKING_DATA';

export const setEmpTrackingData = payload => {
  return {
    type: SET_EMP_TRACKING_DATA,
    payload,
  };
};
