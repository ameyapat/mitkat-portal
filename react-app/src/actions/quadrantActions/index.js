export const SET_QUADRANT_LIST = 'SET_QUADRANT_LIST';
export const UPDATE_QUADRANT_DETAIL = 'UPDATE_QUADRANT_DETAIL';
export const UPDATE_QUADRANT_POINTS = 'UPDATE_QUADRANT_POINTS';
export const UPDATE_QUADRANT_POINT_DETAILS = 'UPDATE_QUADRANT_POINT_DETAILS';
export const UPDATE_QUADRANTS = 'UPDATE_QUADRANTS';
export const SET_SELECTED_QUADRANT = 'SET_SELECTED_QUADRANT';
export const SET_SELECTED_EVENT_ITEM = 'SET_SELECTED_EVENT_ITEM';
export const UPDATE_FILTER_OPTIONS = 'UPDATE_FILTER_OPTIONS';
export const UPDATE_REGION = 'UPDATE_REGION';

export const setQuadrantList = payload => {
  return {
    type: SET_QUADRANT_LIST,
    payload,
  };
};

export const updateQuadrantDetail = payload => {
  return {
    type: UPDATE_QUADRANT_DETAIL,
    payload,
  };
};

export const updateQuadrantPoints = payload => {
  return {
    type: UPDATE_QUADRANT_POINTS,
    payload,
  };
};

export const updateQuadrantPointDetails = payload => {
  return {
    type: UPDATE_QUADRANT_POINT_DETAILS,
    payload,
  };
};

export const updateQuadrants = payload => {
  return {
    type: UPDATE_QUADRANTS,
    payload,
  };
};

export const setSelectedQuadrant = payload => {
  return {
    type: SET_SELECTED_QUADRANT,
    payload,
  };
};

export const setSelectedEventItem = payload => {
  return {
    type: SET_SELECTED_EVENT_ITEM,
    payload,
  };
};

export const updateFilterOptions = payload => {
  return {
    type: UPDATE_FILTER_OPTIONS,
    payload,
  };
};

export const updateRegion = payload => {
  return {
    type: UPDATE_REGION,
    payload,
  };
};
