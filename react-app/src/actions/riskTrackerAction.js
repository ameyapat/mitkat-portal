export const GET_RISK_ALERTS = 'GET_RISK_ALERTS';
export const GET_RISK_ALERTS_SUCCESS = 'GET_RISK_ALERTS_SUCCESS';
export const GET_RISK_ALERTS_CLEAR = 'GET_RISK_ALERTS_SUCCESS_CLEAR';
export const GET_RISK_ALERTS_ERROR = 'GET_RISK_ALERTS_ERROR';
export const TOGGLE_ENABLE_SEARCH = "TOGGLE_ENABLE_SEARCH";

export const fetchRiskTracker = (reqPayload) => {
    return{
        type: GET_RISK_ALERTS,
        payload: reqPayload
    }
}

export const fetchRiskSuccess = (successPayload) => {
    return{
        type: GET_RISK_ALERTS_SUCCESS,
        payload: successPayload
    }
}

export const fetchRiskError = (errorPayload) => {
    return{
        type: GET_RISK_ALERTS_ERROR,
        payload: errorPayload
    }
}

export const toggleEnableSearch = (payload) => {
    return {
        type: TOGGLE_ENABLE_SEARCH,
        payload
    }
}