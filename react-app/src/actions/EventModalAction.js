export const OPEN_EVENT_MODAL = 'OPEN_EVENT_MODAL';
export const SET_IMPACT_RADIUS = 'SET_IMPACT_RADIUS';

export function openEventModal(eventObj) {
  return {
    type: OPEN_EVENT_MODAL,
    payload: eventObj,
  };
}

export function setImpactRadius(impactRadius) {
  return {
    type: SET_IMPACT_RADIUS,
    payload: impactRadius,
  };
}
