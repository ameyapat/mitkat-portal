export const SET_DASHBOARD_DATA = 'SET_DASHBOARD_DATA';
export const UPDATE_DASHBOARD_FILTERS = 'UPDATE_DASHBOARD_FILTERS';
export const UPDATE_DASHBOARD_EVENT_DETAILS = 'UPDATE_DASHBOARD_EVENT_DETAILS';
export const TOGGLE_BARCLAYS_LOCATION = 'TOGGLE_BARCLAYS_LOCATION';
export const TOGGLE_SETTINGS_MODAL = 'TOGGLE_SETTINGS_MODAL';
export const TOGGLE_EVENT_AND_RISK_DATA = 'TOGGLE_EVENT_AND_RISK_DATA';
export const TOGGLE_UPLOAD_MODAL = 'TOGGLE_UPLOAD_MODAL';

// Filter Compare
export const SET_DASHBOARD_DATA_LEFT = 'SET_DASHBOARD_DATA_LEFT';
export const UPDATE_DASHBOARD_FILTERS_LEFT = 'UPDATE_DASHBOARD_FILTERS_LEFT';
export const SET_DASHBOARD_DATA_RIGHT = 'SET_DASHBOARD_DATA_RIGHT';
export const UPDATE_DASHBOARD_FILTERS_RIGHT = 'UPDATE_DASHBOARD_FILTERS_RIGHT';

export const toggleBarclaysLocation = show => {
  return {
    type: TOGGLE_BARCLAYS_LOCATION,
    payload: show,
  };
};

export const toggleSettingsModal = showSettings => {
  return {
    type: TOGGLE_SETTINGS_MODAL,
    payload: showSettings,
  };
};

export const toggleUploadModal = showUploads => {
  return {
    type: TOGGLE_UPLOAD_MODAL,
    payload: showUploads,
  };
};

export const togglEventAndRiskData = payload => {
  return {
    type: TOGGLE_EVENT_AND_RISK_DATA,
    payload,
  };
};

export const setDashboardData = payload => {
  return {
    type: SET_DASHBOARD_DATA,
    payload,
  };
};

export const updateFilters = payload => {
  return {
    type: UPDATE_DASHBOARD_FILTERS,
    payload,
  };
};
export const updateDashboardEventDetails = payload => {
  return {
    type: UPDATE_DASHBOARD_EVENT_DETAILS,
    payload,
  };
};

// Filter one
export const setDashboardDataLeft = payload => {
  return {
    type: SET_DASHBOARD_DATA_LEFT,
    payload,
  };
};

export const updateFiltersLeft = payload => {
  return {
    type: UPDATE_DASHBOARD_FILTERS_LEFT,
    payload,
  };
};

// Filter two
export const setDashboardDataRight = payload => {
  return {
    type: SET_DASHBOARD_DATA_RIGHT,
    payload,
  };
};

export const updateFiltersRight = payload => {
  return {
    type: UPDATE_DASHBOARD_FILTERS_RIGHT,
    payload,
  };
};
