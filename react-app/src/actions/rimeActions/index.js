import {
  LOADING_FEED,
  SET_BOUNDS,
  SET_MANUAL_BOUNDS,
  UPDATE_CATEGORIES,
  UPDATE_RELEVANCY,
  UPDATE_EVENT_TYPE,
  UPDATE_LOCATIONS,
  UPDATE_REFRESH_DURATION,
  UPDATE_OPERATIONS,
  UPDATE_TAGS,
  UPDATE_START_DATE,
  UPDATE_END_DATE,
  UPDATE_LANGUAGE,
  UPDATE_FEED,
  SEARCH_FEED,
  UPDATE_SEARCH_FEED,
  UPDATE_ADMIN_SELECTED_FEED,
  UPDATE_SELECT_ALL_RISK_CAT,
  UPDATE_SELECT_ALL_RELEVANCY,
  UPDATE_SELECT_ALL_LANGUAGE,
  UPDATE_EVENT_SEARCH_CATEGORIES,
  UPDATE_EVENT_SEARCH_RELEVANCY,
  UPDATE_EVENT_SEARCH_LOCATIONS,
  UPDATE_EVENT_SEARCH_OPERATIONS,
  UPDATE_EVENT_SEARCH_TAGS,
  UPDATE_EVENT_SEARCH_LANGUAGE,
  UPDATE_EVENT_SEARCH_START_DATE,
  UPDATE_EVENT_SEARCH_END_DATE,
  UPDATE_EVENT_SEARCH_SELECT_ALL_RISK_CAT,
  UPDATE_EVENT_SEARCH_SELECT_ALL_RELEVANCY,
  UPDATE_EVENT_SEARCH_SELECT_ALL_LANGUAGE,
  GET_RIME_EVENT_PINS,
  SET_LIVE_LIST_ITEM_DETAILS,
  GET_RIME_SUBSCRIPTION_DATA,
  SHOW_LOCATION_PIN,
  SHOW_API_HIT_TIME,
  SHOW_GEOFENCE_EVENT_NOS,
  LIVE_MONITOR_SEARCH_FEED,
  UPDATE_LIVE_MONITOR_SEARCH_FEED,
} from './constant';

export const setLiveListItemDetails = payload => {
  return {
    type: SET_LIVE_LIST_ITEM_DETAILS,
    payload,
  };
};

export const loadingFeed = payload => ({ type: LOADING_FEED, payload });
export const setBoundsAction = payload => ({ type: SET_BOUNDS, payload });
export const setManualBoundsAction = payload => ({
  type: SET_MANUAL_BOUNDS,
  payload,
});
export const updateSelectedCategories = payload => ({
  type: UPDATE_CATEGORIES,
  payload,
});

export const showLocationPin = payload => {
  return {
    type: SHOW_LOCATION_PIN,
    payload,
  };
};

export const updateSelectedRelevancy = payload => ({
  type: UPDATE_RELEVANCY,
  payload,
});
export const updateSelectedEventType = payload => ({
  type: UPDATE_EVENT_TYPE,
  payload,
});

export const updateSelectedLocations = payload => ({
  type: UPDATE_LOCATIONS,
  payload,
});

export const updateRefreshDuration = payload => ({
  type: UPDATE_REFRESH_DURATION,
  payload,
});

export const updateOperations = payload => ({
  type: UPDATE_OPERATIONS,
  payload,
});

export const updateTags = payload => ({
  type: UPDATE_TAGS,
  payload,
});

export const updateSelectedLanguage = payload => ({
  type: UPDATE_LANGUAGE,
  payload,
});

export const updateEventSearchSelectedCategories = payload => ({
  type: UPDATE_EVENT_SEARCH_CATEGORIES,
  payload,
});

export const updateEventSearchSelectedRelevancy = payload => ({
  type: UPDATE_EVENT_SEARCH_RELEVANCY,
  payload,
});

export const updateEventSearchSelectedLocations = payload => ({
  type: UPDATE_EVENT_SEARCH_LOCATIONS,
  payload,
});

export const updateEventSearchOperations = payload => ({
  type: UPDATE_EVENT_SEARCH_OPERATIONS,
  payload,
});

export const updateEventSearchTags = payload => ({
  type: UPDATE_EVENT_SEARCH_TAGS,
  payload,
});

export const updateStartDate = payload => ({
  type: UPDATE_START_DATE,
  payload,
});

export const updateEndDate = payload => ({
  type: UPDATE_END_DATE,
  payload,
});

export const updateEventSearchStartDate = payload => ({
  type: UPDATE_EVENT_SEARCH_START_DATE,
  payload,
});

export const updateEventSearchEndDate = payload => ({
  type: UPDATE_EVENT_SEARCH_END_DATE,
  payload,
});

export const updateEventSearchLanguage = payload => ({
  type: UPDATE_EVENT_SEARCH_LANGUAGE,
  payload,
});

export const getRimeEventPins = payload => {
  return {
    type: GET_RIME_EVENT_PINS,
    payload,
  };
};

export const searchFeed = payload => {
  return {
    type: SEARCH_FEED,
    payload,
  };
};

export const updateSearchFeed = payload => {
  return {
    type: UPDATE_SEARCH_FEED,
    payload,
  };
};
export const updateAdminSelectedFeed = payload => {
  return {
    type: UPDATE_ADMIN_SELECTED_FEED,
    payload,
  };
};

export const liveMonitorSearchFeed = payload => {
  return {
    type: LIVE_MONITOR_SEARCH_FEED,
    payload,
  };
};

export const updateLiveMonitorSearchFeed = payload => {
  return {
    type: UPDATE_LIVE_MONITOR_SEARCH_FEED,
    payload,
  };
};

export const updateFeedAction = payload => {
  return {
    type: UPDATE_FEED,
    payload,
  };
};

export const handleSelectAllRiskCat = payload => {
  return {
    type: UPDATE_SELECT_ALL_RISK_CAT,
    payload,
  };
};

export const handleSelectAllRelevancy = payload => {
  return {
    type: UPDATE_SELECT_ALL_RELEVANCY,
    payload,
  };
};

export const handleSelectAllLanguage = payload => {
  return {
    type: UPDATE_SELECT_ALL_LANGUAGE,
    payload,
  };
};

export const handleEventSearchSelectAllRiskCat = payload => {
  return {
    type: UPDATE_EVENT_SEARCH_SELECT_ALL_RISK_CAT,
    payload,
  };
};

export const handleEventSearchSelectAllRelevancy = payload => {
  return {
    type: UPDATE_EVENT_SEARCH_SELECT_ALL_RELEVANCY,
    payload,
  };
};

export const handleEventSearchSelectAllLanguage = payload => {
  return {
    type: UPDATE_EVENT_SEARCH_SELECT_ALL_LANGUAGE,
    payload,
  };
};

export const getRimeSubscriptionData = payload => {
  return {
    type: GET_RIME_SUBSCRIPTION_DATA,
    payload,
  };
};

export const showApiHitTime = payload => {
  return {
    type: SHOW_API_HIT_TIME,
    payload,
  };
};

export const showGeoFenceEventNumbers = payload => {
  return {
    type: SHOW_GEOFENCE_EVENT_NOS,
    payload,
  };
};
