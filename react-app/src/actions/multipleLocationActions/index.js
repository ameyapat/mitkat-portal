export const UPDATE_MULTIPLE_LOCATION = 'UPDATE_MULTIPLE_LOCATION'

export const updateMultipleLocation = (locations) => {
    return {
        type: UPDATE_MULTIPLE_LOCATION,
        payload: locations
    }
}