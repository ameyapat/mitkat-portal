export const UPLOAD_IMAGE = 'UPLOAD_IMAGE';
export const IMAGE_UPLOADED = 'IMAGE_UPLOADED';

export function uploadImage(data){
    return {
        type: UPLOAD_IMAGE,
        payload: data,
    }
}