import * as actionTypes from './actionTypes';
export const SET_PARTNER_DETAILS = 'SET_PARTNER_DETAILS';

export function setAuthToken(authCode) {
  return {
    type: actionTypes.SET_AUTH_TOKEN,
    payload: authCode,
  };
}

export function setIsCovidDashAuth(payload) {
  return {
    type: actionTypes.SET_IS_COVID_DASH_AUTH,
    payload,
  };
}

export function toggleToast(toastObj) {
  return {
    type: actionTypes.TOGGLE_TOAST,
    payload: toastObj,
  };
}

export function setMetrics(metricsObj) {
  return {
    type: actionTypes.SET_METRICS,
    payload: metricsObj,
  };
}

export function toggleIsFetch(isFetching) {
  return {
    type: actionTypes.TOGGLE_IS_FETCHING,
    payload: isFetching,
  };
}

export function setIsLoaded(isLoaded) {
  return {
    type: actionTypes.IS_LOADED,
    payload: isLoaded,
  };
}

export function toggleUser(userType) {
  return {
    type: actionTypes.TOGGLE_USER_TYPE,
    payload: userType,
  };
}

export function setFilters(filters) {
  return {
    type: actionTypes.SET_FILTERS,
    payload: filters,
  };
}

export function setTor(tor) {
  return {
    type: actionTypes.SET_TOR,
    payload: tor,
  };
}

export function setRegions(regions) {
  return {
    type: actionTypes.SET_REGIONS,
    payload: regions,
  };
}

export function setSelectAllRegions(selectAllRegions) {
  return {
    type: actionTypes.SET_SELECT_ALL_REGIONS,
    payload: selectAllRegions,
  };
}

export function setLor(lor) {
  return {
    type: actionTypes.SET_LOR,
    payload: lor,
  };
}

export function setToe(toe) {
  return {
    type: actionTypes.SET_TOE,
    payload: toe,
  };
}

export function setDays(days) {
  return {
    type: actionTypes.SET_DAYS,
    payload: days,
  };
}

export function setEventDetails(eventDetails) {
  return {
    type: actionTypes.SET_EVENT_DETAILS,
    payload: eventDetails,
  };
}

export function setDefaultEventDetails(eventDetails) {
  return {
    type: actionTypes.SET_DEFAULT_EVENT_DETAILS,
    payload: eventDetails,
  };
}

export function setUserRoles(userRole) {
  return {
    type: actionTypes.SET_USER_ROLES,
    payload: userRole,
  };
}

export function applyArchiveFilters(archiveFilters) {
  return {
    type: actionTypes.APPLY_ARCHIVE_FILTERS,
    payload: archiveFilters,
  };
}

export function resetPageNo() {
  return {
    type: actionTypes.RESET_PAGENO,
    payload: 0,
  };
}

export function resetEventFilters() {
  return {
    type: actionTypes.RESET_EVENT_FILTERS,
  };
}

export function handlePageNo(pageNo) {
  return {
    type: actionTypes.HANDLE_PAGE_NUM,
    payload: pageNo,
  };
}

export function setTorFilters(filters) {
  return {
    type: actionTypes.SET_TOR_FILTERS,
    payload: filters,
  };
}

export function setLorFilters(filters) {
  return {
    type: actionTypes.SET_LOR_FILTERS,
    payload: filters,
  };
}

export function refreshEvents(hasRefreshed) {
  return {
    type: actionTypes.REFRESH_EVENTS,
    payload: hasRefreshed,
  };
}

export function setUpdateEditable(isEditable) {
  return {
    type: actionTypes.SET_UPDATE_EDITABLE,
    payload: isEditable,
  };
}

export function updateEventTitle(updateObj) {
  return {
    type: actionTypes.UPDATE_EVENT_TITLE,
    payload: updateObj,
  };
}

export function toggleRimeFilters(showRimeFilter) {
  return {
    type: actionTypes.TOGGLE_RIME_FILTER,
    payload: showRimeFilter,
  };
}

export function toggleRimeEventArchives(showRimeEventArchives) {
  return {
    type: actionTypes.TOGGLE_RIME_EVENT_AECHIVES,
    payload: showRimeEventArchives,
  };
}

export function toggleRiskTrackerBox(riskTrackerObj) {
  return {
    type: actionTypes.TOGGLE_RISK_TRACKER_INFO,
    payload: riskTrackerObj,
  };
}

export function toggleLockScreen(lockScreen) {
  return {
    type: actionTypes.TOGGLE_LOCK_SCREEN,
    payload: lockScreen,
  };
}

export function toggleShowSettings(showSettings) {
  return {
    type: actionTypes.TOGGLE_SHOW_SETTINGS,
    payload: showSettings,
  };
}

export function handleSelectAllTOE(isSelectAll) {
  return {
    type: actionTypes.HANDLE_SELECT_ALL_TOE,
    payload: isSelectAll,
  };
}

export function handleSelectAllTOR(isSelectAll) {
  return {
    type: actionTypes.HANDLE_SELECT_ALL_TOR,
    payload: isSelectAll,
  };
}

export function handleSelectAllLOR(isSelectAll) {
  return {
    type: actionTypes.HANDLE_SELECT_ALL_LOR,
    payload: isSelectAll,
  };
}

export function handleSelectAllArchivesTOR(isSelectAll) {
  return {
    type: actionTypes.HANDLE_SELECT_ALL_ARCHIVES_TOR,
    payload: isSelectAll,
  };
}

export function handleSelectAllArchivesLOR(isSelectAll) {
  return {
    type: actionTypes.HANDLE_SELECT_ALL_ARCHIVES_LOR,
    payload: isSelectAll,
  };
}

export function setTrackerDetails(trackerDetails) {
  return {
    type: actionTypes.SET_TRACKER_DETAILS,
    payload: trackerDetails,
  };
}

export function setAppPermissions(permissions) {
  return {
    type: actionTypes.SET_APP_PERMISSION,
    payload: permissions,
  };
}

export function setPartnerDetails(partnerDetails) {
  return {
    type: SET_PARTNER_DETAILS,
    payload: partnerDetails,
  };
}

export const setHeader = title => {
  return {
    type: actionTypes.SET_HEADER,
    payload: title,
  };
};

export const restrictAccess = access => {
  return {
    type: actionTypes.RESTRICT_ACCESS,
    payload: access,
  };
};

export const updateWatchList = watchlist => {
  return {
    type: actionTypes.UPDATE_WATCH_LIST,
    payload: watchlist,
  };
};
