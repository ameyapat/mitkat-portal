export const SET_QUERY_NEWS_DATA = 'SET_QUERY_NEWS_DATA';

export const setQueryNewsData = value => {
  return {
    type: SET_QUERY_NEWS_DATA,
    payload: value,
  };
};
