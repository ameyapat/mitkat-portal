export const THEME_CHANGE_TOGGLE = 'THEME_CHANGE_TOGGLE';

export const themeChangeToggle = value => {
  return {
    type: THEME_CHANGE_TOGGLE,
    payload: value,
  };
};
