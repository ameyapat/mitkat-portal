export const SET_TOPIC_ANALYSIS_DATA = 'SET_TOPIC_ANALYSIS_DATA';
export const SET_SELECTED_ITEM = 'SET_SELECTED_ITEM';

export const setTopicAnalysisData = payload => {
  return {
    type: SET_TOPIC_ANALYSIS_DATA,
    payload,
  };
};

export const setSelectedItem = selectedItem => {
  return {
    type: SET_SELECTED_ITEM,
    payload: selectedItem,
  };
};
