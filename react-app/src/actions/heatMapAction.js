export const SET_REGION_DATA = 'SET_REGION_DATA';
export const SET_BREADCRUMBS = 'SET_BREADCRUMBS';
export const SET_SELECTED_REGION = 'SET_SELECTED_REGION';
export const RESET_SELECTED_REGION = 'RESET_SELECTED_REGION';
export const SET_HEAT_MAP_OFFICE_LOCATIONS = 'SET_HEAT_MAP_OFFICE_LOCATIONS';

export function setRegionDetails(regionDetails) {
  return {
    type: SET_REGION_DATA,
    payload: regionDetails,
  };
}

export function setBreadcrumbs(breadcrumbValue) {
  return {
    type: SET_BREADCRUMBS,
    payload: breadcrumbValue,
  };
}

export function setSelectedRegion(payload) {
  return {
    type: SET_SELECTED_REGION,
    payload,
  };
}

export function resetSelectedRegion(payload) {
  return {
    type: RESET_SELECTED_REGION,
    payload,
  };
}

export function setHeatMapOfficeLocation(payload) {
  return {
    type: SET_HEAT_MAP_OFFICE_LOCATIONS,
    payload,
  };
}
