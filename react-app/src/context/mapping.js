export const riskCategoryUrls = [
  {
    id: 1,
    label: 'Cyber / Technology',
    defaultUrl:
      'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Cyber+crime.png',
    riskLevels: [
      {
        id: 1,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Cyber-VL.png',
        label: 'very low',
      },
      {
        id: 2,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Cyber-L.png',
        label: 'low',
      },
      {
        id: 3,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Cyber-M.png',
        label: 'medium',
      },
      {
        id: 4,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Cyber-H.png',
        label: 'high',
      },
      {
        id: 5,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Cyber-VH.png',
        label: 'ver high',
      },
    ],
  },
  {
    id: 2,
    label: 'Extremism',
    defaultUrl:
      'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Terrorism.png',
    riskLevels: [
      {
        id: 1,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Terror-VL.png',
        label: 'very low',
      },
      {
        id: 2,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Terror-L.png',
        label: 'low',
      },
      {
        id: 3,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Terror-L.png',
        label: 'medium',
      },
      {
        id: 4,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Terror-H.png',
        label: 'high',
      },
      {
        id: 5,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Terror-VH.png',
        label: 'ver high',
      },
    ],
  },
  {
    id: 3,
    label: 'Civil Disturbance',
    defaultUrl:
      'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Protest.png',
    riskLevels: [
      {
        id: 1,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Civil-VL.png',
        label: 'very low',
      },
      {
        id: 2,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Civil-L.png',
        label: 'low',
      },
      {
        id: 3,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Civil-M.png',
        label: 'medium',
      },
      {
        id: 4,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Civil-H.png',
        label: 'high',
      },
      {
        id: 5,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Civil-VH.png',
        label: 'ver high',
      },
    ],
  },
  {
    id: 4,
    label: 'Environment',
    defaultUrl:
      'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Envronment.png',
    riskLevels: [
      {
        id: 1,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Env-VL.png',
        label: 'very low',
      },
      {
        id: 2,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Env-L.png',
        label: 'low',
      },
      {
        id: 3,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Env-M.png',
        label: 'medium',
      },
      {
        id: 4,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Env-H.png',
        label: 'high',
      },
      {
        id: 5,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Env-VH.png',
        label: 'ver high',
      },
    ],
  },
  {
    id: 5,
    label: 'Health',
    defaultUrl:
      'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Health.png',
    riskLevels: [
      {
        id: 1,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Health-VL.png',
        label: 'very low',
      },
      {
        id: 2,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Health-L.png',
        label: 'low',
      },
      {
        id: 3,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Health-M.png',
        label: 'medium',
      },
      {
        id: 4,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Health-H.png',
        label: 'high',
      },
      {
        id: 5,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Health-VH.png',
        label: 'ver high',
      },
    ],
  },
  {
    id: 6,
    label: 'Critical Infrastructure',
    defaultUrl:
      'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Infrastructure.png',
    riskLevels: [
      {
        id: 1,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Infra-VL.png',
        label: 'very low',
      },
      {
        id: 2,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Infra-L.png',
        label: 'low',
      },
      {
        id: 3,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Infra-M.png',
        label: 'medium',
      },
      {
        id: 4,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Infra-H.png',
        label: 'high',
      },
      {
        id: 5,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Infra-VH.png',
        label: 'ver high',
      },
    ],
  },
  {
    id: 7,
    label: 'Crime',
    defaultUrl:
      'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Crime.png',
    riskLevels: [
      {
        id: 1,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Crime-VL.png',
        label: 'very low',
      },
      {
        id: 2,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Crime-L.png',
        label: 'low',
      },
      {
        id: 3,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Crime-M.png',
        label: 'medium',
      },
      {
        id: 4,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Crime-H.png',
        label: 'high',
      },
      {
        id: 5,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Crime-VH.png',
        label: 'ver high',
      },
    ],
  },
  {
    id: 8,
    label: 'Travel Risks',
    defaultUrl:
      'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Travel+Risks+(1).png',
    riskLevels: [
      {
        id: 1,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Travel+Risks+(1).png',
        label: 'very low',
      },
      {
        id: 2,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Travel+Risks+(1).png',
        label: 'low',
      },
      {
        id: 3,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Travel+Risks+(1).png',
        label: 'medium',
      },
      {
        id: 4,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Travel+Risks+(1).png',
        label: 'high',
      },
      {
        id: 5,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Travel+Risks+(1).png',
        label: 'ver high',
      },
    ],
  },
  {
    id: 9,
    label: 'Natural Disasters',
    defaultUrl:
      'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Natural+Distasters+(1).png',
    riskLevels: [
      {
        id: 1,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Natural+Distasters+(1).png',
        label: 'very low',
      },
      {
        id: 2,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Natural+Distasters+(1).png',
        label: 'low',
      },
      {
        id: 3,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Natural+Distasters+(1).png',
        label: 'medium',
      },
      {
        id: 4,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Natural+Distasters+(1).png',
        label: 'high',
      },
      {
        id: 5,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Natural+Distasters+(1).png',
        label: 'very high',
      },
    ],
  },
  {
    id: 10,
    label: 'External Threats',
    defaultUrl:
      'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/External+Threats+(1).png',
    riskLevels: [
      {
        id: 1,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/External+Threats+(1).png',
        label: 'very low',
      },
      {
        id: 2,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/External+Threats+(1).png',
        label: 'low',
      },
      {
        id: 3,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/External+Threats+(1).png',
        label: 'medium',
      },
      {
        id: 4,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/External+Threats+(1).png',
        label: 'high',
      },
      {
        id: 5,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/External+Threats+(1).png',
        label: 'ver high',
      },
    ],
  },
  {
    id: 11,
    label: 'Political',
    defaultUrl:
      'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Political+(1).png',
    riskLevels: [
      {
        id: 1,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Political+(1).png',
        label: 'very low',
      },
      {
        id: 2,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Political+(1).png',
        label: 'low',
      },
      {
        id: 3,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Political+(1).png',
        label: 'medium',
      },
      {
        id: 4,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Political+(1).png',
        label: 'high',
      },
      {
        id: 5,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Political+(1).png',
        label: 'ver high',
      },
    ],
  },
  {
    id: 12,
    label: 'Regulatory',
    defaultUrl:
      'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Regulatory+(1).png',
    riskLevels: [
      {
        id: 1,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Regulatory+(1).png',
        label: 'very low',
      },
      {
        id: 2,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Regulatory+(1).png',
        label: 'low',
      },
      {
        id: 3,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Regulatory+(1).png',
        label: 'medium',
      },
      {
        id: 4,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Regulatory+(1).png',
        label: 'high',
      },
      {
        id: 5,
        url:
          'https://mitkatrisktracker.s3.ap-south-1.amazonaws.com/RiskCategoryIcons/Regulatory+(1).png',
        label: 'ver high',
      },
    ],
  },
];
