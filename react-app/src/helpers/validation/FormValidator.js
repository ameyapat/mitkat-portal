import validator from 'validator';

export default class FormValidator{
    constructor(validationRules){
        this.validationRules = validationRules;
    }

    validate(state){
                     let validationObj = this.valid();

                     this.validationRules.map(rule => {
                       if (!validationObj[rule.field].isInvalid) {
                         const fieldVal = state[rule.field].toString();
                         const args = rule.args || [];
                         const validationMethod =
                           typeof rule.method === 'string'
                             ? validator[rule.method]
                             : rule.method;

                         if (
                           validationMethod(fieldVal, ...args, state) !==
                           rule.validWhen
                         ) {
                           validationObj[rule.field] = {
                             isInvalid: true,
                             message: rule.message,
                           };
                           validationObj.isValid = false;
                         }
                       }
                     });

                     return validationObj;
                   }

    valid(){
        const validationObj = {};

        this.validationRules.map(rule => {
            validationObj[rule.field] = {isInvalid: false, message: ''}
        })

        return { isValid: true, ...validationObj }
    }
}