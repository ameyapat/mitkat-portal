const rules = [
  {
    field: 'email',
    method: 'isEmpty',
    validWhen: false,
    message: 'Email is required',
  },
  {
    field: 'username',
    method: 'isEmpty',
    validWhen: false,
    message: 'Username is required',
  },
];

export default rules;
