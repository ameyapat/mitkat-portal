const IS_UAT = process.env.IS_UAT;
const uatUrl =
  'http://mitkatportal-env-test.p47upbw4ur.ap-south-1.elasticbeanstalk.com';
const prodUrl = 'https://mitkatrisktracker.com';
const uatRime =
  'http://mitkatportal-env-test.p47upbw4ur.ap-south-1.elasticbeanstalk.com/api/rime/feed';
const prodRime = 'https://mitkatrisktracker.com/api/rime/feed';
let baseUrl = IS_UAT ? uatUrl : prodUrl;
const rimeServer = 'https://mitkatadvisory.net';

export const rimeUrl = IS_UAT ? uatRime : prodRime;

// Mitkat Website URL
const mitkatWebUrl = 'https://mitkatadvisory.com';

//globalnewsapi
const globalnewsapi = 'https://api.globalnewsapi.com';
const topEvents = rimeServer + '/top10Events';

let apiEndPoints = {
  getUserRole: `api/auth/getuserroles`, //get userrole
  getUserId: `api/auth/checkuserid`,
  signUpAdmin: 'api/admin/signup/admin',
  signInAdmin: 'api/auth/signin',
  adminCreateUser: 'api/admin/signup/user',
  adminCreateClient: 'api/admin/signup/client',
  adminListAllUsers: 'api/admin/allusers',
  adminListAllClients: 'api/admin/allclients',
  adminUpdateUser: 'api/admin/updateUser',
  adminUpdateClientDetails: 'api/admin/clientdetails/add',
  updateClientDetails: 'api/admin/clientdetails/update',
  adminGetClientDetails: 'api/admin/clientdetails',
  showPinsOnMap: 'api/client/getterminalresponse', //to show all pins on map
  getEventDetails: 'api/client/getevent', //to get event details
  getNationalEvents: 'api/client/nationalevents', //to get national events
  getRiskAlerts: 'api/client/riskalert', //to get current risk alert
  showRiskAlertDetails: 'api/client/riskalertdetails', //to get alert details
  getNotificationAlerts: 'api/client/notification',
  getSmsAlerts: 'api/client/riskalert', //get sms alerts
  getEventArchives: 'api/client/archiveevents', //get event archives
  downloadEventPdf: `api/client/pdfreport`,
  clientMetricsBar: `api/client/topbar`,
  fetchRiskLevel: `api/alliedservices/risklevel`,
  fetchRiskCategory: `api/alliedservices/riskcategory`,
  fetchSubRiskCategory: 'api/alliedservices/getrisksubcategories',
  fetchCountries: `api/alliedservices/country`,
  fetchStates: `api/alliedservices/state`,
  fetchMetros: `api/alliedservices/city`,
  fetchImpactIndustry: `api/alliedservices/impactindustrytype`,
  fetchTypeOfProducts: `api/alliedservices/producttype`,
  fetchTypeOfStory: `api/alliedservices/regiontype`,
  addEventCreator: `api/creator/addevent`,
  editEventCreator: `api/creator/editevent`,
  getFilteredFeed: `api/rime/screen/getevents`,
  getLiveEvents: `api/creator/liveevent`, //get live events
  getLiveEventsUpdatesList: `api/creator/getliveeventupdates`,
  addEventUpdate: `api/creator/addupdate`,
  editEventUpdate: `api/creator/editupdate`,
  deleteEventUpdate: `api/creator/deleteupdate`,
  getEventHistory: `api/creator/geteventlist`,
  filterEventHistory: `api/creator/geteventlist/filter`,
  getCreatorList: `api/alliedservices/creatorlist`,
  getTypeOfProducts: `api/alliedservices/producttype`,
  viewEventHistory: `api/creator/getevent`,
  getTodaysEvent: `api/approver/gettodaysevents`, //approver api starts
  editTodaysEvent: `api/approver/getevent`,
  updateTodaysEvent: `api/approver/editevent`,
  deleteTodaysEvent: `api/approver/deletevent`,
  getApproveStatus: `api/alliedservices/status`,
  getRiskProducts: `api/alliedservices/risktrackertypes`,
  getEventsForProduct: `api/approver/geteventsforproduct`,
  dispatchMail: `api/approver/sendemail`,
  addEmailAlert: `api/creator/addemailalert`,
  addScreeningEmailAlert: `api/creator/addscreeningemailalert`,
  addEvent: `api/rime/screen/createevent`,
  getEmailAlert: `api/approver/gettodaysemailalerts`,
  getEventEmailAlert: `api/approver/geteventsforemailalerts`,
  dispatchEmailAlert: `api/approver/sendemailalerts`,
  getClientList: `api/alliedservices/clientlist`,
  createSmsAlert: `api/creator/addriskalert`,
  getSmsAlertsApproval: `api/approver/getsmsalertsforapproval`,
  editSmsAlertApproval: `api/approver/getsmsalert`,
  updateSmsAlertApproval: `api/approver/editsmsalert`,
  getSmsDispatchList: `api/approver/smsalertsdispatchlist`,
  dispatchSms: `api/approver/sendsms`,
  getCalendarEvents: `api/client/geteventcalender`,
  getClientLocations: `api/client/getterminallocations`,
  addNewClientLocation: `api/admin/clientdetails/addlocation`,
  viewClientLocation: `api/admin/clientdetails/getlocations`,
  deleteClientLocation: `api/admin/clientdetails/deletelocation`,
  getIndustrySpecificView: `api/client/getindustryspecificview`,
  addSelectedEvents: `api/rime/screen/addevents`,
  pastEvents:`api/rime/screen/pastevents`,
  getClientStates: `api/client/getclientstates`,
  getIndustrySpecificEvents: `api/client/getindustryspecificevents`,
  getIndustryStateList: `api/client/getstatelist`,
  getIndustryGraph: `api/client/getindustrygraph`,
  getDefaultQueryList: `api/admin/clientdetails/getspocs`,
  getUsersInAdmin: `api/admin/getallinternalusers`,
  handleUserQuery: `api/admin/clientdetails/addspoc`,
  validateUserSubscription: `api/client/checkquerysubscription`,
  getQueryList: `api/client/getquerylist`,
  addNewQuery: `api/client/addquery`,
  getQueryDetails: `api/client/getquerydetails`,
  getQueryListCreator: `api/creator/getquerylist`,
  getQueryDetailsCreator: `api/creator/getquerydetails`,
  addQueryResponseCreator: `api/creator/addqueryresponse`,
  addNewAdvisory: `api/creator/uploadadvisory`, // advisory api routes start
  editAdvisory: 'api/creator/advisorydetails',
  updateAdvisory: 'api/creator/editadvisory',
  getAdvisoryListCreator: `api/creator/getalladvisories`,
  getAdvisoryList: `api/client/getalladvisories`,
  getTravelAdvisories: `api/creator/traveladvisorylist`, // for listing travel advisories
  uploadTravelAdvisory: `api/creator/uploadtraveladvisory`, //upload travel advisory
  uploadQueryFile: `api/creator/uploadqueryfile`,
  viewAdvisory: `api/creator/viewadvisory`,
  deleteAdvisory: `api/creator/deleteadvisory`,
  downloadAdvisory: `api/creator/downloadadvisory`, // advisory api routes end
  addNewReport: `api/creator/uploadweeklyreport`, // reports create
  editNewReport: `api/creator/getweeklyreport`, // reports edit
  updateNewReport: `api/creator/editweeklyreport`, // reports update
  getReportsCreator: `api/creator/getallweeklyreports`,
  getReports: `api/client/getallweeklyreports`,
  viewReport: `api/creator/viewweeklyreport`,
  deleteReport: `api/creator/deleteweeklyreport`,
  downloadReport: `api/creator/downloadreport`, // reports api routes end
  addSpecialReport: `api/creator/uploadspecialreports`, // special-reports api routes start
  editSpecialReport: `api/creator/getspecialreport`, // special-reports edit
  updateSpecialReport: `api/creator/editspecialreports`, // special-reports update
  getSpecialReportsCreator: `api/creator/getallspecialreports`,
  getSpecialReports: `api/client/getallspecialreports`,
  viewSpecialReport: `api/creator/viewspecialreport`,
  deleteSpecialReport: `api/creator/deletespecialreport`,
  downloadSpecialReport: `api/creator/downloadspecialreports`, // special-reports api routes end
  addRiskReview: `api/creator/uploadriskreviews`, // risk-review create
  editRiskReview: `api/creator/getriskreview`, // risk-review edit
  updateRiskReview: `api/creator/editriskreview`, // risk-review update
  getRiskReviewsCreator: `api/creator/getallriskreviews`,
  getRiskReviews: `api/client/getallriskreviews`,
  viewRiskReview: `api/creator/viewriskreviews`,
  deleteRiskReview: `api/creator/deleteriskreview`,
  downloadRiskReview: `api/creator/downloadriskreviews`, // risk-review api routes end
  riskTrackerNew: `api/creator/riskTrackerNew`, //get new daily risk
  getEventsForPeriod: `api/creator/getEventsForPeriod`,
  downloadEventsInExcel: `api/creator/downloadEventsInExcel`, //download excel sheet
  sendSelfEmail: `api/creator/selfEmailEvents`,
  getAnalytics: `api/client/analytics/getAnalytics`,
  trackLiveEvents: `api/client/getLiveUpdates`,
  impactRadius: `api/alliedservices/defaultImpactRadius`, //{riskcategoryid}/(risklevelid}
  checkTravelAdSub: `api/client/checktraveladvisorysubscription`,
  filterGodrejEvents: `/api/godrej/filterevents`,
  getMonthsGodrej: `api/godrej/client/months`,
  getCountriesGodrej: `api/godrej/client/countries`,
  getBusinessGroupsGodrej: `api/godrej/client/businessgroups`,
  viewGodrejEvent: `api/godrej/viewgodrejevent`,
  getMonthsGodrejAdmin: `api/godrej/months`,
  riskLevelGodrejAdmin: `api/godrej/risklevels`,
  pestelCatGodrejAdmin: `api/godrej/pestelcategory`,
  countriesGodrejAdmin: `api/godrej/countries`,
  bizGrpsGodrejAdmin: `api/godrej/businessgroups`,
  addGodrejEventAdmin: `api/godrej/addgodrejevent`,
  godrejEventListAdmin: `api/godrej/geteventlist`,
  viewGodrejEventAdmin: `api/godrej/viewgodrejevent`,
  getRiskLevelGodrej: `api/godrej/client/risklevels`,
  downloadGodrejEvents: `api/godrej/downloadriskwatch`,
  uploadImageGodrej: `api/godrej/uploadImage`,
  deleteImageGodrej: `api/godrej/deleteImage`,
  getEmergingRisks: `api/godrej/previousEmergingRisks`,
  rimeFeedUrl: `api/rime/filteredfeed`,
  addCoronaReading: `api/corona/addcoronareading`,
  getCoronaReading: `api/corona/getAllReadings`,
  getSingleCoronaReading: `api/corona/getreading`,
  coronaDashboard: `api/corona/coronadashboardnew`,
  stateWiseDashboard: `api/corona/statewisedashboardnew`,
  cityWiseDashboard: `api/corona/citywisedashboardnew`,
  getCoronaAdvisory: `api/corona/coronaadvisory`,
  getStateDetails: `api/corona/getstatedetails`,
  postStateDetails: `api/corona/poststatedetails`,
  getCoronaMapDetails: `api/corona/coronamapnew`,
  downloadDistrictReport: `api/corona/pdfdistrictreport`,
  downloadDistrictReportExcel: `api/corona/exceldistrictreport`,
  downloadSummary: `api/corona/pdfsummaryreport`,
  getDistricts: `api/corona/getdistricts`,
  saveDistricts: `api/corona/savedistricts`,
  geojson: `api/corona/geojson`,
  addCoronaCity: `api/corona/addcoronareading/city`,
  getAllCoronaCities: `api/corona/getAllReadings/city`,
  getReadingCovidCity: `api/corona/getreading/city`,
  downloadCovidCity: `api/corona/pdfmetroreport`,
  getCityDetails: `api/corona/getcitydetails`,
  postCityDetails: `api/corona/postcitydetails`,
  getEmpDetails: `api/corona/employeedetails`,
  downloadEmpExcel: `api/corona/excelemployeereport`,
  excelLocationReport: `api/corona/excellocationreport`,
  downloadRimeEmpLocations: `api/rime/rimedownload`,
  getEventListPortal: `api/client/geteventlist`,
  getLiveUpdatePortal: `api/client/getliveupdatelist`,
  getTotalEvents: `api/client/totalevents`,
  downloadEventArchives: `api/client/downloadarchiveevents`,
  resetPassword: 'api/client/updatepassword',
  trackDispatch: 'api/approver/getdispatchstatus',
  getClientType: 'api/alliedservices/clienttype',
  getQualityList: 'api/admin/performance/qualtylist',
  getAllTopics: 'api/approver/getalltopics',
  addEditTopic: 'api/approver/addoredittopic',
  getLiveTopics: 'api/approver/getlivetopics',
  getIndiaWeekly: 'api/approver/getindiaweekly',
  getSouthAsiaWeekly: 'api/approver/getsaweekly',
  getApacWeekly: 'api/approver/getapacweekly',
  updateWeekly: 'api/approver/updateweeklyevent',
  emeaWeekly: 'api/approver/getemeaweekly',
  americasWeekly: 'api/approver/getamericasweekly',
  dispatchIndiaWeekly: 'api/approver/dispatchindiaweekly',
  dispatchSouthAsiaWeekly: 'api/approver/dispatchsaweekly',
  dispatchApacWeekly: 'api/approver/dispatchapacweekly',
  dispatchEmeaWeekly: 'api/approver/dispatchemeaweekly',
  dispatchAmericasWeekly: 'api/approver/dispatchamericasweekly',
  getCountryList: 'api/corona/countrylist',
  editCountryDetails: 'api/corona/countrydetails',
  editCountryList: 'api/corona/editcountry',
  getCountryReadingList: 'api/corona/countryreadinglist',
  editCountryReading: 'api/corona/getcountryreading',
  updateCoronaCountry: 'api/corona/addoreditcoronacountryreading',
  getCoronaCountryDashboard: 'api/corona/getcoronacountrydashboard',
  downloadCountryReport: 'api/corona/pdfcountryreport',
  getEventCountryList: 'api/client/country',
  getCountryEventDetails: 'api/client/geteventcalendernew',
  getWeekly: 'api/client/getweekly',
  getWeeklyEvents: 'api/client/getweeklyevent',
  //barclays starts
  getCountryListBarclays: 'api/barclays/country',
  getCityListBarclays: 'api/barclays/city',
  getAssetListBarclays: 'api/barclays/asset',
  getRiskLevelBarclays: 'api/barclays/risklevel',
  getRiskCategoryBarclays: 'api/barclays/riskcategory',
  getSubRiskCategoryBarclays: 'api/barclays/risksubcategory',
  getEventListBarclays: 'api/barclays/eventlistfordate',
  getEventBarclays: 'api/barclays/getevent',
  deleteEventBarclays: 'api/barclays/deleteevent',
  updateEventBarclays: 'api/barclays/createeditevent',
  getDashboardDataBarclays: 'api/barclays/getdashboard',
  downloadEventsBarclays: 'api/barclays/downloadevents',
  getDashboardEventDetailsBarclays: 'api/barclays/client/getevent',
  downloadPdfBarclays: 'api/barclays/pdfreport',
  uploadEventImg: 'api/barclays/addimage',
  uploadReportingImg: 'api/creator/uploadreportimage',
  //barclays ends
  quadrantEventList: 'api/approver/quadranteventlist',
  quadrantEventDetails: 'api/approver/getquadrantevent',
  updateQuadrantDetails: 'api/approver/createeditquadrantevent',
  deleteQuadrantDetails: 'api/approver/deletequadrantevent',
  getQuadrants: 'api/client/getfourquadrants',
  getQuadrantsV1: `api/client/getfourquadrantsnew`,
  getRegions: `api/alliedservices/regions`,
  getRiskExposureData: `api/client/analytics/exposureview`,
  getCoverageRating: `api/approver/coveragerating`,
  getBlogs: `api/client/getallblogs`,
  getPodcast: `api/client/getpodcastlist`,
  getSimilarityAnalysis: 'api/approver/similarityanalysis',
  getAffectedEntities: 'api/client/getaffectedEntities',
  downloadAffectedEmployees: 'api/client/downloademployeesexcel',
  downloadAffectedAssets: 'api/client/downloadlocationsexcel',
  getAllPartners: 'api/admin/allpartners',
  createPartner: 'api/admin/signup/partner',
  checkSubscriptions: 'api/client/checksubscriptions',
  updateSettings: 'api/admin/clientdetails/changesetting',
  rimeRegions: 'api/admin/clientdetails/rimeregionlist',
  getPartnerEvents: 'api/approver/partnerevents', // GET,
  getPartnerEventById: 'api/approver/partner/getevent', // GET
  deletePartnerEventById: 'api/approver/partner/delete', // GET
  updatePartnerEvent: 'api/approver/partner/editevent', // POST
  travelCountryList: 'api/client/travelcountrylist',
  eventCreatorTrackingId: 'api/creator/eventsbytrackingid',
  eventClientTrackingId: 'api/client/eventsbytrackingid',
  downloadArchives: 'api/client/downloadarchiveeventsnew',
  getPartnerDetails: 'api/admin/getpartnerdetails',
  addQuery: 'api/approver/addquery',
  downloadQuery: 'api/approver/downloadqueries',
  getRelatedEvents: 'api/client/getrelatedevents',
  getRelevantClientList: 'api/alliedservices/getreleventclientlist',
  topicAnalysis: 'api/client/analytics/gettopicanalysis',
  getLocationAnalysis: 'api/client/analytics/getregionrating',
  getClientLocationAnalysis: 'api/client/analytics/getclientlocationrating',
  getClientLocationsAnalysis: 'api/client/analytics/getclientlocations',
  getRimeSavedFilters: 'api/rime/filter',
  getRimeSubscriptions: 'api/rime/allsubscriptions',
  setRimeSubscriptions: 'api/rime/subscription',
  //blog
  getAllBlogs: 'api/creator/getallblogs',
  createBlog: 'api/creator/uploadblog',
  viewBlog: 'api/creator/viewblog',
  deleteBlog: 'api/creator/deleteblog',
  editBlog: 'api/creator/getblog',
  updateBlog: 'api/creator/editblog',
  downloadBlog: 'api/creator/downloadblog',
  //podcast
  getAllPodcast: 'api/creator/getpodcastlist',
  createUpdatePodcast: 'api/creator/createpodcast',
  deletePodcast: 'api/creator/deletepodcast',
  editPodcast: 'api/creator/getpodcast',
  //podcast end
  editUserRole: 'api/admin/edituserrole',
  getViewList: 'api/admin/clientdetails/getviewlist',
  rimeTimeRange: 'eventTimeRange',
  rimeDateRange: 'eventDateRange',
  addWatchList: 'api/client/addwatchlist',
  removeWatchList: 'api/client/deletewatchlist',
  getWatchList: 'api/client/getwatchlist',
  getCountryAnalysis: 'api/rime/screen/getcountryanalysis',
  getCityAnalysis: 'api/rime/screen/getcityanalysis',
  getMissedLinks: 'api/rime/screen/getmissedlinks',
  getCuratorAnalysis: 'api/rime/screen/getuseranalysis',
  deleteScreenEvent: 'api/rime/screen/deleteevent',
  editFilteredCardEvent: 'api/rime/screen/editevent',
  downloadCountryAnalysis: 'api/rime/screen/countryanalysisexcel',
  getMonitoringFeed: 'api/rime/screen/getmonitoringscreen',
  clientUserList: 'api/admin/getclientusers',
  addClientUser: 'api/admin/signup/clientuser',
  deleteClientUser: 'api/admin/deleteclientuser',
  clientLocationList: 'api/admin/clientdetails/getlocations',
  addClientLocation: 'api/admin/clientdetails/client/addlocation',
};

export const API_ROUTES = {
  getUserRole: `${baseUrl}/${apiEndPoints.getUserRole}`,
  getUserId: `${baseUrl}/${apiEndPoints.getUserId}`,
  signUpAdmin: `${baseUrl}/${apiEndPoints.signUpAdmin}`,
  signInAdmin: `${baseUrl}/${apiEndPoints.signInAdmin}`,
  adminCreateUser: `${baseUrl}/${apiEndPoints.adminCreateUser}`,
  adminCreateClient: `${baseUrl}/${apiEndPoints.adminCreateClient}`,
  adminListAllUsers: `${baseUrl}/${apiEndPoints.adminListAllUsers}`,
  adminListAllClients: `${baseUrl}/${apiEndPoints.adminListAllClients}`,
  adminUpdateUser: `${baseUrl}/${apiEndPoints.adminUpdateUser}`,
  adminUpdateClientDetails: `${baseUrl}/${apiEndPoints.adminUpdateClientDetails}`,
  updateClientDetails: `${baseUrl}/${apiEndPoints.updateClientDetails}`,
  adminGetClientDetails: `${baseUrl}/${apiEndPoints.adminGetClientDetails}`,
  showPinsOnMap: `${baseUrl}/${apiEndPoints.showPinsOnMap}`,
  getEventDetails: `${baseUrl}/${apiEndPoints.getEventDetails}`,
  getNationalEvents: `${baseUrl}/${apiEndPoints.getNationalEvents}`,
  getRiskAlerts: `${baseUrl}/${apiEndPoints.getRiskAlerts}`,
  showRiskAlertDetails: `${baseUrl}/${apiEndPoints.showRiskAlertDetails}`,
  getNotificationAlerts: `${baseUrl}/${apiEndPoints.getNotificationAlerts}`,
  getSmsAlerts: `${baseUrl}/${apiEndPoints.getSmsAlerts}`,
  getEventArchives: `${baseUrl}/${apiEndPoints.getEventArchives}`,
  downloadEventPdf: `${baseUrl}/${apiEndPoints.downloadEventPdf}`,
  clientMetricsBar: `${baseUrl}/${apiEndPoints.clientMetricsBar}`,
  fetchRiskLevel: `${baseUrl}/${apiEndPoints.fetchRiskLevel}`,
  fetchRiskCategory: `${baseUrl}/${apiEndPoints.fetchRiskCategory}`,
  fetchSubRiskCategory: `${baseUrl}/${apiEndPoints.fetchSubRiskCategory}`,
  fetchCountries: `${baseUrl}/${apiEndPoints.fetchCountries}`,
  fetchStates: `${baseUrl}/${apiEndPoints.fetchStates}`, //fetch states
  fetchMetros: `${baseUrl}/${apiEndPoints.fetchMetros}`,
  fetchImpactIndustry: `${baseUrl}/${apiEndPoints.fetchImpactIndustry}`,
  fetchTypeOfProducts: `${baseUrl}/${apiEndPoints.fetchTypeOfProducts}`,
  fetchTypeOfStory: `${baseUrl}/${apiEndPoints.fetchTypeOfStory}`,
  addEventCreator: `${baseUrl}/${apiEndPoints.addEventCreator}`,
  editEventCreator: `${baseUrl}/${apiEndPoints.editEventCreator}`,
  getFilteredFeed: `${baseUrl}/${apiEndPoints.getFilteredFeed}`,
  getLiveEvents: `${baseUrl}/${apiEndPoints.getLiveEvents}`, //live events urls
  getLiveEventsUpdatesList: `${baseUrl}/${apiEndPoints.getLiveEventsUpdatesList}`,
  addEventUpdate: `${baseUrl}/${apiEndPoints.addEventUpdate}`,
  editEventUpdate: `${baseUrl}/${apiEndPoints.editEventUpdate}`,
  deleteEventUpdate: `${baseUrl}/${apiEndPoints.deleteEventUpdate}`,
  getEventHistory: `${baseUrl}/${apiEndPoints.getEventHistory}`,
  filterEventHistory: `${baseUrl}/${apiEndPoints.filterEventHistory}`,
  getCreatorList: `${baseUrl}/${apiEndPoints.getCreatorList}`,
  editEventHistory: `${baseUrl}/${apiEndPoints.editEventHistory}`,
  viewEventHistory: `${baseUrl}/${apiEndPoints.viewEventHistory}`,
  getTodaysEvent: `${baseUrl}/${apiEndPoints.getTodaysEvent}`, //approver api starts
  getCountryAnalysis: `${baseUrl}/${apiEndPoints.getCountryAnalysis}`,
  getCityAnalysis: `${baseUrl}/${apiEndPoints.getCityAnalysis}`,
  getMissedLinks: `${baseUrl}/${apiEndPoints.getMissedLinks}`,
  getCuratorAnalysis: `${baseUrl}/${apiEndPoints.getCuratorAnalysis}`,
  editTodaysEvent: `${baseUrl}/${apiEndPoints.editTodaysEvent}`,
  updateTodaysEvent: `${baseUrl}/${apiEndPoints.updateTodaysEvent}`, //save/update event
  deleteTodaysEvent: `${baseUrl}/${apiEndPoints.deleteTodaysEvent}`,
  getApproveStatus: `${baseUrl}/${apiEndPoints.getApproveStatus}`,
  getRiskProducts: `${baseUrl}/${apiEndPoints.getRiskProducts}`,
  getEventsForProduct: `${baseUrl}/${apiEndPoints.getEventsForProduct}`,
  dispatchMail: `${baseUrl}/${apiEndPoints.dispatchMail}`,
  addEmailAlert: `${baseUrl}/${apiEndPoints.addEmailAlert}`,
  addscreeningEmailAlert: `${baseUrl}/${apiEndPoints.addScreeningEmailAlert}`,
  addEvent: `${baseUrl}/${apiEndPoints.addEvent}`,
  getEmailAlert: `${baseUrl}/${apiEndPoints.getEmailAlert}`,
  getEventEmailAlert: `${baseUrl}/${apiEndPoints.getEventEmailAlert}`,
  dispatchEmailAlert: `${baseUrl}/${apiEndPoints.dispatchEmailAlert}`,
  getClientList: `${baseUrl}/${apiEndPoints.getClientList}`,
  createSmsAlert: `${baseUrl}/${apiEndPoints.createSmsAlert}`, //create sms alerts
  getSmsAlertsApproval: `${baseUrl}/${apiEndPoints.getSmsAlertsApproval}`, //get sms alerts
  editSmsAlertApproval: `${baseUrl}/${apiEndPoints.editSmsAlertApproval}`, // open for editing / sms alerts
  updateSmsAlertApproval: `${baseUrl}/${apiEndPoints.updateSmsAlertApproval}`, //update sms alerts
  getSmsDispatchList: `${baseUrl}/${apiEndPoints.getSmsDispatchList}`, //get sms dispatch list
  dispatchSms: `${baseUrl}/${apiEndPoints.dispatchSms}`,
  getCalendarEvents: `${baseUrl}/${apiEndPoints.getCalendarEvents}`,
  getClientLocations: `${baseUrl}/${apiEndPoints.getClientLocations}`,
  addNewClientLocation: `${baseUrl}/${apiEndPoints.addNewClientLocation}`,
  viewClientLocation: `${baseUrl}/${apiEndPoints.viewClientLocation}`,
  deleteClientLocation: `${baseUrl}/${apiEndPoints.deleteClientLocation}`,
  getIndustrySpecificView: `${baseUrl}/${apiEndPoints.getIndustrySpecificView}`,
  getClientStates: `${baseUrl}/${apiEndPoints.getClientStates}`,
  getIndustrySpecificEvents: `${baseUrl}/${apiEndPoints.getIndustrySpecificEvents}`,
  getIndustryStateList: `${baseUrl}/${apiEndPoints.getIndustryStateList}`,
  getIndustryGraph: `${baseUrl}/${apiEndPoints.getIndustryGraph}`,
  getDefaultQueryList: `${baseUrl}/${apiEndPoints.getDefaultQueryList}`,
  getUsersInAdmin: `${baseUrl}/${apiEndPoints.getUsersInAdmin}`,
  handleUserQuery: `${baseUrl}/${apiEndPoints.handleUserQuery}`,
  validateUserSubscription: `${baseUrl}/${apiEndPoints.validateUserSubscription}`,
  getQueryList: `${baseUrl}/${apiEndPoints.getQueryList}`,
  addNewQuery: `${baseUrl}/${apiEndPoints.addNewQuery}`,
  getQueryDetails: `${baseUrl}/${apiEndPoints.getQueryDetails}`,
  getQueryListCreator: `${baseUrl}/${apiEndPoints.getQueryListCreator}`,
  getQueryDetailsCreator: `${baseUrl}/${apiEndPoints.getQueryDetailsCreator}`,
  addQueryResponseCreator: `${baseUrl}/${apiEndPoints.addQueryResponseCreator}`,
  addNewAdvisory: `${baseUrl}/${apiEndPoints.addNewAdvisory}`,
  editAdvisory: `${baseUrl}/${apiEndPoints.editAdvisory}`,
  updateAdvisory: `${baseUrl}/${apiEndPoints.updateAdvisory}`,
  getAdvisoryListCreator: `${baseUrl}/${apiEndPoints.getAdvisoryListCreator}`,
  getAdvisoryList: `${baseUrl}/${apiEndPoints.getAdvisoryList}`,
  getTravelAdvisories: `${baseUrl}/${apiEndPoints.getTravelAdvisories}`,
  uploadTravelAdvisory: `${baseUrl}/${apiEndPoints.uploadTravelAdvisory}`,
  uploadQueryFile: `${baseUrl}/${apiEndPoints.uploadQueryFile}`,
  viewAdvisory: `${baseUrl}/${apiEndPoints.viewAdvisory}`,
  deleteAdvisory: `${baseUrl}/${apiEndPoints.deleteAdvisory}`,
  downloadAdvisory: `${baseUrl}/${apiEndPoints.downloadAdvisory}`,
  addNewReport: `${baseUrl}/${apiEndPoints.addNewReport}`,
  editNewReport: `${baseUrl}/${apiEndPoints.editNewReport}`,
  updateNewReport: `${baseUrl}/${apiEndPoints.updateNewReport}`,
  getReportsCreator: `${baseUrl}/${apiEndPoints.getReportsCreator}`,
  getReports: `${baseUrl}/${apiEndPoints.getReports}`,
  viewReport: `${baseUrl}/${apiEndPoints.viewReport}`,
  deleteReport: `${baseUrl}/${apiEndPoints.deleteReport}`,
  downloadReport: `${baseUrl}/${apiEndPoints.downloadReport}`,
  addSpecialReport: `${baseUrl}/${apiEndPoints.addSpecialReport}`,
  editSpecialReport: `${baseUrl}/${apiEndPoints.editSpecialReport}`,
  updateSpecialReport: `${baseUrl}/${apiEndPoints.updateSpecialReport}`,
  getSpecialReportsCreator: `${baseUrl}/${apiEndPoints.getSpecialReportsCreator}`,
  getSpecialReports: `${baseUrl}/${apiEndPoints.getSpecialReports}`,
  viewSpecialReport: `${baseUrl}/${apiEndPoints.viewSpecialReport}`,
  deleteSpecialReport: `${baseUrl}/${apiEndPoints.deleteSpecialReport}`,
  downloadSpecialReport: `${baseUrl}/${apiEndPoints.downloadSpecialReport}`,
  addRiskReview: `${baseUrl}/${apiEndPoints.addRiskReview}`,
  editRiskReview: `${baseUrl}/${apiEndPoints.editRiskReview}`,
  updateRiskReview: `${baseUrl}/${apiEndPoints.updateRiskReview}`,
  getRiskReviewsCreator: `${baseUrl}/${apiEndPoints.getRiskReviewsCreator}`,
  getRiskReviews: `${baseUrl}/${apiEndPoints.getRiskReviews}`,
  viewRiskReview: `${baseUrl}/${apiEndPoints.viewRiskReview}`,
  deleteRiskReview: `${baseUrl}/${apiEndPoints.deleteRiskReview}`,
  downloadRiskReview: `${baseUrl}/${apiEndPoints.downloadRiskReview}`,
  riskTrackerNew: `${baseUrl}/${apiEndPoints.riskTrackerNew}`,
  getEventsForPeriod: `${baseUrl}/${apiEndPoints.getEventsForPeriod}`,
  downloadEventsInExcel: `${baseUrl}/${apiEndPoints.downloadEventsInExcel}`,
  sendSelfEmail: `${baseUrl}/${apiEndPoints.sendSelfEmail}`,
  getAnalytics: `${baseUrl}/${apiEndPoints.getAnalytics}`,
  addSelectedEvents: `${baseUrl}/${apiEndPoints.addSelectedEvents}`,
  pastEvents:  `${baseUrl}/${apiEndPoints.pastEvents}`,
  trackLiveEvents: `${baseUrl}/${apiEndPoints.trackLiveEvents}`,
  impactRadius: `${baseUrl}/${apiEndPoints.impactRadius}`,
  checkTravelAdSub: `${baseUrl}/${apiEndPoints.checkTravelAdSub}`,
  filterGodrejEvents: `${baseUrl}/${apiEndPoints.filterGodrejEvents}`,
  getMonthsGodrej: `${baseUrl}/${apiEndPoints.getMonthsGodrej}`,
  getCountriesGodrej: `${baseUrl}/${apiEndPoints.getCountriesGodrej}`,
  getBusinessGroupsGodrej: `${baseUrl}/${apiEndPoints.getBusinessGroupsGodrej}`,
  viewGodrejEvent: `${baseUrl}/${apiEndPoints.viewGodrejEvent}`,
  getMonthsGodrejAdmin: `${baseUrl}/${apiEndPoints.getMonthsGodrejAdmin}`, // admin api get months
  riskLevelGodrejAdmin: `${baseUrl}/${apiEndPoints.riskLevelGodrejAdmin}`, // admin api
  pestelCatGodrejAdmin: `${baseUrl}/${apiEndPoints.pestelCatGodrejAdmin}`, // admin api
  countriesGodrejAdmin: `${baseUrl}/${apiEndPoints.countriesGodrejAdmin}`, // admin api
  bizGrpsGodrejAdmin: `${baseUrl}/${apiEndPoints.bizGrpsGodrejAdmin}`, // admin api
  addGodrejEventAdmin: `${baseUrl}/${apiEndPoints.addGodrejEventAdmin}`, // admin api add new godrej event
  godrejEventListAdmin: `${baseUrl}/${apiEndPoints.godrejEventListAdmin}`, // admin api get event list
  viewGodrejEventAdmin: `${baseUrl}/${apiEndPoints.viewGodrejEventAdmin}`, // admin api view individual event item from list
  getRiskLevelGodrej: `${baseUrl}/${apiEndPoints.getRiskLevelGodrej}`, // admin api view individual event item from list
  downloadGodrejEvents: `${baseUrl}/${apiEndPoints.downloadGodrejEvents}`, // admin api view individual event item from list
  uploadImageGodrej: `${baseUrl}/${apiEndPoints.uploadImageGodrej}`, // upload api for adding godrej images
  deleteImageGodrej: `${baseUrl}/${apiEndPoints.deleteImageGodrej}`, // delete api for deleting godrej images
  getEmergingRisks: `${baseUrl}/${apiEndPoints.getEmergingRisks}`, // get emerging risks
  rimeFeedUrl: `${baseUrl}/${apiEndPoints.rimeFeedUrl}`, // rime feed url
  addCoronaReading: `${baseUrl}/${apiEndPoints.addCoronaReading}`, // add corona reading
  getCoronaReading: `${baseUrl}/${apiEndPoints.getCoronaReading}`, // get corona reading
  getSingleCoronaReading: `${baseUrl}/${apiEndPoints.getSingleCoronaReading}`, // get single corona reading
  coronaDashboard: `${baseUrl}/${apiEndPoints.coronaDashboard}`, // get single corona reading
  stateWiseDashboard: `${baseUrl}/${apiEndPoints.stateWiseDashboard}`, // get state wise dashboard
  cityWiseDashboard: `${baseUrl}/${apiEndPoints.cityWiseDashboard}`, // get city wise dashboard
  geojson: `${baseUrl}/${apiEndPoints.geojson}`, // get geojson
  getCoronaAdvisory: `${baseUrl}/${apiEndPoints.getCoronaAdvisory}`, // get corona advisory
  getStateDetails: `${baseUrl}/${apiEndPoints.getStateDetails}`, // get state details corona
  postStateDetails: `${baseUrl}/${apiEndPoints.postStateDetails}`, // post state details corona
  getCoronaMapDetails: `${baseUrl}/${apiEndPoints.getCoronaMapDetails}`, // post covid-19map details
  downloadDistrictReport: `${baseUrl}/${apiEndPoints.downloadDistrictReport}`, // download district reports covid-19
  downloadDistrictReportExcel: `${baseUrl}/${apiEndPoints.downloadDistrictReportExcel}`, // download district reports excel covid-19
  downloadSummary: `${baseUrl}/${apiEndPoints.downloadSummary}`, // download summary covid-19
  getDistricts: `${baseUrl}/${apiEndPoints.getDistricts}`, // get districts
  saveDistricts: `${baseUrl}/${apiEndPoints.saveDistricts}`, // save districts
  addCoronaCity: `${baseUrl}/${apiEndPoints.addCoronaCity}`, // add corona cities
  getAllCoronaCities: `${baseUrl}/${apiEndPoints.getAllCoronaCities}`, // get all corona cities
  getReadingCovidCity: `${baseUrl}/${apiEndPoints.getReadingCovidCity}`, // edit all corona cities
  downloadCovidCity: `${baseUrl}/${apiEndPoints.downloadCovidCity}`, // download covid city
  getCityDetails: `${baseUrl}/${apiEndPoints.getCityDetails}`, // get city details
  postCityDetails: `${baseUrl}/${apiEndPoints.postCityDetails}`, // post city details
  getEmpDetails: `${baseUrl}/${apiEndPoints.getEmpDetails}`, // get emp details details
  downloadEmpExcel: `${baseUrl}/${apiEndPoints.downloadEmpExcel}`, //download details details on excel
  excelLocationReport: `${baseUrl}/${apiEndPoints.excelLocationReport}`, //location details on excel
  downloadRimeEmpLocations: `${baseUrl}/${apiEndPoints.downloadRimeEmpLocations}`, //location details on excel rime
  getEventListPortal: `${baseUrl}/${apiEndPoints.getEventListPortal}`, // new portal event list api
  getLiveUpdatePortal: `${baseUrl}/${apiEndPoints.getLiveUpdatePortal}`, // new portal live update api
  getTotalEvents: `${baseUrl}/${apiEndPoints.getTotalEvents}`, // get total events
  downloadEventArchives: `${baseUrl}/${apiEndPoints.downloadEventArchives}`, // get total events
  resetPassword: `${baseUrl}/${apiEndPoints.resetPassword}`, // get total events
  trackDispatch: `${baseUrl}/${apiEndPoints.trackDispatch}`,
  getClientType: `${baseUrl}/${apiEndPoints.getClientType}`,
  getQualityList: `${baseUrl}/${apiEndPoints.getQualityList}`,
  getAllTopics: `${baseUrl}/${apiEndPoints.getAllTopics}`,
  addEditTopic: `${baseUrl}/${apiEndPoints.addEditTopic}`,
  getLiveTopics: `${baseUrl}/${apiEndPoints.getLiveTopics}`,
  getIndiaWeekly: `${baseUrl}/${apiEndPoints.getIndiaWeekly}`,
  getSouthAsiaWeekly: `${baseUrl}/${apiEndPoints.getSouthAsiaWeekly}`,
  emeaWeekly: `${baseUrl}/${apiEndPoints.emeaWeekly}`,
  americasWeekly: `${baseUrl}/${apiEndPoints.americasWeekly}`,
  getApacWeekly: `${baseUrl}/${apiEndPoints.getApacWeekly}`,
  updateWeekly: `${baseUrl}/${apiEndPoints.updateWeekly}`,
  dispatchIndiaWeekly: `${baseUrl}/${apiEndPoints.dispatchIndiaWeekly}`,
  dispatchSouthAsiaWeekly: `${baseUrl}/${apiEndPoints.dispatchSouthAsiaWeekly}`,
  dispatchApacWeekly: `${baseUrl}/${apiEndPoints.dispatchApacWeekly}`,
  dispatchEmeaWeekly: `${baseUrl}/${apiEndPoints.dispatchEmeaWeekly}`,
  dispatchAmericasWeekly: `${baseUrl}/${apiEndPoints.dispatchAmericasWeekly}`,
  getCountryList: `${baseUrl}/${apiEndPoints.getCountryList}`,
  editCountryDetails: `${baseUrl}/${apiEndPoints.editCountryDetails}`,
  editCountryList: `${baseUrl}/${apiEndPoints.editCountryList}`,
  getCountryReadingList: `${baseUrl}/${apiEndPoints.getCountryReadingList}`,
  editCountryReading: `${baseUrl}/${apiEndPoints.editCountryReading}`,
  updateCoronaCountry: `${baseUrl}/${apiEndPoints.updateCoronaCountry}`,
  getCoronaCountryDashboard: `${baseUrl}/${apiEndPoints.getCoronaCountryDashboard}`,
  downloadCountryReport: `${baseUrl}/${apiEndPoints.downloadCountryReport}`,
  getEventCountryList: `${baseUrl}/${apiEndPoints.getEventCountryList}`,
  getCountryEventDetails: `${baseUrl}/${apiEndPoints.getCountryEventDetails}`,
  getWeekly: `${baseUrl}/${apiEndPoints.getWeekly}`,
  getWeeklyEvents: `${baseUrl}/${apiEndPoints.getWeeklyEvents}`,
  getCountryListBarclays: `${baseUrl}/${apiEndPoints.getCountryListBarclays}`,
  getCityListBarclays: `${baseUrl}/${apiEndPoints.getCityListBarclays}`,
  getAssetListBarclays: `${baseUrl}/${apiEndPoints.getAssetListBarclays}`,
  getRiskLevelBarclays: `${baseUrl}/${apiEndPoints.getRiskLevelBarclays}`,
  getRiskCategoryBarclays: `${baseUrl}/${apiEndPoints.getRiskCategoryBarclays}`,
  getSubRiskCategoryBarclays: `${baseUrl}/${apiEndPoints.getSubRiskCategoryBarclays}`,
  getEventListBarclays: `${baseUrl}/${apiEndPoints.getEventListBarclays}`,
  getEventBarclays: `${baseUrl}/${apiEndPoints.getEventBarclays}`,
  deleteEventBarclays: `${baseUrl}/${apiEndPoints.deleteEventBarclays}`,
  updateEventBarclays: `${baseUrl}/${apiEndPoints.updateEventBarclays}`,
  getDashboardDataBarclays: `${baseUrl}/${apiEndPoints.getDashboardDataBarclays}`,
  downloadEventsBarclays: `${baseUrl}/${apiEndPoints.downloadEventsBarclays}`,
  getDashboardEventDetailsBarclays: `${baseUrl}/${apiEndPoints.getDashboardEventDetailsBarclays}`,
  downloadPdfBarclays: `${baseUrl}/${apiEndPoints.downloadPdfBarclays}`,
  quadrantEventList: `${baseUrl}/${apiEndPoints.quadrantEventList}`,
  quadrantEventDetails: `${baseUrl}/${apiEndPoints.quadrantEventDetails}`,
  updateQuadrantDetails: `${baseUrl}/${apiEndPoints.updateQuadrantDetails}`,
  deleteQuadrantDetails: `${baseUrl}/${apiEndPoints.deleteQuadrantDetails}`,
  getQuadrants: `${baseUrl}/${apiEndPoints.getQuadrants}`,
  uploadEventImg: `${baseUrl}/${apiEndPoints.uploadEventImg}`,
  uploadReportingImg: `${baseUrl}/${apiEndPoints.uploadReportingImg}`,
  getQuadrantsV1: `${baseUrl}/${apiEndPoints.getQuadrantsV1}`,
  getRegions: `${baseUrl}/${apiEndPoints.getRegions}`,
  getRiskExposureData: `${baseUrl}/${apiEndPoints.getRiskExposureData}`,
  getCoverageRating: `${baseUrl}/${apiEndPoints.getCoverageRating}`,
  getBlogs: `${baseUrl}/${apiEndPoints.getBlogs}`,
  getPodcast: `${baseUrl}/${apiEndPoints.getPodcast}`,
  getSimilarityAnalysis: `${baseUrl}/${apiEndPoints.getSimilarityAnalysis}`,
  getAffectedEntities: `${baseUrl}/${apiEndPoints.getAffectedEntities}`,
  downloadAffectedEmployees: `${baseUrl}/${apiEndPoints.downloadAffectedEmployees}`,
  downloadAffectedAssets: `${baseUrl}/${apiEndPoints.downloadAffectedAssets}`,
  getAllPartners: `${baseUrl}/${apiEndPoints.getAllPartners}`,
  createPartner: `${baseUrl}/${apiEndPoints.createPartner}`,
  checkSubscriptions: `${baseUrl}/${apiEndPoints.checkSubscriptions}`,
  updateSettings: `${baseUrl}/${apiEndPoints.updateSettings}`,
  rimeRegions: `${baseUrl}/${apiEndPoints.rimeRegions}`,
  getPartnerEvents: `${baseUrl}/${apiEndPoints.getPartnerEvents}`,
  getPartnerEventById: `${baseUrl}/${apiEndPoints.getPartnerEventById}`,
  deletePartnerEventById: `${baseUrl}/${apiEndPoints.deletePartnerEventById}`,
  updatePartnerEvent: `${baseUrl}/${apiEndPoints.updatePartnerEvent}`,
  travelCountryList: `${baseUrl}/${apiEndPoints.travelCountryList}`,
  eventCreatorTrackingId: `${baseUrl}/${apiEndPoints.eventCreatorTrackingId}`,
  eventClientTrackingId: `${baseUrl}/${apiEndPoints.eventClientTrackingId}`,
  downloadArchives: `${baseUrl}/${apiEndPoints.downloadArchives}`,
  getPartnerDetails: `${baseUrl}/${apiEndPoints.getPartnerDetails}`,
  addQuery: `${baseUrl}/${apiEndPoints.addQuery}`,
  downloadQuery: `${baseUrl}/${apiEndPoints.downloadQuery}`,
  getRelatedEvents: `${baseUrl}/${apiEndPoints.getRelatedEvents}`,
  getRelevantClientList: `${baseUrl}/${apiEndPoints.getRelevantClientList}`,
  topicAnalysis: `${baseUrl}/${apiEndPoints.topicAnalysis}`,
  getLocationAnalysis: `${baseUrl}/${apiEndPoints.getLocationAnalysis}`,
  getClientLocationAnalysis: `${baseUrl}/${apiEndPoints.getClientLocationAnalysis}`,
  getClientLocationsAnalysis: `${baseUrl}/${apiEndPoints.getClientLocationsAnalysis}`,
  getAllBlogs: `${baseUrl}/${apiEndPoints.getAllBlogs}`,
  createBlog: `${baseUrl}/${apiEndPoints.createBlog}`,
  viewBlog: `${baseUrl}/${apiEndPoints.viewBlog}`,
  deleteBlog: `${baseUrl}/${apiEndPoints.deleteBlog}`,
  editBlog: `${baseUrl}/${apiEndPoints.editBlog}`,
  updateBlog: `${baseUrl}/${apiEndPoints.updateBlog}`,
  downloadBlog: `${baseUrl}/${apiEndPoints.downloadBlog}`,
  getAllPodcast: `${baseUrl}/${apiEndPoints.getAllPodcast}`,
  createUpdatePodcast: `${baseUrl}/${apiEndPoints.createUpdatePodcast}`,
  deletePodcast: `${baseUrl}/${apiEndPoints.deletePodcast}`,
  editPodcast: `${baseUrl}/${apiEndPoints.editPodcast}`,
  editUserRole: `${baseUrl}/${apiEndPoints.editUserRole}`,
  getViewList: `${baseUrl}/${apiEndPoints.getViewList}`,
  getRimeSavedFilters: `${baseUrl}/${apiEndPoints.getRimeSavedFilters}`,
  getRimeSubscriptions: `${baseUrl}/${apiEndPoints.getRimeSubscriptions}`,
  setRimeSubscriptions: `${baseUrl}/${apiEndPoints.setRimeSubscriptions}`,
  rimeTimeRange: `${rimeServer}/${apiEndPoints.rimeTimeRange}`,
  rimeDateRange: `${rimeServer}/${apiEndPoints.rimeDateRange}`,
  addWatchList: `${baseUrl}/${apiEndPoints.addWatchList}`,
  removeWatchList: `${baseUrl}/${apiEndPoints.removeWatchList}`,
  getWatchList: `${baseUrl}/${apiEndPoints.getWatchList}`,
  deleteScreenEvent: `${baseUrl}/${apiEndPoints.deleteScreenEvent}`,
  editFilteredCardEvent: `${baseUrl}/${apiEndPoints.editFilteredCardEvent}`,
  topEvents,
  downloadCountryAnalysis: `${baseUrl}/${apiEndPoints.downloadCountryAnalysis}`,
  getMonitoringFeed: `${baseUrl}/${apiEndPoints.getMonitoringFeed}`,
  clientUserList: `${baseUrl}/${apiEndPoints.clientUserList}`,
  addClientUser: `${baseUrl}/${apiEndPoints.addClientUser}`,
  deleteClientUser: `${baseUrl}/${apiEndPoints.deleteClientUser}`,
  clientLocationList: `${baseUrl}/${apiEndPoints.clientLocationList}`,
  addClientLocation: `${baseUrl}/${apiEndPoints.addClientLocation}`,
};

export const websiteApiEndpoints = {
  login: `api/logincoviduser`,
};

export const websiteEndPoint = {
  login: `${mitkatWebUrl}/${websiteApiEndpoints.login}`,
};

export const keywordMonitorApiEndpoints = {
  queryingNews: `api/search`,
};

export const keywordMonitor = {
  queryingNews: `${globalnewsapi}/${keywordMonitorApiEndpoints.queryingNews}`,
};
