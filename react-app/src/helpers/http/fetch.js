import { toggleIsFetch, toggleToast } from '../../actions';
import store from '../../store';
import Cookies from 'universal-cookie';

export function fetchApi(params) {
  const cookies = new Cookies();
  let reqUrl = params.url;
  let reqObj = {
    method: params.method,
    mode: 'cors',
    cache: 'default',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${params.isAuth ? params.authToken : ''}`,
    },
    // body: JSON.stringify(params.data ? params.data : ""),
    ...(params.data && {
      body: JSON.stringify(params.data ? params.data : ''),
    }),
  };

  params.showToggle && store.dispatch(toggleIsFetch(true));

  return fetch(reqUrl, reqObj)
    .then(response => {
      // force logout if unauthorished
      if (response.status === 401 || response.status === 405) {
        cookies.remove('authToken');
        cookies.remove('userRole');
        window.location.href = '/end-of-session';
        return;
      }

      if (params.isBlob) {
        return response.blob();
      } else {
        return response.json();
      }
    })
    .then(data => {
      store.dispatch(toggleIsFetch(false));
      return data;
    })
    .catch(e => {
      store.dispatch(toggleIsFetch(false));
      store.dispatch(
        toggleToast({ toastMsg: 'Something went wrong!', showToast: true }),
      );
      return e;
    });
}

export function uploadApi(url, reqObj) {
  store.dispatch(toggleIsFetch(true));

  return fetch(url, reqObj)
    .then(res => {
      // force logout if unauthorished
      if (res.status === 401 || res.status === 405) {
        window.location.href = '/end-of-session';
        return;
      }

      store.dispatch(toggleIsFetch(false));
      return res.json();
    })
    .then(data => data)
    .catch(e => {
      store.dispatch(toggleIsFetch(false));
      store.dispatch(
        toggleToast({ toastMsg: 'Something went wrong!', showToast: true }),
      );
      return e;
    });
}