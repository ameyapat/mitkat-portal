import globalVariables from '../sass/globalVariables.scss';

export const RISK_LEVEL_COLORS = {
  1: '#EBF7C9', //verylow
  2: '#b8e994', //low
  3: '#FEEA81', //medium
  4: '#FCD293', //high
  5: '#E48F6F', //veryhigh
};

export const RISK_CATEGORY_ICONS = {
  1: 'icon-uniE9C7',
  2: 'icon-uniE9BF',
  3: 'icon-uniE9D6',
  4: 'icon-uniE9EC',
  5: 'icon-uniE9F8',
  6: 'icon-uniE95B',
  7: 'icon-uniE9C5',
};

export const RISK_CATEGORY_NAME = {
  1: 'Cyber / Technology',
  2: 'Extremism',
  3: 'Civil Disturbance',
  4: 'Environment',
  5: 'Health',
  6: 'Critical Infrastructure',
  7: 'Crime',
  8: 'Travel Risks',
  9: 'Natural Disasters',
  10: 'External Threats',
  11: 'Political',
  12: 'Regulatory',
};

export const RISK_CATEGORIES_NEON = {
  1: '#FFDC34', // Cyber
  2: '#C5005E', // Insurgency / Terrorism / Extremism
  3: '#7FFBFF', // Civil Disturbance
  4: '#23CD98', // Environment
  5: '#EEA672', // Health
  6: '#B07AFB', // Critical Infrastructure
  7: '#4BB2E7', // Crime
};

export const RISK_LEVEL_COLORS_NEON = {
  0: globalVariables.globalVeryLow, //verylow
  1: globalVariables.globalVeryLow, //verylow
  2: globalVariables.globalLow, //low
  3: globalVariables.globalMedium, //medium
  4: globalVariables.globalHigh, //high
  5: globalVariables.globalVeryHigh, //veryhigh
};
export const RISK_LEVEL_NAME = {
  0: 'Very Low',
  1: 'Very Low',
  2: 'Low',
  3: 'Medium',
  4: 'High',
  5: 'Very High',
};

export const navItems = {
  userOptions: [
    // {
    //   id: 1,
    //   title: 'Dashboard',
    //   icon: 'fas fa-tachometer-alt',
    //   to: '',
    //   type: 'dashboard',
    //   blackList: [],
    // },
    {
      id: 2,
      title: 'Client Management',
      icon: 'fas fa-users',
      to: '/client',
      type: 'clientManagement',
      blackList: [],
    },
    {
      id: 3,
      title: 'User Management',
      icon: 'fas fa-user',
      to: '/user',
      type: 'userManagement',
      blackList: [],
    },
    {
      id: 33,
      title: 'Partner Management',
      icon: 'fas fa-user',
      to: '/partner',
      type: 'partnerManagement',
      blackList: ['ROLE_PARTNER_ADMIN'],
    },
    {
      id: 4,
      title: 'Performance Index',
      icon: 'fas fa-indent',
      to: '/performanceIndex',
      type: 'performanceIndex',
      blackList: [],
    },
  ],
  covid_19_tabs: [
    {
      id: 9,
      title: 'Create Corona Alert',
      icon: 'fas fa-virus',
      type: 'createCoronaAlert',
      isNew: true,
      isRoute: false,
      blackList: [],
    },
    {
      id: 10,
      title: 'View Corona Alerts',
      icon: 'fas fa-shield-virus',
      type: 'viewCoronaAlerts',
      to: '/viewCoronaAlerts',
      isNew: true,
      isRoute: true,
      blackList: [],
    },
    {
      id: 99,
      title: 'Create Corona Alert - City',
      icon: 'fas fa-disease',
      type: 'createCoronaAlertCity',
      isNew: true,
      isRoute: false,
      blackList: [],
    },
    {
      id: 1010,
      title: 'View Corona Alerts - City',
      icon: 'fas fa-city',
      type: 'viewCoronaAlertsCity',
      to: '/viewCoronaAlertsCity',
      isNew: true,
      isRoute: true,
      blackList: [],
    },
    {
      id: 9878,
      title: 'Create Corona Alert - Country',
      icon: 'fas fa-lungs-virus',
      type: 'createCovidAlert',
      to: '/createCovidAlert',
      isRoute: false,
      blackList: [],
    },
    {
      id: 98787,
      title: 'View Corona Alerts - Country',
      icon: 'fas fa-people-arrows',
      type: 'viewCovidAlert',
      to: '/viewCovidAlert',
      isRoute: true,
      blackList: [],
    },
    {
      id: 1111,
      title: 'Covid-19 - City',
      icon: 'fas fa-infinity',
      type: 'viewCovidCity',
      to: '/viewCovidCity',
      isNew: true,
      isRoute: true,
      blackList: [],
    },
    {
      id: 11,
      title: 'Covid-19 - State',
      icon: 'fas fa-house-user',
      type: 'viewCovidState',
      to: '/viewCovidState',
      isNew: true,
      isRoute: true,
      blackList: [],
    },
    {
      id: 98788,
      title: 'Covid-19 - Country',
      icon: 'fas fa-head-side-virus',
      type: 'viewCovidCountry',
      to: '/viewCovidCountry',
      isRoute: true,
      blackList: [],
    },
  ],
  deliverables_tabs: [
    {
      id: 2,
      title: 'Create New Event',
      icon: 'fas fa-calendar-week',
      type: 'createEvent',
      isRoute: false,
      blackList: [],
    },
    {
      id: 5,
      title: 'Create Email Alert',
      icon: 'fas fa-envelope',
      type: 'createEmailAlert',
      isRoute: false,
      blackList: [],
    },
    {
      id: 4,
      title: 'Add Event Updates',
      icon: 'fas fa-edit',
      type: 'addEventUpdates',
      to: '/updateEvents',
      isRoute: true,
      blackList: [],
    },
    {
      id: 892,
      title: 'Topic Management',
      icon: 'fas fa-book-reader',
      type: 'topicManagement',
      to: '/topicManagement',
      isRoute: true,
      blackList: [],
    },
  ],
  eventDatabase_tabs: [
    {
      id: 22,
      title: 'Daily Risk Tracker',
      icon: 'fas fa-chart-line',
      type: 'dailyRiskTracker',
      to: '/dailyRiskTracker',
      isRoute: true,
      blackList: [],
    },
    {
      id: 33,
      title: 'Download Events',
      icon: 'fas fa-download',
      type: 'downloadEvents',
      to: '/downloadEvents',
      isRoute: true,
      blackList: [],
    },
    {
      id: 44,
      title: 'Edit By Tracking Id',
      icon: 'fas fa-tachometer-alt',
      type: 'trackingId',
      to: '/trackingId',
      isRoute: true,
      blackList: [],
    },
  ],
  other_tabs: [
    {
      id: 887,
      title: 'Monthly Compilation',
      icon: 'far fa-list-alt',
      type: 'monthlyCompilation',
      to: '/monthlyCompilation',
      isRoute: true,
      blackList: [],
    },
    {
      id: 893,
      title: 'Coverage Rating',
      icon: 'fas fa-book-reader',
      type: 'coverageRating',
      to: '/coverageRating',
      isRoute: true,
      blackList: [],
    },
    {
      id: 4,
      title: 'Track Dispatch',
      icon: 'fas fa-truck-moving',
      type: 'trackDispatch',
      to: '/trackDispatch',
      isRoute: true,
      blackList: [],
    },
    {
      id: 5,
      title: 'Duplicate Events',
      icon: 'fas fa-clone',
      type: 'duplicateEvents',
      to: '/duplicateEvents',
      isRoute: true,
      blackList: [],
    },
  ],
  upload_tabs: [
    {
      id: 1,
      title: 'Monthly',
      icon: 'fas fa-file-pdf',
      type: 'reports',
      to: '/reports',
      blackList: [],
    },
    {
      id: 2,
      title: 'Advisory',
      icon: 'far fa-file-alt',
      type: 'advisory',
      to: '/advisory',
      blackList: [],
    },
    {
      id: 3,
      title: 'Special Reports',
      icon: 'fas fa-file-image',
      type: 'specialReports',
      to: '/specialReports',
      blackList: [],
    },
    {
      id: 4,
      title: 'Risk Review',
      icon: 'fas fa-exclamation-triangle',
      type: 'riskReview',
      to: '/riskReview',
      blackList: [],
    },
    {
      id: 5,
      title: 'Travel Advisories',
      icon: 'fas fa-car',
      type: 'travelAdvisory',
      to: '/travelAdvisory',
      blackList: [],
    },
    {
      id: 6,
      title: 'Blog',
      icon: 'fas fa-blog',
      type: 'blog',
      to: '/blog',
      blackList: [],
    },
    {
      id: 7,
      title: 'Podcast',
      icon: 'fas fa-podcast',
      type: 'podcast',
      to: '/podcast',
      blackList: [],
    },
  ],
  eventManagement: [
    {
      id: 1,
      title: 'Dashboard',
      icon: 'fas fa-tachometer-alt',
      to: '',
      type: 'dashboard',
      isRoute: true,
    },
    // {
    //   id: 9,
    //   title: 'Create Corona Alert',
    //   icon: 'fas fa-virus',
    //   type: 'createCoronaAlert',
    //   isNew: true,
    //   isRoute: false,
    // },
    // {
    //   id: 99,
    //   title: 'Create Corona Alert - City',
    //   icon: 'fas fa-disease',
    //   type: 'createCoronaAlertCity',
    //   isNew: true,
    //   isRoute: false,
    // },
    // {
    //   id: 10,
    //   title: 'View Corona Alerts',
    //   icon: 'fas fa-shield-virus',
    //   type: 'viewCoronaAlerts',
    //   to: '/viewCoronaAlerts',
    //   isNew: true,
    //   isRoute: true,
    // },
    // {
    //   id: 1010,
    //   title: 'View Corona Alerts - City',
    //   icon: 'fas fa-city',
    //   type: 'viewCoronaAlertsCity',
    //   to: '/viewCoronaAlertsCity',
    //   isNew: true,
    //   isRoute: true,
    // },
    // {
    //   id: 11,
    //   title: 'Covid-19 - State',
    //   icon: 'fas fa-house-user',
    //   type: 'viewCovidState',
    //   to: '/viewCovidState',
    //   isNew: true,
    //   isRoute: true,
    // },
    // {
    //   id: 1111,
    //   title: 'Covid-19 - City',
    //   icon: 'fas fa-infinity',
    //   type: 'viewCovidCity',
    //   to: '/viewCovidCity',
    //   isNew: true,
    //   isRoute: true,
    // },
    // {
    //   id: 2,
    //   title: 'Create New Event',
    //   icon: 'fas fa-calendar-week',
    //   type: 'createEvent',
    //   isRoute: false,
    // },
    {
      id: 22,
      title: 'Daily Risk Tracker',
      icon: 'fas fa-chart-line',
      type: 'dailyRiskTracker',
      to: '/dailyRiskTracker',
      isRoute: true,
    },
    {
      id: 33,
      title: 'Download Events',
      icon: 'fas fa-download',
      type: 'downloadEvents',
      to: '/downloadEvents',
      isRoute: true,
    },
    // {
    //   id: 3,
    //   title: 'Event Archives',
    //   icon: 'fas fa-newspaper',
    //   type: 'updateEvent',
    //   to: '/editEvents',
    //   isRoute: true,
    // },
    {
      id: 4,
      title: 'Add Event Updates',
      icon: 'fas fa-edit',
      type: 'addEventUpdates',
      to: '/updateEvents',
      isRoute: true,
    },
    // {
    //   id: 5,
    //   title: 'Create Email Alert',
    //   icon: 'fas fa-envelope',
    //   type: 'createEmailAlert',
    //   isRoute: false,
    // },
    {
      id: 6,
      title: 'Create Sms Alert',
      icon: 'fas fa-sms',
      type: 'createSmsAlert',
      isRoute: false,
    },
    // {
    //   id: 7,
    //   title: 'Create Risk Watch',
    //   icon: 'fas fa-exclamation',
    //   type: 'createRiskWatch',
    //   isRoute: false,
    // },
    // {
    //   id: 8,
    //   title: 'View Risk Watch',
    //   icon: 'fas fa-exclamation-triangle',
    //   type: 'viewRiskWatch',
    //   to: '/viewRiskWatch',
    //   isRoute: true,
    // },
    // {
    //   id: 9878,
    //   title: 'Create Corona Alert - Country',
    //   icon: 'fas fa-lungs-virus',
    //   type: 'createCovidAlert',
    //   to: '/createCovidAlert',
    //   isRoute: false,
    // },
    // {
    //   id: 98787,
    //   title: 'View Corona Alerts - Country',
    //   icon: 'fas fa-people-arrows',
    //   type: 'viewCovidAlert',
    //   to: '/viewCovidAlert',
    //   isRoute: true,
    // },
    // {
    //   id: 98788,
    //   title: 'Covid-19 - Country',
    //   icon: 'fas fa-head-side-virus',
    //   type: 'viewCovidCountry',
    //   to: '/viewCovidCountry',
    //   isRoute: true,
    // },
    // {
    //   id: 887,
    //   title: 'Monthly Compilation',
    //   icon: 'far fa-list-alt',
    //   type: 'monthlyCompilation',
    //   to: '/monthlyCompilation',
    //   isRoute: true,
    // },
    // {
    //   id: 892,
    //   title: 'Topic Management',
    //   icon: 'fas fa-book-reader',
    //   type: 'topicManagement',
    //   to: '/topicManagement',
    //   isRoute: true,
    // },
    // {
    //   id: 892342,
    //   title: 'Kaleidoscope',
    //   icon: 'fab fa-quinscape',
    //   type: 'kaleidoscope',
    //   to: '/kaleidoscope',
    //   isRoute: true,
    // },
  ],
  weekly: [
    {
      id: 1,
      title: 'India Weekly',
      icon: 'fas fa-calendar-week',
      type: 'indiaWeekly',
      to: '/indiaWeekly',
    },
    {
      id: 2,
      title: 'South Asia Weekly',
      icon: 'fas fa-calendar-alt',
      type: '/southasiaWeekly',
      to: '/southasiaWeekly',
    },
    {
      id: 3,
      title: 'APAC Weekly',
      icon: 'fas fa-calendar-check',
      type: '/apacWeekly',
      to: '/apacWeekly',
    },
    {
      id: 22,
      title: 'EMEA Weekly',
      icon: 'fas fa-calendar-alt',
      type: '/emeaWeekly',
      to: '/emeaWeekly',
    },
    {
      id: 33,
      title: 'Americas Weekly',
      icon: 'fas fa-calendar-check',
      type: '/americasWeekly',
      to: '/americasWeekly',
    },
    {
      id: 892342,
      title: 'Kaleidoscope',
      icon: 'fab fa-quinscape',
      type: 'kaleidoscope',
      to: '/kaleidoscope',
      isRoute: true,
    },
  ],
  eventApprovals: [
    {
      id: 1,
      title: 'Approve Deliverables',
      icon: 'fas fa-thumbs-up',
      type: 'amd',
      to: '/amd',
    },
    {
      id: 2,
      title: 'Approve Email Alert',
      icon: 'fas fa-envelope',
      type: '/approveEmailAlert',
      to: '/approveEmail',
    },
    // {
    //   id: 3,
    //   title: 'Approve SMS Alert',
    //   icon: 'fas fa-sms',
    //   type: '/approveSmsAlert',
    //   to: '/approveSms',
    // },
  ],
  approve_deliverable_tabs: [
    {
      id: 1,
      title: 'Approve Deliverables',
      icon: 'fas fa-thumbs-up',
      type: 'amd',
      to: '/amd',
    },
    {
      id: 2,
      title: 'Approve Email Alert',
      icon: 'fas fa-envelope',
      type: '/approveEmailAlert',
      to: '/approveEmail',
    },
    {
      id: 3,
      title: 'Approve Partner Alerts',
      icon: 'fas fa-book-reader',
      type: 'partnerAlerts',
      to: '/partnerAlerts',
      isRoute: true,
      blackList: [],
    },
  ],
  dispatchOptions: [
    {
      id: 1,
      title: 'Dispatch Bulk Alerts',
      icon: 'fas fa-truck',
      type: 'dmd',
      to: '/dmd',
    },
    {
      id: 2,
      title: 'Dispatch Email Alert',
      icon: 'fas fa-envelope',
      type: '/dispatchEmailAlert',
      to: '/dispatchEmail',
    },
    // {
    //   id: 3,
    //   title: 'Dispatch SMS Alert',
    //   icon: 'fas fa-sms',
    //   type: '/dispatchSmsAlert',
    //   to: '/dispatchSms',
    // },
  ],
  queryOptions: [
    {
      id: 1,
      title: 'View / Answer Queries',
      icon: 'fas fa-question',
      type: 'query',
      to: '/queries',
      isRoute: true,
      blackList: [],
    },
    {
      id: 2,
      title: 'Create Queries',
      icon: 'fas fa-question',
      type: 'createQueries',
      isRoute: false,
      blackList: [],
    },
    {
      id: 2,
      title: 'Download Queries',
      icon: 'fas fa-download',
      type: 'downloadQueries',
      isRoute: false,
      blackList: [],
    },
  ],
  advisoryOptions: [
    {
      id: 1,
      title: 'Advisory',
      icon: 'far fa-file-alt',
      type: 'advisory',
      to: '/advisory',
    },
    {
      id: 2,
      title: 'Travel Advisory',
      icon: 'fas fa-car',
      type: 'travelAdvisory',
      to: '/travelAdvisory',
    },
  ],
  reportsOptions: [
    {
      id: 1,
      title: 'Weekly/Monthly',
      icon: 'fas fa-file-pdf',
      type: 'reports',
      to: '/reports',
    },
    {
      id: 2,
      title: 'Special Reports',
      icon: 'fas fa-file-image',
      type: 'specialReports',
      to: '/specialReports',
    },
  ],
  riskReviewOptions: [
    {
      id: 1,
      title: 'Risk Review',
      icon: 'fas fa-exclamation-triangle',
      type: 'riskReview',
      to: '/riskReview',
    },
  ],
  rimeDashboard: [
    {
      id: 1,
      title: 'Dashboard',
      icon: 'fas fa-users',
      to: '/client/rime',
      isRoute: true,
    },
  ],
  datasurfer_tabs: [
    {
      id: 2,
      title: 'Monitoring Screen',
      icon: 'fas fa-book-reader',
      type: 'monitoringScreen',
      to: '/monitoringScreen',
      isRoute: true,
      blackList: [],
    },
    {
      id: 3,
      title: 'Country Monitoring',
      icon: 'far fa-list-alt',
      type: 'countryAnalysis',
      to: '/countryAnalysis',
      isRoute: true,
      blackList: [],
    },
    {
      id: 4,
      title: 'City Monitoring',
      icon: 'far fa-list-alt',
      type: 'cityAnalysis',
      to: '/cityAnalysis',
      isRoute: true,
      blackList: [],
    },
  ],
  screening_tabs: [
    {
      id: 1,
      title: 'Screening',
      icon: 'far fa-list-alt',
      type: 'rimeScreening',
      to: '/rimeScreening',
      isRoute: true,
      blackList: [],
    },
    {
      id: 2,
      title: 'Add Event',
      icon: 'fas fa-book-reader',
      type: 'addEvent',
      to: '/addEvent',
      isRoute: true,
      blackList: [],
    },
    {
      id: 3,
      title: 'Events by Surfers',
      icon: 'fas fa-book-reader',
      type: 'rimeFilteredData',
      to: '/rimeFilteredData',
      isRoute: true,
      blackList: [],
    },
    {
      id: 4,
      title: 'Curator Analysis',
      icon: 'far fa-list-alt',
      type: 'curatorAnalysis',
      to: '/curatorAnalysis',
      isRoute: true,
      blackList: [],
    },
    {
      id: 5,
      title: 'Missed Links',
      icon: 'far fa-list-alt',
      type: 'missedLinks',
      to: '/missedLinks',
      isRoute: true,
      blackList: [],
    },
    {
      id: 6,
      title: 'Past Data',
      icon: 'fas fa-book-reader',
      type: 'pastData',
      to: '/pastData',
      isRoute: true,
      blackList: [],
    },
  ],
};

export const metrics = [
  {
    id: 1,
    color: '#2ecc71',
    number: 15234,
    title: 'Recent Events',
  },
  {
    id: 2,
    color: '#e74c3c',
    number: 534,
    title: 'Live Events',
  },
  {
    id: 3,
    color: '#3498db',
    number: 134,
    title: 'Email Alerts',
  },
  {
    id: 4,
    color: '#e67e22',
    number: 96,
    title: 'SMS Alerts',
  },
];
