import { API_ROUTES } from './http/apiRoutes';
import { fetchApi } from './http/fetch';
import Cookies from 'universal-cookie';

export function toBool(bool) {
  if (bool === 'false' || bool === false) {
    return 0;
  } else {
    return 1;
  }
}

export function toBoolean(bool) {
  return (bool && bool ? 'true' : 'false') || 'false';
}

export function getArrFromObj(obj) {
  let newArr = [];

  obj.forEach(item => {
    if (item.hasOwnProperty('value')) {
      newArr.push(item.value);
    } else {
      newArr.push(item);
    }
  });

  return newArr;
}

export function isNull(value) {
  let isNull = false;

  if (
    value === 'undefined' ||
    value === undefined ||
    value === null ||
    value === ''
  ) {
    isNull = true;
  } else {
    isNull = false;
  }

  return isNull;
}

export function hasData(value) {
  return value && value.length > 0 ? true : false;
}

export function hasNewObj(newObj, oldObj) {
  let hasNewObj = false;

  for (let i = 0; i < newObj.length - 1; i++) {
    if (newObj[i].id !== oldObj[i].id) {
      // let hasNewObj = true;
      return true;
    }
  }

  return hasNewObj;
}

export function filterArrObjects(arrVal) {
  return arrVal.map(item => item.value);
}

export function filterByLatLng(arrVal) {
  const newArr = [];

  arrVal.map(i => {
    const newObj = {};
    newObj.latitude = i.lat;
    newObj.longitude = i.lng;
    newArr.push(newObj);
  });

  return newArr;
}

//get countries
export function fetchCountries(authToken = null) {
  const cookies = new Cookies();
  const authTokenViaCookie = cookies.get('authToken');

  return new Promise((res, rej) => {
    let countries = [];
    let reqObj = {
      url: API_ROUTES.fetchCountries,
      method: 'POST',
      isAuth: true,
      authToken: authToken || authTokenViaCookie,
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        data.map(i => {
          let countriesObj = {};
          countriesObj['value'] = i.id;
          countriesObj['label'] = i.countryname;
          countries.push(countriesObj);
        });
        res(countries);
      })
      .catch(e => {
        rej(e, 'Error fetching countires');
      });
  });
}
//get client-types
export function fetchClientTypes(authToken) {
  return new Promise((res, rej) => {
    let clientTypes = [];
    let reqObj = {
      url: API_ROUTES.getClientType,
      method: 'GET',
      isAuth: true,
      authToken,
      showToggle: false,
    };

    fetchApi(reqObj)
      .then(data => {
        data.map(i => {
          let clientTypeObj = {};
          clientTypeObj['value'] = i.id;
          clientTypeObj['label'] = i.type;
          clientTypes.push(clientTypeObj);
        });
        res(clientTypes);
      })
      .catch(e => {
        rej(e, 'Error fetching client types!');
      });
  });
}
//get states
export function fetchStates(authToken) {
  return new Promise((res, rej) => {
    let states = [];
    let reqObj = {
      url: API_ROUTES.fetchStates,
      method: 'POST',
      isAuth: true,
      authToken,
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        data.map(i => {
          let statesObj = {};
          statesObj['value'] = i.id;
          statesObj['label'] = i.statename;
          states.push(statesObj);
        });
        res(states);
      })
      .catch(e => {
        rej(e, 'Error fetching states');
      });
  });
}

//get cities
export function fetchCities(authToken) {
  return new Promise((res, rej) => {
    let cities = [];
    let reqObj = {
      url: API_ROUTES.fetchMetros,
      method: 'POST',
      isAuth: true,
      authToken,
      showToggle: true,
    };
    fetchApi(reqObj)
      .then(data => {
        data.map(i => {
          let citiesObj = {};
          citiesObj['value'] = i.id;
          citiesObj['label'] = i.cityname;
          cities.push(citiesObj);
        });
        res(cities);
      })
      .catch(e => {
        rej(e, 'Error fetching cities');
      });
  });
}

// export const getClientLocations = () => {
//   let { cookies, clientId } = this.props;
//   let authTokenFromCookie = cookies.get('authToken');

//   let params = {
//     url: `${API_ROUTES.viewClientLocation}/${clientId}`,
//     method: 'POST',
//     isAuth: true,
//     authToken: authTokenFromCookie,
//     showToggle: true,
//   };

//   fetchApi(params)
//     .then(data => {
//       this.setState({ clientLocations: data });
//     })
//     .catch(e => {
//       console.log(e);
//     });
// };

//get products
export function fetchProducts(authToken) {
  return new Promise((res, rej) => {
    let typeOfProducts = [];
    let reqObj = {
      url: API_ROUTES.fetchTypeOfProducts,
      method: 'POST',
      isAuth: true,
      authToken,
      showToggle: true,
    };
    fetchApi(reqObj)
      .then(data => {
        data.map(i => {
          let productsObj = {};
          productsObj['value'] = i.id;
          productsObj['label'] = i.type;
          typeOfProducts.push(productsObj);
        });
        res(typeOfProducts);
      })
      .catch(e => {
        rej(e, 'Error fetching cities');
      });
  });
}

//get impact industries
export function fetchIndustries(authToken) {
  return new Promise((res, rej) => {
    let impactIndustries = [];
    let reqObj = {
      url: API_ROUTES.fetchImpactIndustry,
      method: 'POST',
      isAuth: true,
      authToken,
      showToggle: true,
    };
    fetchApi(reqObj)
      .then(data => {
        data.map(i => {
          let industryObj = {};
          industryObj['value'] = i.id;
          industryObj['label'] = i.industryname;
          impactIndustries.push(industryObj);
        });
        res(impactIndustries);
      })
      .catch(e => {
        rej(e, 'Error fetching cities');
      });
  });
}

//fetch impact radius value
export function getImpactRadius(riskObj) {
  return new Promise((res, rej) => {
    let reqObj = {
      url: API_ROUTES.impactRadius,
      method: 'POST',
      isAuth: true,
      authToken: riskObj.authToken,
      data: {
        risklevel: `${riskObj.riskLevelId}`,
        riskcategory: `${riskObj.riskCategoryId}`,
      },
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        res(data);
      })
      .catch(e => {
        rej(e, 'Error fetching impact radius');
      });
  });
}

//create value & label object

export function createValueLabel(values, type) {
  let newArr = [];

  switch (type) {
    case 'country':
      values.map(i => {
        let newObj = {};
        newObj['value'] = i.id;
        newObj['label'] = i.countryname;
        newArr.push(newObj);
      });
      return newArr;
    case 'state':
      values.map(i => {
        let newObj = {};
        newObj['value'] = i.id;
        newObj['label'] = i.statename;
        newArr.push(newObj);
      });
      return newArr;
    case 'city':
      values.map(i => {
        let newObj = {};
        newObj['value'] = i.id;
        newObj['label'] = i.cityname;
        newArr.push(newObj);
      });
      return newArr;
    case 'impactIndustry':
      values.map(i => {
        let newObj = {};
        newObj['value'] = i.id;
        newObj['label'] = i.industryname;
        newArr.push(newObj);
      });
      return newArr;
    case 'clients':
      values.map(i => {
        let newObj = {};
        newObj['value'] = i.id;
        newObj['label'] = i.clientName;
        newArr.push(newObj);
      });
      return newArr;
    case 'businessGroup':
      values.map(i => {
        let newObj = {};
        newObj['value'] = i.id;
        newObj['label'] = i.businessName;
        newArr.push(newObj);
      });
      return newArr;
    default:
      return newArr;
  }
}

export function createLabelObj(labelObj, type) {
  let newArr = [];
  switch (type) {
    case 'selectedStory':
      let newObj1 = {};
      newObj1.value = labelObj.id;
      newObj1.label = labelObj.regiontype;
      return newObj1;
    case 'selectedCategory':
      let newObj2 = {};
      newObj2.value = labelObj.id;
      newObj2.label = labelObj.riskcategory;
      return newObj2;
    case 'selectedRiskLevel':
      let newObj3 = {};
      newObj3.value = labelObj.id;
      newObj3.label = labelObj.level;
      return newObj3;
    default:
      return { value: '', label: '' };
  }
}

//get risktrack products

export function fetchRiskProducts(authToken) {
  return new Promise((res, rej) => {
    let reqObj = {
      url: API_ROUTES.getRiskProducts,
      method: 'POST',
      isAuth: true,
      authToken,
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        res(data);
      })
      .catch(e => console.log(e));
  });
}

//search by keyword
export function searchByKeyword(value, list) {
  return list.filter(val => {
    let regex = new RegExp(value, 'gi');
    return (
      val.title.match(regex) ||
      val.country.match(regex) ||
      val.storytype.match(regex) ||
      val.creator.match(regex) ||
      val.storytype.match(regex)
    );
  });
}

export function searchByTitle(value, list) {
  return list.filter(val => {
    let regex = new RegExp(value, 'gi');
    return val.title.match(regex);
  });
}

export function advancedSearch(value, list) {
  let regex = new RegExp(value, 'gi');

  return list.filter((item, idx) => {
    for (let k in item) {
      if (k != 'id' && item[k].match(regex)) {
        return item;
      }
    }
  });
}

export function searchRiskTrackerEvents(value, list) {
  let regex = new RegExp(value, 'gi');

  return list.filter((item, idx) => {
    return item.title.match(regex);
  });
}

export function rimeFilteredFeedSearch(value, list) {
  let regex = new RegExp(value, 'gi');

  return list.filter((item, idx) => {
    if (
      item?.eventTitle.match(regex) ||
      item?.eventLoc.match(regex) ||
      item?.userName.match(regex)
    ) {
      return item;
    }
  });
}

export function rimeScreeningFeedSearch(value, list) {
  let regex = new RegExp(value, 'gi');

  return list.filter((item, idx) => {
    if (item?.eventTitle.match(regex) || item?.eventLoc.match(regex)) {
      return item;
    }
  });
}

export function searchRimeEvents(value, list) {
  let regex = new RegExp(value, 'gi');

  return list.filter((item, idx) => {
    return item.eventTitle.match(regex);
  });
}

export function searchGodrejEvents(value, list) {
  let regex = new RegExp(value, 'gi');

  return list.filter((item, idx) => {
    for (let k in item) {
      let stringItem = item[k].toString();
      if (stringItem.match(regex)) {
        return item;
      }
    }
  });
}

export function debounce(func, delay) {
  return function() {
    let timeout;
    let context = this;
    let args = arguments;

    let later = function() {
      func.apply(context, args);
    };

    clearTimeout(timeout);

    timeout = setTimeout(later, delay);
  };
}

export function checkDuplicateKeys(arr, item) {
  let hasDuplicateIds = false;

  if (arr.length > 0) {
    arr.forEach(el => {
      if (el.eventId === item.eventId) {
        hasDuplicateIds = true;
      }
    });
  } else {
    hasDuplicateIds = false;
  }

  return hasDuplicateIds;
}

export function sortBy(data, type, sortBy) {
  switch (type) {
    case 'confirmed':
      return data.sort((b, a) => {
        if (sortBy === 'dsc') {
          if (b.totalinfected > a.totalinfected) {
            return -1;
          } else {
            return 1;
          }
        } else {
          if (b.totalinfected > a.totalinfected) {
            return 1;
          } else {
            return -1;
          }
        }
      });
    case 'death':
      return data.sort((b, a) => {
        if (sortBy === 'dsc') {
          if (b.totaldeaths > a.totaldeaths) {
            return -1;
          } else {
            return 1;
          }
        } else {
          if (b.totaldeaths > a.totaldeaths) {
            return 1;
          } else {
            return -1;
          }
        }
      });
    case 'recovered':
      return data.sort((b, a) => {
        if (sortBy === 'dsc') {
          if (b.totalrecoverd > a.totalrecoverd) {
            return -1;
          } else {
            return 1;
          }
        } else {
          if (b.totalrecoverd > a.totalrecoverd) {
            return 1;
          } else {
            return -1;
          }
        }
      });
    case 'state':
      return data.sort((b, a) => {
        if (sortBy === 'dsc') {
          if (b.statename > a.statename) {
            return -1;
          } else {
            return 1;
          }
        } else {
          if (b.statename > a.statename) {
            return 1;
          } else {
            return -1;
          }
        }
      });
    case 'city':
      return data.sort((b, a) => {
        if (sortBy === 'dsc') {
          if (b.statename > a.cityname) {
            return -1;
          } else {
            return 1;
          }
        } else {
          if (b.statename > a.cityname) {
            return 1;
          } else {
            return -1;
          }
        }
      });
    default:
      console.log('errr');
      break;
  }
}

export function delayMeBy(func, interval, pause) {
  console.log(pause, ' << pause');
  if (!pause) {
    func();
  }
  // func();
  // setInterval(() => func(), interval);

  const clearTimeout = setInterval(() => {
    if (!pause) {
      func();
    }
  }, interval);

  if (pause) {
    clearInterval(clearTimeout);
  }
}

export function sortByCoverage(data, type, sortBy) {
  switch (type) {
    case 'country':
      return data.sort((b, a) => {
        if (sortBy === 'dsc') {
          if (b.id > a.id) {
            return -1;
          } else {
            return 1;
          }
        } else {
          if (b.id > a.id) {
            return 1;
          } else {
            return -1;
          }
        }
      });
    case 'population':
      return data.sort((b, a) => {
        if (sortBy === 'dsc') {
          if (b.countryratingpopulation > a.countryratingpopulation) {
            return -1;
          } else {
            return 1;
          }
        } else {
          if (b.countryratingpopulation > a.countryratingpopulation) {
            return 1;
          } else {
            return -1;
          }
        }
      });
    case 'gdp':
      return data.sort((b, a) => {
        if (sortBy === 'dsc') {
          if (b.countryratinggdp > a.countryratinggdp) {
            return -1;
          } else {
            return 1;
          }
        } else {
          if (b.countryratinggdp > a.countryratinggdp) {
            return 1;
          } else {
            return -1;
          }
        }
      });
    case 'total':
      return data.sort((b, a) => {
        if (sortBy === 'dsc') {
          if (b.totalevents > a.totalevents) {
            return -1;
          } else {
            return 1;
          }
        } else {
          if (b.totalevents > a.totalevents) {
            return 1;
          } else {
            return -1;
          }
        }
      });

    default:
      console.log('errr');
      break;
  }
}

export function sortByCountryAnalysis(data, type, sortBy) {
  switch (type) {
    case 'country':
      return data.sort((b, a) => {
        if (sortBy === 'dsc') {
          if (b.id > a.id) {
            return -1;
          } else {
            return 1;
          }
        } else {
          if (b.id > a.id) {
            return 1;
          } else {
            return -1;
          }
        }
      });
    case 'riskTrackerAlerts':
      return data.sort((b, a) => {
        if (sortBy === 'dsc') {
          if (b.riskTrackerNumber > a.riskTrackerNumber) {
            return -1;
          } else {
            return 1;
          }
        } else {
          if (b.riskTrackerNumber > a.riskTrackerNumber) {
            return 1;
          } else {
            return -1;
          }
        }
      });
    case 'rimeAlerts':
      return data.sort((b, a) => {
        if (sortBy === 'dsc') {
          if (b.rimeNumber > a.rimeNumber) {
            return -1;
          } else {
            return 1;
          }
        } else {
          if (b.rimeNumber > a.rimeNumber) {
            return 1;
          } else {
            return -1;
          }
        }
      });
    case 'crucial':
      return data.sort((b, a) => {
        if (sortBy === 'dsc') {
          if (b.crucial > a.crucial) {
            return -1;
          } else {
            return 1;
          }
        } else {
          if (b.crucial > a.crucial) {
            return 1;
          } else {
            return -1;
          }
        }
      });
    case 'warning':
      return data.sort((b, a) => {
        if (sortBy === 'dsc') {
          if (b.warning > a.warning) {
            return -1;
          } else {
            return 1;
          }
        } else {
          if (b.warning > a.warning) {
            return 1;
          } else {
            return -1;
          }
        }
      });
    case 'notificaton':
      return data.sort((b, a) => {
        if (sortBy === 'dsc') {
          if (b.notificaton > a.notificaton) {
            return -1;
          } else {
            return 1;
          }
        } else {
          if (b.notificaton > a.notificaton) {
            return 1;
          } else {
            return -1;
          }
        }
      });
    case 'highrelevency':
      return data.sort((b, a) => {
        if (sortBy === 'dsc') {
          if (b.highrelevency > a.highrelevency) {
            return -1;
          } else {
            return 1;
          }
        } else {
          if (b.highrelevency > a.highrelevency) {
            return 1;
          } else {
            return -1;
          }
        }
      });
    case 'mediumrelevency':
      return data.sort((b, a) => {
        if (sortBy === 'dsc') {
          if (b.mediumrelevency > a.mediumrelevency) {
            return -1;
          } else {
            return 1;
          }
        } else {
          if (b.mediumrelevency > a.mediumrelevency) {
            return 1;
          } else {
            return -1;
          }
        }
      });
    case 'lowrelevency':
      return data.sort((b, a) => {
        if (sortBy === 'dsc') {
          if (b.lowrelevency > a.lowrelevency) {
            return -1;
          } else {
            return 1;
          }
        } else {
          if (b.lowrelevency > a.lowrelevency) {
            return 1;
          } else {
            return -1;
          }
        }
      });

    default:
      console.log('errr');
      break;
  }
}

export function sortByMissedLinks(data, type, sortBy) {
  switch (type) {
    case 'id':
      return data.sort((b, a) => {
        if (sortBy === 'dsc') {
          if (b.id > a.id) {
            return -1;
          } else {
            return 1;
          }
        } else {
          if (b.id > a.id) {
            return 1;
          } else {
            return -1;
          }
        }
      });
    case 'title':
      return data.sort((b, a) => {
        if (sortBy === 'dsc') {
          if (b.title > a.title) {
            return -1;
          } else {
            return 1;
          }
        } else {
          if (b.title > a.title) {
            return 1;
          } else {
            return -1;
          }
        }
      });
    case 'link':
      return data.sort((b, a) => {
        if (sortBy === 'dsc') {
          if (b.link > a.link) {
            return -1;
          } else {
            return 1;
          }
        } else {
          if (b.link > a.link) {
            return 1;
          } else {
            return -1;
          }
        }
      });
    case 'date':
      return data.sort((b, a) => {
        if (sortBy === 'dsc') {
          if (b.date > a.date) {
            return -1;
          } else {
            return 1;
          }
        } else {
          if (b.date > a.date) {
            return 1;
          } else {
            return -1;
          }
        }
      });
    default:
      console.log('errr');
      break;
  }
}

export function hasLength(itemArr) {
  return itemArr && itemArr.length > 0 ? itemArr : [];
}

export function generateRandomColors() {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

export const countriesObjToArr = countriesArr => {
  return countriesArr.map(selectedCountry => {
    return {
      value: selectedCountry.country,
      label: selectedCountry.id,
    };
  });
};

export const getSelectedTopic = ({ id, allTopics }) => {
  return allTopics.filter(item => item.value === id)[0] || {};
};

export function localDateTime(dateTime) {
  const dateString = dateTime;
  const userOffset = new Date().getTimezoneOffset();
  const localDate = new Date(dateString);
  const utcDate = new Date(localDate.getTime() - userOffset);
  return utcDate;
}
