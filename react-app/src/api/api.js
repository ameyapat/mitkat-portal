import { uploadApi, fetchApi } from '../helpers/http/fetch';
import { API_ROUTES, rimeUrl } from '../helpers/http/apiRoutes';
import { toggleToast } from '../actions/index';
import Cookies from 'universal-cookie';
import store from '../store';
const axios = require('axios');

export const uploadImage = data => {
  const cookies = new Cookies();
  const authToken = cookies.get('authToken');
  const { image, eventId, imageId } = data;
  const formData = new FormData();
  formData.append('image', image);
  formData.append('eventid', eventId);
  formData.append('imageid', imageId);

  const params = {
    url: `${API_ROUTES.uploadImageGodrej}`,
    reqObj: {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${authToken}`,
      },
      body: formData,
    },
  };

  uploadApi(params.url, params.reqObj).then(res => {
    if (res.success) {
      let toastObj = {
        toastMsg: 'Image Added Successfully!',
        showToast: true,
      };
      store.dispatch(toggleToast(toastObj));
    } else {
      let toastObj = {
        toastMsg: 'Something went wrong!',
        showToast: true,
      };
      store.dispatch(toggleToast(toastObj));
    }
  });
};

export const getEmergingRisks = data => {
  const cookies = new Cookies();
  const authToken = cookies.get('authToken');

  return new Promise((resolve, reject) => {
    const reqObj = {
      url: API_ROUTES.getEmergingRisks,
      isAuth: true,
      authToken,
      method: 'POST',
      data,
      showToggle: true,
    };

    fetchApi(reqObj)
      .then(data => {
        if (data.status === '200') {
          resolve(data.output);
        } else {
          reject('Error!');
        }
      })
      .catch(e => reject(e));
  });
};

export const deleteGodrejImage = data => {
  const cookies = new Cookies();
  const authToken = cookies.get('authToken');
  return new Promise((res, rej) => {
    // deleteImageGodrej
    const { eventId, imageId } = data;
    const reqData = {
      eventid: eventId,
      imageid: imageId,
    };
    const reqObj = {
      url: `${API_ROUTES.deleteImageGodrej}`,
      reqObj: {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${authToken}`,
        },
        data: reqData,
      },
    };

    fetchApi(reqObj)
      .then(data => {
        if (data.status === '200') {
          res(data.output);
        } else {
          rej('Error! deleting image');
        }
      })
      .catch(e => rej(e));
  });
};

export const getRimeSavedFilters = () => {
  const cookies = new Cookies();
  const authToken = cookies.get('authToken');
  return new Promise((resolve, reject) => {
    let reqObj = {
      url: `${API_ROUTES.getRimeSavedFilters}`,
      method: 'GET',
      isAuth: true,
      authToken,
      showToggle: false,
    };
    fetchApi(reqObj)
      .then(response => {
        resolve(response);
      })
      .catch(e => reject(e));
  });
};

export const saveRimeFilters = reqData => {
  const cookies = new Cookies();
  const authToken = cookies.get('authToken');
  return new Promise((resolve, reject) => {
    let reqObj = {
      url: `${API_ROUTES.getRimeSavedFilters}`,
      method: 'POST',
      isAuth: true,
      authToken,
      showToggle: false,
      data: reqData,
    };
    fetchApi(reqObj)
      .then(response => {
        resolve(response);
      })
      .catch(e => reject(e));
  });
};

export const getRimeFeed = reqData => {
  const cookies = new Cookies();
  const authToken = cookies.get('authToken');
  return new Promise((resolve, reject) => {
    let reqObj = {
      url: `${API_ROUTES.rimeTimeRange}`,
      method: 'POST',
      isAuth: true,
      authToken,
      showToggle: false,
      data: reqData,
    };
    fetchApi(reqObj)
      .then(response => {
        resolve(response);
      })
      .catch(e => reject(e));
  });
};

export const getRimeEventSearchFeed = reqData => {
  const cookies = new Cookies();
  const authToken = cookies.get('authToken');
  return new Promise((resolve, reject) => {
    let reqObj = {
      url: `${API_ROUTES.rimeDateRange}`,
      method: 'POST',
      isAuth: true,
      authToken,
      showToggle: false,
      data: reqData,
    };
    fetchApi(reqObj)
      .then(response => {
        resolve(response);
      })
      .catch(e => reject(e));
  });
};

export const downloadRimeEventSearchExcel = reqData => {
  const cookies = new Cookies();
  const authToken = cookies.get('authToken');
  let reqObj = {
    url: `${API_ROUTES.rimeDateRange}`,
    method: 'POST',
    isAuth: true,
    authToken,
    data: reqData,
    isBlob: true,
    showToggle: true,
  };

  fetchApi(reqObj)
    .then(blobObj => {
      let a = document.createElement('a');
      document.body.appendChild(a);
      let objUrl = new Blob([blobObj], { type: 'application/vnd.ms-excel' });
      let url = window.URL.createObjectURL(objUrl);
      a.href = url;
      a.download = 'RimeEvents.xls';
      a.click();
      window.URL.revokeObjectURL(url);
    })
    .catch(e => console.log(e));
};

export const getRimeSubscriptions = () => {
  const cookies = new Cookies();
  const authToken = cookies.get('authToken');
  return new Promise((resolve, reject) => {
    let reqObj = {
      url: API_ROUTES.getRimeSubscriptions,
      method: 'GET',
      isAuth: true,
      authToken,
    };
    fetchApi(reqObj)
      .then(response => {
        resolve(response);
      })
      .catch(e => reject(e));
  });
};

export const deleteRimeSubscriptions = id => {
  const cookies = new Cookies();
  const authToken = cookies.get('authToken');
  return new Promise((resolve, reject) => {
    let reqObj = {
      url: `${API_ROUTES.setRimeSubscriptions}/${id}`,
      method: 'DELETE',
      isAuth: true,
      authToken,
    };
    fetchApi(reqObj)
      .then(response => {
        resolve(response);
      })
      .catch(e => {
        let toastObj = {
          toastMsg: 'Error deleting Subscription',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      });
  });
};

export const createRimeSubscriptions = reqData => {
  const cookies = new Cookies();
  const authToken = cookies.get('authToken');
  return new Promise((resolve, reject) => {
    let reqObj = {
      url: API_ROUTES.setRimeSubscriptions,
      method: 'POST',
      isAuth: true,
      authToken,
      showToggle: false,
      data: reqData,
    };
    fetchApi(reqObj)
      .then(response => {
        resolve(response);
      })
      .catch(e => reject(e));
  });
};

export const getEachRimeSubscriptions = id => {
  const cookies = new Cookies();
  const authToken = cookies.get('authToken');
  return new Promise((resolve, reject) => {
    let reqObj = {
      url: `${API_ROUTES.setRimeSubscriptions}/${id}`,
      method: 'GET',
      isAuth: true,
      authToken,
    };
    fetchApi(reqObj)
      .then(response => {
        resolve(response);
      })
      .catch(e => {
        let toastObj = {
          toastMsg: 'Error Fetching Subscription',
          showToast: true,
        };
        store.dispatch(toggleToast(toastObj));
      });
  });
};

export const updateRimeSubscriptions = (id, reqData) => {
  const cookies = new Cookies();
  const authToken = cookies.get('authToken');
  return new Promise((resolve, reject) => {
    let reqObj = {
      url: `${API_ROUTES.setRimeSubscriptions}/${id}`,
      method: 'POST',
      isAuth: true,
      authToken,
      showToggle: false,
      data: reqData,
    };
    fetchApi(reqObj)
      .then(response => {
        resolve(response);
      })
      .catch(e => reject(e));
  });
};

export const showEventDetail = eventId => {
  const cookies = new Cookies();
  const authToken = cookies.get('authToken');
  let reqObj = {
    method: 'POST',
    url: `${API_ROUTES.getEventDetails}/${eventId}`,
    isAuth: true,
    authToken: authToken,
    showToggle: true,
  };

  return new Promise((resolve, reject) => {
    fetchApi(reqObj)
      .then(data => {
        resolve(data);
      })
      .catch(e => reject(e));
  });
};

export const getRiskAlerts = reqPayload => {
  return new Promise((res, rej) => {
    fetchApi(reqPayload)
      .then(data => {
        res(data);
      })
      .catch(e => {
        rej(e);
      });
  });
};

export function resetPassword(payload) {
  return new Promise((resolve, reject) => {
    fetchApi(payload)
      .then(data => {
        resolve(data);
      })
      .catch(e => {
        reject(e);
      });
  });
}

export function getAllTopics() {
  const cookies = new Cookies();
  const authToken = cookies.get('authToken');

  const reqObj = {
    method: 'GET',
    url: API_ROUTES.getAllTopics,
    isAuth: true,
    authToken: authToken,
    showToggle: true,
  };

  return new Promise((res, rej) => {
    fetchApi(reqObj)
      .then(data => {
        res(data);
      })
      .catch(e => rej(e));
  });
}

export function getLiveTopics() {
  const cookies = new Cookies();
  const authToken = cookies.get('authToken');

  const reqObj = {
    method: 'GET',
    url: API_ROUTES.getLiveTopics,
    isAuth: true,
    authToken,
    showToggle: true,
  };

  return new Promise((res, rej) => {
    fetchApi(reqObj)
      .then(data => {
        res(data);
      })
      .catch(e => rej(e));
  });
}

export function getRiskCategories() {
  const cookies = new Cookies();
  const authToken = cookies.get('authToken');
  const reqObj = {
    url: API_ROUTES.fetchRiskCategory,
    isAuth: true,
    authToken,
    method: 'POST',
    showToggle: true,
  };

  return new Promise((res, rej) => {
    fetchApi(reqObj)
      .then(data => {
        let categories = [];
        data.map(i => {
          let riskCategoryObj = {};
          riskCategoryObj['value'] = i.id;
          riskCategoryObj['label'] = i.riskcategory;
          categories.push(riskCategoryObj);
        });
        res(categories);
      })
      .catch(e => rej(e));
  });
}

export function getRiskLevels() {
  const cookies = new Cookies();
  const authToken = cookies.get('authToken');
  const reqObj = {
    url: API_ROUTES.fetchRiskLevel,
    isAuth: true,
    authToken,
    method: 'POST',
    showToggle: true,
  };

  return new Promise((res, rej) => {
    fetchApi(reqObj)
      .then(data => {
        let levels = [];
        data.map(i => {
          let riskLevelObj = {};
          riskLevelObj['value'] = i.id;
          riskLevelObj['label'] = i.level;
          levels.push(riskLevelObj);
        });
        res(levels);
      })
      .catch(e => rej(e));
  });
}

export function updateTopic(reqData) {
  const cookies = new Cookies();
  const authToken = cookies.get('authToken');

  const reqObj = {
    method: 'POST',
    url: API_ROUTES.addEditTopic,
    isAuth: true,
    authToken,
    showToggle: true,
    data: reqData,
  };

  return new Promise((res, rej) => {
    fetchApi(reqObj)
      .then(data => {
        res(data);
      })
      .catch(e => rej(e));
  });
}

export const downloadEventDetailsPdf = id => {
  const cookies = new Cookies();
  const authToken = cookies.get('authToken');

  let reqObj = {
    url: `${API_ROUTES.downloadEventPdf}/${id}`,
    isAuth: true,
    authToken,
    isBlob: true,
    method: 'POST',
    showToggle: true,
  };

  fetchApi(reqObj).then(blobObj => {
    var a = document.createElement('a');
    document.body.appendChild(a);
    let objUrl = new Blob([blobObj], { type: 'application/pdf' });
    let url = window.URL.createObjectURL(objUrl);
    a.href = url;
    a.download = 'mitkat-report';
    a.click();
    window.URL.revokeObjectURL(url);
  });
};
