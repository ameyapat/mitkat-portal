export const getEventList = state => {
  return state && state.mapState && state.mapState.eventDetails;
};

export const getDefaultEventDetails = state => {
  return state && state.mapState && state.mapState.defaultEventDetails;
};
