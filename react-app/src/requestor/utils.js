import Cookies from 'universal-cookie';

export const generateReqHeaders = ({
  method = 'GET',
  url,
  reqData,
  isBlob = false,
}) => {
  const cookies = new Cookies();
  const authToken = cookies.get('authToken');

  const reqObj = {
    isAuth: true,
    authToken,
    method,
    url,
    showToggle: true,
    ...(reqData && { data: reqData }),
    isBlob,
  };

  return reqObj;
};
