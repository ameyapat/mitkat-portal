import { fetchApi } from '../../helpers/http/fetch';
import { API_ROUTES } from '../../helpers/http/apiRoutes';
import { generateReqHeaders } from '../utils';

export const getTopicAnalysis = topicId => {
  const reqObj = generateReqHeaders({
    url: `${API_ROUTES.topicAnalysis}/${topicId}`,
  });
  return new Promise((res, rej) => {
    fetchApi(reqObj)
      .then(response => {
        res(response);
      })
      .catch(e => rej(e));
  });
};
