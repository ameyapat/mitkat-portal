import Cookies from 'universal-cookie';
import { fetchApi } from '../../helpers/http/fetch';
import { API_ROUTES } from '../../helpers/http/apiRoutes';
import { generateReqHeaders } from '../utils';

export const getCountryListBarclays = () => {
  const reqObj = generateReqHeaders({ url: API_ROUTES.getCountryListBarclays });

  return new Promise((res, rej) => {
    fetchApi(reqObj)
      .then(response => {
        res(response);
      })
      .catch(e => rej(e));
  });
};

export const getCityListBarclays = () => {
  const reqObj = generateReqHeaders({ url: API_ROUTES.getCityListBarclays });

  return new Promise((res, rej) => {
    fetchApi(reqObj)
      .then(response => {
        res(response);
      })
      .catch(e => rej(e));
  });
};

export const getAssetListBarclays = () => {
  const reqObj = generateReqHeaders({ url: API_ROUTES.getAssetListBarclays });

  return new Promise((res, rej) => {
    fetchApi(reqObj)
      .then(response => {
        res(response);
      })
      .catch(e => rej(e));
  });
};

export const getRiskLevelBarclays = () => {
  const reqObj = generateReqHeaders({ url: API_ROUTES.getRiskLevelBarclays });

  return new Promise((res, rej) => {
    fetchApi(reqObj)
      .then(response => {
        res(response);
      })
      .catch(e => rej(e));
  });
};

export const getRiskCategoryBarclays = () => {
  const reqObj = generateReqHeaders({
    url: API_ROUTES.getRiskCategoryBarclays,
  });

  return new Promise((res, rej) => {
    fetchApi(reqObj)
      .then(response => {
        res(response);
      })
      .catch(e => rej(e));
  });
};

export const getSubRiskCategoryBarclays = id => {
  const reqObj = generateReqHeaders({
    url: `${API_ROUTES.getSubRiskCategoryBarclays}/${id}`,
  });

  return new Promise((res, rej) => {
    fetchApi(reqObj)
      .then(response => {
        res(response);
      })
      .catch(e => rej(e));
  });
};

export const getEventListBarclays = date => {
  const reqObj = generateReqHeaders({
    url: `${API_ROUTES.getEventListBarclays}/${date}`,
  });

  return new Promise((res, rej) => {
    fetchApi(reqObj)
      .then(response => {
        res(response);
      })
      .catch(e => rej(e));
  });
};

export const getEventBarclays = id => {
  const reqObj = generateReqHeaders({
    url: `${API_ROUTES.getEventBarclays}/${id}`,
  });

  return new Promise((res, rej) => {
    fetchApi(reqObj)
      .then(response => {
        res(response);
      })
      .catch(e => rej(e));
  });
};

export const deleteEventBarclays = id => {
  const reqObj = generateReqHeaders({
    url: `${API_ROUTES.deleteEventBarclays}/${id}`,
  });

  return new Promise((res, rej) => {
    fetchApi(reqObj)
      .then(response => {
        res(response);
      })
      .catch(e => rej(e));
  });
};

export const updateEventBarclays = reqData => {
  const reqObj = generateReqHeaders({
    url: API_ROUTES.updateEventBarclays,
    method: 'POST',
    reqData,
  });

  return new Promise((res, rej) => {
    fetchApi(reqObj)
      .then(response => {
        res(response);
      })
      .catch(e => rej(e));
  });
};

export const getDashboardInfoBarclays = reqData => {
  const reqObj = generateReqHeaders({
    url: API_ROUTES.getDashboardDataBarclays,
    method: 'POST',
    reqData,
  });

  return new Promise((res, rej) => {
    fetchApi(reqObj)
      .then(response => {
        res(response);
      })
      .catch(e => rej(e));
  });
};

export const downloadEventsBarclays = reqData => {
  const reqObj = generateReqHeaders({
    url: API_ROUTES.downloadEventsBarclays,
    method: 'POST',
    reqData,
    isBlob: true,
  });

  return new Promise((res, rej) => {
    fetchApi(reqObj)
      .then(blobObj => {
        let a = document.createElement('a');
        document.body.appendChild(a);
        let objUrl = new Blob([blobObj], { type: 'application/vnd.ms-excel' });
        let url = window.URL.createObjectURL(objUrl);
        a.href = url;
        a.download = 'event-report-barclays-excel.xls';
        a.click();
        window.URL.revokeObjectURL(url);
      })
      .catch(e => rej(e));
  });
};

export const getDashEventDetailsBarclays = id => {
  const reqObj = generateReqHeaders({
    url: `${API_ROUTES.getDashboardEventDetailsBarclays}/${id}`,
  });

  return new Promise((res, rej) => {
    fetchApi(reqObj)
      .then(response => {
        res(response);
      })
      .catch(e => rej(e));
  });
};

export const downloadPdfBarclays = id => {
  const reqObj = generateReqHeaders({
    url: `${API_ROUTES.downloadPdfBarclays}/${id}`,
    isBlob: true,
  });

  fetchApi(reqObj)
    .then(blobObj => {
      let a = document.createElement('a');
      document.body.appendChild(a);
      let objUrl = new Blob([blobObj], { type: 'application/pdf' });
      let url = window.URL.createObjectURL(objUrl);
      a.href = url;
      a.download = 'report-barclays-pdf';
      a.click();
      window.URL.revokeObjectURL(url);
    })
    .catch(e => console.log(e));
};
