import { fetchApi } from '../../helpers/http/fetch';
import { API_ROUTES } from '../../helpers/http/apiRoutes';
import Cookies from 'universal-cookie';
import store from '../../store';
import { setPartnerDetails } from '../../actions';

export const getPartnerDetails = token => {
  return new Promise((res, rej) => {
    let reqObj = {
      isAuth: true,
      authToken: token,
      url: API_ROUTES.getPartnerDetails,
      method: 'GET',
      showToggle: true,
    };
    const cookies = new Cookies();
    fetchApi(reqObj)
      .then(data => {
        if (data) {
          let d = new Date();
          d.setTime(d.getTime() + 24 * 60 * 60 * 1000);
          cookies.set('partnerId', data.partnerId, {
            path: '/',
            expires: d,
          });

          store.dispatch(setPartnerDetails(data));
          res(data);
        }
      })
      .catch(e => rej(e));
  });
};
