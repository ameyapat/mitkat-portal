import { fetchApi } from '../../helpers/http/fetch';
import { API_ROUTES } from '../../helpers/http/apiRoutes';
import { generateReqHeaders } from '../utils';

export const getRiskExposureData = days => {
  const reqObj = generateReqHeaders({
    url: `${API_ROUTES.getRiskExposureData}/${days}`,
  });
  return new Promise((res, rej) => {
    fetchApi(reqObj)
      .then(response => {
        res(response);
      })
      .catch(e => rej(e));
  });
};
