import Cookies from 'universal-cookie';
import { fetchApi } from '../../helpers/http/fetch';
import { websiteEndPoint, API_ROUTES } from '../../helpers/http/apiRoutes';

const generateReqHeaders = ({
  method = 'GET',
  url,
  reqData,
  isBlob = false,
  newAuthToken,
}) => {
  const cookies = new Cookies();
  const authToken = cookies.get('authToken');

  const reqObj = {
    isAuth: true,
    authToken: authToken || newAuthToken,
    method,
    url,
    showToggle: true,
    ...(reqData && { data: reqData }),
    isBlob,
  };

  return reqObj;
};

export const authWebUser = reqData => {
  const reqObj = generateReqHeaders({
    url: websiteEndPoint.login,
    method: 'POST',
    reqData,
    isAuth: false,
  });
  const cookies = new Cookies();

  return new Promise((res, rej) => {
    fetchApi(reqObj)
      .then(response => {
        let d = new Date();
        d.setTime(d.getTime() + 24 * 60 * 60 * 1000);

        cookies.set('isCovidDashAuth', response.success, {
          path: '/',
          expires: d,
        });

        res(response);
      })
      .catch(e => rej(e));
  });
};

export const getSubscriptions = token => {
  const reqObj = generateReqHeaders({
    url: API_ROUTES.checkSubscriptions,
    method: 'GET',
    isAuth: true,
    newAuthToken: token,
  });

  return new Promise((res, rej) => {
    fetchApi(reqObj)
      .then(response => {
        res(response);
      })
      .catch(e => rej(e));
  });
};

export const updateSettings = reqData => {
  const reqObj = generateReqHeaders({
    url: API_ROUTES.updateSettings,
    method: 'POST',
    isAuth: true,
    reqData,
  });
  return new Promise((res, rej) => {
    fetchApi(reqObj)
      .then(response => {
        res(response);
      })
      .catch(e => rej(e));
  });
};

export const getRimeRegions = () => {
  const reqObj = generateReqHeaders({
    url: API_ROUTES.rimeRegions,
    method: 'GET',
    isAuth: true,
  });

  return new Promise((res, rej) => {
    fetchApi(reqObj)
      .then(data => {
        let rimeRegions = [];
        data.map(i => {
          let rimeRegionsObj = {};
          rimeRegionsObj['value'] = i.id;
          rimeRegionsObj['label'] = i.regionName;
          rimeRegions.push(rimeRegionsObj);
        });
        res(rimeRegions);
      })
      .catch(e => rej(e));
  });
};

export const getUsersInAdmin = () => {
  const reqObj = generateReqHeaders({
    url: API_ROUTES.getUsersInAdmin,
    method: 'POST',
    isAuth: true,
  });

  return new Promise((resolve, reject) => {
    fetchApi(reqObj)
      .then(data => {
        resolve(data);
      })
      .catch(e => reject(e));
  });
};

export const getTravelCountryList = () => {
  const reqObj = generateReqHeaders({
    url: API_ROUTES.travelCountryList,
    method: 'GET',
    isAuth: true,
  });

  return new Promise((resolve, reject) => {
    fetchApi(reqObj)
      .then(data => {
        resolve(data);
      })
      .catch(e => reject(e));
  });
};

export const getClientTrackingId = (trackingId = 'RINTP7840813') => {
  const reqObj = generateReqHeaders({
    url: `${API_ROUTES.eventClientTrackingId}/${trackingId}`,
    method: 'GET',
    isAuth: true,
  });

  return new Promise((resolve, reject) => {
    fetchApi(reqObj)
      .then(data => {
        resolve(data);
      })
      .catch(e => reject(e));
  });
};

export const downloadArchives = reqData => {
  const reqObj = generateReqHeaders({
    url: API_ROUTES.downloadArchives,
    method: 'POST',
    isAuth: true,
    isBlob: true,
    reqData,
  });

  return new Promise((resolve, reject) => {
    fetchApi(reqObj)
      .then(data => {
        resolve(data);
      })
      .catch(e => reject(e));
  });
};

export const fetchLocationAnalysisData = reqData => {
  const reqObj = generateReqHeaders({
    url: API_ROUTES.getLocationAnalysis,
    method: 'POST',
    isAuth: true,
    reqData,
  });

  return new Promise((resolve, reject) => {
    fetchApi(reqObj)
      .then(data => {
        resolve(data);
      })
      .catch(e => reject(e));
  });
};

export const fetchClientLocationAnalysisData = reqData => {
  const reqObj = generateReqHeaders({
    url: `${API_ROUTES.getClientLocationAnalysis}/${reqData.locationid}`,
    method: 'GET',
    isAuth: true,
  });

  return new Promise((resolve, reject) => {
    fetchApi(reqObj)
      .then(data => {
        resolve(data);
      })
      .catch(e => reject(e));
  });
};

export const fetchClientLocationsData = () => {
  const reqObj = generateReqHeaders({
    url: API_ROUTES.getClientLocations,
    method: 'POST',
    isAuth: true,
  });

  return new Promise((resolve, reject) => {
    fetchApi(reqObj)
      .then(data => {
        resolve(data);
      })
      .catch(e => reject(e));
  });
};

export const addToWatchList = id => {
  const reqObj = generateReqHeaders({
    url: `${API_ROUTES.addWatchList}/${id}`,
    method: 'GET',
    isAuth: true,
  });

  return new Promise((resolve, reject) => {
    fetchApi(reqObj)
      .then(data => {
        resolve(data);
      })
      .catch(e => reject(e));
  });
};

export const removeFromWatchList = id => {
  const reqObj = generateReqHeaders({
    url: `${API_ROUTES.removeWatchList}/${id}`,
    method: 'GET',
    isAuth: true,
  });

  return new Promise((resolve, reject) => {
    fetchApi(reqObj)
      .then(data => {
        resolve(data);
      })
      .catch(e => reject(e));
  });
};

export const getWatchList = () => {
  const reqObj = generateReqHeaders({
    url: API_ROUTES.getWatchList,
    method: 'GET',
    isAuth: true,
  });

  return new Promise((resolve, reject) => {
    fetchApi(reqObj)
      .then(data => {
        resolve(data);
      })
      .catch(e => reject(e));
  });
};

export const getClientUserList = () => {
  const reqObj = generateReqHeaders({
    url: API_ROUTES.clientUserList,
    method: 'GET',
    isAuth: true,
  });

  return new Promise((resolve, reject) => {
    fetchApi(reqObj)
      .then(data => {
        resolve(data);
      })
      .catch(e => reject(e));
  });
};

export const setNewClientUser = reqData => {
  const reqObj = generateReqHeaders({
    url: API_ROUTES.addClientUser,
    method: 'POST',
    isAuth: true,
    reqData,
  });

  return new Promise((resolve, reject) => {
    fetchApi(reqObj)
      .then(data => {
        resolve(data);
      })
      .catch(e => reject(e));
  });
};

export const deleteClientUser = id => {
  const reqObj = generateReqHeaders({
    url: `${API_ROUTES.deleteClientUser}/${id}`,
    method: 'GET',
    isAuth: true,
  });

  return new Promise((resolve, reject) => {
    fetchApi(reqObj)
      .then(data => {
        resolve(data);
      })
      .catch(e => reject(e));
  });
};

export const getClientLocationList = () => {
  const reqObj = generateReqHeaders({
    url: API_ROUTES.clientLocationList,
    method: 'GET',
    isAuth: true,
  });

  return new Promise((resolve, reject) => {
    fetchApi(reqObj)
      .then(data => {
        resolve(data);
      })
      .catch(e => reject(e));
  });
};

export const setNewClientLocation = reqData => {
  const reqObj = generateReqHeaders({
    url: API_ROUTES.addClientLocation,
    method: 'POST',
    isAuth: true,
    reqData,
  });

  return new Promise((resolve, reject) => {
    fetchApi(reqObj)
      .then(data => {
        resolve(data);
      })
      .catch(e => reject(e));
  });
};

export const deleteClientLocation = id => {
  const reqObj = generateReqHeaders({
    url: `${API_ROUTES.deleteClientLocation}/${id}`,
    method: 'POST',
    isAuth: true,
  });

  return new Promise((resolve, reject) => {
    fetchApi(reqObj)
      .then(data => {
        resolve(data);
      })
      .catch(e => reject(e));
  });
};
