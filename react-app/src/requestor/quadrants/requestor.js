import { fetchApi } from '../../helpers/http/fetch';
import { API_ROUTES } from '../../helpers/http/apiRoutes';
import { generateReqHeaders } from '../utils';

export const getQuadrantEventList = () => {
  const reqObj = generateReqHeaders({ url: API_ROUTES.quadrantEventList });

  return new Promise((res, rej) => {
    fetchApi(reqObj)
      .then(response => {
        res(response);
      })
      .catch(e => rej(e));
  });
};

export const getQuadrantEventDetails = id => {
  const reqObj = generateReqHeaders({
    url: `${API_ROUTES.quadrantEventDetails}/${id}`,
  });

  return new Promise((res, rej) => {
    fetchApi(reqObj)
      .then(response => {
        res(response);
      })
      .catch(e => rej(e));
  });
};

export const updateQuadrantDetails = reqData => {
  const reqObj = generateReqHeaders({
    url: API_ROUTES.updateQuadrantDetails,
    method: 'POST',
    reqData,
  });

  return new Promise((res, rej) => {
    fetchApi(reqObj)
      .then(response => {
        res(response);
      })
      .catch(e => rej(e));
  });
};

export const deleteQuadrantDetails = id => {
  const reqObj = generateReqHeaders({
    url: `${API_ROUTES.deleteQuadrantDetails}/${id}`,
  });

  return new Promise((res, rej) => {
    fetchApi(reqObj)
      .then(response => {
        res(response);
      })
      .catch(e => rej(e));
  });
};

export const getQuadrants = () => {
  const reqObj = generateReqHeaders({ url: API_ROUTES.getQuadrants });

  return new Promise((res, rej) => {
    fetchApi(reqObj)
      .then(response => {
        res(response);
      })
      .catch(e => rej(e));
  });
};

export const getQuadrantsV1 = () => {
  const reqObj = generateReqHeaders({ url: API_ROUTES.getQuadrantsV1 });

  return new Promise((res, rej) => {
    fetchApi(reqObj)
      .then(response => {
        res(response);
      })
      .catch(e => rej(e));
  });
};

export const getRegions = () => {
  const reqObj = generateReqHeaders({ url: API_ROUTES.getRegions });

  return new Promise((res, rej) => {
    fetchApi(reqObj)
      .then(response => {
        res(response);
      })
      .catch(e => rej(e));
  });
};
