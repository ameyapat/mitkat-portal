import Cookies from 'universal-cookie';
import { fetchApi, uploadApi } from '../../helpers/http/fetch';
import { API_ROUTES } from '../../helpers/http/apiRoutes';

const generateReqHeaders = ({
  method = 'GET',
  url,
  reqData,
  isBlob = false,
}) => {
  const cookies = new Cookies();
  const authToken = cookies.get('authToken');

  const reqObj = {
    isAuth: true,
    authToken,
    method,
    url,
    showToggle: true,
    ...(reqData && { data: reqData }),
    isBlob,
  };

  return reqObj;
};

const generateUploadReqHeaders = ({ method = 'POST', url, formData }) => {
  const cookies = new Cookies();
  const authToken = cookies.get('authToken');

  const reqHeaders = {
    url,
    reqObj: {
      method,
      headers: {
        Authorization: `Bearer ${authToken}`,
      },
      body: formData,
    },
  };

  return reqHeaders;
};

export const getPartnerEvents = () => {
  const reqObj = generateReqHeaders({
    url: API_ROUTES.getPartnerEvents,
  });

  return new Promise((res, rej) => {
    fetchApi(reqObj)
      .then(response => {
        res(response);
      })
      .catch(e => rej(e));
  });
};

export const getPartnerEventById = eventId => {
  const reqObj = generateReqHeaders({
    url: `${API_ROUTES.getPartnerEventById}/${eventId}`,
  });

  return new Promise((res, rej) => {
    fetchApi(reqObj)
      .then(response => {
        res(response);
      })
      .catch(e => rej(e));
  });
};

export const updtePartnerEvent = reqData => {
  const reqObj = generateReqHeaders({
    url: API_ROUTES.updatePartnerEvent,
    method: 'POST',
    reqData,
  });

  return new Promise((res, rej) => {
    fetchApi(reqObj)
      .then(response => {
        res(response);
      })
      .catch(e => rej(e));
  });
};

export const deletePartnerEvent = eventId => {
  const reqObj = generateReqHeaders({
    url: `${API_ROUTES.deletePartnerEventById}/${eventId}`,
  });

  return new Promise((res, rej) => {
    fetchApi(reqObj)
      .then(response => {
        res(response);
      })
      .catch(e => rej(e));
  });
};

export const getCreatorTrackingId = (trackingId = 'RINTP7840813') => {
  const reqObj = generateReqHeaders({
    url: `${API_ROUTES.eventCreatorTrackingId}/${trackingId}`,
    method: 'GET',
    isAuth: true,
  });

  return new Promise((resolve, reject) => {
    fetchApi(reqObj)
      .then(data => {
        resolve(data);
      })
      .catch(e => reject(e));
  });
};

export const getRelevantClientList = (eventId = '') => {
  const reqObj = generateReqHeaders({
    url: `${API_ROUTES.getRelevantClientList}/${eventId}`,
    method: 'GET',
    isAuth: true,
  });

  return new Promise((resolve, reject) => {
    fetchApi(reqObj)
      .then(data => {
        resolve(data);
      })
      .catch(e => reject(e));
  });
};

export const getAdvisoryDetails = (eventId = '') => {
  const reqObj = generateReqHeaders({
    url: `${API_ROUTES.editAdvisory}/${eventId}`,
    method: 'GET',
    isAuth: true,
  });

  return new Promise((resolve, reject) => {
    fetchApi(reqObj)
      .then(data => {
        resolve(data);
      })
      .catch(e => reject(e));
  });
};

export const getSpecialReportDetails = (eventId = '') => {
  const reqObj = generateReqHeaders({
    url: `${API_ROUTES.editSpecialReport}/${eventId}`,
    method: 'GET',
    isAuth: true,
  });

  return new Promise((resolve, reject) => {
    fetchApi(reqObj)
      .then(data => {
        resolve(data);
      })
      .catch(e => reject(e));
  });
};

export const updateSpecialReport = formData => {
  const req = generateUploadReqHeaders({
    url: `${API_ROUTES.updateSpecialReport}`,
    method: 'POST',
    formData,
  });

  return new Promise((resolve, reject) => {
    uploadApi(req.url, req.reqObj)
      .then(data => {
        resolve(data);
      })
      .catch(e => reject(e));
  });
};

export const getRiskReviewDetails = (eventId = '') => {
  const reqObj = generateReqHeaders({
    url: `${API_ROUTES.editRiskReview}/${eventId}`,
    method: 'GET',
    isAuth: true,
  });

  return new Promise((resolve, reject) => {
    fetchApi(reqObj)
      .then(data => {
        resolve(data);
      })
      .catch(e => reject(e));
  });
};

export const editUserRole = reqData => {
  const reqObj = generateReqHeaders({
    url: `${API_ROUTES.editUserRole}`,
    method: 'POST',
    reqData,
  });

  return new Promise((resolve, reject) => {
    fetchApi(reqObj)
      .then(data => {
        resolve(data);
      })
      .catch(e => reject(e));
  });
};

export const updateRiskReview = formData => {
  const req = generateUploadReqHeaders({
    url: `${API_ROUTES.updateSpecialReport}`,
    method: 'POST',
    formData,
  });

  return new Promise((resolve, reject) => {
    uploadApi(req.url, req.reqObj)
      .then(data => {
        resolve(data);
      })
      .catch(e => reject(e));
  });
};

export const getMonthlyDetails = (eventId = '') => {
  const reqObj = generateReqHeaders({
    url: `${API_ROUTES.editNewReport}/${eventId}`,
    method: 'GET',
    isAuth: true,
  });

  return new Promise((resolve, reject) => {
    fetchApi(reqObj)
      .then(data => {
        resolve(data);
      })
      .catch(e => reject(e));
  });
};

export const updateMonthlyReport = formData => {
  const req = generateUploadReqHeaders({
    url: `${API_ROUTES.editNewReport}`,
    method: 'POST',
    formData,
  });

  return new Promise((resolve, reject) => {
    uploadApi(req.url, req.reqObj)
      .then(data => {
        resolve(data);
      })
      .catch(e => reject(e));
  });
};
/* BLOG APIS */
export const getBlogDetails = (reportId = '') => {
  const reqObj = generateReqHeaders({
    url: `${API_ROUTES.editBlog}/${reportId}`,
    method: 'GET',
    isAuth: true,
  });

  return new Promise((resolve, reject) => {
    fetchApi(reqObj)
      .then(data => {
        resolve(data);
      })
      .catch(e => reject(e));
  });
};

export const getBlogList = () => {
  const reqObj = generateReqHeaders({
    url: `${API_ROUTES.getAllBlogs}`,
    method: 'POST',
    isAuth: true,
  });

  return new Promise((resolve, reject) => {
    fetchApi(reqObj)
      .then(data => {
        resolve(data);
      })
      .catch(e => reject(e));
  });
};

export const viewBlog = id => {
  const reqObj = generateReqHeaders({
    url: `${API_ROUTES.viewBlog}/${id}`,
    method: 'POST',
    isAuth: true,
  });

  return new Promise((resolve, reject) => {
    fetchApi(reqObj)
      .then(data => {
        resolve(data);
      })
      .catch(e => reject(e));
  });
};

export const deleteBlog = id => {
  const reqObj = generateReqHeaders({
    url: `${API_ROUTES.deleteBlog}/${id}`,
    method: 'POST',
    isAuth: true,
  });

  return new Promise((resolve, reject) => {
    fetchApi(reqObj)
      .then(data => {
        resolve(data);
      })
      .catch(e => reject(e));
  });
};

export const downloadBlog = id => {
  const reqObj = generateReqHeaders({
    url: `${API_ROUTES.downloadBlog}/${id}`,
    method: 'POST',
    isAuth: true,
    isBlob: true,
  });

  return new Promise((resolve, reject) => {
    fetchApi(reqObj)
      .then(data => {
        resolve(data);
      })
      .catch(e => reject(e));
  });
};

/*PODCAST*/
export const getPodcastList = () => {
  const reqObj = generateReqHeaders({
    url: `${API_ROUTES.getAllPodcast}`,
    method: 'GET',
    isAuth: true,
  });

  return new Promise((resolve, reject) => {
    fetchApi(reqObj)
      .then(data => {
        resolve(data);
      })
      .catch(e => reject(e));
  });
};

export const editPodcast = id => {
  const reqObj = generateReqHeaders({
    url: `${API_ROUTES.editPodcast}/${id}`,
    method: 'GET',
    isAuth: true,
  });

  return new Promise((resolve, reject) => {
    fetchApi(reqObj)
      .then(data => {
        resolve(data);
      })
      .catch(e => reject(e));
  });
};

export const deletePodcast = id => {
  const reqObj = generateReqHeaders({
    url: `${API_ROUTES.deletePodcast}/${id}`,
    method: 'GET',
    isAuth: true,
  });

  return new Promise((resolve, reject) => {
    fetchApi(reqObj)
      .then(data => {
        resolve(data);
      })
      .catch(e => reject(e));
  });
};

export const createUpdatePodcast = reqData => {
  const reqObj = generateReqHeaders({
    url: `${API_ROUTES.createUpdatePodcast}`,
    method: 'POST',
    reqData,
  });

  return new Promise((resolve, reject) => {
    fetchApi(reqObj)
      .then(data => {
        resolve(data);
      })
      .catch(e => reject(e));
  });
};

export const getViewList = () => {
  const reqObj = generateReqHeaders({
    url: `${API_ROUTES.getViewList}`,
    method: 'GET',
    isAuth: true,
  });

  return new Promise((resolve, reject) => {
    fetchApi(reqObj)
      .then(data => {
        resolve(data);
      })
      .catch(e => reject(e));
  });
};

export const downloadCountryAnalysis = id => {
  const reqObj = generateReqHeaders({
    url: `${API_ROUTES.downloadCountryAnalysis}/${id}`,
    method: 'GET',
    isAuth: true,
    isBlob: true,
  });

  return new Promise((resolve, reject) => {
    fetchApi(reqObj)
      .then(blobObj => {
        let objUrl = new Blob([blobObj], { type: 'application/vnd.ms-excel' });
        resolve(objUrl);
      })
      .catch(e => reject(e));
  });
};