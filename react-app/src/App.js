import React, { Component } from 'react';
import Layout from './components/Layout';
import './sass/custom.scss';
//cookie
import { CookiesProvider } from 'react-cookie';
//router
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './store';
//utils
import { MuiPickersUtilsProvider } from 'material-ui-pickers';
import DateFnsUtils from '@date-io/date-fns';

class App extends Component {
  render() {
    return (
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <CookiesProvider>
          <Router>
            <Provider store={store}>
              <Layout />
            </Provider>
          </Router>
        </CookiesProvider>
      </MuiPickersUtilsProvider>
    );
  }
}

export default App;
