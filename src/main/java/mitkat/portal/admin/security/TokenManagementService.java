package mitkat.portal.admin.security;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import mitkat.portal.admin.model.user.TokenMap;
import mitkat.portal.admin.repository.application.TokenRepository;
import mitkat.portal.admin.service.DateCompare;

@Service
public class TokenManagementService {

	@Autowired
	TokenRepository tokenrepository;
	
	@Value("${app.concurrentlogins}")
    private int concurrentlogins;
	
	
	public void addToken(Long id, String token) {
		
		TokenMap tempmap = new TokenMap();
		
		tempmap.setUserId(id);
		tempmap.setToken(token);
		tempmap.setTokentime(Instant.now());
		
		tokenrepository.save(tempmap);
		
		List<TokenMap> tokenlist = tokenrepository.findByUserId(id);
		
		if(tokenlist.size() > concurrentlogins)
		{
			Collections.sort(tokenlist, new DateCompare());
			
			TokenMap tempmap1 = tokenrepository.getOne(tokenlist.get(0).getId());
			
			tokenrepository.delete(tempmap1);
			
		}
		
		
		
		
		
	}


	public boolean checktoken(String authToken) {
		
		
		
		
		
		List<TokenMap> tokenlist = new ArrayList<>();
		
		try {
			
			tokenlist = tokenrepository.findByToken(authToken);
			
		}catch(Exception e) {}
		
		if(tokenlist != null && !tokenlist.isEmpty() && tokenlist.size() > 0)
		{
			return true;
		}
		else {
			
			return false;
		}
		
	}

}
