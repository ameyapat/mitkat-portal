package mitkat.portal.admin.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint {

	@Value("${app.concurrentlogins}")
    private int concurrentlogins;
	
    private static final Logger logger = LoggerFactory.getLogger(JwtAuthenticationEntryPoint.class);
    @Override
    public void commence(HttpServletRequest httpServletRequest,
                         HttpServletResponse httpServletResponse,
                         AuthenticationException e) throws IOException, ServletException {
        logger.error("Responding with unauthorized error. Message - {}", e.getMessage());
        
     //   httpServletResponse.setContentType("application/json");
     //   httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        
     //   if(e.getMessage().length() > 0)	
     //   {httpServletResponse.getOutputStream().println("{\"error\":\""+e.getMessage()+"\"}");}
     //   else
     //   {httpServletResponse.getOutputStream().println("{\"error\":\""+"Simultanious logins detected above permissible amount. This can cause if more than "+concurrentlogins+" concurrent logins are"+"\"}");}
        
        httpServletResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, e.getMessage());
    }
	
}