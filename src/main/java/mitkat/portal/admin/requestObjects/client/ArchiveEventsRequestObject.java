package mitkat.portal.admin.requestObjects.client;

import java.util.List;

public class ArchiveEventsRequestObject {

	List<Integer> risklevel;
	
	List<Integer> riskcategory;
	
	List<Integer> countries;
	
	List<Integer> states;
	
	List<Integer> cities;
	
	String startdate;
	
	String enddate;

	public List<Integer> getRisklevel() {
		return risklevel;
	}

	public void setRisklevel(List<Integer> risklevel) {
		this.risklevel = risklevel;
	}

	public List<Integer> getRiskcategory() {
		return riskcategory;
	}

	public void setRiskcategory(List<Integer> riskcategory) {
		this.riskcategory = riskcategory;
	}

	public List<Integer> getCountries() {
		return countries;
	}

	public void setCountries(List<Integer> countries) {
		this.countries = countries;
	}

	public List<Integer> getStates() {
		return states;
	}

	public void setStates(List<Integer> states) {
		this.states = states;
	}

	public List<Integer> getCities() {
		return cities;
	}

	public void setCities(List<Integer> cities) {
		this.cities = cities;
	}

	public String getStartdate() {
		return startdate;
	}

	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}

	public String getEnddate() {
		return enddate;
	}

	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}


	
	
	
	
	
}
