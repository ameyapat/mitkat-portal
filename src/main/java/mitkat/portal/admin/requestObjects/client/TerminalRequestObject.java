package mitkat.portal.admin.requestObjects.client;

import java.util.List;

public class TerminalRequestObject {
	
	int days;
	
	List<Integer> eventtense;
	
	List<Integer> risklevel;
	
	List<Integer> riskcategory;

	public int getDays() {
		return days;
	}

	public void setDays(int days) {
		this.days = days;
	}

	public List<Integer> getEventtense() {
		return eventtense;
	}

	public void setEventtense(List<Integer> eventtense) {
		this.eventtense = eventtense;
	}

	public List<Integer> getRisklevel() {
		return risklevel;
	}

	public void setRisklevel(List<Integer> risklevel) {
		this.risklevel = risklevel;
	}

	public List<Integer> getRiskcategory() {
		return riskcategory;
	}

	public void setRiskcategory(List<Integer> riskcategory) {
		this.riskcategory = riskcategory;
	}
	
	
	
	

}
