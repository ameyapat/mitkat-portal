package mitkat.portal.admin.requestObjects.creator;

public class UpdateRequestObject {
	
	long eventid;
	
	long updateid;
	
	String title;

	String discription;

	

	public long getEventid() {
		return eventid;
	}

	public void setEventid(long eventid) {
		this.eventid = eventid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDiscription() {
		return discription;
	}

	public void setDiscription(String discription) {
		this.discription = discription;
	}

	public long getUpdateid() {
		return updateid;
	}

	public void setUpdateid(long updateid) {
		this.updateid = updateid;
	}

	
	
	

}
