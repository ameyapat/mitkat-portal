package mitkat.portal.admin.requestObjects.creator;

import java.util.Date;
import java.util.List;

public class EventRequestObject {
	
	
	private Long id;
	private String title;
	private String description;
	private String background;
	private String impact;
	private String recommendation;
	private String links;
	private Double latitude;
	private Double longitude;
	private Date storydate;
	private Date validitydate;
	private Long riskLevel;
	private Long riskcategory;
	private Long regionType;
	private List<Long> impactindustry;
	private List<Long> countries;
	private List<Long> state;
	private List<Long> cities;
	private boolean importance;
	private long status;
	private boolean liveevent;
	
	//new
	private String description2;
	private String background2;
	private String impact2;
	private String recommendation2;
	private List<String> descriptionbullets;
	private List<String> backgroundbullets;
	private List<String> impactbullets;
	private List<String> recommendationbullets;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getBackground() {
		return background;
	}
	public void setBackground(String background) {
		this.background = background;
	}
	public String getImpact() {
		return impact;
	}
	public void setImpact(String impact) {
		this.impact = impact;
	}
	public String getRecommendation() {
		return recommendation;
	}
	public void setRecommendation(String recommendation) {
		this.recommendation = recommendation;
	}
	public String getLinks() {
		return links;
	}
	public void setLinks(String links) {
		this.links = links;
	}
	
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public Date getStorydate() {
		return storydate;
	}
	public void setStorydate(Date storydate) {
		this.storydate = storydate;
	}
	public Date getValiditydate() {
		return validitydate;
	}
	public void setValiditydate(Date validitydate) {
		this.validitydate = validitydate;
	}
	
	public Long getRiskLevel() {
		return riskLevel;
	}
	public void setRiskLevel(Long riskLevel) {
		this.riskLevel = riskLevel;
	}
	public Long getRiskcategory() {
		return riskcategory;
	}
	public void setRiskcategory(Long riskcategory) {
		this.riskcategory = riskcategory;
	}
	public Long getRegionType() {
		return regionType;
	}
	public void setRegionType(Long regionType) {
		this.regionType = regionType;
	}
	public List<Long> getImpactindustry() {
		return impactindustry;
	}
	public void setImpactindustry(List<Long> impactindustry) {
		this.impactindustry = impactindustry;
	}
	public List<Long> getCountries() {
		return countries;
	}
	public void setCountries(List<Long> countries) {
		this.countries = countries;
	}
	public List<Long> getState() {
		return state;
	}
	public void setState(List<Long> state) {
		this.state = state;
	}
	public List<Long> getCities() {
		return cities;
	}
	public void setCities(List<Long> cities) {
		this.cities = cities;
	}
	public boolean isImportance() {
		return importance;
	}
	public void setImportance(boolean importance) {
		this.importance = importance;
	}
	public long getStatus() {
		return status;
	}
	public void setStatus(long status) {
		this.status = status;
	}
	public boolean isLiveevent() {
		return liveevent;
	}
	public void setLiveevent(boolean liveevent) {
		this.liveevent = liveevent;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDescription2() {
		return description2;
	}
	public void setDescription2(String description2) {
		this.description2 = description2;
	}
	public String getBackground2() {
		return background2;
	}
	public void setBackground2(String background2) {
		this.background2 = background2;
	}
	public String getImpact2() {
		return impact2;
	}
	public void setImpact2(String impact2) {
		this.impact2 = impact2;
	}
	public String getRecommendation2() {
		return recommendation2;
	}
	public void setRecommendation2(String recommendation2) {
		this.recommendation2 = recommendation2;
	}
	public List<String> getDescriptionbullets() {
		return descriptionbullets;
	}
	public void setDescriptionbullets(List<String> descriptionbullets) {
		this.descriptionbullets = descriptionbullets;
	}
	public List<String> getBackgroundbullets() {
		return backgroundbullets;
	}
	public void setBackgroundbullets(List<String> backgroundbullets) {
		this.backgroundbullets = backgroundbullets;
	}
	public List<String> getImpactbullets() {
		return impactbullets;
	}
	public void setImpactbullets(List<String> impactbullets) {
		this.impactbullets = impactbullets;
	}
	public List<String> getRecommendationbullets() {
		return recommendationbullets;
	}
	public void setRecommendationbullets(List<String> recommendationbullets) {
		this.recommendationbullets = recommendationbullets;
	}
	
	
	
}
