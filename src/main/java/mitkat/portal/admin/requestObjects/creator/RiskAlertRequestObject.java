package mitkat.portal.admin.requestObjects.creator;

import java.util.List;

public class RiskAlertRequestObject {
	private int id;
	private String title;
	private String links;
	private Double latitude;
	private Double longitude;
	private Long riskLevel;
	private Long riskcategory;
	private Long regionType;
	private List<Long> impactindustry;
	private List<Long> countries;
	private List<Long> state;
	private List<Long> cities;
	private long status;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getLinks() {
		return links;
	}
	public void setLinks(String links) {
		this.links = links;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public Long getRiskLevel() {
		return riskLevel;
	}
	public void setRiskLevel(Long riskLevel) {
		this.riskLevel = riskLevel;
	}
	public Long getRiskcategory() {
		return riskcategory;
	}
	public void setRiskcategory(Long riskcategory) {
		this.riskcategory = riskcategory;
	}
	public Long getRegionType() {
		return regionType;
	}
	public void setRegionType(Long regionType) {
		this.regionType = regionType;
	}
	public List<Long> getImpactindustry() {
		return impactindustry;
	}
	public void setImpactindustry(List<Long> impactindustry) {
		this.impactindustry = impactindustry;
	}
	public List<Long> getCountries() {
		return countries;
	}
	public void setCountries(List<Long> countries) {
		this.countries = countries;
	}
	public List<Long> getState() {
		return state;
	}
	public void setState(List<Long> state) {
		this.state = state;
	}
	public List<Long> getCities() {
		return cities;
	}
	public void setCities(List<Long> cities) {
		this.cities = cities;
	}
	public long getStatus() {
		return status;
	}
	public void setStatus(long status) {
		this.status = status;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
	
}
