package mitkat.portal.admin.requestObjects.clientprofile;

import java.util.Date;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import mitkat.portal.admin.model.clientprofile.OfficeLocation;

public class ClientProfileRequestObject {

	
	private long clientid;
	private String companyname;
	private Date contractdate;
	private Date contractenddate;
	private String contactnumber;
	private String contactperson;
	private MultipartFile logo;
	private List<String> clientemails;
	private List<OfficeLocation> officelocations;
	private List<Integer> countries;
	private List<Integer> states;
	private List<Integer> cities;
	private List<Integer> producttypes;
	private List<Integer> impactindustry;
	public long getClientid() {
		return clientid;
	}
	public void setClientid(long clientid) {
		this.clientid = clientid;
	}
	public String getCompanyname() {
		return companyname;
	}
	public void setCompanyname(String companyname) {
		this.companyname = companyname;
	}
	public Date getContractdate() {
		return contractdate;
	}
	public void setContractdate(Date contractdate) {
		this.contractdate = contractdate;
	}
	public String getContactnumber() {
		return contactnumber;
	}
	public void setContactnumber(String contactnumber) {
		this.contactnumber = contactnumber;
	}
	public String getContactperson() {
		return contactperson;
	}
	public void setContactperson(String contactperson) {
		this.contactperson = contactperson;
	}
	
	
	
	public MultipartFile getLogo() {
		return logo;
	}
	public void setLogo(MultipartFile logo) {
		this.logo = logo;
	}
	public List<String> getClientemails() {
		return clientemails;
	}
	public void setClientemails(List<String> clientemails) {
		this.clientemails = clientemails;
	}
	public List<OfficeLocation> getOfficelocations() {
		return officelocations;
	}
	public void setOfficelocations(List<OfficeLocation> officelocations) {
		this.officelocations = officelocations;
	}
	public List<Integer> getCountries() {
		return countries;
	}
	public void setCountries(List<Integer> countries) {
		this.countries = countries;
	}
	public List<Integer> getStates() {
		return states;
	}
	public void setStates(List<Integer> states) {
		this.states = states;
	}
	public List<Integer> getCities() {
		return cities;
	}
	public void setCities(List<Integer> cities) {
		this.cities = cities;
	}
	public List<Integer> getProducttypes() {
		return producttypes;
	}
	public void setProducttypes(List<Integer> producttypes) {
		this.producttypes = producttypes;
	}
	public Date getContractenddate() {
		return contractenddate;
	}
	public void setContractenddate(Date contractenddate) {
		this.contractenddate = contractenddate;
	}
	public List<Integer> getImpactindustry() {
		return impactindustry;
	}
	public void setImpactindustry(List<Integer> impactindustry) {
		this.impactindustry = impactindustry;
	}
	
	
}
