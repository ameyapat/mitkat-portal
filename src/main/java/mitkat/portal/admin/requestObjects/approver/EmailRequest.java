package mitkat.portal.admin.requestObjects.approver;

import java.util.List;

public class EmailRequest {
	
	private int logo;
	private int signature;
	private long clientid;
	private List<Long> eventid;
	
	
	public int getLogo() {
		return logo;
	}
	public void setLogo(int logo) {
		this.logo = logo;
	}
	public int getSignature() {
		return signature;
	}
	public void setSignature(int signature) {
		this.signature = signature;
	}
	public long getClientid() {
		return clientid;
	}
	public void setClientid(long clientid) {
		this.clientid = clientid;
	}
	public List<Long> getEventid() {
		return eventid;
	}
	public void setEventid(List<Long> eventid) {
		this.eventid = eventid;
	}
	
	
}
