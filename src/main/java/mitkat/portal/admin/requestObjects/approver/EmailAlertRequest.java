package mitkat.portal.admin.requestObjects.approver;

import java.util.List;

public class EmailAlertRequest {

	Long eventids;
	List<Long> clientids;
	int logo;
	int signature;
	
	public Long getEventids() {
		return eventids;
	}
	public void setEventids(Long eventids) {
		this.eventids = eventids;
	}
	public List<Long> getClientids() {
		return clientids;
	}
	public void setClientids(List<Long> clientids) {
		this.clientids = clientids;
	}
	public int getLogo() {
		return logo;
	}
	public void setLogo(int logo) {
		this.logo = logo;
	}
	public int getSignature() {
		return signature;
	}
	public void setSignature(int signature) {
		this.signature = signature;
	}
	
	
	
}
