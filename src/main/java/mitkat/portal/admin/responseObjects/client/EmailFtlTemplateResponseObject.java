package mitkat.portal.admin.responseObjects.client;

import java.util.List;

public class EmailFtlTemplateResponseObject {
		
	int id;
	String country;
	String state;
	String title;
	long risklevel;
	long riskcategiry;
	
	
	String discription;
	String background;
	String impact;
	String recommendation;
	
	String description2;
	String background2;
	String impact2;
	String recommendation2;
	List<String> descriptionbullets;
	List<String> backgroundbullets;
	List<String> impactbullets;
	List<String> recommendationbullets;
	
	
	
	int discid;
	int backid;
	int impactid;
	int recommid;
	
	int discid2;
	int backid2;
	int impactid2;
	int recommid2;
	
	int discidbullet;
	int backidbullet;
	int impactidbullet;
	int recommidbullet;
	
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public long getRisklevel() {
		return risklevel;
	}
	public void setRisklevel(long l) {
		this.risklevel = l;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDiscription() {
		return discription;
	}
	public void setDiscription(String discription) {
		this.discription = discription;
	}
	public String getBackground() {
		return background;
	}
	public void setBackground(String background) {
		this.background = background;
	}
	public String getImpact() {
		return impact;
	}
	public void setImpact(String impact) {
		this.impact = impact;
	}
	public String getRecommendation() {
		return recommendation;
	}
	public void setRecommendation(String recommendation) {
		this.recommendation = recommendation;
	}
	public int getDiscid() {
		return discid;
	}
	public void setDiscid(int discid) {
		this.discid = discid;
	}
	public int getBackid() {
		return backid;
	}
	public void setBackid(int backid) {
		this.backid = backid;
	}
	public int getImpactid() {
		return impactid;
	}
	public void setImpactid(int impactid) {
		this.impactid = impactid;
	}
	public int getRecommid() {
		return recommid;
	}
	public void setRecommid(int recommid) {
		this.recommid = recommid;
	}
	public long getRiskcategiry() {
		return riskcategiry;
	}
	public void setRiskcategiry(long riskcategiry) {
		this.riskcategiry = riskcategiry;
	}
	public String getDescription2() {
		return description2;
	}
	public void setDescription2(String description2) {
		this.description2 = description2;
	}
	public String getBackground2() {
		return background2;
	}
	public void setBackground2(String background2) {
		this.background2 = background2;
	}
	public String getImpact2() {
		return impact2;
	}
	public void setImpact2(String impact2) {
		this.impact2 = impact2;
	}
	public String getRecommendation2() {
		return recommendation2;
	}
	public void setRecommendation2(String recommendation2) {
		this.recommendation2 = recommendation2;
	}
	public List<String> getDescriptionbullets() {
		return descriptionbullets;
	}
	public void setDescriptionbullets(List<String> descriptionbullets) {
		this.descriptionbullets = descriptionbullets;
	}
	public List<String> getBackgroundbullets() {
		return backgroundbullets;
	}
	public void setBackgroundbullets(List<String> backgroundbullets) {
		this.backgroundbullets = backgroundbullets;
	}
	public List<String> getImpactbullets() {
		return impactbullets;
	}
	public void setImpactbullets(List<String> impactbullets) {
		this.impactbullets = impactbullets;
	}
	public List<String> getRecommendationbullets() {
		return recommendationbullets;
	}
	public void setRecommendationbullets(List<String> recommendationbullets) {
		this.recommendationbullets = recommendationbullets;
	}
	public int getDiscid2() {
		return discid2;
	}
	public void setDiscid2(int discid2) {
		this.discid2 = discid2;
	}
	public int getBackid2() {
		return backid2;
	}
	public void setBackid2(int backid2) {
		this.backid2 = backid2;
	}
	public int getImpactid2() {
		return impactid2;
	}
	public void setImpactid2(int impactid2) {
		this.impactid2 = impactid2;
	}
	public int getRecommid2() {
		return recommid2;
	}
	public void setRecommid2(int recommid2) {
		this.recommid2 = recommid2;
	}
	public int getDiscidbullet() {
		return discidbullet;
	}
	public void setDiscidbullet(int discidbullet) {
		this.discidbullet = discidbullet;
	}
	public int getBackidbullet() {
		return backidbullet;
	}
	public void setBackidbullet(int backidbullet) {
		this.backidbullet = backidbullet;
	}
	public int getImpactidbullet() {
		return impactidbullet;
	}
	public void setImpactidbullet(int impactidbullet) {
		this.impactidbullet = impactidbullet;
	}
	public int getRecommidbullet() {
		return recommidbullet;
	}
	public void setRecommidbullet(int recommidbullet) {
		this.recommidbullet = recommidbullet;
	}
	
	

}
