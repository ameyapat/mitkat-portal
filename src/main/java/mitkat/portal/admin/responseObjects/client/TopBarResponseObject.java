package mitkat.portal.admin.responseObjects.client;

public class TopBarResponseObject {

	int riskcategoryid;
	
	int numberofevents;

	String riskCategory;
	
	public int getRiskcategoryid() {
		return riskcategoryid;
	}

	public void setRiskcategoryid(int riskcategoryid) {
		this.riskcategoryid = riskcategoryid;
	}

	public int getNumberofevents() {
		return numberofevents;
	}

	public void setNumberofevents(int numberofevents) {
		this.numberofevents = numberofevents;
	}

	public String getRiskCategory() {
		return riskCategory;
	}

	public void setRiskCategory(String riskCategory) {
		this.riskCategory = riskCategory;
	}
	
	
	
}
