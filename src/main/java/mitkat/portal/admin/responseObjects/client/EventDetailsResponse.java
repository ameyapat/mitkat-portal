package mitkat.portal.admin.responseObjects.client;

import java.time.Instant;
import java.util.List;

import mitkat.portal.admin.model.creator.Update;


public class EventDetailsResponse {

	private long id;
	
	private String title;
	
	private String description;
	
	private String background;
	
	private String impact;
	
	private String recommendation;

	private Double latitude;
	
	private Double longitude;
	
	private Instant storydate;
	
	private Instant validitydate;

	private List<Update> updates;
	
	
	private String background2;
	private String impact2;
	private String recommendation2;
	private List<String> descriptionbullets;
	private List<String> backgroundbullets;
	private List<String> impactbullets;
	private List<String> recommendationbullets;
	
	
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getBackground() {
		return background;
	}

	public void setBackground(String background) {
		this.background = background;
	}

	public String getImpact() {
		return impact;
	}

	public void setImpact(String impact) {
		this.impact = impact;
	}

	public String getRecommendation() {
		return recommendation;
	}

	public void setRecommendation(String recommendation) {
		this.recommendation = recommendation;
	}



	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Instant getStorydate() {
		return storydate;
	}

	public void setStorydate(Instant storydate) {
		this.storydate = storydate;
	}

	public Instant getValiditydate() {
		return validitydate;
	}

	public void setValiditydate(Instant validitydate) {
		this.validitydate = validitydate;
	}

	public List<Update> getUpdates() {
		return updates;
	}

	public void setUpdates(List<Update> updates) {
		this.updates = updates;
	}

	public String getBackground2() {
		return background2;
	}

	public void setBackground2(String background2) {
		this.background2 = background2;
	}

	public String getImpact2() {
		return impact2;
	}

	public void setImpact2(String impact2) {
		this.impact2 = impact2;
	}

	public String getRecommendation2() {
		return recommendation2;
	}

	public void setRecommendation2(String recommendation2) {
		this.recommendation2 = recommendation2;
	}

	public List<String> getDescriptionbullets() {
		return descriptionbullets;
	}

	public void setDescriptionbullets(List<String> descriptionbullets) {
		this.descriptionbullets = descriptionbullets;
	}

	public List<String> getBackgroundbullets() {
		return backgroundbullets;
	}

	public void setBackgroundbullets(List<String> backgroundbullets) {
		this.backgroundbullets = backgroundbullets;
	}

	public List<String> getImpactbullets() {
		return impactbullets;
	}

	public void setImpactbullets(List<String> impactbullets) {
		this.impactbullets = impactbullets;
	}

	public List<String> getRecommendationbullets() {
		return recommendationbullets;
	}

	public void setRecommendationbullets(List<String> recommendationbullets) {
		this.recommendationbullets = recommendationbullets;
	}
	
	
}
