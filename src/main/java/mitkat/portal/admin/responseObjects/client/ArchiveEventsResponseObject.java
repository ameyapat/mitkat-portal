package mitkat.portal.admin.responseObjects.client;

public class ArchiveEventsResponseObject {
	
	
    long id;
	
	String title;
	
	long risklevel;
	
	long riskcategory;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public long getRisklevel() {
		return risklevel;
	}

	public void setRisklevel(long risklevel) {
		this.risklevel = risklevel;
	}

	public long getRiskcategory() {
		return riskcategory;
	}

	public void setRiskcategory(long riskcategory) {
		this.riskcategory = riskcategory;
	}

	
	
}
