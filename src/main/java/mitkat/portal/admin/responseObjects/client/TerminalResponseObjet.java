package mitkat.portal.admin.responseObjects.client;

import java.util.List;

public class TerminalResponseObjet {

	long id;
	
	String title;
	
	Double latitude;
	
	Double longitude;
	
	int eventtense;
	
	long risklevel;
	
	long riskcategory;
	
	// 0  for past
	// 1 for live
	// 2 for future

	List<UpdateResponse> updates;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public int getEventtense() {
		return eventtense;
	}

	public void setEventtense(int eventtense) {
		this.eventtense = eventtense;
	}

	public List<UpdateResponse> getUpdates() {
		return updates;
	}

	public void setUpdates(List<UpdateResponse> updates) {
		this.updates = updates;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getRisklevel() {
		return risklevel;
	}

	public void setRisklevel(long risklevel) {
		this.risklevel = risklevel;
	}

	public long getRiskcategory() {
		return riskcategory;
	}

	public void setRiskcategory(long riskcategory) {
		this.riskcategory = riskcategory;
	}
	
	
}
