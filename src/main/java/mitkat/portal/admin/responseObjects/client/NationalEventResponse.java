package mitkat.portal.admin.responseObjects.client;

public class NationalEventResponse {
	
	long id;
	
	String title;
	
	long riskcategory;
	
	long risklevel;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public long getRiskcategory() {
		return riskcategory;
	}

	public void setRiskcategory(long riskcategory) {
		this.riskcategory = riskcategory;
	}

	public long getRisklevel() {
		return risklevel;
	}

	public void setRisklevel(long risklevel) {
		this.risklevel = risklevel;
	}
	
	

}
