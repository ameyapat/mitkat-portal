package mitkat.portal.admin.responseObjects.client;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import mitkat.portal.admin.model.clientprofile.City;
import mitkat.portal.admin.model.clientprofile.Country;
import mitkat.portal.admin.model.clientprofile.State;
import mitkat.portal.admin.model.creator.RegionType;
import mitkat.portal.admin.model.creator.RiskCategory;
import mitkat.portal.admin.model.creator.RiskLevel;

public class RiskAlertDetailsResponse {
	
	private String title;
	
	private Double latitude;
	
	private Double longitude;
		
	private Instant alertdate;
	
	private RiskLevel riskLevel1;
	
	private RiskCategory riskCategory1;
	
	private RegionType regionType1;
	
	private List<Country> countries = new ArrayList<Country>();
	
	private List<State> state = new ArrayList<State>();
	
	private List<City> cities = new ArrayList<City>();

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Instant getAlertdate() {
		return alertdate;
	}

	public void setAlertdate(Instant alertdate) {
		this.alertdate = alertdate;
	}

	public RiskLevel getRiskLevel1() {
		return riskLevel1;
	}

	public void setRiskLevel1(RiskLevel riskLevel1) {
		this.riskLevel1 = riskLevel1;
	}

	public RiskCategory getRiskCategory1() {
		return riskCategory1;
	}

	public void setRiskCategory1(RiskCategory riskCategory1) {
		this.riskCategory1 = riskCategory1;
	}

	public RegionType getRegionType1() {
		return regionType1;
	}

	public void setRegionType1(RegionType regionType1) {
		this.regionType1 = regionType1;
	}

	public List<Country> getCountries() {
		return countries;
	}

	public void setCountries(List<Country> countries) {
		this.countries = countries;
	}

	public List<State> getState() {
		return state;
	}

	public void setState(List<State> state) {
		this.state = state;
	}

	public List<City> getCities() {
		return cities;
	}

	public void setCities(List<City> cities) {
		this.cities = cities;
	}
	
	
	
}
