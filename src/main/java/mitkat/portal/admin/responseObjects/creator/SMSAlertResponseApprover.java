package mitkat.portal.admin.responseObjects.creator;

import java.time.LocalDate;
import java.util.List;

import mitkat.portal.admin.model.clientprofile.City;
import mitkat.portal.admin.model.clientprofile.Country;
import mitkat.portal.admin.model.clientprofile.State;
import mitkat.portal.admin.model.creator.ImpactIndustry;
import mitkat.portal.admin.model.creator.RegionType;
import mitkat.portal.admin.model.creator.RiskCategory;
import mitkat.portal.admin.model.creator.RiskLevel;
import mitkat.portal.admin.model.creator.Status;

public class SMSAlertResponseApprover
{

	Long id;
	String title;
	String links;
	Double latitude;
	Double longitude;
	LocalDate alertdate;
	RiskLevel riskLevel;
	RiskCategory riskCategory;
	RegionType regionType;
	List<ImpactIndustry> impactindustry;
	List<Country> countries;
	List<State> state;
	List<City> cities;
	Status status;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getLinks() {
		return links;
	}
	public void setLinks(String links) {
		this.links = links;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public LocalDate getAlertdate() {
		return alertdate;
	}
	public void setAlertdate(LocalDate alertdate) {
		this.alertdate = alertdate;
	}
	public RiskLevel getRiskLevel() {
		return riskLevel;
	}
	public void setRiskLevel(RiskLevel riskLevel) {
		this.riskLevel = riskLevel;
	}
	public RiskCategory getRiskCategory() {
		return riskCategory;
	}
	public void setRiskCategory(RiskCategory riskCategory) {
		this.riskCategory = riskCategory;
	}
	public RegionType getRegionType() {
		return regionType;
	}
	public void setRegionType(RegionType regionType) {
		this.regionType = regionType;
	}
	public List<ImpactIndustry> getImpactindustry() {
		return impactindustry;
	}
	public void setImpactindustry(List<ImpactIndustry> impactindustry) {
		this.impactindustry = impactindustry;
	}
	public List<Country> getCountries() {
		return countries;
	}
	public void setCountries(List<Country> countries) {
		this.countries = countries;
	}
	public List<State> getState() {
		return state;
	}
	public void setState(List<State> state) {
		this.state = state;
	}
	public List<City> getCities() {
		return cities;
	}
	public void setCities(List<City> cities) {
		this.cities = cities;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	
	
	
}
