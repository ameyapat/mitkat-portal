package mitkat.portal.admin.responseObjects.creator;

import java.time.LocalDate;
import java.util.List;

import mitkat.portal.admin.model.clientprofile.City;
import mitkat.portal.admin.model.clientprofile.Country;
import mitkat.portal.admin.model.clientprofile.State;
import mitkat.portal.admin.model.creator.ImpactIndustry;
import mitkat.portal.admin.model.creator.RegionType;
import mitkat.portal.admin.model.creator.RiskCategory;
import mitkat.portal.admin.model.creator.RiskLevel;
import mitkat.portal.admin.model.creator.Update;

public class EventDetailsResponseCreator {

	Long id;
	String title;
	String description;
	String background;
	String impact;
	String recommendation;
	String links;
	Double latitude;
	Double longitude;
	boolean importance;
	boolean liveevent;
	LocalDate storydate;
	LocalDate validitydate;
	RiskLevel riskLevel;
	RiskCategory riskCategory;
	RegionType regionType;
	List<ImpactIndustry> impactindustry;
	List<Country> countries;
	List<State> state;
	List<City> cities;
	List<Update> updates;
	
	private String description2;
	private String background2;
	private String impact2;
	private String recommendation2;
	private List<String> descriptionbullets;
	private List<String> backgroundbullets;
	private List<String> impactbullets;
	private List<String> recommendationbullets;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getBackground() {
		return background;
	}
	public void setBackground(String background) {
		this.background = background;
	}
	public String getImpact() {
		return impact;
	}
	public void setImpact(String impact) {
		this.impact = impact;
	}
	public String getRecommendation() {
		return recommendation;
	}
	public void setRecommendation(String recommendation) {
		this.recommendation = recommendation;
	}
	public String getLinks() {
		return links;
	}
	public void setLinks(String links) {
		this.links = links;
	}
	
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public boolean isImportance() {
		return importance;
	}
	public void setImportance(boolean importance) {
		this.importance = importance;
	}
	public boolean isLiveevent() {
		return liveevent;
	}
	public void setLiveevent(boolean liveevent) {
		this.liveevent = liveevent;
	}
	public LocalDate getStorydate() {
		return storydate;
	}
	public void setStorydate(LocalDate storydate) {
		this.storydate = storydate;
	}
	public LocalDate getValiditydate() {
		return validitydate;
	}
	public void setValiditydate(LocalDate validitydate) {
		this.validitydate = validitydate;
	}
	public RiskLevel getRiskLevel() {
		return riskLevel;
	}
	public void setRiskLevel(RiskLevel riskLevel) {
		this.riskLevel = riskLevel;
	}
	public RiskCategory getRiskCategory() {
		return riskCategory;
	}
	public void setRiskCategory(RiskCategory riskCategory) {
		this.riskCategory = riskCategory;
	}
	public RegionType getRegionType() {
		return regionType;
	}
	public void setRegionType(RegionType regionType) {
		this.regionType = regionType;
	}
	public List<ImpactIndustry> getImpactindustry() {
		return impactindustry;
	}
	public void setImpactindustry(List<ImpactIndustry> impactindustry) {
		this.impactindustry = impactindustry;
	}
	public List<Country> getCountries() {
		return countries;
	}
	public void setCountries(List<Country> countries) {
		this.countries = countries;
	}
	public List<State> getState() {
		return state;
	}
	public void setState(List<State> state) {
		this.state = state;
	}
	public List<City> getCities() {
		return cities;
	}
	public void setCities(List<City> cities) {
		this.cities = cities;
	}
	public List<Update> getUpdates() {
		return updates;
	}
	public void setUpdates(List<Update> updates) {
		this.updates = updates;
	}
	public String getDescription2() {
		return description2;
	}
	public void setDescription2(String description2) {
		this.description2 = description2;
	}
	public String getBackground2() {
		return background2;
	}
	public void setBackground2(String background2) {
		this.background2 = background2;
	}
	public String getImpact2() {
		return impact2;
	}
	public void setImpact2(String impact2) {
		this.impact2 = impact2;
	}
	public String getRecommendation2() {
		return recommendation2;
	}
	public void setRecommendation2(String recommendation2) {
		this.recommendation2 = recommendation2;
	}
	public List<String> getDescriptionbullets() {
		return descriptionbullets;
	}
	public void setDescriptionbullets(List<String> descriptionbullets) {
		this.descriptionbullets = descriptionbullets;
	}
	public List<String> getBackgroundbullets() {
		return backgroundbullets;
	}
	public void setBackgroundbullets(List<String> backgroundbullets) {
		this.backgroundbullets = backgroundbullets;
	}
	public List<String> getImpactbullets() {
		return impactbullets;
	}
	public void setImpactbullets(List<String> impactbullets) {
		this.impactbullets = impactbullets;
	}
	public List<String> getRecommendationbullets() {
		return recommendationbullets;
	}
	public void setRecommendationbullets(List<String> recommendationbullets) {
		this.recommendationbullets = recommendationbullets;
	}
	
	
	
}
