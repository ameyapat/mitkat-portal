package mitkat.portal.admin.responseObjects.approver;

public class ClientListResponse {

	long id;
	
	String clientName;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	
	
	
}
