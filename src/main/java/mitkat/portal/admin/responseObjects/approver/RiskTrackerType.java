package mitkat.portal.admin.responseObjects.approver;

public class RiskTrackerType {
	
	private long id;
	
	private String trackerName;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTrackerName() {
		return trackerName;
	}

	public void setTrackerName(String trackerName) {
		this.trackerName = trackerName;
	}
	
	

}
