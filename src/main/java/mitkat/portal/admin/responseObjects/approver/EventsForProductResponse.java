package mitkat.portal.admin.responseObjects.approver;

import java.time.Instant;
import java.time.LocalDate;

public class EventsForProductResponse {
	
	long id;
	LocalDate date;
	String title;
	String country;
	String State;
	String Metro;
	String storytype;
	boolean importance;
	int relevency;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getState() {
		return State;
	}
	public void setState(String state) {
		State = state;
	}
	public String getMetro() {
		return Metro;
	}
	public void setMetro(String metro) {
		Metro = metro;
	}
	public String getStorytype() {
		return storytype;
	}
	public void setStorytype(String regiontype) {
		this.storytype = regiontype;
	}
	public boolean isImportance() {
		return importance;
	}
	public void setImportance(boolean importance) {
		this.importance = importance;
	}
	public int getRelevency() {
		return relevency;
	}
	public void setRelevency(int relevency) {
		this.relevency = relevency;
	}
	
	

}
