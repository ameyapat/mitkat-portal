package mitkat.portal.admin.responseObjects.approver;

import java.util.List;

public class EmailResponseObject {

	int totalFailedEmails;
	
	List<String> failedEmailList;

	public int getTotalFailedEmails() {
		return totalFailedEmails;
	}

	public void setTotalFailedEmails(int totalFailedEmails) {
		this.totalFailedEmails = totalFailedEmails;
	}

	public List<String> getFailedEmailList() {
		return failedEmailList;
	}

	public void setFailedEmailList(List<String> failedEmailList) {
		this.failedEmailList = failedEmailList;
	}

	
	
}
