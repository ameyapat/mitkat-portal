package mitkat.portal.admin.responseObjects.authentication;

import mitkat.portal.admin.model.user.Role;

public class JwtAuthenticationResponse {
    private String accessToken;
    private String tokenType = "Bearer";
    private Role roletype;

    public JwtAuthenticationResponse(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

	public Role getRoletype() {
		return roletype;
	}

	public void setRoletype(Role roletype) {
		this.roletype = roletype;
	}



}
