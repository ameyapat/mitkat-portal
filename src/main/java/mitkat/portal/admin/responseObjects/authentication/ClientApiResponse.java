package mitkat.portal.admin.responseObjects.authentication;

public class ClientApiResponse {
	private Boolean success;
    private String message;
    private Long clientid;
   
    
    
    public ClientApiResponse(Boolean success, String message, Long clientid) {
		super();
		this.success = success;
		this.message = message;
		this.clientid = clientid;
	}

	public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

	public Long getClientid() {
		return clientid;
	}

	public void setClientid(Long clientid) {
		this.clientid = clientid;
	}
	
	
}
