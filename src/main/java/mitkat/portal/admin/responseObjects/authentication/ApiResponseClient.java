package mitkat.portal.admin.responseObjects.authentication;

public class ApiResponseClient {
    private Boolean success;
    private String message;
    private long clientid;

   

    public ApiResponseClient(Boolean success, String message, long clientid) {
		super();
		this.success = success;
		this.message = message;
		this.clientid = clientid;
	}

	public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

	public long getClientid() {
		return clientid;
	}

	public void setClientid(long clientid) {
		this.clientid = clientid;
	}

    

}
