package mitkat.portal.admin.responseObjects.eventresponse;

import java.time.Instant;
import java.time.LocalDate;


public class EventResponse {
	
	private long id;
	LocalDate validitydate;
	private String title;
	private String country;
	private String state;
	private String city;
	private String status;
	private String creator;
	private String storytype;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	
	public LocalDate getValiditydate() {
		return validitydate;
	}
	public void setValiditydate(LocalDate validitydate) {
		this.validitydate = validitydate;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getStorytype() {
		return storytype;
	}
	public void setStorytype(String storytype) {
		this.storytype = storytype;
	}
	
	
}
