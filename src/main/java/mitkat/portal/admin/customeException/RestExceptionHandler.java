package mitkat.portal.admin.customeException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RestController
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(Exception.class)
	public ResponseEntity<ErrorResponse> handleException(Exception exc)
	{
		ErrorResponse error = new ErrorResponse(
										HttpStatus.BAD_GATEWAY.value(),
										exc.getMessage(),
										System.currentTimeMillis()
									);
				
				
		
		
		return new ResponseEntity<>(error,HttpStatus.BAD_GATEWAY);
	}
}
