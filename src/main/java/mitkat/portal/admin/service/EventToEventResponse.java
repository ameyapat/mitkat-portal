package mitkat.portal.admin.service;

import mitkat.portal.admin.model.creator.Report;
import mitkat.portal.admin.responseObjects.eventresponse.EventResponse;

public interface EventToEventResponse {

	public EventResponse convertEventToEventresponse(Report event);
}
