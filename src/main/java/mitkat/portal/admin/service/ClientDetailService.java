package mitkat.portal.admin.service;

import javax.validation.Valid;

import mitkat.portal.admin.model.clientprofile.ClientProfile;
import mitkat.portal.admin.requestObjects.clientprofile.ClientProfileRequestObject;
import mitkat.portal.admin.security.UserPrincipal;

public interface ClientDetailService {

	ClientProfile uploadClientDetails(@Valid ClientProfileRequestObject clientprofilerequest,
			UserPrincipal userprincipal);

	ClientProfile getClientDetails(long userprincipal);

	ClientProfile updateClientDetails(@Valid ClientProfileRequestObject clientprofilerequest,
			UserPrincipal userprincipal);
	
}
