package mitkat.portal.admin.service;

import java.util.List;

import javax.validation.Valid;

import mitkat.portal.admin.requestObjects.client.ArchiveEventsRequestObject;
import mitkat.portal.admin.requestObjects.client.TerminalRequestObject;
import mitkat.portal.admin.responseObjects.client.ArchiveEventsResponseObject;
import mitkat.portal.admin.responseObjects.client.EventDetailsResponse;
import mitkat.portal.admin.responseObjects.client.NationalEventResponse;
import mitkat.portal.admin.responseObjects.client.RiskAlertResponse;
import mitkat.portal.admin.responseObjects.client.TerminalResponseObjet;
import mitkat.portal.admin.responseObjects.client.TopBarResponseObject;
import mitkat.portal.admin.responseObjects.eventresponse.PagedResponse;
import mitkat.portal.admin.security.UserPrincipal;

public interface TerminalService {

	List<TerminalResponseObjet> getTerminalEvents(@Valid TerminalRequestObject request, UserPrincipal userprincipal);

	EventDetailsResponse getEvent(@Valid long eventid);

	List<NationalEventResponse> getNationalEvents();

	RiskAlertResponse getRiskAlerts(UserPrincipal userprincipal);

	List<RiskAlertResponse> getMultipleRiskAlerts(UserPrincipal userprincipal);

	List<RiskAlertResponse> getEventNotification(UserPrincipal userprincipal);

	List<ArchiveEventsResponseObject> getArchiveEvents(UserPrincipal userprincipal, ArchiveEventsRequestObject request);

	List<TopBarResponseObject> getTopBar(List<TerminalResponseObjet> teminalobject);

	PagedResponse<ArchiveEventsResponseObject> getArchiveEventsNew(UserPrincipal userprincipal,
			ArchiveEventsRequestObject request, int pageno);

}
