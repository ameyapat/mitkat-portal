package mitkat.portal.admin.service;

import org.springframework.stereotype.Service;

import mitkat.portal.admin.security.UserPrincipal;
import mitkat.portal.admin.customeException.BadRequestException;
import mitkat.portal.admin.model.creator.Report;
import mitkat.portal.admin.repository.application.ReportRepository;
import mitkat.portal.admin.requestObjects.event.FilterEventRequest;
import mitkat.portal.admin.responseObjects.eventresponse.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

@Service
public class EventServiceImplementation implements EventService{

	@Autowired 
	ReportRepository reportrepository;
	
	@Autowired
	EventToEventResponse eventtoeventresponse;
	
	
	@Override
	public int getNumberOfPages(UserPrincipal currentUser, int page, int size) {
		
		Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, "createdAt");
		int i = 0;
		
		
		Page<Report> reports = reportrepository.findAll(pageable);
		i =  reports.getTotalPages();
		
		if(page>i)
		{
			 throw new BadRequestException("Page Number "+page+" is not available!");
		}
	return i;	
	}
	
	
public PagedResponse<EventResponse> getAllEvents(UserPrincipal currentUser, int page, int size)	
{   
	
	Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, "createdAt");
	Page<Report> reports = reportrepository.findAll(pageable);
	
	if(reports.getNumberOfElements() == 0) {
        return new PagedResponse<>(Collections.emptyList(), reports.getNumber(),
        		reports.getSize(), reports.getTotalElements(), reports.getTotalPages(), reports.isLast());
    }
	
	List<EventResponse> eventresponse = new ArrayList<>();
	List<Report> templist = reports.getContent();
	
	for(Report tempreport:templist)
	{	
		EventResponse temp = eventtoeventresponse.convertEventToEventresponse(tempreport);
		eventresponse.add(temp);
	}
	
	
	/**
	List<Long> reportids = reports.map(Report::getId).getContent();
	List<EventResponse> eventresponse = new ArrayList<>();
	for(Long i:reportids)
	{  System.out.println(i);
		Report report = reportrepository.getOne(i);
		EventResponse temp = eventtoeventresponse.convertEventToEventresponse(report);
		eventresponse.add(temp);
		
	}
	**/
	return new PagedResponse<>(eventresponse, reports.getNumber(),
    		reports.getSize(), reports.getTotalElements(), reports.getTotalPages(), reports.isLast());
}

@Override
public PagedResponse<EventResponse> getFilteredEvents(UserPrincipal userprinciple, int page, int size,
		FilterEventRequest filterrequest) {
	
	
	
	Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, "createdAt");
	Page<Report> reports = null;
	
	if((filterrequest.getStartdate() == null))
	{
		if(filterrequest.getCreator()==0 && filterrequest.getStatus()==0)
		{reports = reportrepository.findAll(pageable);}
		else if(filterrequest.getStatus()==0)
		{reports = reportrepository.filterReportC(pageable,filterrequest.getCreator());}
		else if(filterrequest.getCreator()==0)
		{reports = reportrepository.filterReportS(pageable,filterrequest.getStatus());}	
		else
		{reports = reportrepository.filterReportCS(pageable,filterrequest.getCreator(),filterrequest.getStatus());}}
	else
	{  /*
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
		Date startdate = null;
		try {
			startdate = formatter.parse(filterrequest.getStartdate());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Instant newstartdate = (startdate.toInstant()).minus(1, ChronoUnit.DAYS);
		Date enddate=null;
		try {
			enddate = formatter.parse(filterrequest.getEnddate());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Instant newenddate = (enddate.toInstant()).plus(1, ChronoUnit.DAYS);
		*/
		
		Instant newstartdate = filterrequest.getStartdate().toInstant().minus(1, ChronoUnit.DAYS);
		Instant newenddate = filterrequest.getEnddate().toInstant().plus(1, ChronoUnit.DAYS);
		
		if(filterrequest.getCreator()==0  && filterrequest.getStatus()==0)
		{reports = reportrepository.filterReportT(pageable,newstartdate,newenddate);}
		else if(filterrequest.getStatus()==0)
		{reports = reportrepository.filterReportCT(pageable,filterrequest.getCreator(),newstartdate,newenddate);}
		else if(filterrequest.getCreator()==0 )
		{reports = reportrepository.filterReportST(pageable,filterrequest.getStatus(),newstartdate,newenddate);}
		else
		{reports = reportrepository.filterReportCST(pageable,filterrequest.getCreator(),filterrequest.getStatus(),newstartdate,newenddate);}	
	}	
		
	if(reports.getNumberOfElements() == 0) {
        return new PagedResponse<>(Collections.emptyList(), reports.getNumber(),
        		reports.getSize(), reports.getTotalElements(), reports.getTotalPages(), reports.isLast());
    }
	
	List<EventResponse> eventresponse = new ArrayList<>();
	List<Report> templist = reports.getContent();
	
	for(Report tempreport:templist)
	{	
		EventResponse temp = eventtoeventresponse.convertEventToEventresponse(tempreport);
		eventresponse.add(temp);
	}
	
	
	/**
	List<Long> reportids = reports.map(Report::getId).getContent();
	List<EventResponse> eventresponse = new ArrayList<>();
	for(Long i:reportids)
	{  System.out.println(i);
		Report report = reportrepository.getOne(i);
		EventResponse temp = eventtoeventresponse.convertEventToEventresponse(report);
		eventresponse.add(temp);
		
	}
	**/
	return new PagedResponse<>(eventresponse, reports.getNumber(),
    		reports.getSize(), reports.getTotalElements(), reports.getTotalPages(), reports.isLast());
}	
	

public static boolean isNullOrEmpty(String str) {
    if(str != null && !str.isEmpty())
        return false;
    return true;
}


@Override
public int getNumberOfPagesOFFilter(UserPrincipal userprinciple, int pagenumber, int i,
		FilterEventRequest filterrequest) {
	Pageable pageable = PageRequest.of(pagenumber, i, Sort.Direction.DESC, "createdAt");
	Page<Report> reports = null;
	
	if(filterrequest.getStartdate() == null)
	{
		if(filterrequest.getCreator()==0 && filterrequest.getStatus()==0)
		{reports = reportrepository.findAll(pageable);}
		else if(filterrequest.getStatus()==0)
		{reports = reportrepository.filterReportC(pageable,filterrequest.getCreator());}
		else if(filterrequest.getCreator()==0)
		{reports = reportrepository.filterReportS(pageable,filterrequest.getStatus());}	
		else
		{reports = reportrepository.filterReportCS(pageable,filterrequest.getCreator(),filterrequest.getStatus());}}
	else
	{/*
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
		Date startdate = null;
		try {
			startdate = formatter.parse(filterrequest.getStartdate());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Instant newstartdate = (startdate.toInstant()).minus(1, ChronoUnit.DAYS);
		Date enddate=null;
		try {
			enddate = formatter.parse(filterrequest.getEnddate());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Instant newenddate = (enddate.toInstant()).plus(1, ChronoUnit.DAYS);
	*/	
		Instant newstartdate = filterrequest.getStartdate().toInstant().minus(1, ChronoUnit.DAYS);
		Instant newenddate = filterrequest.getEnddate().toInstant().plus(1, ChronoUnit.DAYS);
		
		if(filterrequest.getCreator()==0  && filterrequest.getStatus()==0)
		{reports = reportrepository.filterReportT(pageable,newstartdate,newenddate);}
		else if(filterrequest.getStatus()==0)
		{reports = reportrepository.filterReportCT(pageable,filterrequest.getCreator(),newstartdate,newenddate);}
		else if(filterrequest.getCreator()==0 )
		{reports = reportrepository.filterReportST(pageable,filterrequest.getStatus(),newstartdate,newenddate);}
		else
		{reports = reportrepository.filterReportCST(pageable,filterrequest.getCreator(),filterrequest.getStatus(),newstartdate,newenddate);}	
	}	
	
	i =  reports.getTotalPages();
	
	if(pagenumber>i)
	{
		 throw new BadRequestException("Page Number "+pagenumber+" is not available!");
	}
	
	return i;
}


@Override
public List<EventResponse> getLiveEvents() {
	
	Instant date = LocalDate.now().atStartOfDay().toInstant(ZoneOffset.UTC).minus(1, ChronoUnit.DAYS);
	List<Report> reports = reportrepository.getTerminalReports(date);
	List<EventResponse> response = new ArrayList<>();
	
	if(reports != null && reports.size()>0)
	{for (Report report:reports)
	{
		if(report.isLiveevent())
		{
			EventResponse temp = new EventResponse();
			temp =  eventtoeventresponse.convertEventToEventresponse(report);
			response.add(temp);
			
		}
	}}
	return response;
}





}
