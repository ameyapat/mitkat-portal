package mitkat.portal.admin.service;

import java.util.List;

import mitkat.portal.admin.requestObjects.event.FilterEventRequest;
import mitkat.portal.admin.responseObjects.eventresponse.EventResponse;
import mitkat.portal.admin.responseObjects.eventresponse.PagedResponse;
import mitkat.portal.admin.security.UserPrincipal;

public interface EventService {

	public PagedResponse<EventResponse> getAllEvents(UserPrincipal currentUser, int page, int size)	;

	public PagedResponse<EventResponse> getFilteredEvents(UserPrincipal userprinciple, int pagenumber, int i,
			FilterEventRequest filterrequest);

	public int getNumberOfPages(UserPrincipal userprinciple, int pagenumber, int i);

	public int getNumberOfPagesOFFilter(UserPrincipal userprinciple, int pagenumber, int i,
			FilterEventRequest filterrequest);

	public List<EventResponse> getLiveEvents();
}
