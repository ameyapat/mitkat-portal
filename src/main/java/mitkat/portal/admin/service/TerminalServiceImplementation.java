package mitkat.portal.admin.service;



import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import mitkat.portal.admin.customeException.ResourceNotFoundException;
import mitkat.portal.admin.model.clientprofile.City;
import mitkat.portal.admin.model.clientprofile.ClientProfile;
import mitkat.portal.admin.model.clientprofile.Country;
import mitkat.portal.admin.model.clientprofile.State;
import mitkat.portal.admin.model.creator.BackgroundBullets;
import mitkat.portal.admin.model.creator.DescriptionBullets;
import mitkat.portal.admin.model.creator.ImpactBullets;
import mitkat.portal.admin.model.creator.RecommendationBullet;
import mitkat.portal.admin.model.creator.Report;
import mitkat.portal.admin.model.creator.RiskAlert;
import mitkat.portal.admin.model.creator.RiskCategory;
import mitkat.portal.admin.model.creator.Update;
import mitkat.portal.admin.repository.application.CityRepository;
import mitkat.portal.admin.repository.application.Countryrepository;
import mitkat.portal.admin.repository.application.ReportRepository;
import mitkat.portal.admin.repository.application.RiskAlertRepository;
import mitkat.portal.admin.repository.application.RiskCategoryRepository;
import mitkat.portal.admin.repository.application.RiskLevelRepository;
import mitkat.portal.admin.repository.application.StateRepository;
import mitkat.portal.admin.requestObjects.client.ArchiveEventsRequestObject;
import mitkat.portal.admin.requestObjects.client.TerminalRequestObject;
import mitkat.portal.admin.responseObjects.client.ArchiveEventsResponseObject;
import mitkat.portal.admin.responseObjects.client.EventDetailsResponse;
import mitkat.portal.admin.responseObjects.client.NationalEventResponse;
import mitkat.portal.admin.responseObjects.client.RiskAlertResponse;
import mitkat.portal.admin.responseObjects.client.TerminalResponseObjet;
import mitkat.portal.admin.responseObjects.client.TopBarResponseObject;
import mitkat.portal.admin.responseObjects.client.UpdateResponse;
import mitkat.portal.admin.responseObjects.eventresponse.PagedResponse;
import mitkat.portal.admin.security.UserPrincipal;

@Service
public class TerminalServiceImplementation implements TerminalService {

	@Autowired
	ClientDetailService clientdetailservice;
	
	@Autowired
	RiskCategoryRepository riskcategoryrepository;
	
	@Autowired
	RiskLevelRepository risklevelrepository;
	
	@Autowired
	ReportRepository reportrepository;
	
	@Autowired
	RiskAlertRepository riskalertrepository;
	
	@Autowired
	Countryrepository countryrepository;
	
	@Autowired
	StateRepository staterepository;
	
	@Autowired
	CityRepository cityrepository;
	
	public static boolean isNullOrEmpty(String str) {
	    if(str != null && !str.isEmpty())
	        return false;
	    return true;
	}
	
	@Override
	public List<TerminalResponseObjet> getTerminalEvents(@Valid TerminalRequestObject request,
			UserPrincipal userprincipal) {
		
		ClientProfile clientprofile = clientdetailservice.getClientDetails(userprincipal.getId());
		
		
		//default 1
		Instant date = LocalDate.now().atStartOfDay().toInstant(ZoneOffset.UTC).minus((request.getDays()-1), ChronoUnit.DAYS);
		
		
		
		List<Report> reports1 = reportrepository.getTerminalReports(date);
		List<Report> reprortfilter1 = new ArrayList<>();
		
		
		List<Report> reports = new ArrayList<>();
		
		
		//status filter
		for(Report temp:reports1)
		{
			if(temp.getStatus().getId() == 2)
			{
				reports.add(temp);
			}
		}
		
		//Filter with country
		
		if(clientprofile.getCountries() != null)
		{
			for(Country tempcountry:clientprofile.getCountries())
			{
				for(Report tempreport:reports)
				{
					if(tempreport.getCountries().contains(tempcountry))
					{ reprortfilter1.add(tempreport);  }
					
				}
			}
			
		}
		
		//Filter with states
		
		if(clientprofile.getState() != null)
		{
			for( State tempstate : clientprofile.getState())
			{
				for(Report tempreport:reports)
				{
					if(tempreport.getState().contains(tempstate))
					{ reprortfilter1.add(tempreport);  }
					
				}
			}
			
		}
		
		
		//Filter with city
		
		if(clientprofile.getCities() != null)
		{
			for( City tempcity: clientprofile.getCities())
			{
				for(Report tempreport:reports)
				{
					if(tempreport.getCities().contains(tempcity))
					{ reprortfilter1.add(tempreport);  }
					
				}
			}
			
		}
		
		
		reports.clear();
		
		//filter with risk category
		
		List<Report> reprortfilter2 = new ArrayList<>();
		
		for(int i: request.getRiskcategory())
		{
			
			
			for(Report tempreport: reprortfilter1)
			{
				if(tempreport.getRiskCategory().getId()==i)
				{
					
					reprortfilter2.add(tempreport);
					
				}
				
				
			}
			
			
		}
		
		
		reprortfilter1.clear();
		
		//filter with risk level
		
		List<Report> reprortfilter3 = new ArrayList<>();
		
		for(int i: request.getRisklevel())
		{
			
			for(Report tempreport:reprortfilter2)
			{
				
				if(tempreport.getRiskLevel().getId()==i)
				{
					
					reprortfilter3.add(tempreport);
					
				}
					
				
			}
			
		}
		
		
		reprortfilter2.clear();
		
		Set<Report> tempset = new LinkedHashSet<>();
		tempset.addAll(reprortfilter3);
		reprortfilter3.clear();
		reprortfilter3.addAll(tempset);
		
		
		List<TerminalResponseObjet> response = new ArrayList <>();
		Instant today = LocalDate.now().atStartOfDay().toInstant(ZoneOffset.UTC);
		
		
		
		
		
		for (Report tempreport:reprortfilter3)
		{
			
			TerminalResponseObjet temp = new TerminalResponseObjet();
			temp.setId(tempreport.getId());
			temp.setTitle(tempreport.getTitle());
			temp.setLatitude(tempreport.getLatitude());
			temp.setLongitude(tempreport.getLongitude());
			temp.setRiskcategory(tempreport.getRiskCategory().getId());
			temp.setRisklevel(tempreport.getRiskLevel().getId());
			
			Instant newsenddate = tempreport.getValiditydate();
			
			if(tempreport.isLiveevent())
			{
			temp.setEventtense(1);
			List<UpdateResponse> updateresponse = new ArrayList<>();
			
			if(tempreport.getUpdates()!= null)
			{for(Update tempupdate: tempreport.getUpdates())
			{
				
				UpdateResponse tempupdateresponse = new UpdateResponse();
				
				tempupdateresponse.setTitle(tempupdate.getTitle());
				tempupdateresponse.setTimestamp(tempupdate.getTimeupdate());
				updateresponse.add(tempupdateresponse);
				
				
			}
			}
			temp.setUpdates(updateresponse);
			
			}
			else if(newsenddate.isAfter(today))
			{
				
				temp.setEventtense(2);
				
				
			}	
			else
			{
				
				temp.setEventtense(0);
				
			}
			response.add(temp);
			
		}
		
		//filter with event tense
		List<TerminalResponseObjet> responsenew = new ArrayList <>();
		
		for(TerminalResponseObjet temp:response)
		{
			if(request.getEventtense().contains(temp.getEventtense()))
			{responsenew.add(temp);}
		}
		
		response.clear();
		return responsenew;
	}

	@Override
	public EventDetailsResponse getEvent(@Valid long eventid) {
		
		//Report report = reportrepository.getOne(eventid);
		Report report = reportrepository.findById(eventid).orElseThrow(() -> new ResourceNotFoundException("Report", "Report Id", eventid));
		
		EventDetailsResponse response = new EventDetailsResponse();
		
		response.setId(report.getId());
		if(report.getBackground() != null)
		{response.setBackground(report.getBackground());}
		
		if(report.getDescription() != null)
		{response.setDescription(report.getDescription());}
		
		if(report.getImpact() != null)
		{response.setImpact(report.getImpact());}
		
		if(report.getRecommendation() != null)
		{response.setRecommendation(report.getRecommendation());}
		
		if(report.getDescription2() != null)
		{response.setDescription(report.getDescription2());}
		
		if(report.getBackground2() != null)
		{response.setBackground2(report.getBackground2());}
		
		if(report.getImpact2() != null)
		{response.setImpact2(report.getImpact2());}
		
		if(report.getRecommendation2() != null)
		{response.setRecommendation2(report.getRecommendation2());}
		
		
		if(report.getDescriptionbullets() != null)
		{
			List<String> templist = new ArrayList<>();
			for(DescriptionBullets temp: report.getDescriptionbullets())
			{
				templist.add(temp.getBulletpoint());
			}
			response.setDescriptionbullets(templist);
		}
		
		if(report.getBackgroundbullets() != null)
		{	List<String> templist = new ArrayList<>();
			for(BackgroundBullets temp: report.getBackgroundbullets())
			{
				templist.add(temp.getBulletpoint());
			}
			response.setBackgroundbullets(templist);
		}
		
		if(report.getImpactbullets() != null)
		{
			List<String> templist = new ArrayList<>();
			for(ImpactBullets temp: report.getImpactbullets() )
			{
				templist.add(temp.getBulletpoint());
			}
			response.setImpactbullets(templist);
		}
		
		if(report.getRecommendationbullets() != null)
		{
			List<String> templist = new ArrayList<>();
			for(RecommendationBullet temp: report.getRecommendationbullets())
			{
				templist.add(temp.getBulletpoint());
			}
			response.setRecommendationbullets(templist);
		}
		
		
		response.setLatitude(report.getLatitude());
		response.setLongitude(report.getLongitude());
		
		
		
		if(report.getStorydate() != null)
	//	{response.setStorydate(report.getStorydate().plus(1, ChronoUnit.DAYS));}
		{response.setStorydate(report.getStorydate());}
			
		if(report.getTitle() != null)
		{response.setTitle(report.getTitle());}
		
		if(report.getValiditydate() != null)
	//	{response.setValiditydate(report.getValiditydate().plus(1, ChronoUnit.DAYS));}
		{response.setValiditydate(report.getValiditydate());}
		
		if(report.getUpdates() != null)
		{response.setUpdates(report.getUpdates());}
		
		return response;
	}

	@Override
	public List<NationalEventResponse> getNationalEvents() {
		
		Instant date = LocalDate.now().atStartOfDay().toInstant(ZoneOffset.UTC).minus(0, ChronoUnit.DAYS);
		List<Report> reports = reportrepository.getTerminalReports(date);
		
		List<NationalEventResponse> response = new ArrayList<>();
		
		if(reports != null)
		{for(Report temp: reports)
		{
			
			if(temp.getRegionType().getId()==1 && temp.getStatus().getId() == 2 )
			{
				NationalEventResponse tempresponse = new NationalEventResponse();
				tempresponse.setId(temp.getId());
				tempresponse.setTitle(temp.getTitle());
				tempresponse.setRiskcategory(temp.getRiskCategory().getId());
				tempresponse.setRisklevel(temp.getRiskLevel().getId());
				response.add(tempresponse);
			}
		}
		}
		return response;
	}

	@Override
	public RiskAlertResponse getRiskAlerts(UserPrincipal userprincipal) {
		
		Instant date = LocalDate.now().atStartOfDay().toInstant(ZoneOffset.UTC);
		ClientProfile clientprofile = clientdetailservice.getClientDetails(userprincipal.getId());
		List<RiskAlert> riskalerts = riskalertrepository.getTerminalAlerts(date);
		
		List<RiskAlert> filteredalert = new ArrayList<>();
		
		//Filter with country
		
				if(clientprofile.getCountries() != null)
				{
					for(Country tempcountry:clientprofile.getCountries())
					{
						for(RiskAlert tempalert:riskalerts)
						{
							if(tempalert.getCountries().contains(tempcountry))
							{ filteredalert.add(tempalert);  }
							
						}
					}
					
				}
		
				if(clientprofile.getState() != null)
				{
					for( State tempstate : clientprofile.getState())
					{
						for(RiskAlert tempalert:riskalerts)
						{
							if(tempalert.getState().contains(tempstate))
							{ filteredalert.add(tempalert);  }
							
						}
					}
					
				}
				
				
				//Filter with city
				
				if(clientprofile.getCities() != null)
				{
					for( City tempcity: clientprofile.getCities())
					{
						for(RiskAlert tempalert:riskalerts)
						{
							if(tempalert.getCities().contains(tempcity))
							{ filteredalert.add(tempalert);  }
							
						}
					}
					
				}
						
		riskalerts.clear();		
		
		Set<RiskAlert> tempset = new LinkedHashSet<>();
		tempset.addAll(filteredalert);
		filteredalert.clear();
		filteredalert.addAll(tempset);
		
		List<RiskAlert> filteredalertnew = new ArrayList<>();
		for(RiskAlert temp:filteredalert)
		{
			if(temp.getStatus1().getId() == 2)
			{
				filteredalertnew.add(temp);	
			}
		}
		
		filteredalert.clear();
		RiskAlert tempalert = new RiskAlert();
		try {
			tempalert = filteredalertnew.get(0);
		
		for(RiskAlert temp:filteredalertnew)
		{
			if(temp.getAlertdate().isAfter(tempalert.getAlertdate()))
			{ tempalert = temp; }
		}
		}catch(Exception e) {}
		
		RiskAlertResponse response = new RiskAlertResponse();
		
		try {
			
			response.setId(tempalert.getId());
			
			ZoneId zoneId = ZoneId.of( "Asia/Kolkata" );
			LocalDateTime datetimekolkata = tempalert.getAlertdate().atZone(zoneId).toLocalDateTime();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy hh:mm:ss a");	
			response.setTimestamp(datetimekolkata.format(formatter));
			response.setTitle(tempalert.getTitle());
		}catch(Exception e) {}
		
		
		return response;
	}

	@Override
	public List<RiskAlertResponse> getMultipleRiskAlerts(UserPrincipal userprincipal) {
		
		Instant date = LocalDate.now().atStartOfDay().toInstant(ZoneOffset.UTC);
		ClientProfile clientprofile = clientdetailservice.getClientDetails(userprincipal.getId());
		List<RiskAlert> riskalerts = riskalertrepository.getTerminalAlerts(date);
		
		List<RiskAlert> filteredalert = new ArrayList<>();
		
		//Filter with country
		
				if(clientprofile.getCountries() != null)
				{
					for(Country tempcountry:clientprofile.getCountries())
					{
						for(RiskAlert tempalert:riskalerts)
						{
							if(tempalert.getCountries().contains(tempcountry))
							{ filteredalert.add(tempalert);  }
							
						}
					}
					
				}
		
				if(clientprofile.getState() != null)
				{
					for( State tempstate : clientprofile.getState())
					{
						for(RiskAlert tempalert:riskalerts)
						{
							if(tempalert.getState().contains(tempstate))
							{ filteredalert.add(tempalert);  }
							
						}
					}
					
				}
				
				
				//Filter with city
				
				if(clientprofile.getCities() != null)
				{
					for( City tempcity: clientprofile.getCities())
					{
						for(RiskAlert tempalert:riskalerts)
						{
							if(tempalert.getCities().contains(tempcity))
							{ filteredalert.add(tempalert);  }
							
						}
					}
					
				}
						
		riskalerts.clear();		
		
		Set<RiskAlert> tempset = new LinkedHashSet<>();
		tempset.addAll(filteredalert);
		filteredalert.clear();
		filteredalert.addAll(tempset);
		
		Collections.sort(filteredalert, new DateCompareSMS());
		
		List<RiskAlertResponse> filteredalertnew = new ArrayList<>();
		for(RiskAlert temp:filteredalert)
		{
			if(temp.getStatus1().getId() == 2)
			{
				RiskAlertResponse response = new RiskAlertResponse();
				
				try {
					
					response.setId(temp.getId());
					ZoneId zoneId = ZoneId.of( "Asia/Kolkata" );
					LocalDateTime datetimekolkata = temp.getAlertdate().atZone(zoneId).toLocalDateTime();
					DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy hh:mm:ss a");	
					response.setTimestamp(datetimekolkata.format(formatter));
					response.setTitle(temp.getTitle());
					filteredalertnew.add(response);
					
				}catch(Exception e) {}
			}
		}
		
		return filteredalertnew;
	}

	@Override
	public List<RiskAlertResponse> getEventNotification(UserPrincipal userprincipal) {
		ClientProfile clientprofile = clientdetailservice.getClientDetails(userprincipal.getId());
		
        Instant date = LocalDate.now().atStartOfDay().toInstant(ZoneOffset.UTC).minus(1, ChronoUnit.DAYS);
		
		List<Report> reports = reportrepository.getTerminalReports(date);
		List<Report> reprortfilter1 = new ArrayList<>();
		
		//Filter with country
		
		if(clientprofile.getCountries() != null)
		{
			for(Country tempcountry:clientprofile.getCountries())
			{
				for(Report tempreport:reports)
				{
					if(tempreport.getCountries().contains(tempcountry))
					{ reprortfilter1.add(tempreport);  }
					
				}
			}
			
		}
		
		//Filter with states
		
		if(clientprofile.getState() != null)
		{
			for( State tempstate : clientprofile.getState())
			{
				for(Report tempreport:reports)
				{
					if(tempreport.getState().contains(tempstate))
					{ reprortfilter1.add(tempreport);  }
					
				}
			}
			
		}
		
		
		//Filter with city
		
		if(clientprofile.getCities() != null)
		{
			for( City tempcity: clientprofile.getCities())
			{
				for(Report tempreport:reports)
				{
					if(tempreport.getCities().contains(tempcity))
					{ reprortfilter1.add(tempreport);  }
					
				}
			}
			
		}
		
		
		reports.clear();
		Set<Report> tempset = new LinkedHashSet<>();
		tempset.addAll(reprortfilter1);
		reprortfilter1.clear();
		reprortfilter1.addAll(tempset);
		ZoneId zoneId = ZoneId.of( "Asia/Kolkata" );
		Instant nowminusten = Instant.now().minus(1080, ChronoUnit.MINUTES);
		List<RiskAlertResponse> filteredalertnew = new ArrayList<>();
		
		Collections.sort(reprortfilter1, new DateCompareReport());
		
		for(Report temp:reprortfilter1)
		{
			
			if(temp.getCreatedAt().isAfter(nowminusten))
			{
				RiskAlertResponse tempnotification = new RiskAlertResponse();
				tempnotification.setId(temp.getId());
				tempnotification.setTitle("New Event Added:"+temp.getTitle());
				
				LocalDateTime datetimekolkata = temp.getCreatedAt().atZone(zoneId).toLocalDateTime();
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy hh:mm:ss");	
				tempnotification.setTimestamp(datetimekolkata.format(formatter));
				
				filteredalertnew.add(tempnotification);
			}
			else if(temp.getCreatedAt().isBefore(nowminusten) && temp.getUpdatedAt().isAfter(nowminusten))
			{
				RiskAlertResponse tempnotification = new RiskAlertResponse();
				tempnotification.setId(temp.getId());
				tempnotification.setTitle("Event Updated:"+temp.getTitle());
				
				LocalDateTime datetimekolkata = temp.getUpdatedAt().atZone(zoneId).toLocalDateTime();
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy hh:mm:ss");	
				tempnotification.setTimestamp(datetimekolkata.format(formatter));
				
				filteredalertnew.add(tempnotification);
			}		
		}
		return filteredalertnew;
	}

	@Override
	public List<ArchiveEventsResponseObject> getArchiveEvents(UserPrincipal userprincipal,
			ArchiveEventsRequestObject request) {
		
		ClientProfile clientprofile = clientdetailservice.getClientDetails(userprincipal.getId());
		
		
		
		Instant StartDate = LocalDate.parse(request.getStartdate()).atStartOfDay(ZoneId.of("Asia/Calcutta")).toInstant();
		Instant EndDate =LocalDate.parse(request.getEnddate()).atStartOfDay(ZoneId.of("Asia/Calcutta")).toInstant();
		EndDate = EndDate.plus(1, ChronoUnit.DAYS);
		
		//filter with start date
		List<Report> reprortfilter7 = reportrepository.getTerminalReports(StartDate);
		List<Report> reports = new ArrayList<>();
		
		//filter with end date
		for( Report tempreport:reprortfilter7)
		{
			if(tempreport.getValiditydate().isBefore(EndDate))
			{
				reports.add(tempreport);
				
			}
		}
		
		
		List<Report> reprortfilter1 = new ArrayList<>();
		//Filter with country
		
				if(clientprofile.getCountries() != null)
				{
					for(Country tempcountry:clientprofile.getCountries())
					{
						for(Report tempreport:reports)
						{
							if(tempreport.getCountries().contains(tempcountry))
							{ reprortfilter1.add(tempreport);  }
							
						}
					}
					
				}
				
				//Filter with states
				
				if(clientprofile.getState() != null)
				{
					for( State tempstate : clientprofile.getState())
					{
						for(Report tempreport:reports)
						{
							if(tempreport.getState().contains(tempstate))
							{ reprortfilter1.add(tempreport);  }
							
						}
					}
					
				}
				
				
				//Filter with city
				
				if(clientprofile.getCities() != null)
				{
					for( City tempcity: clientprofile.getCities())
					{
						for(Report tempreport:reports)
						{
							if(tempreport.getCities().contains(tempcity))
							{ reprortfilter1.add(tempreport);  }
							
						}
					}
					
				}
				
				reports.clear();
				//filter with risk category
				
				List<Report> reprortfilter2 = new ArrayList<>();
				
				for(int i: request.getRiskcategory())
				{
					
					
					for(Report tempreport: reprortfilter1)
					{
						if(tempreport.getRiskCategory().getId()==i)
						{
							
							reprortfilter2.add(tempreport);
							
						}
						
						
					}
					
					
				}
				
				
				reprortfilter1.clear();
				
				//filter with risk level
				
				List<Report> reprortfilter3 = new ArrayList<>();
				
				for(int i: request.getRisklevel())
				{
					
					for(Report tempreport:reprortfilter2)
					{
						
						if(tempreport.getRiskLevel().getId()==i)
						{
							
							reprortfilter3.add(tempreport);
							
						}
							
						
					}
					
				}
				
				
				reprortfilter2.clear();
				Set<Report> tempset = new LinkedHashSet<>();
				tempset.addAll(reprortfilter3);
				reprortfilter3.clear();
				reprortfilter3.addAll(tempset);
				tempset.clear();
				
				//filter as per country filter
				
				List<Report> reprortfilter4 = new ArrayList<>();
				
				if(request.getCountries() != null)
				{
					for(int i:request.getCountries())
				{
					Country tempcountry = countryrepository.getOne(i);
					
					  for(Report tempreport:reprortfilter3)
					  {
						  if(tempreport.getCountries().contains(tempcountry))
						  {
							  reprortfilter4.add(tempreport);
						  }
					  }
					
					
				}
				
				}
				
				
				//filter as per State filter

				
				
				if(request.getStates() != null)
				{
					for(int i:request.getStates())
				{
					State tempstate = staterepository.getOne(i);
					
					  for(Report tempreport:reprortfilter3)
					  {
						  if(tempreport.getState().contains(tempstate))
						  {
							  reprortfilter4.add(tempreport);
						  }
					  }
					
					
				}
				
				}
				
				
				//filter as per City filter
				
				
				
				if(request.getCities() != null)
				{
					for(int i:request.getCities())
				{
					City tempcity = cityrepository.getOne(i);
					
					  for(Report tempreport:reprortfilter3)
					  {
						  if(tempreport.getCities().contains(tempcity))
						  {
							  reprortfilter4.add(tempreport);
						  }
					  }
					
					
				}
				
				}
				reprortfilter3.clear();
			
				tempset.addAll(reprortfilter4);
				reprortfilter4.clear();
				reprortfilter4.addAll(tempset);
				tempset.clear();
				
				List<ArchiveEventsResponseObject> response = new ArrayList<ArchiveEventsResponseObject>();
				
				for(Report tempreport:reprortfilter4)
				{
					
					ArchiveEventsResponseObject temp = new ArchiveEventsResponseObject();
					temp.setId(tempreport.getId());
					temp.setTitle(tempreport.getTitle());
					temp.setRiskcategory(tempreport.getRiskCategory().getId());
					temp.setRisklevel(tempreport.getRiskLevel().getId());
					
					response.add(temp);
				}
					
					
		return response;
	}
	
	
	
	
	@Override
	public PagedResponse<ArchiveEventsResponseObject> getArchiveEventsNew(UserPrincipal userprincipal,
			ArchiveEventsRequestObject request,int pageno) {
		
		ClientProfile clientprofile = clientdetailservice.getClientDetails(userprincipal.getId());
		
		Pageable pageable = PageRequest.of(pageno, 10, Sort.Direction.DESC, "validitydate");
		
		
		Instant StartDate = LocalDate.parse(request.getStartdate()).atStartOfDay(ZoneId.of("Asia/Calcutta")).toInstant();
		Instant EndDate =LocalDate.parse(request.getEnddate()).atStartOfDay(ZoneId.of("Asia/Calcutta")).toInstant();
		EndDate = EndDate.plus(1, ChronoUnit.DAYS);
		List<Long> risklevels = new ArrayList<>();
		
		for(int i:request.getRisklevel())
		{
			Long j = (long) i;
			risklevels.add(j);
		}
		
		List<Long> riskcategories = new ArrayList<>();
		for(int i:request.getRiskcategory())
		{
			Long j = (long) i;
			riskcategories.add(j);
		}
		
		List<Long> countries = new ArrayList<>();
		List<Long> states = new ArrayList<>();
		List<Long> cities = new ArrayList<>();
		
		
		
		if(request.getCountries() != null && !request.getCountries().isEmpty())
		{
	    for(int i:request.getCountries())
		
		{
			Long j = (long) i;
			
		 //   Country temp = countryrepository.getOne(j);
		 //   if(clientprofile.getCountries().contains(temp))
			{countries.add(j);}
		}
		}
		
		
		
		if(request.getStates() != null && !request.getStates().isEmpty())
		{
		for(int i:request.getStates())
		{
			
			Long j = (long) i;
		 //   State temp = staterepository.getOne(j);
		 //   if(clientprofile.getState().contains(temp))
			{states.add(j);}
		}
		}
		
		
		
		if(request.getCities() != null && !request.getCities().isEmpty())
		{
		for(int i:request.getCities() )
		{
			Long j = (long) i;
		//    City temp = cityrepository.getOne(j);
		//    if(clientprofile.getCities().contains(temp))
			{cities.add(j);}
		}
		}
		
		
		
		Long status =(long)2;
		
		int a = 0;
		int b = 0; 
		int c = 0;
		
		if(countries != null && !countries.isEmpty())
		{a = 1;}
		if(states != null && !states.isEmpty())
		{b = 1;}
		if(cities != null && !cities.isEmpty())
		{c = 1;}	
		
		Page<Report> reports = new PageImpl<>(Collections.emptyList());
		
		
	
		
		try {
		if(a == 0 && b == 0 && c ==0 && ((request.getCountries() != null && !request.getCountries().isEmpty())|| (request.getStates() != null && !request.getStates().isEmpty()) || (request.getCities() != null && !request.getCities().isEmpty())))
		{}
		else
		{	
		if(a== 0)
		{
			if(b==0 && c > 0)
			{	
				
				reports = reportrepository.getTerminalReportsNew1(pageable,StartDate,EndDate,risklevels,status,riskcategories,cities);}
			else if(c == 0 && b > 0)
			{ 
				
				reports = reportrepository.getTerminalReportsNew2(pageable,StartDate,EndDate,risklevels,status,riskcategories,states);}
			else if (b ==0 && c == 0)
			{ 
				
				reports = reportrepository.getTerminalReportsNew3(pageable,StartDate,EndDate,risklevels,status,riskcategories);}	
			else
			{  
				
				reports = reportrepository.getTerminalReportsNew4(pageable,StartDate,EndDate,risklevels,status,riskcategories,states,cities);}	
		}
		else
		{
			if(b ==0 && c > 0)
			{ 
				
				reports = reportrepository.getTerminalReportsNew5(pageable,StartDate,EndDate,risklevels,status,riskcategories,countries,cities);}
			else if(c==0 && b>0)
			{
				
				reports = reportrepository.getTerminalReportsNew6(pageable,StartDate,EndDate,risklevels,status,riskcategories,countries,states);}
			else if (b ==0 && c ==0)
			{ 
				
				reports = reportrepository.getTerminalReportsNew7(pageable,StartDate,EndDate,risklevels,status,riskcategories,countries);}	
			else
			{ 
				
				reports = reportrepository.getTerminalReportsNew(pageable,StartDate,EndDate,risklevels,status,riskcategories,countries,states,cities);}	
			 
		}
		
		}
		
		}catch(Exception e) {e.printStackTrace();}
		if(reports.getNumberOfElements() == 0) {
	        return new PagedResponse<>(Collections.emptyList(), reports.getNumber(),
	        		reports.getSize(), reports.getTotalElements(), reports.getTotalPages(), reports.isLast());
	    }
		
		
		
		else
		{
		List<ArchiveEventsResponseObject> response = new ArrayList<ArchiveEventsResponseObject>();
		
		for(Report tempreport:reports)
		{
			
			ArchiveEventsResponseObject temp = new ArchiveEventsResponseObject();
			temp.setId(tempreport.getId());
			temp.setTitle(tempreport.getTitle());
			temp.setRiskcategory(tempreport.getRiskCategory().getId());
			temp.setRisklevel(tempreport.getRiskLevel().getId());
			
			response.add(temp);
		}
			
			
		return new PagedResponse<>(response, reports.getNumber(),
	    		reports.getSize(), reports.getTotalElements(), reports.getTotalPages(), reports.isLast());
		}
		
	}
	
	

	@Override
	public List<TopBarResponseObject> getTopBar(List<TerminalResponseObjet> teminalobject) {
		
		List<RiskCategory> riskcategories = riskcategoryrepository.findAll();
		List<TopBarResponseObject> response = new ArrayList<>();
		for(RiskCategory tempcategory:riskcategories)
		{
			TopBarResponseObject tempresponse = new TopBarResponseObject();
			tempresponse.setRiskcategoryid((int) tempcategory.getId());
			tempresponse.setRiskCategory(tempcategory.getRiskcategory());
			int i =0;
		for(TerminalResponseObjet temp:teminalobject)
		{
			if(temp.getRiskcategory() == tempcategory.getId())
			{
				i = i+1;
			}
		}
		
			tempresponse.setNumberofevents(i);
			response.add(tempresponse);
		}
		return response;
	}

}
