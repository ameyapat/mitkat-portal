package mitkat.portal.admin.service;


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import mitkat.portal.admin.customeException.ResourceNotFoundException;
import mitkat.portal.admin.model.clientprofile.ClientProfile;
import mitkat.portal.admin.model.creator.RiskAlert;
import mitkat.portal.admin.model.user.User;
import mitkat.portal.admin.repository.application.ClientProfileRepository;
import mitkat.portal.admin.repository.application.RiskAlertRepository;
import mitkat.portal.admin.repository.user.UserRepository;
import mitkat.portal.admin.requestObjects.approver.SMSRequest;

@Service
public class SMSService {

	@Autowired
	RiskAlertRepository riskalertrepository;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	ClientProfileRepository clientprofilerepository;
	
	public void sendSMS(@Valid SMSRequest request) {
		
		
		RiskAlert riskalert = riskalertrepository.getOne(request.getSmsid());
		
		List<User> allUsers =  userRepository.findAll();
    	List<User> outUsers = new ArrayList<User>();
    	for(User tempUser:allUsers)
    	{
    		if(tempUser.getRoles().iterator().next().getId()==1)
    		{
    			outUsers.add(tempUser);
    		}
    		
    	}
		
    	List<String> number =new ArrayList<>();
		
    	for(User tempuser: outUsers)
    	{
    		try {
    		ClientProfile profile = clientprofilerepository.findByclientid(tempuser.getId()).orElseThrow(
                    () -> new ResourceNotFoundException("Client", "id", tempuser.getId()));
    		
    		if(riskalert.getCountries() != null && profile.getCountries() != null)
    		{
    			if(!Collections.disjoint(riskalert.getCountries(),profile.getCountries()))
    			{
    				number.add(profile.getContactnumber());	
    			}
    		}
    		
    		if(riskalert.getState() != null && profile.getState() != null)
    		{
    			if(!Collections.disjoint(riskalert.getState(),profile.getState()))
    			{
    				number.add(profile.getContactnumber());	
    			}
    		}
    		
    		if(riskalert.getCities() != null && profile.getCities() != null)
    		{
    			if(!Collections.disjoint(riskalert.getCities(),profile.getCities()))
    			{
    				number.add(profile.getContactnumber());	
    			}
    		}
    		}catch(Exception e) {e.printStackTrace();}
    		
    	}
    	
    	Set<String> finalnumbers = new HashSet<>();
    	finalnumbers.addAll(number);
    	number.clear();
    	number.addAll(finalnumbers);
    	finalnumbers.clear();
    	
    	int size = number.size();
    	int i = 0;
    	
    	
    	if(size >100)
    	{
    	while(size > 100)
    	{
    		List<String> newnumber = new ArrayList<>();
    		int j =0;
    		while(j < 100)
    		{
    			
    			newnumber.add(number.get(i+j));
    			j = j+1;
    		}	
    		
    		String outputnumbers = String.join(",", newnumber);
        	
    		final String uri = "http://203.212.70.200/smpp/sendsms?username=DEMO28NEWHTTP&password=rock@878&to="+outputnumbers+"&from=MITKAT&text="+riskalert.getTitle()+"&category=bulk";
    	     
    	    RestTemplate restTemplate = new RestTemplate();
    	   
    	    String result = restTemplate.getForObject(uri, String.class);
    		
    		i = i +100;
    	    size = size-100;
    	}
    	}
    	
    	List<String> newnumber = new ArrayList<>();
    	int j = 0;
    	while(j < size)
    	{
    		newnumber.add(number.get(i+j));
			j = j+1;
    	}	
		
    	String outputnumbers = String.join(",", newnumber);
    
		final String uri = "http://203.212.70.200/smpp/sendsms?username=DEMO28NEWHTTP&password=rock@878&to="+outputnumbers+"&from=MITKAT&text="+riskalert.getTitle()+"&category=bulk";
	     
	    RestTemplate restTemplate = new RestTemplate();
	   
	    String result = restTemplate.getForObject(uri, String.class);
	    
	    
	    
	}
	
	
	

}
