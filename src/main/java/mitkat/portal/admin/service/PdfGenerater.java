package mitkat.portal.admin.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.function.Supplier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.html.WebColors;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import mitkat.portal.admin.customeException.ResourceNotFoundException;
import mitkat.portal.admin.model.creator.BackgroundBullets;
import mitkat.portal.admin.model.creator.DescriptionBullets;
import mitkat.portal.admin.model.creator.ImpactBullets;
import mitkat.portal.admin.model.creator.RecommendationBullet;
import mitkat.portal.admin.model.creator.Report;
import mitkat.portal.admin.repository.application.CityRepository;
import mitkat.portal.admin.repository.application.Countryrepository;
import mitkat.portal.admin.repository.application.ReportRepository;
import mitkat.portal.admin.repository.application.RiskAlertRepository;
import mitkat.portal.admin.repository.application.RiskCategoryRepository;
import mitkat.portal.admin.repository.application.RiskLevelRepository;
import mitkat.portal.admin.repository.application.StateRepository;

@Service
public class PdfGenerater {

	@Autowired
	ClientDetailService clientdetailservice;
	
	@Autowired
	RiskCategoryRepository riskcategoryrepository;
	
	@Autowired
	RiskLevelRepository risklevelrepository;
	
	@Autowired
	ReportRepository reportrepository;
	
	@Autowired
	RiskAlertRepository riskalertrepository;
	
	@Autowired
	Countryrepository countryrepository;
	
	@Autowired
	StateRepository staterepository;
	
	@Autowired
	CityRepository cityrepository;

	public ByteArrayInputStream generateEventReport(long eventid) throws DocumentException, MalformedURLException, IOException {
		
		Report report = reportrepository.findById(eventid).orElseThrow(() -> new ResourceNotFoundException("Report","Report Id",eventid));
		
		BaseColor greyColor = WebColors.getRGBColor("#f2f2f2");
		BaseColor veryHigh = WebColors.getRGBColor("#E48F6F");
		BaseColor high = WebColors.getRGBColor("#FCD293");
		BaseColor medium = WebColors.getRGBColor("#FEEA81");
		BaseColor low = WebColors.getRGBColor("#D2E3A4");
		BaseColor verylow = WebColors.getRGBColor("#EBF7C9");
		BaseColor blue = WebColors.getRGBColor("#0070c0");
		BaseColor mitkatbluefaintplus = WebColors.getRGBColor("#EAEAEA");
		
		
		Font fontcalibriblackmid = FontFactory.getFont("Calibri", 12);
		Font fontcalibriblack = FontFactory.getFont("Calibri", 8);
		Font basefont = FontFactory.getFont(FontFactory.TIMES_ROMAN,8f);
		Font boldbasefont = FontFactory.getFont(FontFactory.TIMES_BOLD,8f);
		boldbasefont.setColor(blue);
		
		Font titlefont = FontFactory.getFont(FontFactory.TIMES_BOLD,8f);
		
		Document document = new Document();
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        PdfWriter.getInstance(document, out);
        document.open();
        
        //header
        try {
        PdfPTable table1 = new PdfPTable(1);
        table1.setWidthPercentage(100);
        PdfPCell t1c1 = new PdfPCell ();
        t1c1.setPadding(0);
        Image header = Image.getInstance("header.jpg");
        t1c1.addElement(header);
        table1.addCell(t1c1);
        document.add(table1);
        }catch(Exception e) {}
        
        
        // Title Table
        PdfPTable table2 = new PdfPTable(2);
        table2.setWidthPercentage(100);
        table2.setWidths(new int[]{1,4});

        /*
        Image crimeImage = Image.getInstance("crime.png");
        PdfPCell t2c1 = new PdfPCell ();
    	t2c1.addElement(crimeImage);
    	table2.addCell(t2c1);
       */
        
    	PdfPCell t2c2 = new PdfPCell ();
    	t2c2.setColspan(2);
    	t2c2.setPaddingBottom(10);
        t2c2.setBackgroundColor(greyColor);
        Paragraph pr1t2c2 = new Paragraph(report.getTitle(),fontcalibriblackmid);
        pr1t2c2.setAlignment(Element.ALIGN_CENTER);
        t2c2.addElement(pr1t2c2);
        table2.addCell(t2c2);
        
        
        PdfPCell t2new1 = new PdfPCell ();
        t2new1.setPaddingBottom(10);
        t2new1.setBackgroundColor(mitkatbluefaintplus);
        Paragraph t2pnew1 = new Paragraph("Event relevent till date",fontcalibriblack);
        t2pnew1.setAlignment(Element.ALIGN_CENTER);
        t2new1.addElement(t2pnew1);
        table2.addCell(t2new1);
        
        Instant instantdate = report.getValiditydate().plus(1,ChronoUnit.DAYS);
        LocalDate date = LocalDateTime.ofInstant(instantdate, ZoneOffset.UTC).toLocalDate();
        
        PdfPCell t2new2 = new PdfPCell ();
        t2new2.setPaddingBottom(10);
        t2new2.setPaddingLeft(5);
        Paragraph t2pnew2 = new Paragraph(date.toString(),fontcalibriblack);
        t2new2.addElement(t2pnew2);
        table2.addCell(t2new2); 
        
        PdfPCell t2new3 = new PdfPCell ();
        t2new3.setPaddingBottom(10);
        t2new3.setBackgroundColor(mitkatbluefaintplus);
        Paragraph t2pnew3 = new Paragraph("Risk Category",fontcalibriblack);
        t2pnew3.setAlignment(Element.ALIGN_CENTER);
        t2new3.addElement(t2pnew3);
        table2.addCell(t2new3);
        
        PdfPCell t2new4 = new PdfPCell ();
        t2new4.setPaddingBottom(10);
        t2new4.setPaddingLeft(5);
        Paragraph t2pnew4 = new Paragraph(report.getRiskCategory().getRiskcategory(),fontcalibriblack);
        t2new4.addElement(t2pnew4);
        table2.addCell(t2new4); 
        
        PdfPCell t2new5 = new PdfPCell ();
        t2new5.setPaddingBottom(10);
        t2new5.setBackgroundColor(mitkatbluefaintplus);
        Paragraph t2pnew5 = new Paragraph("Risk Level",fontcalibriblack);
        t2pnew5.setAlignment(Element.ALIGN_CENTER);
        t2new5.addElement(t2pnew5);
        table2.addCell(t2new5);
        
        PdfPCell t2c3 = new PdfPCell ();
        t2c3.setVerticalAlignment(Element.ALIGN_MIDDLE);
        t2c3.setPaddingBottom(10);
        t2c3.setPaddingLeft(5);
        if(report.getRiskLevel().getId()==1)
        {
        t2c3.setBackgroundColor(verylow);
        Paragraph temp = new Paragraph("Very Low",fontcalibriblack);
        //temp.setAlignment(Element.ALIGN_CENTER);
        t2c3.addElement(temp);
        }
        else if(report.getRiskLevel().getId()==2)
        {
        t2c3.setBackgroundColor(low);
        Paragraph temp = new Paragraph("Low",fontcalibriblack);
        //temp.setAlignment(Element.ALIGN_CENTER);
        t2c3.addElement(temp);
        }	
        else if(report.getRiskLevel().getId()==3)
        {
        t2c3.setBackgroundColor(medium);
        Paragraph temp = new Paragraph("Medium",fontcalibriblack);
        //temp.setAlignment(Element.ALIGN_CENTER);
        t2c3.addElement(temp);
        }
        else if(report.getRiskLevel().getId()==4)
        {
        t2c3.setBackgroundColor(high);
        Paragraph temp = new Paragraph("High",fontcalibriblack);
        //temp.setAlignment(Element.ALIGN_CENTER);
        t2c3.addElement(temp);
        }	
        else if(report.getRiskLevel().getId()==5)
        {
        t2c3.setBackgroundColor(veryHigh);
        Paragraph temp = new Paragraph("Very High",fontcalibriblack);
        //temp.setAlignment(Element.ALIGN_CENTER);
        t2c3.addElement(temp);
        }
        Paragraph pr1t2c3 = new Paragraph("",titlefont);
        t2c3.addElement(pr1t2c3);
        table2.addCell(t2c3);
        
        document.add(table2);
        
        //report elements table
        
        PdfPTable table3 = new PdfPTable(2);
        table3.setWidthPercentage(100);
        table3.setWidths(new int[]{1,4});
        
        if(report.getDescription() != null && !report.getDescription().equalsIgnoreCase("na"))
        {
        	PdfPCell t3c1 = new PdfPCell ();
        	t3c1.setBackgroundColor(mitkatbluefaintplus);
        	t3c1.setPaddingBottom(10);
        	Paragraph pr1t3c1 = new Paragraph("Description",fontcalibriblack);
        	pr1t3c1.setAlignment(Element.ALIGN_CENTER);
        	t3c1.addElement(pr1t3c1);
        	table3.addCell(t3c1);
        	
        	PdfPCell t3c2 = new PdfPCell ();
        	t3c2.setPaddingBottom(10);
        	t3c2.setPaddingLeft(5);
        	Paragraph pr1t3c2 = new Paragraph(report.getDescription(),fontcalibriblack);
        	t3c2.addElement(pr1t3c2);
        	
        	if(report.getDescription2() != null)
        	{	
        	Paragraph dec1 = new Paragraph(" ",fontcalibriblack);
        	Paragraph dec2 = new Paragraph(report.getDescription2(),fontcalibriblack);
        	t3c2.addElement(dec1);
        	t3c2.addElement(dec2);
        	}
        	
        	if(report.getDescriptionbullets() != null)
        	{
        		Paragraph temp1 = new Paragraph(" ",fontcalibriblack);
    			t3c2.addElement(temp1);
        		
        		for(DescriptionBullets tempbullets: report.getDescriptionbullets())
        		{	
        			
        			Paragraph temp2 = new Paragraph("- "+tempbullets.getBulletpoint(),fontcalibriblack);
        			t3c2.addElement(temp2);
        		}
        		
        		
        	}
        	
        	table3.addCell(t3c2);
         	
        }
        
        if(report.getBackground() != null && !report.getBackground().equalsIgnoreCase("na"))
        {
        	PdfPCell t3c3 = new PdfPCell ();
        	t3c3.setBackgroundColor(mitkatbluefaintplus);
        	t3c3.setPaddingBottom(10);
        	Paragraph pr1t3c3 = new Paragraph("Background",fontcalibriblack);
        	pr1t3c3.setAlignment(Element.ALIGN_CENTER);
        	t3c3.addElement(pr1t3c3);
        	table3.addCell(t3c3);
        	
        	PdfPCell t3c4 = new PdfPCell ();
        	t3c4.setPaddingBottom(10);
        	t3c4.setPaddingLeft(5);
        	Paragraph pr1t3c4 = new Paragraph(report.getBackground(),fontcalibriblack);
        	t3c4.addElement(pr1t3c4);
        	
        	if(report.getBackground2() != null)
        	{	
        	Paragraph dec1 = new Paragraph(" ",fontcalibriblack);
        	Paragraph dec2 = new Paragraph(report.getBackground2(),fontcalibriblack);
        	t3c4.addElement(dec1);
        	t3c4.addElement(dec2);
        	}
        	
        	if(report.getBackgroundbullets() != null)
        	{
        		Paragraph temp1 = new Paragraph(" ",fontcalibriblack);
    			t3c4.addElement(temp1);
        		
        		for(BackgroundBullets tempbullets: report.getBackgroundbullets())
        		{	
        			
        			Paragraph temp2 = new Paragraph("- "+tempbullets.getBulletpoint(),fontcalibriblack);
        			t3c4.addElement(temp2);
        		}
        		
        		
        	}
        	
        	table3.addCell(t3c4);
         	
        } 
        
        if(report.getImpact() != null && !report.getImpact().equalsIgnoreCase("na"))
        {
        	PdfPCell t3c5 = new PdfPCell ();
        	t3c5.setBackgroundColor(mitkatbluefaintplus);
        	t3c5.setPaddingBottom(10);
        	Paragraph pr1t3c5 = new Paragraph("Impact Analysis",fontcalibriblack);
        	pr1t3c5.setAlignment(Element.ALIGN_CENTER);
        	t3c5.addElement(pr1t3c5);
        	table3.addCell(t3c5);
        	
        	PdfPCell t3c6 = new PdfPCell ();
        	t3c6.setPaddingBottom(10);
        	t3c6.setPaddingLeft(5);
        	Paragraph pr1t3c6 = new Paragraph(report.getImpact(),fontcalibriblack);
        	t3c6.addElement(pr1t3c6);
        	
        	if(report.getImpact2() != null)
        	{	
        	Paragraph dec1 = new Paragraph(" ",fontcalibriblack);
        	Paragraph dec2 = new Paragraph(report.getImpact2(),fontcalibriblack);
        	t3c6.addElement(dec1);
        	t3c6.addElement(dec2);
        	}
        	
        	if(report.getImpactbullets() != null)
        	{
        		Paragraph temp1 = new Paragraph(" ",fontcalibriblack);
    			t3c6.addElement(temp1);
    			
        		for(ImpactBullets tempbullets: report.getImpactbullets())
        		{	
        			
        			Paragraph temp2 = new Paragraph("- "+tempbullets.getBulletpoint(),fontcalibriblack);
        			t3c6.addElement(temp2);
        		}
        		
        		
        	}
        	
        	table3.addCell(t3c6);
         	
        } 
          
        if(report.getRecommendation() != null && !report.getRecommendation().equalsIgnoreCase("na"))
        {
        	PdfPCell t3c7 = new PdfPCell ();
        	t3c7.setBackgroundColor(mitkatbluefaintplus);
        	t3c7.setPaddingBottom(10);
        	Paragraph pr1t3c7 = new Paragraph("Recommendation",fontcalibriblack);
        	pr1t3c7.setAlignment(Element.ALIGN_CENTER);
        	t3c7.addElement(pr1t3c7);
        	table3.addCell(t3c7);
        	
        	PdfPCell t3c8 = new PdfPCell ();
        	t3c8.setPaddingBottom(10);
        	t3c8.setPaddingLeft(5);
        	Paragraph pr1t3c8 = new Paragraph(report.getRecommendation(),fontcalibriblack);
        	t3c8.addElement(pr1t3c8);
        	
        	if(report.getRecommendation2() != null)
        	{	
        	Paragraph dec1 = new Paragraph(" ",fontcalibriblack);
        	Paragraph dec2 = new Paragraph(report.getRecommendation2(),fontcalibriblack);
        	t3c8.addElement(dec1);
        	t3c8.addElement(dec2);
        	}
        	
        	if(report.getRecommendationbullets() != null)
        	{
        		
        		for(RecommendationBullet tempbullets: report.getRecommendationbullets())
        		{	
        			Paragraph temp1 = new Paragraph(" ",fontcalibriblack);
        			t3c8.addElement(temp1);
        			Paragraph temp2 = new Paragraph("* "+tempbullets.getBulletpoint(),fontcalibriblack);
        			t3c8.addElement(temp2);
        		}
        		
        		
        	}
        	
        	table3.addCell(t3c8);
         	
        } 
        document.add(table3);
        
        //footer
        /**
        try {
        PdfPTable table4 = new PdfPTable(1);
        table4.setWidthPercentage(100);
        PdfPCell t4c1 = new PdfPCell ();
        t4c1.setPadding(0);
        Image footer = Image.getInstance("footer.jpg");
        footer.setAlignment(Element.ALIGN_CENTER);
        t4c1.addElement(footer);
        table4.addCell(t4c1);
        
        document.add(table4);
        }catch(Exception e) {}
        
        **/
        document.close();
        
        
		
        return new ByteArrayInputStream(out.toByteArray());
	}
	
	
}
