package mitkat.portal.admin.service;

import java.util.Comparator;

import mitkat.portal.admin.model.creator.Report;
import mitkat.portal.admin.model.creator.RiskAlert;



public class DateCompareSMS implements Comparator<RiskAlert> {

	@Override
	public int compare(RiskAlert message1, RiskAlert message2) {
		
		return message2.getUpdatedAt().compareTo(message1.getUpdatedAt());
	}

}