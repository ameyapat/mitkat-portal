package mitkat.portal.admin.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mitkat.portal.admin.model.clientprofile.ClientProfile;
import mitkat.portal.admin.model.clientprofile.ProductType;
import mitkat.portal.admin.repository.application.ClientProfileRepository;
import mitkat.portal.admin.repository.application.ProductTypeRepository;
import mitkat.portal.admin.responseObjects.approver.RiskTrackerType;

@Service
public class RiskTrackerTypeServiceImplementation implements RiskTrackerTypeService {

	@Autowired
	ProductTypeRepository producttyperepository;
	
	@Autowired
	ClientProfileRepository clientprofilerepository;
	
	@Override
	public List<RiskTrackerType> getAll() {
		
		List<ClientProfile> clientprofiles = clientprofilerepository.findAll();
		ProductType producttype = producttyperepository.getOne((long) 1);
		List<RiskTrackerType> response = new ArrayList<>();
		for(ClientProfile temp:clientprofiles)
		{
			if(temp.getProducttype().contains(producttype))
			{}
			else
			{
				RiskTrackerType type = new RiskTrackerType();
				type.setId(temp.getClientid());
				type.setTrackerName(temp.getCompanyname());
				response.add(type);
			}	
		}
		
		RiskTrackerType typeirt = new RiskTrackerType();
		typeirt.setId(0);
		typeirt.setTrackerName("IRT");
		response.add(typeirt);
		return response;
	}

}
