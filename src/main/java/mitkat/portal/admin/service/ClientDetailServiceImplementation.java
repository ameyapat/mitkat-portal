package mitkat.portal.admin.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mitkat.portal.admin.customeException.ResourceNotFoundException;
import mitkat.portal.admin.model.clientprofile.City;
import mitkat.portal.admin.model.clientprofile.ClientEmailIds;
import mitkat.portal.admin.model.clientprofile.ClientProfile;
import mitkat.portal.admin.model.clientprofile.Country;
import mitkat.portal.admin.model.clientprofile.OfficeLocation;
import mitkat.portal.admin.model.clientprofile.ProductType;
import mitkat.portal.admin.model.clientprofile.State;
import mitkat.portal.admin.model.creator.ImpactIndustry;
import mitkat.portal.admin.repository.application.CityRepository;
import mitkat.portal.admin.repository.application.ClientEmailIdsRepository;
import mitkat.portal.admin.repository.application.ClientProfileRepository;
import mitkat.portal.admin.repository.application.Countryrepository;
import mitkat.portal.admin.repository.application.ImpactIndustryRepository;
import mitkat.portal.admin.repository.application.ProductTypeRepository;
import mitkat.portal.admin.repository.application.StateRepository;
import mitkat.portal.admin.requestObjects.clientprofile.ClientProfileRequestObject;
import mitkat.portal.admin.savefiles.FileStorageService;
import mitkat.portal.admin.security.UserPrincipal;

@Service
public class ClientDetailServiceImplementation implements ClientDetailService {
	
	@Autowired
	ClientProfileRepository clientprofilerepository;
	
	@Autowired
	ClientEmailIdsRepository clientemailrepository;
	
	@Autowired
	Countryrepository countryrepository;
	
	@Autowired
	StateRepository staterepository;
	
	@Autowired
	CityRepository cityrepository;
	
	@Autowired
	ProductTypeRepository producttyperepository;
	
	@Autowired
	ImpactIndustryRepository impactindustryrepository;
	
	@Autowired
    FileStorageService fileStorageService;
	
	@Override
	public ClientProfile uploadClientDetails(@Valid ClientProfileRequestObject clientprofilerequest,
			UserPrincipal userprincipal) {
		ClientProfile profile = new ClientProfile();
		
	 	if(clientprofilerepository.existsByclientid(clientprofilerequest.getClientid()))
	 	{  profile = clientprofilerepository.findByclientid(clientprofilerequest.getClientid()).get();}
	 	
	 	else {
		 profile.setClientid(clientprofilerequest.getClientid());}
		 
		 if(clientprofilerequest.getCompanyname() != null)
		 {profile.setCompanyname(clientprofilerequest.getCompanyname());}
		 
		 if(clientprofilerequest.getContractdate() != null)
		 {profile.setContractdate(clientprofilerequest.getContractdate());}
		
		 if(clientprofilerequest.getContractenddate() != null)
		 {profile.setContractenddate(clientprofilerequest.getContractenddate());}
		 
		 if(clientprofilerequest.getContactnumber() != null) 
		 {profile.setContactnumber(clientprofilerequest.getContactnumber());}
		 
		 if(clientprofilerequest.getContactperson() != null) 
		 profile.setContactperson(clientprofilerequest.getContactperson());
		 
		 if(clientprofilerequest.getLogo() != null)
		 {
			 
			 String fileName = fileStorageService.storeFile(clientprofilerequest.getLogo(),clientprofilerequest.getCompanyname()); 
			 profile.setFilename(fileName);
		}
		 
		 if(clientprofilerequest.getLogo() != null)
		 {try {
			profile.setLogo(clientprofilerequest.getLogo().getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} }	 
		 
		 
		 if(clientprofilerequest.getClientemails() != null)
		 {
		
		 for(String tempemail:clientprofilerequest.getClientemails())
		 {
			 ClientEmailIds emailx = new ClientEmailIds();
			 emailx.setEmail(tempemail);
			
			 
			 
			 profile.addEmailIds(emailx);
		 }
		 
		 }
		 
		 if(clientprofilerequest.getOfficelocations() != null)
		 {
		 for(OfficeLocation templocation:clientprofilerequest.getOfficelocations())
		 {
			 OfficeLocation locationx = new OfficeLocation();
			 locationx.setAddress(templocation.getAddress());
			 locationx.setLatitude(templocation.getLatitude());
			 locationx.setLongitude(templocation.getLongitude());
			 locationx.setProfilelocations(profile);
			 profile.addOfficeLocations(locationx);
			 
		 }
		
		 }
		 
		 if(clientprofilerequest.getCountries() != null)
		 {List<Country> countryin = new ArrayList<Country>();
		 for(Integer tempintcountry:clientprofilerequest.getCountries())
		 {
			 Country tempcountry = countryrepository.getOne(tempintcountry);
			 countryin.add(tempcountry);
			 
		 }
		 profile.setCountries(countryin);
		 }
		 
		 
		 if(clientprofilerequest.getStates()!=null)
		 {List<State> statein = new ArrayList<State>();
		 for(Integer tempintstate:clientprofilerequest.getStates())
		 {
			 State tempstate = staterepository.getOne(tempintstate);
			 statein.add(tempstate);
			 
		 }
		 profile.setState(statein);
		 }
		 
		 if(clientprofilerequest.getCities()!=null)
		 {List<City> cityin = new ArrayList<City>();
		 for(Integer tempincity:clientprofilerequest.getCities())
		 {
			 City tempcity = cityrepository.getOne(tempincity);
			 cityin.add(tempcity);
			 
		 }
		 profile.setCities(cityin);
		 }
		 
		 if(clientprofilerequest.getProducttypes()!=null)
		 {List<ProductType> producttypein = new ArrayList<ProductType>();
		 for(long tempinproducttypein:clientprofilerequest.getProducttypes())
		 {
			 ProductType tempproducttype = producttyperepository.getOne(tempinproducttypein);
			 producttypein.add(tempproducttype);
			 
		 }
		 profile.setProducttype(producttypein);
		 }
		 
		 if(clientprofilerequest.getImpactindustry()!=null)
		 {List<ImpactIndustry> impactindustryin = new ArrayList<ImpactIndustry>();
		 for(long tempinproducttypein:clientprofilerequest.getImpactindustry())
		 {
			ImpactIndustry tempimpact = impactindustryrepository.getOne(tempinproducttypein);
			impactindustryin.add(tempimpact);
			 
		 }
		 profile.setImpactindustry(impactindustryin);
		 }
		 
		 clientprofilerepository.save(profile);
		 

		
		return null;
	}


	@Override
	public ClientProfile getClientDetails(long id) {
		ClientProfile clientprofile =clientprofilerepository.findByclientid(id).get();
		return clientprofile;
	}


	@Override
	public ClientProfile updateClientDetails(@Valid ClientProfileRequestObject clientprofilerequest,
			UserPrincipal userprincipal) {
ClientProfile profile = clientprofilerepository.findByclientid(clientprofilerequest.getClientid()).orElseThrow(() -> new ResourceNotFoundException("Client", "Client Id", clientprofilerequest.getClientid()));
		
		
		 
		 if(clientprofilerequest.getCompanyname() != null)
		 {profile.setCompanyname(clientprofilerequest.getCompanyname());}
		 
		 if(clientprofilerequest.getContractdate() != null)
		 {profile.setContractdate(clientprofilerequest.getContractdate());}
		
		 if(clientprofilerequest.getContractenddate() != null)
		 {profile.setContractenddate(clientprofilerequest.getContractenddate());}
		 
		 if(clientprofilerequest.getContactnumber() != null) 
		 {profile.setContactnumber(clientprofilerequest.getContactnumber());}
		 
		 if(clientprofilerequest.getContactperson() != null) 
		 profile.setContactperson(clientprofilerequest.getContactperson());
		 
		 if(clientprofilerequest.getLogo() != null)
		 {
			 
			 String fileName = fileStorageService.storeFile(clientprofilerequest.getLogo(),clientprofilerequest.getCompanyname()); 
			 profile.setFilename(fileName);
		}
		 
		 if(clientprofilerequest.getLogo() != null)
		 {try {
			profile.setLogo(clientprofilerequest.getLogo().getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} }	 
		 
		 
		 if(clientprofilerequest.getClientemails() != null)
		 {
			 int i = profile.getClientemail().size();
			 while(i > 0)
			 {	long j = profile.getClientemail().get(i-1).getId();
				 ClientEmailIds emailtemp = clientemailrepository.getOne(j);
				 profile.getClientemail().remove(i-1);
				 clientemailrepository.delete(emailtemp);
				 i = i -1;
			 }
		 for(String tempemail:clientprofilerequest.getClientemails())
		 {
			 ClientEmailIds emailx = new ClientEmailIds();
			 emailx.setEmail(tempemail);
			
			 profile.addEmailIds(emailx);
		 }
		 
		 }
		 
		 if(clientprofilerequest.getOfficelocations() != null)
		 {
		 for(OfficeLocation templocation:clientprofilerequest.getOfficelocations())
		 {
			 OfficeLocation locationx = new OfficeLocation();
			 locationx.setAddress(templocation.getAddress());
			 locationx.setLatitude(templocation.getLatitude());
			 locationx.setLongitude(templocation.getLongitude());
			 locationx.setProfilelocations(profile);
			 profile.addOfficeLocations(locationx);
			 
		 }
		
		 }
		 
		 if(clientprofilerequest.getCountries() != null)
		 {List<Country> countryin = new ArrayList<Country>();
		 for(Integer tempintcountry:clientprofilerequest.getCountries())
		 {
			 Country tempcountry = countryrepository.getOne(tempintcountry);
			 countryin.add(tempcountry);
			 
		 }
		 profile.setCountries(countryin);
		 }
		 
		 
		 if(clientprofilerequest.getStates()!=null)
		 {List<State> statein = new ArrayList<State>();
		 for(Integer tempintstate:clientprofilerequest.getStates())
		 {
			 State tempstate = staterepository.getOne(tempintstate);
			 statein.add(tempstate);
			 
		 }
		 profile.setState(statein);
		 }
		 
		 if(clientprofilerequest.getCities()!=null)
		 {List<City> cityin = new ArrayList<City>();
		 for(Integer tempincity:clientprofilerequest.getCities())
		 {
			 City tempcity = cityrepository.getOne(tempincity);
			 cityin.add(tempcity);
			 
		 }
		 profile.setCities(cityin);
		 }
		 
		 if(clientprofilerequest.getProducttypes()!=null)
		 {List<ProductType> producttypein = new ArrayList<ProductType>();
		 for(long tempinproducttypein:clientprofilerequest.getProducttypes())
		 {
			 ProductType tempproducttype = producttyperepository.getOne(tempinproducttypein);
			 producttypein.add(tempproducttype);
			 
		 }
		 profile.setProducttype(producttypein);
		 }
		 
		 if(clientprofilerequest.getImpactindustry()!=null)
		 {List<ImpactIndustry> impactindustryin = new ArrayList<ImpactIndustry>();
		 for(long tempinproducttypein:clientprofilerequest.getImpactindustry())
		 {
			ImpactIndustry tempimpact = impactindustryrepository.getOne(tempinproducttypein);
			impactindustryin.add(tempimpact);
			 
		 }
		 profile.setImpactindustry(impactindustryin);
		 }
		 
		 clientprofilerepository.save(profile);
		 

		
		return null;
	}

}
