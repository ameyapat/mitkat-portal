package mitkat.portal.admin.service;

import java.util.Comparator;

import mitkat.portal.admin.model.user.TokenMap;

public class DateCompare implements Comparator<TokenMap> {

	@Override
	public int compare(TokenMap message1, TokenMap message2) {
		
		return message1.getTokentime().compareTo(message2.getTokentime());
	}

}
