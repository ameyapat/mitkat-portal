package mitkat.portal.admin.service;

import java.util.Comparator;

import mitkat.portal.admin.model.creator.Report;



public class DateCompareReport implements Comparator<Report> {

	@Override
	public int compare(Report message1, Report message2) {
		
		return message2.getUpdatedAt().compareTo(message1.getUpdatedAt());
	}

}