package mitkat.portal.admin.service;

import java.util.List;

import javax.validation.Valid;

import mitkat.portal.admin.model.creator.RiskAlert;
import mitkat.portal.admin.requestObjects.creator.RiskAlertRequestObject;
import mitkat.portal.admin.responseObjects.approver.TodaysSMSResponse;
import mitkat.portal.admin.responseObjects.client.RiskAlertDetailsResponse;
import mitkat.portal.admin.responseObjects.creator.SMSAlertResponseApprover;
import mitkat.portal.admin.security.UserPrincipal;

public interface RiskAlertService {

	void addRiskAlert(@Valid RiskAlertRequestObject request, UserPrincipal userprinciple);

	RiskAlertDetailsResponse getRiskAlert(long alertid);

	SMSAlertResponseApprover converToAlertResponseApprover(RiskAlert riskalert);

	void updateRiskAlert(@Valid RiskAlertRequestObject request, UserPrincipal userprinciple);

	List<TodaysSMSResponse> getTodaysDispatchList();

	
	
	
}
