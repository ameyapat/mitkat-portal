package mitkat.portal.admin.service;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mitkat.portal.admin.customeException.ResourceNotFoundException;
import mitkat.portal.admin.model.clientprofile.City;
import mitkat.portal.admin.model.clientprofile.Country;
import mitkat.portal.admin.model.clientprofile.State;
import mitkat.portal.admin.model.creator.BackgroundBullets;
import mitkat.portal.admin.model.creator.DescriptionBullets;
import mitkat.portal.admin.model.creator.ImpactBullets;
import mitkat.portal.admin.model.creator.ImpactIndustry;
import mitkat.portal.admin.model.creator.RecommendationBullet;
import mitkat.portal.admin.model.creator.Report;
import mitkat.portal.admin.model.creator.Update;
import mitkat.portal.admin.repository.application.BackgroundBulletsRepository;
import mitkat.portal.admin.repository.application.CityRepository;
import mitkat.portal.admin.repository.application.Countryrepository;
import mitkat.portal.admin.repository.application.DescriptionBulletsRepository;
import mitkat.portal.admin.repository.application.ImpactBulletRepository;
import mitkat.portal.admin.repository.application.ImpactIndustryRepository;
import mitkat.portal.admin.repository.application.ProductTypeRepository;
import mitkat.portal.admin.repository.application.RecommendationBulletsRepository;
import mitkat.portal.admin.repository.application.RegionTypeRepository;
import mitkat.portal.admin.repository.application.ReportRepository;
import mitkat.portal.admin.repository.application.RiskCategoryRepository;
import mitkat.portal.admin.repository.application.RiskLevelRepository;
import mitkat.portal.admin.repository.application.StateRepository;
import mitkat.portal.admin.repository.application.StatusRepository;
import mitkat.portal.admin.repository.application.UpdateRepository;
import mitkat.portal.admin.requestObjects.creator.EventRequestObject;
import mitkat.portal.admin.requestObjects.creator.UpdateRequestObject;
import mitkat.portal.admin.responseObjects.approver.EventDetailResponseApprover;
import mitkat.portal.admin.responseObjects.creator.EventDetailsResponseCreator;
import mitkat.portal.admin.security.UserPrincipal;

@Service
public class ReportServiceImplementation implements ReportService {


	@Autowired
	Countryrepository countryrepository;
	
	@Autowired
	StateRepository staterepository;
	
	@Autowired
	CityRepository cityrepository;
	
	@Autowired
	ProductTypeRepository producttyperepository;
	
	@Autowired
	ImpactIndustryRepository impactindustryrepository;
	
	@Autowired
	RegionTypeRepository regiontyperepository;
	
	@Autowired
	ReportRepository reportrepository;
	
	@Autowired
	RiskCategoryRepository riskcategoryrepository;
	
	@Autowired
	RiskLevelRepository risklevelrepository;
	
	@Autowired
	StatusRepository statusrepository;
	
	@Autowired
	UpdateRepository updaterepository;
	
	@Autowired
	DescriptionBulletsRepository descriptionbulletsrepository;
	
	@Autowired
	BackgroundBulletsRepository backgroundbulletsrepository;
	
	@Autowired
	ImpactBulletRepository impactbulletrepository;
	
	@Autowired
	RecommendationBulletsRepository recommendationbulletrepository;
	
	@Override
	public void addNewReport(@Valid EventRequestObject eventrequest,UserPrincipal currentUser) {
		
		
		Report event = new Report();
		
		event.setTitle(eventrequest.getTitle());
		
		if(eventrequest.getDescription() != null)
		{event.setDescription(eventrequest.getDescription());}
		
		if(eventrequest.getDescription2() != null)
		{event.setDescription2(eventrequest.getDescription2());}
			
		if(eventrequest.getDescriptionbullets() != null)
		{
			for(String tempbullet: eventrequest.getDescriptionbullets())
			{
				DescriptionBullets bullet = new DescriptionBullets();
				bullet.setBulletpoint(tempbullet);
				
				event.addDescriptionBullet(bullet);
			}
		}
		
		if(eventrequest.getBackground() != null)
		{event.setBackground(eventrequest.getBackground());}
		
		if(eventrequest.getBackground2() != null)
		{event.setBackground2(eventrequest.getBackground2());}
		
		if(eventrequest.getBackgroundbullets() != null)
		{
			for(String tempbullet: eventrequest.getBackgroundbullets())
			{
				BackgroundBullets bullet = new BackgroundBullets();
				bullet.setBulletpoint(tempbullet);
				
				event.addBackgroundBullet(bullet);
			}
		}
		
		if(eventrequest.getImpact() != null)
		{event.setImpact(eventrequest.getImpact());}
		
		if(eventrequest.getImpact2() != null)
		{event.setImpact2(eventrequest.getImpact2());}
		
		if(eventrequest.getImpactbullets() != null)
		{
			for(String tempbullet: eventrequest.getImpactbullets())
			{
				ImpactBullets bullet = new ImpactBullets();
				bullet.setBulletpoint(tempbullet);
				
				event.addImpactBullet(bullet);
			}
		}
		
		if(eventrequest.getRecommendation() != null)
		{event.setRecommendation(eventrequest.getRecommendation());}
		
		if(eventrequest.getRecommendation2() != null)
		{event.setRecommendation2(eventrequest.getRecommendation2());}
		
		
		if(eventrequest.getRecommendationbullets() != null)
		{
			for(String tempbullet: eventrequest.getRecommendationbullets())
			{
				RecommendationBullet bullet = new RecommendationBullet();
				bullet.setBulletpoint(tempbullet);
				
				event.addRecommendationBullet(bullet);
			}
		}
		
		
		if(eventrequest.getLinks() != null)
		{event.setLinks(eventrequest.getLinks());}
		
		double latitude =eventrequest.getLatitude();
		double longitude =eventrequest.getLongitude();
		List<Double> coordinates = new ArrayList<>();
		coordinates.add(latitude);
		coordinates.add(longitude);
		List<Double> coordinates1 = new ArrayList<>();
		coordinates1 = checkLatitudeLongitude(coordinates);
		
		event.setLatitude(coordinates1.get(0));
		event.setLongitude(coordinates1.get(1));
		
		
		
		event.setStorydate(eventrequest.getStorydate().toInstant().plus(1, ChronoUnit.DAYS));
		event.setValiditydate(eventrequest.getValiditydate().toInstant().plus(1, ChronoUnit.DAYS));
		
		
		event.setRiskLevel(risklevelrepository.getOne(eventrequest.getRiskLevel()));
		event.setRiskCategory(riskcategoryrepository.getOne(eventrequest.getRiskcategory()));
		event.setRegionType(regiontyperepository.getOne(eventrequest.getRegionType()));
		event.setStatus(statusrepository.getOne((long) 1));
		
		if(eventrequest.getCountries() != null)
		{List<Country> tempcountry = new ArrayList<>();
		for(Long i : eventrequest.getCountries())
		{
			tempcountry.add(countryrepository.getOne(i));
		}
		event.setCountries(tempcountry);
		}
		
		if(eventrequest.getState() != null)
		{List<State> tempstate = new ArrayList<>();
		for(Long i : eventrequest.getState())
		{
			tempstate.add(staterepository.getOne(i));
		}
		event.setState(tempstate);
		}
		
		if(eventrequest.getCities() != null)
		{List<City> tempcity = new ArrayList<>();
		for(Long i : eventrequest.getCities())
		{
			tempcity.add(cityrepository.getOne(i));
		}
		event.setCities(tempcity);
		}
		
		List<ImpactIndustry> tempindustry = new ArrayList<>();
		for(Long i : eventrequest.getImpactindustry())
		{
			tempindustry.add(impactindustryrepository.getOne(i));
		}
		event.setImpactindustry(tempindustry);
		
		event.setImportance(eventrequest.isImportance());
		
		event.setLiveevent(false);
		if(eventrequest.isLiveevent())
		{event.setLiveevent(true);}
		reportrepository.save(event);
		
		
		
	}

	public static int random_range(int x1, int x2){
	    return (int) (Math.floor(x1 + (Math.random() * (x2 - x1))));
	}

	private List<Double> checkLatitudeLongitude(List<Double> coordinates) {
		
		Instant date = LocalDate.now().atStartOfDay().toInstant(ZoneOffset.UTC).minus(1, ChronoUnit.DAYS);
		List<Report> reports = reportrepository.getTerminalReports(date);
		
		int i =0;
		for(Report temp:reports)
		{
			int a =0;
			int b=0;		
			a = Double.compare(temp.getLatitude(), coordinates.get(0));
			b = Double.compare(temp.getLongitude(), coordinates.get(1));
			
			
			
			if( a == 0 && b ==0 )
			{
				i = i+1; 
			}
			
		}
		
		if(i > 0)
		{
			int x = random_range(-20, 20);
			coordinates.set(0,(coordinates.get(0)+0.005*x));
			coordinates.set(1,(coordinates.get(1)+0.005*x));
			checkLatitudeLongitude(coordinates);
		}
		
		return coordinates;
	}


	@Override
	public void updateReport(@Valid EventRequestObject eventrequest,UserPrincipal userprincipal) {
		
		
		Report event = reportrepository.findById(eventrequest.getId()).orElseThrow(() -> new ResourceNotFoundException("Report", "Report Id", eventrequest.getId()));;
		
		event.setTitle(eventrequest.getTitle());
		
		if(eventrequest.getDescription() != null)
		{event.setDescription(eventrequest.getDescription());}
		
		if(eventrequest.getDescription2() != null)
		{event.setDescription2(eventrequest.getDescription2());}
		
		
		if(event.getDescriptionbullets() != null)
		{
			Hibernate.initialize(event.getDescriptionbullets());
			
			List<Long> bulletids = new ArrayList<>();
			List<DescriptionBullets> bullets = event.getDescriptionbullets();
			
			for(DescriptionBullets tempbullet: bullets)
			{bulletids.add(tempbullet.getId());}
			
			for(Long tempid:bulletids)
			{
				DescriptionBullets bullet = descriptionbulletsrepository.findById(tempid).orElseThrow(
		                () -> new ResourceNotFoundException("Bullet-Point", "id", tempid));
				event.getDescriptionbullets().remove(bullet);
				descriptionbulletsrepository.delete(bullet);
			}
			
		}
		
		if(event.getBackgroundbullets() != null)
		{
			Hibernate.initialize(event.getBackgroundbullets());
			
			List<Long> bulletids = new ArrayList<>();
			List<BackgroundBullets> bullets = event.getBackgroundbullets();
			
			for(BackgroundBullets tempbullet: bullets)
			{bulletids.add(tempbullet.getId());}
			
			for(Long tempid:bulletids)
			{
				BackgroundBullets bullet = backgroundbulletsrepository.findById(tempid).orElseThrow(
		                () -> new ResourceNotFoundException("Bullet-Point", "id", tempid));
				event.getBackgroundbullets().remove(bullet);
				backgroundbulletsrepository.delete(bullet);
			}
			
		}
		
		if(event.getImpactbullets() != null)
		{
			Hibernate.initialize(event.getImpactbullets());
			
			List<Long> bulletids = new ArrayList<>();
			List<ImpactBullets> bullets = event.getImpactbullets();
			
			for(ImpactBullets tempbullet: bullets)
			{bulletids.add(tempbullet.getId());}
			
			for(Long tempid:bulletids)
			{
				ImpactBullets bullet = impactbulletrepository.findById(tempid).orElseThrow(
		                () -> new ResourceNotFoundException("Bullet-Point", "id", tempid));
				event.getImpactbullets().remove(bullet);
				impactbulletrepository.delete(bullet);
			}
			
		}
		
		if(event.getRecommendationbullets() != null)
		{
			Hibernate.initialize(event.getRecommendationbullets());
			
			List<Long> bulletids = new ArrayList<>();
			List<RecommendationBullet> bullets = event.getRecommendationbullets();
			
			for(RecommendationBullet tempbullet: bullets)
			{bulletids.add(tempbullet.getId());}
			
			for(Long tempid:bulletids)
			{
				RecommendationBullet bullet = recommendationbulletrepository.findById(tempid).orElseThrow(
		                () -> new ResourceNotFoundException("Bullet-Point", "id", tempid));
				event.getRecommendationbullets().remove(bullet);
				recommendationbulletrepository.delete(bullet);
			}
			
		}
		
		
		if(eventrequest.getDescriptionbullets() != null)
		{
			for(String tempbullet: eventrequest.getDescriptionbullets())
			{
				DescriptionBullets bullet = new DescriptionBullets();
				bullet.setBulletpoint(tempbullet);
				
				event.addDescriptionBullet(bullet);
			}
		}
		
		if(eventrequest.getBackground() != null)
		{event.setBackground(eventrequest.getBackground());}
		
		if(eventrequest.getBackground2() != null)
		{event.setBackground2(eventrequest.getBackground2());}
		
		
		if(eventrequest.getBackgroundbullets() != null)
		{
			for(String tempbullet: eventrequest.getBackgroundbullets())
			{
				BackgroundBullets bullet = new BackgroundBullets();
				bullet.setBulletpoint(tempbullet);
				
				event.addBackgroundBullet(bullet);
			}
		}
		
		if(eventrequest.getImpact() != null)
		{event.setImpact(eventrequest.getImpact());}
		
		if(eventrequest.getImpact2() != null)
		{event.setImpact2(eventrequest.getImpact2());}
		
		
		if(eventrequest.getImpactbullets() != null)
		{
			for(String tempbullet: eventrequest.getImpactbullets())
			{
				ImpactBullets bullet = new ImpactBullets();
				bullet.setBulletpoint(tempbullet);
				
				event.addImpactBullet(bullet);
			}
		}
		
		if(eventrequest.getRecommendation() != null)
		{event.setRecommendation(eventrequest.getRecommendation());}
		
		if(eventrequest.getRecommendation2() != null)
		{event.setRecommendation2(eventrequest.getRecommendation2());}
		
		
		if(eventrequest.getRecommendationbullets() != null)
		{
			for(String tempbullet: eventrequest.getRecommendationbullets())
			{
				RecommendationBullet bullet = new RecommendationBullet();
				bullet.setBulletpoint(tempbullet);
				
				event.addRecommendationBullet(bullet);
			}
		}

		
		if(eventrequest.getLinks() != null)
		{event.setLinks(eventrequest.getLinks());}
		
		double latitude =eventrequest.getLatitude();
		double longitude =eventrequest.getLongitude();
		List<Double> coordinates = new ArrayList<>();
		coordinates.add(latitude);
		coordinates.add(longitude);
		List<Double> coordinates1 = new ArrayList<>();
		coordinates1 = checkLatitudeLongitude(coordinates);
		event.setLatitude(coordinates1.get(0));
		event.setLongitude(coordinates1.get(1));
		
		
		event.setStorydate(eventrequest.getStorydate().toInstant().plus(1, ChronoUnit.DAYS));
		event.setValiditydate(eventrequest.getValiditydate().toInstant().plus(1, ChronoUnit.DAYS));
		
		
		event.setRiskLevel(risklevelrepository.getOne(eventrequest.getRiskLevel()));
		event.setRiskCategory(riskcategoryrepository.getOne(eventrequest.getRiskcategory()));
		event.setRegionType(regiontyperepository.getOne(eventrequest.getRegionType()));
		event.setStatus(statusrepository.getOne(eventrequest.getStatus()));
		
		
		
		List<Country> tempcountry = new ArrayList<>();
		for(Long i : eventrequest.getCountries())
		{
			tempcountry.add(countryrepository.getOne(i));
		}
		event.setCountries(tempcountry);
		
		List<State> tempstate = new ArrayList<>();
		for(Long i : eventrequest.getState())
		{
			tempstate.add(staterepository.getOne(i));
		}
		event.setState(tempstate);
		
		List<City> tempcity = new ArrayList<>();
		for(Long i : eventrequest.getCities())
		{
			tempcity.add(cityrepository.getOne(i));
		}
		event.setCities(tempcity);
		
		List<ImpactIndustry> tempindustry = new ArrayList<>();
		for(Long i : eventrequest.getImpactindustry())
		{
			tempindustry.add(impactindustryrepository.getOne(i));
		}
		event.setImpactindustry(tempindustry);
		
		event.setImportance(eventrequest.isImportance());
		
		event.setLiveevent(false);
		if(eventrequest.isLiveevent())
		{event.setLiveevent(true);}
		
		reportrepository.save(event);

		
	}


	@Override
	public EventDetailsResponseCreator converToEventResponse(Report report) {
		
		EventDetailsResponseCreator response = new EventDetailsResponseCreator();
		
		response.setId(report.getId());
		
		if(report.getBackground() != null)
		{response.setBackground(report.getBackground()); }
		if(report.getCities() != null)
		{response.setCities(report.getCities());}
		if(report.getCountries() != null)
		{response.setCountries(report.getCountries());}	
		if(report.getDescription() != null)
		{response.setDescription(report.getDescription());}
		if(report.getImpact()!=null)
		{response.setImpact(report.getImpact());}
		if(report.getImpactindustry() != null)
		{response.setImpactindustry(report.getImpactindustry());}
		
		if(report.getDescription2() != null)
		{response.setDescription2(report.getDescription2());}
		if(report.getBackground2() != null)
		{response.setBackground2(report.getBackground2());}
		if(report.getImpact2() != null)
		{response.setImpact2(report.getImpact2());}
		if(report.getRecommendation2() != null)
		{response.setRecommendation2(report.getRecommendation2());;}
		
		if(report.getBackgroundbullets() != null)
		{	
			List<String> bullets = new ArrayList<>();
			for(BackgroundBullets temp :report.getBackgroundbullets() )
			{bullets.add(temp.getBulletpoint());}
			response.setBackgroundbullets(bullets);
		}
		
		if(report.getDescriptionbullets() != null)
		{	
			List<String> bullets = new ArrayList<>();
			for(DescriptionBullets temp :report.getDescriptionbullets() )
			{bullets.add(temp.getBulletpoint());}
			response.setDescriptionbullets(bullets);
		}
		
		if(report.getImpactbullets() != null)
		{	
			List<String> bullets = new ArrayList<>();
			for(ImpactBullets temp :report.getImpactbullets() )
			{bullets.add(temp.getBulletpoint());}
			response.setImpactbullets(bullets);
		}
		
		if(report.getRecommendationbullets() != null)
		{	
			List<String> bullets = new ArrayList<>();
			for(RecommendationBullet temp :report.getRecommendationbullets() )
			{bullets.add(temp.getBulletpoint());}
			response.setRecommendationbullets(bullets);
		}
		
		
		response.setLatitude(report.getLatitude());
		response.setLongitude(report.getLongitude());
		
		if(report.getLinks() != null)
		{response.setLinks(report.getLinks());}
		if(report.getRecommendation()!=null)
		{response.setRecommendation(report.getRecommendation());}
		if(report.getRegionType() != null)
		{response.setRegionType(report.getRegionType());}
		if(report.getRiskCategory()!=null)
		{response.setRiskCategory(report.getRiskCategory());}
		if(report.getRiskLevel() !=null)
		{response.setRiskLevel(report.getRiskLevel());}
		if(report.getState() != null)
		{response.setState(report.getState());}	
		if(report.getTitle() != null)
		{response.setTitle(report.getTitle());}
		if(report.getUpdates() != null )
		{response.setUpdates(report.getUpdates());}
		
		
		LocalDate storydate = LocalDateTime.ofInstant(report.getStorydate(), ZoneOffset.ofHours(6)).toLocalDate();
		LocalDate validitydate = LocalDateTime.ofInstant(report.getValiditydate(), ZoneOffset.ofHours(6)).toLocalDate();
		
		response.setStorydate(storydate);
		response.setValiditydate(validitydate);
		
		response.setImportance(report.isImportance());
		response.setLiveevent(report.isLiveevent());
		
		return response;
	}


	@Override
	public void editReport(UserPrincipal currentUser, @Valid EventRequestObject eventrequest) {
Report event = reportrepository.findById(eventrequest.getId()).orElseThrow(() -> new ResourceNotFoundException("Report", "Report Id", eventrequest.getId()));;
		
		event.setTitle(eventrequest.getTitle());
		
		if(event.getDescriptionbullets() != null)
		{
			Hibernate.initialize(event.getDescriptionbullets());
			
			List<Long> bulletids = new ArrayList<>();
			List<DescriptionBullets> bullets = event.getDescriptionbullets();
			
			for(DescriptionBullets tempbullet: bullets)
			{bulletids.add(tempbullet.getId());}
			
			for(Long tempid:bulletids)
			{
				DescriptionBullets bullet = descriptionbulletsrepository.findById(tempid).orElseThrow(
		                () -> new ResourceNotFoundException("Bullet-Point", "id", tempid));
				event.getDescriptionbullets().remove(bullet);
				descriptionbulletsrepository.delete(bullet);
			}
			
		}
		
		if(event.getBackgroundbullets() != null)
		{
			Hibernate.initialize(event.getBackgroundbullets());
			
			List<Long> bulletids = new ArrayList<>();
			List<BackgroundBullets> bullets = event.getBackgroundbullets();
			
			for(BackgroundBullets tempbullet: bullets)
			{bulletids.add(tempbullet.getId());}
			
			for(Long tempid:bulletids)
			{
				BackgroundBullets bullet = backgroundbulletsrepository.findById(tempid).orElseThrow(
		                () -> new ResourceNotFoundException("Bullet-Point", "id", tempid));
				event.getBackgroundbullets().remove(bullet);
				backgroundbulletsrepository.delete(bullet);
			}
			
		}
		
		if(event.getImpactbullets() != null)
		{
			Hibernate.initialize(event.getImpactbullets());
			
			List<Long> bulletids = new ArrayList<>();
			List<ImpactBullets> bullets = event.getImpactbullets();
			
			for(ImpactBullets tempbullet: bullets)
			{bulletids.add(tempbullet.getId());}
			
			for(Long tempid:bulletids)
			{
				ImpactBullets bullet = impactbulletrepository.findById(tempid).orElseThrow(
		                () -> new ResourceNotFoundException("Bullet-Point", "id", tempid));
				event.getImpactbullets().remove(bullet);
				impactbulletrepository.delete(bullet);
			}
			
		}
		
		if(event.getRecommendationbullets() != null)
		{
			Hibernate.initialize(event.getRecommendationbullets());
			
			List<Long> bulletids = new ArrayList<>();
			List<RecommendationBullet> bullets = event.getRecommendationbullets();
			
			for(RecommendationBullet tempbullet: bullets)
			{bulletids.add(tempbullet.getId());}
			
			for(Long tempid:bulletids)
			{
				RecommendationBullet bullet = recommendationbulletrepository.findById(tempid).orElseThrow(
		                () -> new ResourceNotFoundException("Bullet-Point", "id", tempid));
				event.getRecommendationbullets().remove(bullet);
				recommendationbulletrepository.delete(bullet);
			}
			
		}
		
		
		if(eventrequest.getDescription() != null)
		{event.setDescription(eventrequest.getDescription());}
		
		if(eventrequest.getDescription2() != null)
		{event.setDescription2(eventrequest.getDescription2());}
			
		if(eventrequest.getDescriptionbullets() != null)
		{
			for(String tempbullet: eventrequest.getDescriptionbullets())
			{
				DescriptionBullets bullet = new DescriptionBullets();
				bullet.setBulletpoint(tempbullet);
				
				event.addDescriptionBullet(bullet);
			}
		}
		
		if(eventrequest.getBackground() != null)
		{event.setBackground(eventrequest.getBackground());}
		
		if(eventrequest.getBackground2() != null)
		{event.setBackground2(eventrequest.getBackground2());}
		
		if(eventrequest.getBackgroundbullets() != null)
		{
			for(String tempbullet: eventrequest.getBackgroundbullets())
			{
				BackgroundBullets bullet = new BackgroundBullets();
				bullet.setBulletpoint(tempbullet);
				
				event.addBackgroundBullet(bullet);
			}
		}
		
		if(eventrequest.getImpact() != null)
		{event.setImpact(eventrequest.getImpact());}
		
		if(eventrequest.getImpact2() != null)
		{event.setImpact2(eventrequest.getImpact2());}
		
		if(eventrequest.getImpactbullets() != null)
		{
			for(String tempbullet: eventrequest.getImpactbullets())
			{
				ImpactBullets bullet = new ImpactBullets();
				bullet.setBulletpoint(tempbullet);
				
				event.addImpactBullet(bullet);
			}
		}
		
		if(eventrequest.getRecommendation() != null)
		{event.setRecommendation(eventrequest.getRecommendation());}
		
		if(eventrequest.getRecommendation2() != null)
		{event.setRecommendation2(eventrequest.getRecommendation2());}
		
		
		if(eventrequest.getRecommendationbullets() != null)
		{
			for(String tempbullet: eventrequest.getRecommendationbullets())
			{
				RecommendationBullet bullet = new RecommendationBullet();
				bullet.setBulletpoint(tempbullet);
				
				event.addRecommendationBullet(bullet);
			}
		}

		
		if(eventrequest.getLinks() != null)
		{event.setLinks(eventrequest.getLinks());}
		
		double latitude =eventrequest.getLatitude();
		double longitude =eventrequest.getLongitude();
		List<Double> coordinates = new ArrayList<>();
		coordinates.add(latitude);
		coordinates.add(longitude);
		List<Double> coordinates1 = new ArrayList<>();
		coordinates1 = checkLatitudeLongitude(coordinates);
		event.setLatitude(coordinates1.get(0));
		event.setLongitude(coordinates1.get(1));
		
		
		event.setStorydate(eventrequest.getStorydate().toInstant().plus(1, ChronoUnit.DAYS));
		event.setValiditydate(eventrequest.getValiditydate().toInstant().plus(1, ChronoUnit.DAYS));
		
		
		event.setRiskLevel(risklevelrepository.getOne(eventrequest.getRiskLevel()));
		event.setRiskCategory(riskcategoryrepository.getOne(eventrequest.getRiskcategory()));
		event.setRegionType(regiontyperepository.getOne(eventrequest.getRegionType()));
	
		
		List<Country> tempcountry = new ArrayList<>();
		for(Long i : eventrequest.getCountries())
		{
			tempcountry.add(countryrepository.getOne(i));
		}
		event.setCountries(tempcountry);
		
		List<State> tempstate = new ArrayList<>();
		for(Long i : eventrequest.getState())
		{
			tempstate.add(staterepository.getOne(i));
		}
		event.setState(tempstate);
		
		List<City> tempcity = new ArrayList<>();
		for(Long i : eventrequest.getCities())
		{
			tempcity.add(cityrepository.getOne(i));
		}
		event.setCities(tempcity);
		
		List<ImpactIndustry> tempindustry = new ArrayList<>();
		for(Long i : eventrequest.getImpactindustry())
		{
			tempindustry.add(impactindustryrepository.getOne(i));
		}
		event.setImpactindustry(tempindustry);
		
		event.setImportance(eventrequest.isImportance());
		
		event.setLiveevent(false);
		if(eventrequest.isLiveevent())
		{event.setLiveevent(true);}
		
		reportrepository.save(event);
		
	}


	@Override
	public void addUpdate(@Valid UpdateRequestObject request) {
		Report report = reportrepository.findById(request.getEventid()).orElseThrow(
                () -> new ResourceNotFoundException("Report", "id", request.getEventid()));
		
		
		Update update = new Update();
		
		if(request.getTitle() != null)
		{update.setTitle(request.getTitle());}
		
		if(request.getDiscription() != null)
		{update.setDescription(request.getDiscription());}
		
		ZoneId zoneId = ZoneId.of( "Asia/Kolkata" );
		ZonedDateTime datetimekolkat = LocalDateTime.now().atZone(zoneId);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss");
		String timenow = datetimekolkat.format(formatter);
		
		update.setTimeupdate(timenow);
		report.addUpdate(update);
		updaterepository.save(update);
		
	}

	

	
	
	
	@Override
	public void editUpdate(@Valid UpdateRequestObject request) {
		// TODO Auto-generated method stub
		
		Update update = updaterepository.findById(request.getUpdateid()).orElseThrow(
                () -> new ResourceNotFoundException("Update ID", "id", request.getUpdateid()));
		
		if(request.getTitle() != null)
		{update.setTitle(request.getTitle());}
		
		if(request.getDiscription() != null)
		{update.setDescription(request.getDiscription());}
		
		updaterepository.save(update);
		
		
	}


	@Override
	public void deleteUpdate(UserPrincipal userprinciple, long updateid) {
		
		try {
			
			updaterepository.deleteById(updateid);	
		}
		catch(Exception e) {}
		
	}


	@Override
	public EventDetailResponseApprover converToEventResponseApprover(Report report) {
		EventDetailResponseApprover response = new EventDetailResponseApprover();
		
		response.setId(report.getId());
		
		if(report.getBackground() != null)
		{response.setBackground(report.getBackground()); }
		if(report.getCities() != null)
		{response.setCities(report.getCities());}
		if(report.getCountries() != null)
		{response.setCountries(report.getCountries());}	
		if(report.getDescription() != null)
		{response.setDescription(report.getDescription());}
		if(report.getImpact()!=null)
		{response.setImpact(report.getImpact());}
		if(report.getImpactindustry() != null)
		{response.setImpactindustry(report.getImpactindustry());}
		
		if(report.getDescription2() != null)
		{response.setDescription2(report.getDescription2());}
		if(report.getBackground2() != null)
		{response.setBackground2(report.getBackground2());}
		if(report.getImpact2() != null)
		{response.setImpact2(report.getImpact2());}
		if(report.getRecommendation2() != null)
		{response.setRecommendation2(report.getRecommendation2());;}
		
		if(report.getBackgroundbullets() != null)
		{	
			List<String> bullets = new ArrayList<>();
			for(BackgroundBullets temp :report.getBackgroundbullets() )
			{bullets.add(temp.getBulletpoint());}
			response.setBackgroundbullets(bullets);
		}
		
		if(report.getDescriptionbullets() != null)
		{	
			List<String> bullets = new ArrayList<>();
			for(DescriptionBullets temp :report.getDescriptionbullets() )
			{bullets.add(temp.getBulletpoint());}
			response.setDescriptionbullets(bullets);
		}
		
		if(report.getImpactbullets() != null)
		{	
			List<String> bullets = new ArrayList<>();
			for(ImpactBullets temp :report.getImpactbullets() )
			{bullets.add(temp.getBulletpoint());}
			response.setImpactbullets(bullets);
		}
		
		if(report.getRecommendationbullets() != null)
		{	
			List<String> bullets = new ArrayList<>();
			for(RecommendationBullet temp :report.getRecommendationbullets() )
			{bullets.add(temp.getBulletpoint());}
			response.setRecommendationbullets(bullets);
		}
		

		
		response.setLatitude(report.getLatitude());
		response.setLongitude(report.getLongitude());
		
		if(report.getLinks() != null)
		{response.setLinks(report.getLinks());}
		if(report.getRecommendation()!=null)
		{response.setRecommendation(report.getRecommendation());}
		if(report.getRegionType() != null)
		{response.setRegionType(report.getRegionType());}
		if(report.getRiskCategory()!=null)
		{response.setRiskCategory(report.getRiskCategory());}
		if(report.getRiskLevel() !=null)
		{response.setRiskLevel(report.getRiskLevel());}
		if(report.getState() != null)
		{response.setState(report.getState());}	
		if(report.getTitle() != null)
		{response.setTitle(report.getTitle());}
		
		response.setStatus(report.getStatus());
		
		LocalDate storydate = LocalDateTime.ofInstant(report.getStorydate(), ZoneOffset.ofHours(6)).toLocalDate();
		LocalDate validitydate = LocalDateTime.ofInstant(report.getValiditydate(), ZoneOffset.ofHours(6)).toLocalDate();
		
		response.setStorydate(storydate);
		response.setValiditydate(validitydate);
		
		response.setImportance(report.isImportance());
		response.setLiveevent(report.isLiveevent());
		
		return response;
	}


	@Override
	public void addNewEmailAlert(@Valid EventRequestObject eventrequest, UserPrincipal currentUser) {
Report event = new Report();
		
		event.setTitle(eventrequest.getTitle());
		
		if(eventrequest.getDescription() != null)
		{event.setDescription(eventrequest.getDescription());}
		
		if(eventrequest.getDescription2() != null)
		{event.setDescription2(eventrequest.getDescription2());}
			
		if(eventrequest.getDescriptionbullets() != null)
		{
			for(String tempbullet: eventrequest.getDescriptionbullets())
			{
				DescriptionBullets bullet = new DescriptionBullets();
				bullet.setBulletpoint(tempbullet);
				
				event.addDescriptionBullet(bullet);
			}
		}
		
		if(eventrequest.getBackground() != null)
		{event.setBackground(eventrequest.getBackground());}
		
		if(eventrequest.getBackground2() != null)
		{event.setBackground2(eventrequest.getBackground2());}
		
		if(eventrequest.getBackgroundbullets() != null)
		{
			for(String tempbullet: eventrequest.getBackgroundbullets())
			{
				BackgroundBullets bullet = new BackgroundBullets();
				bullet.setBulletpoint(tempbullet);
				
				event.addBackgroundBullet(bullet);
			}
		}
		
		if(eventrequest.getImpact() != null)
		{event.setImpact(eventrequest.getImpact());}
		
		if(eventrequest.getImpact2() != null)
		{event.setImpact2(eventrequest.getImpact2());}
		
		if(eventrequest.getImpactbullets() != null)
		{
			for(String tempbullet: eventrequest.getImpactbullets())
			{
				ImpactBullets bullet = new ImpactBullets();
				bullet.setBulletpoint(tempbullet);
				
				event.addImpactBullet(bullet);
			}
		}
		
		if(eventrequest.getRecommendation() != null)
		{event.setRecommendation(eventrequest.getRecommendation());}
		
		if(eventrequest.getRecommendation2() != null)
		{event.setRecommendation2(eventrequest.getRecommendation2());}
		
		
		if(eventrequest.getRecommendationbullets() != null)
		{
			for(String tempbullet: eventrequest.getRecommendationbullets())
			{
				RecommendationBullet bullet = new RecommendationBullet();
				bullet.setBulletpoint(tempbullet);
				
				event.addRecommendationBullet(bullet);
			}
		}
		
		if(eventrequest.getLinks() != null)
		{event.setLinks(eventrequest.getLinks());}
		
		double latitude =eventrequest.getLatitude();
		double longitude =eventrequest.getLongitude();
		List<Double> coordinates = new ArrayList<>();
		coordinates.add(latitude);
		coordinates.add(longitude);
		List<Double> coordinates1 = new ArrayList<>();
		coordinates1 = checkLatitudeLongitude(coordinates);
		event.setLatitude(coordinates1.get(0));
		event.setLongitude(coordinates1.get(1));
		
		
		event.setStorydate(eventrequest.getStorydate().toInstant().plus(1, ChronoUnit.DAYS));
		event.setValiditydate(eventrequest.getValiditydate().toInstant().plus(1, ChronoUnit.DAYS));
		
		
		event.setRiskLevel(risklevelrepository.getOne(eventrequest.getRiskLevel()));
		event.setRiskCategory(riskcategoryrepository.getOne(eventrequest.getRiskcategory()));
		event.setRegionType(regiontyperepository.getOne(eventrequest.getRegionType()));
		event.setStatus(statusrepository.getOne((long) 1));
		
		if(eventrequest.getCountries() != null)
		{List<Country> tempcountry = new ArrayList<>();
		for(Long i : eventrequest.getCountries())
		{
			tempcountry.add(countryrepository.getOne(i));
		}
		event.setCountries(tempcountry);
		}
		
		if(eventrequest.getState() != null)
		{List<State> tempstate = new ArrayList<>();
		for(Long i : eventrequest.getState())
		{
			tempstate.add(staterepository.getOne(i));
		}
		event.setState(tempstate);
		}
		
		if(eventrequest.getCities() != null)
		{List<City> tempcity = new ArrayList<>();
		for(Long i : eventrequest.getCities())
		{
			tempcity.add(cityrepository.getOne(i));
		}
		event.setCities(tempcity);
		}
		
		List<ImpactIndustry> tempindustry = new ArrayList<>();
		for(Long i : eventrequest.getImpactindustry())
		{
			tempindustry.add(impactindustryrepository.getOne(i));
		}
		event.setImpactindustry(tempindustry);
		
		event.setImportance(eventrequest.isImportance());
		event.setRiskalert(true);
		event.setLiveevent(false);
		if(eventrequest.isLiveevent())
		{event.setLiveevent(true);}
		reportrepository.save(event);
		

		
	}


	@Override
	public void deletereport(@Valid long eventid) {
		Report report = reportrepository.findById(eventid).orElseThrow(
                () -> new ResourceNotFoundException("Report", "id", eventid));
		report.setCities(null);
		report.setCountries(null);
		report.setImpactindustry(null);
		report.setState(null);
		//report.setUpdates(null);
		
		if(report.isLiveevent())
		{
			Hibernate.initialize(report.getUpdates());
			if(report.getUpdates() != null && report.getUpdates().size() >0)
			{
				List<Long> updateids = new ArrayList<>();
			
				List<Update> updates = report.getUpdates();
				for(Update temp: updates)
				{
					updateids.add(temp.getId());
					
				}
				
			for(Long temp: updateids)
			{
				Update update = updaterepository.findById(temp).orElseThrow(
	                () -> new ResourceNotFoundException("Report", "id", eventid));
				report.getUpdates().remove(update);
				updaterepository.delete(update);
			}	
		
			
			}
		
		}
		
		if(report.getDescriptionbullets() != null)
		{
			Hibernate.initialize(report.getDescriptionbullets());
			
			List<Long> bulletids = new ArrayList<>();
			List<DescriptionBullets> bullets = report.getDescriptionbullets();
			
			for(DescriptionBullets tempbullet: bullets)
			{bulletids.add(tempbullet.getId());}
			
			for(Long tempid:bulletids)
			{
				DescriptionBullets bullet = descriptionbulletsrepository.findById(tempid).orElseThrow(
		                () -> new ResourceNotFoundException("Bullet-Point", "id", eventid));
				report.getDescriptionbullets().remove(bullet);
				descriptionbulletsrepository.delete(bullet);
			}
			
		}
		
		if(report.getBackgroundbullets() != null)
		{
			Hibernate.initialize(report.getBackgroundbullets());
			
			List<Long> bulletids = new ArrayList<>();
			List<BackgroundBullets> bullets = report.getBackgroundbullets();
			
			for(BackgroundBullets tempbullet: bullets)
			{bulletids.add(tempbullet.getId());}
			
			for(Long tempid:bulletids)
			{
				BackgroundBullets bullet = backgroundbulletsrepository.findById(tempid).orElseThrow(
		                () -> new ResourceNotFoundException("Bullet-Point", "id", eventid));
				report.getBackgroundbullets().remove(bullet);
				backgroundbulletsrepository.delete(bullet);
			}
			
		}
		
		if(report.getImpactbullets() != null)
		{
			Hibernate.initialize(report.getImpactbullets());
			
			List<Long> bulletids = new ArrayList<>();
			List<ImpactBullets> bullets = report.getImpactbullets();
			
			for(ImpactBullets tempbullet: bullets)
			{bulletids.add(tempbullet.getId());}
			
			for(Long tempid:bulletids)
			{
				ImpactBullets bullet = impactbulletrepository.findById(tempid).orElseThrow(
		                () -> new ResourceNotFoundException("Bullet-Point", "id", eventid));
				report.getImpactbullets().remove(bullet);
				impactbulletrepository.delete(bullet);
			}
			
		}
		
		if(report.getRecommendationbullets() != null)
		{
			Hibernate.initialize(report.getRecommendationbullets());
			
			List<Long> bulletids = new ArrayList<>();
			List<RecommendationBullet> bullets = report.getRecommendationbullets();
			
			for(RecommendationBullet tempbullet: bullets)
			{bulletids.add(tempbullet.getId());}
			
			for(Long tempid:bulletids)
			{
				RecommendationBullet bullet = recommendationbulletrepository.findById(tempid).orElseThrow(
		                () -> new ResourceNotFoundException("Bullet-Point", "id", eventid));
				report.getRecommendationbullets().remove(bullet);
				recommendationbulletrepository.delete(bullet);
			}
			
		}
		
		report.setLiveevent(false);
		report.setImportance(false);
		report.setRiskalert(false);
		
		
		
		
		
		reportrepository.delete(report);
		
	}


	

}
