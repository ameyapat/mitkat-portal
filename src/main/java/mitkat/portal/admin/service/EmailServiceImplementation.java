package mitkat.portal.admin.service;


import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.mail.MailSendException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import freemarker.core.ParseException;
import freemarker.template.Configuration;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateNotFoundException;
import mitkat.portal.admin.repository.application.ClientProfileRepository;
import mitkat.portal.admin.repository.application.Countryrepository;
import mitkat.portal.admin.repository.application.MitKatRepository;
import mitkat.portal.admin.repository.application.ReportRepository;
import mitkat.portal.admin.requestObjects.approver.EmailAlertRequest;
import mitkat.portal.admin.requestObjects.approver.EmailRequest;
import mitkat.portal.admin.responseObjects.approver.EmailResponseObject;
import mitkat.portal.admin.responseObjects.client.EmailFtlTemplateResponseObject;
import mitkat.portal.admin.savefiles.FileStorageService;
import mitkat.portal.admin.model.clientprofile.ClientEmailIds;
import mitkat.portal.admin.model.clientprofile.ClientProfile;
import mitkat.portal.admin.model.clientprofile.Country;
import mitkat.portal.admin.model.clientprofile.State;
import mitkat.portal.admin.model.creator.BackgroundBullets;
import mitkat.portal.admin.model.creator.DescriptionBullets;
import mitkat.portal.admin.model.creator.ImpactBullets;
import mitkat.portal.admin.model.creator.RecommendationBullet;
import mitkat.portal.admin.model.creator.Report;


@Service
public class EmailServiceImplementation implements EmailService {

	@Autowired
	MitKatRepository mitkatrepository;
	
	@Autowired
	ClientProfileRepository clientprofilerepository;
	
	@Autowired
	ReportRepository reportrepository;
	
	@Autowired
	JavaMailSender mailSenderobj;

	@Autowired
    Configuration freemarkerConfig;
	
	@Autowired
	Countryrepository countryrepository ;
	
	@Autowired 
	FileStorageService filestorageservice;
	
	public static boolean isNullOrEmpty(String str) {
	    if(str != null && !str.isEmpty())
	        return false;
	    return true;
	}
	
	@Override
	public EmailResponseObject sendEmail(@Valid EmailRequest emailrequest) throws TemplateNotFoundException, MalformedTemplateNameException, ParseException, IOException, MessagingException, TemplateException {
		DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MMM-yyyy");
		LocalDate date = LocalDate.now();
		String date0 = date.format(format);
		ClientProfile clientprofile = clientprofilerepository.findByclientid(emailrequest.getClientid()).get();
		
		List<Report> reports = new ArrayList<>();
		for (long id: emailrequest.getEventid())
		{
			Report temp = reportrepository.getOne(id);
			reports.add(temp);
		}
		List<ClientEmailIds> emails = clientprofilerepository.findByclientid(emailrequest.getClientid()).get().getClientemail();
		
		Country india = countryrepository.getOne(1);
		List<EmailFtlTemplateResponseObject> indiatitles = new ArrayList<>();
		int i =1;
		
		for(Report r:reports)
		{
			if(r.getCountries().contains(india))
			{ EmailFtlTemplateResponseObject temp = new EmailFtlTemplateResponseObject();
			  temp.setCountry("INDIA");
			  for(State s:r.getState())
			  {temp.setState(s.getStatename()); }	
			  temp.setTitle(r.getTitle());
			  temp.setRisklevel(r.getRiskLevel().getId());
			  temp.setRiskcategiry(r.getRiskCategory().getId());
			  
			  
			  if(i%2 == 0)
			  {temp.setId(0);}
			  else {temp.setId(1);}
			  i = i+1;
			  if(isNullOrEmpty(r.getDescription()))
			  {
				 temp.setDiscid(0); 
			  }
			  else
			  {
				  temp.setDiscid(1);
				  temp.setDiscription(r.getDescription());
			  }
			  
			  if(isNullOrEmpty(r.getBackground()))
			  {
				 temp.setBackid(0); 
			  }
			  else
			  {
				  temp.setBackid(1); 
				  temp.setBackground(r.getBackground());
			  } 
			  
			  if(isNullOrEmpty(r.getImpact()))
			  {
				 temp.setImpactid(0); 
			  }
			  else
			  {
				  temp.setImpactid(1);
				  temp.setImpact(r.getImpact());
			  } 
			  
			  if(isNullOrEmpty(r.getRecommendation()))
			  {
				 temp.setRecommid(0); 
			  }
			  else
			  {
				  temp.setRecommid(1); 
				  temp.setRecommendation(r.getRecommendation());
			  } 
			  
			  if(isNullOrEmpty(r.getDescription2()))
			  {
				  temp.setDiscid2(0);
			  }	  
			  else
			  {
				  temp.setDiscid2(1);
				  temp.setDescription2(r.getDescription2());
				  }
			  
			  if(isNullOrEmpty(r.getBackground2()))
			  {
				  temp.setBackid2(0);
			  }
			  else
			  {
				  temp.setBackid2(1);
				  temp.setBackground2(r.getBackground2());}
			  
			  if(isNullOrEmpty(r.getImpact2()))
			  {
				  temp.setImpactid2(0);
			  }
			  else
			  {
				  temp.setImpactid2(1);
				  temp.setImpact2(r.getImpact2());}
			  
			  if(isNullOrEmpty(r.getRecommendation2()))
			  {
				  temp.setRecommid2(0);
			  }
			  else
			  { 
				  temp.setRecommid2(1);
				  temp.setRecommendation2(r.getRecommendation2());}
			 
			  if(r.getDescriptionbullets() != null)
			  {	  
				  temp.setDiscidbullet(1);
				  List<String> bullets = new ArrayList<>();
			  for(DescriptionBullets tempbullets : r.getDescriptionbullets())
			  {
				  bullets.add(tempbullets.getBulletpoint());
			  }
			  
			  	temp.setDescriptionbullets(bullets);
			  }
			  else
			  {temp.setDiscidbullet(0);}	  
			  
			  if(r.getBackgroundbullets() != null)
			  {	  
				  temp.setBackidbullet(1);
				  List<String> bullets = new ArrayList<>();
			  for(BackgroundBullets tempbullets : r.getBackgroundbullets())
			  {
				  bullets.add(tempbullets.getBulletpoint());
			  }
			  	temp.setBackgroundbullets(bullets);
			  }
			  else
			  {temp.setBackidbullet(0);}	  
			  
			  if(r.getImpactbullets() != null)
			  {	  temp.setImpactidbullet(1);
				  List<String> bullets = new ArrayList<>();
			  for(ImpactBullets tempbullets : r.getImpactbullets())
			  {
				  bullets.add(tempbullets.getBulletpoint());
			  }
			  	temp.setImpactbullets(bullets);
			  }
			  else
			  { temp.setImpactidbullet(0);}
			  
			  if(r.getRecommendationbullets() != null)
			  {	  
				  temp.setRecommidbullet(1);
				  List<String> bullets = new ArrayList<>();
			  for(RecommendationBullet tempbullets : r.getRecommendationbullets())
			  {
				  bullets.add(tempbullets.getBulletpoint());
			  }
			  	temp.setRecommendationbullets(bullets);
			  }
			  else
			  {temp.setRecommidbullet(0);}	  
			  
			  indiatitles.add(temp);
			}
		}
		int indiatitlenumbers = indiatitles.size();
		
		List<EmailFtlTemplateResponseObject> regionaltitles = new ArrayList<>();
		Country pakistan = countryrepository.getOne(2);
		Country bangladesh = countryrepository.getOne(3);
		Country srilanka = countryrepository.getOne(4);
		Country maldives = countryrepository.getOne(5);
		Country nepal = countryrepository.getOne(6);
		Country afghanistan = countryrepository.getOne(7);
		
		i =1;
		for(Report r:reports)
		{
			
			if(r.getCountries().contains(pakistan)||r.getCountries().contains(bangladesh)||r.getCountries().contains(srilanka)||r.getCountries().contains(maldives)||r.getCountries().contains(nepal)||r.getCountries().contains(afghanistan))
			{ 
				EmailFtlTemplateResponseObject temp = new EmailFtlTemplateResponseObject();
				for(Country c:r.getCountries())
				  {temp.setCountry(c.getCountryname());}
				temp.setTitle(r.getTitle());
				temp.setRisklevel(r.getRiskLevel().getId());
				 temp.setRiskcategiry(r.getRiskCategory().getId());
				if(i%2 == 0)
				  {temp.setId(0);}
				  else {temp.setId(1);}
				i=i+1;
				
				 if(isNullOrEmpty(r.getDescription()))
				  {
					 temp.setDiscid(0); 
				  }
				  else
				  {
					  temp.setDiscid(1);
					  temp.setDiscription(r.getDescription());
				  }
				  
				  if(isNullOrEmpty(r.getBackground()))
				  {
					 temp.setBackid(0); 
				  }
				  else
				  {
					  temp.setBackid(1); 
					  temp.setBackground(r.getBackground());
				  } 
				  
				  if(isNullOrEmpty(r.getImpact()))
				  {
					 temp.setImpactid(0); 
				  }
				  else
				  {
					  temp.setImpactid(1);
					  temp.setImpact(r.getImpact());
				  } 
				  
				  if(isNullOrEmpty(r.getRecommendation()))
				  {
					 temp.setRecommid(0); 
				  }
				  else
				  {
					  temp.setRecommid(1); 
					  temp.setRecommendation(r.getRecommendation());
				  } 
				  if(isNullOrEmpty(r.getDescription2()))
				  {
					  temp.setDiscid2(0);
				  }	  
				  else
				  {
					  temp.setDiscid2(1);
					  temp.setDescription2(r.getDescription2());
					  }
				  
				  if(isNullOrEmpty(r.getBackground2()))
				  {
					  temp.setBackid2(0);
				  }
				  else
				  {
					  temp.setBackid2(1);
					  temp.setBackground2(r.getBackground2());}
				  
				  if(isNullOrEmpty(r.getImpact2()))
				  {
					  temp.setImpactid2(0);
				  }
				  else
				  {
					  temp.setImpactid2(1);
					  temp.setImpact2(r.getImpact2());}
				  
				  if(isNullOrEmpty(r.getRecommendation2()))
				  {
					  temp.setRecommid2(0);
				  }
				  else
				  { 
					  temp.setRecommid2(1);
					  temp.setRecommendation2(r.getRecommendation2());}
				 
				  if(r.getDescriptionbullets() != null)
				  {	  
					  temp.setDiscidbullet(1);
					  List<String> bullets = new ArrayList<>();
				  for(DescriptionBullets tempbullets : r.getDescriptionbullets())
				  {
					  bullets.add(tempbullets.getBulletpoint());
				  }
				  
				  	temp.setDescriptionbullets(bullets);
				  }
				  else
				  {temp.setDiscidbullet(0);}	  
				  
				  if(r.getBackgroundbullets() != null)
				  {	  
					  temp.setBackidbullet(1);
					  List<String> bullets = new ArrayList<>();
				  for(BackgroundBullets tempbullets : r.getBackgroundbullets())
				  {
					  bullets.add(tempbullets.getBulletpoint());
				  }
				  	temp.setBackgroundbullets(bullets);
				  }
				  else
				  {temp.setBackidbullet(0);}	  
				  
				  if(r.getImpactbullets() != null)
				  {	  temp.setImpactidbullet(1);
					  List<String> bullets = new ArrayList<>();
				  for(ImpactBullets tempbullets : r.getImpactbullets())
				  {
					  bullets.add(tempbullets.getBulletpoint());
				  }
				  	temp.setImpactbullets(bullets);
				  }
				  else
				  { temp.setImpactidbullet(0);}
				  
				  if(r.getRecommendationbullets() != null)
				  {	  
					  temp.setRecommidbullet(1);
					  List<String> bullets = new ArrayList<>();
				  for(RecommendationBullet tempbullets : r.getRecommendationbullets())
				  {
					  bullets.add(tempbullets.getBulletpoint());
				  }
				  	temp.setRecommendationbullets(bullets);
				  }
				  else
				  {temp.setRecommidbullet(0);}	  
				  
				regionaltitles.add(temp);
			}
			
		}
		int regionaltitlenumbers = regionaltitles.size();
		
		
		List<EmailFtlTemplateResponseObject> asiapacifictitles = new ArrayList<>();
		Country singapore = countryrepository.getOne(8);
		Country Philli = countryrepository.getOne(9);
		Country Indonasia = countryrepository.getOne(10);
		Country Australia = countryrepository.getOne(11);
		Country Malasia = countryrepository.getOne(12);
		Country Thailand = countryrepository.getOne(13);
		Country Vietnam = countryrepository.getOne(14);
		Country Cambodia = countryrepository.getOne(15);
		Country Oman = countryrepository.getOne(16);
		Country China = countryrepository.getOne(17);
		Country korea = countryrepository.getOne(18);
		Country japan = countryrepository.getOne(19);
		Country newzeland = countryrepository.getOne(20);
		Country taiwan = countryrepository.getOne(21);
		
		
		i=1;
		for(Report r:reports)
		{
			if(r.getCountries().contains(singapore) ||r.getCountries().contains(Philli)||r.getCountries().contains(Indonasia)
					||r.getCountries().contains(Australia)||r.getCountries().contains(Malasia)||r.getCountries().contains(Thailand)	
					||r.getCountries().contains(Vietnam)||r.getCountries().contains(Cambodia)||r.getCountries().contains(Oman)
					||r.getCountries().contains(China)||r.getCountries().contains(korea)||r.getCountries().contains(japan)
					||r.getCountries().contains(newzeland)||r.getCountries().contains(taiwan)
					)
			{
				EmailFtlTemplateResponseObject temp = new EmailFtlTemplateResponseObject();
				for(Country c:r.getCountries())
				  {temp.setCountry(c.getCountryname());}
				temp.setTitle(r.getTitle());
				temp.setRisklevel(r.getRiskLevel().getId());
				 temp.setRiskcategiry(r.getRiskCategory().getId());
				
				if(i%2 == 0)
				  {temp.setId(0);}
				  else {temp.setId(1);}
				i=i+1;
				
				 if(isNullOrEmpty(r.getDescription()))
				  {
					 temp.setDiscid(0); 
				  }
				  else
				  {
					  temp.setDiscid(1);
					  temp.setDiscription(r.getDescription());
				  }
				  
				  if(isNullOrEmpty(r.getBackground()))
				  {
					 temp.setBackid(0); 
				  }
				  else
				  {
					  temp.setBackid(1); 
					  temp.setBackground(r.getBackground());
				  } 
				  
				  if(isNullOrEmpty(r.getImpact()))
				  {
					 temp.setImpactid(0); 
				  }
				  else
				  {
					  temp.setImpactid(1);
					  temp.setImpact(r.getImpact());
				  } 
				  
				  if(isNullOrEmpty(r.getRecommendation()))
				  {
					 temp.setRecommid(0); 
				  }
				  else
				  {
					  temp.setRecommid(1); 
					  temp.setRecommendation(r.getRecommendation());
				  } 
				  if(isNullOrEmpty(r.getDescription2()))
				  {
					  temp.setDiscid2(0);
				  }	  
				  else
				  {
					  temp.setDiscid2(1);
					  temp.setDescription2(r.getDescription2());
					  }
				  
				  if(isNullOrEmpty(r.getBackground2()))
				  {
					  temp.setBackid2(0);
				  }
				  else
				  {
					  temp.setBackid2(1);
					  temp.setBackground2(r.getBackground2());}
				  
				  if(isNullOrEmpty(r.getImpact2()))
				  {
					  temp.setImpactid2(0);
				  }
				  else
				  {
					  temp.setImpactid2(1);
					  temp.setImpact2(r.getImpact2());}
				  
				  if(isNullOrEmpty(r.getRecommendation2()))
				  {
					  temp.setRecommid2(0);
				  }
				  else
				  { 
					  temp.setRecommid2(1);
					  temp.setRecommendation2(r.getRecommendation2());}
				 
				  if(r.getDescriptionbullets() != null)
				  {	  
					  temp.setDiscidbullet(1);
					  List<String> bullets = new ArrayList<>();
				  for(DescriptionBullets tempbullets : r.getDescriptionbullets())
				  {
					  bullets.add(tempbullets.getBulletpoint());
				  }
				  
				  	temp.setDescriptionbullets(bullets);
				  }
				  else
				  {temp.setDiscidbullet(0);}	  
				  
				  if(r.getBackgroundbullets() != null)
				  {	  
					  temp.setBackidbullet(1);
					  List<String> bullets = new ArrayList<>();
				  for(BackgroundBullets tempbullets : r.getBackgroundbullets())
				  {
					  bullets.add(tempbullets.getBulletpoint());
				  }
				  	temp.setBackgroundbullets(bullets);
				  }
				  else
				  {temp.setBackidbullet(0);}	  
				  
				  if(r.getImpactbullets() != null)
				  {	  temp.setImpactidbullet(1);
					  List<String> bullets = new ArrayList<>();
				  for(ImpactBullets tempbullets : r.getImpactbullets())
				  {
					  bullets.add(tempbullets.getBulletpoint());
				  }
				  	temp.setImpactbullets(bullets);
				  }
				  else
				  { temp.setImpactidbullet(0);}
				  
				  if(r.getRecommendationbullets() != null)
				  {	  
					  temp.setRecommidbullet(1);
					  List<String> bullets = new ArrayList<>();
				  for(RecommendationBullet tempbullets : r.getRecommendationbullets())
				  {
					  bullets.add(tempbullets.getBulletpoint());
				  }
				  	temp.setRecommendationbullets(bullets);
				  }
				  else
				  {temp.setRecommidbullet(0);}	  
				  
				
				asiapacifictitles.add(temp);
				
			}
			
		}
		int asiapacifictitlenumbers = asiapacifictitles.size();
		
		
		String html = null;
		MimeMessage mimeMessage = mailSenderobj.createMimeMessage();
		MimeMessageHelper mimeMsgHelperObj = null;
	
		mimeMsgHelperObj = new MimeMessageHelper(mimeMessage, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, "UTF-8");
		
		
		/**
		
		if(emailrequest.getLogo()==1)
	    {mimeMsgHelperObj.addAttachment("logo.png", new ClassPathResource("logo/mitkat.jpg"));}
	    
	    if(emailrequest.getSignature()==1)
	    {mimeMsgHelperObj.addAttachment("logo.png", new ClassPathResource("logo/facebook.jpg"));
	    mimeMsgHelperObj.addAttachment("logo.png", new ClassPathResource("logo/footer.jpg"));
	    mimeMsgHelperObj.addAttachment("logo.png", new ClassPathResource("logo/linkedin.jpg"));
	    mimeMsgHelperObj.addAttachment("logo.png", new ClassPathResource("logo/twitter.jpg"));
	    }
		**/
	    
	//	mimeMsgHelperObj.addAttachment("logo.png", new ByteArrayResource(mitkatrepository.getOne((long) 1).getLogo()));
		
		Template t = freemarkerConfig.getTemplate("irt-template-innovation3.ftl");
		
		
		
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("date", date0);
		model.put("reports", reports);
		model.put("indiatitles", indiatitles);
		model.put("indiatitlenumbers", indiatitlenumbers);
		model.put("regionaltitles", regionaltitles);
		model.put("regionaltitlenumbers",regionaltitlenumbers);
		model.put("asiapacifictitles", asiapacifictitles);
		model.put("asiapacifictitlenumbers", asiapacifictitlenumbers);
		model.put("logoadd", emailrequest.getLogo());
		model.put("signatureadd", emailrequest.getSignature());
		
		
		html = FreeMarkerTemplateUtils.processTemplateIntoString(t, model);
		
		mimeMsgHelperObj.setText(html, true);
		
		String mitkat = "mitkat";
		mimeMsgHelperObj.addInline("logo.png", new ClassPathResource("logo/"+mitkat+".jpg"));
		
		
		if(emailrequest.getLogo()==2) {
		Resource resource = filestorageservice.loadFileAsResource(clientprofile.getCompanyname());
		mimeMsgHelperObj.addInline("clientlogo.png", resource);
		}
		
		mimeMsgHelperObj.addInline("facebook.png", new ClassPathResource("logo/facebook.jpg"));
		mimeMsgHelperObj.addInline("footer.png", new ClassPathResource("logo/footer.jpg"));
		mimeMsgHelperObj.addInline("linkedin.png", new ClassPathResource("logo/linkedin.jpg"));
		mimeMsgHelperObj.addInline("twitter.png", new ClassPathResource("logo/twitter.jpg"));
		
		mimeMsgHelperObj.addInline("crime.png", new ClassPathResource("logo/crime.png"));
		mimeMsgHelperObj.addInline("civil.png", new ClassPathResource("logo/civil.png"));
		mimeMsgHelperObj.addInline("cyber.png", new ClassPathResource("logo/cyber.png"));
		mimeMsgHelperObj.addInline("environment.png", new ClassPathResource("logo/environment.png"));
		mimeMsgHelperObj.addInline("health.png", new ClassPathResource("logo/health.png"));
		mimeMsgHelperObj.addInline("infra.png", new ClassPathResource("logo/infra.png"));
		mimeMsgHelperObj.addInline("terror.png", new ClassPathResource("logo/terror.png"));
		
		
		
		mimeMsgHelperObj.setFrom("Mitkat-Advisory-Services-Pvt.-Ltd."); 
		mimeMsgHelperObj.setSubject("Risk Tracker");
		
		EmailResponseObject response = new EmailResponseObject();
		int x = 0;
		List<String> failedEmails = new ArrayList<>();
		
		for(ClientEmailIds temp:emails)
		{
			
			mimeMsgHelperObj.setTo(temp.getEmail());
			
			try {
				
			mailSenderobj.send(mimeMessage);
				
			}catch(MailSendException e)
			{
				e.printStackTrace();
				
				x = x+1;
				failedEmails.add(temp.getEmail());
			}
		
		}
		response.setTotalFailedEmails(x);
		response.setFailedEmailList(failedEmails);
		return response;
	}

	@Override
	public EmailResponseObject sendEmailAlert(@Valid EmailAlertRequest emailrequest) throws MessagingException, TemplateNotFoundException, MalformedTemplateNameException, ParseException, IOException, TemplateException {
		DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MMM-yyyy");
		LocalDate date = LocalDate.now();
		String date0 = date.format(format);
		Report r = reportrepository.findById(emailrequest.getEventids()).get();
		EmailResponseObject response = new EmailResponseObject();
		List<String> failedEmails = new ArrayList<>();
		int x = 0;
		
		for(Long clientid: emailrequest.getClientids())
		{
			ClientProfile clientprofile = clientprofilerepository.findByclientid(clientid).get();
			System.out.println(clientprofile.getCompanyname());
			EmailFtlTemplateResponseObject temp = new EmailFtlTemplateResponseObject();
			
			for(Country c:r.getCountries())
			  {temp.setCountry(c.getCountryname());}
			temp.setTitle(r.getTitle());
			temp.setRisklevel(r.getRiskLevel().getId());
			temp.setRiskcategiry(r.getRiskCategory().getId());
			
			 if(isNullOrEmpty(r.getDescription()))
			  {
				 temp.setDiscid(0); 
			  }
			  else
			  {
				  temp.setDiscid(1);
				  temp.setDiscription(r.getDescription());
			  }
			  
			  if(isNullOrEmpty(r.getBackground()))
			  {
				 temp.setBackid(0); 
			  }
			  else
			  {
				  temp.setBackid(1); 
				  temp.setBackground(r.getBackground());
			  } 
			  
			  if(isNullOrEmpty(r.getImpact()))
			  {
				 temp.setImpactid(0); 
			  }
			  else
			  {
				  temp.setImpactid(1);
				  temp.setImpact(r.getImpact());
			  } 
			  
			  if(isNullOrEmpty(r.getRecommendation()))
			  {
				 temp.setRecommid(0); 
			  }
			  else
			  {
				  temp.setRecommid(1); 
				  temp.setRecommendation(r.getRecommendation());
			  }
			  if(isNullOrEmpty(r.getDescription2()))
			  {
				  temp.setDiscid2(0);
			  }	  
			  else
			  {
				  temp.setDiscid2(1);
				  temp.setDescription2(r.getDescription2());
				  }
			  
			  if(isNullOrEmpty(r.getBackground2()))
			  {
				  temp.setBackid2(0);
			  }
			  else
			  {
				  temp.setBackid2(1);
				  temp.setBackground2(r.getBackground2());}
			  
			  if(isNullOrEmpty(r.getImpact2()))
			  {
				  temp.setImpactid2(0);
			  }
			  else
			  {
				  temp.setImpactid2(1);
				  temp.setImpact2(r.getImpact2());}
			  
			  if(isNullOrEmpty(r.getRecommendation2()))
			  {
				  temp.setRecommid2(0);
			  }
			  else
			  { 
				  temp.setRecommid2(1);
				  temp.setRecommendation2(r.getRecommendation2());}
			 
			  if(r.getDescriptionbullets() != null)
			  {	  
				  temp.setDiscidbullet(1);
				  List<String> bullets = new ArrayList<>();
			  for(DescriptionBullets tempbullets : r.getDescriptionbullets())
			  {
				  bullets.add(tempbullets.getBulletpoint());
			  }
			  
			  	temp.setDescriptionbullets(bullets);
			  }
			  else
			  {temp.setDiscidbullet(0);}	  
			  
			  if(r.getBackgroundbullets() != null)
			  {	  
				  temp.setBackidbullet(1);
				  List<String> bullets = new ArrayList<>();
			  for(BackgroundBullets tempbullets : r.getBackgroundbullets())
			  {
				  bullets.add(tempbullets.getBulletpoint());
			  }
			  	temp.setBackgroundbullets(bullets);
			  }
			  else
			  {temp.setBackidbullet(0);}	  
			  
			  if(r.getImpactbullets() != null)
			  {	  temp.setImpactidbullet(1);
				  List<String> bullets = new ArrayList<>();
			  for(ImpactBullets tempbullets : r.getImpactbullets())
			  {
				  bullets.add(tempbullets.getBulletpoint());
			  }
			  	temp.setImpactbullets(bullets);
			  }
			  else
			  { temp.setImpactidbullet(0);}
			  
			  if(r.getRecommendationbullets() != null)
			  {	  
				  temp.setRecommidbullet(1);
				  List<String> bullets = new ArrayList<>();
			  for(RecommendationBullet tempbullets : r.getRecommendationbullets())
			  {
				  bullets.add(tempbullets.getBulletpoint());
			  }
			  	temp.setRecommendationbullets(bullets);
			  }
			  else
			  {temp.setRecommidbullet(0);}	  
			  
		
			  String html = null;
			  MimeMessage mimeMessage = mailSenderobj.createMimeMessage();
			  MimeMessageHelper mimeMsgHelperObj = null;
			
			 
				mimeMsgHelperObj = new MimeMessageHelper(mimeMessage, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, "UTF-8");
			
		
			  System.out.println(1);
			  Template t = freemarkerConfig.getTemplate("irt-template-emailalert.ftl");
			  System.out.println(2);
			  Map<String, Object> model = new HashMap<String, Object>();
			  System.out.println(3);
		
			  model.put("date", date0);
			  model.put("titles", temp);
			  model.put("logoadd", emailrequest.getLogo());
			  model.put("signatureadd", emailrequest.getSignature());
		
			  System.out.println(4);
				html = FreeMarkerTemplateUtils.processTemplateIntoString(t, model);
				
				System.out.println(5);
				
				mimeMsgHelperObj.setText(html, true);
				
				String mitkat = "mitkat";
				mimeMsgHelperObj.addInline("logo.png", new ClassPathResource("logo/"+mitkat+".jpg"));
				
				System.out.println(6);
				if(emailrequest.getLogo()==2) {
				Resource resource = filestorageservice.loadFileAsResource(clientprofile.getCompanyname());
				mimeMsgHelperObj.addInline("clientlogo.png", resource);
				}
				System.out.println(7);
			  mimeMsgHelperObj.addInline("facebook.png", new ClassPathResource("logo/facebook.jpg"));
				mimeMsgHelperObj.addInline("footer.png", new ClassPathResource("logo/footer.jpg"));
				mimeMsgHelperObj.addInline("linkedin.png", new ClassPathResource("logo/linkedin.jpg"));
				mimeMsgHelperObj.addInline("twitter.png", new ClassPathResource("logo/twitter.jpg"));
				
				mimeMsgHelperObj.addInline("crime.png", new ClassPathResource("logo/crime.png"));
				mimeMsgHelperObj.addInline("civil.png", new ClassPathResource("logo/civil.png"));
				mimeMsgHelperObj.addInline("cyber.png", new ClassPathResource("logo/cyber.png"));
				mimeMsgHelperObj.addInline("environment.png", new ClassPathResource("logo/environment.png"));
				mimeMsgHelperObj.addInline("health.png", new ClassPathResource("logo/health.png"));
				mimeMsgHelperObj.addInline("infra.png", new ClassPathResource("logo/infra.png"));
				mimeMsgHelperObj.addInline("terror.png", new ClassPathResource("logo/terror.png"));
				
				mimeMsgHelperObj.setFrom("Mitkat-Advisory-Services-Pvt.-Ltd."); 
				mimeMsgHelperObj.setSubject("Risk Tracker");
				System.out.println(8);
		
				for(ClientEmailIds tempemail:clientprofile.getClientemail())
				{
					System.out.println(tempemail.getEmail());
					mimeMsgHelperObj.setTo(tempemail.getEmail());
					try {
						
						mailSenderobj.send(mimeMessage);
							
						}catch(MailSendException e)
						{
							x =x+1;
							
							failedEmails.add(tempemail.getEmail());
						}
				}
		}
		System.out.println(9);
		response.setTotalFailedEmails(x);
		response.setFailedEmailList(failedEmails);
		return response;
	}
	
	

}
