package mitkat.portal.admin.service;

import java.util.List;

import mitkat.portal.admin.responseObjects.approver.RiskTrackerType;

public interface RiskTrackerTypeService {

	List<RiskTrackerType> getAll();

}
