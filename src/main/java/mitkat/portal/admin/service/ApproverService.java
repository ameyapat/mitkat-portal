package mitkat.portal.admin.service;

import java.util.List;

import mitkat.portal.admin.responseObjects.approver.EventsForProductResponse;
import mitkat.portal.admin.responseObjects.approver.TodaysReportsResponse;
import mitkat.portal.admin.responseObjects.approver.TodaysSMSResponse;

public interface ApproverService {

	List<TodaysReportsResponse> getTodaysResports();

	List<EventsForProductResponse> getEventsForResponse(long clientid);

	List<TodaysReportsResponse> getEmailAlerts();

	List<EventsForProductResponse> getEventsForEmailAlerts();

	List<TodaysSMSResponse> getSMSAlertsForApproval();


}
