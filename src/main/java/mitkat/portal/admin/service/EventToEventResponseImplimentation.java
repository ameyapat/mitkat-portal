package mitkat.portal.admin.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mitkat.portal.admin.model.clientprofile.City;
import mitkat.portal.admin.model.clientprofile.Country;
import mitkat.portal.admin.model.clientprofile.State;
import mitkat.portal.admin.model.creator.Report;
import mitkat.portal.admin.repository.user.UserRepository;
import mitkat.portal.admin.responseObjects.eventresponse.EventResponse;



@Service
public class EventToEventResponseImplimentation implements EventToEventResponse {
	
	@Autowired
	UserRepository userrepository;
	
	@Override
	public EventResponse convertEventToEventresponse(Report event) {
       EventResponse eventresponse = new EventResponse ();
       LocalDate validitydate = LocalDateTime.ofInstant(event.getValiditydate(), ZoneOffset.ofHours(6)).toLocalDate();
		eventresponse.setId(event.getId());
		eventresponse.setValiditydate(validitydate);
		eventresponse.setTitle(event.getTitle());
		eventresponse.setStatus(event.getStatus().getStatus());
		eventresponse.setStorytype(event.getRegionType().getRegiontype());
		eventresponse.setCreator(userrepository.getOne(event.getCreatedBy()).getName());
		
		String c = null;
		for(Country temp: event.getCountries())
		{
			c = temp.getCountryname();
		}
		eventresponse.setCountry(c);
		for(State temp: event.getState())
		{
			c = temp.getStatename();
		}
		eventresponse.setState(c);
		for(City temp: event.getCities())
		{
			c = temp.getCityname();
		}
		eventresponse.setCity(c);
		return eventresponse;
	}

}
