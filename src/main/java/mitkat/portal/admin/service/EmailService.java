package mitkat.portal.admin.service;

import java.io.IOException;

import javax.mail.MessagingException;
import javax.validation.Valid;

import freemarker.core.ParseException;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.TemplateException;
import freemarker.template.TemplateNotFoundException;
import mitkat.portal.admin.requestObjects.approver.EmailAlertRequest;
import mitkat.portal.admin.requestObjects.approver.EmailRequest;
import mitkat.portal.admin.responseObjects.approver.EmailResponseObject;

public interface EmailService {

	EmailResponseObject sendEmail(@Valid EmailRequest emailrequest) throws TemplateNotFoundException, MalformedTemplateNameException, ParseException, IOException, MessagingException, TemplateException;

	EmailResponseObject sendEmailAlert(@Valid EmailAlertRequest emailrequest) throws MessagingException, TemplateNotFoundException, MalformedTemplateNameException, ParseException, IOException, TemplateException;

}
