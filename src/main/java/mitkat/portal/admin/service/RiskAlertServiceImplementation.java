package mitkat.portal.admin.service;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mitkat.portal.admin.customeException.ResourceNotFoundException;
import mitkat.portal.admin.model.clientprofile.City;
import mitkat.portal.admin.model.clientprofile.Country;
import mitkat.portal.admin.model.clientprofile.State;
import mitkat.portal.admin.model.creator.ImpactIndustry;
import mitkat.portal.admin.model.creator.RiskAlert;
import mitkat.portal.admin.repository.application.CityRepository;
import mitkat.portal.admin.repository.application.Countryrepository;
import mitkat.portal.admin.repository.application.ImpactIndustryRepository;
import mitkat.portal.admin.repository.application.RegionTypeRepository;
import mitkat.portal.admin.repository.application.RiskAlertRepository;
import mitkat.portal.admin.repository.application.RiskCategoryRepository;
import mitkat.portal.admin.repository.application.RiskLevelRepository;
import mitkat.portal.admin.repository.application.StateRepository;
import mitkat.portal.admin.repository.application.StatusRepository;
import mitkat.portal.admin.repository.user.UserRepository;
import mitkat.portal.admin.requestObjects.creator.RiskAlertRequestObject;
import mitkat.portal.admin.responseObjects.approver.TodaysSMSResponse;
import mitkat.portal.admin.responseObjects.client.RiskAlertDetailsResponse;
import mitkat.portal.admin.responseObjects.creator.SMSAlertResponseApprover;
import mitkat.portal.admin.security.UserPrincipal;

@Service
public class RiskAlertServiceImplementation implements RiskAlertService{

	@Autowired
	RiskAlertRepository riskalertrepository;
	
	@Autowired
	Countryrepository countryrepository;
	
	@Autowired
	StateRepository staterepository;
	
	@Autowired
	CityRepository cityrepository;
	
	@Autowired
	RegionTypeRepository regiontyperepository;
	
	@Autowired
	RiskLevelRepository risklevelrepository;
	
	@Autowired
	StatusRepository statusrepository;
	
	@Autowired
	RiskCategoryRepository riskcategoryrepository;
	
	@Autowired
	ImpactIndustryRepository impactindustryrepository;
	
	@Autowired
	UserRepository userrepository;
	
	@Override
	public void addRiskAlert(@Valid RiskAlertRequestObject request, UserPrincipal userprinciple) {
		
		RiskAlert riskalert = new RiskAlert();
		
		riskalert.setTitle(request.getTitle());
		
		if(request.getLinks() != null)
		{riskalert.setLinks(request.getLinks());}
		
		riskalert.setLatitude(request.getLatitude());
		
		riskalert.setLongitude(request.getLongitude());
		
		riskalert.setRiskLevel1(risklevelrepository.getOne(request.getRiskLevel()));
		
		riskalert.setRiskCategory1(riskcategoryrepository.getOne(request.getRiskcategory()));
		
		riskalert.setRegionType1(regiontyperepository.getOne(request.getRegionType()));
		
		if(request.getCountries() != null)
		{List<Country> tempcountry = new ArrayList<>();
		for(Long i : request.getCountries())
		{
			tempcountry.add(countryrepository.getOne(i));
		}
		riskalert.setCountries(tempcountry);
		}
		
		if(request.getState() != null)
		{List<State> tempstate = new ArrayList<>();
		for(Long i : request.getState())
		{
			tempstate.add(staterepository.getOne(i));
		}
		riskalert.setState(tempstate);
		}
		
		if(request.getCities() != null)
		{List<City> tempcity = new ArrayList<>();
		for(Long i : request.getCities())
		{
			tempcity.add(cityrepository.getOne(i));
		}
		riskalert.setCities(tempcity);
		}
		
		List<ImpactIndustry> tempindustry = new ArrayList<>();
		for(Long i : request.getImpactindustry())
		{
			tempindustry.add(impactindustryrepository.getOne(i));
		}
		riskalert.setImpactindustry(tempindustry);

		riskalert.setStatus1(statusrepository.getOne((long) 1));
		
		LocalDateTime todayKolkata = LocalDateTime.now(ZoneId.of("Asia/Kolkata"));
		Instant instant = todayKolkata.atZone(ZoneId.of("Asia/Kolkata")).toInstant();
		
		
		
	//	Instant instant1 = LocalDateTime.now().atZone(ZoneId.of("Asia/Kolkata")).toInstant();
		
		
		
		riskalert.setAlertdate(instant);
		
		riskalertrepository.save(riskalert);
	}

	@Override
	public RiskAlertDetailsResponse getRiskAlert(long alertid) {
		
		RiskAlertDetailsResponse response = new RiskAlertDetailsResponse();
		RiskAlert alert = riskalertrepository.findById(alertid).orElseThrow(
                () -> new ResourceNotFoundException("Risk Alert", "id", alertid));
		
		response.setAlertdate(alert.getAlertdate());
		response.setCities(alert.getCities());
		response.setCountries(alert.getCountries());
		response.setLatitude(alert.getLatitude());
		response.setLongitude(alert.getLongitude());
		response.setRegionType1(alert.getRegionType1());
		response.setRiskCategory1(alert.getRiskCategory1());
		response.setRiskLevel1(alert.getRiskLevel1());
		response.setState(alert.getState());
		response.setTitle(alert.getTitle());
		
		return response;
	}

	@Override
	public SMSAlertResponseApprover converToAlertResponseApprover(RiskAlert riskalert) {
		SMSAlertResponseApprover response = new SMSAlertResponseApprover();
		
		LocalDate storydate = LocalDateTime.ofInstant(riskalert.getAlertdate(), ZoneOffset.ofHours(6)).toLocalDate();
		response.setAlertdate(storydate);
		
		response.setCities(riskalert.getCities());
		
		response.setCountries(riskalert.getCountries());
		
		response.setId(riskalert.getId());
		
		response.setImpactindustry(riskalert.getImpactindustry());
		
		response.setLatitude(riskalert.getLatitude());
		
		response.setLinks(riskalert.getLinks());
		
		response.setLongitude(riskalert.getLongitude());
		
		response.setRiskCategory(riskalert.getRiskCategory1());
		
		response.setRiskLevel(riskalert.getRiskLevel1());
		
		response.setState(riskalert.getState());
		
		response.setStatus(riskalert.getStatus1());
		
		response.setTitle(riskalert.getTitle());
		
		response.setRegionType(riskalert.getRegionType1());
		
		return response;
	}

	@Override
	public void updateRiskAlert(@Valid RiskAlertRequestObject request, UserPrincipal userprinciple) {
		RiskAlert riskalert = riskalertrepository.getOne((long) request.getId());

		riskalert.setTitle(request.getTitle());
		
		if(request.getLinks() != null)
		{riskalert.setLinks(request.getLinks());}
		
		riskalert.setLatitude(request.getLatitude());
		
		riskalert.setLongitude(request.getLongitude());
		
		riskalert.setRiskLevel1(risklevelrepository.getOne(request.getRiskLevel()));
		
		riskalert.setRiskCategory1(riskcategoryrepository.getOne(request.getRiskcategory()));
		
		riskalert.setRegionType1(regiontyperepository.getOne(request.getRegionType()));
		
		if(request.getCountries() != null)
		{List<Country> tempcountry = new ArrayList<>();
		for(Long i : request.getCountries())
		{
			tempcountry.add(countryrepository.getOne(i));
		}
		riskalert.setCountries(tempcountry);
		}
		
		if(request.getState() != null)
		{List<State> tempstate = new ArrayList<>();
		for(Long i : request.getState())
		{
			tempstate.add(staterepository.getOne(i));
		}
		riskalert.setState(tempstate);
		}
		
		if(request.getCities() != null)
		{List<City> tempcity = new ArrayList<>();
		for(Long i : request.getCities())
		{
			tempcity.add(cityrepository.getOne(i));
		}
		riskalert.setCities(tempcity);
		}
		
		List<ImpactIndustry> tempindustry = new ArrayList<>();
		for(Long i : request.getImpactindustry())
		{
			tempindustry.add(impactindustryrepository.getOne(i));
		}
		riskalert.setImpactindustry(tempindustry);

	
		
		riskalert.setStatus1(statusrepository.getOne(request.getStatus()));
		
		LocalDateTime todayKolkata = LocalDateTime.now(ZoneId.of("Asia/Kolkata"));
		Instant instant = todayKolkata.atZone(ZoneId.of("Asia/Kolkata")).toInstant();
		
		
		
	//	Instant instant1 = LocalDateTime.now().atZone(ZoneId.of("Asia/Kolkata")).toInstant();
		
		
		
		riskalert.setAlertdate(instant);
		
		riskalertrepository.save(riskalert);
		
		
		
	}

	@Override
	public List<TodaysSMSResponse> getTodaysDispatchList() {

		Instant date = Instant.now().minus(2, ChronoUnit.DAYS);
		
		List<RiskAlert> riskalerts = new ArrayList<>();
		
		try {
		 riskalerts = riskalertrepository.getRiskAlertsForApproval(date);
		}catch(Exception e) {}
		
		List<TodaysSMSResponse> response = new ArrayList<>();
		
		if(!riskalerts.isEmpty())
		{
			
		for(RiskAlert tempreport: riskalerts)
		{
			if(tempreport.getStatus1().getId() == 2)
		
			{TodaysSMSResponse tempresponse = new TodaysSMSResponse();
			tempresponse.setId(tempreport.getId());
			
			LocalDate storydate = LocalDateTime.ofInstant(tempreport.getAlertdate(), ZoneOffset.ofHours(6)).toLocalDate();
			tempresponse.setDate(storydate);
			tempresponse.setTitle(tempreport.getTitle());
			
			for(Country tempcountry:tempreport.getCountries())
			{
				tempresponse.setCountry(tempcountry.getCountryname());
			}
			
			for(State tempstate:tempreport.getState())
			{
				tempresponse.setState(tempstate.getStatename());
			}
			
			for(City tempcity: tempreport.getCities())
			{
				
				tempresponse.setCity(tempcity.getCityname());
			}
	
			tempresponse.setStatus(tempreport.getStatus1());
			tempresponse.setCreator(userrepository.getOne(tempreport.getCreatedBy()).getName());
			
			response.add(tempresponse);
			}
			
	}
			
		
		}
		return response;
	}
	
	
	
}
