package mitkat.portal.admin.service;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mitkat.portal.admin.model.clientprofile.City;
import mitkat.portal.admin.model.clientprofile.ClientProfile;
import mitkat.portal.admin.model.clientprofile.Country;
import mitkat.portal.admin.model.clientprofile.State;
import mitkat.portal.admin.model.creator.Report;
import mitkat.portal.admin.model.creator.RiskAlert;
import mitkat.portal.admin.repository.application.ClientProfileRepository;
import mitkat.portal.admin.repository.application.ReportRepository;
import mitkat.portal.admin.repository.application.RiskAlertRepository;
import mitkat.portal.admin.repository.user.UserRepository;
import mitkat.portal.admin.responseObjects.approver.EventsForProductResponse;
import mitkat.portal.admin.responseObjects.approver.TodaysReportsResponse;
import mitkat.portal.admin.responseObjects.approver.TodaysSMSResponse;

@Service
public class ApproverServiceImplementation implements ApproverService {

	@Autowired
	ReportRepository reportrepository;
	
	@Autowired
	UserRepository userrepository;
	
	@Autowired
	ClientProfileRepository clientprofilerepository;
	
	@Autowired
	RiskAlertRepository riskalertrepository;
	
	@Override
	public List<TodaysReportsResponse> getTodaysResports() {
		
	//	LocalDate localDate = LocalDate.now(ZoneId.of("GMT+05:30"));
	//	Instant date = localDate.atStartOfDay().toInstant(ZoneOffset.UTC).minus(6, ChronoUnit.HOURS) ;
	//	List<Report> reports =  reportrepository.getTodaysReports(date);
		
		List<Report> reports =  reportrepository.getUnapprovedReports((long) 1);
		
		List<TodaysReportsResponse> response = new ArrayList<>();
		
		for(Report tempreport:reports)
		{
			TodaysReportsResponse tempresponse = new TodaysReportsResponse();
			tempresponse.setId(tempreport.getId());
			
			LocalDate storydate = LocalDateTime.ofInstant(tempreport.getStorydate(), ZoneOffset.ofHours(6)).toLocalDate();
			tempresponse.setDate(storydate);
			tempresponse.setTitle(tempreport.getTitle());
			
			for(Country tempcountry:tempreport.getCountries())
			{
				tempresponse.setCountry(tempcountry.getCountryname());
			}
			
			for(State tempstate:tempreport.getState())
			{
				tempresponse.setState(tempstate.getStatename());
			}
			
			for(City tempcity: tempreport.getCities())
			{
				
				tempresponse.setCity(tempcity.getCityname());
			}
	
			tempresponse.setStatus(tempreport.getStatus());
			tempresponse.setCreator(userrepository.getOne(tempreport.getCreatedBy()).getName());
			tempresponse.setType(tempreport.getRegionType().getRegiontype());
			tempresponse.setImportance(tempreport.isImportance());
			response.add(tempresponse);
		}
		return response;
	}

	@Override
	public List<TodaysReportsResponse> getEmailAlerts() {
		LocalDate localDate = LocalDate.now(ZoneId.of("GMT+05:30"));
		Instant date = localDate.atStartOfDay().toInstant(ZoneOffset.UTC).minus(30, ChronoUnit.HOURS) ;
		List<Report> reports =  reportrepository.getTodaysReports(date);
		
		List<TodaysReportsResponse> response = new ArrayList<>();
		
		for(Report tempreport:reports)
		{
			if(tempreport.isRiskalert())
			{TodaysReportsResponse tempresponse = new TodaysReportsResponse();
			tempresponse.setId(tempreport.getId());
			
			LocalDate storydate = LocalDateTime.ofInstant(tempreport.getStorydate(), ZoneOffset.ofHours(6)).toLocalDate();
			tempresponse.setDate(storydate);
			tempresponse.setTitle(tempreport.getTitle());
			
			for(Country tempcountry:tempreport.getCountries())
			{
				tempresponse.setCountry(tempcountry.getCountryname());
			}
			
			for(State tempstate:tempreport.getState())
			{
				tempresponse.setState(tempstate.getStatename());
			}
			
			for(City tempcity: tempreport.getCities())
			{
				
				tempresponse.setCity(tempcity.getCityname());
			}
	
			tempresponse.setStatus(tempreport.getStatus());
			tempresponse.setCreator(userrepository.getOne(tempreport.getCreatedBy()).getName());
			tempresponse.setType(tempreport.getRegionType().getRegiontype());
			tempresponse.setImportance(tempreport.isImportance());
			response.add(tempresponse);
			}
		}
		return response;
	}
	
	
	@Override
	public List<EventsForProductResponse> getEventsForResponse(long clientid) {
		
		List<EventsForProductResponse> response = new ArrayList<>();
		
		LocalDate localDate = LocalDate.now(ZoneId.of("GMT+05:30"));
		Instant date = localDate.atStartOfDay().toInstant(ZoneOffset.UTC).minus(6, ChronoUnit.HOURS) ;
		// List<Report> reports =  reportrepository.getTodaysReports(date);
		
		Instant enddate = date.plus(1, ChronoUnit.DAYS) ;
		List<Report> reports =  reportrepository.getTodaysReportsNew(date,enddate);
		
		List<Report> reportsnew = new ArrayList<>();
		
		for(Report temp: reports)
		{
			if(temp.getStatus().getId()==2)
			{
				reportsnew.add(temp);
			}
			
		}
		
		
		if(clientid == 0)
		{
			for(Report temp:reportsnew)
			{
				EventsForProductResponse tempresponse = new EventsForProductResponse();
				tempresponse.setId(temp.getId());
				LocalDate storydate = LocalDateTime.ofInstant(temp.getStorydate(), ZoneOffset.ofHours(6)).toLocalDate();
				tempresponse.setDate(storydate);
				tempresponse.setTitle(temp.getTitle());
				tempresponse.setStorytype(temp.getRegionType().getRegiontype());
				tempresponse.setImportance(temp.isImportance());
				for(Country tempcountry:temp.getCountries())
				{tempresponse.setCountry(tempcountry.getCountryname());}
				for(State tempstate:temp.getState())
				{tempresponse.setState(tempstate.getStatename());}
				for(City tempcity:temp.getCities())
				{tempresponse.setMetro(tempcity.getCityname());}
				tempresponse.setRelevency(0);
				response.add(tempresponse);
			}	
		}
		else {
			
		ClientProfile clientprofile = clientprofilerepository.findByclientid (clientid).get();
		List<Country> clientcountry = clientprofile.getCountries();
		List<City> clientcity = clientprofile.getCities();
		List<State> clientstate = clientprofile.getState();
		
		
		
		for(Report temp:reportsnew)
		{
			EventsForProductResponse tempresponse = new EventsForProductResponse();
			tempresponse.setId(temp.getId());
			LocalDate storydate = LocalDateTime.ofInstant(temp.getStorydate(), ZoneOffset.ofHours(6)).toLocalDate();
			tempresponse.setDate(storydate);
			tempresponse.setTitle(temp.getTitle());
			tempresponse.setStorytype(temp.getRegionType().getRegiontype());
			
			for(Country tempcountry:temp.getCountries())
			{tempresponse.setCountry(tempcountry.getCountryname());}
			for(State tempstate:temp.getState())
			{tempresponse.setState(tempstate.getStatename());}
			for(City tempcity:temp.getCities())
			{tempresponse.setMetro(tempcity.getCityname());}
			
			tempresponse.setImportance(temp.isImportance());
			tempresponse.setRelevency(0);
			int i = 0;
			
			for(Country tempcountry:temp.getCountries())
			{ if(clientcountry.contains(tempcountry))
			{i = i+1;}}
			for(State tempstate:temp.getState())
			{if(clientstate.contains(tempstate))
				{i=i+1;}}
			for(City tempcity:temp.getCities())
			{if(clientcity.contains(tempcity))
				{i=i+1;}}
			
			if(i>0)
			{tempresponse.setRelevency(1);}
			response.add(tempresponse);
		}
		}
		return response;
	}

	@Override
	public List<EventsForProductResponse> getEventsForEmailAlerts() {
		
		List<EventsForProductResponse> response = new ArrayList<>();
		
		LocalDate localDate = LocalDate.now(ZoneId.of("GMT+05:30"));
		Instant date = localDate.atStartOfDay().toInstant(ZoneOffset.UTC).minus(6, ChronoUnit.HOURS) ;
		List<Report> reports =  reportrepository.getTodaysReports(date);
		List<Report> reportsnew = new ArrayList<>();
		
		for(Report temp: reports)
		{
			if(temp.getStatus().getId()==2 && temp.isRiskalert())
			{
				reportsnew.add(temp);
			}
			
		}
		
		
		for(Report temp:reportsnew)
		{
			EventsForProductResponse tempresponse = new EventsForProductResponse();
			tempresponse.setId(temp.getId());
			LocalDate storydate = LocalDateTime.ofInstant(temp.getStorydate(), ZoneOffset.ofHours(6)).toLocalDate();
			tempresponse.setDate(storydate);
			tempresponse.setTitle(temp.getTitle());
			tempresponse.setStorytype(temp.getRegionType().getRegiontype());
			tempresponse.setImportance(temp.isImportance());
			tempresponse.setRelevency(0);
			
			for(Country tempcountry:temp.getCountries())
			{tempresponse.setCountry(tempcountry.getCountryname());}
			for(State tempstate:temp.getState())
			{tempresponse.setState(tempstate.getStatename());}
			for(City tempcity:temp.getCities())
			{tempresponse.setMetro(tempcity.getCityname());}
			
			response.add(tempresponse);
		}	
		
		
		return response;
	}

	@Override
	public List<TodaysSMSResponse> getSMSAlertsForApproval() {
		
		Instant date = Instant.now().minus(2, ChronoUnit.DAYS);
		
		List<RiskAlert> riskalerts = new ArrayList<>();
		
		try {
		 riskalerts = riskalertrepository.getRiskAlertsForApproval(date);
		}catch(Exception e) {}
		
		List<TodaysSMSResponse> response = new ArrayList<>();
		
		if(!riskalerts.isEmpty())
		{
		for(RiskAlert tempreport: riskalerts)
		{
			TodaysSMSResponse tempresponse = new TodaysSMSResponse();
			tempresponse.setId(tempreport.getId());
			
			LocalDate storydate = LocalDateTime.ofInstant(tempreport.getAlertdate(), ZoneOffset.ofHours(6)).toLocalDate();
			tempresponse.setDate(storydate);
			tempresponse.setTitle(tempreport.getTitle());
			
			for(Country tempcountry:tempreport.getCountries())
			{
				tempresponse.setCountry(tempcountry.getCountryname());
			}
			
			for(State tempstate:tempreport.getState())
			{
				tempresponse.setState(tempstate.getStatename());
			}
			
			for(City tempcity: tempreport.getCities())
			{
				
				tempresponse.setCity(tempcity.getCityname());
			}
	
			tempresponse.setStatus(tempreport.getStatus1());
			tempresponse.setCreator(userrepository.getOne(tempreport.getCreatedBy()).getName());
			
			response.add(tempresponse);
			
			
			
		}
		}
		return response;
	}

	


}
