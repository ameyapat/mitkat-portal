package mitkat.portal.admin.service;

import javax.validation.Valid;

import mitkat.portal.admin.model.creator.Report;
import mitkat.portal.admin.requestObjects.creator.EventRequestObject;
import mitkat.portal.admin.requestObjects.creator.UpdateRequestObject;
import mitkat.portal.admin.responseObjects.approver.EventDetailResponseApprover;
import mitkat.portal.admin.responseObjects.creator.EventDetailsResponseCreator;
import mitkat.portal.admin.security.UserPrincipal;

public interface ReportService {

	void addNewReport(@Valid EventRequestObject eventrequest,UserPrincipal currentUser);

	void updateReport(EventRequestObject request,UserPrincipal userprincipal);

	EventDetailsResponseCreator converToEventResponse(Report report);

	void editReport(UserPrincipal currentUser, @Valid EventRequestObject eventrequest);

	void addUpdate(@Valid UpdateRequestObject request);

	void editUpdate(@Valid UpdateRequestObject request);

	void deleteUpdate(UserPrincipal userprinciple, long updateid);

	EventDetailResponseApprover converToEventResponseApprover(Report report);

	void addNewEmailAlert(@Valid EventRequestObject eventrequest, UserPrincipal currentUser);

	void deletereport(@Valid long eventid);

	

}
