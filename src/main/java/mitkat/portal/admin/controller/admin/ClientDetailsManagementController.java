package mitkat.portal.admin.controller.admin;

import java.io.IOException;


import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonFormat;

import mitkat.portal.admin.model.clientprofile.ClientProfile;
import mitkat.portal.admin.model.clientprofile.Country;
import mitkat.portal.admin.model.clientprofile.ProductType;
import mitkat.portal.admin.model.clientprofile.State;
import mitkat.portal.admin.model.creator.ImpactIndustry;
import mitkat.portal.admin.repository.application.CityRepository;
import mitkat.portal.admin.repository.application.ClientEmailIdsRepository;
import mitkat.portal.admin.repository.application.ClientProfileRepository;
import mitkat.portal.admin.repository.application.Countryrepository;
import mitkat.portal.admin.repository.application.ImpactIndustryRepository;
import mitkat.portal.admin.repository.application.ProductTypeRepository;
import mitkat.portal.admin.repository.application.StateRepository;
import mitkat.portal.admin.requestObjects.clientprofile.ClientProfileRequestObject;
import mitkat.portal.admin.requestObjects.clientprofile.LogoRequest;
import mitkat.portal.admin.requestObjects.clientprofile.daterequest;
import mitkat.portal.admin.responseObjects.authentication.ApiResponse;
import mitkat.portal.admin.security.UserPrincipal;
import mitkat.portal.admin.service.ClientDetailService;
import mitkat.portal.admin.customeException.ResourceNotFoundException;
import mitkat.portal.admin.model.clientprofile.City;
import mitkat.portal.admin.model.clientprofile.ClientEmailIds;

@RestController
@RequestMapping("/api/admin/clientdetails")
public class ClientDetailsManagementController {

	@Autowired
	ClientProfileRepository clientprofilerepository;
	
	@Autowired
	ClientEmailIdsRepository clientemailrepository;
	
	@Autowired
	Countryrepository countryrepository;
	
	@Autowired
	StateRepository staterepository;
	
	@Autowired
	CityRepository cityrepository;
	
	@Autowired
	ProductTypeRepository producttyperepository;
	
	@Autowired
	ImpactIndustryRepository impactindustryrepository;
	
	@Autowired
	ClientDetailService clientdetailservice;
	
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(ClientDetailsManagementController.class);
	
	 @PostMapping("/add")
	 @PreAuthorize("hasRole('ADMIN')")
	 @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
	 public ResponseEntity<?> uploadClientDetails(@Valid @RequestBody ClientProfileRequestObject clientprofilerequest,UserPrincipal userprincipal ) throws IOException,Exception 
	 {
		 @SuppressWarnings("unused")
		ClientProfile profile =clientdetailservice.uploadClientDetails(clientprofilerequest,userprincipal);	 
		 ResponseEntity<?> responseEntity = new ResponseEntity<Object>(new ApiResponse(true, "Client registered successfully"),HttpStatus.CREATED);
		return responseEntity;
	 }
	
	 
	 @PostMapping("/update")
	 @PreAuthorize("hasRole('ADMIN')")
	 @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
	 public ResponseEntity<?> updateClientDetails(@Valid @RequestBody ClientProfileRequestObject clientprofilerequest,UserPrincipal userprincipal ) throws IOException,Exception 
	 {
		 @SuppressWarnings("unused")
		ClientProfile profile =clientdetailservice.updateClientDetails(clientprofilerequest,userprincipal);	 
		 ResponseEntity<?> responseEntity = new ResponseEntity<Object>(new ApiResponse(true, "Client Updated successfully"),HttpStatus.CREATED);
		return responseEntity;
	 }
	 
	 
	 @PostMapping("/{clientid}")
	 @PreAuthorize("hasRole('ADMIN')")
	 public ClientProfile getClientDetails (@Valid @PathVariable("clientid") int clientid )
	 {
		ClientProfile tempprofile = clientprofilerepository.findByclientid(clientid).orElseThrow(() -> new ResourceNotFoundException("Client", "Client Id", clientid));
		return tempprofile; 
	 }
	 
	 
	 
	 
	 @PostMapping("/{clientid}/update/companyname/{companyname}")
	 @PreAuthorize("hasRole('ADMIN')")
	 public ResponseEntity<?> updateCompanyName (@Valid @PathVariable ("clientid") int clientid, @Valid @PathVariable ("companyname") String companyname )
	 {	ClientProfile tempprofile = clientprofilerepository.findByclientid(clientid).orElseThrow(() -> new ResourceNotFoundException("Client", "Client Id", clientid));
	 	
	 	tempprofile.setCompanyname(companyname);
	 	clientprofilerepository.save(tempprofile);
		ResponseEntity<?> responseEntity = new ResponseEntity<Object>(new ApiResponse(true, "Company name updated successfully"),HttpStatus.CREATED);
		return responseEntity;
	 }

	 @PostMapping("/{clientid}/update/contractdate")
	 @PreAuthorize("hasRole('ADMIN')")
	 @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
	 public ResponseEntity<?> updateContractDate (@Valid @RequestBody daterequest datein, @Valid @PathVariable ("clientid") int clientid)
	 {	ClientProfile tempprofile = clientprofilerepository.findByclientid(clientid).orElseThrow(() -> new ResourceNotFoundException("Client", "Client Id", clientid));;
	 	
	 	tempprofile.setContractdate(datein.getDatein());;
	 	clientprofilerepository.save(tempprofile);
		return new ResponseEntity(new ApiResponse(true, "Contract date updated successfully"),HttpStatus.CREATED);
	 }

	 @PostMapping("/{clientid}/update/contractenddate")
	 @PreAuthorize("hasRole('ADMIN')")
	 @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
	 public ResponseEntity<?> updateContractEndDate (@Valid @RequestBody daterequest datein, @Valid @PathVariable ("clientid") int clientid)
	 {	ClientProfile tempprofile = clientprofilerepository.findByclientid(clientid).orElseThrow(() -> new ResourceNotFoundException("Client", "Client Id", clientid));;
	 	
	 	tempprofile.setContractenddate(datein.getDatein());;
	 	clientprofilerepository.save(tempprofile);
		return new ResponseEntity(new ApiResponse(true, "Contract End date updated successfully"),HttpStatus.CREATED);
	 }
	 
	 @PostMapping("/{clientid}/update/contactnumber/{contactnumber}")
	 @PreAuthorize("hasRole('ADMIN')")
	 public ResponseEntity<?> updateContactnumber (@Valid @PathVariable ("clientid") int clientid, @Valid @PathVariable ("contactnumber") String contactnumber )
	 {	ClientProfile tempprofile = clientprofilerepository.findByclientid(clientid).orElseThrow(() -> new ResourceNotFoundException("Client", "Client Id", clientid));;
	 	
	 	tempprofile.setContactnumber(contactnumber);
	 	clientprofilerepository.save(tempprofile);
		return new ResponseEntity(new ApiResponse(true, "Contact number updated successfully"),HttpStatus.CREATED);
	 }
	 
	 @PostMapping("/{clientid}/update/contactperson/{contactperson}")
	 @PreAuthorize("hasRole('ADMIN')")
	 public ResponseEntity<?> updateContactPerson (@Valid @PathVariable ("clientid") int clientid, @Valid @PathVariable ("contactperson") String contactperson )
	 {	ClientProfile tempprofile = clientprofilerepository.findByclientid(clientid).orElseThrow(() -> new ResourceNotFoundException("Client", "Client Id", clientid));;
	 	
	 	tempprofile.setContactperson(contactperson);
	 	clientprofilerepository.save(tempprofile);
		return new ResponseEntity(new ApiResponse(true, "Contact person name updated successfully"),HttpStatus.CREATED);
	 }
	
	 @PostMapping("/{clientid}/update/logo")
	 @PreAuthorize("hasRole('ADMIN')")
	 public ResponseEntity<?> updateContactPerson (@Valid @PathVariable ("clientid") int clientid, @RequestBody LogoRequest logo) throws IOException
	 {	ClientProfile tempprofile = clientprofilerepository.findByclientid(clientid).orElseThrow(() -> new ResourceNotFoundException("Client", "Client Id", clientid));;
	 	
	 	tempprofile.setLogo(logo.getFilein().getBytes());
	 	clientprofilerepository.save(tempprofile);
		return new ResponseEntity(new ApiResponse(true, "Company logo updated successfully"),HttpStatus.CREATED);
	 }

	 @PostMapping("/{clientid}/update/email/{emailid_id}/{newemailaddress}")
	 @PreAuthorize("hasRole('ADMIN')")
	 public ResponseEntity<?> updateEmailID(@Valid @PathVariable ("clientid") int clientid, @Valid @PathVariable ("emailid_id") long id,@Valid @PathVariable ("newemailaddress") String newemailaddress )
	 {		ClientEmailIds temp = clientemailrepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Email", "Email Id", id));;
	 		temp.setEmail(newemailaddress);
	 		clientemailrepository.save(temp);
		 return new ResponseEntity(new ApiResponse(true, "Email updated successfully"),HttpStatus.CREATED);
	 }
	 
	 @PostMapping("/{clientid}/update/email/{emailid_id}")
	 @PreAuthorize("hasRole('ADMIN')")
	 public ResponseEntity<?> deleteEmailID(@Valid @PathVariable ("clientid") int clientid, @Valid @PathVariable ("emailid_id") long id)
	 {		
	 		clientemailrepository.deleteById(id);
		 return new ResponseEntity(new ApiResponse(true, "Email updated successfully"),HttpStatus.CREATED);
	 }
	 
	 @PostMapping("/{clientid}/update/addemail/{newemailaddress}")
	 @PreAuthorize("hasRole('ADMIN')")
	 public ResponseEntity<?> addEmailID(@Valid @PathVariable ("clientid") int clientid, @Valid @PathVariable ("newemailaddress") String newemailaddress)
	 {		ClientProfile temp = clientprofilerepository.findByclientid(clientid).orElseThrow(() -> new ResourceNotFoundException("Client", "Client Id", clientid));;
	 		ClientEmailIds tempemail = new ClientEmailIds();
	 		tempemail.setEmail(newemailaddress);
	 		tempemail.setProfileemail(temp);
	 		temp.getClientemail().add(tempemail);
	 		clientprofilerepository.save(temp);
		 return new ResponseEntity(new ApiResponse(true, "Email updated successfully"),HttpStatus.CREATED);
	 }
	 
	 @PostMapping("/{clientid}/update/country/{initialcountryid}/{newcountryid}")
	 @PreAuthorize("hasRole('ADMIN')")
	 public ResponseEntity<?> updateCountry(@Valid @PathVariable ("clientid") int clientid, @Valid @PathVariable ("initialcountryid") long initid,@Valid @PathVariable ("newcountryid") long finalid )
	 {		ClientProfile temp = clientprofilerepository.findByclientid(clientid).orElseThrow(() -> new ResourceNotFoundException("Client", "Client Id", clientid));
	 		Country tempcountry = countryrepository.findById(initid).orElseThrow(() -> new ResourceNotFoundException("Country", "Country Id", initid));
	 		temp.getCountries().remove(tempcountry);
	 		Country newcountry = countryrepository.findById(initid).orElseThrow(() -> new ResourceNotFoundException("Country", "Country Id", initid));
	 		temp.getCountries().add(newcountry);
	 		clientprofilerepository.save(temp);
		 return new ResponseEntity(new ApiResponse(true, "Country updated successfully"),HttpStatus.CREATED);
	 }
	 

	 @PostMapping("/{clientid}/update/country/{initialcountryid}")
	 @PreAuthorize("hasRole('ADMIN')")
	 public ResponseEntity<?> deleteCountry(@Valid @PathVariable ("clientid") int clientid, @Valid @PathVariable ("initialcountryid") long initid )
	 {ClientProfile temp = clientprofilerepository.findByclientid(clientid).orElseThrow(() -> new ResourceNotFoundException("Client", "Client Id", clientid));;
		Country tempcountry = countryrepository.findById(initid).orElseThrow(() -> new ResourceNotFoundException("Country", "Country Id", initid));
		temp.getCountries().remove(tempcountry);
		clientprofilerepository.save(temp);
		 return new ResponseEntity(new ApiResponse(true, "Country updated successfully"),HttpStatus.CREATED);
	 
	 }
	 

	 @PostMapping("/{clientid}/update/addcountry/{newcountryid}")
	 @PreAuthorize("hasRole('ADMIN')")
	 public ResponseEntity<?> addCountry(@Valid @PathVariable ("clientid") int clientid,@Valid @PathVariable ("newcountryid") long finalid )
	 {		ClientProfile temp = clientprofilerepository.findByclientid(clientid).orElseThrow(() -> new ResourceNotFoundException("Client", "Client Id", clientid));;
	 		Country newcountry = countryrepository.findById(finalid).orElseThrow(() -> new ResourceNotFoundException("Country", "Country Id", finalid));;
	 		temp.getCountries().add(newcountry);
	 		clientprofilerepository.save(temp);
		 return new ResponseEntity(new ApiResponse(true, "Country updated successfully"),HttpStatus.CREATED);
	 }
	 
	 
	 @PostMapping("/{clientid}/update/state/{initialstateid}/{newstateid}")
	 @PreAuthorize("hasRole('ADMIN')")
	 public ResponseEntity<?> updateState(@Valid @PathVariable ("clientid") int clientid, @Valid @PathVariable ("initialstateid") long initid,@Valid @PathVariable ("newstateid") long finalid )
	 {		ClientProfile temp = clientprofilerepository.findByclientid(clientid).orElseThrow(() -> new ResourceNotFoundException("Client", "Client Id", clientid));;
	 		State tempstate = staterepository.findById(initid).orElseThrow(() -> new ResourceNotFoundException("State", "State Id", initid));;
	 		temp.getState().remove(tempstate);
	 		State newstate = staterepository.findById(finalid).orElseThrow(() -> new ResourceNotFoundException("State", "State Id", finalid));;
	 		temp.getState().add(newstate);
	 		clientprofilerepository.save(temp);
		 return new ResponseEntity(new ApiResponse(true, "State updated successfully"),HttpStatus.CREATED);
	 }
	 

	 @PostMapping("/{clientid}/update/state/{initialstateid}")
	 @PreAuthorize("hasRole('ADMIN')")
	 public ResponseEntity<?> deleteState(@Valid @PathVariable ("clientid") int clientid, @Valid @PathVariable ("initialstateid") long initid )
	 {	ClientProfile temp = clientprofilerepository.findByclientid(clientid).orElseThrow(() -> new ResourceNotFoundException("Client", "Client Id", clientid));
	 	State tempstate = staterepository.findById(initid).orElseThrow(() -> new ResourceNotFoundException("State", "State Id", initid));;
	 	temp.getState().remove(tempstate);
		clientprofilerepository.save(temp);
		 return new ResponseEntity(new ApiResponse(true, "State updated successfully"),HttpStatus.CREATED);
	 
	 }
	 

	 @PostMapping("/{clientid}/update/addstate/{newstateid}")
	 @PreAuthorize("hasRole('ADMIN')")
	 public ResponseEntity<?> addState(@Valid @PathVariable ("clientid") int clientid,@Valid @PathVariable ("newstateid") long finalid )
	 {		ClientProfile temp = clientprofilerepository.findByclientid(clientid).orElseThrow(() -> new ResourceNotFoundException("Client", "Client Id", clientid));
	 		State newstate = staterepository.findById(finalid).orElseThrow(() -> new ResourceNotFoundException("State", "State Id", finalid));;
	 		temp.getState().add(newstate);
	 		clientprofilerepository.save(temp);
		 return new ResponseEntity(new ApiResponse(true, "State updated successfully"),HttpStatus.CREATED);
	 }
	 
	 @PostMapping("/{clientid}/update/city/{initialcityid}/{newcityid}")
	 @PreAuthorize("hasRole('ADMIN')")
	 public ResponseEntity<?> updateCity(@Valid @PathVariable ("clientid") int clientid, @Valid @PathVariable ("initialcityid") long initid,@Valid @PathVariable ("newcityid") long finalid )
	 {		ClientProfile temp = clientprofilerepository.findByclientid(clientid).orElseThrow(() -> new ResourceNotFoundException("Client", "Client Id", clientid));
	 		City tempcity = cityrepository.findById(initid).orElseThrow(() -> new ResourceNotFoundException("City", "City Id", initid));
	 		temp.getCities().remove(tempcity);
	 		City newcity = cityrepository.findById(finalid).orElseThrow(() -> new ResourceNotFoundException("City", "City Id", finalid));;
	 		temp.getCities().add(newcity);
	 		clientprofilerepository.save(temp);
		 return new ResponseEntity(new ApiResponse(true, "City updated successfully"),HttpStatus.CREATED);
	 }
	 

	 @PostMapping("/{clientid}/update/city/{initialcityid}")
	 @PreAuthorize("hasRole('ADMIN')")
	 public ResponseEntity<?> deleteCity(@Valid @PathVariable ("clientid") int clientid, @Valid @PathVariable ("initialcityid") long initid )
	 {	ClientProfile temp = clientprofilerepository.findByclientid(clientid).orElseThrow(() -> new ResourceNotFoundException("Client", "Client Id", clientid));
	 	City tempcity = cityrepository.findById(initid).orElseThrow(() -> new ResourceNotFoundException("City", "City Id", initid));
		temp.getCities().remove(tempcity);
		clientprofilerepository.save(temp);
		 return new ResponseEntity(new ApiResponse(true, "City updated successfully"),HttpStatus.CREATED);
	 
	 }
	 

	 @PostMapping("/{clientid}/update/addstate/{newcityid}")
	 @PreAuthorize("hasRole('ADMIN')")
	 public ResponseEntity<?> addCity(@Valid @PathVariable ("clientid") int clientid,@Valid @PathVariable ("newcityid") long finalid )
	 {		ClientProfile temp = clientprofilerepository.findByclientid(clientid).orElseThrow(() -> new ResourceNotFoundException("Client", "Client Id", clientid));
	 		City newcity = cityrepository.findById(finalid).orElseThrow(() -> new ResourceNotFoundException("City", "City Id", finalid));
	 		temp.getCities().add(newcity);
	 		clientprofilerepository.save(temp);
		 return new ResponseEntity(new ApiResponse(true, "City updated successfully"),HttpStatus.CREATED);
	 }
	 
	 @PostMapping("/{clientid}/update/producttype/{initialproducttypeid}/{newproducttypeid}")
	 @PreAuthorize("hasRole('ADMIN')")
	 public ResponseEntity<?> updateProductType(@Valid @PathVariable ("clientid") int clientid, @Valid @PathVariable ("initialproducttypeid") long initid,@Valid @PathVariable ("newproducttypeid") long finalid )
	 {		ClientProfile temp = clientprofilerepository.findByclientid(clientid).orElseThrow(() -> new ResourceNotFoundException("Client", "Client Id", clientid));
	 		ProductType tempproduct = producttyperepository.findById(initid).orElseThrow(() -> new ResourceNotFoundException("Product Type", "Product Type Id", initid));
	 		temp.getProducttype().remove(tempproduct);
	 		ProductType newproducttype = producttyperepository.findById(finalid).orElseThrow(() -> new ResourceNotFoundException("Product Type", "Product Type Id", finalid));
	 		temp.getProducttype().add(newproducttype);
	 		clientprofilerepository.save(temp);
		 return new ResponseEntity(new ApiResponse(true, "Product Type updated successfully"),HttpStatus.CREATED);
	 }
	 

	 @PostMapping("/{clientid}/update/producttype/{initialproducttypeid}")
	 @PreAuthorize("hasRole('ADMIN')")
	 public ResponseEntity<?> deleteProductType(@Valid @PathVariable ("clientid") int clientid, @Valid @PathVariable ("initialproducttypeid") long initid )
	 {	ClientProfile temp = clientprofilerepository.findByclientid(clientid).orElseThrow(() -> new ResourceNotFoundException("Client", "Client Id", clientid));
	 	ProductType tempproduct = producttyperepository.findById(initid).orElseThrow(() -> new ResourceNotFoundException("Product Type", "Product Type Id", initid));
		temp.getProducttype().remove(tempproduct);
		clientprofilerepository.save(temp);
		 return new ResponseEntity(new ApiResponse(true, "Product Type updated successfully"),HttpStatus.CREATED);
	 
	 }
	 
	 @PostMapping("/{clientid}/update/addproducttype/{newproducttypeid}")
	 @PreAuthorize("hasRole('ADMIN')")
	 public ResponseEntity<?> addProductType(@Valid @PathVariable ("clientid") int clientid,@Valid @PathVariable ("newproducttypeid") long finalid )
	 {		ClientProfile temp = clientprofilerepository.findByclientid(clientid).orElseThrow(() -> new ResourceNotFoundException("Client", "Client Id", clientid));
	 		ProductType newproducttype = producttyperepository.findById(finalid).orElseThrow(() -> new ResourceNotFoundException("Product Type", "Product Type Id", finalid));
	 		temp.getProducttype().add(newproducttype);
	 		clientprofilerepository.save(temp);
		 return new ResponseEntity(new ApiResponse(true, "Product Type updated successfully"),HttpStatus.CREATED);
	 } 
	 
	 @PostMapping("/{clientid}/update/impactindustry/{initialimpactid}/{newimpactid}")
	 @PreAuthorize("hasRole('ADMIN')")
	 public ResponseEntity<?> updateImpactIndustry(@Valid @PathVariable ("clientid") int clientid, @Valid @PathVariable ("initialimpactid") long initid,@Valid @PathVariable ("newimpactid") long finalid )
	 {		ClientProfile temp = clientprofilerepository.findByclientid(clientid).orElseThrow(() -> new ResourceNotFoundException("Client", "Client Id", clientid));
	 		ImpactIndustry tempimpact = impactindustryrepository.findById(initid).orElseThrow(() -> new ResourceNotFoundException("Impact Industry", "Impact Industry Id", clientid));
	 		temp.getImpactindustry().remove(tempimpact);
	 		ImpactIndustry newimpact = impactindustryrepository.findById(finalid).orElseThrow(() -> new ResourceNotFoundException("Impact Industry", "Impact Industry Id", finalid));
	 		temp.getImpactindustry().add(newimpact);
	 		clientprofilerepository.save(temp);
		 return new ResponseEntity(new ApiResponse(true, "Impact Industry updated successfully"),HttpStatus.CREATED);
	 }
	 

	 @PostMapping("/{clientid}/update/impactindustry/{initialimpactid}")
	 @PreAuthorize("hasRole('ADMIN')")
	 public ResponseEntity<?> deleteImpactIndustry(@Valid @PathVariable ("clientid") int clientid, @Valid @PathVariable ("initialimpactid") long initid )
	 {	ClientProfile temp = clientprofilerepository.findByclientid(clientid).orElseThrow(() -> new ResourceNotFoundException("Client", "Client Id", clientid));
	 ImpactIndustry tempimpact = impactindustryrepository.findById(initid).orElseThrow(() -> new ResourceNotFoundException("Impact Industry", "Impact Industry Id", clientid));
		temp.getImpactindustry().remove(tempimpact);
		clientprofilerepository.save(temp);
		 return new ResponseEntity(new ApiResponse(true, "Impact Industry updated successfully"),HttpStatus.CREATED);
	 
	 }
	 
	 @PostMapping("/{clientid}/update/addimpactindustry/{newimpactid}")
	 @PreAuthorize("hasRole('ADMIN')")
	 public ResponseEntity<?> addImpactIndustry(@Valid @PathVariable ("clientid") int clientid,@Valid @PathVariable ("newimpactid") long finalid )
	 {		ClientProfile temp = clientprofilerepository.findByclientid(clientid).orElseThrow(() -> new ResourceNotFoundException("Client", "Client Id", clientid));
	 		ImpactIndustry newimpact = impactindustryrepository.findById(finalid).orElseThrow(() -> new ResourceNotFoundException("Impact Industry", "Impact Industry Id", finalid));
	 		temp.getImpactindustry().add(newimpact);
	 		clientprofilerepository.save(temp);
		 return new ResponseEntity(new ApiResponse(true, "Impact Industry updated successfully"),HttpStatus.CREATED);
	 } 
	 

	 
	 
	 
	 }
