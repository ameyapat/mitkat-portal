package mitkat.portal.admin.controller.admin;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mitkat.portal.admin.customeException.AppException;
import mitkat.portal.admin.customeException.ResourceNotFoundException;
import mitkat.portal.admin.model.user.Role;
import mitkat.portal.admin.model.user.RoleName;
import mitkat.portal.admin.model.user.User;
import mitkat.portal.admin.repository.user.RoleRepository;
import mitkat.portal.admin.repository.user.UserRepository;
import mitkat.portal.admin.requestObjects.authentication.PasswordRequest;
import mitkat.portal.admin.requestObjects.authentication.SignUpRequest;
import mitkat.portal.admin.requestObjects.authentication.SignUpRequestInternalUser;
import mitkat.portal.admin.responseObjects.authentication.ApiResponse;
import mitkat.portal.admin.responseObjects.authentication.ApiResponseClient;
import mitkat.portal.admin.responseObjects.authentication.ClientApiResponse;
import mitkat.portal.admin.security.JwtTokenProvider;
import mitkat.portal.admin.security.UserPrincipal;

@RestController
@RequestMapping("/api/admin")
public class UserManagementController {

	@Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtTokenProvider tokenProvider;
    
    private static final Logger logger = LoggerFactory.getLogger(UserManagementController.class);
	
    
    @PostMapping("/signup/client")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> registerClient(@Valid @RequestBody SignUpRequest signUpRequest) throws Exception {
        if(userRepository.existsByUsername(signUpRequest.getUsername())) {
            return new ResponseEntity(new ApiResponse(false, "Username is already taken!"),
                    HttpStatus.BAD_REQUEST);
        }

        if(userRepository.existsByEmail(signUpRequest.getEmail())) {
            return new ResponseEntity(new ApiResponse(false, "Email Address already in use!"),
                    HttpStatus.BAD_REQUEST);
        }

        // Creating user's account
        User user = new User(signUpRequest.getName(), signUpRequest.getUsername(),
                signUpRequest.getEmail(), signUpRequest.getPassword());

        user.setPassword(passwordEncoder.encode(user.getPassword()));

        Role userRole = roleRepository.findByName(RoleName.ROLE_CLIENT)
                .orElseThrow(() -> new AppException("User Role not set."));

        user.setRoles(Collections.singleton(userRole));

        User result = userRepository.save(user);

       /** URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/api/users/{username}")
                .buildAndExpand(result.getUsername()).toUri();

        return ResponseEntity.created(location).body(new ApiResponse(true, "User registered successfully"));
    	**/
/*
        return new ResponseEntity(new ApiResponseClient(true, "User registered successfully",result.getId()),HttpStatus.CREATED);
**/
        return new ResponseEntity(new ClientApiResponse(true, "User registered successfully",result.getId()),HttpStatus.CREATED);
    
    }
    
    @PostMapping("/signup/user")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequestInternalUser signUpRequest) throws Exception {
        if(userRepository.existsByUsername(signUpRequest.getUsername())) {
            return new ResponseEntity(new ApiResponse(false, "Username is already taken!"),
                    HttpStatus.BAD_REQUEST);
        }

        if(userRepository.existsByEmail(signUpRequest.getEmail())) {
            return new ResponseEntity(new ApiResponse(false, "Email Address already in use!"),
                    HttpStatus.BAD_REQUEST);
        }

        if(signUpRequest.getClearance() == 1 ) {
        // Creating user's account
        User user = new User(signUpRequest.getName(), signUpRequest.getUsername(),
                signUpRequest.getEmail(), signUpRequest.getPassword());

        user.setPassword(passwordEncoder.encode(user.getPassword()));

        Role userRole = roleRepository.findByName(RoleName.ROLE_CREATOR)
                .orElseThrow(() -> new AppException("User Role not set."));

        user.setRoles(Collections.singleton(userRole));

        User result = userRepository.save(user);
        }
        else {
        	 User user = new User(signUpRequest.getName(), signUpRequest.getUsername(),
 	                signUpRequest.getEmail(), signUpRequest.getPassword());

 	        user.setPassword(passwordEncoder.encode(user.getPassword()));

 	        Role userRole = roleRepository.findByName(RoleName.ROLE_APPROVER)
 	                .orElseThrow(() -> new AppException("User Role not set."));

 	        user.setRoles(Collections.singleton(userRole));

 	        User result = userRepository.save(user);
        	
        }
        
       
        
       /** URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/api/users/{username}")
                .buildAndExpand(result.getUsername()).toUri();

        return ResponseEntity.created(location).body(new ApiResponse(true, "User registered successfully"));
    	**/
        return new ResponseEntity(new ApiResponse(true, "User registered successfully"),HttpStatus.CREATED);
    
    }
    
    @PostMapping("/allusers")     
    @PreAuthorize("hasRole('ADMIN')")
    public List<User> getAllUsers() throws Exception
    {	
    	List<User> allUsers =  userRepository.findAll();
    	List<User> outUsers = new ArrayList<User>();
    	for(User tempUser:allUsers)
    	{
    		if(tempUser.getRoles().iterator().next().getId()==3 || tempUser.getRoles().iterator().next().getId()==4)
    		{
    			outUsers.add(tempUser);
    		}
    		
    	}
    	
    	return outUsers;
    	
    }
    
    @PostMapping("/allclients")     
    @PreAuthorize("hasRole('ADMIN')")
    public List<User> getAllClients()throws Exception
    {	
    	List<User> allUsers =  userRepository.findAll();
    	List<User> outUsers = new ArrayList<User>();
    	for(User tempUser:allUsers)
    	{
    		if(tempUser.getRoles().iterator().next().getId()==1)
    		{
    			outUsers.add(tempUser);
    		}
    		
    	}
    	
    	return outUsers;
    	
    }
    
    
    @PostMapping("/updateUser/{userid}/password")     
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> updateUserPassword(@PathVariable("userid") long userid, @Valid@RequestBody PasswordRequest updaterequest )
    {
    	
    	User user = userRepository.findById(userid).orElseThrow(() -> new ResourceNotFoundException("User", "User Id", userid));;
    	user.setPassword(passwordEncoder.encode(updaterequest.getPassword()));
    	User result = userRepository.save(user);
    	return new ResponseEntity(new ApiResponse(true, "Password updated successfully"),HttpStatus.CREATED);
    }
    
    @PostMapping("/updateUser/{userid}/username/{usenamenew}")     
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> updateUserName(@PathVariable("userid") long userid, @PathVariable("usenamenew") String usernamenew )
    {
    	User user = userRepository.findById(userid).orElseThrow(() -> new ResourceNotFoundException("User", "User Id", userid));
    	 if(userRepository.existsByUsername(usernamenew)) {
             return new ResponseEntity(new ApiResponse(false, "Username is already taken!"),
                     HttpStatus.BAD_REQUEST);
         }
    	user.setUsername(usernamenew);
    	User result = userRepository.save(user);
    	return new ResponseEntity(new ApiResponse(true, "Username updated successfully"),HttpStatus.CREATED);
    }
    
    @PostMapping("/updateUser/{userid}/name/{namenew}")     
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> updateName(@PathVariable("userid") long userid, @PathVariable("namenew") String namenew )
    {
    	User user = userRepository.findById(userid).orElseThrow(() -> new ResourceNotFoundException("User", "User Id", userid));
    	user.setName(namenew);
    	User result = userRepository.save(user);
    	return new ResponseEntity(new ApiResponse(true, "Name updated successfully"),HttpStatus.CREATED);
    }
    
    @PostMapping("/updateUser/{userid}/email/{emailnew}")     
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> updateEmail(@PathVariable("userid") long userid, @PathVariable("emailnew") String emailnew )
    {
    	User user = userRepository.findById(userid).orElseThrow(() -> new ResourceNotFoundException("User", "User Id", userid));
    	 if(userRepository.existsByEmail(emailnew)) {
             return new ResponseEntity(new ApiResponse(false, "Email Address already in use!"),
                     HttpStatus.BAD_REQUEST);
         }
    	user.setEmail(emailnew);
    	User result = userRepository.save(user);
    	return new ResponseEntity(new ApiResponse(true, "Email updated successfully"),HttpStatus.CREATED);
    }
    
    @PostMapping("/deleteUser/{userid}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> deleteUser(@PathVariable("userid") long userid)
    {
    	User user = userRepository.findById(userid).orElseThrow(() -> new ResourceNotFoundException("User", "User Id", userid));
    	userRepository.deleteById(userid);
    	return new ResponseEntity(new ApiResponse(true, "User deleted successfully"),HttpStatus.CREATED);
    }
    
    
}
