package mitkat.portal.admin.controller.admin;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mitkat.portal.admin.model.user.Mitkat;
import mitkat.portal.admin.repository.application.MitKatRepository;
import mitkat.portal.admin.requestObjects.clientprofile.Mitkatrequest;

@RestController
@RequestMapping("/api/mitkat")
public class MitKatDetailsManagementController {

	@Autowired
	MitKatRepository mitkatrepository;
	
	@PostMapping("/add")
	public void addDetails(Mitkatrequest request) throws Exception
	{
		Mitkat mitkat = new Mitkat();
		mitkat.setCompanyName(request.getCompanyName());
		mitkat.setAddress(request.getAddress());
		mitkat.setContactnumber(request.getContactnumber());
		mitkat.setContactperson(request.getContactperson());
		mitkat.setEmail(request.getEmail());
		try {
			mitkat.setLogo(request.getMultipartfile().getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		mitkatrepository.save(mitkat);
	}
	
	@PostMapping("/update")
	public void updateDetails(Mitkatrequest request)throws Exception
	{
		Mitkat mitkat = mitkatrepository.getOne((long) 1);
		mitkat.setCompanyName(request.getCompanyName());
		mitkat.setAddress(request.getAddress());
		mitkat.setContactnumber(request.getContactnumber());
		mitkat.setContactperson(request.getContactperson());
		mitkat.setEmail(request.getEmail());
		try {
			mitkat.setLogo(request.getMultipartfile().getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		mitkatrepository.save(mitkat);
	}
	
	
}
