package mitkat.portal.admin.controller.approver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonFormat;

import freemarker.core.ParseException;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.TemplateException;
import freemarker.template.TemplateNotFoundException;
import mitkat.portal.admin.customeException.ResourceNotFoundException;
import mitkat.portal.admin.model.creator.Report;
import mitkat.portal.admin.model.creator.RiskAlert;
import mitkat.portal.admin.repository.application.ReportRepository;
import mitkat.portal.admin.repository.application.RiskAlertRepository;
import mitkat.portal.admin.requestObjects.approver.EmailAlertRequest;
import mitkat.portal.admin.requestObjects.approver.EmailRequest;
import mitkat.portal.admin.requestObjects.approver.SMSRequest;
import mitkat.portal.admin.requestObjects.creator.EventRequestObject;
import mitkat.portal.admin.requestObjects.creator.RiskAlertRequestObject;
import mitkat.portal.admin.responseObjects.approver.ClientListResponse;
import mitkat.portal.admin.responseObjects.approver.EmailResponseObject;
import mitkat.portal.admin.responseObjects.approver.EventDetailResponseApprover;
import mitkat.portal.admin.responseObjects.approver.EventsForProductResponse;
import mitkat.portal.admin.responseObjects.approver.TodaysReportsResponse;
import mitkat.portal.admin.responseObjects.approver.TodaysSMSResponse;
import mitkat.portal.admin.responseObjects.authentication.ApiResponse;
import mitkat.portal.admin.responseObjects.creator.SMSAlertResponseApprover;
import mitkat.portal.admin.security.CurrentUser;
import mitkat.portal.admin.security.UserPrincipal;
import mitkat.portal.admin.service.ApproverService;
import mitkat.portal.admin.service.EmailService;
import mitkat.portal.admin.service.ReportService;
import mitkat.portal.admin.service.RiskAlertService;
import mitkat.portal.admin.service.SMSService;


@RestController
@RequestMapping("/api/approver")
public class ApproverController {

	@Autowired
	ApproverService approverservice;
	
	@Autowired
	ReportService reportservice;
	
	@Autowired
	EmailService emailservice;

	@Autowired
	ReportRepository reportrepository;
	
	@Autowired
	RiskAlertRepository riskalertrepository;
	
	@Autowired
	SMSService smsservice;
	
	@Autowired
	RiskAlertService riskalertservice;
	
	@PostMapping("/gettodaysevents")
	@PreAuthorize("hasRole('APPROVER')")
	public List<TodaysReportsResponse> getResponse()throws Exception
	{
		return approverservice.getTodaysResports();
	}
	
	@PostMapping("/gettodaysemailalerts")
	@PreAuthorize("hasRole('APPROVER')")
	public List<TodaysReportsResponse> getEmailAlerts()throws Exception
	{
		return approverservice.getEmailAlerts();
	}
	

	
	@PostMapping("/getevent/{eventid}")
	@PreAuthorize("hasRole('APPROVER')")
	public EventDetailResponseApprover getEvent(@PathVariable("eventid")long eventid)
	{
		Report report = reportrepository.findById(eventid).orElseThrow(
                () -> new ResourceNotFoundException("Report", "id", eventid));
		
		EventDetailResponseApprover response = reportservice.converToEventResponseApprover(report);
		
		return response;
	}
	
	
	@PostMapping("/editevent")
	@PreAuthorize("hasRole('APPROVER')")
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
	public ResponseEntity<?> editEvent(@Valid @RequestBody EventRequestObject request, UserPrincipal userprincipal)
	{
		
		reportservice.updateReport(request,userprincipal);
		ResponseEntity<?> responseEntity = new ResponseEntity<Object>(new ApiResponse(true, "Report Updated successfully"),HttpStatus.CREATED);
		return responseEntity;
	}

	@PostMapping("/deletevent/{eventid}")
	@PreAuthorize("hasRole('APPROVER')")
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
	public ResponseEntity<?> deleteEvent(@Valid @PathVariable("eventid") long eventid, UserPrincipal userprincipal)
	{
		
		reportservice.deletereport(eventid);
		ResponseEntity<?> responseEntity = new ResponseEntity<Object>(new ApiResponse(true, "Report deleted successfully"),HttpStatus.CREATED);
		return responseEntity;
	}
	
    @PostMapping("/geteventsforproduct/{clientid}")
    @PreAuthorize("hasRole('APPROVER')")
	public List<EventsForProductResponse> getEventsForProduct(@Valid@PathVariable ("clientid") long clientid)throws Exception
	{
    	return approverservice.getEventsForResponse(clientid);}

    @PostMapping("/geteventsforemailalerts")
    @PreAuthorize("hasRole('APPROVER')")
	public List<EventsForProductResponse> getEventsForEmailAlerts()throws Exception
	{
    	return approverservice.getEventsForEmailAlerts();}
    
    @PostMapping("/sendemail")
    @PreAuthorize("hasRole('APPROVER')")
    public EmailResponseObject sendEmail(@Valid@RequestBody EmailRequest emailrequest) throws TemplateNotFoundException, MalformedTemplateNameException, ParseException, IOException, MessagingException, TemplateException
    {  
    	EmailResponseObject response= emailservice.sendEmail(emailrequest);
    	return response;
    }
    
    
    @PostMapping("/sendemailalerts")
    @PreAuthorize("hasRole('APPROVER')")
    public EmailResponseObject sendEmailAlerts(@Valid@RequestBody EmailAlertRequest emailrequest) throws TemplateNotFoundException, MalformedTemplateNameException, ParseException, IOException, MessagingException, TemplateException
    {  
    	EmailResponseObject response= emailservice.sendEmailAlert(emailrequest);
    	return response;
    }
    

    
	@PostMapping("/getsmsalertsforapproval")
	public List<TodaysSMSResponse> getSMSAlertsForApproval() throws Exception
	{
		List<TodaysSMSResponse> response = approverservice.getSMSAlertsForApproval();
		
		return response;
	}
	
	@PostMapping("/getsmsalert/{alertid}")
	@PreAuthorize("hasRole('APPROVER')")
	public SMSAlertResponseApprover getSMSAlert(@PathVariable("alertid")long alertid)
	{
		
		RiskAlert riskalert = riskalertrepository.findById(alertid).orElseThrow(
                () -> new ResourceNotFoundException("Report", "id", alertid));
		
		
		SMSAlertResponseApprover response = riskalertservice.converToAlertResponseApprover(riskalert);
		
		return response;
	}
	
	
	@PostMapping("/editsmsalert")
	@PreAuthorize("hasRole('APPROVER')")
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
	public ResponseEntity<?> editSMSAlert(@Valid@RequestBody RiskAlertRequestObject request, @CurrentUser UserPrincipal userprinciple )throws Exception
	{
		riskalertservice.updateRiskAlert(request,userprinciple);
		
		ResponseEntity<?> responseEntity = new ResponseEntity<Object>(new ApiResponse(true, "SMS Alert Updated successfully"),HttpStatus.CREATED);
		return responseEntity;
	}
	
	@PostMapping("/smsalertsdispatchlist")
	@PreAuthorize("hasRole('APPROVER')")
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
	public List<TodaysSMSResponse> alertDispatchList()throws Exception
	{
		return riskalertservice.getTodaysDispatchList();
	}
	

	@PostMapping("/sendsms")
	public ResponseEntity<?> sendSMS(@Valid@RequestBody SMSRequest request)
	{
		try {
		smsservice.sendSMS(request);
		}catch(Exception e) {e.printStackTrace();}
		
		ResponseEntity<?> responseEntity = new ResponseEntity<Object>(new ApiResponse(true, "SMS Alert Sent successfully"),HttpStatus.CREATED);
		
		return responseEntity;
	}
}



