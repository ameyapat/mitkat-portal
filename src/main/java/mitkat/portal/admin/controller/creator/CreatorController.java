package mitkat.portal.admin.controller.creator;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonFormat;

import mitkat.portal.admin.customeException.ResourceNotFoundException;
import mitkat.portal.admin.model.creator.Report;
import mitkat.portal.admin.model.creator.Update;
import mitkat.portal.admin.repository.application.CityRepository;
import mitkat.portal.admin.repository.application.Countryrepository;
import mitkat.portal.admin.repository.application.ImpactIndustryRepository;
import mitkat.portal.admin.repository.application.ProductTypeRepository;
import mitkat.portal.admin.repository.application.RegionTypeRepository;
import mitkat.portal.admin.repository.application.ReportRepository;
import mitkat.portal.admin.repository.application.RiskCategoryRepository;
import mitkat.portal.admin.repository.application.RiskLevelRepository;
import mitkat.portal.admin.repository.application.StateRepository;
import mitkat.portal.admin.repository.application.StatusRepository;
import mitkat.portal.admin.requestObjects.creator.EventRequestObject;
import mitkat.portal.admin.requestObjects.creator.RiskAlertRequestObject;
import mitkat.portal.admin.requestObjects.creator.UpdateRequestObject;
import mitkat.portal.admin.requestObjects.event.FilterEventRequest;
import mitkat.portal.admin.responseObjects.authentication.ApiResponse;
import mitkat.portal.admin.responseObjects.creator.EventDetailsResponseCreator;
import mitkat.portal.admin.responseObjects.eventresponse.EventResponse;
import mitkat.portal.admin.responseObjects.eventresponse.PagedResponse;
import mitkat.portal.admin.security.CurrentUser;
import mitkat.portal.admin.security.UserPrincipal;
import mitkat.portal.admin.service.EventService;
import mitkat.portal.admin.service.ReportService;
import mitkat.portal.admin.service.RiskAlertService;
import mitkat.portal.admin.service.SMSService;

@RestController
@RequestMapping("/api/creator")
public class CreatorController {
	
	@Autowired
	Countryrepository countryrepository;
	
	@Autowired
	StateRepository staterepository;
	
	@Autowired
	CityRepository cityrepository;
	
	@Autowired
	ProductTypeRepository producttyperepository;
	
	@Autowired
	ImpactIndustryRepository impactindustryrepository;
	
	@Autowired
	RegionTypeRepository regiontyperepository;
	
	@Autowired
	ReportRepository reportrepository;
	
	@Autowired
	RiskCategoryRepository riskcategoryrepository;
	
	@Autowired
	RiskLevelRepository risklevelrepository;
	
	@Autowired
	StatusRepository statusrepository;
	
	@Autowired
	ReportService reportservice;
	
	@Autowired
	EventService eventservice;
	
	@Autowired
	RiskAlertService riskalertservice;
	
	@Autowired
	SMSService smsservice;
	
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(CreatorController.class);
	
	@PostMapping("/addevent")
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
	public ResponseEntity<?> addEvent(@Valid@RequestBody EventRequestObject eventrequest,@CurrentUser UserPrincipal currentUser)
	{
		
			reportservice.addNewReport(eventrequest,currentUser);
		
		
		return new ResponseEntity<Object>(new ApiResponse(true, "Event added successfully"),HttpStatus.CREATED);
	}
	
	@PostMapping("/editevent")
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
	public ResponseEntity<?> updateEvent(@Valid@RequestBody EventRequestObject eventrequest,@CurrentUser UserPrincipal currentUser)throws Exception
	{
		
			reportservice.editReport(currentUser,eventrequest);
		
		
		return new ResponseEntity<Object>(new ApiResponse(true, "Event updated successfully"),HttpStatus.CREATED);
	}
	
	
	@PostMapping("/getevent/{eventid}")
	public EventDetailsResponseCreator getEvent(@PathVariable("eventid")long eventid)
	{
		Report report = reportrepository.findById(eventid).orElseThrow(
                () -> new ResourceNotFoundException("Report", "id", eventid));
		EventDetailsResponseCreator response = reportservice.converToEventResponse(report);
		return response;
	}
	
	@PostMapping("/geteventlist/{pagenumber}")
	public PagedResponse<EventResponse> getAllEvents(@CurrentUser UserPrincipal userprinciple,@PathVariable("pagenumber") int pagenumber)
	{   
		@SuppressWarnings("unused")
		int x = eventservice.getNumberOfPages(userprinciple, pagenumber, 10);
		
		return eventservice.getAllEvents(userprinciple, pagenumber, 10);
		
		
	}
	
	@PostMapping("/geteventlist/filter/{pagenumber}")
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
	public PagedResponse<EventResponse> getFilteredEvents (@CurrentUser UserPrincipal userprinciple,@PathVariable("pagenumber") int pagenumber,@RequestBody FilterEventRequest filterrequest)
	{	@SuppressWarnings("unused")
	    int x = eventservice.getNumberOfPagesOFFilter(userprinciple, pagenumber, 10,filterrequest);
	
		return eventservice.getFilteredEvents(userprinciple, pagenumber, 10,filterrequest);
	}
	
	
	
	
	@PostMapping("/liveevent")
	public List<EventResponse> getLiveEvents(@CurrentUser UserPrincipal userprinciple)
	{
		List<EventResponse> response = eventservice.getLiveEvents();
		return response;
	}
	
	@PostMapping("/getliveeventupdates/{eventid}")
	public List<Update> getLiveEventUpdates(@CurrentUser UserPrincipal userprinciple,@PathVariable("eventid") long eventid)
	{  
		Report report = reportrepository.findById(eventid).orElseThrow(
                () -> new ResourceNotFoundException("Report", "id", eventid));;
		List<Update> response =  report.getUpdates();
		return response;
		
	}
	
	@PostMapping("/addupdate")
	public ResponseEntity<?> addUpdate(@Valid@RequestBody UpdateRequestObject request)
	{
		reportservice.addUpdate(request);
		
		return new ResponseEntity<Object>(new ApiResponse(true, "Update added successfully"),HttpStatus.CREATED);
	}
	
	@PostMapping("/editupdate")
	public ResponseEntity<?> editupdate(@Valid@RequestBody UpdateRequestObject request)
	{

		reportservice.editUpdate(request);
		return new ResponseEntity<Object>(new ApiResponse(true, "Update edited successfully"),HttpStatus.CREATED);
		
	}
		
	@PostMapping("/deleteupdate/{updateid}")
	public ResponseEntity<?> deleteupdate(@CurrentUser UserPrincipal userprinciple,@PathVariable("updateid") long updateid)
	{
		reportservice.deleteUpdate(userprinciple,updateid);
		
		return new ResponseEntity<Object>(new ApiResponse(true, "Update deleted successfully"),HttpStatus.CREATED);
		
	}	
		
	@PostMapping("/addemailalert")
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
	public ResponseEntity<?> addEmailAlert(@Valid@RequestBody EventRequestObject eventrequest,@CurrentUser UserPrincipal currentUser)throws Exception
	{
		
			reportservice.addNewEmailAlert(eventrequest,currentUser);
		
		
		return new ResponseEntity<Object>(new ApiResponse(true, "Email Alert added successfully"),HttpStatus.CREATED);
	}	
		
	

	@PostMapping("/addriskalert")
	public ResponseEntity<?> addRiskAlert (@Valid@RequestBody RiskAlertRequestObject request, @CurrentUser UserPrincipal userprinciple )throws Exception
	{
		riskalertservice.addRiskAlert(request,userprinciple);
		
		return new ResponseEntity<Object>(new ApiResponse(true, "Risk Alert added successfully"),HttpStatus.CREATED);}
	
		
	}
	
	