package mitkat.portal.admin.controller.authentication;


import java.util.Collections;

import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mitkat.portal.admin.customeException.AppException;
import mitkat.portal.admin.model.user.Role;
import mitkat.portal.admin.model.user.RoleName;
import mitkat.portal.admin.model.user.User;
import mitkat.portal.admin.repository.user.RoleRepository;
import mitkat.portal.admin.repository.user.UserRepository;
import mitkat.portal.admin.requestObjects.authentication.LoginRequest;
import mitkat.portal.admin.requestObjects.authentication.SignUpRequest;
import mitkat.portal.admin.responseObjects.authentication.ApiResponse;
import mitkat.portal.admin.responseObjects.authentication.JwtAuthenticationResponse;
import mitkat.portal.admin.security.CurrentUser;
import mitkat.portal.admin.security.JwtTokenProvider;
import mitkat.portal.admin.security.UserPrincipal;



@RestController
@RequestMapping("/api/auth")
public class AuthController {

	 	@Autowired
	    AuthenticationManager authenticationManager;

	    @Autowired
	    UserRepository userRepository;

	    @Autowired
	    RoleRepository roleRepository;

	    @Autowired
	    PasswordEncoder passwordEncoder;

	    @Autowired
	    JwtTokenProvider tokenProvider;
	
	    @Autowired
	    UserRepository userrepository;
	    
	    @PostMapping("/signin")
	    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

	        Authentication authentication = authenticationManager.authenticate(
	                new UsernamePasswordAuthenticationToken(
	                        loginRequest.getUsernameOrEmail(),
	                        loginRequest.getPassword()
	                )
	        );

	        SecurityContextHolder.getContext().setAuthentication(authentication);

	        String jwt = tokenProvider.generateToken(authentication);
	        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
	    }
	    
	    @PostMapping("/getuserroles")
	    public Set<Role> getUserDetails(@CurrentUser UserPrincipal currentUser)
	    {
	    	User user = userrepository.getOne(currentUser.getId());
	    	Set<Role> roles = user.getRoles();
	    	return roles;
	    	}
	
	    @PostMapping("/signup/admin")
	    public ResponseEntity<?> registerAdmin(@Valid @RequestBody SignUpRequest signUpRequest) {
	        if(userRepository.existsByUsername(signUpRequest.getUsername())) {
	            ResponseEntity<?> responseEntity = new ResponseEntity<Object>(new ApiResponse(false, "Username is already taken!"),
	                    HttpStatus.BAD_REQUEST);
				return responseEntity;
	        }

	        if(userRepository.existsByEmail(signUpRequest.getEmail())) {
	            return new ResponseEntity<Object>(new ApiResponse(false, "Email Address already in use!"),
	                    HttpStatus.BAD_REQUEST);
	        }

	        // Creating user's account
	        User user = new User(signUpRequest.getName(), signUpRequest.getUsername(),
	                signUpRequest.getEmail(), signUpRequest.getPassword());

	        user.setPassword(passwordEncoder.encode(user.getPassword()));

	        Role userRole = roleRepository.findByName(RoleName.ROLE_ADMIN)
	                .orElseThrow(() -> new AppException("User Role not set."));

	        user.setRoles(Collections.singleton(userRole));

	        @SuppressWarnings("unused")
			User result = userRepository.save(user);

	        
	       // URI location = ServletUriComponentsBuilder
	       //         .fromCurrentContextPath().path("/api/users/{username}")
	       //         .buildAndExpand(result.getUsername()).toUri();

	      //  return ResponseEntity.created(location).body(new ApiResponse(true, "User registered successfully"));
	        ResponseEntity<?> responseEntity = new ResponseEntity<Object>(new ApiResponse(true, "User registered successfully"),HttpStatus.CREATED);
			return responseEntity;
	    }
	   
} 