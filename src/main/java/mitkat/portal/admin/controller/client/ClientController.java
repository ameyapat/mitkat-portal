package mitkat.portal.admin.controller.client;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.itextpdf.text.DocumentException;

import mitkat.portal.admin.requestObjects.client.ArchiveEventsRequestObject;
import mitkat.portal.admin.requestObjects.client.TerminalRequestObject;
import mitkat.portal.admin.responseObjects.client.ArchiveEventsResponseObject;
import mitkat.portal.admin.responseObjects.client.EventDetailsResponse;
import mitkat.portal.admin.responseObjects.client.NationalEventResponse;
import mitkat.portal.admin.responseObjects.client.RiskAlertDetailsResponse;
import mitkat.portal.admin.responseObjects.client.RiskAlertResponse;
import mitkat.portal.admin.responseObjects.client.TerminalResponseObjet;
import mitkat.portal.admin.responseObjects.client.TopBarResponseObject;
import mitkat.portal.admin.responseObjects.eventresponse.PagedResponse;
import mitkat.portal.admin.security.CurrentUser;
import mitkat.portal.admin.security.UserPrincipal;
import mitkat.portal.admin.service.PdfGenerater;
import mitkat.portal.admin.service.RiskAlertService;
import mitkat.portal.admin.service.TerminalService;

@RestController
@RequestMapping("/api/client")
public class ClientController {

	
	@Autowired
	TerminalService terminalservice;
	
	@Autowired
	RiskAlertService riskalertservice;
	
	@Autowired
	PdfGenerater pdfgenerater;

	@PostMapping("/getterminalresponse")
	@PreAuthorize("hasRole('CLIENT')")
	public List<TerminalResponseObjet> getTerminalResponse (@Valid @RequestBody TerminalRequestObject request, @CurrentUser UserPrincipal userprincipal)throws Exception
	{
		
		List<TerminalResponseObjet> response = terminalservice.getTerminalEvents(request,userprincipal);
		
		return response;
	}
	
	@PostMapping("/getevent/{eventid}")
	@PreAuthorize("hasRole('CLIENT')")
	public EventDetailsResponse getEvent(@Valid@PathVariable ("eventid") long eventid)
	{
		EventDetailsResponse response = terminalservice.getEvent(eventid);

		return response;
	}
	
	@PostMapping("/nationalevents")
	@PreAuthorize("hasRole('CLIENT')")
	public List<NationalEventResponse> getNationalEvents ()throws Exception
	{
		List<NationalEventResponse> response = terminalservice.getNationalEvents();
		
		return response;
	}
	
	/**
	@PostMapping("/riskalert")
	@PreAuthorize("hasRole('CLIENT')")
	public RiskAlertResponse getRiskAlert(@CurrentUser UserPrincipal userprincipal)
	{
		RiskAlertResponse response = terminalservice.getRiskAlerts(userprincipal);
		
		return response;
		
	}
	**/
	
	@PostMapping("/riskalert")
	@PreAuthorize("hasRole('CLIENT')")
	public List<RiskAlertResponse> getMultipleRiskAlerts(@CurrentUser UserPrincipal userprincipal)throws Exception
	{
		List<RiskAlertResponse> response = terminalservice.getMultipleRiskAlerts(userprincipal);
		return response;
	}
	
	@PostMapping("/notification")
	@PreAuthorize("hasRole('CLIENT')")
	public List<RiskAlertResponse> getEventNotification(@CurrentUser UserPrincipal userprincipal)throws Exception
	{
		List<RiskAlertResponse> response = terminalservice.getEventNotification(userprincipal);
		return response;
	}
	
	@PostMapping("/riskalertdetails/{alertid}")
	@PreAuthorize("hasRole('CLIENT')")
	public RiskAlertDetailsResponse getRiskAlertDetails(@CurrentUser UserPrincipal userprincipal, @PathVariable("alertid") long alertid)
	{
		RiskAlertDetailsResponse riskalert = riskalertservice.getRiskAlert(alertid);
		return riskalert;
		
		
	}

	/*
	@PostMapping("/archiveevents")
	@PreAuthorize("hasRole('CLIENT')")
	public List<ArchiveEventsResponseObject> getArchiveEvents(@CurrentUser UserPrincipal userprincipal,@Valid @RequestBody ArchiveEventsRequestObject request)throws Exception
	{
		List<ArchiveEventsResponseObject> response = terminalservice.getArchiveEvents(userprincipal,request);
		
		return response;
	}
	
	/*/
	@PostMapping("/archiveevents/{pageno}")
	@PreAuthorize("hasRole('CLIENT')")
	public PagedResponse<ArchiveEventsResponseObject> getArchiveEventsNew(@CurrentUser UserPrincipal userprincipal,@Valid @RequestBody ArchiveEventsRequestObject request,@PathVariable("pageno") int pageno)throws Exception
	{
		  
		
		return terminalservice.getArchiveEventsNew(userprincipal,request,pageno);
	}
	
	
	
	@PostMapping("/pdfreport/{eventid}")
	@PreAuthorize("hasRole('CLIENT')")
	public ResponseEntity<InputStreamResource> downloadPdfEvent(@CurrentUser UserPrincipal userprincipal,@PathVariable("eventid") long eventid) throws IOException, DocumentException
	{
		
		ByteArrayInputStream bis = pdfgenerater.generateEventReport(eventid);
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=EventReport.pdf");

        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
	}
	
	@PostMapping("/topbar")
	@PreAuthorize("hasRole('CLIENT')")
	public List<TopBarResponseObject> getTopBar(@CurrentUser UserPrincipal userprincipal,@Valid @RequestBody TerminalRequestObject request)
	{
		List<TerminalResponseObjet> teminalobject = terminalservice.getTerminalEvents(request,userprincipal);
		List<TopBarResponseObject> response = terminalservice.getTopBar(teminalobject);
		
		return response;
	}
	

}
