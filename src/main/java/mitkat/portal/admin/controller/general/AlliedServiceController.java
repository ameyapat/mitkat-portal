package mitkat.portal.admin.controller.general;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mitkat.portal.admin.model.clientprofile.Country;
import mitkat.portal.admin.model.clientprofile.ProductType;
import mitkat.portal.admin.model.clientprofile.State;
import mitkat.portal.admin.model.creator.ImpactIndustry;
import mitkat.portal.admin.model.creator.RegionType;
import mitkat.portal.admin.model.creator.RiskCategory;
import mitkat.portal.admin.model.creator.RiskLevel;
import mitkat.portal.admin.model.creator.Status;
import mitkat.portal.admin.model.user.Role;
import mitkat.portal.admin.model.user.User;
import mitkat.portal.admin.repository.application.CityRepository;
import mitkat.portal.admin.repository.application.ClientEmailIdsRepository;
import mitkat.portal.admin.repository.application.Countryrepository;
import mitkat.portal.admin.repository.application.ImpactIndustryRepository;
import mitkat.portal.admin.repository.application.ProductTypeRepository;
import mitkat.portal.admin.repository.application.RegionTypeRepository;
import mitkat.portal.admin.repository.application.RiskCategoryRepository;
import mitkat.portal.admin.repository.application.RiskLevelRepository;
import mitkat.portal.admin.repository.application.StateRepository;
import mitkat.portal.admin.repository.application.StatusRepository;
import mitkat.portal.admin.repository.user.UserRepository;
import mitkat.portal.admin.responseObjects.approver.ClientListResponse;
import mitkat.portal.admin.responseObjects.approver.CreatorApproverListResponse;
import mitkat.portal.admin.responseObjects.approver.RiskTrackerType;
import mitkat.portal.admin.service.RiskTrackerTypeService;
import mitkat.portal.admin.model.clientprofile.City;

@RestController
@RequestMapping("/api/alliedservices")
public class AlliedServiceController {

	@Autowired
	ClientEmailIdsRepository clientemailrepository;
	
	@Autowired
	Countryrepository countryrepository;
	
	@Autowired
	StateRepository staterepository;
	
	@Autowired
	CityRepository cityrepository;
	
	@Autowired
	ProductTypeRepository producttyperepository;
	
	@Autowired
	ImpactIndustryRepository impactindustryrepository;
	
	@Autowired
	RegionTypeRepository regiontyperepository;
	
	@Autowired 
	RiskCategoryRepository riskcategoryrepository;
	
	@Autowired
	RiskLevelRepository risklevelrepository;
	
	@Autowired
	StatusRepository statusrepository;
	
	@Autowired
	RiskTrackerTypeService risktrackertypeservice;
	
	@Autowired
	UserRepository userrepository;
	
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(AlliedServiceController.class);

	@PostMapping("/country")
	public List<Country> getAllCountries()throws Exception
	{return countryrepository.findAll();}

	@PostMapping("/state")
	public List<State> getAllStates()throws Exception
	{return staterepository.findAll();}
	
	@PostMapping("/city")
	public List<City> getAllCities()throws Exception
	{ return cityrepository.findAll();}

	@PostMapping("/producttype")
	public List<ProductType> getAllProductTypes()throws Exception
	{return producttyperepository.findAll();}
	
	@PostMapping("/impactindustrytype")
	public List<ImpactIndustry> getAllImpactIndustries()throws Exception
	{return impactindustryrepository.findAll();}

	@PostMapping("/regiontype")
	public List<RegionType> getAllRegionType()throws Exception
	{return regiontyperepository.findAll();}
	
	@PostMapping("/riskcategory")
	public List<RiskCategory> getAllRiskCategory()throws Exception
	{return riskcategoryrepository.findAll();}
	
	@PostMapping("/risklevel")
	public List<RiskLevel> getAllRiskLevel()throws Exception
	{return risklevelrepository.findAll();}
	
	@PostMapping("/status")
	public List<Status> getAllStatus()throws Exception
	{return statusrepository.findAll();}
	
	@PostMapping("/risktrackertypes")
	public List<RiskTrackerType> getAllTypes()throws Exception
	{
		return risktrackertypeservice.getAll();
	}
	
	@PostMapping("/creatorlist")
	public List<CreatorApproverListResponse> getCreatorApproverList()throws Exception
	{
		List<User> users = userrepository.findAll();
		List<CreatorApproverListResponse> response = new ArrayList<>();
		for(User temp:users)
		{
			int i=0;
			for(Role temprole:temp.getRoles())
			{	
				if(temprole.getId() ==3 || temprole.getId() ==4)
				{
					i = i+1;
				}	
			}
			
			if(i>0)
			{
				CreatorApproverListResponse tempresponse = new CreatorApproverListResponse();
				tempresponse.setId(temp.getId());
				tempresponse.setUsername(temp.getName());
				response.add(tempresponse);
			}
		}
		CreatorApproverListResponse tempresponse = new CreatorApproverListResponse();
		tempresponse.setId(0);
		tempresponse.setUsername("Select All");
		response.add(tempresponse);
		return response;
	}
	
	 @PostMapping("/clientlist")
	    public List<ClientListResponse> getClientList()
	    {
	    	List<ClientListResponse> response = new ArrayList<>();
	    	List<User> users = userrepository.findAll();
	    	
	    	for(User temp:users)
			{
				int i=0;
				for(Role temprole:temp.getRoles())
				{	
					if(temprole.getId() ==1)
					{
						i = i+1;
					}	
				}
				
				if(i>0)
				{
					ClientListResponse tempresponse = new ClientListResponse();
					tempresponse.setId(temp.getId());
					tempresponse.setClientName(temp.getName());
					response.add(tempresponse);
				}
			}
	    	
	    	return response;
	    	
	    }
	
	
}
