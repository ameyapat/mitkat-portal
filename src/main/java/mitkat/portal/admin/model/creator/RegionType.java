package mitkat.portal.admin.model.creator;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name="regiontype")
public class RegionType {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	long id;
	
	@NotBlank
	@Size(max=20)
	String regiontype;
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="regionType",cascade=CascadeType.ALL)
	@Fetch(FetchMode.SELECT)
	private List<Report> report;

	@OneToMany(fetch=FetchType.LAZY, mappedBy="regionType1",cascade=CascadeType.ALL)
	@Fetch(FetchMode.SELECT)
	private List<RiskAlert> riskalert;
	
	
	public long getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRegiontype() {
		return regiontype;
	}

	public void setRegiontype(String regiontype) {
		this.regiontype = regiontype;
	}

	

	public void setReport(List<Report> report) {
		this.report = report;
	}


	public void setRiskalert(List<RiskAlert> riskalert) {
		this.riskalert = riskalert;
	}
	
	
	
}
