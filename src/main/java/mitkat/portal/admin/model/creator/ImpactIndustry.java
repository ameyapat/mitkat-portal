package mitkat.portal.admin.model.creator;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="impactindustry")
public class ImpactIndustry {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	
	private String industryname;


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getIndustryname() {
		return industryname;
	}


	public void setIndustryname(String industryname) {
		this.industryname = industryname;
	}
	
	
	
	
}
