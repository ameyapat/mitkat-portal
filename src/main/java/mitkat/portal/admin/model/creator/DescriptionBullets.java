package mitkat.portal.admin.model.creator;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="descriptionbullet")
public class DescriptionBullets {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	private String bulletpoint;
	
	@ManyToOne(cascade= {CascadeType.DETACH,CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH})
	@JoinColumn(name="description_bullet")
	private Report reportdescbullet;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getBulletpoint() {
		return bulletpoint;
	}

	public void setBulletpoint(String bulletpoint) {
		this.bulletpoint = bulletpoint;
	}

	public Report getReportdescbullet() {
		return reportdescbullet;
	}

	public void setReportdescbullet(Report reportdescbullet) {
		this.reportdescbullet = reportdescbullet;
	}
	
	
	
	
}
