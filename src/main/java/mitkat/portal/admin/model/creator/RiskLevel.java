package mitkat.portal.admin.model.creator;

import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;


@Entity
@Table(name="risklevel")
public class RiskLevel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	long id;
	
	@NotBlank
	@Size(max=20)
	String level;
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="riskLevel",cascade=CascadeType.ALL)
	@Fetch(FetchMode.SELECT)
	private List<Report> report;
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="riskLevel1",cascade=CascadeType.ALL)
	@Fetch(FetchMode.SELECT)
	private List<RiskAlert> riskalert;
	
	
	public long getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public void setReport(List<Report> report) {
		this.report = report;
	}


	public void setRiskalert(List<RiskAlert> riskalert) {
		this.riskalert = riskalert;
	}

	public void setId(long id) {
		this.id = id;
	}
	
//ddsfsd
	
	
	
}
