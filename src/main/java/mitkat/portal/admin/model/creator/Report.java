package mitkat.portal.admin.model.creator;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import mitkat.portal.admin.auditObjects.UserDateAudit;
import mitkat.portal.admin.model.clientprofile.City;
import mitkat.portal.admin.model.clientprofile.Country;
import mitkat.portal.admin.model.clientprofile.State;

@Entity
@Table(name="report")
@JsonIgnoreProperties({"hibernateLazyInitializer"})
public class Report extends UserDateAudit {

	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotNull
	private String title;
	
	private String description;
	private String description2;
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="reportdescbullet",cascade=CascadeType.ALL)
	@Fetch(FetchMode.SELECT)
	private List<DescriptionBullets> descriptionbullets;
	
	
	private String background;
	private String background2;
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="reportbackbullet",cascade=CascadeType.ALL)
	@Fetch(FetchMode.SELECT)
	private List<BackgroundBullets> backgroundbullets;
	
	private String impact;
	private String impact2;
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="impactbullet",cascade=CascadeType.ALL)
	@Fetch(FetchMode.SELECT)
	private List<ImpactBullets> impactbullets;
	
	private String recommendation;
	private String recommendation2;
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="recommendationbullet",cascade=CascadeType.ALL)
	@Fetch(FetchMode.SELECT)
	private List<RecommendationBullet> recommendationbullets;
	
	
	private String links;
	
	private double latitude;
	
	private double longitude;
	
	private boolean importance;
	
	private boolean liveevent;
	
	private boolean riskalert;
	
	//@Temporal(TemporalType.DATE)
	private Instant storydate;
	
	//@Temporal(TemporalType.DATE)
	private Instant validitydate;
	
	@ManyToOne(cascade= {CascadeType.DETACH,CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH})
	@JoinColumn(name="user_risklevel")
	private RiskLevel riskLevel;

	@ManyToOne(cascade= {CascadeType.DETACH,CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH})
	@JoinColumn(name="user_riskcategory")
	private RiskCategory riskCategory;
	
	@ManyToOne(cascade= {CascadeType.DETACH,CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH})
	@JoinColumn(name="user_regiontype")
	private RegionType regionType;
	
	@ManyToOne(cascade= {CascadeType.DETACH,CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH})
	@JoinColumn(name="user_status")
	private Status status;
	
	@ManyToMany(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    @JoinTable(name = "report_impactindustry",
            joinColumns = @JoinColumn(name = "report_id"),
            inverseJoinColumns = @JoinColumn(name = "impactindustry_id")) 
	private List<ImpactIndustry> impactindustry = new ArrayList<ImpactIndustry>();
	
	@ManyToMany(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    @JoinTable(name = "report_country",
            joinColumns = @JoinColumn(name = "report_id"),
            inverseJoinColumns = @JoinColumn(name = "country_id")) 
	private List<Country> countries = new ArrayList<Country>();
	
	@ManyToMany(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    @JoinTable(name = "report_state",
            joinColumns = @JoinColumn(name = "report_id"),
            inverseJoinColumns = @JoinColumn(name = "state_id")) 
	private List<State> state = new ArrayList<State>();
	
	@ManyToMany(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    @JoinTable(name = "report_city",
            joinColumns = @JoinColumn(name = "report_id"),
            inverseJoinColumns = @JoinColumn(name = "city_id")) 
	private List<City> cities = new ArrayList<City>();
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="reportupdate",cascade=CascadeType.ALL)
	@Fetch(FetchMode.SELECT)
	private List<Update> updates;
	
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getBackground() {
		return background;
	}

	public void setBackground(String background) {
		this.background = background;
	}

	public String getImpact() {
		return impact;
	}

	public void setImpact(String impact) {
		this.impact = impact;
	}

	public String getRecommendation() {
		return recommendation;
	}

	public void setRecommendation(String recommendation) {
		this.recommendation = recommendation;
	}

	public String getLinks() {
		return links;
	}

	public void setLinks(String links) {
		this.links = links;
	}

	

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Instant getStorydate() {
		return storydate;
	}

	public void setStorydate(Instant storydate) {
		this.storydate = storydate;
	}

	public Instant getValiditydate() {
		return validitydate;
	}

	public void setValiditydate(Instant validitydate) {
		this.validitydate = validitydate;
	}

	public RiskLevel getRiskLevel() {
		return riskLevel;
	}

	public void setRiskLevel(RiskLevel riskLevel) {
		this.riskLevel = riskLevel;
	}

	public RiskCategory getRiskCategory() {
		return riskCategory;
	}

	public void setRiskCategory(RiskCategory riskCategory) {
		this.riskCategory = riskCategory;
	}

	public RegionType getRegionType() {
		return regionType;
	}

	public void setRegionType(RegionType regionType) {
		this.regionType = regionType;
	}

	public List<ImpactIndustry> getImpactindustry() {
		return impactindustry;
	}

	public void setImpactindustry(List<ImpactIndustry> impactindustry) {
		this.impactindustry = impactindustry;
	}

	public List<Country> getCountries() {
		return countries;
	}

	public void setCountries(List<Country> countries) {
		this.countries = countries;
	}

	public List<State> getState() {
		return state;
	}

	public void setState(List<State> state) {
		this.state = state;
	}

	public List<City> getCities() {
		return cities;
	}

	public void setCities(List<City> cities) {
		this.cities = cities;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public boolean isImportance() {
		return importance;
	}

	public void setImportance(boolean importance) {
		this.importance = importance;
	}

	public boolean isLiveevent() {
		return liveevent;
	}

	public void setLiveevent(boolean liveevent) {
		this.liveevent = liveevent;
	}

	public boolean isRiskalert() {
		return riskalert;
	}

	public void setRiskalert(boolean riskalert) {
		this.riskalert = riskalert;
	}
	
	public List<Update> getUpdates() {
		return updates;
	}

	public void setUpdates(List<Update> updates) {
		this.updates = updates;
	}
	
	

	public String getDescription2() {
		return description2;
	}

	public void setDescription2(String description2) {
		this.description2 = description2;
	}

	public List<DescriptionBullets> getDescriptionbullets() {
		return descriptionbullets;
	}

	public void setDescriptionbullets(List<DescriptionBullets> descriptionbullets) {
		this.descriptionbullets = descriptionbullets;
	}

	public String getBackground2() {
		return background2;
	}

	public void setBackground2(String background2) {
		this.background2 = background2;
	}

	public String getImpact2() {
		return impact2;
	}

	public void setImpact2(String impact2) {
		this.impact2 = impact2;
	}

	public String getRecommendation2() {
		return recommendation2;
	}

	public void setRecommendation2(String recommendation2) {
		this.recommendation2 = recommendation2;
	}

	
	
	public List<BackgroundBullets> getBackgroundbullets() {
		return backgroundbullets;
	}

	public void setBackgroundbullets(List<BackgroundBullets> backgroundbullets) {
		this.backgroundbullets = backgroundbullets;
	}
	
	
	

	public List<ImpactBullets> getImpactbullets() {
		return impactbullets;
	}

	public void setImpactbullets(List<ImpactBullets> impactbullets) {
		this.impactbullets = impactbullets;
	}

	
	
	public List<RecommendationBullet> getRecommendationbullets() {
		return recommendationbullets;
	}

	public void setRecommendationbullets(List<RecommendationBullet> recommendationbullets) {
		this.recommendationbullets = recommendationbullets;
	}

	public void addUpdate(Update update)
	{
		if(updates == null)
		{updates = new ArrayList<>();}
		updates.add(update);
		update.setReportupdate(this);
		
	}
	
	public void addDescriptionBullet(DescriptionBullets tempbullet)
	{
		if(descriptionbullets == null)
		{descriptionbullets = new ArrayList<>();}
		descriptionbullets.add(tempbullet);
		tempbullet.setReportdescbullet(this);
		
	}
	
	public void addBackgroundBullet(BackgroundBullets tempbullet)
	{
		if(backgroundbullets == null)
		{backgroundbullets = new ArrayList<>();}
		backgroundbullets.add(tempbullet);
		tempbullet.setReportbackbullet(this);
		
	}
	
	public void addImpactBullet(ImpactBullets tempbullet)
	{
		if(impactbullets == null)
		{impactbullets = new ArrayList<>();}
		impactbullets.add(tempbullet);
		tempbullet.setImpactbullet(this);
		
	}
	
	
	public void addRecommendationBullet(RecommendationBullet tempbullet)
	{
		if(recommendationbullets == null)
		{recommendationbullets = new ArrayList<>();}
		recommendationbullets.add(tempbullet);
		tempbullet.setRecommendationbullet(this);
		
	}
	
}
