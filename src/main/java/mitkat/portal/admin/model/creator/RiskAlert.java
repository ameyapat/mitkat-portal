package mitkat.portal.admin.model.creator;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import mitkat.portal.admin.auditObjects.UserDateAudit;
import mitkat.portal.admin.model.clientprofile.City;
import mitkat.portal.admin.model.clientprofile.Country;
import mitkat.portal.admin.model.clientprofile.State;

@Entity
@Table(name="riskalert")
@JsonIgnoreProperties({"hibernateLazyInitializer"})
public class RiskAlert extends UserDateAudit{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotNull
	private String title;
		
	private String links;
	
	private Double latitude;
	
	private Double longitude;
		
	//@Temporal(TemporalType.DATE)
	private Instant alertdate;
	
	@ManyToOne(cascade= {CascadeType.DETACH,CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH})
	@JoinColumn(name="riskalert_risklevel")
	private RiskLevel riskLevel1;

	@ManyToOne(cascade= {CascadeType.DETACH,CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH})
	@JoinColumn(name="riskalert_riskcategory")
	private RiskCategory riskCategory1;
	
	@ManyToOne(cascade= {CascadeType.DETACH,CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH})
	@JoinColumn(name="riskalert_regiontype")
	private RegionType regionType1;
	
	@ManyToOne(cascade= {CascadeType.DETACH,CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH})
	@JoinColumn(name="riskalert_status")
	private Status status1;
	
	@ManyToMany(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    @JoinTable(name = "riskalert_impactindustry",
            joinColumns = @JoinColumn(name = "riskalert_id"),
            inverseJoinColumns = @JoinColumn(name = "impactindustry_id")) 
	private List<ImpactIndustry> impactindustry = new ArrayList<ImpactIndustry>();
	
	@ManyToMany(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    @JoinTable(name = "riskalert_country",
            joinColumns = @JoinColumn(name = "riskalert_id"),
            inverseJoinColumns = @JoinColumn(name = "country_id")) 
	private List<Country> countries = new ArrayList<Country>();
	
	@ManyToMany(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    @JoinTable(name = "riskalert_state",
            joinColumns = @JoinColumn(name = "riskalert_id"),
            inverseJoinColumns = @JoinColumn(name = "state_id")) 
	private List<State> state = new ArrayList<State>();
	
	@ManyToMany(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    @JoinTable(name = "riskalert_city",
            joinColumns = @JoinColumn(name = "riskalert_id"),
            inverseJoinColumns = @JoinColumn(name = "city_id")) 
	private List<City> cities = new ArrayList<City>();
	

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLinks() {
		return links;
	}

	public void setLinks(String links) {
		this.links = links;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	

	public List<ImpactIndustry> getImpactindustry() {
		return impactindustry;
	}

	public void setImpactindustry(List<ImpactIndustry> impactindustry) {
		this.impactindustry = impactindustry;
	}

	public List<Country> getCountries() {
		return countries;
	}

	public void setCountries(List<Country> countries) {
		this.countries = countries;
	}

	public List<State> getState() {
		return state;
	}

	public void setState(List<State> state) {
		this.state = state;
	}

	public List<City> getCities() {
		return cities;
	}

	public void setCities(List<City> cities) {
		this.cities = cities;
	}

	public Instant getAlertdate() {
		return alertdate;
	}

	public void setAlertdate(Instant alertdate) {
		this.alertdate = alertdate;
	}

	public RiskLevel getRiskLevel1() {
		return riskLevel1;
	}

	public void setRiskLevel1(RiskLevel riskLevel1) {
		this.riskLevel1 = riskLevel1;
	}

	public RiskCategory getRiskCategory1() {
		return riskCategory1;
	}

	public void setRiskCategory1(RiskCategory riskCategory1) {
		this.riskCategory1 = riskCategory1;
	}

	public RegionType getRegionType1() {
		return regionType1;
	}

	public void setRegionType1(RegionType regionType1) {
		this.regionType1 = regionType1;
	}

	public Status getStatus1() {
		return status1;
	}

	public void setStatus1(Status status1) {
		this.status1 = status1;
	}

	
	
}
