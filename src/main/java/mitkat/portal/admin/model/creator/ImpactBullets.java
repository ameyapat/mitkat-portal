package mitkat.portal.admin.model.creator;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="impactbullet")
public class ImpactBullets {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	private String bulletpoint;
	
	@ManyToOne(cascade= {CascadeType.DETACH,CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH})
	@JoinColumn(name="impact_bullet")
	private Report impactbullet;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getBulletpoint() {
		return bulletpoint;
	}

	public void setBulletpoint(String bulletpoint) {
		this.bulletpoint = bulletpoint;
	}

	public Report getImpactbullet() {
		return impactbullet;
	}

	public void setImpactbullet(Report impactbullet) {
		this.impactbullet = impactbullet;
	}
	
	
	
	
}
