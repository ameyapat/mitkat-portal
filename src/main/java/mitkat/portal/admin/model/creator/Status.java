package mitkat.portal.admin.model.creator;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name="status")
public class Status {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	private String status;
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="status",cascade=CascadeType.ALL)
	@Fetch(FetchMode.SELECT)
	private List<Report> reportstatus;

	@OneToMany(fetch=FetchType.LAZY, mappedBy="status1",cascade=CascadeType.ALL)
	@Fetch(FetchMode.SELECT)
	private List<RiskAlert> riskalert;
	
	public long getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


	public void setReportstatus(List<Report> reportstatus) {
		this.reportstatus = reportstatus;
	}



	public void setRiskalert(List<RiskAlert> riskalert) {
		this.riskalert = riskalert;
	}
	
	
	
	
}
