package mitkat.portal.admin.model.creator;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name="riskcategory")
public class RiskCategory {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	long id;
	
	@NotBlank
	@Size(max=20)
	String riskcategory;
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="riskCategory",cascade=CascadeType.ALL)
	@Fetch(FetchMode.SELECT)
	private List<Report> report;

	@OneToMany(fetch=FetchType.LAZY, mappedBy="riskCategory1",cascade=CascadeType.ALL)
	@Fetch(FetchMode.SELECT)
	private List<RiskAlert> riskalert;
	
	public long getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRiskcategory() {
		return riskcategory;
	}

	public void setRiskcategory(String riskcategory) {
		this.riskcategory = riskcategory;
	}

	public void setReport(List<Report> report) {
		this.report = report;
	}

	public void setRiskalert(List<RiskAlert> riskalert) {
		this.riskalert = riskalert;
	}
	
	

}
