package mitkat.portal.admin.model.clientprofile;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import mitkat.portal.admin.model.creator.ImpactIndustry;

@Entity
@Table(name="clientdetails")
public class ClientProfile {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name="clientid")
	private long clientid;
	
	@Column(name="companyname")
	private String companyname;
	
	@Column(name="contractdate")
	@Temporal(TemporalType.DATE)
	private Date contractdate;
	
	@Temporal(TemporalType.DATE)
	private Date contractenddate;
	
	@Column(name="contactnumber")
	private String contactnumber;
	
	@Column(name="contactperson")
	private String contactperson;
	
	@Lob @Basic(fetch = FetchType.LAZY)
	@Column(name="logo", nullable=false)
	private byte[] logo;
	
	@Column(name="filename")
	private String filename;
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="profileemail",cascade=CascadeType.ALL)
	@Fetch(FetchMode.SELECT)
	private List<ClientEmailIds> clientemail; 
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="profilelocations",cascade=CascadeType.ALL)
	@Fetch(FetchMode.SELECT)
	private List<OfficeLocation> officelocations;
	
	@ManyToMany(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    @JoinTable(name = "profile_country",
            joinColumns = @JoinColumn(name = "clientprofile_id"),
            inverseJoinColumns = @JoinColumn(name = "country_id")) 
	private List<Country> countries = new ArrayList<Country>();
	
	@ManyToMany(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    @JoinTable(name = "profile_state",
            joinColumns = @JoinColumn(name = "clientprofile_id"),
            inverseJoinColumns = @JoinColumn(name = "state_id")) 
	private List<State> state = new ArrayList<State>();
	
	@ManyToMany(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    @JoinTable(name = "profile_city",
            joinColumns = @JoinColumn(name = "clientprofile_id"),
            inverseJoinColumns = @JoinColumn(name = "city_id")) 
	private List<City> cities = new ArrayList<City>();
	
	@ManyToMany(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    @JoinTable(name = "profile_producttype",
            joinColumns = @JoinColumn(name = "clientprofile_id"),
            inverseJoinColumns = @JoinColumn(name = "producttype_id")) 
	private List<ProductType> producttype = new ArrayList<ProductType>();

	@ManyToMany(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    @JoinTable(name = "profile_impactindustry",
            joinColumns = @JoinColumn(name = "clientprofile_id"),
            inverseJoinColumns = @JoinColumn(name = "impactindustry_id")) 
	private List<ImpactIndustry> impactindustry = new ArrayList<ImpactIndustry>();
	
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getClientid() {
		return clientid;
	}

	public void setClientid(long clientid) {
		this.clientid = clientid;
	}

	public String getCompanyname() {
		return companyname;
	}

	public void setCompanyname(String companyname) {
		this.companyname = companyname;
	}

	public Date getContractdate() {
		return contractdate;
	}

	public void setContractdate(Date contractdate) {
		this.contractdate = contractdate;
	}

	public String getContactnumber() {
		return contactnumber;
	}

	public void setContactnumber(String contactnumber) {
		this.contactnumber = contactnumber;
	}

	public String getContactperson() {
		return contactperson;
	}

	public void setContactperson(String contactperson) {
		this.contactperson = contactperson;
	}

	public byte[] getLogo() {
		return logo;
	}

	public void setLogo(byte[] logo) {
		this.logo = logo;
	}

	

	public List<ClientEmailIds> getClientemail() {
		return clientemail;
	}

	public void setClientemail(List<ClientEmailIds> clientemail) {
		this.clientemail = clientemail;
	}

	public List<OfficeLocation> getOfficelocations() {
		return officelocations;
	}

	public void setOfficelocations(List<OfficeLocation> officelocations) {
		this.officelocations = officelocations;
	}

	public List<Country> getCountries() {
		return countries;
	}

	public void setCountries(List<Country> countries) {
		this.countries = countries;
	}

	public List<State> getState() {
		return state;
	}

	public void setState(List<State> state) {
		this.state = state;
	}

	public List<City> getCities() {
		return cities;
	}

	public void setCities(List<City> cities) {
		this.cities = cities;
	}

	public List<ProductType> getProducttype() {
		return producttype;
	}

	public void setProducttype(List<ProductType> producttype) {
		this.producttype = producttype;
	}
	
	public Date getContractenddate() {
		return contractenddate;
	}

	public void setContractenddate(Date contractenddate) {
		this.contractenddate = contractenddate;
	}
	
	public List<ImpactIndustry> getImpactindustry() {
		return impactindustry;
	}

	public void setImpactindustry(List<ImpactIndustry> impactindustry) {
		this.impactindustry = impactindustry;
	}
	
	
	
	

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public void addEmailIds(ClientEmailIds emailid)
	{
		if(clientemail == null)
		{clientemail = new ArrayList<>();}
		clientemail.add(emailid);
		emailid.setProfileemail(this);
	
	}
	
	public void addOfficeLocations (OfficeLocation templocation)
	{
		if(officelocations == null)
		{officelocations = new ArrayList <>();}
		officelocations.add(templocation);
		templocation.setProfilelocations(this);
	}
	
	
	
}
