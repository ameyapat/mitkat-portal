package mitkat.portal.admin.model.clientprofile;

import javax.persistence.*;
import javax.validation.constraints.Email;


@Entity
@Table(name="clientemail")
public class ClientEmailIds {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Email
    private String email;
	
	@ManyToOne(cascade= {CascadeType.DETACH,CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH})
	@JoinColumn(name="client_id")
	private ClientProfile profileemail;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	/**
	public ClientProfile getProfileemail() {
		return profileemail;
	}
**/
	public void setProfileemail(ClientProfile profileemail) {
		this.profileemail = profileemail;
	}
	
	
	
	
}
