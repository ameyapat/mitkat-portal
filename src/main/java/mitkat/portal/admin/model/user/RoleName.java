package mitkat.portal.admin.model.user;

public enum RoleName {

	ROLE_CLIENT,
    ROLE_ADMIN,
    ROLE_CREATOR,
    ROLE_APPROVER
	
}
