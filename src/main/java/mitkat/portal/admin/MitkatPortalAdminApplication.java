package mitkat.portal.admin;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

import mitkat.portal.admin.savefiles.FileStorageProperties;

@SpringBootApplication
@EntityScan(basePackageClasses = { 
		MitkatPortalAdminApplication.class,
		Jsr310JpaConverters.class 
})
@EnableConfigurationProperties({
    FileStorageProperties.class
})
public class MitkatPortalAdminApplication {
	
	@PostConstruct
	  public void init(){
	    // Setting Spring Boot SetTimeZone
	    TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	  }

	public static void main(String[] args) {
		SpringApplication.run(MitkatPortalAdminApplication.class, args);
	}
}
