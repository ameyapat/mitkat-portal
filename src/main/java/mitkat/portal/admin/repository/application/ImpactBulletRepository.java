package mitkat.portal.admin.repository.application;

import org.springframework.data.jpa.repository.JpaRepository;

import mitkat.portal.admin.model.creator.ImpactBullets;

public interface ImpactBulletRepository extends JpaRepository<ImpactBullets, Long>{

}
