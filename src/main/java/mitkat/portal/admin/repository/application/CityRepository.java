package mitkat.portal.admin.repository.application;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import mitkat.portal.admin.model.clientprofile.City;

public interface CityRepository extends JpaRepository<City, Long> {

	City getOne(long cityid);
		
	Optional<City> findByCityname (String cityname);
}
