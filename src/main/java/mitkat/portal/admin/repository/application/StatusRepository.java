package mitkat.portal.admin.repository.application;

import org.springframework.data.jpa.repository.JpaRepository;

import mitkat.portal.admin.model.creator.Status;

public interface StatusRepository extends JpaRepository<Status, Long> {

	
	
}
