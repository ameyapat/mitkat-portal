package mitkat.portal.admin.repository.application;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import mitkat.portal.admin.model.clientprofile.ClientProfile;

public interface ClientProfileRepository extends JpaRepository<ClientProfile, Long> {

	ClientProfile getOne(long clientprofileid);
	
	void deleteById (long clientprofileid);
	
	Optional <ClientProfile> findByclientid (long clientid);

	boolean existsByclientid(long clientid);

	
}
