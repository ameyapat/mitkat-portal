package mitkat.portal.admin.repository.application;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;


import mitkat.portal.admin.model.creator.ImpactIndustry;

public interface ImpactIndustryRepository extends JpaRepository<ImpactIndustry, Long> {

	ImpactIndustry getOne(long id);
	
	Optional<ImpactIndustry> findByIndustryname (String industryname);
	
}
