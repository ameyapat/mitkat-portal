package mitkat.portal.admin.repository.application;

import org.springframework.data.jpa.repository.JpaRepository;

import mitkat.portal.admin.model.creator.RiskLevel;

public interface RiskLevelRepository extends JpaRepository<RiskLevel, Long> {

}
