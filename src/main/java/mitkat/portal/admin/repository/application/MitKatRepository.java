package mitkat.portal.admin.repository.application;

import org.springframework.data.jpa.repository.JpaRepository;

import mitkat.portal.admin.model.user.Mitkat;

public interface MitKatRepository extends JpaRepository<Mitkat, Long> {

}
