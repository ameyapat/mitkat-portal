package mitkat.portal.admin.repository.application;

import java.time.Instant;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import mitkat.portal.admin.model.creator.RiskAlert;

public interface RiskAlertRepository extends JpaRepository<RiskAlert, Long> {

	@Query("SELECT r FROM RiskAlert r where r.alertdate >=:today")
	List<RiskAlert> getTerminalAlerts (@Param("today") Instant today);

	@Query("SELECT r FROM RiskAlert r where r.alertdate >=:date")
	List<RiskAlert> getRiskAlertsForApproval(@Param("date") Instant date);
	
}
