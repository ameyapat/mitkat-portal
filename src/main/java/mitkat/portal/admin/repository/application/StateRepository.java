package mitkat.portal.admin.repository.application;

import org.springframework.data.jpa.repository.JpaRepository;


import mitkat.portal.admin.model.clientprofile.State;

public interface StateRepository extends JpaRepository<State, Long>{

	State getOne (long stateid);
}
