package mitkat.portal.admin.repository.application;

import org.springframework.data.jpa.repository.JpaRepository;

import mitkat.portal.admin.model.creator.RegionType;

public interface RegionTypeRepository extends JpaRepository<RegionType, Long> {

}
