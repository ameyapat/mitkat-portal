package mitkat.portal.admin.repository.application;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import mitkat.portal.admin.model.clientprofile.Country;

public interface Countryrepository extends JpaRepository<Country, Long> {

	Country getOne(long countryid);
	
	Optional<Country> findByCountryname (String countryname);
	
	
}
