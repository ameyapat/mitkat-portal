package mitkat.portal.admin.repository.application;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import mitkat.portal.admin.model.user.TokenMap;

public interface TokenRepository extends JpaRepository<TokenMap, Long>{

	List<TokenMap> findByUserId(Long userId);
	List<TokenMap> findByToken(String token);
	
	
}
