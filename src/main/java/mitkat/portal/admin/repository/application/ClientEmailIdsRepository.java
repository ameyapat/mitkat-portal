package mitkat.portal.admin.repository.application;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import mitkat.portal.admin.model.clientprofile.ClientEmailIds;

public interface ClientEmailIdsRepository extends JpaRepository<ClientEmailIds,Long> {

	ClientEmailIds getOne(long id);
	
	Optional<ClientEmailIds> findByemail (String email);
	
	 void deleteById(long id);
	
}
