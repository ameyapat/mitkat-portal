package mitkat.portal.admin.repository.application;

import org.springframework.data.jpa.repository.JpaRepository;

import mitkat.portal.admin.model.creator.BackgroundBullets;


public interface BackgroundBulletsRepository extends JpaRepository<BackgroundBullets, Long>{

}
