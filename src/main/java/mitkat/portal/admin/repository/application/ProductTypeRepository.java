package mitkat.portal.admin.repository.application;

import org.springframework.data.jpa.repository.JpaRepository;

import mitkat.portal.admin.model.clientprofile.ProductType;

public interface ProductTypeRepository extends JpaRepository<ProductType, Long> {

	
	ProductType getOne (Long producttypeid);
}
