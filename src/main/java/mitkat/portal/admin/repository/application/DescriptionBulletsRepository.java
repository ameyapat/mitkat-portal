package mitkat.portal.admin.repository.application;

import org.springframework.data.jpa.repository.JpaRepository;

import mitkat.portal.admin.model.creator.DescriptionBullets;

public interface DescriptionBulletsRepository extends JpaRepository<DescriptionBullets, Long> {

}
