package mitkat.portal.admin.repository.application;






import java.time.Instant;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import mitkat.portal.admin.model.creator.Report;

public interface ReportRepository extends JpaRepository<Report, Long> {

	 
	
	@Query("SELECT r FROM Report r where r.createdBy = :createdBy and r.status.id = :status and r.storydate BETWEEN :startdate and :enddate")
	Page<Report> filterReportCST(Pageable pageable, @Param("createdBy")long creator, @Param("status")long status, @Param("startdate")Instant date, @Param("enddate")Instant date2);

	@Query("SELECT r FROM Report r where r.createdBy = :createdBy and r.status.id = :status")
	Page<Report> filterReportCS(Pageable pageable, @Param("createdBy")long creator, @Param("status")long status);
	
	@Query("SELECT r FROM Report r where r.createdBy = :createdBy and r.storydate BETWEEN :startdate and :enddate")
	Page<Report> filterReportCT(Pageable pageable, @Param("createdBy")long creator, @Param("startdate")Instant date, @Param("enddate")Instant date2);
	
	@Query("SELECT r FROM Report r where r.status.id = :status and r.storydate BETWEEN :startdate and :enddate")
	Page<Report> filterReportST(Pageable pageable, @Param("status")long status, @Param("startdate")Instant date, @Param("enddate")Instant date2);
	
	@Query("SELECT r FROM Report r where r.createdBy = :createdBy")
	Page<Report> filterReportC(Pageable pageable, @Param("createdBy")long creator);
	
	@Query("SELECT r FROM Report r where r.status.id = :status")
	Page<Report> filterReportS(Pageable pageable, @Param("status")long status);

	@Query("SELECT r FROM Report r where r.storydate BETWEEN :startdate and :enddate")
	Page<Report> filterReportT(Pageable pageable, @Param("startdate")Instant date, @Param("enddate")Instant date2);
	
	@Query("SELECT r FROM Report r where r.storydate >=:today")
	List<Report> getTodaysReports (@Param("today") Instant today);
	
	@Query("SELECT r FROM Report r where r.storydate BETWEEN :today and :enddate")
	List<Report> getTodaysReportsNew (@Param("today") Instant today, @Param("enddate") Instant enddate);
	
	@Query("SELECT r FROM Report r where r.validitydate >=:today")
	List<Report> getTerminalReports (@Param("today") Instant today);

	@Query(nativeQuery = true , value="SELECT * FROM report r "
			+ "where "
			+ "user_status = :status")
	List<Report> getUnapprovedReports(@Param("status") Long status) ;

	@Query(nativeQuery = true , value="SELECT * FROM report r "
			+ "where "
			+ "user_status = :status and "
			+ "validitydate BETWEEN :StartDate and :EndDate and "
			+ "user_risklevel IN :risklevels and "
			+ "user_riskcategory IN :riskcategories and "
			+ "(id IN (SELECT report_id FROM report_country rc INNER JOIN "
			+ "country c ON rc.country_id = c.id where id IN :countries) or "
			+ "id IN (SELECT report_id FROM report_state rs INNER JOIN "
			+ "state s ON rs.state_id = s.id where id IN :states) or "
			+ "id IN (SELECT report_id FROM report_city rt INNER JOIN "
			+ "city t ON rt.city_id = t.id where id IN :cities))")
	Page<Report> getTerminalReportsNew (Pageable pageable, @Param("StartDate") Instant StartDate,@Param("EndDate") Instant EndDate, @Param("risklevels")List<Long> risklevels,
			@Param("status") Long status, @Param("riskcategories")List<Long> riskcategories,@Param("countries") List<Long> countries,@Param("states") List<Long> states, @Param("cities")List<Long> cities);

	@Query(nativeQuery = true , value="SELECT * FROM report r "
			+ "where "
			+ "user_status = :status and "
			+ "validitydate BETWEEN :StartDate and :EndDate and "
			+ "user_risklevel IN :risklevels and "
			+ "user_riskcategory IN :riskcategories and "
			+ "( "
			+ "id IN (SELECT report_id FROM report_city rt INNER JOIN "
			+ "city t ON rt.city_id = t.id where id IN :cities))")
	Page<Report> getTerminalReportsNew1(Pageable pageable, @Param("StartDate") Instant StartDate,@Param("EndDate") Instant EndDate, @Param("risklevels")List<Long> risklevels,
			@Param("status") Long status, @Param("riskcategories")List<Long> riskcategories, @Param("cities")List<Long> cities);

	@Query(nativeQuery = true , value="SELECT * FROM report r "
			+ "where "
			+ "user_status = :status and "
			+ "validitydate BETWEEN :StartDate and :EndDate and "
			+ "user_risklevel IN :risklevels and "
			+ "user_riskcategory IN :riskcategories and "
			+ "( "
			+ "id IN (SELECT report_id FROM report_state rs INNER JOIN "
			+ "state s ON rs.state_id = s.id where id IN :states))")
	Page<Report> getTerminalReportsNew2(Pageable pageable, @Param("StartDate") Instant StartDate,@Param("EndDate") Instant EndDate, @Param("risklevels")List<Long> risklevels,
			@Param("status") Long status, @Param("riskcategories")List<Long> riskcategories, @Param("states") List<Long> states);

	@Query(nativeQuery = true , value="SELECT * FROM report r "
			+ "where "
			+ "user_status = :status and "
			+ "validitydate BETWEEN :StartDate and :EndDate and "
			+ "user_risklevel IN :risklevels and "
			+ "user_riskcategory IN :riskcategories")
	Page<Report> getTerminalReportsNew3(Pageable pageable,@Param("StartDate") Instant StartDate,@Param("EndDate") Instant EndDate, @Param("risklevels")List<Long> risklevels,
			@Param("status") Long status, @Param("riskcategories")List<Long> riskcategories);

	@Query(nativeQuery = true , value="SELECT * FROM report r "
			+ "where "
			+ "user_status = :status and "
			+ "validitydate BETWEEN :StartDate and :EndDate and "
			+ "user_risklevel IN :risklevels and "
			+ "user_riskcategory IN :riskcategories and "
			+ "( "
			+ "id IN (SELECT report_id FROM report_state rs INNER JOIN "
			+ "state s ON rs.state_id = s.id where id IN :states) or "
			+ "id IN (SELECT report_id FROM report_city rt INNER JOIN "
			+ "city t ON rt.city_id = t.id where id IN :cities))")
	Page<Report> getTerminalReportsNew4(Pageable pageable, @Param("StartDate") Instant StartDate,@Param("EndDate") Instant EndDate, @Param("risklevels")List<Long> risklevels,
			@Param("status") Long status, @Param("riskcategories")List<Long> riskcategories,@Param("states") List<Long> states, @Param("cities")List<Long> cities);

	@Query(nativeQuery = true , value="SELECT * FROM report r "
			+ "where "
			+ "user_status = :status and "
			+ "validitydate BETWEEN :StartDate and :EndDate and "
			+ "user_risklevel IN :risklevels and "
			+ "user_riskcategory IN :riskcategories and "
			+ "(id IN (SELECT report_id FROM report_country rc INNER JOIN "
			+ "country c ON rc.country_id = c.id where id IN :countries) or "
			+ "id IN (SELECT report_id FROM report_city rt INNER JOIN "
			+ "city t ON rt.city_id = t.id where id IN :cities))")
	Page<Report> getTerminalReportsNew5(Pageable pageable, @Param("StartDate") Instant StartDate,@Param("EndDate") Instant EndDate, @Param("risklevels")List<Long> risklevels,
			@Param("status") Long status, @Param("riskcategories")List<Long> riskcategories,@Param("countries") List<Long> countries,@Param("cities")List<Long> cities);

	@Query(nativeQuery = true , value="SELECT * FROM report r "
			+ "where "
			+ "user_status = :status and "
			+ "validitydate BETWEEN :StartDate and :EndDate and "
			+ "user_risklevel IN :risklevels and "
			+ "user_riskcategory IN :riskcategories and "
			+ "(id IN (SELECT report_id FROM report_country rc INNER JOIN "
			+ "country c ON rc.country_id = c.id where id IN :countries) or "
			+ "id IN (SELECT report_id FROM report_state rs INNER JOIN "
			+ "state s ON rs.state_id = s.id where id IN :states))")
	Page<Report> getTerminalReportsNew6(Pageable pageable, @Param("StartDate") Instant StartDate,@Param("EndDate") Instant EndDate, @Param("risklevels")List<Long> risklevels,
			@Param("status") Long status, @Param("riskcategories")List<Long> riskcategories,@Param("countries") List<Long> countries,@Param("states") List<Long> states);

	@Query(nativeQuery = true , value="SELECT * FROM report r "
			+ "where "
			+ "user_status = :status and "
			+ "validitydate BETWEEN :StartDate and :EndDate and "
			+ "user_risklevel IN :risklevels and "
			+ "user_riskcategory IN :riskcategories and "
			+ "(id IN (SELECT report_id FROM report_country rc INNER JOIN "
			+ "country c ON rc.country_id = c.id where id IN :countries))")
	Page<Report> getTerminalReportsNew7(Pageable pageable, @Param("StartDate") Instant StartDate,@Param("EndDate") Instant EndDate, @Param("risklevels")List<Long> risklevels,
			@Param("status") Long status, @Param("riskcategories")List<Long> riskcategories,@Param("countries") List<Long> countries);

	
	
	
	
	
	
	
}
