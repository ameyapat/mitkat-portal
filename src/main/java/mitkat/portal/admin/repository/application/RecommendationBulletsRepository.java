package mitkat.portal.admin.repository.application;

import org.springframework.data.jpa.repository.JpaRepository;


import mitkat.portal.admin.model.creator.RecommendationBullet;

public interface RecommendationBulletsRepository extends JpaRepository<RecommendationBullet, Long> {

}
