<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Alert Emailer</title>
</head>

<body>

<table style=" border-collapse: collapse;  width: 80%;">


  <tr>
  <td>
  <table style=" border-collapse: collapse;  width: 100%;">
  
  <tr>
    <td colspan="7" style="background:#004d91;padding:8.5pt 8.5pt 8.5pt 8.5pt">
    
    <p><span style="font-size:18.0pt;font-family:&quot;Cambria Math&quot;,serif;color:white">RISK TRACKER</span></p>
        <p><span style="font-size:14.0pt;font-family:&quot;Cambria Math&quot;,serif;color:white">${date}</span></p>               
    </td>
    <td colspan="1" style=" background:#0f6fbc;padding:0cm 0cm 0cm 0cm;">&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  </td>
    <td colspan="1" style="background:#009c31;padding:0cm 0cm 0cm 0cm;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
    <td colspan="1" style="background:#2dc25c;padding:0cm 0cm 0cm 0cm;">&nbsp;&nbsp;&nbsp;&nbsp;</td>
    <td colspan="2" style="background:#f2f2f2;padding:0cm 0cm 0cm 0cm;">
    <p align="center" style="background:#f2f2f2; margin-bottom:4.0pt;text-align:center"><span style="font-size:12.0pt;font-family:&quot;Cambria Math&quot;,&quot;serif&quot;color:#1f497d">
        <#if logoadd == 1>
        <img width="132" height="56" src="cid:logo.png" alt="mitkat" class="CToWUd">
        <#elseif logoadd == 2 >
        <img width="132" height="56" src="cid:clientlogo.png" alt="mitkat" class="CToWUd">
        </#if>
        </span><u></u><u></u></p>
    
    </td>
  </tr>
  
  
  </table>
  </td>
  </tr>
  
  
  <tr>
    <td>
    
    
      
                
            <table style="width:100%; border-collapse: collapse; table-layout:fixed;">  
              <tr>
                
                <td colspan="10" style="background:#f2f2f2; text-align:center; border-top:solid #d9d9d9 1.0pt;border-left:solid #d9d9d9 1.0pt;border-bottom:solid #d9d9d9 1.0pt;border-right:solid #d9d9d9 1.0pt; padding:4pt 8.5pt 4pt 8.5pt;height:26.85pt;"><span style="font-family:Calibri ;font-size:14.0pt; color:#0070c0;"> <b> ${titles.title} </b></span></td>
                
                <td colspan="1" style='text-align:center; border-top:solid #d9d9d9 1.0pt;border-left:solid #d9d9d9 1.0pt;border-bottom:solid #d9d9d9 1.0pt;border-right:solid #d9d9d9 1.0pt;width:60px'>
                
                <#if titles.riskcategiry == 1>
            		<img width="55" height="55" src="cid:cyber.png" alt="mitkat" class="CToWUd">
            		<#elseif titles.riskcategiry == 2>
            		<img width="55" height="55" src="cid:terror.png" alt="mitkat" class="CToWUd">
            		<#elseif titles.riskcategiry == 3>
            		<img width="55" height="55" src="cid:civil.png" alt="mitkat" class="CToWUd">
            		<#elseif titles.riskcategiry == 4>
            		<img width="55" height="55" src="cid:environment.png" alt="mitkat" class="CToWUd">
            		<#elseif titles.riskcategiry == 5>
            		<img width="55" height="55" src="cid:health.png" alt="mitkat" class="CToWUd">
            		<#elseif titles.riskcategiry == 6>
            		<img width="55" height="55" src="cid:infra.png" alt="mitkat" class="CToWUd">
            		<#elseif titles.riskcategiry == 7>
            		<img width="55" height="55" src="cid:crime.png" alt="mitkat" class="CToWUd">
            		</#if>
            		
                 </td>
                
                <#if titles.risklevel == 1>
                <td  colspan ='1' style='border-top:solid #d9d9d9 1.0pt;border-left:solid #d9d9d9 1.0pt;border-bottom:solid #d9d9d9 1.0pt;border-right:solid #d9d9d9 1.0pt;background:#c5e0b3;padding:0cm 8.5pt 0cm 8.5pt;height:26.85pt;width:30px'><p><b><span style="color:white">VERY LOW</span></b><u></u><u></u></p></td>
            <#elseif titles.risklevel == 2>
            <td  colspan ='1' style='border-top:solid #d9d9d9 1.0pt;border-left:solid #d9d9d9 1.0pt;border-bottom:solid #d9d9d9 1.0pt;border-right:solid #d9d9d9 1.0pt;background:#ffe599;padding:0cm 8.5pt 0cm 8.5pt;height:26.85pt;width:30px'><p><b><span style="color:white">LOW</span></b><u></u><u></u></p></td>
          <#elseif titles.risklevel == 3>
            <td  colspan ='1' style='border-top:solid #d9d9d9 1.0pt;border-left:solid #d9d9d9 1.0pt;border-bottom:solid #d9d9d9 1.0pt;border-right:solid #d9d9d9 1.0pt;background:#f4b083;padding:0cm 8.5pt 0cm 8.5pt;height:26.85pt;width:30px'><p><b><span style="color:white">MEDIUM</span></b><u></u><u></u></p></td>
          <#elseif titles.risklevel == 4>
            <td  colspan ='1' style='border-top:solid #d9d9d9 1.0pt;border-left:solid #d9d9d9 1.0pt;border-bottom:solid #d9d9d9 1.0pt;border-right:solid #d9d9d9 1.0pt;background:#ed7d31;padding:0cm 8.5pt 0cm 8.5pt;height:26.85pt;width:30px'><p><b><span style="color:white">HIGH</span></b><u></u><u></u></p></td>
          <#elseif titles.risklevel == 5>
            <td  colspan ='1' style='border-top:solid #d9d9d9 1.0pt;border-left:solid #d9d9d9 1.0pt;border-bottom:solid #d9d9d9 1.0pt;border-right:solid #d9d9d9 1.0pt;background:#c45911;padding:0cm 8.5pt 0cm 8.5pt;height:26.85pt;width:30px'><p><b><span style="color:white">VERY HIGH</span></b><u></u><u></u></p></td>
          </#if>
              </tr>
              </table>
              <table style="width:100%; border-collapse: collapse; table-layout:fixed;"> 
                  <#if titles.discid == 1>
                <tr>
                  <td colspan ="2" style='padding:8.5px 8.5px 8.5px 8.5px;background:#f2f2f2; text-align:center; border-top:solid #d9d9d9 1.0pt;border-left:solid #d9d9d9 1.0pt;border-bottom:solid #d9d9d9 1.0pt;border-right:solid #d9d9d9 1.0pt;'><b><span style="font-family:Calibri ;color:#0070c0"> DESCRIPTION </span></b><span style="color:#00b050"></span></td>
                <td colspan = "10" style='padding:8.5px 8.5px 8.5px 8.5px; border-top:solid #d9d9d9 1.0pt;border-left:solid #d9d9d9 1.0pt;border-bottom:solid #d9d9d9 1.0pt;border-right:solid #d9d9d9 1.0pt;'><p style="margin: 0cm 0cm 0.0001pt; caret-color: rgb(213, 213, 213);font-family:Calibri ;align="justify"; text-size-adjust: auto;">
                ${titles.discription}
                
                <#if titles.discid2 == 1>
            		<br>
            		${titles.description2}
            	</#if>	
            	
            	<#if titles.discidbullet == 1>	
            		<br>
            		<ul>
            		<#list titles.descriptionbullets as bullet>	
            		<li>${bullet}</li>
            		</#list>
            		</ul>
            	</#if>	
            		
                <span style="color:#00b050"></span></p></td>
                </tr>
                </#if>
                
                <#if titles.backid == 1>
              <tr>
                <td colspan ="2" style='padding:8.5px 8.5px 8.5px 8.5px;background:#f2f2f2; text-align:center; border-top:solid #d9d9d9 1.0pt;border-left:solid #d9d9d9 1.0pt;border-bottom:solid #d9d9d9 1.0pt;border-right:solid #d9d9d9 1.0pt;'><b><span style="font-family:Calibri ;color:#0070c0"> BACKGROUND </span></b><span style="color:#00b050"></span></td>
                <td colspan = "10" style='padding:8.5px 8.5px 8.5px 8.5px; border-top:solid #d9d9d9 1.0pt;border-left:solid #d9d9d9 1.0pt;border-bottom:solid #d9d9d9 1.0pt;border-right:solid #d9d9d9 1.0pt;'><p style="margin: 0cm 0cm 0.0001pt; caret-color: rgb(190, 190, 190);font-family:Calibri;align="justify"; text-size-adjust: auto;">
                ${titles.background}
                
                <#if titles.backid2 == 1>	
                <br>
            		${titles.background2}
				</#if>
				
				<#if titles.backidbullet == 1>
            		<br>
            		<ul>
            		<#list titles.backgroundbullets as bullet>	
            		<li>${bullet}</li>
            		</#list>
            		</ul>
                </#if>
                
                <span style="color:#00b050"></span></p></td>
              </tr>
                </#if>
                
                
                    <#if titles.impactid == 1>
                <tr>
                    <td colspan="2" style='padding:8.5px 8.5px 8.5px 8.5px;background:#f2f2f2;text-align:center; border-top:solid #d9d9d9 1.0pt;border-left:solid #d9d9d9 1.0pt;border-bottom:solid #d9d9d9 1.0pt;border-right:solid #d9d9d9 1.0pt;'><b><span style="font-family:Calibri ;color:#0070c0">IMPACT ANALYSIS</span></b><span style="color:#00b050"></span></td>
                    <td colspan="10" style="padding:8.5px 8.5px 8.5px 8.5px; border-top:solid #d9d9d9 1.0pt;border-left:solid #d9d9d9 1.0pt;border-bottom:solid #d9d9d9 1.0pt;border-right:solid #d9d9d9 1.0pt;"><p style="margin: 0cm 0cm 0.0001pt; caret-color: rgb(128, 128, 128);font-family:Calibri;align="justify"; text-size-adjust: auto;">
                    ${titles.impact}
                
                	<#if titles.impactid2 == 1>
                    <br>
                    ${titles.impact2}
                    </#if>
                    
                    <#if titles.impactidbullet == 1>
            		<br>
            		<ul>
            		<#list titles.impactbullets as bullet>	
            		<li>${bullet}</li>
            		</#list>
            		</ul>
            		</#if>
            		
                    <span style="color:#00b050"></span></p></td>
              </tr>
                </#if>
                
                <#if titles.recommid == 1>
                <tr>
                <td colspan="2" style='padding:8.5px 8.5px 8.5px 8.5px;background:#f2f2f2;text-align:center; border-top:solid #d9d9d9 1.0pt;border-left:solid #d9d9d9 1.0pt;border-bottom:solid #d9d9d9 1.0pt;border-right:solid #d9d9d9 1.0pt;'><b><span style="font-family:Calibri ;color:#0070c0">RECOMMENDATIONS</span></b><span style="color:#00b050"></span></td>
                <td colspan="10" style="padding:8.5px 8.5px 8.5px 8.5px; border-top:solid #d9d9d9 1.0pt;border-left:solid #d9d9d9 1.0pt;border-bottom:solid #d9d9d9 1.0pt;border-right:solid #d9d9d9 1.0pt;"> <p style="margin: 0cm 0cm 0.0001pt; caret-color: rgb(213, 213, 213);font-family:Calibri;align="justify"; text-size-adjust: auto;">
                ${titles.recommendation}
               
                <#if titles.recommid2 == 1>
                <br>
            		${titles.recommendation2}
            	</#if>	
            		
            	 <#if titles.recommidbullet == 1>	
            		<br>
            		<ul>
            		<#list titles.recommendationbullets as bullet>	
            		<li>${bullet}</li>
            		</#list>
            		</ul>
            	</#if>	
                <span style="color:#00b050"></span></p></td>
                </tr>
                </#if>
                
                
                <tr>
                <td colspan ="12" style='border-top:none;border-left:solid #d9d9d9 1.0pt;border-bottom:none;border-right:solid #d9d9d9 1.0pt;background:#737373;padding:0cm 8.5pt 0cm 8.5pt; height:5pt;'></td>
                </tr>
              </table>  
              
              
    </td>
  </tr>
  
  
  
  
   <#if signatureadd == 1>
            <tr>
                <td   style="border-top:none;border-left:solid #d9d9d9 1.0pt;border-bottom:none;border-right:solid #d9d9d9 1.0pt;background:#d9d9d9;padding:0cm 8.5pt 0cm 8.5pt">
                    <h1 style="margin-top:0cm"></h1>
                </td>
            </tr>
            
            <tr>
                <td  style="border-top:none;border-left:solid #d9d9d9 1.0pt;border-bottom:none;border-right:solid #d9d9d9 1.0pt;padding:8.5pt 8.5pt 8.5pt 8.5pt;background:#d9d9d9;">
                    <p class="MsoNormal" style="text-align:justify"><b><span style="font-size:10.0pt">©2018 All rights reserved. The information contained herein is the Intellectual Property of MitKat Advisory Services Pvt. Ltd. Any unauthorized use of this content, in any form, violates our rights.</span></b><u></u><u></u></p>
                    <p class="MsoNormal" style="text-align:justify">_____________________________________________________</p>
                    <p class="MsoNormal" style="text-align:justify">Warm Regards,</p>
                    <p class="MsoNormal" style="text-align:justify"><b>Information Services Team</b></p>
                    <p class="MsoNormal" style="text-align:justify"><b><span style="color:#00b0f0">MitKat Advisory Services Pvt. Ltd.</span></b><u></u><u></u></p>
                    <p class="MsoNormal" style="text-align:justify"><a href="http://www.mitkatadvisory.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=http://www.mitkatadvisory.com/&amp;source=gmail&amp;ust=1526471072879000&amp;usg=AFQjCNEknYWuCU_qlCCaHZ3qCbDSQQnAvA">www.mitkatadvisory.com</a></p>
                    <p class="MsoNormal" style="text-align:justify"> </p>
                    <div class="MsoNormal" align="center" style="text-align:justify"><b><span>Follow Us On</span></b><span>&nbsp;</span><a href="https://urldefense.proofpoint.com/v1/url?u=https://twitter.com/MitKat_Advisory&amp;k=wdHsQuqY0Mqq1fNjZGIYnA%3D%3D&amp;r=Fea/2czlQalpTO8Ys2zJVF6M3lM21vyG1wWrwRXx6cM%3D&amp;m=dos2tJnRu9tKFAw/sJ0yEmE9/Fgrf1KfW79LmcvT7N4%3D&amp;s=6e0f40669dc4c8ee9b355c92e024e1914ce553314b59b2fc8c171a3597abaaa7" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://urldefense.proofpoint.com/v1/url?u%3Dhttps://twitter.com/MitKat_Advisory%26k%3DwdHsQuqY0Mqq1fNjZGIYnA%253D%253D%26r%3DFea/2czlQalpTO8Ys2zJVF6M3lM21vyG1wWrwRXx6cM%253D%26m%3Ddos2tJnRu9tKFAw/sJ0yEmE9/Fgrf1KfW79LmcvT7N4%253D%26s%3D6e0f40669dc4c8ee9b355c92e024e1914ce553314b59b2fc8c171a3597abaaa7&amp;source=gmail&amp;ust=1516344460840000&amp;usg=AFQjCNHXNITriR3SbTDYsoDvvJfx80Ia5g"><b><span style="color:windowtext;text-decoration:none">                       <img border="0" width="18" height="18" style="width:.1916in;height:.1916in" id="m_4740183638205490023Picture_x0020_9" src="cid:twitter.png" alt="Description: escription: Description: Description: Description: Description: Desc" class="CToWUd"></span></b></a><a href="https://urldefense.proofpoint.com/v1/url?u=http://www.linkedin.com/company/2295134?trk%3Dtyah&amp;k=wdHsQuqY0Mqq1fNjZGIYnA%3D%3D&amp;r=Fea/2czlQalpTO8Ys2zJVF6M3lM21vyG1wWrwRXx6cM%3D&amp;m=dos2tJnRu9tKFAw/sJ0yEmE9/Fgrf1KfW79LmcvT7N4%3D&amp;s=3cc48285dcf2ce3dce3316ff82fa2070a81dd87cd0d71686d1f55372f50a35ec" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://urldefense.proofpoint.com/v1/url?u%3Dhttp://www.linkedin.com/company/2295134?trk%253Dtyah%26k%3DwdHsQuqY0Mqq1fNjZGIYnA%253D%253D%26r%3DFea/2czlQalpTO8Ys2zJVF6M3lM21vyG1wWrwRXx6cM%253D%26m%3Ddos2tJnRu9tKFAw/sJ0yEmE9/Fgrf1KfW79LmcvT7N4%253D%26s%3D3cc48285dcf2ce3dce3316ff82fa2070a81dd87cd0d71686d1f55372f50a35ec&amp;source=gmail&amp;ust=1516344460840000&amp;usg=AFQjCNFvNn19HSgTX5qjYQMf-eQRplkIww"><b><span style="color:windowtext;text-decoration:none">                       <img border="0" width="18" height="18" style="width:.1916in;height:.1916in" id="m_4740183638205490023Picture_x0020_10" src="http://admin.risktracker.co.in/images/mailformat/client_116_linkedin.jpg" alt="Description: escription: Description: Description: Description: Description: Desc" class="CToWUd"></span></b></a><a href="https://www.facebook.com/mitkatglobal?ref=ts&amp;fref=ts" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://www.facebook.com/mitkatglobal?ref%3Dts%26fref%3Dts&amp;source=gmail&amp;ust=1516344460840000&amp;usg=AFQjCNHeSdtrQtl8gC81jjMS4M8BrOurmA"><span style="color:windowtext;text-decoration:none">                       <img border="0" width="18" height="18" style="width:.1916in;height:.1916in" id="m_4740183638205490023Picture_x0020_11" src="http://admin.risktracker.co.in/images/mailformat/client_116_fb.jpg" alt="Description: escription: Description: Description: Description: Description: Desc" class="CToWUd"></span></a><span><u></u><u></u></span> </div> <u></u><u></u>
                    <p></p>
                    <p class="MsoNormal" style="text-align:justify"><b>Our Services:</b></p>
                    <p class="MsoNormal" style="text-align:justify"> <a href="https://www.mitkatadvisory.com/information-services" target="_blank">Information Services</a> &nbsp;&nbsp; |&nbsp; &nbsp; <a href="https://www.mitkatadvisory.com/security-consulting-and-design" target="_blank">Security Consulting and Design</a> &nbsp;&nbsp; <b>|<i>&nbsp;&nbsp;&nbsp;&nbsp;</i></b> <a href="https://www.mitkatadvisory.com/cyber-security-and-resilience" target="_blank">Cyber Security and Resilience</a><b><i> </i></b> </p>
                    <p class="MsoNormal" style="text-align:justify"> <a href="https://www.mitkatadvisory.com/workForce-development" target="_blank">Workforce Development</a><b><i> &nbsp;&nbsp;</i>|<i>&nbsp;&nbsp;&nbsp;&nbsp;</i></b> <a href="https://www.mitkatadvisory.com/women-empowerment" target="_blank">Women Empowerment</a><b><i>&nbsp;&nbsp;&nbsp;</i>|<i>&nbsp; &nbsp;</i></b> <a href="https://www.mitkatadvisory.com/integrity-risk-management" target="_blank">Integrity Risk  Management</a><b><i>&nbsp;&nbsp;&nbsp;</i>|<i>&nbsp; &nbsp;</i></b> <a href="https://www.mitkatadvisory.com/managed-services" target="_blank">Managed Services</a></p>
                    <p class="MsoNormal" style="text-align:justify">&nbsp;<u></u><u></u></p>
                    <p class="MsoNormal" style="text-align:justify">
                        <a href="https://urldefense.proofpoint.com/v1/url?u=http://www.mitkatadvisory.com/&amp;                         k=wdHsQuqY0Mqq1fNjZGIYnA%3D%3D%0A&amp;r=Fea%2F2czlQalpTO8Ys2zJVF6M3lM21vyG1wWrwR                         Xx6cM%3D%0A&amp;m=dos2tJnRu9tKFAw%2FsJ0yEmE9%2FFgrf1KfW79LmcvT7N4%3D%0A&amp;s=                         eb0b7abe5c3b1f5e9bea9a26e48ca93cb8aa0048ed570ff8d7545a2661edf23d" target="_blank"> <span style="color:windowtext;text-decoration:none">                                                      <img border="0" width="860" height="99" src="cid:footer.png" alt="cription: escription: Description: Description: Description: Descr" class="CToWUd">                          </span> </a>
                    </p>
                    <p class="MsoNormal" style="text-align:justify"><span style="font-size:10.0pt">&nbsp;</span></p>
                    <p class="MsoNormal" style="text-align:justify"><span style="font-size:9.0pt">DISCLAIMER <br>The contents of this E-mail (including the contents of the enclosure/(s) or attachment/(s) if any) are privileged and confidential material of MitKat Advisory Services Pvt. Ltd. and should not be disclosed to, used by or copied in any manner by anyone other than the intended addressee/(s). If this E-mail (including the enclosure/(s) or attachment/(s) if any) has been received in error, please advise the sender immediately and delete it from your system.</span><u></u><u></u></p>
                </td>
            </tr>
    </#if>
  
  
  
</table>
</body>
</html>